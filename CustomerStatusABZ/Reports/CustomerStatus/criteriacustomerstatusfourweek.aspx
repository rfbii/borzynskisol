<%@ Page language="c#" Inherits="CustomerStatus.Reports.CustomerStatus.CriteriaCustomerStatusFourWeek" Codebehind="CriteriaCustomerStatusFourWeek.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>CustomerStatusInput</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="C#" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
    <body oncontextmenu="return false;">
	<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
			<table align="center">
				<tr>
					<td align="center">
						Customer Status Four Week Report</td>
				</tr>
			</table>
			<table align="center">
				<tr>
					<td style="HEIGHT: 54px"><asp:Label id="lbCustomer" runat="server">Customer:</asp:Label></td>
					<td style="HEIGHT: 54px"><asp:TextBox id="txtCustomer" runat="server" LookupSearch="CustomerSingleCustomerStatus" SingleLookup=""></asp:TextBox></td>
					<td style="HEIGHT: 54px">
						<asp:ListBox id="customerList" Font-Names="Courier New" runat="server" AutoPostBack="True" Width="387px"
							Height="200px" onselectedindexchanged="customerList_SelectedIndexChanged"></asp:ListBox></td>
				</tr>
				<tr>
					<td colspan="3">
						<asp:CheckBoxList ID="CommoditiesCheckBoxList" runat="server" AutoPostBack="True"></asp:CheckBoxList>
					</td>
				</tr>
                </div>
				<tr>
					<td></td>
					<td>
						<asp:Button id="cmdRun" runat="server" Text="Run Report" onclick="cmdRun_Click"></asp:Button></td>
					<td><asp:button id="cmdReport" runat="server" Text="Return to Reports Menu" onclick="cmdReport_Click"></asp:button></td>
				</tr>
			</table>
			<asp:Label id="Message" runat="server" ForeColor="Red">Message</asp:Label>
		<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
		</form>
	</body>
</html>
