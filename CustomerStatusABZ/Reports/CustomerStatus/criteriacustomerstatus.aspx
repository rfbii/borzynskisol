<%@ Page language="c#" Inherits="CustomerStatus.Reports.CustomerStatus.CriteriaCustomerStatus" Codebehind="CriteriaCustomerStatus.aspx.cs" %>
<%@ Register Assembly="KeyCentralABZLib" Namespace="KeyCentralLib" TagPrefix="cc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>CustomerStatusInput</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C#" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
    <body oncontextmenu="return false;">
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
			<script type="text/vbscript" language="VBScript">
			Function doConfirm(msg)
				if MsgBox(msg,vbYesNo +vbDefaultButton2) = vbYes then
				doConfirm = true
				else
				doConfirm = false
				end if
			End function
			</script>
			<script type="text/javascript" language="javascript">
				function AskIfFieldsBlank(dateFieldName)
				{
					if(document.getElementById(dateFieldName +"_minDate").value =="" && document.getElementById(dateFieldName +"_maxDate").value =="" && document.getElementById("txtOrderNumber").value =="" && document.getElementById("txtCustomerPO").value =="" && document.getElementById("txtCustomer").value =="")
						return doConfirm('Are you sure you want to run this report without any criteria?');
					else
						return true;
				}
			</script>
			<table align="center">
				<tr>
					<td align="center">Customer Status Report</td>
				</tr>
				<tr>
					<td align="center" colspan="2">
                        <cc1:datecriteria id="ShipDate" runat="server"></cc1:datecriteria>
                    </td>
				</tr>
			</table>
			<table align="center">
				<tr>
					<td><asp:label id="lbOrderNumber" runat="server">Order Number:</asp:label></td>
					<td><asp:textbox id="txtOrderNumber" runat="server" EnterAsTab=""></asp:textbox></td>
					<td></td>
				</tr>
				<tr>
					<td><asp:label id="lbCustomerPO" runat="server">Customer PO:</asp:label></td>
					<td><asp:textbox id="txtCustomerPO" runat="server" EnterAsTab=""></asp:textbox></td>
					<td></td>
				</tr>
				<tr>
					<td align="right"><asp:label id="lbCustomer" runat="server">Customer:</asp:label></td>
					<td><asp:textbox id="txtCustomer" runat="server" SingleLookup="customerId_Key" LookupSearch="CustomerSingle" EnterAsTab=""></asp:textbox>
						<input type="hidden" id="customerId_Key" runat="server" name="customerId_Key"/></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3">
						<asp:CheckBoxList ID="CommoditiesCheckBoxList" runat="server" AutoPostBack="True"></asp:CheckBoxList>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><asp:button id="cmdRun" runat="server" Text="Run Report" TabFinish="" EnterAsTab="" onclick="cmdRun_Click"></asp:button></td>
					<td><asp:button id="cmdReport" runat="server" Text="Return to Reports Menu" onclick="cmdReport_Click"></asp:button></td>
				</tr>
			</table>
			<asp:Label id="Message" runat="server" ForeColor="Red">Message</asp:Label>
			</div>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
			</form>
	</body>
</html>
