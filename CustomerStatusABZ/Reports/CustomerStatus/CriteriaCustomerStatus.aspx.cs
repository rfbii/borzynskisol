using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Oracle.DataAccess.Client;
using System.Data.SqlClient;
using System.Data.OleDb; 
using KeyCentral.Lookups;
using KeyCentral.Functions;

namespace CustomerStatus.Reports.CustomerStatus
{
	/// <summary>
	/// Summary description for CriteriaCustomerStatus.
	/// </summary>
	public partial class CriteriaCustomerStatus :  BasePage
	{
		#region Declares
		protected ReportSessionManager reportSessionManager;
		#endregion

		#region Param Properties
		private bool AllCheckBoxesSelected { get { return (bool)Session["AllCheckBoxesSelected"]; } }
		#endregion
		
		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Customer Status Report") == false)
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Customer Status");

			#region SessionManager
			reportSessionManager = new ReportSessionManager(this,Request["ReportName"].ToString());
			
			//Load Vars from session if this report is in SessionManager
			if(IsPostBack ==false && reportSessionManager.CheckReportVars())
			{
				txtOrderNumber.Text = (string)Session["OrderNumber"];
				ShipDate.MinDate = (string)Session["MinShipDate"];
				ShipDate.MaxDate = (string)Session["MaxShipDate"];
				txtCustomerPO.Text = (string)Session["CustomerPO"];
				txtCustomer.Text = (string)Session["Customer"];
			}
			#endregion
			
			cmdRun.Attributes.Add("onClick","return AskIfFieldsBlank('ShipDate');");
			ShipDate.Description = "Ship";

			if (CheckSecurity("Progressive") == true && IsAdmin() == false)
			{
				lbCustomer.Visible = false;
				txtCustomer.Text = "PROPRO";
				txtCustomer.Visible = false;
			}

			if(IsPostBack ==false)
			{
				Message.Text ="";

				//Check for returned error messages
				if (Request.QueryString["Records"]== "None")
					Message.Text = "No Records Found.";

				CommoditiesCheckBoxList.DataSource = GetCommodities();
				CommoditiesCheckBoxList.DataTextField = "Name";
				CommoditiesCheckBoxList.DataValueField = "Cmtyidx";
				CommoditiesCheckBoxList.RepeatDirection = RepeatDirection.Horizontal;
				CommoditiesCheckBoxList.RepeatColumns = 4;
				CommoditiesCheckBoxList.CellSpacing = 20;
				CommoditiesCheckBoxList.DataBind();
				CommoditiesCheckBoxList.Items.Insert(0,new ListItem("ALL","-1"));

				#region load selected commodities
				SqlConnection Conn = GetSQLConn();
				SqlCommand selectCMD = new SqlCommand("use CustomerStatus; SELECT Commodity_Key FROM Customer_Status_Commodities WHERE User_Key=@User_Key", Conn);
				selectCMD.Parameters.Add("@User_Key",Session["User_Key"].ToString());
				SqlDataReader myReader = selectCMD.ExecuteReader();

				if(!myReader.Read())
				{
					CommoditiesCheckBoxList.Items[0].Selected = true;
					reportSessionManager.AddSession("AllCheckBoxesSelected", true);
					reportSessionManager.SaveSessions();
				}
				else
				{
					myReader.Close();
					reportSessionManager.AddSession("AllCheckBoxesSelected", false);
					reportSessionManager.SaveSessions();

					myReader = selectCMD.ExecuteReader();
					while(myReader.Read()==true) //Existing Header
					{
						for(int i = 0; i < CommoditiesCheckBoxList.Items.Count; i++)
						{
							if(myReader.GetInt32(0).ToString() == CommoditiesCheckBoxList.Items[i].Value)
								CommoditiesCheckBoxList.Items[i].Selected = true;
						}
					}
				}

				myReader.Close();
				Conn.Close();
				Conn.Dispose();
				#endregion
			}
            if (IsPostBack && Session["CustomerSingle"] != null)
            {
                if (Session["CustomerSingle"].ToString() != "")
                {
                    this.txtCustomer.Text = Misc.ParseTildaString(Session["CustomerSingle"].ToString().Replace("!", "~"), 2);
                    this.customerId_Key.Value = Misc.ParseTildaString(Session["CustomerSingle"].ToString().Replace("!", "~"), 0) + "~" + Misc.ParseTildaString(Session["CustomerSingle"].ToString().Replace("!", "~"), 1);
                }
            }

			//if(IsPostBack)
			//{
			//	CommoditiesCheckBoxList.Items[0].Selected = false;
			//}
		}
		protected void cmdRun_Click(object sender, System.EventArgs e)
		{
			Page.Validate();

			if (Page.IsValid)
			{
				ArrayList Commodities = new ArrayList();
				ArrayList CommoditiesByName = new ArrayList();

				#region save selected commodities
				SqlConnection Conn = GetSQLConn();
				SqlCommand deleteCMD = new SqlCommand("use CustomerStatus; DELETE FROM Customer_Status_Commodities WHERE User_Key=@User_Key", Conn);
				deleteCMD.Parameters.Add("@User_Key",Session["User_Key"].ToString());
				deleteCMD.ExecuteNonQuery();

				for(int i = 1; i < CommoditiesCheckBoxList.Items.Count; i++)
				{
					if(CommoditiesCheckBoxList.Items[i].Selected)
					{
						SqlCommand insertCMD = new SqlCommand("use CustomerStatus; insert into [Customer_Status_Commodities] (User_Key,Commodity_Key) values(@User_Key,@Commodity_Key) Select @@Identity", Conn);
						insertCMD.Parameters.Add("@User_Key",Session["User_Key"].ToString());
						insertCMD.Parameters.Add("@Commodity_Key",CommoditiesCheckBoxList.Items[i].Value);
						insertCMD.ExecuteNonQuery();

						CommodityType commodityType = FindOrAddCommodityType(Commodities,Convert.ToInt32(CommoditiesCheckBoxList.Items[i].Value),CommoditiesCheckBoxList.Items[i].Text);

						CommoditiesByName.Add(new CommodityType(CommoditiesCheckBoxList.Items[i].Text));
					}
				}

				Conn.Close();
				Conn.Dispose();
				#endregion

				//Add Report Vars
				reportSessionManager.AddSession("CustomerStatusCommodities",Commodities);
				reportSessionManager.AddSession("CustomerStatusCommoditiesByName",CommoditiesByName);
				reportSessionManager.AddSession("OrderNumber",this.txtOrderNumber.Text);
				reportSessionManager.AddSession("MinShipDate",ShipDate.MinDate);
				reportSessionManager.AddSession("MaxShipDate",ShipDate.MaxDate);
				reportSessionManager.AddSession("CustomerPO",this.txtCustomerPO.Text);
				reportSessionManager.AddSession("Customer",this.txtCustomer.Text);
				
				//Let the reportSessionManager know that we are done
				reportSessionManager.SaveSessions();

				//Build next page string and redirect to it
				Response.Redirect("ReportCustomerStatus.aspx" + "?LastPage=" + Misc.EncodeQueryString(Request.Path + "?ReportName=" + Request.QueryString["ReportName"]) + "&&ReportName=" + Request["ReportName"]);
			}
			//TODO: Do we need to do anything if the page isn't vaild?
		}

		protected void cmdReport_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Reports/Customer Status");
		}

		private void CommoditiesCheckBoxList_SelectedIndexChanged(object sender, EventArgs e)
		{
			if(CommoditiesCheckBoxList.SelectedIndex == -1)
			{
				CommoditiesCheckBoxList.Items[0].Selected = true;
				reportSessionManager.AddSession("AllCheckBoxesSelected", true);
				reportSessionManager.SaveSessions();
			}
			else if(CommoditiesCheckBoxList.SelectedIndex == 0 && AllCheckBoxesSelected == true)
			{
				CommoditiesCheckBoxList.Items[0].Selected = false;

				reportSessionManager.AddSession("AllCheckBoxesSelected", false);
				reportSessionManager.SaveSessions();
			}
			else if(CommoditiesCheckBoxList.SelectedIndex == 0 && AllCheckBoxesSelected == false)
			{
				CommoditiesCheckBoxList.Items[0].Selected = true;

				for(int i = 1; i < CommoditiesCheckBoxList.Items.Count; i++)
					CommoditiesCheckBoxList.Items[i].Selected = false;

				reportSessionManager.AddSession("AllCheckBoxesSelected", true);
				reportSessionManager.SaveSessions();
			}
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.CommoditiesCheckBoxList.SelectedIndexChanged += new EventHandler(this.CommoditiesCheckBoxList_SelectedIndexChanged);

		}
		#endregion

		#region Display Code
		public string CheckForCommodityCount(object oItem)
		{
//			ReceiveProductAmount val = (ReceiveProductAmount)((RepeaterItem)oItem).DataItem;
//
//			if(val.Week >1 && (val.Week-1) %13 ==0)
//				return "</table></td><td><table><tr><td><font size=2>Week</font></td><td><font size=2>Amount</font></td></tr>";
//			else
//				return "";

			return "";
		}

		public string DisplayCommodityCheckBox(object oItem)
		{
			return "";
		}
		#endregion

		#region Private Helpers
		private CriteriaCustomerStatus.CommodityType FindOrAddCommodityType(ArrayList list,int CommodityKey,string CommodityName)
		{
			CriteriaCustomerStatus.CommodityType tempCommodityType = new CriteriaCustomerStatus.CommodityType(CommodityKey,CommodityName);
			int tempCommodityTypeIdx = list.BinarySearch(tempCommodityType);

			if(tempCommodityTypeIdx <0)
				list.Insert(Math.Abs(tempCommodityTypeIdx)-1,tempCommodityType);
			else
				tempCommodityType = (CriteriaCustomerStatus.CommodityType)list[tempCommodityTypeIdx];

			return tempCommodityType;
		}

		private DataSet GetCommodities()
		{
			DataSet commodities = new DataSet();

			OracleConnection OracleConn = GetOracleConn();
			string oracleSQL = FormatOracle("Select Name, Cmtyidx From #COMPANY_NUMBER#.Ic_Ps_Commodity Order by Name");

			OracleDataAdapter OracleData = new OracleDataAdapter(oracleSQL,OracleConn);
			OracleData.SelectCommand.BindByName = true;
			OracleData.Fill(commodities);

			OracleData.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();

			return commodities;
		}
		#endregion

		#region CBO
		
		public class CommodityType: IComparable
		{
			#region Declares
			public int CommodityKey;
			public string CommodityName;
			#endregion

			#region Constructor
			public CommodityType(int commodityKey)
			{
				CommodityKey = commodityKey;
			}

			public CommodityType(int commodityKey, string commodityName)
			{
				CommodityKey = commodityKey;
				CommodityName = commodityName;
			}

			public CommodityType(string commodityName)
			{
				CommodityName = commodityName;
			}
		
			#endregion

			#region IComparable Members

			public int CompareTo(object obj)
			{
				int tempCompare;
				CommodityType Y = (CommodityType) obj;

				//Compare CommodityKey
				tempCompare = this.CommodityKey.CompareTo(Y.CommodityKey);
				if(tempCompare != 0)
					return tempCompare;

				return 0;
			}

			#endregion
		}
		#endregion
	}
}
