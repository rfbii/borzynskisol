<%@ Page EnableViewState="False" language="c#" Inherits="CustomerStatus.Reports.CustomerStatus.ReportCustomerStatus" Codebehind="ReportCustomerStatus.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
			<!-- Email Start -->
		<title>Customer Status</title>
		<style type="text/css">.ReportTable.TD { FONT-SIZE: 8pt; display: inline }
	.ReportTable.TD.OrderHeader1 { BORDER-TOP: #000000 1px solid; FONT-SIZE: 8pt }
	.ReportTable.TD.OrderHeader2 { FONT-SIZE: 8pt }
	.ReportTable.TD.OrderHeader2Right { BORDER-RIGHT: lightgrey 1px solid; FONT-SIZE: 8pt }
	.ReportTable.TD.OrderHeader2Top { BORDER-TOP: lightgrey 1px solid; FONT-SIZE: 8pt }
	.ReportTable.TD.OrderHeader2TopRight { BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; FONT-SIZE: 8pt }
	.ReportTable.TD.OrderHeader3 { BORDER-TOP: lightgrey 1px solid; FONT-SIZE: 8pt }
	.ReportTable.TD.OrderHeader4 { FONT-SIZE: 8pt; display: inline }
	.ReportTable.TD.OrderHeader4Number { BORDER-LEFT: lightgrey 1px solid; FONT-SIZE: 8pt }
	.ReportTable.TD.DetailLine { BORDER-TOP: lightgrey 1px solid; FONT-WEIGHT: bold; FONT-SIZE: 8pt}
	.ReportTable.TD.DetailLineNumber { BORDER-TOP: lightgrey 1px solid; FONT-WEIGHT: bold; BORDER-LEFT: lightgrey 1px solid; FONT-SIZE: 8pt }
	.ReportTable.TD.SubTotalSoldTop { BORDER-TOP: #000000 1px solid; FONT-SIZE: 8pt }
	.ReportTable.TD.SubTotalSold { FONT-SIZE: 8pt }
	.ReportTable.TD.SubTotalShipTop { BORDER-TOP: #000000 1px solid; FONT-SIZE: 8pt }
	.ReportTable.TD.SubTotalShip { FONT-SIZE: 8pt }
	.ReportTable.TD.OrderFooter { BORDER-TOP: #000000 3px double; FONT-SIZE: 8pt }
	.Title { FONT-WEIGHT: bold; FONT-SIZE: 12pt; COLOR: black; FONT-FAMILY: Arial }
	.Title1 { FONT-WEIGHT: bold; FONT-SIZE: 10pt; FONT-FAMILY: Arial }
		</style>
				<!-- Email End -->
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="/KeystoneCustomerStatus/Inc/_styles.css" type="text/css" rel="stylesheet" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body bgcolor="#ffffff" onload="SetupPrinting();">
		<object id="factory" style="DISPLAY: none" codebase="http://www.meadroid.com/scriptx/ScriptX.cab#Version=6,1,431,2" classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814" viewastext="">
		</object>
		<script type="text/javascript" language="javascript">
		function SetupPrinting()
		{
			factory.printing.footer = "Printed: &d &t&bPage &p of &P&bCustomer Status Report";
			factory.printing.header = "";
			factory.printing.leftMargin = .5;
			factory.printing.rightMargin = .5;
			factory.printing.topMargin = .5;
			factory.printing.bottomMargin = .5;
		}  
		</script>
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"--><!-- Email Start -->
			<table cellspacing="0" cellpadding="0" border="0" width="720" class="ReportTable">
				<tr>
					<td width="120" align="center" colspan="2"><font color="#000000" size="3" face="Tahoma">Invoiced</font></td>
					<td width="120" align="center" colspan="2"><font color="#008000" size="3" face="Tahoma">Shipped</font></td>
					<td width="120" align="center" colspan="2"><font color="#0000ff" size="3" face="Tahoma">Ordered</font></td>
					<td width="120" align="center" colspan="2"><font color="#800080" size="3" face="Tahoma">Projected</font></td>
					<td width="120" bgcolor="#ffff00" align="center" colspan="2"><font color="#000000" size='3' face="Tahoma">Paid</font></td>
					<td width="120" align="center"><font color="#ff0000" size="3" face="Tahoma">Hold</font></td>
				</tr>
			</table>
			<div id="BodyDiv" class="scroll">
			
				<table cellspacing="0" cellpadding="0" border="0" width="720" class="ReportTable">
					<tr>
						<td align="center">
							<table cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td class="Title" align="left" colspan="11">Customer Status Report
								</td>
							</tr>
							</table>
							<table cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td class="Title1" align="left" colspan="11">
									<div id="CommentLine" runat="server"></div>
								</td>
							</tr>
							</table>
						</td>
					</tr>
				</table>
				<table style="COLOR: black" bgcolor="white" class="ReportTable">
					<tr>
						<td></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" width="720" class="ReportTable">
					<asp:repeater id="Repeater1" runat="server">
						<ItemTemplate>
							<%# CheckForChanges(Container)%>
							<%# SubTotalShip(Container)%>
							<%# SubTotalSold(Container)%>
							<%# OrderFooter() %>
							<%# OrderHeader1(Container)%>
							<%# Detail(Container)%>
							<%# CalcTotals(Container)%>
							<%# NewRecord(Container)%>
						</ItemTemplate>
						<FooterTemplate>
							<%# OrderFooter() %>
							<%# SubTotalShip(null)%>
							<%# SubTotalSold(null)%>
							<%# GrandTotal()%>
						</FooterTemplate>
					</asp:repeater>
				</table>
				<!-- Email End -->
			</div>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
		</form>
	</body>
</html>
