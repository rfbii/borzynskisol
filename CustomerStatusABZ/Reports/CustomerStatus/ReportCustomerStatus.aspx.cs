using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using KeyCentral.Functions;
using Oracle.DataAccess.Client;
using System.Collections;

namespace CustomerStatus.Reports.CustomerStatus
{
	public partial class ReportCustomerStatus : BasePage
	{
		#region Declares
		public DateTime dt1;
		public DateTime dt2;
		public DateTime dt3;
		public DateTime dt4;
		protected Repeater Repeater2;
		protected ReportTotalsHelper totalHelper;
        protected bool hasLoaded =false;
		#endregion
	
		#region Param Properties
		private string MinShipDate{get{return Session["MinShipDate"].ToString();}}
		private string MaxShipDate{get{return Session["MaxShipDate"].ToString();}}

		private string OrderNumber{get{return Session["OrderNumber"].ToString();}}
		private string CustomerPO{get{return Session["CustomerPO"].ToString();}}
		private string Customer{get{return Session["Customer"].ToString();}}
		private ArrayList Commodities{get{return (ArrayList)Session["CustomerStatusCommodities"];}}
		private ArrayList CommoditiesByName{get{return (ArrayList)Session["CustomerStatusCommoditiesByName"];}}
		#endregion

		#region Totals
		#region Vars
		private string strLastOrder="";
		private bool bOrderChanged= false;

		private string strLastSold="";
		private bool bSoldChanged= false;
		private double iSoldUnits=0;
		private float fSoldAmount=0.0F;
		private float fSoldReceipt=0.0F;

		private string strLastShip="";
		private bool bShipChanged= false;
		private double iShipUnits=0;
		private float fShipAmount=0.0F;
		private float fShipReceipt=0.0F;

		private double  iTotalUnits=0;
		private float fTotalAmount=0.0F;
		private float fTotalReceipt=0.0F;
		#endregion

		public string CheckForChanges(object oItem)
		{
			if(strLastOrder != GetField(oItem,"SONO"))
				bOrderChanged = true;
			if(strLastSold != GetField(oItem,"SoldToName") + " " + GetField(oItem,"SoldToCity")+ " " + GetField(oItem,"SoldToState")+ " " + GetField(oItem,"SoldToZip"))
				bSoldChanged = true;
			if(strLastShip != GetField(oItem,"ShipToName") + " " + GetField(oItem,"ShipToCity")+ " " + GetField(oItem,"ShipToState")+ " " + GetField(oItem,"ShipToZip"))
				bShipChanged = true;

			return "";
		}
		public string NewRecord(object oItem)
		{
			bOrderChanged=false;
			bSoldChanged=false;
			bShipChanged=false;
			
			if(oItem != null)
			{
				strLastOrder = GetField(oItem,"SONO");
				strLastSold = GetField(oItem,"SoldToName") + " " + GetField(oItem,"SoldToCity")+ " " + GetField(oItem,"SoldToState")+ " " + GetField(oItem,"SoldToZip");
				strLastShip = GetField(oItem,"ShipToName") + " " + GetField(oItem,"ShipToCity")+ " " + GetField(oItem,"ShipToState")+ " " + GetField(oItem,"ShipToZip");
			}
			else
			{
				bOrderChanged=true;
				bSoldChanged=true;
				bShipChanged=true;
			}
			return "";
		}
		public string CalcTotals(object oItem)
		{
			if (GetField(oItem,"ArLineIdx") == "")
			{
				iSoldUnits += (double)GetField(oItem,"IcQnt",0.0m);//Int32.Parse(GetField(oItem,"IcQnt"));
				iShipUnits += (double)GetField(oItem,"IcQnt",0.0m);//Int32.Parse(GetField(oItem,"IcQnt"));
				iTotalUnits += (double)GetField(oItem,"IcQnt",0.0m);//Int32.Parse(GetField(oItem,"IcQnt"));
			}
			else
			{
				iSoldUnits += (double)GetField(oItem,"Qnt",0.0m);//Int32.Parse(GetField(oItem,"Qnt"));
				iShipUnits += (double)GetField(oItem,"Qnt",0.0m);//Int32.Parse(GetField(oItem,"Qnt"));
				iTotalUnits += (double)GetField(oItem,"Qnt",0.0m);//Int32.Parse(GetField(oItem,"Qnt"));	
			}
			
			
			//Receipt Price only get added once per new order.
			if(strLastOrder=="" || strLastOrder != GetField(oItem,"SONO"))
			{
				fSoldReceipt += float.Parse(GetField(oItem,"RECEIPTAMT"));
				fShipReceipt += float.Parse(GetField(oItem,"RECEIPTAMT"));
				fTotalReceipt += float.Parse(GetField(oItem,"RECEIPTAMT"));

				if (GetField(oItem,"ArLineIdx") == "")
				{
					fSoldAmount += float.Parse(GetField(oItem,"SoldAmt"));
					fShipAmount += float.Parse(GetField(oItem,"SoldAmt"));
					fTotalAmount += float.Parse(GetField(oItem,"SoldAmt"));
				}
				else
				{
					fSoldAmount += float.Parse(GetField(oItem,"PriceAmt"));
					fShipAmount += float.Parse(GetField(oItem,"PriceAmt"));
					fTotalAmount += float.Parse(GetField(oItem,"PriceAmt"));
				}
			}

			return "";
		}

		public string SubTotalSold(object oItem)
		{
			string strRet="";
			if((strLastSold!="" && bSoldChanged) || oItem ==null)
			{
				strRet = @"
					<tr bgcolor=#ffffff>
						<td colSpan='7'>
							<table cellSpacing='0' cellPadding='0' border='0' ID='Table7' style='COLOR: purple FONT-SIZE: 8pt' bgcolor=#ffffff>
								<tr height='20'>
									<td class='SubTotalSoldTop' width='50' style='FONT-SIZE: 8pt'>&nbsp;</td>
									<td class='SubTotalSoldTop' width='130' align=left style='FONT-SIZE: 8pt'>Sub Totals for Sold To:&nbsp;</td>
									<td class='SubTotalSoldTop' width='580' colspan='9' style='FONT-SIZE: 8pt'>" + strLastSold + @"</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr bgcolor=#ffffff>
						<td colSpan='7'>
							<table cellSpacing='0' cellPadding='0' border='0' ID='Table8'style='COLOR: purple FONT-SIZE: 8pt' bgcolor=#ffffff>
								<tr height='20'>
									<td class='SubTotalSold' width='200' style='FONT-SIZE: 8pt'>&nbsp;</td>
									<td class='SubTotalSold' width='50' style='FONT-SIZE: 8pt'>Units:</td>
									<td class='SubTotalSold' width='80' style='FONT-SIZE: 8pt'>" + iSoldUnits + @"</td>
									<td class='SubTotalSold' width='55' style='FONT-SIZE: 8pt'><b>Amount:</b></td>
									<td class='SubTotalSold' width='95' style='FONT-SIZE: 8pt'><b>" + fSoldAmount.ToString("C") + @"</b></td>
									<td class='SubTotalSold' width='50' style='FONT-SIZE: 8pt'>Receipts:</td>
									<td class='SubTotalSold' width='100' style='FONT-SIZE: 8pt'>" + fSoldReceipt.ToString("C") + @"</td>
									<td class='SubTotalSold' width='50' style='FONT-SIZE: 8pt'><b>Balance:</b></td>
									<td class='SubTotalSold' width='80' colspan='3' align=left style='FONT-SIZE: 8pt'><b>" + (fSoldAmount - fSoldReceipt).ToString("C") + @"</b></td>
								</tr>
								
							</table>
						</td>
					</tr>";

				iSoldUnits =0;
				fSoldAmount=0.0F;
				fSoldReceipt=0.0F;
			}

			return strRet;
		}
		public string SubTotalShip(object oItem)
		{
			string strRet="";
			if((strLastShip!="" && bShipChanged) || oItem == null)
			{
				strRet = @"
					<tr bgcolor=#ffffff>
						<td colSpan='7'>
							<table cellSpacing='0' cellPadding='0' border='0' ID='Table5'style='COLOR: fuchsia FONT-SIZE: 8pt' bgcolor=#ffffff>
								<tr height='20'>
									<td class='SubTotalShipTop' width='50' style='FONT-SIZE: 8pt'>&nbsp;</td>
									<td class='SubTotalShipTop' width='130' align=left style='FONT-SIZE: 8pt'>Sub Totals for Ship To:&nbsp;</td>
									<td class='SubTotalShipTop' width='580' colspan='9' style='FONT-SIZE: 8pt'>" + strLastShip + @"</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr bgcolor=#ffffff>
						<td colSpan='7'>
							<table cellSpacing='0' cellPadding='0' border='0' ID='Table6'style='COLOR: fuchsia FONT-SIZE: 8pt' bgcolor=#ffffff>
								<tr height='20'>
									<td class='SubTotalShip' width='200' style = 'FONT-SIZE: 8pt'>&nbsp;</td>
									<td class='SubTotalShip' width='50' style = 'FONT-SIZE: 8pt'>Units:</td>
									<td class='SubTotalShip' width='80' style = 'FONT-SIZE: 8pt'>" + iShipUnits + @"</td>
									<td class='SubTotalShip' width='55' style = 'FONT-SIZE: 8pt'><b>Amount:</b></td>
									<td class='SubTotalShip' width='95' style = 'FONT-SIZE: 8pt'><b>" + fShipAmount.ToString("C") + @"</b></td>
									<td class='SubTotalShip' width='50' style = 'FONT-SIZE: 8pt'>Receipts:</td>
									<td class='SubTotalShip' width='100' style = 'FONT-SIZE: 8pt'>" + fShipReceipt.ToString("C") + @"</td>
									<td class='SubTotalShip' width='50' style = 'FONT-SIZE: 8pt'><b>Balance:</b></td>
									<td class='SubTotalShip' width='80'colspan='3' align=left style = 'FONT-SIZE: 8pt'><b>" + (fShipAmount - fShipReceipt).ToString("C") + @"</b></td>
								</tr>
							</table>
						</td>
					</tr>";

				iShipUnits =0;
				fShipAmount=0.0F;
				fShipReceipt=0.0F;
			}

			return strRet;
		}
		public string GrandTotal()
		{
			string strRet="";
			if(true)
			{
				strRet = @"
					<tr bgcolor=#ffffff>
						<td colSpan='7'>
							<table cellSpacing='0' cellPadding='1' border='0' ID='Table5' style='COLOR: Black FONT-SIZE: 8pt'>
								<tr>
									<td class='SubTotalShipTop' width='50' style='FONT-SIZE: 8pt'>&nbsp;</td>
									<td class='SubTotalShipTop' width='130' style='FONT-SIZE: 8pt'>Grand Totals:</td>
									<td class='SubTotalShipTop' width='580' colspan='9 style='FONT-SIZE: 8pt'>&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr bgcolor=#ffffff>
						<td colSpan='7'>
							<table cellSpacing='0' cellPadding='1' border='0' ID='Table6' style='COLOR: Black FONT-SIZE: 8pt'>
								<tr>
									<td class='SubTotalShip' width='200' style = 'FONT-SIZE: 8pt'>&nbsp;</td>
									<td class='SubTotalShip' width='50' style = 'FONT-SIZE: 8pt'>Units:</td>
									<td class='SubTotalShip' width='80' style = 'FONT-SIZE: 8pt'>" + iTotalUnits + @"</td>
									<td class='SubTotalShip' width='55' style = 'FONT-SIZE: 8pt'><b>Amount:</b></td>
									<td class='SubTotalShip' width='95' style = 'FONT-SIZE: 8pt'><b>" + fTotalAmount.ToString("C") + @"</b></td>
									<td class='SubTotalShip' width='50' style = 'FONT-SIZE: 8pt'>Receipts:</td>
									<td class='SubTotalShip' width='100' style = 'FONT-SIZE: 8pt'>" + fTotalReceipt.ToString("C") + @"</td>
									<td class='SubTotalShip' width='50' style = 'FONT-SIZE: 8pt'><b>Balance:</b></td>
									<td class='SubTotalShip' width='80'colspan='3' align=left style='FONT-SIZE: 8pt'><b>" + (fTotalAmount - fTotalReceipt).ToString("C") + @"</b></td>
								</tr>
							</table>
						</td>
					</tr>";
			}

			return strRet;
		}

		public string OrderHeader1(object oItem)
		{
			string strRet="";
			string holdFlag;
			string soStatus="";
			float orderAmt;
			double pallets;
			double units;
			if (GetField(oItem,"ArLineIdx") == "")
			{
				orderAmt= float.Parse(GetField(oItem,"SoldAmt"));
				pallets = totalHelper.GetTotal(1,GetField(oItem,"SONO"));
				units = totalHelper.GetTotal(0,GetField(oItem,"SONO"));
			}
			else
			{
				orderAmt= float.Parse(GetField(oItem,"PriceAmt"));
				pallets = totalHelper.GetTotal(3,GetField(oItem,"SONO"));
				units = totalHelper.GetTotal(2,GetField(oItem,"SONO"));
				
			}
			if (GetField(oItem,"HOLDFLAG")=="N")
			{
				holdFlag = "No";
			}
			else
			{
				holdFlag = "Hold";
			}
			if (GetField(oItem,"SOSTATUS")=="C")
			{
				soStatus ="Paid in Full";
			}
				else if (GetField(oItem,"SOSTATUS")=="2")
				{
					soStatus ="Ordered";
				}
					else if (GetField(oItem,"SOSTATUS")=="4")
					{
						soStatus ="Shipped";
					}
						else if (GetField(oItem,"SOSTATUS")=="6")
						{
							soStatus ="Invoice";
						}
			if(strLastOrder=="" || bOrderChanged)
			{
				strRet = @"<tr><td><table border='0' cellPadding='1' cellspacing='0' bgcolor='" + GetOrderBackColor(oItem) + @"' style='COLOR:" +GetOrderFontColor(oItem) + @"'>
					<tr><td colspan='7'>
					<table border='0' cellPadding='1' cellspacing='0' style='COLOR: " +GetOrderFontColor(oItem) + @"'>
						<tr>
							<td width='50' class='OrderHeader1' style='FONT-SIZE: 8pt'>Order:</td>
							<td width='65' class='OrderHeader1' style='FONT-SIZE: 8pt' align='left'>" + GetField(oItem, "SONO") + @"</td>
							<td width='30' class='OrderHeader1' style='FONT-SIZE: 8pt'>Hold:</td>
							<td width='50' class='OrderHeader1' style='FONT-SIZE: 8pt'>" + holdFlag + @"</td>
							<td width='40' class='OrderHeader1' style='FONT-SIZE: 8pt'>Status:</td>
							<td width='70' class='OrderHeader1' style='FONT-SIZE: 8pt'>" + soStatus + @"</td>
							<td width='55' class='OrderHeader1' style='FONT-SIZE: 8pt'>Cust. PO:</td>
							<td width='150' class='OrderHeader1' style='FONT-SIZE: 8pt'>&nbsp;" + GetField(oItem, "CUSTPOREF") + @"</td>
							<td width='50' class='OrderHeader1' style='FONT-SIZE: 8pt'>" + GetField(oItem, "Descr") + @"</td>
							<td width='50' class='OrderHeader1' style='FONT-SIZE: 8pt'>" + GetField(oItem, "PayTerms") + @"</td>
							<td width='150' class='OrderHeader1' align=right style='FONT-SIZE: 8pt'>" + GetField(oItem, "SalesMan") + @"</td>
						</tr>
					</table>
					</td></tr>
					<tr><td colspan='7'>
						<table cellSpacing='0' cellPadding='1' border='0' style='COLOR: " + GetOrderFontColor(oItem) + @" FONT-SIZE: 8pt'>
							<tr>
								<td width='50'  class='OrderHeader2Top' style='FONT-SIZE: 8pt'>Sold To:</td>
								<td width='285' class='OrderHeader2TopRight' align=left colspan='5' style='FONT-SIZE: 8pt'>" + GetField(oItem, "SoldToName") + @"</td>
								<td width='70'  class='OrderHeader2Top'	align=right colspan='2' style='FONT-SIZE: 8pt'>Order Date:</td>
								<td width='150' class='OrderHeader2TopRight' style='FONT-SIZE: 8pt'>&nbsp;" + GetDateField(oItem, "SoDateTime") + @"</td>
								<td width='60'  class='OrderHeader2Top' style='FONT-SIZE: 8pt'><b>Amount:</b></td>
								<td width='150' class='OrderHeader2Top' align=right style='FONT-SIZE: 8pt'><b>" + orderAmt.ToString("C") + @"</b></td>
							</tr>
							<tr>
								<td class='OrderHeader2' style='FONT-SIZE: 8pt'></td>
								<td class='OrderHeader2Right' align=left colspan='5' style='FONT-SIZE: 8pt'>" + GetField(oItem, "SoldToCity") + " " + GetField(oItem, "SoldToState") + " " + GetField(oItem, "SoldToZip") + @"</td>
								<td class='OrderHeader2'align=right colspan='2' style='FONT-SIZE: 8pt'>Ship Date:</td>
								<td class='OrderHeader2Right' style='FONT-SIZE: 8pt'>&nbsp;" + GetDateField(oItem, "ShipDateTime") + @"</td>
								<td class='OrderHeader2' style='FONT-SIZE: 8pt'>Receipts:</td>
								<td class='OrderHeader2' align=right style='FONT-SIZE: 8pt'>" + float.Parse(GetField(oItem, "RECEIPTAMT")).ToString("C") + @"</td>
							</tr>
							<tr>
								<td class='OrderHeader2Top' style='FONT-SIZE: 8pt'>Ship To:</td>
								<td class='OrderHeader2TopRight' align=left colspan='5' style='FONT-SIZE: 8pt'>" + GetField(oItem, "ShipToName") + @"</td>
								<td class='OrderHeader2Top' align=right colspan='2' style='FONT-SIZE: 8pt'>Inv. Date:</td>
								<td class='OrderHeader2TopRight' style='FONT-SIZE: 8pt'>&nbsp;" + GetDateField(oItem, "InvoiceDate") + @"</td>
								<td class='OrderHeader2Top' style='FONT-SIZE: 8pt'><b>Balance:</b></td>
								<td class='OrderHeader2Top' align=right style='FONT-SIZE: 8pt'><b>" + (orderAmt - float.Parse(GetField(oItem, "RECEIPTAMT"))).ToString("C") + @"<b></td>
							</tr>
							<tr>
								<td class='OrderHeader2' style='FONT-SIZE: 8pt'></td>
								<td class='OrderHeader2Right' align=left colspan='5' style='FONT-SIZE: 8pt'>" + GetField(oItem, "ShipToCity") + " " + GetField(oItem, "ShipToState") + " " + GetField(oItem, "ShipToZip") + @"</td>
								<td class='OrderHeader2' align=right colspan='2' style='FONT-SIZE: 8pt'>Del. Date:</td>
								<td class='OrderHeader2Right' style='FONT-SIZE: 8pt'>&nbsp;" + GetDateField(oItem, "TOETADATETIME") + @"</td>
								<td class='OrderHeader2' style='FONT-SIZE: 8pt'></td>
								<td class='OrderHeader2' style='FONT-SIZE: 8pt'></td>
							</tr>
						</table>
						</td></tr>
						<tr><td colspan='7'>
							<table cellSpacing='0' cellPadding='1' border='0' style='COLOR: " + GetOrderFontColor(oItem) + @"'>
								<tr>
									<td class='OrderHeader3' width='50' style='FONT-SIZE: 8pt'>Carrier:</td>
									<td class='OrderHeader3' width='230' align=left colspan='2' style='FONT-SIZE: 8pt'>" + GetField(oItem, "CARRIER") + @"&nbsp;</td>
									<td class='OrderHeader3' width='90' align=right colspan='2' style='FONT-SIZE: 8pt'>Trailer License:</td>
									<td class='OrderHeader3' width='90' align='left' style='FONT-SIZE: 8pt'>&nbsp;" + GetField(oItem, "TRAILERLICENSE") + @"</td>
									<td class='OrderHeader3' width='50' style='FONT-SIZE: 8pt'>Pallets:</td>
									<td class='OrderHeader3' width='80' align='left' style='FONT-SIZE: 8pt'>" + pallets.ToString("###.00") + @"</td>
									<td class='OrderHeader3' width='50' style='FONT-SIZE: 8pt'>Units:</td>
									<td class='OrderHeader3' width='120' align='left' style='FONT-SIZE: 8pt'>" + units + @"</td>
									<td class='OrderHeader3' style='FONT-SIZE: 8pt'></td>
								</tr>
						</table>
						</td></tr>
							<tr>
						<td width='370' class='OrderHeader4' align=center style='FONT-SIZE: 8pt'>Description</td>
						<td width='95' class='OrderHeader4' style='FONT-SIZE: 8pt'>Warehouse</td>
						<td width='50' class='OrderHeader4Number' align=right style='FONT-SIZE: 8pt'>Order</td>
						<td width='50' class='OrderHeader4Number' align=right style='FONT-SIZE: 8pt'>Quantity</td>
						<td width='60' class='OrderHeader4Number' align=right style='FONT-SIZE: 8pt'>FOB/Cost</td>
						<td width='60' class='OrderHeader4Number' align=right style='FONT-SIZE: 8pt'>Price</td>
						<td width='70' class='OrderHeader4Number' align=right style='FONT-SIZE: 8pt'>Amount</td>
					</tr>";
			}

			return strRet;
		}
		public string Detail(object oItem)
		{
			string strRet="";
			float OrderPrice;
			float OrderSalesPrice;
			float Quanitiy;
			if (GetField(oItem,"ArLineIdx") == "")
			{
				OrderPrice = float.Parse(GetField(oItem,"FOBPrice").ToString());
				OrderSalesPrice = float.Parse(GetField(oItem,"SalesPrice").ToString());
			}
			else
			{
				OrderPrice = float.Parse(GetField(oItem,"InvoicePrice").ToString());
				OrderSalesPrice = float.Parse(GetField(oItem,"InvoiceSalePrice").ToString());
			}
			if (GetField(oItem,"Qnt") == "")
				Quanitiy = float.Parse(GetField(oItem,"IcQnt").ToString());
			else
				Quanitiy = float.Parse(GetField(oItem,"Qnt").ToString());
			{
				strRet = @"
					<tr>
						<td width='370' class='DetailLine' style='FONT-SIZE: 8pt'>" + StringFormater.FormatString(GetField(oItem, "INVCDESCR"), 43) + "&nbsp;" + (GetField(oItem, "Uom")) + @"</td>
						<td width='95' class='DetailLine' style='FONT-SIZE: 8pt''>";
						if (GetField(oItem,"Warehouse") == "")
						{
							strRet += @"&nbsp;";
						}
						strRet += @"" + StringFormater.FormatString(GetField(oItem,"Warehouse"),12) + @"</td>
						<td width='50' class='DetailLineNumber' align=right style='FONT-SIZE: 8pt'>";
						if (GetField(oItem,"IcQnt") == "")
						{
							strRet += @"&nbsp;";
						}
						strRet += @"" + GetField(oItem,"IcQnt") + @"</td>
						<td width='50' class='DetailLineNumber'align=right style='FONT-SIZE: 8pt'>";
						if (GetField(oItem,"Qnt") == "")
						{
							strRet += @"&nbsp;";
						}
						strRet += @"" + GetField(oItem,"Qnt") + @"</td>
						<td width='60' class='DetailLineNumber' align=right style='FONT-SIZE: 8pt'>" + OrderPrice.ToString("C") + @"</td>
						<td width='60' class='DetailLineNumber' align=right style='FONT-SIZE: 8pt'>" + OrderSalesPrice.ToString("C") + @"</td>
						<td width='70' class='DetailLineNumber' align=right style='FONT-SIZE: 8pt'>" + (OrderSalesPrice * Quanitiy).ToString("C") + @"</td>
					</tr>";
			}

			return strRet;
		}
										   
		public string OrderFooter()
		{
			string strRet="";
			if(strLastOrder!="" && bOrderChanged)
			{
				strRet = @"</table></td></tr>";
			}

			return strRet;
		}

		private string GetOrderBackColor(object oItem)
		{
			string ret = "white";
			if(GetField(oItem,"SoStatus")=="C")
				ret = "yellow";


			return ret;
		}
		private string GetOrderFontColor(object oItem)
		{
			string ret = "black";
			string CustomerPO = GetField(oItem,"CUSTPOREF");

			if(GetField(oItem,"SoStatus")=="2")
				ret = "blue";
			if(GetField(oItem,"SoStatus")=="4")
				ret = "green";
			if(GetField(oItem,"SoStatus")=="6")
				ret = "black";
			if(CustomerPO.Length >= 4)
			{
					if(CustomerPO.Substring(0,4).ToLower()=="proj")
						ret = "#800000";
			}
			if(GetField(oItem,"HOLDFLAG")!="N")
				ret = "red";
			
			return ret;
		}

		private string GetField(object oItem,string strField)
		{
			return DataBinder.Eval(oItem,"DataItem." + strField).ToString();
		}
		
		private double GetField(object oItem,string strField,double defaultValue)
		{
			object value = DataBinder.Eval(oItem,"DataItem." + strField);
			if(value != null && value != DBNull.Value)
				return (double)value;
			else
				return defaultValue;
		}
		private decimal GetField(object oItem,string strField,decimal defaultValue)
		{
			object value = DataBinder.Eval(oItem,"DataItem." + strField);
			if(value != null && value != DBNull.Value)
				return (decimal)value;
			else
				return defaultValue;
		}
		private string GetDateField(object oItem,string strField)
		{
			string str = DataBinder.Eval(oItem,"DataItem." + strField).ToString();
			if(str.IndexOf(" ",0)>0)
				return str.Substring(0,str.IndexOf(" ",0));
			else
				return str;
		}
		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
            if (hasLoaded)
                return;
            else
                hasLoaded = true;

			if (CheckSecurity("Customer Status Report") == false)
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Customer Status");
			
			JavaStatusMessage status = new JavaStatusMessage(this);
			status.ShowMessage("Computing");
			SetCommentLine();
			ShowReport(CustomerStatusReport.GetCustomerStatusReport(this.Session));
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}
		
		private void InitializeComponent()
		{    
			this.Load += new EventHandler(this.Page_Load);

		}
		#endregion
		private void SetCommentLine()
		{
			StringBuilder comment = new StringBuilder();

			if(MinShipDate != "")
			{ 
				comment.Append(MinShipDate);
				comment.Append(" TO ");
				comment.Append(MaxShipDate);
				comment.Append(" ");
			}
			if(OrderNumber != "")
			{ 
				comment.Append("; OrderNumber: ");
				comment.Append(OrderNumber);
				comment.Append(" ");
			}
			if(CustomerPO != "")
			{ 
				comment.Append("; Customer PO: ");
				comment.Append(CustomerPO);
				comment.Append(" ");
			}
			if(Customer != "")
			{ 
				comment.Append("; Customer: ");
				comment.Append(Customer);
				comment.Append(" ");
			}
			if(CommoditiesByName.Count > 0)
			{
				comment.Append("; Commodity: ");
				for(int i = 0; i < CommoditiesByName.Count; i++)
				{
					comment.Append(((CriteriaCustomerStatus.CommodityType)CommoditiesByName[i]).CommodityName);
					comment.Append(", ");
				}

				comment.Remove(comment.Length - 2,2);
			}
			CommentLine.InnerText = comment.ToString();
		}

		private void FilterDataSet(DataSet dataSet)
		{
			if(Commodities.Count > 0)
			{
				bool keepOrder = false;
				string orderNumber = "-1";
				int firstOrderIndex = -1;
				int secondOrderIndex = -1;

				for(int x = 0; x < dataSet.Tables[0].Rows.Count; x++)
				{
					//if(dataSet.Tables[0].Rows[x]["CmtyIdx"] != DBNull.Value)
					//{
						if(dataSet.Tables[0].Rows[x]["SoNo"] != DBNull.Value)
						{
							if(x == 0)
							{
								orderNumber = dataSet.Tables[0].Rows[x]["SoNo"].ToString();
								keepOrder = false;
								firstOrderIndex = x;
								secondOrderIndex = x;

								int tempCommodityIndex = -1;
								if(dataSet.Tables[0].Rows[x]["CmtyIdx"] != DBNull.Value)
									tempCommodityIndex = Convert.ToInt32(dataSet.Tables[0].Rows[x]["CmtyIdx"]);

								CriteriaCustomerStatus.CommodityType tempCommodityType = new CriteriaCustomerStatus.CommodityType(tempCommodityIndex);

								//If order has a commodity from selected criteria, keep
								if(Commodities.BinarySearch(tempCommodityType) >= 0)
								{
									keepOrder = true;
								}
							}
							else
							{
								if(dataSet.Tables[0].Rows[x]["SoNo"].ToString() != orderNumber)
								{
									if(keepOrder == false)
									{
//										for(int i = firstOrderIndex; i <= secondOrderIndex; i++)
//										{
//											dataSet.Tables[0].Rows.RemoveAt(i);
//											x--;
//										}

										for(int i = secondOrderIndex; i >= firstOrderIndex; i--)
										{
											dataSet.Tables[0].Rows.RemoveAt(i);
											x--;
										}
									}

									//if(x != dataSet.Tables[0].Rows.Count - 1)
									//{
										orderNumber = dataSet.Tables[0].Rows[x]["SoNo"].ToString();
										keepOrder = false;
										firstOrderIndex = x;
										secondOrderIndex = x;

										int tempCommodityIndex = -1;
										if(dataSet.Tables[0].Rows[x]["CmtyIdx"] != DBNull.Value)
											tempCommodityIndex = Convert.ToInt32(dataSet.Tables[0].Rows[x]["CmtyIdx"]);

										CriteriaCustomerStatus.CommodityType tempCommodityType = new CriteriaCustomerStatus.CommodityType(tempCommodityIndex);

										//If order has a commodity from selected criteria, keep
										if(Commodities.BinarySearch(tempCommodityType) >= 0)
										{
											keepOrder = true;
										}
									//}
								}
								else if(dataSet.Tables[0].Rows[x]["SoNo"].ToString() == orderNumber && keepOrder == false)
								{
									secondOrderIndex = x;

									//if(dataSet.Tables[0].Rows[x]["CmtyIdx"] != DBNull.Value)
									//{
										int tempCommodityIndex = -1;
										if(dataSet.Tables[0].Rows[x]["CmtyIdx"] != DBNull.Value)
											tempCommodityIndex = Convert.ToInt32(dataSet.Tables[0].Rows[x]["CmtyIdx"]);

										CriteriaCustomerStatus.CommodityType tempCommodityType = new CriteriaCustomerStatus.CommodityType(tempCommodityIndex);

										//If order has a commodity from selected criteria, keep
										if(Commodities.BinarySearch(tempCommodityType) >= 0)
										{
											keepOrder = true;
										}
									//}
								}
							}
						}
					//}
					//else
					//{
						//if line doesn't have a commodity index, remove
					//	dataSet.Tables[0].Rows.RemoveAt(x);
					//	x--;
					//}
				}

				if(keepOrder == false && dataSet.Tables[0].Rows.Count > 0)
				{
//					for(int i = firstOrderIndex; i < dataSet.Tables[0].Rows.Count; i++)
//					{
//						dataSet.Tables[0].Rows.RemoveAt(i);
//						i--;
//					}

					for(int i = secondOrderIndex; i >= firstOrderIndex; i--)
					{
						dataSet.Tables[0].Rows.RemoveAt(i);
					}
				}
			}
		}

		private void ShowReport(string strCommand)
		{
			OracleConnection OracleConn = GetOracleConn();
			DataSet dsRet = new DataSet();
			OracleDataAdapter OracleData = new OracleDataAdapter(strCommand, OracleConn);
			
			OracleData.SelectCommand.BindByName = true;
			OracleData.SelectCommand.Parameters.Add(":MinShipDate",MinShipDate);
			OracleData.SelectCommand.Parameters.Add(":MaxShipDate",MaxShipDate);
			OracleData.SelectCommand.Parameters.Add(":OrderNumber",OrderNumber.Trim());
			OracleData.SelectCommand.Parameters.Add(":CustomerPO",CustomerPO.Trim());
			OracleData.SelectCommand.Parameters.Add(":Customer",Customer.Trim());

			OracleData.Fill(dsRet);
			dsRet.Tables[0].DefaultView.Sort = "SoldToName,ShipToName,ShiptoCity,ShipDateTime";
			FilterDataSet(dsRet);
			totalHelper = new ReportTotalsHelper();
			totalHelper.AddSubTotal1And1("SONO","IcQnt");//0
			totalHelper.AddSubTotal1And1("SONO","OrderPallets");//1
			totalHelper.AddSubTotal1And1("SONO","Qnt");//2
			totalHelper.AddSubTotal1And1("SONO","InvoicePallets");//3
			totalHelper.CalcTotals(dsRet);
				
			if (dsRet.Tables[0].DefaultView.Count != 0)
			{
				this.Repeater1.DataSource = dsRet.Tables[0].DefaultView;
				Repeater1.DataBind();
			}
			else
				JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"])+ "&&Records=None");
				
			
			OracleData.Dispose();
			Misc.CleanUpOracle(OracleConn);
		}
		
	}
}
