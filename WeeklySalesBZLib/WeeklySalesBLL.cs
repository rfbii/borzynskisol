using System;
using System.Collections;
using System.Web.UI;
using KeyCentral.Functions;
using System.Data;

namespace WeeklySales.App_Code
{
	/// <summary>
	/// Summary description for BLLWeeklySales.
	/// </summary>
	public class WeeklySalesBLL:BaseBLL, IBaseBLL
	{
		#region Declares
		public ArrayList ReportItems;//ReportItem
		public ArrayList TotalDetails;//ItemDetail
		public ArrayList Valid_Warehouses;
		private DateTime yearStartDate;
		private ArrayList warehouseFilter;

		
		public double TotalAmount
		{
			get
			{
				double tempVal = 0.0f;

				int loopLimit = TotalDetails.Count;
				for(int x=0;x<loopLimit;x++)
					tempVal += ((ItemDetail)TotalDetails[x]).Amount;

				return tempVal;
			}
		}
			
		public double TotalQnt
		{
			get
			{
				double tempVal = 0.0f;

				int loopLimit = TotalDetails.Count;
				for(int x=0;x<loopLimit;x++)
					tempVal += ((ItemDetail)TotalDetails[x]).Qnt;

				return tempVal;
			}
		}
			
		public DateTime YearStartDate
		{
			get
			{
				return yearStartDate;
			}
			set
			{
				yearStartDate = value;
			}
		}

		private bool gross;
		private bool actual;
		//public SortedList ReportIndex;//key=sortstring,value=report index
		#endregion

		#region Constructor
		public WeeklySalesBLL(Page page):base(page)
		{
			yearStartDate = DateTime.MinValue;
		}
		
		#endregion

		#region Main Get Function
		public void GetWeekSalesData(bool gross, bool actual, string customer, string shipToIdx, string shipToLoc, ArrayList commodityIdx, string varietyIdx, string salesPerson, ArrayList warehouse,ArrayList commoditySizeIdx,string minDate,string maxDate)
		{
			this.gross = gross;
			this.actual = actual;

			WeeklySalesDAL dal = new WeeklySalesDAL(page);

			dal.GetWeekSalesData(customer,shipToIdx,shipToLoc,
				commodityIdx,varietyIdx,salesPerson,warehouse,
				commoditySizeIdx,minDate,maxDate,this);


			//return new ReportData();
		}

		public void GetWeekSalesData2(bool gross, bool actual, string customer, string shipToIdx, string shipToLoc, ArrayList commodityIdx, ArrayList varietyIdx, string salesPerson, ArrayList warehouse,ArrayList sizeIdx,ArrayList sizeName,string minDate,string maxDate)
		{
			this.gross = gross;
			this.actual = actual;

			WeeklySalesDAL dal = new WeeklySalesDAL(page);

			if(Valid_Warehouses == null)
				dal.GetWeekSalesData(customer,shipToIdx,shipToLoc,
					commodityIdx,varietyIdx,salesPerson,warehouse,
					sizeIdx,sizeName,minDate,maxDate,this);
			else
			{
				if(warehouse != null)
                    warehouseFilter = KeyCentral.Lookups.CriteriaBoxRecord.GetArrayOfKeys(warehouse);
				dal.GetWeekSalesData(customer,shipToIdx,shipToLoc,
					commodityIdx,varietyIdx,salesPerson,new ArrayList(),
					sizeIdx,sizeName,minDate,maxDate,this);

				//Filter warehouse based on valid_warehosues

			}

			//return new ReportData();
		}

		#endregion

		#region IBaseBLL Members

		public void ProcessData(IDataReader reader)
		{
			ArrayList invalidWarehouseRecords = new ArrayList();
			ReportItems = new ArrayList();
			TotalDetails = new ArrayList();
			DateTime lastDate = new DateTime();

			//Setup the FieldNames that can change...
			string amountFieldName = "FOBQnt";
			if(gross )
				amountFieldName = "SalesQnt";

			string qntFieldName = "EquivQnt";
			if(actual)
				qntFieldName = "Qnt";


			while(reader.Read())
			{
                int warehouseIdx = reader["WarehouseIdx"] == DBNull.Value ? -1 : (int)Convert.ToDecimal(reader["WarehouseIdx"].ToString());
                int tagIdx = reader["tagIdx"] == DBNull.Value ? -1 : (int)Convert.ToDecimal(reader["tagIdx"].ToString());
                int varietyIdx = reader["VarietyIdx"] == DBNull.Value ? -1 : (int)Convert.ToDecimal(reader["VarietyIdx"].ToString());

				//if no validWarehouse list, or no warehouse or tag on this record, or this is a sales record, or this is a valid warehouse
				if(Valid_Warehouses == null || warehouseIdx == -1 || tagIdx == -1 ||
					(string)reader["Type"] =="Sales" || Valid_Warehouses.Contains(warehouseIdx) )
				{
                    SaveRecordInfo((DateTime)reader["ShipDateTime"], (string)reader["INVCDESCR"], (double)(decimal)reader[amountFieldName], (double)(decimal)reader[qntFieldName], warehouseIdx, varietyIdx);
				}
				else if(reader["LotId"] !=DBNull.Value)//if Valid_warehouse is passed and there isn't LotID ignore this record.
				{
					InvalidWarehouseRecords tempInvalidWarehouseRecords = FindOrAddInvalidWarehouseRecords(invalidWarehouseRecords,(DateTime)reader["ShipDateTime"],yearStartDate,(string)reader["INVCDESCR"],warehouseIdx,tagIdx,(string)reader["LotId"],(string)reader["ProductName"],varietyIdx);

					tempInvalidWarehouseRecords.Amount +=(double)(decimal)reader[amountFieldName];
					tempInvalidWarehouseRecords.Qnt += (double)(decimal)reader[qntFieldName];
				}
			}
			reader.Close();

			FindLastValidWarehouse(invalidWarehouseRecords);
			
			//Now that we have the correct warehouse save them to our CBO
			int loopLimit = invalidWarehouseRecords.Count;
			for(int x=0;x<loopLimit;x++)
			{
				InvalidWarehouseRecords tempRecord = (InvalidWarehouseRecords)invalidWarehouseRecords[x];
				//Save the record
				SaveRecordInfo(tempRecord.StartDate,tempRecord.Desc,tempRecord.Amount,tempRecord.Qnt,tempRecord.WarehouseIdx,tempRecord.VarietyIdx);
			}
			
		}
		/// <summary>
		/// Changes the warehouse to the last valid warehouse 
		/// </summary>
		/// <param name="invalidWarehouseRecords">ArrayList of InvaliceWarehouseRecords to be changed</param>
		private void FindLastValidWarehouse(ArrayList invalidWarehouseRecords)
		{
			ArrayList neededLotIds = InvalidWarehouseRecords.GetAllLots(invalidWarehouseRecords);
			
			if(neededLotIds.Count==0)
				return;

			DataSet transdata = WeeklySalesDAL.GetRawRepackInputs(neededLotIds);
			
			//Setup the DataView we will need, used to setup filters and sort
			DataView sortedTransfer = new DataView(transdata.Tables[0],"recissuetype=4","TagIdx,ProductName",DataViewRowState.CurrentRows);
			DataView sortedRepack= new DataView(transdata.Tables[0],"recissuetype=3","IcRunIdx",DataViewRowState.CurrentRows);

			//Loop through all records
			int loopLimit = invalidWarehouseRecords.Count;
			for(int x=0;x<loopLimit;x++)
			{
				bool ValidWarehouseNotFound = false; //Used to make sure we don't loop forever if there isn't a valid warehouse found in the chian.
				InvalidWarehouseRecords tempRecord = (InvalidWarehouseRecords)invalidWarehouseRecords[x];

				//While this record doesn't have a vaild warehouse
				while(Valid_Warehouses.Contains(tempRecord.WarehouseIdx)==false && ValidWarehouseNotFound==false)
				{
					//Find the transferIndex of the transfer for this tag and product
					int transferIndex = sortedTransfer.Find(new object[]{tempRecord.TagIdx,tempRecord.ProductName});
					if(transferIndex ==-1)
					{
						ValidWarehouseNotFound = true;
						continue;
					}
					//find the repack for the runidx found above
					int repackIndex = sortedRepack.Find(new object[]{sortedTransfer[transferIndex]["IcRunIdx"]});
					if(repackIndex ==-1)
					{
						ValidWarehouseNotFound = true;
						continue;
					}
					//Update this record with the prvouse pack's data.
					tempRecord.ProductName = (string)sortedRepack[repackIndex]["productname"];
					tempRecord.TagIdx		= (int)(decimal)sortedRepack[repackIndex]["TagIdx"];
					tempRecord.WarehouseIdx = (int)(decimal)sortedRepack[repackIndex]["WarehouseIdx"];

				}
			}

		}

		/// <summary>
		/// Filters warehouse based on warehouseFilter and places the values in the correct CBO object.
		/// </summary>
		/// <param name="date"></param>
		/// <param name="desc"></param>
		/// <param name="amount"></param>
		/// <param name="qnt"></param>
		/// <param name="warehouseIdx"></param>
		private void SaveRecordInfo(DateTime date,string desc,double amount,double qnt,int warehouseIdx,int varietyIdx)
		{
			//Make sure no warehouse filter or that this warehouse is in the list, or that we don't have a warehouse to filter
			if(warehouseFilter == null || warehouseFilter.Count == 0 || 
				warehouseFilter.Contains(warehouseIdx.ToString()))
			{
				ReportItem tempItem;
				ItemDetail tempItemDetail;
				//Get the ReportItem
				tempItem = FindOrAddReportItem(ReportItems,date);
				
				//Get the ItemDetail and add values
				tempItemDetail =FindOrAddItemDetail(tempItem.Details,desc);
				tempItemDetail.Amount += amount;
                tempItemDetail.Qnt += qnt;
                tempItemDetail.WarehouseIdx = warehouseIdx;
                tempItemDetail.VarietyIdx = varietyIdx;

				//Add to the total Details
				tempItemDetail =FindOrAddItemDetail(TotalDetails,desc);
				tempItemDetail.Amount += amount;
				tempItemDetail.Qnt += qnt;

			}
			else
			{}//the warehouse didn't match the filter and was eliminated
			

		}

		private ReportItem FindOrAddReportItem(ArrayList list,DateTime date)
		{
			ReportItem tempItem = new ReportItem(date,yearStartDate);
			int tempReportIdx = list.BinarySearch(tempItem);

			if(tempReportIdx <0)
				list.Insert(Math.Abs(tempReportIdx)-1,tempItem);
			else
				tempItem = (ReportItem)list[tempReportIdx];

			return tempItem;
		}
		private ItemDetail FindOrAddItemDetail(ArrayList list,string desc)
		{
			ItemDetail tempItemDetail = new ItemDetail(0,0,desc);
			int tempReportDetailIdx = list.BinarySearch(tempItemDetail);

			if(tempReportDetailIdx <0)
				list.Insert(Math.Abs(tempReportDetailIdx)-1,tempItemDetail);
			else 
				tempItemDetail = (ItemDetail)list[tempReportDetailIdx];

			return tempItemDetail;
		}

		private InvalidWarehouseRecords FindOrAddInvalidWarehouseRecords(ArrayList list,DateTime date,DateTime yearStartDate,string desc,int warehouseIdx,int tagIdx,string lotId,string productName,int varietyIdx)
		{
            InvalidWarehouseRecords tempItemDetail = new InvalidWarehouseRecords(date, yearStartDate, desc, warehouseIdx, tagIdx, lotId, productName, varietyIdx);
			int tempReportDetailIdx = list.BinarySearch(tempItemDetail);

			if(tempReportDetailIdx <0)
				list.Insert(Math.Abs(tempReportDetailIdx)-1,tempItemDetail);
			else 
				tempItemDetail = (InvalidWarehouseRecords)list[tempReportDetailIdx];

			return tempItemDetail;
		}
		#endregion

		#region CBO
		public class InvalidWarehouseRecords :IComparable
		{
			#region declares
			//Key Fields
			public int Week;
			public int Year;
			public string LotId;
			public string Desc;
			public string ProductName;
			public int WarehouseIdx;
			public int TagIdx;
            public int VarietyIdx;

			//Data fields
			public double Amount;
			public double Qnt;
			#endregion

			#region Properties
			public DateTime StartDate{get{return Misc.WeekToStartDate(Week,Year);}}
			/// <summary>
			/// Build an ArrayList of unique lot ID's.
			/// </summary>
			/// <param name="data">ArrayList of InvalidWarehouseRecords</param>
			/// <returns>ArrayList of Lot ID's (strings)</returns>
			public static ArrayList GetAllLots(ArrayList data)
			{
				ArrayList returnValue = new ArrayList();

				int loopLimit = data.Count;
				for(int x=0;x<loopLimit;x++)
				{
					InvalidWarehouseRecords tempRecord = (InvalidWarehouseRecords)data[x];

					int tempIndex = returnValue.BinarySearch(tempRecord.LotId);
					if(tempIndex<0)
						returnValue.Insert(Math.Abs(tempIndex)-1,tempRecord.LotId);
				}

				return returnValue;
			}
			#endregion
			#region constructor
			public InvalidWarehouseRecords(DateTime date,DateTime yearStartDate,string desc,int warehouseIdx,int tagIdx,string lotId,string productName,int varietyIdx)
			{
				if(yearStartDate == DateTime.MinValue)//Week should be based on old DateToWeek
				{
					Week = Int32.Parse(Misc.DateToWeek(date));
					Year = date.Year;

					//Make sure we don't have week rollover issues
					if(Week>40 && date.DayOfYear<20)//start of year is last week of last year
						Year--;
					if(Week<10 && date.DayOfYear >100)//End of Year is first week of next year
						Year++;
				}
				else//Week should be based on passed start date.
				{
					Misc.WeekBaseData weekData = Misc.GetWeekBase(yearStartDate);
					Week = Misc.GetDateBasedWeek(date,weekData,false);
					Year = Int32.Parse(Misc.GetDateBasedWeekYear(date,weekData));
				}
				Desc = desc;
				WarehouseIdx = warehouseIdx;
				TagIdx = tagIdx;
				LotId = lotId;
				ProductName = productName;
                VarietyIdx = varietyIdx;

				Amount = 0.0f;
				Qnt = 0.0f;
			}
			#endregion
			
			#region IComparable Members

			public int CompareTo(object obj)
			{
				int tempCompare;
				InvalidWarehouseRecords Y = (InvalidWarehouseRecords) obj;

				//Compare StartDate
				tempCompare = this.StartDate.CompareTo(Y.StartDate);
				if(tempCompare != 0)
					return tempCompare;
				
				//Compare LotId
				tempCompare = this.LotId.CompareTo(Y.LotId);
				if(tempCompare != 0)
					return tempCompare;

				//Compare Desc
				tempCompare = this.Desc.CompareTo(Y.Desc);
				if(tempCompare != 0)
					return tempCompare;
				
				//Compare ProductName
				tempCompare = this.ProductName.CompareTo(Y.ProductName);
				if(tempCompare != 0)
					return tempCompare;
				
				//Compare WarehouseIdx
				tempCompare = this.WarehouseIdx.CompareTo(Y.WarehouseIdx);
				if(tempCompare != 0)
					return tempCompare;

				//Compare TagIdx
				tempCompare = this.TagIdx.CompareTo(Y.TagIdx);
				if(tempCompare != 0)
					return tempCompare;

                //Compare WarehouseIdx
                tempCompare = this.VarietyIdx.CompareTo(Y.VarietyIdx);
                if (tempCompare != 0)
                    return tempCompare;

				return 0;
			}

			#endregion

		}


		public class ItemDetail: IComparable
		{
			public double Amount;
			public double Qnt;
			public string Desc;
			public int WarehouseIdx;
            public int VarietyIdx;

			public ItemDetail()
			{
				Amount = 0.0f;
				Qnt = 0.0f;
				Desc = String.Empty;
				WarehouseIdx = 0;
                VarietyIdx = 0;
			}

			public ItemDetail(double amount,double qnt,string desc)
			{
				Amount = amount;
				Qnt = qnt;
				Desc = desc;
			}

			#region IComparable Members

			public int CompareTo(object obj)
			{
				int tempCompare;
				ItemDetail Y = (ItemDetail) obj;

				//Compare Desc
				tempCompare = this.Desc.CompareTo(Y.Desc);
				if(tempCompare != 0)
					return tempCompare;

				return 0;
			}

			#endregion
		}
		public class ReportItem :IComparable
		{
			#region Declares
			public ArrayList Details;
			
			public int Week;
			public int Year;
			#endregion

			#region Properties
			public DateTime StartDate{get{return Misc.WeekToStartDate(Week,Year);}}
			public DateTime EndDate{get{return Misc.WeekToEndDate(Week,Year);}}
			public double Amount
			{
				get
				{
					double tempVal = 0.0f;

					int loopLimit = Details.Count;
					for(int x=0;x<loopLimit;x++)
						tempVal += ((ItemDetail)Details[x]).Amount;

					return tempVal;
				}
			}
			
			public double Qnt
			{
				get
				{
					double tempVal = 0.0f;

					int loopLimit = Details.Count;
					for(int x=0;x<loopLimit;x++)
						tempVal += ((ItemDetail)Details[x]).Qnt;

					return tempVal;
				}
			}
			#endregion

			#region Constructor/Dispose
			public ReportItem(int week,int year)
			{
				//BaseSetup
				Details = new ArrayList();
				Week = week;
				Year = year;
			}
			
			public ReportItem(DateTime date,DateTime yearStartDate)
			{
				//BaseSetup
				Details = new ArrayList();
				
				if(yearStartDate == DateTime.MinValue)//Week should be based on old DateToWeek
				{
					Week = Int32.Parse(Misc.DateToWeek(date));
					Year = date.Year;

					//Make sure we don't have week rollover issues
					if(Week>40 && date.DayOfYear<20)//start of year is last week of last year
						Year--;
					if(Week<10 && date.DayOfYear >100)//End of Year is first week of next year
						Year++;
				}
				else//Week should be based on passed start date.
				{
					Misc.WeekBaseData weekData = Misc.GetWeekBase(yearStartDate);
					Week = Misc.GetDateBasedWeek(date,weekData,false);
					Year = Int32.Parse(Misc.GetDateBasedWeekYear(date,weekData));
				}
			}
			#endregion

			#region Code to remove
	/*
			public int SetDateValues(DateTime closeDate,Misc.WeekBaseData weekData)
			{
				Week = Misc.GetDateBasedWeek(closeDate,weekData,false); 
				StartDate = Misc.GetDateBasedWeekStartDate(Week,weekData,false);
				EndDate = Misc.GetDateBasedWeekEndDate(Week,weekData,false);

				
				if(Week< weekData.FirstWeekNumber)
				{
					WeekInSession = 53 -(weekData.FirstWeekNumber-Week);
					return Int32.Parse(Misc.GetDateBasedWeekYear(closeDate,weekData))-1;
				}
				else
				{
					WeekInSession = Week-weekData.FirstWeekNumber;
					return Int32.Parse(Misc.GetDateBasedWeekYear(closeDate,weekData));
				}
				
			}
*/
			/*
			public void AddAmount(int Year,double amount)
			{
				ItemDetail tempItemDetail = (ItemDetail)Details[Year];

				if(tempItemDetail == null)
					Details.Add(Year,new ItemDetail(amount,0.0f));
				else
					tempItemDetail.Amount += amount;
			}

			public void AddQnt(int Year,double qnt)
			{
				ItemDetail tempItemDetail = (ItemDetail)Details[Year];

				if(tempItemDetail == null)
					Details.Add(Year,new ItemDetail(0.0f,qnt));
				else
					tempItemDetail.Qnt += qnt;
			}


			public double GetAmount(int year)
			{
				ItemDetail itemDetail = (ItemDetail)Details[year];

				if(itemDetail == null)
					return 0.0f;
				else
					return itemDetail.Amount;
			}
			public double GetQnt(int year)
			{
				ItemDetail itemDetail = (ItemDetail)Details[year];

				if(itemDetail == null)
					return 0.0f;
				else
					return itemDetail.Qnt;
			}
*/
			#endregion

			#region IComparable Members

			public int CompareTo(object obj)
			{
				int tempCompare;
				ReportItem Y = (ReportItem) obj;

				//Compare StartDate
				tempCompare = this.StartDate.CompareTo(Y.StartDate);
				if(tempCompare != 0)
					return tempCompare;

				return 0;
			}

			#endregion
		}
		#endregion
	}
}
