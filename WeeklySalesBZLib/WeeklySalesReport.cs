using System;
using System.Collections;
using System.Data.SqlClient;
using KeyCentral.Functions;
using System.Web.SessionState;
using System.Data;
using System.ComponentModel;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Web.UI;

namespace WeeklySales.App_Code
{
	public class WeeklySalesData
	{
		
		#region declares
		private Page page;
		//private Exception lastError;
		private bool sortShipTo=false;
		#endregion
						
		#region Constructor
		
		public  WeeklySalesData(Page page)
		{
			this.page = page;
		}
		public bool SortShipTo{get{return sortShipTo;}set{sortShipTo=value;}}
		
		#endregion
	
		public  WeeklySalesData()
		{}
		public DataSet GetWeekSalesData(string minShip, string maxShip, string customer, string shipToIdx, string shipToLoc, string commodity, string varietyIdx, string salesPerson, ArrayList warehouse)
		{    
			OracleConnection OracleConn = Misc.GetOracleConn(page.Session);
			DataSet dsRet = new DataSet();
			OracleDataAdapter OracleData = new OracleDataAdapter(BuildDataString(warehouse), OracleConn);
				
			OracleData.SelectCommand.BindByName = true;
			OracleData.SelectCommand.Parameters.Add(":MinShipDate",minShip);
			OracleData.SelectCommand.Parameters.Add(":MaxShipDate",maxShip);
			OracleData.SelectCommand.Parameters.Add(":Customer",customer);
			OracleData.SelectCommand.Parameters.Add(":ShipToIdx",shipToIdx);
			OracleData.SelectCommand.Parameters.Add(":ShipToLocation",shipToLoc);
			OracleData.SelectCommand.Parameters.Add(":Commodity",commodity);
			OracleData.SelectCommand.Parameters.Add(":VarietyIdx",varietyIdx);
			OracleData.SelectCommand.Parameters.Add(":SalesPerson",salesPerson);
			OracleData.Fill(dsRet);
			dsRet.Tables[0].Columns.Add("Week",System.Type.GetType("System.Int32"));
			FillWeek(dsRet);
			if (sortShipTo)
				dsRet.Tables[0].DefaultView.Sort = "SHIPDATETIME,ShipToName,ShipToCity,ShipToState,ShipToZip,INVCDESCR";
			else
				dsRet.Tables[0].DefaultView.Sort = "SHIPDATETIME,INVCDESCR";	
			
			Misc.CleanUpOracle(OracleConn);

			return dsRet;
		}

		private string BuildDataString(ArrayList warehouse)
		{
			string reportCommand = KeyCentral.Functions.Misc.FormatOracle(page.Session,strWeeklySales);
			WhereClauseBuilder whereClause = new WhereClauseBuilder();

			//build the functions
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32,"Ic_Inventory.WarehouseIdx",(ArrayList)warehouse);
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32,"Ic_Warehouse.WarehouseIdx",(ArrayList)warehouse);
			//Put the where clause into our SQL command
			return whereClause.BuildWhereClause(reportCommand);				
		}
	
		/*public static string GetWeeklySalesReport()
		{
			return strWeeklySales.Replace("#COMPANY_NUMBER#",page.Session["Company_Key"].ToString());
		}*/
		private void FillWeek(DataSet data)
		{
			foreach(DataRow row in data.Tables[0].Rows)
			{
				row["Week"] = Misc.DateToWeek(DateTime.Parse(row["ShipDateTime"].ToString()));
			}
		}
		#region strWeeklySales
		private static string strWeeklySales =@"
SELECT	/***** Invoiced Orders **********/
	Ar_Trx_Header.ArTrxHdrIdx,
	Ar_Trx_Header.SONO,
	Ar_Trx_Line.ArTrxHdrIdx as ArLineIdx,
	Ar_Trx_Header.SoDateTime,
	Ar_Trx_Header.ShipDateTime,
	Ar_Trx_Header.InvoiceDate,
	Sales_Name.LastCoName as SalesMan,
	Cust_Name.LastCoName As SoldToName,
	Cust_Loc.City As SoldToCity,
	Cust_Loc.State As SoldToState,
	Cust_Loc.Zip As SoldToZip,
	ShipTo_Name.LastCoName As ShipToName,
	ShipTo_Loc.City As ShipToCity,
	ShipTo_Loc.State As ShipToState,
	ShipTo_Loc.Zip As ShipToZip,
	Fc_Sale_Terms.DESCR,
	Ar_Pay_Terms.DESCR as PayTerms,
	Ar_Trx_Header.CUSTPOREF,
	Ar_Trx_Header.CARRIER,
	Ar_Trx_Header.TRAILERLICENSE,
	Ar_Trx_Line.UnitsPerPallet,
	Ar_Trx_Line.INVCDESCR,  
	Fc_Unit_Of_Measure.Uom,   
	0 as IcQnt,
	Ar_Trx_Line.PRICE as FOBPrice,   
	Ar_Trx_Line.SALEPRICE as SalesPrice, 
	Ar_Trx_Line.ConsignFlag as ConsignFlag,
	'' as Warehouse,  
	Ar_Trx_Header.SOAMT as SoldAmt,   
	Ar_Trx_Header.ARAMT as PriceAMT,   
	Ar_Trx_Header.SOSTATUS,   
	Ar_Trx_Header.RECEIPTAMT, 
	Ar_Trx_Header.HOLDFLAG, 
	Ar_Trx_Header.TOETADATETIME,   
	sum(Ar_Trx_Product.Qnt) as Qnt,
	trunc(sum((Ar_Trx_Product.Qnt * Decode(NVL(Ic_Ps_Size.EquivFactor,0),0,1,Ic_Ps_Size.EquivFactor))),16)  as EquivQnt,
	trunc(sum(Decode(Ar_Trx_Line.UnitsPerPallet,0,0,(Ar_Trx_Product.Qnt/Ar_Trx_Line.UnitsPerPallet))),16) as InvoicePallets,
	0
	
FROM  
	#COMPANY_NUMBER#.Ar_Trx_Header,					/* BaseTable *//* Condition; ShipDateTime between(:MinShipDate,:MaxShipDate)*/ 
	#COMPANY_NUMBER#.Ar_Trx_Line,					/* BaseTable */ 
	#COMPANY_NUMBER#.Ar_Trx_Detail,					/* BaseTable */ 
	#COMPANY_NUMBER#.Ar_Trx_Product,				/* BaseTable */ 
	#COMPANY_NUMBER#.Ar_Trx_Header_Ship,			/* BaseTable */

	#COMPANY_NUMBER#.Ic_Product_Produce,			/* Condition; VarietyIdx=:VarietyIdx */
	#COMPANY_NUMBER#.Ic_Ps_Commodity,				/* Condition; Name=:Commodity */
	#COMPANY_NUMBER#.Ic_Ps_Size,					/* Output; EquivFactor */

	#COMPANY_NUMBER#.Fc_Unit_Of_Measure,			/* Output; Uom */
	#COMPANY_NUMBER#.FC_NAME Cust_Name,				/* Condition; Id=:Customer *//* Output; LastCoName */
	#COMPANY_NUMBER#.FC_NAME_LOCATION Cust_Loc,		/* Output; City,State,Zip */
	#COMPANY_NUMBER#.FC_NAME ShipTo_Name,			/* Condition; NameIdx=:ShipToIdx */
	#COMPANY_NUMBER#.FC_NAME_LOCATION ShipTo_Loc,	/* Condition; namelocationseq=:ShipToLocation */
	#COMPANY_NUMBER#.Fc_Sale_Terms,					/* Condition; Descr != 'REJECTED' */ 
	#COMPANY_NUMBER#.Ar_Pay_Terms,					/* Output; DESCR */
	#COMPANY_NUMBER#.Fc_Name Sales_Name,			/* Condition; Id=:SalesPerson */
	#COMPANY_NUMBER#.Ic_Warehouse					/* Condition; WCB(Ic_Warehouse.WarehouseIdx)*/ 
    			
WHERE  
	Ar_Trx_Header.SoType				in (2, 5) and
	Ar_Trx_Header.SoStatus				in ('6','7','C','P') and 
	LOWER(SUBSTR(Ar_Trx_Header.SoNo,1,1))	not in ('c','x','r') and	
	Ar_Trx_Line.ArTrxHdrIdx				= AR_TRX_HEADER.ArTrxHdrIdx and
	Ar_Trx_Line.ArTrxLineTrxType		= '1' and 
	Ar_Trx_Detail.ArTrxHdrIdx			= Ar_Trx_Line.ArTrxHdrIdx and
	Ar_Trx_Detail.ArTrxLineSeq			= Ar_Trx_Line.ArTrxLineSeq and
	Ar_Trx_Detail.ArTrxLineTrxType		= Ar_Trx_Line.ArTrxLineTrxType	 and
	Ar_Trx_Product.ArTrxHdrIdx			= Ar_Trx_Detail.ArTrxHdrIdx and
	Ar_Trx_Product.ArTrxDtlSeq			= Ar_Trx_Detail.ArTrxDtlSeq and
	Ar_Trx_Header_Ship.ShipHdrIdx		= Ar_Trx_Detail.ShipHdrIdx and 

	Ic_Product_Produce.ProductIdx		= Ar_Trx_Line.ProductIdx and
	Ic_Ps_Commodity.CmtyIdx				= Ic_Product_Produce.CmtyIdx and
	Ic_Ps_Size.SizeIdx					= Ic_Product_Produce.SizeIdx  and

	Fc_Unit_Of_Measure.UomIdx			= Ar_Trx_Line.UomIdx And 
	Cust_Name.NameIdx					= Ar_Trx_Header.CUSTNAMEIDX and 
	Cust_Loc.nameidx					= Ar_Trx_Header.tocustnameidx and 
	Cust_Loc.namelocationseq			= '1' and
	ShipTo_Name.NameIdx					= Ar_Trx_Header.ToCUSTNAMEIDX and 
	ShipTo_Loc.nameidx					= Ar_Trx_Header.tocustnameidx and  
	ShipTo_Loc.namelocationseq			= Ar_Trx_Header.tocustlocseq and
	Fc_Sale_Terms.SaleTermsIdx			= Ar_Trx_Header.SaleTermsIdx and
	Fc_Sale_Terms.Descr					<> 'REJECTED' and
	Ar_Pay_Terms.ArPayTermsIdx			= Ar_Trx_Header.ArPayTermsIdx and
	Sales_Name.NameIdx					= Ar_Trx_Header.SalesPersonNameIdx and

	/* Ic_Warehouse is null if this is a non-inventory purchase */
	Ic_Warehouse.NameIdx			(+) = Ar_Trx_Header_Ship.FromNameIdx and
	Ic_Warehouse.NameLocationSeq	(+) = Ar_Trx_Header_Ship.FromLocationSeq and

	/* Conditions */
	Ar_Trx_Header.ShipDateTime     between 
	Decode(:MinShipDate,'',To_Date('01/01/1900','MM/DD/YYYY'),To_Date(:MinShipDate, 'MM/DD/YY')) and 
	Decode(:MaxShipDate,'',To_Date('01/01/2099','MM/DD/YYYY'),To_Date(:MaxShipDate, 'MM/DD/YY')) and
	
	(Ic_Product_Produce.VarietyIdx  = :VarietyIdx   or :VarietyIdx  is null ) and
	lower(Ic_Ps_Commodity.Name)		= DECODE(:Commodity, '', lower(Ic_Ps_Commodity.Name), lower(:Commodity)) and
	lower(Cust_Name.Id)				= DECODE(:Customer, '', lower(Cust_Name.Id), lower(:Customer)) and
	ShipTo_Name.NameIdx				= DECODE(:ShipToIdx, '', ShipTo_Name.NameIdx, :ShipToIdx)and
	ShipTo_Loc.namelocationseq		= DECODE(:ShipToLocation, '', ShipTo_Loc.namelocationseq, :ShipToLocation)and
	lower(Sales_Name.Id)			= DECODE(:SalesPerson, '', lower(Sales_Name.Id),lower(:SalesPerson)) 

	#WhereClause:Ic_Warehouse.WarehouseIdx#

group by

	Ar_Trx_Header.ArTrxHdrIdx,
	Ar_Trx_Header.SONO,
	Ar_Trx_Line.ArTrxHdrIdx,
	Ar_Trx_Header.SoDateTime,
	Ar_Trx_Header.ShipDateTime,
	Ar_Trx_Header.InvoiceDate,
	Sales_Name.LastCoName,
	Cust_Name.LastCoName ,
	Cust_Loc.City,
	Cust_Loc.State,
	Cust_Loc.Zip,
	ShipTo_Name.LastCoName ,
	ShipTo_Loc.City ,
	ShipTo_Loc.State ,
	ShipTo_Loc.Zip ,
	Fc_Sale_Terms.DESCR,
	Ar_Pay_Terms.DESCR ,
	Ar_Trx_Header.CUSTPOREF,
	Ar_Trx_Header.CARRIER,
	Ar_Trx_Header.TRAILERLICENSE,
	Ar_Trx_Line.UnitsPerPallet,
	Ar_Trx_Line.INVCDESCR,  
	Fc_Unit_Of_Measure.Uom,  
	Ar_Trx_Line.PRICE ,   
	Ar_Trx_Line.SALEPRICE , 
	Ar_Trx_Line.ConsignFlag ,
	Ar_Trx_Header.SOAMT ,   
	Ar_Trx_Header.ARAMT,   
	Ar_Trx_Header.SOSTATUS,   
	Ar_Trx_Header.RECEIPTAMT, 
	Ar_Trx_Header.HOLDFLAG, 
	Ar_Trx_Header.TOETADATETIME

union all

SELECT		/***** Sales Orders **********/ 
	Ar_Trx_Header.ArTrxHdrIdx,
	Ar_Trx_Header.SONO,
	Ar_So_Line.ArTrxHdrIdx as ArLineIdx,
	Ar_Trx_Header.SoDateTime,
	Ar_Trx_Header.ShipDateTime,
	Ar_Trx_Header.InvoiceDate,
	Sales_Name.LastCoName as SalesMan,
	Cust_Name.LastCoName As SoldToName,
	Cust_Loc.City As SoldToCity,
	Cust_Loc.State As SoldToState,
	Cust_Loc.Zip As SoldToZip,
	ShipTo_Name.LastCoName As ShipToName,
	ShipTo_Loc.City As ShipToCity,
	ShipTo_Loc.State As ShipToState,
	ShipTo_Loc.Zip As ShipToZip,
	Fc_Sale_Terms.DESCR,
	Ar_Pay_Terms.DESCR as PayTerms,
	Ar_Trx_Header.CUSTPOREF,
	Ar_Trx_Header.CARRIER,
	Ar_Trx_Header.TRAILERLICENSE,
	Ar_So_Line.UnitsPerPallet,
	Ar_So_Line.INVCDESCR,   
	Fc_Unit_Of_Measure.Uom,  
	Ar_So_Product.IcQnt as IcQnt,
	Ar_So_Line.PRICE as FOBPrice,   
	Ar_So_Line.SALEPRICE as SalesPrice, 
	Ar_So_Line.ConsignFlag as ConsignFlag,
	Warehouse_Loc.Descr as Warehouse,  
	Ar_Trx_Header.SOAMT as SoldAmt,   
	Ar_Trx_Header.ARAMT as PriceAMT,   
	Ar_Trx_Header.SOSTATUS,   
	Ar_Trx_Header.RECEIPTAMT, 
	Ar_Trx_Header.HOLDFLAG, 
	Ar_Trx_Header.TOETADATETIME,   
	Ar_So_Product.Qnt as Qnt,
	trunc((Ar_So_Product.Qnt * Decode(NVL(Ic_Ps_Size.EquivFactor,0),0,1,Ic_Ps_Size.EquivFactor)),16)  as EquivQnt,
	0 as InvoicePallets,
	trunc(Decode(Ar_So_Line.UnitsPerPallet,0,0,(Ar_So_Product.Qnt/Ar_So_Line.UnitsPerPallet)),16) as OrderPallets
	 				
FROM  
	#COMPANY_NUMBER#.Ar_Trx_Header,					/* BaseTable *//* Condition; ShipDateTime between(:MinShipDate,:MaxShipDate)*/
	#COMPANY_NUMBER#.Ar_So_Line,					/* BaseTable */
	#COMPANY_NUMBER#.Ar_So_Detail,					/* BaseTable */
	#COMPANY_NUMBER#.Ar_So_Product,					/* BaseTable */
	#COMPANY_NUMBER#.Ar_Trx_Header_Ship, 			/* BaseTable */

	#COMPANY_NUMBER#.Ic_Product_Produce, 			/* Condition; VarietyIdx=:VarietyIdx */
	#COMPANY_NUMBER#.Ic_Ps_Commodity, 				/* Condition; Name=:Commodity */
	#COMPANY_NUMBER#.Ic_Ps_Size, 					/* Output; EquivFactor */

	#COMPANY_NUMBER#.Fc_Unit_Of_Measure, 			/* Output; Uom */
	#COMPANY_NUMBER#.FC_NAME Cust_Name, 			/* Condition; Id=:Customer */
	#COMPANY_NUMBER#.FC_NAME_LOCATION Cust_Loc, 	/* Output; City,State,Zip */
	#COMPANY_NUMBER#.FC_NAME ShipTo_Name, 			/* Condition; NameIdx=:ShipToIdx */
	#COMPANY_NUMBER#.FC_NAME_LOCATION ShipTo_Loc, 	/* Condition; namelocationseq=:ShipToLocation */
	#COMPANY_NUMBER#.Fc_Sale_Terms, 				/* Condition; Descr <> 'REJECTED' */ 
	#COMPANY_NUMBER#.Ar_Pay_Terms,					/* Output; DESCR */
	#COMPANY_NUMBER#.Fc_Name Sales_Name, 			/* Condition; Id=:SalesPerson */

	#COMPANY_NUMBER#.Ic_Warehouse, 					/* Condition; WCB(Ic_Warehouse.WarehouseIdx)*/ 
	#COMPANY_NUMBER#.FC_NAME_LOCATION Warehouse_Loc	/* Output;Descr */
				
WHERE  

	Ar_Trx_Header.SoType				in (2,5) and 
	Ar_Trx_Header.SoStatus				in ('2','3','4','5') and
	LOWER(SUBSTR(Ar_Trx_Header.SoNo,1,1))	not in ('c','x','r') and	
	Ar_So_Line.ArTrxHdrIdx				= Ar_Trx_Header.ArTrxHdrIdx And  
	Ar_So_Line.ArSoLineTrxType			= '1' and  
	Ar_So_Detail.ArTrxHdrIdx			= Ar_So_Line.ArTrxHdrIdx and
	Ar_So_Detail.ArSoLineSeq			= Ar_So_Line.ArSoLineSeq and
	Ar_So_Detail.ArSoLineTrxType		= Ar_So_Line.ArSoLineTrxType and
	Ar_So_Product.ArTrxHdrIdx			= Ar_So_Detail.ArTrxHdrIdx and
	Ar_So_Product.ArSoDtlSeq			= Ar_So_Detail.ArSoDtlSeq and
	Ar_Trx_Header_Ship.ShipHdrIdx		= Ar_So_Detail.ShipHdrIdx and

	Ic_Product_Produce.ProductIdx		= Ar_So_Line.ProductIdx and
	Ic_Ps_Commodity.CmtyIdx				= Ic_Product_Produce.CmtyIdx and
	Ic_Ps_Size.SizeIdx					= Ic_Product_Produce.SizeIdx  and

	Fc_Unit_Of_Measure.UomIdx		    = Ar_So_Line.UomIdx And 
	Cust_Name.NameIdx 		            = Ar_Trx_Header.CUSTNAMEIDX and 
	Cust_Loc.nameidx			        = Ar_Trx_Header.tocustnameidx and 
	Cust_Loc.namelocationseq		    = '1' and
	ShipTo_Name.NameIdx		            = Ar_Trx_Header.ToCUSTNAMEIDX and 
	ShipTo_Loc.nameidx		            = Ar_Trx_Header.tocustnameidx and  
	ShipTo_Loc.namelocationseq		    = Ar_Trx_Header.tocustlocseq and
	Fc_Sale_Terms.SaleTermsIdx		    = Ar_Trx_Header.SaleTermsIdx and
	Fc_Sale_Terms.Descr 		        <> 'REJECTED' and
	Ar_Pay_Terms.ArPayTermsIdx		    = Ar_Trx_Header.ArPayTermsIdx and
	Sales_Name.NameIdx 		            = Ar_Trx_Header.SalesPersonNameIdx and

	/* Ic_Warehouse is null if this is a non-inventory purchase */
	Ic_Warehouse.NameIdx			(+) = Ar_Trx_Header_Ship.FromNameIdx and
	Ic_Warehouse.NameLocationSeq	(+) = Ar_Trx_Header_Ship.FromLocationSeq and
	Warehouse_Loc.NameIdx			(+) = Ic_Warehouse.NameIdx AND  
	Warehouse_Loc.NameLocationSeq	(+) = Ic_Warehouse.NameLocationSeq and 


  	/* Conditions */
	Ar_Trx_Header.ShipDateTime between 
	Decode(:MinShipDate,'',To_Date('01/01/1900','MM/DD/YYYY'),To_Date(:MinShipDate, 'MM/DD/YY')) and 
	Decode(:MaxShipDate,'',To_Date('01/01/2099','MM/DD/YYYY'),To_Date(:MaxShipDate, 'MM/DD/YY')) and
	lower(Cust_Name.Id)				= DECODE(:Customer, '', lower(Cust_Name.Id), lower(:Customer)) and
	ShipTo_Name.NameIdx				= DECODE(:ShipToIdx, '', ShipTo_Name.NameIdx, :ShipToIdx)and
	ShipTo_Loc.namelocationseq		= DECODE(:ShipToLocation, '', ShipTo_Loc.namelocationseq, :ShipToLocation)and
	lower(Ic_Ps_Commodity.Name)		= DECODE(:Commodity, '', lower(Ic_Ps_Commodity.Name), lower(:Commodity)) and
	(Ic_Product_Produce.VarietyIdx  = :VarietyIdx   or :VarietyIdx  is null ) and
	lower(Sales_Name.Id)			= DECODE(:SalesPerson, '', lower(Sales_Name.Id),lower(:SalesPerson))

	#WhereClause:Ic_Warehouse.WarehouseIdx#	

";

		#endregion

	}
}
