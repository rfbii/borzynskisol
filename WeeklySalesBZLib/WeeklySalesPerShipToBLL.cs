using System;
using System.Collections;
using System.Web.UI;
using KeyCentral.Functions;
using System.Data;

namespace WeeklySales.App_Code
{
	/// <summary>
	/// Summary description for BLLWeeklySales.
	/// </summary>
	public class WeeklySalesPerShipToBLL:BaseBLL, IBaseBLL
	{
		#region Declares
		public ArrayList ReportItems;//ReportItem
		public ArrayList TotalDetails;//ItemDetail

		
		public double TotalAmount
		{
			get
			{
				double tempVal = 0.0f;

				int loopLimit = TotalDetails.Count;
				for(int x=0;x<loopLimit;x++)
					tempVal += ((ItemDetail)TotalDetails[x]).Amount;

				return tempVal;
			}
		}
			
		public double TotalQnt
		{
			get
			{
				double tempVal = 0.0f;

				int loopLimit = TotalDetails.Count;
				for(int x=0;x<loopLimit;x++)
					tempVal += ((ItemDetail)TotalDetails[x]).Qnt;

				return tempVal;
			}
		}
			

		private bool gross;
		private bool actual;
		//public SortedList ReportIndex;//key=sortstring,value=report index
		#endregion

		#region Constructor
		public WeeklySalesPerShipToBLL(Page page):base(page){}
		
		#endregion

		#region Main Get Function
		public void GetWeekSalesData(bool gross, bool actual, string customer, string shipToIdx, string shipToLoc, ArrayList commodityIdx, string varietyIdx, string salesPerson, ArrayList warehouse,ArrayList commoditySizeIdx,string minDate,string maxDate)
		{
			this.gross = gross;
			this.actual = actual;

			WeeklySalesDAL dal = new WeeklySalesDAL(page);

			dal.GetWeekSalesData(customer,shipToIdx,shipToLoc,
				commodityIdx,varietyIdx,salesPerson,
				warehouse,commoditySizeIdx,minDate,maxDate,this);


			//return new ReportData();
		}

		#endregion

		#region IBaseBLL Members

		public void ProcessData(IDataReader reader)
		{
			ReportItem tempItem;
			ItemShipTo tempItemShipTo;
			ItemDetail tempItemDetail;
			ReportItems = new ArrayList();
			TotalDetails = new ArrayList();
			DateTime lastDate = new DateTime();

			//Setup the FieldNames that can change...
			string amountFieldName = "FOBQnt";
			if(gross )
				amountFieldName = "SalesQnt";

			string qntFieldName = "EquivQnt";
			if(actual)
				qntFieldName = "Qnt";


			while(reader.Read())
			{
				//Get the ReportItem
				tempItem = FindOrAddReportItem(ReportItems,(DateTime)reader["ShipDateTime"]);
				
				//Get the ItemShipTo
				object test1=reader["ShipToName"];
				object test2=reader["ShipToCity"];
				object test3=reader["ShipToState"];
				object test4=reader["ShipToZip"];
				tempItemShipTo = FindOrAddItemShipTo(tempItem.Details,(string)reader["ShipToName"],
									(string)reader["ShipToCity"],(string)reader["ShipToState"],(string)reader["ShipToZip"]);

				//Get the ItemDetail and add values
				tempItemDetail =FindOrAddItemDetail(tempItemShipTo.Details,(string)reader["INVCDESCR"]);
				tempItemDetail.Amount += (double)(decimal)reader[amountFieldName];
				tempItemDetail.Qnt += (double)(decimal)reader[qntFieldName];



				//Add to the total Details
				tempItemDetail =FindOrAddItemDetail(TotalDetails,(string)reader["INVCDESCR"]);
				tempItemDetail.Amount += (double)(decimal)reader[amountFieldName];
				tempItemDetail.Qnt += (double)(decimal)reader[qntFieldName];




/*
				//if the date changed get a new week.
				if(lastWeek==-1 ||lastDate !=  (DateTime)reader["ShipDateTime"])
				{
					lastWeek = Int32.Parse(Misc.DateToWeek((DateTime)reader["ShipDateTime"]));
					lastDate = (DateTime)reader["ShipDateTime"];
				}*/
			}
			reader.Close();
		}

		private ReportItem FindOrAddReportItem(ArrayList list,DateTime date)
		{
			ReportItem tempItem = new ReportItem(date);
			int tempReportIdx = list.BinarySearch(tempItem);

			if(tempReportIdx <0)
				list.Insert(Math.Abs(tempReportIdx)-1,tempItem);
			else
				tempItem = (ReportItem)list[tempReportIdx];

			return tempItem;
		}
		private ItemDetail FindOrAddItemDetail(ArrayList list,string desc)
		{
			ItemDetail tempItemDetail = new ItemDetail(0,0,desc);
			int tempReportDetailIdx = list.BinarySearch(tempItemDetail);

			if(tempReportDetailIdx <0)
				list.Insert(Math.Abs(tempReportDetailIdx)-1,tempItemDetail);
			else 
				tempItemDetail = (ItemDetail)list[tempReportDetailIdx];

			return tempItemDetail;
		}

		private ItemShipTo FindOrAddItemShipTo(ArrayList list,string name,string city,string state,string zip)
		{
			ItemShipTo tempItem = new ItemShipTo(name,city,state,zip);
			int tempIdx = list.BinarySearch(tempItem);

			if(tempIdx <0)
				list.Insert(Math.Abs(tempIdx)-1,tempItem);
			else 
				tempItem = (ItemShipTo)list[tempIdx];

			return tempItem;
		}

		#endregion

		#region CBO

		public class ItemDetail: IComparable
		{
			public double Amount;
			public double Qnt;
			public string Desc;

			public ItemDetail()
			{
				Amount = 0.0f;
				Qnt = 0.0f;
				Desc = String.Empty;
			}

			public ItemDetail(double amount,double qnt,string desc)
			{
				Amount = amount;
				Qnt = qnt;
				Desc = desc;
			}
			#region IComparable Members

			public int CompareTo(object obj)
			{
				int tempCompare;
				ItemDetail Y = (ItemDetail) obj;

				//Compare Desc
				tempCompare = this.Desc.CompareTo(Y.Desc);
				if(tempCompare != 0)
					return tempCompare;

				return 0;
			}

			#endregion
		}
		
		public class ItemShipTo: IComparable
		{
			#region Declares
			public ArrayList Details;

			public string ShipToName;
			public string ShipToCity;
			public string ShipToState;
			public string ShipToZip;
			#endregion

			public double Amount
			{
				get
				{
					double tempVal = 0.0f;

					int loopLimit = Details.Count;
					for(int x=0;x<loopLimit;x++)
						tempVal += ((ItemDetail)Details[x]).Amount;

					return tempVal;
				}
			}
			
			public double Qnt
			{
				get
				{
					double tempVal = 0.0f;

					int loopLimit = Details.Count;
					for(int x=0;x<loopLimit;x++)
						tempVal += ((ItemDetail)Details[x]).Qnt;

					return tempVal;
				}
			}

			public string ShipToDesc
			{
				get
				{
					return ShipToName + " " + ShipToCity + " " + ShipToState + " " + ShipToZip;
				}
			}
			public ItemShipTo()
			{
				Details = new ArrayList();
				ShipToName = String.Empty;
				ShipToCity = String.Empty;
				ShipToState = String.Empty;
				ShipToZip = String.Empty;
			}

			public ItemShipTo(string name,string city,string state,string zip)
			{
				Details = new ArrayList();
				ShipToName = name;
				ShipToCity = city;
				ShipToState = state;
				ShipToZip = zip;
			}
			#region IComparable Members

			public int CompareTo(object obj)
			{
				int tempCompare;
				ItemShipTo Y = (ItemShipTo) obj;

				//Compare ShipToName
				tempCompare = this.ShipToName.CompareTo(Y.ShipToName);
				if(tempCompare != 0)
					return tempCompare;
				
				//Compare ShipToCity
				tempCompare = this.ShipToCity.CompareTo(Y.ShipToCity);
				if(tempCompare != 0)
					return tempCompare;
				
				//Compare ShipToState
				tempCompare = this.ShipToState.CompareTo(Y.ShipToState);
				if(tempCompare != 0)
					return tempCompare;
				
				//Compare ShipToZip
				tempCompare = this.ShipToZip.CompareTo(Y.ShipToZip);
				if(tempCompare != 0)
					return tempCompare;

				return 0;
			}

			#endregion
		}
		public class ReportItem :IComparable
		{
			#region Declares
			public ArrayList Details;
			
			public int Week;
			public int Year;
			#endregion

			#region Properties
			public DateTime StartDate{get{return Misc.WeekToStartDate(Week,Year);}}
			public DateTime EndDate{get{return Misc.WeekToEndDate(Week,Year);}}
			public double Amount
			{
				get
				{
					double tempVal = 0.0f;

					int loopLimit = Details.Count;
					for(int x=0;x<loopLimit;x++)
						tempVal += ((ItemShipTo)Details[x]).Amount;

					return tempVal;
				}
			}
			
			public double Qnt
			{
				get
				{
					double tempVal = 0.0f;

					int loopLimit = Details.Count;
					for(int x=0;x<loopLimit;x++)
						tempVal += ((ItemShipTo)Details[x]).Qnt;

					return tempVal;
				}
			}
			#endregion

			#region Constructor/Dispose
			public ReportItem(int week,int year)
			{
				//BaseSetup
				Details = new ArrayList();
				Week = week;
				Year = year;
			}
			
			public ReportItem(DateTime date)
			{
				//BaseSetup
				Details = new ArrayList();
				Week = Int32.Parse(Misc.DateToWeek(date));
				Year = date.Year;
			}
			#endregion

			#region Code to remove
	/*
			public int SetDateValues(DateTime closeDate,Misc.WeekBaseData weekData)
			{
				Week = Misc.GetDateBasedWeek(closeDate,weekData,false); 
				StartDate = Misc.GetDateBasedWeekStartDate(Week,weekData,false);
				EndDate = Misc.GetDateBasedWeekEndDate(Week,weekData,false);

				
				if(Week< weekData.FirstWeekNumber)
				{
					WeekInSession = 53 -(weekData.FirstWeekNumber-Week);
					return Int32.Parse(Misc.GetDateBasedWeekYear(closeDate,weekData))-1;
				}
				else
				{
					WeekInSession = Week-weekData.FirstWeekNumber;
					return Int32.Parse(Misc.GetDateBasedWeekYear(closeDate,weekData));
				}
				
			}
*/
			/*
			public void AddAmount(int Year,double amount)
			{
				ItemDetail tempItemDetail = (ItemDetail)Details[Year];

				if(tempItemDetail == null)
					Details.Add(Year,new ItemDetail(amount,0.0f));
				else
					tempItemDetail.Amount += amount;
			}

			public void AddQnt(int Year,double qnt)
			{
				ItemDetail tempItemDetail = (ItemDetail)Details[Year];

				if(tempItemDetail == null)
					Details.Add(Year,new ItemDetail(0.0f,qnt));
				else
					tempItemDetail.Qnt += qnt;
			}


			public double GetAmount(int year)
			{
				ItemDetail itemDetail = (ItemDetail)Details[year];

				if(itemDetail == null)
					return 0.0f;
				else
					return itemDetail.Amount;
			}
			public double GetQnt(int year)
			{
				ItemDetail itemDetail = (ItemDetail)Details[year];

				if(itemDetail == null)
					return 0.0f;
				else
					return itemDetail.Qnt;
			}
*/
			#endregion

			#region IComparable Members

			public int CompareTo(object obj)
			{
				int tempCompare;
				ReportItem Y = (ReportItem) obj;

				//Compare StartDate
				tempCompare = this.StartDate.CompareTo(Y.StartDate);
				if(tempCompare != 0)
					return tempCompare;

				return 0;
			}

			#endregion
		}
		#endregion
	}
}
