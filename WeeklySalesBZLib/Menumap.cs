using System;
using System.Collections.Generic;
using System.Text;
using KeyCentral.Functions;

namespace WeeklySalesLib.App_Code
{
    [MenuMap(Security = "ADMINONLY", Description = "Year Start Date", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "/Reports/Sales Reports/Weekly Sales Admin", SortOrder = 1, Url = "../../../WeeklySalesABZ/Reports/Inputs/InputYearStartDate.aspx?ReportName=YearStartDate")]
    [MenuMap(Security = "ADMINONLY", Description = "Weekly Sales Admin", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "/Reports/Sales Reports", DisplayIfEmpty = false, SortOrder = 1, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Reports/Sales Reports/Weekly Sales Admin")]
    [MenuMap(Security = "", Description = "Sales Reports", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "/Reports", DisplayIfEmpty = false, SortOrder = 2, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Reports/Sales Reports")]
    [MenuMap(Security = "Weekly Sales Report", Description = "Raw Sales Data Report", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "/Reports/Sales Reports", SortOrder = 6, Url = "../../../WeeklySalesABZ/Reports/RawSales/CriteriaRawSales.aspx?ReportName=RawSalesDataReport")]
    [MenuMap(Security = "Weekly Sales Report", Description = "Weekly Sales Report", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "/Reports/Sales Reports", SortOrder = 2, Url = "../../../WeeklySalesABZ/Reports/WeeklySales/CriteriaWeeklySales.aspx?Total=false&&ReportName=WeeklySalesReport")]
    [MenuMap(Security = "Weekly Sales Report", Description = "Weekly Sales Total per ShipTo Report", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "/Reports/Sales Reports", SortOrder = 3, Url = "../../../WeeklySalesABZ/Reports/WeeklySales/CriteriaWeeklySales.aspx?Total=true&&ReportName=WeeklySalesTotalperShipToReport")]
    [MenuMap(Security = "Weekly Sales Report", Description = "Weekly Sales Per Year Report", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "/Reports/Sales Reports", SortOrder = 4, Url = "../../../WeeklySalesABZ/Reports/WeeklySalesPerYear/CriteriaYearWeeklySales.aspx?ReportName=WeeklySalesPerYearReport")]
    [MenuMap(Security = "Weekly Sales Report", Description = "Weekly Sales Per Year Summary Report", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "/Reports/Sales Reports", SortOrder = 5, Url = "../../../WeeklySalesABZ/Reports/WeeklySalesPerYear/CriteriaYearWeeklySales.aspx?ReportName=WeeklySalesPerYearSummaryReport")]

    class Menumap
    {
    }
}
