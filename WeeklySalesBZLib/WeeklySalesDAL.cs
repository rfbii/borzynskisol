using System;
using System.Web.UI;
using System.Collections;
using Oracle.DataAccess.Client;
using System.Data;
using System.Data.SqlClient;
using KeyCentral.Functions;
using KeyCentral.Lookups;

namespace WeeklySales.App_Code
{
	public class WeeklySalesDAL :BaseDAL
	{
		public WeeklySalesDAL(Page page):base(page){}

		public void GetWeekSalesData(string customer, string shipToIdx, string shipToLoc, ArrayList commodityIdx, string varietyIdx, string salesPerson, ArrayList warehouse,ArrayList commoditySizeIdx,string minDate,string maxDate,IBaseBLL bll)
		{    
			//Convert single values to ArrayLists
			ArrayList varieties = new ArrayList();
			if(varietyIdx != "")
				varieties.Add(new CriteriaBoxRecord(varietyIdx,"",""));

			OracleConnection OracleConn = Misc.GetOracleConn(this.famousReportNumber);
			OracleCommand oracleCommand = new OracleCommand(BuildDataString(warehouse,commodityIdx,commoditySizeIdx,varieties,new ArrayList()), OracleConn);
	
			oracleCommand.BindByName = true;
			oracleCommand.Parameters.Add(":Customer",customer);
			oracleCommand.Parameters.Add(":ShipToIdx",shipToIdx);
			oracleCommand.Parameters.Add(":ShipToLocation",shipToLoc);
			oracleCommand.Parameters.Add(":SalesPerson",salesPerson);
			oracleCommand.Parameters.Add(":MinShipDate",minDate);
			oracleCommand.Parameters.Add(":MaxShipDate",maxDate);

			bll.ProcessData(oracleCommand.ExecuteReader());

			Misc.CleanUpOracle(OracleConn);
		}

		/// <summary>
		/// Used by the SRP modual
		/// </summary>
		/// <param name="customer"></param>
		/// <param name="shipToIdx"></param>
		/// <param name="shipToLoc"></param>
		/// <param name="commodityIdx"></param>
		/// <param name="varietyIdx"></param>
		/// <param name="salesPerson"></param>
		/// <param name="warehouse"></param>
		/// <param name="commoditySizeIdx"></param>
		/// <param name="sizeName"></param>
		/// <param name="minDate"></param>
		/// <param name="maxDate"></param>
		/// <param name="bll"></param>
		public void GetWeekSalesData(string customer, string shipToIdx, string shipToLoc, ArrayList commodityIdx, ArrayList varietyIdx, string salesPerson, ArrayList warehouse,ArrayList sizeIdx,ArrayList sizeName,string minDate,string maxDate,IBaseBLL bll)
		{    
			OracleConnection OracleConn = Misc.GetOracleConn(this.famousReportNumber);
			OracleCommand oracleCommand = new OracleCommand(BuildDataString(warehouse,commodityIdx,sizeIdx,varietyIdx,sizeName), OracleConn);
	
			oracleCommand.BindByName = true;
			oracleCommand.Parameters.Add(":Customer",customer);
			oracleCommand.Parameters.Add(":ShipToIdx",shipToIdx);
			oracleCommand.Parameters.Add(":ShipToLocation",shipToLoc);
			oracleCommand.Parameters.Add(":SalesPerson",salesPerson);
			oracleCommand.Parameters.Add(":MinShipDate",minDate);
			oracleCommand.Parameters.Add(":MaxShipDate",maxDate);
				
			bll.ProcessData(oracleCommand.ExecuteReader());

			Misc.CleanUpOracle(OracleConn);
		}
		public static DataSet GetRawRepackInputs( ArrayList transferlots)
		{
			#region Command Text

			string reportCommand = Misc.FormatOracle(System.Web.HttpContext.Current.Session, @" 
			SELECT  Ic_run.IcRunIdx,
					GA_LOT.ID as LotId,
					Decode(HDR.IcTrxType,'2', '2','4') as recissuetype,
					Sum(TrxProd.ICQnt) as QNT,
					Sum(TrxProd.ICQnt * Decode(NVL(Sizes.EquivFactor,0),0,1,Sizes.EquivFactor)) as equiv,
					Sum(TrxProd.ICQnt * Decode(Ic_Ps_Product_Uom.Factor,'',0,Ic_Ps_Product_Uom.Factor)) as weight,
   					Fc_Name_Location.Descr as Warehouse,
					Ic_Product_Id.Name as productname,
					Style.Styleidx,
					Ic_Run.WarehouseIdx,
					INV.TagIdx,
					DECODE(sign(instr(lower(Sizes.Name),'col')),1,'COLOSSAL',DECODE(sign(instr(lower(Sizes.Name),'jum')),1,'JUMBO',DECODE(sign(instr(lower(Sizes.Name),'med')),1,'LARGE MED',Sizes.Name))) as Size_Name
		 
			FROM    #COMPANY_NUMBER#.IC_TRX_DETAIL DTL,
					#COMPANY_NUMBER#.IC_TRX_PRODUCT TRXPROD,
					#COMPANY_NUMBER#.IC_INVENTORY INV,
					#COMPANY_NUMBER#.IC_TRX_LINE LINE,	
					#COMPANY_NUMBER#.IC_TRX_HEADER HDR,
					#COMPANY_NUMBER#.IC_PRODUCT_PRODUCE PROD,
                    #COMPANY_NUMBER#.Ic_Ps_Product_Uom,
					#COMPANY_NUMBER#.IC_PRODUCT_ID,
					#COMPANY_NUMBER#.IC_RUN,
					#COMPANY_NUMBER#.GA_BLOCK,
					#COMPANY_NUMBER#.GA_POOL,
					#COMPANY_NUMBER#.GA_POOL_EXCLUDE_BLOCK_XREF EXCLUDE,
					#COMPANY_NUMBER#.GA_LOT,
					#COMPANY_NUMBER#.GA_GROWER,
					#COMPANY_NUMBER#.FC_NAME,
					#COMPANY_NUMBER#.Ic_Ps_Commodity Cmty,
					#COMPANY_NUMBER#.Ic_Ps_Style Style,
					#COMPANY_NUMBER#.Ic_Ps_Size Sizes,
					#COMPANY_NUMBER#.Ic_Ps_Grade Grade,
					#COMPANY_NUMBER#.Ic_Ps_Label Label,
					#COMPANY_NUMBER#.Ic_Warehouse,
					#COMPANY_NUMBER#.FC_NAME_Location        
	
			WHERE Sizes.BulkFlag = 'N' AND
					HDR.ICRUNIDX = IC_RUN.ICRUNIDX AND
					TRXPROD.ICTRXHDRIDX = DTL.ICTRXHDRIDX AND
					TRXPROD.ICTRXDTLSEQ = DTL.ICTRXDTLSEQ AND
					DTL.GLDELETECODE = 'N' AND
					DTL.ICTRXHDRIDX = HDR.ICTRXHDRIDX AND
					DTL.IcTrxHdrIdx = LINE.IcTrxHdrIdx AND
					DTL.IcTrxLineType = LINE.IcTrxLineType AND
					DTL.IcTrxLineSeq = LINE.IcTrxLineSeq AND
					TRXPROD.InventoryIdx = INV.InventoryIdx AND
					INV.GALOTIDX = GA_LOT.GALOTIDX (+) AND
					INV.PRODUCTIDX = PROD.PRODUCTIDX AND
					INV.PRODUCTIDX = IC_PRODUCT_ID.PRODUCTIDX AND
					INV.ICPRODTYPE IN ('2') AND
					PROD.CmtyIdx = Cmty.CmtyIdx AND
					PROD.StyleIdx = Style.StyleIdx AND
					PROD.SizeIdx = Sizes.SizeIdx AND
					Ic_Ps_Product_Uom.SizeIdx  = PROD.SizeIdx and
					Ic_Ps_Product_Uom.CmtyIdx  = PROD.CmtyIdx and
					Ic_Ps_Product_Uom.StyleIdx = PROD.StyleIdx and
					Ic_Ps_Product_Uom.UomIdx   = '1' and  
					Ic_Ps_Product_Uom.OkForPurchaseFlag    = 'N'and 

					PROD.GradeIdx = Grade.GradeIdx (+) AND
					PROD.LABELIDX = LABEL.LABELIDX(+) AND
					INV.GABLOCKIDX = GA_BLOCK.GABLOCKIDX (+) AND
					INV.POOLIDX = GA_POOL.POOLIDX (+) AND
					INV.GABLOCKIDX = EXCLUDE.GABLOCKIDX (+) AND
					INV.POOLIDX = EXCLUDE.POOLIDX (+) AND
					GA_BLOCK.GROWERNAMEIDX = FC_NAME.NAMEIDX (+) AND
					GA_BLOCK.GROWERNAMEIDX = GA_GROWER.GROWERNAMEIDX (+) AND
			        
					Ic_Run.WarehouseIdx = Ic_Warehouse.WarehouseIdx and	
					Fc_Name_Location.NameIdx			= Ic_Warehouse.NameIdx and
					Fc_Name_Location.NameLocationSeq    = Ic_Warehouse.NameLocationSeq and 
						
					Cmty.Name 			= 'IMPORT SWT' and	
					HDR.IcTrxType <>'2' 
					#WhereClause:Ga_Lot.Id#

			GROUP BY Ic_run.IcRunIdx,
					GA_LOT.ID,
					Decode(HDR.IcTrxType,'2', '2','4'),
					Fc_Name_Location.Descr,
					Ic_Product_Id.Name,
					Style.Styleidx,
					Ic_Run.WarehouseIdx,
					INV.TagIdx,
					Sizes.Name
					
 UNION ALL 
			SELECT  Ic_run.IcRunIdx,
					GA_LOT.ID,
					Decode(Sizes.BulkFlag,'Y',(Decode(Style.NameInvc,'FLD BINS','0','1')),'3') as recissuetype,
       				Sum(Issue.ICQnt) as QNT,
					Sum(Issue.ICQnt * Decode(NVL(Sizes.EquivFactor,0),0,1,Sizes.EquivFactor)) as equiv,
					Sum(Issue.ICQnt * Decode(Ic_Ps_Product_Uom.Factor,'',0,Ic_Ps_Product_Uom.Factor)) as weight,
   					Fc_Name_Location.Descr as Warehouse,
					Ic_Product_Id.Name as productname,
					Style.Styleidx,
					Ic_Run.WarehouseIdx,
					INV.TagIdx,
					DECODE(sign(instr(lower(Sizes.Name),'col')),1,'COLOSSAL',DECODE(sign(instr(lower(Sizes.Name),'jum')),1,'JUMBO',DECODE(sign(instr(lower(Sizes.Name),'med')),1,'LARGE MED',Sizes.Name))) as Size_Name
			
			FROM    #COMPANY_NUMBER#.IC_TRX_ISSUE_HEADER HDR,
					#COMPANY_NUMBER#.Ic_Trx_Issue_Internal Issue,
					#COMPANY_NUMBER#.IC_INVENTORY INV,
					#COMPANY_NUMBER#.IC_PRODUCT_PRODUCE PROD,
					#COMPANY_NUMBER#.IC_PRODUCT_ID,
					#COMPANY_NUMBER#.Ic_Ps_Product_Uom,
					#COMPANY_NUMBER#.IC_RUN,
					#COMPANY_NUMBER#.GA_BLOCK,
					#COMPANY_NUMBER#.GA_POOL,
					#COMPANY_NUMBER#.GA_POOL_EXCLUDE_BLOCK_XREF EXCLUDE,
					#COMPANY_NUMBER#.GA_GROWER,
					#COMPANY_NUMBER#.FC_NAME,
					#COMPANY_NUMBER#.GA_LOT,
					#COMPANY_NUMBER#.Ic_Ps_Commodity Cmty,
					#COMPANY_NUMBER#.Ic_Ps_Style Style,
					#COMPANY_NUMBER#.Ic_Ps_Size Sizes,
					#COMPANY_NUMBER#.Ic_Ps_Grade Grade,
					#COMPANY_NUMBER#.Ic_Ps_Label Label,
					#COMPANY_NUMBER#.Ic_Warehouse,
					#COMPANY_NUMBER#.FC_NAME_Location
			        
			WHERE  Issue.IcIssueHdrIdx = Hdr.IcIssueHdrIdx AND
					Issue.GLDELETECODE = 'N' AND
					Issue.InventoryIdx = INV.InventoryIdx AND
					Issue.IcRunIdx = IC_RUN.ICRUNIDX  AND
					INV.GALOTIDX = GA_LOT.GALOTIDX (+) AND
					INV.PRODUCTIDX = PROD.PRODUCTIDX AND
					INV.PRODUCTIDX = IC_PRODUCT_ID.PRODUCTIDX AND
					PROD.CmtyIdx = Cmty.CmtyIdx AND
					PROD.StyleIdx = Style.StyleIdx AND
					PROD.SizeIdx = Sizes.SizeIdx AND
                    Ic_Ps_Product_Uom.SizeIdx  = PROD.SizeIdx and
					Ic_Ps_Product_Uom.CmtyIdx  = PROD.CmtyIdx and
					Ic_Ps_Product_Uom.StyleIdx = PROD.StyleIdx and
					Ic_Ps_Product_Uom.UomIdx   = '1' and  
					Ic_Ps_Product_Uom.OkForPurchaseFlag    = 'N'and 

					PROD.GradeIdx = Grade.GradeIdx (+) AND
					PROD.LABELIDX = LABEL.LABELIDX(+) AND
					INV.GABLOCKIDX = GA_BLOCK.GABLOCKIDX (+) AND
					INV.POOLIDX = GA_POOL.POOLIDX (+) AND
					INV.GABLOCKIDX = EXCLUDE.GABLOCKIDX (+) AND
					INV.POOLIDX = EXCLUDE.POOLIDX (+) AND
					INV.ICPRODTYPE IN ('2') AND
					GA_BLOCK.GROWERNAMEIDX = FC_NAME.NAMEIDX (+) AND
					GA_BLOCK.GROWERNAMEIDX = GA_GROWER.GROWERNAMEIDX (+) AND
					Cmty.Name 	= 'IMPORT SWT' and	
					Sizes.BulkFlag  ='N'and 
					
					Ic_Run.WarehouseIdx = Ic_Warehouse.WarehouseIdx and	
					Fc_Name_Location.NameIdx			= Ic_Warehouse.NameIdx and
					Fc_Name_Location.NameLocationSeq    = Ic_Warehouse.NameLocationSeq 
					#WhereClause:Ga_Lot.Id#

		GROUP BY 
					Ic_run.IcRunIdx,
					GA_LOT.ID,
					Decode(Sizes.BulkFlag,'Y',(Decode(Style.NameInvc,'FLD BINS','0','1')),'3'),
					Fc_Name_Location.Descr,
					Ic_Product_Id.Name,
					Style.Styleidx,
					Ic_Run.WarehouseIdx,
					INV.TagIdx,
					Sizes.Name
");

			#endregion

			DataSet retData = new DataSet();
			WhereClauseBuilder whereClause = new WhereClauseBuilder();

			//build the functions
			whereClause.Add(WhereClauseUnit.UnitType.String, "Ga_Lot.Id", transferlots);
			OracleDataAdapter retCMD = new OracleDataAdapter(whereClause.BuildWhereClause(reportCommand), Misc.GetOracleConn (System.Web.HttpContext.Current.Session));
			
			retCMD.SelectCommand.BindByName = true;

			retCMD.Fill(retData);
			retCMD.SelectCommand.Connection.Dispose();

			
			//totalHelper = new ReportTotalsHelper();
			//totalHelper.AddSubTotal4And1("recissuetype", "LotId", "IcRunIdx", "productname", "Equiv"); //0
			//totalHelper.AddSubTotal4And1("recissuetype", "LotId", "IcRunIdx", "productname", "Qnt"); //1
			//totalHelper.AddSubTotal4And1("recissuetype", "LotId", "IcRunIdx", "productname", "weight"); //1
			//totalHelper.CalcTotals(retData);

			return retData;
		}
		private string BuildDataString(ArrayList warehouse,ArrayList commodityIdx,ArrayList sizeIdx,ArrayList varietyIdx,ArrayList sizeName)
		{
			string reportCommand = Misc.FormatOracle(page.Session,strWeeklySales);
			WhereClauseBuilder whereClause = new WhereClauseBuilder();

			//build the functions
			//whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32,"Ic_Inventory.WarehouseIdx",warehouse);
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32,"Ic_Warehouse.WarehouseIdx",warehouse);
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32,"Ic_Product_Produce.CmtyIdx",commodityIdx);
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32,"Ic_Product_Produce.SizeIdx",sizeIdx);
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.LikeString,"Ic_Ps_Size.Name",sizeName);
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32,"Ic_Product_Produce.VarietyIdx",varietyIdx);


			//Put the where clause into our SQL command
			return whereClause.BuildWhereClause(reportCommand);				
		}

		#region strWeeklySales
		private static string strWeeklySales = @"

SELECT	/***** Invoiced Orders **********/
	Ar_Trx_Header.ShipDateTime,
	NVL(Ar_Trx_Line.INVCDESCR,'No Description') as InvcDescr,  
	trunc((Ar_Trx_Line.PRICE *Ar_Trx_Product.Qnt),16) as FOBQnt, 
	trunc((Ar_Trx_Line.SALEPRICE *Ar_Trx_Product.Qnt),16) as SalesQnt, 
	Ar_Trx_Product.Qnt as Qnt,
	trunc((Ar_Trx_Product.Qnt * Decode(NVL(Ic_Ps_Size.EquivFactor,0),0,1,Ic_Ps_Size.EquivFactor)),16) as EquivQnt,
	Ar_Trx_Header.SoNo,
	NVL(ShipTo_Name.LastCoName,' ') As ShipToName,
	NVL(ShipTo_Loc.City,' ') As ShipToCity,
	NVL(ShipTo_Loc.State,' ') As ShipToState,
	NVL(ShipTo_Loc.Zip,' ') As ShipToZip,
	NVL(ShipTo_Loc.Descr,' ') As ShipToDescr,
	'Invoiced' as Type,
	NVL(Sales_Name.LastCoName,' ') As SalesPerson,
	NVL(Cust_Name.LastCoName,' ') As CustName,
	NVL(Cust_Loc.City,' ') As CustCity,
	NVL(Cust_Loc.State,' ') As CustState,
	NVL(Style.Name,' ') as style,
    NVL(Ic_Ps_Size.Name,' ') as psize,
    NVL(Grade.Name,' ') as grade,
    NVL(Label.Name,' ') as label,
	NVL(Vrty.Name,' ') as variety,
	NVL(Cmty.Name,' ') as commodity,
	Ware_Loc.Descr as Warehouse,
	Ga_Lot.Id as LotId,
	Grower_Loc.LastCoName as GrowerName,
	Ic_Inventory.TagIdx as TagIdx,
	Ic_Warehouse.WarehouseIdx,
	Ic_Product_Id.Name as ProductName,
	DECODE(sign(instr(lower(Ic_Ps_Size.Name),'col')),1,'COL',DECODE(sign(instr(lower(Ic_Ps_Size.Name),'jum')),1,'JUM',DECODE(sign(instr(lower(Ic_Ps_Size.Name),'med')),1,'MED',Ic_Ps_Size.Name))) as Size_Name,
    Ic_Product_Produce.CmtyIdx,
    Ic_Product_Produce.VarietyIdx,
	Ar_Trx_Detail.CostForeignAmt,
	Ar_Trx_Product.FillType,
	Ar_Trx_Header.ArTrxHdrIdx,
	trunc((Ar_Trx_Header.ArAmt),16) as ArAmt,
	trunc((Ar_Trx_Header.ReceiptAmt),16) as ReceiptAmt,
	Cust_Name.NameIdx as CustomerKey

FROM  
	#COMPANY_NUMBER#.Ar_Trx_Header,					/* BaseTable */
	#COMPANY_NUMBER#.Ar_Trx_Line,					/* BaseTable */
    #COMPANY_NUMBER#.Ar_Trx_Detail,					/* BaseTable */
	#COMPANY_NUMBER#.Ar_Trx_Product,				/* BaseTable */
	#COMPANY_NUMBER#.Ar_Trx_Header_Ship,			/* BaseTable */

	#COMPANY_NUMBER#.Ic_Product_Produce,			/* Condition; VarietyIdx=:VarietyIdx *//* Condition; WCB(Ic_Product_Produce.CmtyIdx)*/ 
	#COMPANY_NUMBER#.Ic_Ps_Size,					/* Output; EquivFactor */


    #COMPANY_NUMBER#.FC_NAME Cust_Name,				/* Condition; Id=:Customer */

	#COMPANY_NUMBER#.FC_NAME ShipTo_Name,			/* Condition; NameIdx=:ShipToIdx */
	#COMPANY_NUMBER#.FC_NAME_LOCATION ShipTo_Loc,	/* Condition; namelocationseq=:ShipToLocation */
	#COMPANY_NUMBER#.Fc_Sale_Terms,					/* Condition; Descr<> 'REJECTED' */ 

	#COMPANY_NUMBER#.Fc_Name Sales_Name,			/* Condition; Id=:SalesPerson */
	#COMPANY_NUMBER#.Ic_Warehouse,					/* Condition; WCB(Ic_Warehouse.WarehouseIdx)*/ 
	#COMPANY_NUMBER#.FC_NAME_LOCATION Cust_Loc,
	#COMPANY_NUMBER#.Ic_Ps_Commodity Cmty,
    #COMPANY_NUMBER#.Ic_Ps_Variety Vrty,
    #COMPANY_NUMBER#.Ic_Ps_Style Style,
    #COMPANY_NUMBER#.Ic_Ps_Grade Grade,
    #COMPANY_NUMBER#.Ic_Ps_Label Label,
	#COMPANY_NUMBER#.FC_NAME_Location Ware_Loc,
	#COMPANY_NUMBER#.Ic_Inventory,
	#COMPANY_NUMBER#.Ga_Lot,
	#COMPANY_NUMBER#.Ga_Block,
	#COMPANY_NUMBER#.Fc_Name Grower_Loc,
	#COMPANY_NUMBER#.IC_PRODUCT_ID

WHERE  
	Ar_Trx_Header.SoType			in (2, 5) and
	Ar_Trx_Header.SoStatus			in ('6','7','C','P') and 
	Ar_Trx_Line.ArTrxHdrIdx			= AR_TRX_HEADER.ArTrxHdrIdx and
	Ar_Trx_Line.ArTrxLineTrxType	= '1' and 
	Ar_Trx_Detail.ArTrxHdrIdx		= Ar_Trx_Line.ArTrxHdrIdx and
	Ar_Trx_Detail.ArTrxLineSeq		= Ar_Trx_Line.ArTrxLineSeq and
	Ar_Trx_Detail.ArTrxLineTrxType	= Ar_Trx_Line.ArTrxLineTrxType and
	Ar_Trx_Product.ArTrxHdrIdx		= Ar_Trx_Detail.ArTrxHdrIdx and
	Ar_Trx_Product.ArTrxDtlSeq		= Ar_Trx_Detail.ArTrxDtlSeq and
	Ar_Trx_Header_Ship.ShipHdrIdx	(+)= Ar_Trx_Detail.ShipHdrIdx and 

	Ic_Product_Produce.ProductIdx	= Ar_Trx_Line.ProductIdx and
	Ic_Ps_Size.SizeIdx				= Ic_Product_Produce.SizeIdx  and

    Ic_Product_Produce.CmtyIdx = Cmty.CmtyIdx (+) AND
    Ic_Product_Produce.VarietyIdx = Vrty.VarietyIdx (+) AND
    Ic_Product_Produce.StyleIdx = Style.StyleIdx (+) AND
    Ic_Product_Produce.GradeIdx = Grade.GradeIdx (+) AND
    Ic_Product_Produce.LABELIDX = LABEL.LABELIDX(+) AND

	Cust_Name.NameIdx				= Ar_Trx_Header.CUSTNAMEIDX and 
	Cust_Loc.nameidx				= Ar_Trx_Header.custnameidx and  
	Cust_Loc.OrderBy			    = '1' and
	ShipTo_Name.NameIdx				= Ar_Trx_Header.ToCUSTNAMEIDX and 
	ShipTo_Loc.nameidx				= Ar_Trx_Header.tocustnameidx and  
	ShipTo_Loc.namelocationseq		= Ar_Trx_Header.tocustlocseq and
	Fc_Sale_Terms.SaleTermsIdx		= Ar_Trx_Header.SaleTermsIdx and
	Fc_Sale_Terms.Descr				<> 'REJECTED' and
	Sales_Name.NameIdx				= Ar_Trx_Header.SalesPersonNameIdx and

	/* Ic_Warehouse is null if this is a non-inventory purchase */
	Ic_Warehouse.NameIdx			(+)= Ar_Trx_Header_Ship.FromNameIdx and
	Ic_Warehouse.NameLocationSeq	(+)= Ar_Trx_Header_Ship.FromLocationSeq and

	Ware_Loc.NameIdx 			(+)= Ic_Warehouse.NameIdx and
	Ware_Loc.NameLocationSeq    (+)= Ic_Warehouse.NameLocationSeq and

	/* Conditions */
	Ar_Trx_Header.ShipDateTime     between 
	Decode(:MinShipDate,'',To_Date('01/01/1900','MM/DD/YYYY'),To_Date(:MinShipDate, 'MM/DD/YY')) and 
	Decode(:MaxShipDate,'',To_Date('01/01/2099','MM/DD/YYYY'),To_Date(:MaxShipDate, 'MM/DD/YY')) and

	Ar_Trx_Header.ToCustNameIdx		= DECODE(:ShipToIdx, '', Ar_Trx_Header.ToCustNameIdx, :ShipToIdx) and
	Ar_Trx_Header.ToCustLocSeq		= DECODE(:ShipToLocation, '', Ar_Trx_Header.ToCustLocSeq, :ShipToLocation)and

	/*(Ic_Product_Produce.VarietyIdx	= :VarietyIdx   or :VarietyIdx  is null ) and*/

	lower(Cust_Name.Id)				= DECODE(:Customer, '', lower(Cust_Name.Id), lower(:Customer)) and
	lower(Sales_Name.Id)			= DECODE(:SalesPerson, '', lower(Sales_Name.Id),lower(:SalesPerson)) and

	Ar_Trx_Product.InventoryIdx = Ic_Inventory.InventoryIdx (+)and
	Ic_Product_Id.ProductIdx (+)= Ic_Inventory.ProductIdx and
	Ic_Inventory.GaLotIdx = Ga_Lot.GaLotIdx (+) and
	Ic_Inventory.GaBlockIdx = Ga_Block.GaBlockIdx (+) and
	Ga_Block.GrowerNameIdx = Grower_Loc.NameIdx (+) and
	Ar_Trx_Detail.GlDeleteCode = 'N'

	#WhereClause:Ic_Warehouse.WarehouseIdx# 
	#WhereClause:Ic_Product_Produce.CmtyIdx# 
	#WhereClause:Ic_Product_Produce.SizeIdx# 
	#WhereClause:Ic_Ps_Size.Name# 
	#WhereClause:Ic_Product_Produce.VarietyIdx# 

union all

SELECT		/***** Sales Orders **********/ 
	Ar_Trx_Header.ShipDateTime,
	NVL(Ar_So_Line.INVCDESCR,'No Description') as InvcDescr,  
	trunc((Ar_So_Line.PRICE * NVL(Ar_So_Product.Qnt,Ar_So_Line.IcQnt)),16) as FOBQnt, 
	trunc((Ar_So_Line.SALEPRICE * NVL(Ar_So_Product.Qnt,Ar_So_Line.IcQnt)),16) as SalesQnt, 
	NVL(Ar_So_Product.Qnt,Ar_So_Line.IcQnt) as Qnt,
	trunc((NVL(Ar_So_Product.Qnt,Ar_So_Line.IcQnt) * Decode(NVL(Ic_Ps_Size.EquivFactor,0),0,1,Ic_Ps_Size.EquivFactor)),16)  as EquivQnt,
	Ar_Trx_Header.SoNo,
	NVL(ShipTo_Name.LastCoName,' ') As ShipToName,
	NVL(ShipTo_Loc.City,' ') As ShipToCity,
	NVL(ShipTo_Loc.State,' ') As ShipToState,
	NVL(ShipTo_Loc.Zip,' ') As ShipToZip,
	NVL(ShipTo_Loc.Descr,' ') As ShipToDescr,
	'Sales' as Type,
	NVL(Sales_Name.LastCoName,' ') As SalesPerson,
	NVL(Cust_Name.LastCoName,' ') As CustName,
	NVL(Cust_Loc.City,' ') As CustCity,
	NVL(Cust_Loc.State,' ') As CustState,
	NVL(Style.Name,' ') as style,
    NVL(Ic_Ps_Size.Name,' ') as psize,
    NVL(Grade.Name,' ') as grade,
    NVL(Label.Name,' ') as label,
	NVL(Vrty.Name,' ') as variety,
	NVL(Cmty.Name,' ') as commodity,
	NVL(Ware_Loc.Descr ,Ware_Loc_Line.Descr) as Warehouse,
	NVL(Ga_Lot.Id,' ') as LotId,
	Grower_Loc.LastCoName as GrowerName	,
	0.0 as TagIdx,
	NVL(Ic_Warehouse.WarehouseIdx ,Ar_So_Line.warehouseIdx ) as WarehouseIdx,
	' ' as ProductName,
	DECODE(sign(instr(lower(Ic_Ps_Size.Name),'col')),1,'COL',DECODE(sign(instr(lower(Ic_Ps_Size.Name),'jum')),1,'JUM',DECODE(sign(instr(lower(Ic_Ps_Size.Name),'med')),1,'MED',Ic_Ps_Size.Name))) as Size_Name,
    Ic_Product_Produce.CmtyIdx,
    Ic_Product_Produce.VarietyIdx,
	Ar_So_Product.CostAmt,
	Ar_So_Product.FillType,
	Ar_Trx_Header.ArTrxHdrIdx,
	trunc((Ar_Trx_Header.ArAmt),16) as ArAmt,
	trunc((Ar_Trx_Header.ReceiptAmt),16) as ReceiptAmt,
	Cust_Name.NameIdx as CustomerKey

FROM  
	#COMPANY_NUMBER#.Ar_Trx_Header, 				/* BaseTable */
	#COMPANY_NUMBER#.Ar_So_Line,					/* BaseTable */
	#COMPANY_NUMBER#.Ar_So_Detail,					/* BaseTable */
	#COMPANY_NUMBER#.Ar_So_Product,					/* BaseTable */
	#COMPANY_NUMBER#.Ar_Trx_Header_Ship, 			/* BaseTable */

	#COMPANY_NUMBER#.Ic_Product_Produce, 			/* Condition; WCB(Ic_Product_Produce.CmtyIdx)*/ 
	#COMPANY_NUMBER#.Ic_Ps_Size, 					/* Output; EquivFactor */

	#COMPANY_NUMBER#.FC_NAME Cust_Name, 			/* Condition; Id=:Customer */
	#COMPANY_NUMBER#.FC_NAME ShipTo_Name,			/* Condition; NameIdx=:ShipToIdx */
	#COMPANY_NUMBER#.FC_NAME_LOCATION ShipTo_Loc,	/* Condition; namelocationseq=:ShipToLocation */
	#COMPANY_NUMBER#.Fc_Sale_Terms, 				/* Condition; Descr<> 'REJECTED' */ 
	#COMPANY_NUMBER#.Fc_Name Sales_Name, 			/* Condition; Id=:SalesPerson */
	#COMPANY_NUMBER#.Ic_Warehouse, 					/* Condition; WCB(Ic_Warehouse.WarehouseIdx)*/ 
    #COMPANY_NUMBER#.Ic_Warehouse Line_Warehouse,
	#COMPANY_NUMBER#.FC_NAME_LOCATION Cust_Loc,
	#COMPANY_NUMBER#.Ic_Ps_Commodity Cmty,
    #COMPANY_NUMBER#.Ic_Ps_Variety Vrty,
    #COMPANY_NUMBER#.Ic_Ps_Style Style,
    #COMPANY_NUMBER#.Ic_Ps_Grade Grade,
    #COMPANY_NUMBER#.Ic_Ps_Label Label,
	#COMPANY_NUMBER#.FC_NAME_Location Ware_Loc,
	#COMPANY_NUMBER#.FC_NAME_Location Ware_Loc_Line,
	#COMPANY_NUMBER#.Ga_Lot,
	#COMPANY_NUMBER#.Ga_Block,
	#COMPANY_NUMBER#.FC_NAME Grower_Loc
				
WHERE  
	Ar_Trx_Header.SoType				in (2,5) and 
	Ar_Trx_Header.SoStatus				in ('2','3') and
	Ar_So_Line.ArTrxHdrIdx				= Ar_Trx_Header.ArTrxHdrIdx And  
	Ar_So_Line.ArSoLineTrxType			= '1' and  
	Ar_So_Detail.ArTrxHdrIdx			(+)= Ar_So_Line.ArTrxHdrIdx and
	Ar_So_Detail.ArSoLineSeq			(+)= Ar_So_Line.ArSoLineSeq and
	Ar_So_Detail.ArSoLineTrxType		(+)= Ar_So_Line.ArSoLineTrxType and
	Ar_So_Product.ArTrxHdrIdx			(+)= Ar_So_Detail.ArTrxHdrIdx and
	Ar_So_Product.ArSoDtlSeq			(+)= Ar_So_Detail.ArSoDtlSeq and
	Ar_Trx_Header_Ship.ShipHdrIdx		(+)= Ar_So_Detail.ShipHdrIdx and

	Ic_Product_Produce.ProductIdx		= Ar_So_Line.ProductIdx and
	Ic_Ps_Size.SizeIdx					= Ic_Product_Produce.SizeIdx  and

    Ic_Product_Produce.CmtyIdx = Cmty.CmtyIdx (+) AND
    Ic_Product_Produce.VarietyIdx = Vrty.VarietyIdx (+) AND
    Ic_Product_Produce.StyleIdx = Style.StyleIdx (+) AND
    Ic_Product_Produce.GradeIdx = Grade.GradeIdx (+) AND
    Ic_Product_Produce.LABELIDX = LABEL.LABELIDX(+) AND

	Cust_Name.NameIdx					= Ar_Trx_Header.CUSTNAMEIDX and 
	Cust_Loc.nameidx					= Ar_Trx_Header.custnameidx and  
	Cust_Loc.namelocationseq			= '1' and
	ShipTo_Name.NameIdx					= Ar_Trx_Header.ToCUSTNAMEIDX and 
	ShipTo_Loc.nameidx					= Ar_Trx_Header.tocustnameidx and  
	ShipTo_Loc.namelocationseq			= Ar_Trx_Header.tocustlocseq and
	Fc_Sale_Terms.SaleTermsIdx			= Ar_Trx_Header.SaleTermsIdx and
	Fc_Sale_Terms.Descr 		        <> 'REJECTED' and
	Sales_Name.NameIdx					= Ar_Trx_Header.SalesPersonNameIdx and
	Ic_Warehouse.NameIdx				= NVL(Ar_Trx_Header_Ship.FromNameIdx,Line_Warehouse.NameIdx) and
	Ic_Warehouse.NameLocationSeq		= NVL(Ar_Trx_Header_Ship.FromLocationSeq,Line_Warehouse.NameLocationSeq) and

	Ware_Loc.NameIdx 					(+)= Ic_Warehouse.NameIdx and
	Ware_Loc.NameLocationSeq			(+)= Ic_Warehouse.NameLocationSeq and
	Line_Warehouse.warehouseIdx			= Ar_So_Line.warehouseIdx and
	Ware_Loc_Line.NameIdx				= Line_Warehouse.NameIdx and
	Ware_Loc_Line.NameLocationSeq		= Line_Warehouse.NameLocationSeq and

  	/* Conditions */
	Ar_Trx_Header.ShipDateTime     between 
	Decode(:MinShipDate,'',To_Date('01/01/1900','MM/DD/YYYY'),To_Date(:MinShipDate, 'MM/DD/YY')) and 
	Decode(:MaxShipDate,'',To_Date('01/01/2099','MM/DD/YYYY'),To_Date(:MaxShipDate, 'MM/DD/YY')) and

	Ar_Trx_Header.ToCustNameIdx		= DECODE(:ShipToIdx, '', Ar_Trx_Header.ToCustNameIdx, :ShipToIdx) and
	Ar_Trx_Header.ToCustLocSeq		= DECODE(:ShipToLocation, '', Ar_Trx_Header.ToCustLocSeq, :ShipToLocation) and

	lower(Cust_Name.Id)				= DECODE(:Customer, '', lower(Cust_Name.Id), lower(:Customer)) and
	lower(Sales_Name.Id)			= DECODE(:SalesPerson, '', lower(Sales_Name.Id),lower(:SalesPerson)) and

	Ar_So_Product.GaLotIdx = Ga_Lot.GaLotIdx (+) and
	Ar_So_Product.GaBlockIdx = Ga_Block.GaBlockIdx (+) and
	Ga_Block.GrowerNameIdx = Grower_Loc.NameIdx (+)

	#WhereClause:Ic_Warehouse.WarehouseIdx#	
	#WhereClause:Ic_Product_Produce.CmtyIdx# 
	#WhereClause:Ic_Product_Produce.SizeIdx# 
	#WhereClause:Ic_Ps_Size.Name# 
	#WhereClause:Ic_Product_Produce.VarietyIdx# 

union all
	
SELECT		/***** Sales Orders **********/ 
	Ar_Trx_Header.ShipDateTime,
	NVL(Ar_So_Line.INVCDESCR,'No Description') as InvcDescr,  
	trunc((Ar_So_Line.PRICE * NVL(Ar_So_Product.Qnt,Ar_So_Line.IcQnt)),16) as FOBQnt, 
	trunc((Ar_So_Line.SALEPRICE * NVL(Ar_So_Product.Qnt,Ar_So_Line.IcQnt)),16) as SalesQnt, 
	NVL(Ar_So_Product.Qnt,Ar_So_Line.IcQnt) as Qnt,
	trunc((NVL(Ar_So_Product.Qnt,Ar_So_Line.IcQnt) * Decode(NVL(Ic_Ps_Size.EquivFactor,0),0,1,Ic_Ps_Size.EquivFactor)),16)  as EquivQnt,
	Ar_Trx_Header.SoNo,
	NVL(ShipTo_Name.LastCoName,' ') As ShipToName,
	NVL(ShipTo_Loc.City,' ') As ShipToCity,
	NVL(ShipTo_Loc.State,' ') As ShipToState,
	NVL(ShipTo_Loc.Zip,' ') As ShipToZip,
	NVL(ShipTo_Loc.Descr,' ') As ShipToDescr,
	'Ship' as Type,
	NVL(Sales_Name.LastCoName,' ') As SalesPerson,
	NVL(Cust_Name.LastCoName,' ') As CustName,
	NVL(Cust_Loc.City,' ') As CustCity,
	NVL(Cust_Loc.State,' ') As CustState,
	NVL(Style.Name,' ') as style,
    NVL(Ic_Ps_Size.Name,' ') as psize,
    NVL(Grade.Name,' ') as grade,
    NVL(Label.Name,' ') as label,
	NVL(Vrty.Name,' ') as variety,
	NVL(Cmty.Name,' ') as commodity,
	NVL(Ware_Loc.Descr ,Ware_Loc_Line.Descr) as Warehouse,
	Ga_Lot.Id as LotId,
	Grower_Loc.LastCoName as GrowerName	,
	Ic_Inventory.TagIdx as TagIdx,
	NVL(Ic_Warehouse.WarehouseIdx ,Ar_So_Line.warehouseIdx ) as WarehouseIdx,
	Ic_Product_Id.Name as ProductName,
	DECODE(sign(instr(lower(Ic_Ps_Size.Name),'col')),1,'COL',DECODE(sign(instr(lower(Ic_Ps_Size.Name),'jum')),1,'JUM',DECODE(sign(instr(lower(Ic_Ps_Size.Name),'med')),1,'MED',Ic_Ps_Size.Name))) as Size_Name,
    Ic_Product_Produce.CmtyIdx,
    Ic_Product_Produce.VarietyIdx,
	Ar_So_Product.CostAmt,
	Ar_So_Product.FillType,
	Ar_Trx_Header.ArTrxHdrIdx,
	trunc((Ar_Trx_Header.ArAmt),16) as ArAmt,
	trunc((Ar_Trx_Header.ReceiptAmt),16) as ReceiptAmt,
	Cust_Name.NameIdx as CustomerKey
	 				
FROM  
	#COMPANY_NUMBER#.Ar_Trx_Header, 				/* BaseTable */
	#COMPANY_NUMBER#.Ar_So_Line,					/* BaseTable */
	#COMPANY_NUMBER#.Ar_So_Detail,					/* BaseTable */
	#COMPANY_NUMBER#.Ar_So_Product,					/* BaseTable */
	#COMPANY_NUMBER#.Ar_Trx_Header_Ship, 			/* BaseTable */

	#COMPANY_NUMBER#.Ic_Product_Produce, 			/* Condition; WCB(Ic_Product_Produce.CmtyIdx)*/ 
	#COMPANY_NUMBER#.Ic_Ps_Size, 					/* Output; EquivFactor */

	#COMPANY_NUMBER#.FC_NAME Cust_Name, 			/* Condition; Id=:Customer */
	#COMPANY_NUMBER#.FC_NAME ShipTo_Name,			/* Condition; NameIdx=:ShipToIdx */
	#COMPANY_NUMBER#.FC_NAME_LOCATION ShipTo_Loc,	/* Condition; namelocationseq=:ShipToLocation */
	#COMPANY_NUMBER#.Fc_Sale_Terms, 				/* Condition; Descr<> 'REJECTED' */ 
	#COMPANY_NUMBER#.Fc_Name Sales_Name, 			/* Condition; Id=:SalesPerson */
	#COMPANY_NUMBER#.Ic_Warehouse, 					/* Condition; WCB(Ic_Warehouse.WarehouseIdx)*/ 
    #COMPANY_NUMBER#.Ic_Warehouse Line_Warehouse,
	#COMPANY_NUMBER#.FC_NAME_LOCATION Cust_Loc,
	#COMPANY_NUMBER#.Ic_Ps_Commodity Cmty,
    #COMPANY_NUMBER#.Ic_Ps_Variety Vrty,
    #COMPANY_NUMBER#.Ic_Ps_Style Style,
    #COMPANY_NUMBER#.Ic_Ps_Grade Grade,
    #COMPANY_NUMBER#.Ic_Ps_Label Label,
	#COMPANY_NUMBER#.FC_NAME_Location Ware_Loc,
	#COMPANY_NUMBER#.FC_NAME_Location Ware_Loc_Line,
	#COMPANY_NUMBER#.Ga_Lot,
	#COMPANY_NUMBER#.Ga_Block,
	#COMPANY_NUMBER#.FC_NAME Grower_Loc,
	#COMPANY_NUMBER#.Ic_Inventory,
	#COMPANY_NUMBER#.IC_PRODUCT_ID
				
WHERE  
	Ar_Trx_Header.SoType				in (2,5) and 
	Ar_Trx_Header.SoStatus				in ('4','5') and /* ship, order_shipped */
	Ar_So_Line.ArTrxHdrIdx				= Ar_Trx_Header.ArTrxHdrIdx And  
	Ar_So_Line.ArSoLineTrxType			= '1' and  
	Ar_So_Detail.ArTrxHdrIdx			= Ar_So_Line.ArTrxHdrIdx and
	Ar_So_Detail.ArSoLineSeq			= Ar_So_Line.ArSoLineSeq and
	Ar_So_Detail.ArSoLineTrxType		= Ar_So_Line.ArSoLineTrxType and
	Ar_So_Product.ArTrxHdrIdx			= Ar_So_Detail.ArTrxHdrIdx and
	Ar_So_Product.ArSoDtlSeq			= Ar_So_Detail.ArSoDtlSeq and
	Ar_Trx_Header_Ship.ShipHdrIdx		(+)= Ar_So_Detail.ShipHdrIdx and

	Ic_Product_Produce.ProductIdx		= Ar_So_Line.ProductIdx and
	Ic_Ps_Size.SizeIdx					= Ic_Product_Produce.SizeIdx  and

    Ic_Product_Produce.CmtyIdx = Cmty.CmtyIdx (+) AND
    Ic_Product_Produce.VarietyIdx = Vrty.VarietyIdx (+) AND
    Ic_Product_Produce.StyleIdx = Style.StyleIdx (+) AND
    Ic_Product_Produce.GradeIdx = Grade.GradeIdx (+) AND
    Ic_Product_Produce.LABELIDX = LABEL.LABELIDX(+) AND

	Cust_Name.NameIdx					= Ar_Trx_Header.CUSTNAMEIDX and 
	Cust_Loc.nameidx					= Ar_Trx_Header.custnameidx and  
	Cust_Loc.namelocationseq			= '1' and
	ShipTo_Name.NameIdx					= Ar_Trx_Header.ToCUSTNAMEIDX and 
	ShipTo_Loc.nameidx					= Ar_Trx_Header.tocustnameidx and  
	ShipTo_Loc.namelocationseq			= Ar_Trx_Header.tocustlocseq and
	Fc_Sale_Terms.SaleTermsIdx			= Ar_Trx_Header.SaleTermsIdx and
	Fc_Sale_Terms.Descr 		        <> 'REJECTED' and
	Sales_Name.NameIdx					= Ar_Trx_Header.SalesPersonNameIdx and
	Ic_Warehouse.NameIdx				= NVL(Ar_Trx_Header_Ship.FromNameIdx,Line_Warehouse.NameIdx) and
	Ic_Warehouse.NameLocationSeq		= NVL(Ar_Trx_Header_Ship.FromLocationSeq,Line_Warehouse.NameLocationSeq) and

	Ware_Loc.NameIdx 					(+)= Ic_Warehouse.NameIdx and
	Ware_Loc.NameLocationSeq			(+)= Ic_Warehouse.NameLocationSeq and
	Line_Warehouse.warehouseIdx			= Ar_So_Line.warehouseIdx and
	Ware_Loc_Line.NameIdx				= Line_Warehouse.NameIdx and
	Ware_Loc_Line.NameLocationSeq		= Line_Warehouse.NameLocationSeq and
	Ar_So_Product.InventoryIdx			= Ic_Inventory.InventoryIdx (+)and
	Ic_Product_Id.ProductIdx			(+)= Ic_Inventory.ProductIdx and

  	/* Conditions */
	Ar_Trx_Header.ShipDateTime     between 
	Decode(:MinShipDate,'',To_Date('01/01/1900','MM/DD/YYYY'),To_Date(:MinShipDate, 'MM/DD/YY')) and 
	Decode(:MaxShipDate,'',To_Date('01/01/2099','MM/DD/YYYY'),To_Date(:MaxShipDate, 'MM/DD/YY')) and

	Ar_Trx_Header.ToCustNameIdx		= DECODE(:ShipToIdx, '', Ar_Trx_Header.ToCustNameIdx, :ShipToIdx) and
	Ar_Trx_Header.ToCustLocSeq		= DECODE(:ShipToLocation, '', Ar_Trx_Header.ToCustLocSeq, :ShipToLocation) and

	lower(Cust_Name.Id)				= DECODE(:Customer, '', lower(Cust_Name.Id), lower(:Customer)) and
	lower(Sales_Name.Id)			= DECODE(:SalesPerson, '', lower(Sales_Name.Id),lower(:SalesPerson)) and

	Ar_So_Product.GaLotIdx = Ga_Lot.GaLotIdx (+) and
	Ar_So_Product.GaBlockIdx = Ga_Block.GaBlockIdx (+) and
	Ga_Block.GrowerNameIdx = Grower_Loc.NameIdx (+)

	#WhereClause:Ic_Warehouse.WarehouseIdx#	
	#WhereClause:Ic_Product_Produce.CmtyIdx# 
	#WhereClause:Ic_Product_Produce.SizeIdx# 
	#WhereClause:Ic_Ps_Size.Name# 
	#WhereClause:Ic_Product_Produce.VarietyIdx# 

order by 1,2";
		#endregion
	}
}
