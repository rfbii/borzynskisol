using System;
using System.Collections;
using System.Web.UI;
using KeyCentral.Functions;
using System.Data;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Text;
using KeyCentral.Lookups;

namespace WeeklySales.App_Code
{
	/// <summary>
	/// Summary description for RawSalesBLL.
	/// </summary>
	public class RawSalesBLL : BaseBLL, IBaseBLL
	{
		#region Declares
		public ArrayList ReportItems;//ReportItem
		public ArrayList RawDetails;//ItemDetail

		private bool gross;
		private bool actual;

		private ArrayList CommodityIdx;
		private string VarietyIdxString;
		private ArrayList VarietyIdx;
		private ArrayList WarehouseIdx;
		#endregion

		#region Constructor
		public RawSalesBLL(Page page) : base(page) { }

		#endregion

		#region Main Get Function
		public void GetRawSalesData(string customer, string shipToIdx, string shipToLoc, ArrayList commodityIdx, string varietyIdx, string salesPerson, ArrayList warehouse, ArrayList commoditySizeIdx, string minDate, string maxDate)
		{
			//RawSalesDAL dal = new RawSalesDAL(page);
			WeeklySalesDAL dal = new WeeklySalesDAL(page);

			/*dal.GetWeekSalesData(customer,shipToIdx,shipToLoc,
				commodityIdx,varietyIdx,salesPerson,
				warehouse,commoditySizeIdx,minDate,maxDate,this);*/

			this.CommodityIdx = commodityIdx;
			this.VarietyIdxString = varietyIdx;
			this.WarehouseIdx = warehouse;

			dal.GetWeekSalesData(customer, shipToIdx, shipToLoc,
				new ArrayList(), "", salesPerson,
				new ArrayList(), commoditySizeIdx, minDate, maxDate, this);
		}
		public void GetRawSalesData(string customer, string shipToIdx, string shipToLoc, ArrayList commodityIdx, ArrayList varietyIdx, string salesPerson, ArrayList warehouse, ArrayList commoditySizeIdx, string minDate, string maxDate)
		{
			WeeklySalesDAL dal = new WeeklySalesDAL(page);

			this.CommodityIdx = commodityIdx;
			this.VarietyIdx = varietyIdx;
			this.WarehouseIdx = warehouse;

			dal.GetWeekSalesData(customer, shipToIdx, shipToLoc,
				commodityIdx, varietyIdx, salesPerson,
				new ArrayList(), new ArrayList(), new ArrayList(), minDate, maxDate, this);
		}
		#endregion

		#region IBaseBLL Members
		public void ProcessData(IDataReader reader)
		{
			ReportItem tempItem;
			ItemDetail tempItemDetail;
			ReportItems = new ArrayList();
			RawDetails = new ArrayList();
			DateTime lastDate = new DateTime();
			ArrayList OrderNumbers = new ArrayList();
			//OrderNumber tempOrderNumber;

			while (reader.Read())
			{
				///*tempOrderNumber = */FindOrAddOrderNumber(OrderNumbers,Convert.ToString(reader["SoNo"]));

				//Get the ReportItem
				tempItem = FindOrAddReportItem(ReportItems, (DateTime)reader["ShipDateTime"]);

				//Get the ItemDetail
				tempItemDetail = FindOrAddItemDetail(tempItem.Details, (DateTime)reader["ShipDateTime"], (string)reader["INVCDESCR"], Convert.ToString(reader["SoNo"]), Convert.ToString(reader["ShipToName"]), Convert.ToString(reader["ShipToCity"]), Convert.ToString(reader["ShipToState"]), Convert.ToString(reader["ShipToZip"]), Convert.ToString(reader["Type"]), Convert.ToString(reader["shiptodescr"]), Convert.ToString(reader["salesperson"]), Convert.ToString(reader["custname"]), Convert.ToString(reader["custcity"]), Convert.ToString(reader["custstate"]), Convert.ToString(reader["style"]), Convert.ToString(reader["psize"]), Convert.ToString(reader["grade"]), Convert.ToString(reader["label"]), Convert.ToString(reader["variety"]), Convert.ToString(reader["commodity"]), Convert.ToString(reader["Warehouse"]), Convert.ToString(reader["LotId"]), Convert.ToString(reader["GrowerName"]), Convert.ToString(reader["CmtyIdx"]), Convert.ToString(reader["VarietyIdx"]), Convert.ToString(reader["WarehouseIdx"]), Convert.ToString(reader["FillType"]), Convert.ToString(reader["ArTrxHdrIdx"]), Convert.ToString(reader["CustomerKey"]));
				tempItemDetail.FOBQnt += Convert.ToDouble(reader["FOBQnt"]);
				tempItemDetail.SalesQnt += Convert.ToDouble(reader["SalesQnt"]);
				tempItemDetail.Qnt += Convert.ToDouble(reader["Qnt"]);
				tempItemDetail.EquivQnt += Convert.ToDouble(reader["EquivQnt"]);
				tempItemDetail.ArAmt += Convert.ToDouble(reader["ArAmt"]);
				tempItemDetail.ReceiptAmt += Convert.ToDouble(reader["ReceiptAmt"]);

				if (!reader.IsDBNull(32))
					tempItemDetail.Cost += Convert.ToDouble(reader["CostForeignAmt"]);
				//tempItemDetail.Weight += Convert.ToDouble(reader["Weight"]);
			}

			for (int i = 0; i < ReportItems.Count; i++)
			{
				for (int j = 0; j < ((ReportItem)ReportItems[i]).Details.Count; j++)
				{
					RawDetails.Insert(RawDetails.Count, ((ReportItem)ReportItems[i]).Details[j]);
				}
			}

			reader.Close();

			//filter out the results using the CommodityId, the VarietyId, and the WarehouseId
			for (int i = RawDetails.Count - 1; i >= 0; i--)
			{
				bool removeLine = false;

				if (VarietyIdxString != null && VarietyIdx == null)
				{
					if (((ItemDetail)RawDetails[i]).VarietyId != VarietyIdxString && VarietyIdxString != "")
						removeLine = true;
				}
				else if (VarietyIdxString == null && VarietyIdx != null)
				{
					for (int j = 0; j < VarietyIdx.Count; j++)
					{
						if (((ItemDetail)RawDetails[i]).VarietyId == ((CriteriaBoxRecord)VarietyIdx[j]).key)
						{
							removeLine = false;
							j = VarietyIdx.Count;
						}
						else
							removeLine = true;
					}
				}

				if (CommodityIdx.Count > 0 && removeLine == false)
				{
					for (int j = 0; j < CommodityIdx.Count; j++)
					{
						if (((ItemDetail)RawDetails[i]).CommodityId == ((CriteriaBoxRecord)CommodityIdx[j]).key)
						{
							removeLine = false;
							j = CommodityIdx.Count;
						}
						else
							removeLine = true;
					}
				}

				if (WarehouseIdx.Count > 0 && removeLine == false)
				{
					for (int j = 0; j < WarehouseIdx.Count; j++)
					{
						removeLine = false;
						j = WarehouseIdx.Count;
					}
				}

				if (removeLine == true)
					RawDetails.RemoveAt(i);
			}
			//}
			//else
			//{
			//    RawDetails = GetFreightAmounts(RawDetails, OrderNumbers);
			//}
		}
		private ReportItem FindOrAddReportItem(ArrayList list, DateTime date)
		{
			ReportItem tempItem = new ReportItem(date);
			int tempReportIdx = list.BinarySearch(tempItem);

			if (tempReportIdx < 0)
				list.Insert(Math.Abs(tempReportIdx) - 1, tempItem);
			//list.Insert(list.Count,tempItem);
			else
				tempItem = (ReportItem)list[tempReportIdx];

			return tempItem;
		}
		private ItemDetail FindOrAddItemDetail(ArrayList list, DateTime shipdatetime, string invdescr, string sono, string shiptoname, string shiptocity, string shiptostate, string shiptozip, string type, string shiptodescr, string salesperson, string custname, string custcity, string custstate, string style, string size, string grade, string label, string variety, string commodity, string warehouse, string lotid, string growername, string commodityId, string varietyId, string warehouseId, string fillType, string arTrxHdrIdx, string customerKey)
		{
			ItemDetail tempItemDetail = new ItemDetail(shipdatetime, invdescr, sono, shiptoname, shiptocity, shiptostate, shiptozip, type, shiptodescr, salesperson, custname, custcity, custstate, style, size, grade, label, variety, commodity, warehouse, lotid, growername, commodityId, varietyId, warehouseId, fillType, arTrxHdrIdx, customerKey);
			int tempReportDetailIdx = list.BinarySearch(tempItemDetail);

			if (tempReportDetailIdx < 0)
				list.Insert(Math.Abs(tempReportDetailIdx) - 1, tempItemDetail);
			else
				tempItemDetail = (ItemDetail)list[tempReportDetailIdx];

			return tempItemDetail;
		}

		//private /*OrderNumber*/void FindOrAddOrderNumber(ArrayList list,string sono)
		//{
		//    OrderNumber tempOrderNumber = new OrderNumber(sono);
		//    int tempOrderNumberIdx = list.BinarySearch(tempOrderNumber);

		//    if(tempOrderNumberIdx <0)
		//        //list.Insert(Math.Abs(tempOrderNumberIdx)-1,new CriteriaBoxRecord(tempOrderNumber.SoNo,"",""));
		//        list.Insert(Math.Abs(tempOrderNumberIdx) - 1, tempOrderNumber);
		//    //else 
		//    //	tempOrderNumber = (OrderNumber)list[tempOrderNumberIdx];

		//    //return tempOrderNumber;
		//}

		//private Order FindOrAddOrder(ArrayList list,string sono)
		//{
		//    Order tempOrder = new Order(sono);
		//    int tempOrderIdx = list.BinarySearch(tempOrder);

		//    if(tempOrderIdx <0)
		//        list.Insert(Math.Abs(tempOrderIdx)-1,tempOrder);
		//    else 
		//        tempOrder = (Order)list[tempOrderIdx];

		//    return tempOrder;
		//}
		#endregion

		#region CBO
		public class ItemDetail : IComparable
		{
			#region Declares
			public DateTime ShipDateTime;
			public string Invdescr;
			public double FOBQnt;
			public double SalesQnt;
			public double Qnt;
			public double EquivQnt;
			//public double Weight;
			public string SoNo;
			public string ShipToName;
			public string ShipToCity;
			public string ShipToState;
			public string ShipToZip;
			public string Type;
			public string ShipToDescr;
			public string SalesPerson;
			public string CustName;
			public string CustCity;
			public string CustState;
			public string Style;
			public string Size;
			public string Grade;
			public string Label;
			public string Variety;
			public string Commodity;
			public string Warehouse;
			public string LotId;
			public string GrowerName;
			//public double FreightAmt;
			public string CommodityId;
			public string VarietyId;
			public string WarehouseId;
			public double Cost;
			public string FillType;
			public string ArTrxHdrIdx;
			public double ArAmt;
			public double ReceiptAmt;
			public string CustomerKey;
			#endregion

			#region Constructor/Dispose
			public ItemDetail()
			{
				ShipDateTime = Convert.ToDateTime("1/1/1900");
				Invdescr = String.Empty;
				FOBQnt = 0.0f;
				SalesQnt = 0.0f;
				Qnt = 0.0f;
				EquivQnt = 0.0f;
				//Weight = 0.0f;
				SoNo = String.Empty;
				ShipToName = String.Empty;
				ShipToCity = String.Empty;
				ShipToState = String.Empty;
				ShipToZip = String.Empty;
				Type = String.Empty;
				ShipToDescr = String.Empty;
				SalesPerson = String.Empty;
				CustName = String.Empty;
				CustCity = String.Empty;
				CustState = String.Empty;
				Style = String.Empty;
				Size = String.Empty;
				Grade = String.Empty;
				Label = String.Empty;
				Variety = String.Empty;
				Commodity = String.Empty;
				Warehouse = String.Empty;
				LotId = String.Empty;
				GrowerName = String.Empty;
				//FreightAmt = 0.0f;
				CommodityId = String.Empty;
				VarietyId = String.Empty;
				WarehouseId = String.Empty;
				Cost = 0.0f;
				FillType = String.Empty;
				ArTrxHdrIdx = String.Empty;
				ArAmt = 0.0f;
				ReceiptAmt = 0.0f;
				ArTrxHdrIdx = CustomerKey;
			}

			public ItemDetail(DateTime shipdatetime, string invdescr, string sono, string shiptoname, string shiptocity, string shiptostate, string shiptozip, string type, string shiptodescr, string salesperson, string custname, string custcity, string custstate, string style, string size, string grade, string label, string variety, string commodity, string warehouse, string lotid, string growername, string commodityId, string varietyId, string warehouseId, string fillType, string arTrxHdrIdx, string customerKey)
			{
				ShipDateTime = shipdatetime;
				Invdescr = invdescr;
				SoNo = sono;
				ShipToName = shiptoname;
				ShipToCity = shiptocity;
				ShipToState = shiptostate;
				ShipToZip = shiptozip;
				Type = type;
				ShipToDescr = shiptodescr;
				SalesPerson = salesperson;
				CustName = custname;
				CustCity = custcity;
				CustState = custstate;
				Style = style;
				Size = size;
				Grade = grade;
				Label = label;
				Variety = variety;
				Commodity = commodity;
				Warehouse = warehouse;
				LotId = lotid;
				GrowerName = growername;
				CommodityId = commodityId;
				VarietyId = varietyId;
				WarehouseId = warehouseId;
				Cost = 0.0f;
				FillType = fillType;
				ArTrxHdrIdx = arTrxHdrIdx;
				ArAmt = 0.0f;
				ReceiptAmt = 0.0f;
				CustomerKey = customerKey;
			}
			#endregion

			#region IComparable Members
			public int CompareTo(object obj)
			{
				int tempCompare;
				ItemDetail Y = (ItemDetail)obj;

				//Compare all
				/*tempCompare = this.Invdescr.CompareTo(Y.Invdescr);
				if(tempCompare != 0)
					return tempCompare;*/

				if ((tempCompare = this.SoNo.CompareTo(Y.SoNo)) != 0)
					return tempCompare;
				else if ((tempCompare = this.Invdescr.CompareTo(Y.Invdescr)) != 0)
					return tempCompare;
				else if ((tempCompare = this.ShipToName.CompareTo(Y.ShipToName)) != 0)
					return tempCompare;
				else if ((tempCompare = this.ShipToCity.CompareTo(Y.ShipToCity)) != 0)
					return tempCompare;
				else if ((tempCompare = this.ShipToState.CompareTo(Y.ShipToState)) != 0)
					return tempCompare;
				else if ((tempCompare = this.ShipToZip.CompareTo(Y.ShipToZip)) != 0)
					return tempCompare;
				else if ((tempCompare = this.Type.CompareTo(Y.Type)) != 0)
					return tempCompare;
				else if ((tempCompare = this.ShipToDescr.CompareTo(Y.ShipToDescr)) != 0)
					return tempCompare;
				else if ((tempCompare = this.SalesPerson.CompareTo(Y.SalesPerson)) != 0)
					return tempCompare;
				else if ((tempCompare = this.CustName.CompareTo(Y.CustName)) != 0)
					return tempCompare;
				else if ((tempCompare = this.CustCity.CompareTo(Y.CustCity)) != 0)
					return tempCompare;
				else if ((tempCompare = this.CustState.CompareTo(Y.CustState)) != 0)
					return tempCompare;
				else if ((tempCompare = this.Style.CompareTo(Y.Style)) != 0)
					return tempCompare;
				else if ((tempCompare = this.Size.CompareTo(Y.Size)) != 0)
					return tempCompare;
				else if ((tempCompare = this.Grade.CompareTo(Y.Grade)) != 0)
					return tempCompare;
				else if ((tempCompare = this.Label.CompareTo(Y.Label)) != 0)
					return tempCompare;
				else if ((tempCompare = this.Variety.CompareTo(Y.Variety)) != 0)
					return tempCompare;
				else if ((tempCompare = this.Commodity.CompareTo(Y.Commodity)) != 0)
					return tempCompare;
				else if ((tempCompare = this.Warehouse.CompareTo(Y.Warehouse)) != 0)
					return tempCompare;
				else if ((tempCompare = this.LotId.CompareTo(Y.LotId)) != 0)
					return tempCompare;
				else if ((tempCompare = this.GrowerName.CompareTo(Y.GrowerName)) != 0)
					return tempCompare;
				else if ((tempCompare = this.CommodityId.CompareTo(Y.CommodityId)) != 0)
					return tempCompare;
				else if ((tempCompare = this.VarietyId.CompareTo(Y.VarietyId)) != 0)
					return tempCompare;
				else if ((tempCompare = this.WarehouseId.CompareTo(Y.WarehouseId)) != 0)
					return tempCompare;
				else if ((tempCompare = this.FillType.CompareTo(Y.FillType)) != 0)
					return tempCompare;
				else if ((tempCompare = this.ArTrxHdrIdx.CompareTo(Y.ArTrxHdrIdx)) != 0)
					return tempCompare;
				else if ((tempCompare = this.CustomerKey.CompareTo(Y.CustomerKey)) != 0)
					return tempCompare;

				return 0;
			}
			#endregion
		}

		public class ReportItem : IComparable
		{
			#region Declares
			public ArrayList Details;

			public int Week;
			public int Year;
			#endregion

			#region Properties
			public DateTime StartDate { get { return Misc.WeekToStartDate(Week, Year); } }
			public DateTime EndDate { get { return Misc.WeekToEndDate(Week, Year); } }

			#endregion

			#region Constructor/Dispose
			public ReportItem(int week, int year)
			{
				//BaseSetup
				Details = new ArrayList();
				Week = week;
				Year = year;
			}

			public ReportItem(DateTime date)
			{
				//BaseSetup
				Details = new ArrayList();
				Week = Int32.Parse(Misc.DateToWeek(date));
				Year = date.Year;
			}
			#endregion

			#region IComparable Members

			public int CompareTo(object obj)
			{
				int tempCompare;
				ReportItem Y = (ReportItem)obj;

				//Compare StartDate
				tempCompare = this.StartDate.CompareTo(Y.StartDate);
				if (tempCompare != 0)
					return tempCompare;

				return 0;
			}

			#endregion
		}

		//public class OrderNumber :IComparable
		//{
		//    #region Declares
		//    public string SoNo;
		//    #endregion

		//    #region Constructor/Dispose
		//    public OrderNumber()
		//    {
		//        SoNo = String.Empty;
		//    }

		//    public OrderNumber(string sono)
		//    {
		//        SoNo = sono;
		//    }
		//    #endregion

		//    #region IComparable Members

		//    public int CompareTo(object obj)
		//    {
		//        int tempCompare;
		//        //OrderNumber Y = new OrderNumber(((CriteriaBoxRecord)obj).key);
		//        OrderNumber Y = (OrderNumber)obj;

		//        if((tempCompare = this.SoNo.CompareTo(Y.SoNo)) != 0)
		//            return tempCompare;

		//        return 0;
		//    }

		//    #endregion
		//}

		//public class Order :IComparable
		//{
		//    #region Declares
		//    public string SoNo;
		//    public ArrayList OrderLines;
		//    public double FreightAmount;
		//    public double TotalWeight;
		//    #endregion

		//    #region Constructor/Dispose
		//    public Order()
		//    {
		//        SoNo = String.Empty;
		//        FreightAmount = 0.0f;
		//        TotalWeight = 0.0f;
		//    }

		//    public Order(string sono)
		//    {
		//        SoNo = sono;
		//        FreightAmount = 0.0f;
		//        TotalWeight = 0.0f;
		//        OrderLines = new ArrayList();
		//    }
		//    #endregion

		//    #region IComparable Members

		//    public int CompareTo(object obj)
		//    {
		//        int tempCompare;
		//        Order Y = (Order) obj;

		//        if((tempCompare = this.SoNo.CompareTo(Y.SoNo)) != 0)
		//            return tempCompare;

		//        return 0;
		//    }

		//    #endregion
		//}
		#endregion
	}
}
