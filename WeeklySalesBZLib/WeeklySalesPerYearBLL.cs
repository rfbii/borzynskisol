using System;
using System.Collections;
using System.Web.UI;
using KeyCentral.Functions;
using System.Data;

namespace WeeklySales.App_Code
{
	public class WeeklySalesPerYearBLL :BaseBLL, IBaseBLL
	{
		#region Declares
		public ArrayList ReportItems;//ReportItem
		public ArrayList WeeklySummary;
		public ArrayList Years;

		private bool gross;
		private bool actual;
		private DateTime yearStartDate;
		//public SortedList ReportIndex;//key=sortstring,value=report index
		#endregion

		public WeeklySalesPerYearBLL(Page page):base(page){}

		public void GetWeekSalesData(bool gross, bool actual, string customer, string shipToIdx, string shipToLoc, ArrayList commodityIdx, string varietyIdx, string salesPerson, ArrayList warehouse,ArrayList commoditySizeIdx,DateTime yearStartDate)
		{
			this.gross = gross;
			this.actual = actual;
			this.yearStartDate = yearStartDate;

			WeeklySalesDAL dal = new WeeklySalesDAL(page);

			dal.GetWeekSalesData(customer,shipToIdx,shipToLoc,
									commodityIdx,varietyIdx,salesPerson,
									warehouse,commoditySizeIdx,"","",this);


			//return new ReportData();
		}
		public void GetWeekSalesData(bool gross, bool actual, string customer, string shipToIdx, string shipToLoc, ArrayList commodityIdx, ArrayList varietyIdx, string salesPerson, ArrayList warehouse,ArrayList commoditySizeIdx,DateTime yearStartDate)
		{
			this.gross = gross;
			this.actual = actual;
			this.yearStartDate = yearStartDate;

			WeeklySalesDAL dal = new WeeklySalesDAL(page);

			dal.GetWeekSalesData( customer,shipToIdx,shipToLoc,
				commodityIdx,varietyIdx,salesPerson,
				warehouse,new ArrayList(),commoditySizeIdx,"","",this);


			//return new ReportData();
		}
		

		#region IBaseBLL Members

		public void ProcessData(IDataReader reader)
		{
			
			WeeklySalesPerYearCBO.ReportItem tempItem;
			WeeklySalesPerYearCBO.WeeklyItem tempWeeklyItem;
			int itemIndex;
			int tempYear;
			ReportItems = new ArrayList();
			WeeklySummary = new ArrayList();
			Years = new ArrayList();

			//Setup the FieldNames that can change...
			string amountFieldName = "FOBQnt";
			if(gross )
				amountFieldName = "SalesQnt";

			string qntFieldName = "EquivQnt";
			if(actual)
				qntFieldName = "Qnt";

			//Get the Year Start Date
			Misc.WeekBaseData weekData = Misc.GetWeekBase(yearStartDate);

			while(reader.Read())
			{
				tempItem = new WeeklySalesPerYearCBO.ReportItem();
				tempYear = tempItem.SetDateValues( reader.GetDateTime(0),weekData);
				if(!reader.IsDBNull(1))
					tempItem.Desc = reader.GetString(1).Trim();
				else
					tempItem.Desc = "NULL";

				//TODO: Make more efficent by only doing look up if date or desc change
				itemIndex = GetIndex(tempItem);

				tempItem = (WeeklySalesPerYearCBO.ReportItem)ReportItems[itemIndex];

				tempItem.AddAmount(tempYear,(double)(decimal)reader[amountFieldName]);
				tempItem.AddQnt(tempYear,(double)(decimal)reader[qntFieldName]);


				//Do the summary data
				tempWeeklyItem = FindOrAddWeeklyItem(WeeklySummary,reader.GetDateTime(0),weekData);
				tempWeeklyItem.AddAmount(tempYear,(double)(decimal)reader[amountFieldName]);
				tempWeeklyItem.AddQnt(tempYear,(double)(decimal)reader[qntFieldName]);


					
				int yearIndex = Years.BinarySearch(tempYear);
				if(yearIndex<0)
					Years.Insert(Math.Abs(yearIndex)-1,tempYear);
					
			}

			reader.Close();
		}
		
		private WeeklySalesPerYearCBO.WeeklyItem FindOrAddWeeklyItem(ArrayList list,DateTime closeDate,Misc.WeekBaseData weekData)
		{
			WeeklySalesPerYearCBO.WeeklyItem tempItem = new WeeklySalesPerYearCBO.WeeklyItem();
			tempItem.SetDateValues(closeDate,weekData);

			int tempReportIdx = list.BinarySearch(tempItem);

			if(tempReportIdx <0)
				list.Insert(Math.Abs(tempReportIdx)-1,tempItem);
			else
				tempItem = (WeeklySalesPerYearCBO.WeeklyItem)list[tempReportIdx];

			return tempItem;
		}

		#endregion

		#region Helpers
		public int GetIndex(WeeklySalesPerYearCBO.ReportItem item)
		{
			int index = ReportItems.BinarySearch(item);

			if(index >=0)
				return index;
			else
			{
				index = Math.Abs(index)-1;
				ReportItems.Insert(index,item);
				return index;
			}
		}
		#endregion

	}
	public class WeeklySalesPerYearCBO
	{
		#region Declares
		public ArrayList ReportItems;//ReportItem
		public ArrayList Years;
		#endregion

		#region CBO
		public class ItemDetail
		{
			public double Amount;
			public double Qnt;

			public ItemDetail()
			{
				Amount = 0.0f;
				Qnt = 0.0f;
			}

			public ItemDetail(double amount,double qnt)
			{
				Amount = amount;
				Qnt = qnt;
			}
		}
		public class ReportItem :IComparable
		{
			#region Declares
			//public int Year;//if it's 2004 it means 2004-2005 sesson  when inserting remember this!
			public SortedList Details;//key=(int)year,value=(ItemDetail)
			public string Desc;
			
			//Date Vars
			public int Week;
			public DateTime StartDate;
			public DateTime EndDate;
			public int WeekInSession;
			#endregion

			#region Constructor/Dispose
			public ReportItem()
			{
				//BaseSetup
				Details = new SortedList();
				Desc = String.Empty;
			}
			#endregion

			public int SetDateValues(DateTime closeDate,Misc.WeekBaseData weekData)
			{
				Week = Misc.GetDateBasedWeek(closeDate,weekData,false); 
				StartDate = Misc.GetDateBasedWeekStartDate(Week,weekData,false);
				EndDate = Misc.GetDateBasedWeekEndDate(Week,weekData,false);

				if(Week< weekData.FirstWeekNumber)
				{
					WeekInSession = 53 -(weekData.FirstWeekNumber-Week);
					return Int32.Parse(Misc.GetDateBasedWeekYear(closeDate,weekData))-1;
				}
				else
				{
					WeekInSession = Week-weekData.FirstWeekNumber;
					return Int32.Parse(Misc.GetDateBasedWeekYear(closeDate,weekData));
				}
			}

			public void AddAmount(int Year,double amount)
			{
				ItemDetail tempItemDetail = (ItemDetail)Details[Year];

				if(tempItemDetail == null)
					Details.Add(Year,new ItemDetail(amount,0.0f));
				else
					tempItemDetail.Amount += amount;
			}

			public void AddQnt(int Year,double qnt)
			{
				ItemDetail tempItemDetail = (ItemDetail)Details[Year];

				if(tempItemDetail == null)
					Details.Add(Year,new ItemDetail(0.0f,qnt));
				else
					tempItemDetail.Qnt += qnt;
			}


			public double GetAmount(int year)
			{
				ItemDetail itemDetail = (ItemDetail)Details[year];

				if(itemDetail == null)
					return 0.0f;
				else
					return itemDetail.Amount;
			}
			public double GetQnt(int year)
			{
				ItemDetail itemDetail = (ItemDetail)Details[year];

				if(itemDetail == null)
					return 0.0f;
				else
					return itemDetail.Qnt;
			}


			#region IComparable Members
			public int CompareTo(object obj)
			{
				int tempCompare;
				ReportItem Y = (ReportItem) obj;

				//Compare WeekInSession
				tempCompare = this.WeekInSession.CompareTo(Y.WeekInSession);
				if(tempCompare != 0)
					return tempCompare;

				//Compare Desc
				tempCompare = this.Desc.CompareTo(Y.Desc);
				if(tempCompare != 0)
					return tempCompare;

				return 0;
			}

			#endregion
		}
		public class WeeklyItem:IComparable
		{
			#region Declares
			public SortedList Details;//key=(int)year,value=(ItemDetail)
			
			//Date Vars
			public int Week;
			public DateTime StartDate;
			public DateTime EndDate;
			public int WeekInSession;
			#endregion

			#region Constructor/Dispose
			public WeeklyItem()
			{
				//BaseSetup
				Details = new SortedList();
			}
			#endregion

			
			public int SetDateValues(DateTime closeDate,Misc.WeekBaseData weekData)
			{
				Week = Misc.GetDateBasedWeek(closeDate,weekData,false); 
				StartDate = Misc.GetDateBasedWeekStartDate(Week,weekData,false);
				EndDate = Misc.GetDateBasedWeekEndDate(Week,weekData,false);

				if(Week< weekData.FirstWeekNumber)
				{
					WeekInSession = 53 -(weekData.FirstWeekNumber-Week);
					return Int32.Parse(Misc.GetDateBasedWeekYear(closeDate,weekData))-1;
				}
				else
				{
					WeekInSession = Week-weekData.FirstWeekNumber;
					return Int32.Parse(Misc.GetDateBasedWeekYear(closeDate,weekData));
				}
			}

			
			public void AddAmount(int Year,double amount)
			{
				ItemDetail tempItemDetail = (ItemDetail)Details[Year];

				if(tempItemDetail == null)
					Details.Add(Year,new ItemDetail(amount,0.0f));
				else
					tempItemDetail.Amount += amount;
			}

			public void AddQnt(int Year,double qnt)
			{
				ItemDetail tempItemDetail = (ItemDetail)Details[Year];

				if(tempItemDetail == null)
					Details.Add(Year,new ItemDetail(0.0f,qnt));
				else
					tempItemDetail.Qnt += qnt;
			}


			public double GetAmount(int year)
			{
				ItemDetail itemDetail = (ItemDetail)Details[year];

				if(itemDetail == null)
					return 0.0f;
				else
					return itemDetail.Amount;
			}
			public double GetQnt(int year)
			{
				ItemDetail itemDetail = (ItemDetail)Details[year];

				if(itemDetail == null)
					return 0.0f;
				else
					return itemDetail.Qnt;
			}

			#region IComparable Members
			public int CompareTo(object obj)
			{
				int tempCompare;
				WeeklyItem Y = (WeeklyItem) obj;

				//Compare WeekInSession
				tempCompare = this.WeekInSession.CompareTo(Y.WeekInSession);
				if(tempCompare != 0)
					return tempCompare;

				return 0;
			}

			#endregion
		}
		#endregion
	}
}
