<%@ Page language="c#" Inherits="KeyCentral.UI.Error.ErrorPage" Codebehind="ErrorPage.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>ErrorPage</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1"/>
		<meta name="CODE_LANGUAGE" content="C#"/>
		<meta name="vs_defaultClientScript" content="JavaScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"--> 
			A server error has occurred.&nbsp; The details of the error are being E-mailed to 
			RFB now.&nbsp;  Please review the work you were doing as you got this Error Message to make sure
			all of your information has been saved.  If you have any questions please call RFB II.
		</form>
	</body>
</html>
