using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Configuration;
using KeyCentral.Functions;

namespace KeyCentral.UI.Error
{
	public partial class ErrorPage : KeyCentral.Functions.BasePage
	{
		protected void Page_Load(object sender, System.EventArgs e)
		{
			JavaStatusMessage jsMessage = new JavaStatusMessage(this);
			jsMessage.ShowMessage("E-mailing error report now");
			Exception lastError = (Exception)Session["CurrentError"];
			Session.Remove("CurrentError");

			try
			{
				if(lastError != null)
				{
					MailMessage mailMessage = new MailMessage();
					mailMessage.From = "ErrorMessage@rfbii.com";
					mailMessage.To = "keystone@rfbii.com";
					mailMessage.Subject = "KeyCentral Error Report";
					mailMessage.BodyFormat = MailFormat.Html;
					mailMessage.Body = CreateEmailBody(lastError);

					SmtpMail.SmtpServer = System.Configuration.ConfigurationSettings.AppSettings["EmailServer"];
					SmtpMail.Send(mailMessage);

					mailMessage = null;
				}
			}
			catch(Exception ex)
			{
				Response.Write("Error sending Error report. Message=" + ex.Message);
			}
		}
		private string CreateEmailBody(Exception ex)
		{
			string ret="<html><body>";
			
			ret += FormatHeader();

			ret += FormatSessionVars();

			ret += "<table>";
			ret += FormatException(ex);
			ret += "</table>";

			ret += FormatFormVars();

			//Remove the temp Session Vars before displaying the Session vars
			Session.Remove("DataOnLastPage");
			Session.Remove("ErrorUrl");
			Session.Remove("CurrentError");



			return ret + "</body></html>";
		}
		private string FormatHeader()
		{
			string ret ="<table><tr><td colspan=2><h2>Error Report</h2></td></tr>";

			ret += "<tr><td>Date/Time:</td><td>" + System.DateTime.Now.ToString() + "</td></tr>";
			ret += "<tr><td>ErrorUrl:</td><td>" + Session["ErrorUrl"] + "</td></tr>";
			
			return ret + "</table><br><br>";
		}
		private string FormatFormVars()
		{
			return "<table>" + Session["DataOnLastPage"] +"</table>";
		}
		private string FormatSessionVars()
		{
			string ret = "<h2>Session info</h2><table>";

			for(int x=0;x<Session.Count;x++)
			{
				//TODO: Expand ArrayLists
				ret += "<tr><td>" + Session.Keys[x].ToString() + "</td><td>" + Session[x].ToString() + "</td></tr>";
			}

			return ret + "</table>";
		}
		private string FormatException(Exception ex)
		{
			string ret="";

			ret += "<tr><td>TargetSite</td><td>" +ex.TargetSite + "</td></tr>";
			ret += "<tr><td>Source</td><td>" +ex.Source + "</td></tr>";
			
			if(ex.Message.Length>100)
				ret += "<tr><td>Message</td><td>" +ex.Message.Substring(0,100) + "</td></tr>";
			else
				ret += "<tr><td>Message</td><td>" +ex.Message + "</td></tr>";

			ret += "<tr><td>Stack Trace</td><td>" +ex.StackTrace + "</td></tr>";

			ret += "<tr><td></td><td>&nbsp;</td></tr>";

			if(ex.InnerException != null)
				return ret + FormatException(ex.InnerException);
			else
				return ret;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
