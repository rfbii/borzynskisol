using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Data.SqlClient;
using KeyCentral.Functions;
using KeyCentral.Lookups;


namespace KeyCentral.UI.ContextMenu
{
    public partial class LookupSearch : System.Web.UI.Page
    {
        #region Declares
        protected LookupSearchData lookupData;
        #endregion

        #region Events
        protected void Page_Load(object sender, System.EventArgs e)
        {
            lookupData = GetSearchItems();

            DisplaySearchControls(lookupData);

            if (!IsPostBack && Request.QueryString["StartOn"] == "Lookup")
                ShowData();

            if (Request.QueryString["LookupType"] == "CustomerSingle")
                this.setFav.Visible = true;

            //if (Request.QueryString["LookupType"] == "CustomerSingleCustomerStatus")
            //    this.setFav.Visible = true;
            
            if (Request.QueryString["LookupType"] == "SalespersonSingleFreightAllocation")
            {
                this.setFav.Text = "Add to Limit";
                this.done.Visible = false;
                this.setFav.Visible = true;
            }

            if (Request.QueryString["LookupType"] == "Lot")
            {
                this.setFav.Visible = false;
            }
        }
        protected void search_Click(object sender, System.EventArgs e)
        {
            try
            {
                ShowData();
            }
            catch (Exception ex)
            {
                this.cancel.Text = BuildDataString() + "\n" + ex.ToString();
            }
        }
        protected void done_Click(object sender, System.EventArgs e)
        {
            ArrayList selected = new ArrayList();
            Session.Add("VarietySingle", "");
            Session.Add("SalespersonSingle", "");
            Session.Add("CustomerSingle", "");
            Session.Add("CustomerSingleCustomerStatus", "");            

            //1234~5678!Name
            if (Request.QueryString["SingleLookup"] == "true")
            {
                string ret = "";
                foreach (DataGridItem row in lookupGrid.Items)
                {
                    if (((CheckBox)row.Cells[0].FindControl("selected")).Checked == true)
                    {
                        ret = row.Cells[1].Text;
                    }

                    if (Request.QueryString["LookupType"] == "VarietySingle")
                    {
                        Session.Add("VarietySingle", ret);
                    }
                    if (Request.QueryString["LookupType"] == "SalespersonSingle")
                    {
                        Session.Add("SalespersonSingle", ret);
                    }
                    if (Request.QueryString["LookupType"] == "CustomerSingle")
                    {
                        Session.Add("CustomerSingle", ret);
                    }
                    if (Request.QueryString["LookupType"] == "CustomerSingleCustomerStatus")
                    {
                        Session.Add("CustomerSingleCustomerStatus", ret);
                    }                   
                }
                
                Response.Write("<script>window.close();</script>");
               //Response.Write("<script>returnValue=\"" + ret + "\";window.close();</script>");
            }
            else
            {
                foreach (DataGridItem row in lookupGrid.Items)
                {
                    if (((CheckBox)row.Cells[0].FindControl("selected")).Checked == true)
                    {
                        selected.Add(row.Cells[1].Text);
                    }
                }
                Session.Add(Request.QueryString["LookupType"] + ":Lookup", selected);
                Session["Lookup"] = Session[Request.QueryString["LookupType"] + ":Lookup"];
                Response.Write("<script>returnValue='';window.close();</script>");
            }
        }
        protected void cancel_Click(object sender, System.EventArgs e)
        {
            Response.Write("<script>window.close();</script>");
        }
        #endregion

        #region Private Helpers
        #region Private DataBase Function

        private string BuildDataString()
        {
            string reportCommand = KeyCentral.Functions.Misc.FormatOracle(Session, lookupData.LookupSQL);

            KeyCentral.Functions.WhereClauseBuilder whereClause = new WhereClauseBuilder();

            //build the functions
            for (int x = 0; x < lookupData.SearchItems.Count; x++)
            {
                SearchItem item = (SearchItem)lookupData.SearchItems[x];
                string searchString;

                if (IsPostBack)//load from user enterd field on postback
                    searchString = (string)Request[item.FieldName];
                else//load from QueryString to run the search the first time.
                    searchString = Request.QueryString["LookupSearchParam" + x.ToString()];

                switch (item.Type)
                {
                    case SearchType.String:
                        whereClause.Add(WhereClauseUnit.UnitType.LikeString, item.FieldName, searchString);
                        break;
                    case SearchType.Limit:
                        whereClause.Add(WhereClauseUnit.UnitType.Int32, item.FieldName, CriteriaBoxRecord.GetArrayOfKeys((ArrayList)Session[item.Description]));
                        break;
                }
            }
            //whereClause.Add(Reports.Code.WhereClauseUnit.UnitType.Int32,"Ga_Lot.GaLotIdx",(ArrayList)ViewState["LotIdx"]);
            //whereClause.Add(Reports.Code.WhereClauseUnit.UnitType.Int32,"Fc_Name.NameIdx",(ArrayList)ViewState["GrowerIdx"]);
            //Put the where clause into our SQL command
            return whereClause.BuildWhereClause(reportCommand);
        }

        private void ShowData()
        {
            OracleConnection OracleConn = KeyCentral.Functions.Misc.GetOracleConn(Session);
            DataSet dsRet = new DataSet();
            OracleDataAdapter OracleData = new OracleDataAdapter(BuildDataString(), OracleConn);

            OracleData.SelectCommand.BindByName = true;

            OracleData.Fill(dsRet);

            //Fill in columns dynamicly to remove the Keys field.
            foreach (DataColumn column in dsRet.Tables[0].Columns)
            {
                if (column.ColumnName != "Keys")
                {
                    BoundColumn temp = new BoundColumn();
                    temp.HeaderText = column.ColumnName;
                    temp.DataField = column.ColumnName;
                    lookupGrid.Columns.Add(temp);
                }
            }

            this.lookupGrid.DataSource = dsRet;
            lookupGrid.DataBind();

            dsRet.Dispose();
            OracleData.Dispose();
            OracleConn.Close();
            OracleConn.Dispose();
        }
        #endregion
        private LookupSearchData GetSearchItems()
        {
            //Note the Key field must be called Keys!
            LookupSearchData lookupData = new LookupSearchData();
            ArrayList searchItems = new ArrayList();
            if (Request.QueryString["LookupType"] == "Lot")
            {
                searchItems.Add(new SearchItem("Lot ID:&nbsp;", "Ga_Lot.ID", SearchType.String));
                searchItems.Add(new SearchItem("Description:&nbsp;", "Ga_Lot.Desc", SearchType.String));
                lookupData.LookupSQL = "select Ga_Lot.GaLotIdx as \"Keys\" ,Ga_Lot.Id,SUBSTR(Ga_Lot.descr,0,20) as \"Description\" from #COMPANY_NUMBER#.Ga_Lot where 1=1 #WhereClause:Ga_Lot.ID# #WhereClause:Ga_Lot.Desc#";
            }
            else if (Request.QueryString["LookupType"] == "Grower")
            {
                searchItems.Add(new SearchItem("Grower Name:&nbsp;", "Fc_Name.LastCoName", SearchType.String));
                searchItems.Add(new SearchItem("Id:&nbsp;", "Fc_Name.Id", SearchType.String));
                searchItems.Add(new SearchItem("Location:&nbsp;", "Fc_Name_Location.Descr", SearchType.String));
                searchItems.Add(new SearchItem("GrowerLimit", "Fc_Name.NameIdx", SearchType.Limit));

                Functions.Security.BuildLimitedToKeys(Session, "grower", "GrowerLimit");
                lookupData.LookupSQL = "select Ga_Grower.GrowerNameIdx as \"Keys\" ,Fc_Name.LastCoName as \"Grower Name\",Fc_Name.Id,Fc_Name_Location.Descr from #COMPANY_NUMBER#.Ga_Grower,#COMPANY_NUMBER#.Fc_Name,#COMPANY_NUMBER#.Fc_Name_Location where Ga_Grower.GrowerNameIdx=Fc_Name.NameIdx and Fc_Name.NameIdx (+)= Fc_Name_Location.NameIdx and Fc_Name_Location.NameLocationSeq (+)= 1 #WhereClause:Fc_Name.LastCoName# #WhereClause:Fc_Name.Id# #WhereClause:Fc_Name_Location.Descr# #WhereClause:Fc_Name.NameIdx# order by 2";
            }
            else if (Request.QueryString["LookupType"] == "Commodity")
            {
                searchItems.Add(new SearchItem("Commodity Name:&nbsp;", "Ic_Ps_Commodity.Name", SearchType.String));

                Functions.Security.BuildLimitedToKeys(Session, "Commodity", "CommodityLimit");
                lookupData.LookupSQL = "Select Ic_Ps_Commodity.CmtyIdx as \"Keys\",	Ic_Ps_Commodity.Name from	#COMPANY_NUMBER#.Ic_Ps_Commodity where 1=1 #WhereClause:Ic_Ps_Commodity.Name# order by 2";
            }
            else if (Request.QueryString["LookupType"] == "CommoditySize")
            {
                searchItems.Add(new SearchItem("Commodity Name:&nbsp;", "Ic_Ps_Commodity.Name", SearchType.String));
                searchItems.Add(new SearchItem("Style", "Ic_Ps_Style.Name", SearchType.String));
                searchItems.Add(new SearchItem("Size", "Ic_Ps_Size.Name", SearchType.String));

                Functions.Security.BuildLimitedToKeys(Session, "CommoditySize", "CommoditySizeLimit");
                lookupData.LookupSQL = "select Ic_Ps_Size.SizeIdx as \"Keys\", Ic_Ps_Commodity.Name as \"Commodity\",Ic_Ps_Style.Name as \"Style\" ,Ic_Ps_Size.Name as \"Size\" from #COMPANY_NUMBER#.Ic_Ps_Commodity ,#COMPANY_NUMBER#.Ic_Ps_Size ,#COMPANY_NUMBER#.Ic_Ps_Style where Ic_Ps_Size.CmtyIdx = Ic_Ps_Commodity.CmtyIdx and Ic_Ps_Style.StyleIdx = Ic_Ps_Size.StyleIdx #WhereClause:Ic_Ps_Commodity.Name# #WhereClause:Ic_Ps_Style.Name# #WhereClause:Ic_Ps_Size.Name#  order by 2,3,4 ";
            }
            else if (Request.QueryString["LookupType"] == "Warehouse")
            {
                searchItems.Add(new SearchItem("Warehouse Name:&nbsp;", "Fc_Name_Location.Descr", SearchType.String));

                if (Session["WarehouseLimit"] != null && ((ArrayList)Session["WarehouseLimit"]).Count != 0)
                    searchItems.Add(new SearchItem("WarehouseLimit", "warehouse.WarehouseIdx", SearchType.Limit));
                else if (Session["WarehouseValid"] != null && ((ArrayList)Session["WarehouseValid"]).Count != 0)
                    searchItems.Add(new SearchItem("WarehouseValid", "warehouse.WarehouseIdx", SearchType.Limit));
                else//WarehouseLimit should be there even if it is empty
                    searchItems.Add(new SearchItem("WarehouseLimit", "warehouse.WarehouseIdx", SearchType.Limit));


                //KeyCentral.Functions.Security.BuildLimitedToKeys(Session,"Warehouse","WarehouseLimit");
                lookupData.LookupSQL = "select warehouse.WarehouseIdx as \"Keys\" ,Fc_Name_Location.Descr as \"Warehouse Name\",Fc_Name_Location.City from #COMPANY_NUMBER#.Ic_Warehouse warehouse,#COMPANY_NUMBER#.Fc_Name_Location where warehouse.NameIdx = Fc_Name_Location.NameIdx and warehouse.NameLocationSeq = Fc_Name_Location.NameLocationSeq #WhereClause:Fc_Name_Location.Descr# #WhereClause:warehouse.WarehouseIdx# order by 2";
            }
            else if (Request.QueryString["LookupType"] == "GrowerBlock")
            {
                searchItems.Add(new SearchItem("Grower Block Name:&nbsp;", "Ga_Block.Name", SearchType.String));
                searchItems.Add(new SearchItem("Grower Block Id:&nbsp;", "Ga_Block.Id", SearchType.String));

                lookupData.LookupSQL = "select Ga_Block.GaBlockIdx as \"Keys\" ,Ga_Block.Id,Ga_Block.Name as \"Grower Block Name\" from #COMPANY_NUMBER#.Ga_Block where 1=1 #WhereClause:Ga_Block.Name# #WhereClause:Ga_Block.Id# order by 2";

            }
            else if (Request.QueryString["LookupType"] == "CustomerSingleCustomerStatus")
            {
                searchItems.Add(new SearchItem("Customer ID:&nbsp;", "Fc_Name.Id", SearchType.String));
                searchItems.Add(new SearchItem("Customer Name:&nbsp;", "Fc_Name.LastCoName", SearchType.String));
                lookupData.LookupSQL = "Select Fc_Name.Id as \"Keys\",Fc_Name.Id, Fc_Name.LastCoName,	Fc_Name_Location.City from	#COMPANY_NUMBER#.Ar_Cust,	#COMPANY_NUMBER#.Fc_Name,	#COMPANY_NUMBER#.Fc_Name_Location where	Ar_Cust.CustNameIdx		= Fc_Name.NameIdx and	Fc_Name.NameIdx			= Fc_Name_Location.NameIdx and	Fc_Name_Location.OrderBy	= '1' #WhereClause:Fc_Name.Id# #WhereClause:Fc_Name.LastCoName# order by 2";
            }
            else if (Request.QueryString["LookupType"] == "CustomerSingle")
            {
                searchItems.Add(new SearchItem("Customer ID:&nbsp;", "Fc_Name.Id", SearchType.String));
                searchItems.Add(new SearchItem("Customer Name:&nbsp;", "Fc_Name.LastCoName", SearchType.String));
                lookupData.LookupSQL = "Select Fc_Name.NameIdx || '~0!' || Fc_Name.Id as \"Keys\",Fc_Name.Id, Fc_Name.LastCoName,	Fc_Name_Location.City from	#COMPANY_NUMBER#.Ar_Cust,	#COMPANY_NUMBER#.Fc_Name,	#COMPANY_NUMBER#.Fc_Name_Location where	Ar_Cust.CustNameIdx		= Fc_Name.NameIdx and	Fc_Name.NameIdx			= Fc_Name_Location.NameIdx and	Fc_Name_Location.OrderBy	= '1' #WhereClause:Fc_Name.Id# #WhereClause:Fc_Name.LastCoName# order by 2";
            }
            else if (Request.QueryString["LookupType"] == "CommoditySingle")
            {
                searchItems.Add(new SearchItem("Commodity Name:&nbsp;", "Ic_Ps_Commodity.Name", SearchType.String));
                //lookupData.LookupSQL = "Select Ic_Ps_Commodity.Name as \"Keys\",Ic_Ps_Commodity.Name from	#COMPANY_NUMBER#.Ic_Ps_Commodity where	'1' = '1' #WhereClause:Ic_Ps_Commodity.Name# order by 2";
                lookupData.LookupSQL = "Select Ic_Ps_Commodity.CmtyIdx || '!' || Ic_Ps_Commodity.Name as \"Keys\",Ic_Ps_Commodity.Name from	#COMPANY_NUMBER#.Ic_Ps_Commodity where	'1' = '1' #WhereClause:Ic_Ps_Commodity.Name# order by 2";
            }
            else if (Request.QueryString["LookupType"] == "ShiptoSingle")
            { //NameIdx~NameLocationSeq!LastCoName,Location.Descr
                searchItems.Add(new SearchItem("Ship To ID:&nbsp;", "Fc_Name.Id", SearchType.String));
                searchItems.Add(new SearchItem("Ship To Name:&nbsp;", "Fc_Name.LastCoName", SearchType.String));
                lookupData.LookupSQL = "Select Fc_Name.NameIdx || '~' || Fc_Name_Location.NameLocationSeq || '!' || Fc_Name.LastCoName || ',' || Fc_Name_Location.Descr as \"Keys\", Fc_Name.LastCoName,Fc_Name_Location.Descr from	#COMPANY_NUMBER#.Ar_Cust,	#COMPANY_NUMBER#.Fc_Name,	#COMPANY_NUMBER#.Fc_Name_Location where	Ar_Cust.CustNameIdx		= Fc_Name.NameIdx and	Fc_Name.NameIdx			= Fc_Name_Location.NameIdx #WhereClause:Fc_Name.Id# #WhereClause:Fc_Name.LastCoName# order by 2";
            }
            else if (Request.QueryString["LookupType"] == "VarietySingle")
            {//1234~5678!Name
                searchItems.Add(new SearchItem("Variety Name:&nbsp;", "Ic_Ps_Variety.Name", SearchType.String));
                searchItems.Add(new SearchItem("Commodity Name:&nbsp;", "Ic_Ps_Commodity.Name", SearchType.String));
                lookupData.LookupSQL = "Select Ic_Ps_Variety.VarietyIdx || '~' || Ic_Ps_Variety.CmtyIdx || '!' || Ic_Ps_Variety.Name as \"Keys\",Ic_Ps_Commodity.Name as Commodity ,Ic_Ps_Variety.Name from #COMPANY_NUMBER#.Ic_Ps_Commodity ,#COMPANY_NUMBER#.Ic_Ps_Variety  where Ic_Ps_Commodity.CmtyIdx = Ic_Ps_Variety.CmtyIdx #WhereClause:Ic_Ps_Variety.Name# #WhereClause:Ic_Ps_Commodity.Name# order by 2";
            }
            else if (Request.QueryString["LookupType"] == "SalespersonSingle")
            {
                searchItems.Add(new SearchItem("Salesperson ID:&nbsp;", "Fc_Name.Id", SearchType.String));
                searchItems.Add(new SearchItem("Salesperson Name:&nbsp;", "Fc_Name.LastCoName", SearchType.String));
                lookupData.LookupSQL = "Select Fc_Name.NameIdx || '~0!' || Fc_Name.Id as \"Keys\",Fc_Name.Id, Fc_Name.LastCoName from	#COMPANY_NUMBER#.Fc_Saleperson,	#COMPANY_NUMBER#.Fc_Name where	Fc_Saleperson.SalespersonNameIdx		= Fc_Name.NameIdx #WhereClause:Fc_Name.Id# #WhereClause:Fc_Name.LastCoName# order by 2";
            }
            else if (Request.QueryString["LookupType"] == "SalespersonSingleFreightAllocation")
            {
                searchItems.Add(new SearchItem("Salesperson ID:&nbsp;", "Fc_Name.Id", SearchType.String));
                searchItems.Add(new SearchItem("Salesperson Name:&nbsp;", "Fc_Name.LastCoName", SearchType.String));
                lookupData.LookupSQL = "Select Fc_Name.NameIdx as \"Keys\",Fc_Name.Id, Fc_Name.LastCoName from	#COMPANY_NUMBER#.Fc_Saleperson,	#COMPANY_NUMBER#.Fc_Name where	Fc_Saleperson.SalespersonNameIdx		= Fc_Name.NameIdx #WhereClause:Fc_Name.Id# #WhereClause:Fc_Name.LastCoName# order by 2";
            }
            else if (Request.QueryString["LookupType"] == "Phase")
            {
                searchItems.Add(new SearchItem("Phase ID:&nbsp;", "Ca_Phase.ID", SearchType.String));
                searchItems.Add(new SearchItem("Description:&nbsp;", "Ca_Phase.Name", SearchType.String));
                lookupData.LookupSQL = "select Ca_Phase.PhaseIdx as \"Keys\" ,Ca_Phase.Id,SUBSTR(Ca_Phase.Name,0,20) as \"Description\" from #COMPANY_NUMBER#.Ca_Phase where 1=1 #WhereClause:Ca_Phase.ID# #WhereClause:Ca_Phase.Name#";
            }
            else if (Request.QueryString["LookupType"] == "CostCenter")
            {
                searchItems.Add(new SearchItem("Cost Center ID:&nbsp;", "Ca_Cost_Center.ID", SearchType.String));
                searchItems.Add(new SearchItem("Description:&nbsp;", "Ca_Cost_Center.Name", SearchType.String));
                lookupData.LookupSQL = "select Ca_Cost_Center.CostCenterIdx as \"Keys\" ,Ca_Cost_Center.Id,SUBSTR(Ca_Cost_Center.Name,0,20) as \"Description\" from #COMPANY_NUMBER#.Ca_Cost_Center where 1=1 #WhereClause:Ca_Cost_Center.ID# #WhereClause:Ca_Cost_Center.Name#";
            }
            else if (Request.QueryString["LookupType"] == "VendorName")
            {
                searchItems.Add(new SearchItem("Vendor ID:&nbsp;", "Fc_Name.Id", SearchType.String));
                searchItems.Add(new SearchItem("Description:&nbsp;", "Fc_Name.LastCoName", SearchType.String));
                lookupData.LookupSQL = "select Fc_Name.NameIdx as \"Keys\" ,Fc_Name.Id,SUBSTR(Fc_Name.LastCoName,0,20) as \"Description\" from #COMPANY_NUMBER#.Fc_Name,#COMPANY_NUMBER#.Ap_Vendor where Ap_Vendor.VendNameIdx = Fc_Name.NameIdx #WhereClause:Fc_Name.Id# #WhereClause:Fc_Name.LastCoName# order by Fc_Name.Id";
            }
            lookupData.SearchItems = searchItems;
            return lookupData;
        }
        private void DisplaySearchControls(LookupSearchData lookupData)
        {
            searchDiv.Controls.Add(new System.Web.UI.LiteralControl("\n<table>"));
            for (int x = 0; x < lookupData.SearchItems.Count; x++)
            {
                SearchItem item = (SearchItem)lookupData.SearchItems[x];
                switch (item.Type)
                {
                    case SearchType.String:
                        TextBox box = new TextBox();
                        box.ID = item.FieldName;

                        if (Page.IsPostBack)
                            box.Text = Request[box.ID];
                        else//Try to load default search info
                            box.Text = Request.QueryString["LookupSearchParam" + x.ToString()];

                        searchDiv.Controls.Add(new System.Web.UI.LiteralControl("\n<tr><td>"));
                        searchDiv.Controls.Add(new System.Web.UI.LiteralControl(item.Description + "</td><td>"));
                        searchDiv.Controls.Add(box);
                        searchDiv.Controls.Add(new System.Web.UI.LiteralControl("</td></tr>"));

                        break;
                }
            }
            searchDiv.Controls.Add(new System.Web.UI.LiteralControl("\n</table>"));
        }
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        protected void setFav_Click(object sender, System.EventArgs e)
        {
            //if (Request.QueryString["LookupType"] == "CustomerSingleCustomerStatus")              
            if (Request.QueryString["LookupType"] == "CustomerSingle")
            {
                SqlConnection Conn = KeyCentral.Functions.Misc.GetSQLConn();

                //SqlCommand deleteCMD = new SqlCommand("delete from Customer_Status_Fav where user_Key=@User_Key",Conn);
                SqlCommand addCMD = new SqlCommand("use CustomerStatus; insert into Customer_Status_Fav (User_Key,Customer_ID) values(@User_Key,@Customer_ID)", Conn);

                //deleteCMD.Parameters.Add("@User_Key",Session["User_Key"]);
                //deleteCMD.ExecuteNonQuery();

                foreach (DataGridItem row in lookupGrid.Items)
                    if (((CheckBox)row.Cells[0].FindControl("selected")).Checked == true)
                    {
                        addCMD.Parameters.Clear();
                        addCMD.Parameters.AddWithValue("@User_Key", Session["User_Key"]);
                        //addCMD.Parameters.AddWithValue("@Customer_ID", row.Cells[1].Text);
                        addCMD.Parameters.AddWithValue("@Customer_ID", Misc.ParseTildaString(row.Cells[1].Text.Replace("!", "~"), 2));
                        addCMD.ExecuteNonQuery();
                    }

                //deleteCMD.Dispose();
                addCMD.Dispose();
                Conn.Close();
                Conn.Dispose();
                Response.Write("<script>window.close();</script>");
            }
            if (Request.QueryString["LookupType"] == "SalespersonSingleFreightAllocation")
            {
                SqlConnection Conn = Misc.GetSQLConn();

                SqlCommand addCMD = new SqlCommand(@"use FreightAllocation;
						if( not exists (select * from Freight_Allocation_Limit where User_Key = @User_Key and Salesman_Id =@Salesman_ID)) 
							insert into Freight_Allocation_Limit (User_Key,Salesman_ID) values(@User_Key,@Salesman_ID)"
                        , Conn);


                foreach (DataGridItem row in lookupGrid.Items)
                    if (((CheckBox)row.Cells[0].FindControl("selected")).Checked == true)
                    {
                        addCMD.Parameters.Clear();
                        addCMD.Parameters.Add("@User_Key", Session["User_Key"]);
                        addCMD.Parameters.Add("@Salesman_ID", row.Cells[1].Text);
                        addCMD.ExecuteNonQuery();
                    }

                //deleteCMD.Dispose();
                addCMD.Dispose();
                Conn.Close();
                Conn.Dispose();
                Response.Write("<script>window.close();</script>");
            }
        }


    }
}
