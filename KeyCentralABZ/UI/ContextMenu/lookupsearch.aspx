<%@ Page language="c#" Inherits="KeyCentral.UI.ContextMenu.LookupSearch" Codebehind="LookupSearch.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>LookupSearch</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<meta http-equiv="CACHE-CONTROL" content="NO-CACHE"/>
		<meta http-equiv="PRAGMA" content="NO-CACHE"/>
		<base target="_self"/>
	</head>
	<body>
		<script type="text/javascript" language="javascript">
		    function SelectAll() {
		        var checkBoxes = document.getElementsByTagName("Input");
		        for (var x = 0; x < checkBoxes.length; x++)
		            if (checkBoxes[x].name.indexOf("selected") > -1)
		                checkBoxes[x].checked = true;
		    }
		    function ClearAll() {
		        var checkBoxes = document.getElementsByTagName("Input");
		        for (var x = 0; x < checkBoxes.length; x++)
		            if (checkBoxes[x].name.indexOf("selected") > -1)
		                checkBoxes[x].checked = false;
		    }
		    function InvertSelected() {
		        var checkBoxes = document.getElementsByTagName("Input");
		        for (var x = 0; x < checkBoxes.length; x++)
		            if (checkBoxes[x].name.indexOf("selected") > -1)
		                checkBoxes[x].checked = !checkBoxes[x].checked;
		    }

		</script>
		<form id="Form1" method="post" runat="server">
			<div id="lookupDiv" style="BORDER-RIGHT: 2px inset; BORDER-TOP: 2px inset; Z-INDEX: 101; LEFT: 304px; OVERFLOW: auto; BORDER-LEFT: 2px inset; WIDTH: 448px; BORDER-BOTTOM: 2px inset; POSITION: absolute; TOP: 16px; HEIGHT: 368px"
				runat="server"><br/>
				<asp:datagrid id="lookupGrid" runat="server" AutoGenerateColumns="False" AllowSorting="True">
					<ItemStyle Font-Size="8pt" Wrap="False"></ItemStyle>
					<Columns>
						<asp:TemplateColumn>
							<ItemTemplate>
								<asp:CheckBox id="selected" runat="server"></asp:CheckBox>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:BoundColumn Visible="False" DataField="Keys"></asp:BoundColumn>
					</Columns>
				</asp:datagrid></div>
			<asp:Button id="setFav" style="Z-INDEX: 111; LEFT: 648px; POSITION: absolute; TOP: 392px" runat="server"
				Text="Add to Fav." onclick="setFav_Click"></asp:Button>
			<input style="Z-INDEX: 110; LEFT: 504px; POSITION: absolute; TOP: 392px" type="button"
				value="Invert Selection" onclick="InvertSelected();"/> <input style="Z-INDEX: 109; LEFT: 421px; POSITION: absolute; TOP: 392px" type="button"
				value="Clear All" onclick="ClearAll();"/> <input style="Z-INDEX: 108; LEFT: 304px; POSITION: absolute; TOP: 392px" type="button"
				value="Select All" onclick="SelectAll();"/>
			<div id="searchDiv" style="BORDER-RIGHT: 2px inset; BORDER-TOP: 2px inset; Z-INDEX: 102; LEFT: 16px; BORDER-LEFT: 2px inset; WIDTH: 264px; BORDER-BOTTOM: 2px inset; POSITION: absolute; TOP: 16px; HEIGHT: 368px"
				runat="server"><br/>
			</div>
			<div style="Z-INDEX: 103; LEFT: 376px; WIDTH: 56px; POSITION: absolute; TOP: 10px; HEIGHT: 16px; BACKGROUND-COLOR: white">&nbsp;Lookup</div>
			<div style="Z-INDEX: 104; LEFT: 24px; WIDTH: 56px; POSITION: absolute; TOP: 10px; HEIGHT: 16px; BACKGROUND-COLOR: white">&nbsp;Search</div>
			<asp:button id="cancel" style="Z-INDEX: 107; LEFT: 216px; POSITION: absolute; TOP: 392px" runat="server"
				Text="Cancel" onclick="cancel_Click"></asp:button>
			<asp:button id="done" style="Z-INDEX: 106; LEFT: 124px; POSITION: absolute; TOP: 392px" runat="server"
				Text="Done" onclick="done_Click"></asp:button>
			<asp:button id="search" style="Z-INDEX: 105; LEFT: 16px; POSITION: absolute; TOP: 392px" runat="server"
				Text="Search" onclick="search_Click"></asp:button>
		</form>
	</body>
</html>
