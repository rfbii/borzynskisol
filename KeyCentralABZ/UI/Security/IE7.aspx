<%@ Page language="c#" Inherits="KeyCentral.UI.Security.IE7" Codebehind="IE7.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>IE7</title>
		<script type="text/javascript" language="JAVASCRIPT">
			function fix()
			{
				document.title = s;	
			}
		</script>
		<link rel="stylesheet" type="text/css" href="../../../StyleSheet1.css"/>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<table bgcolor="navy" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td bgcolor="navy">
						<table cellpadding="0" height="40" cellspacing="0">
							<tr>
								<td class="BaseHeaderLogo" valign="top" height="40" width="17" colspan="2" rowspan="1"><font size="5">R</font></td>
								<td class="BaseHeaderLogo" width="13" height="40"><font size="5">F</font></td>
								<td class="BaseHeaderLogo" valign="bottom" width="3" height="40"><font size="5">B</font></td>
								<td class="BaseHeaderLogoB" height="40"><font color="white" size="5">II Custom 
										Solutions </font>
								</td>
								<td width="13"></td>
								<td class="BaseHeaderLogo" height="40"><font size="5">- Key Central</font><font size="4"><sup> &reg;</sup></font></td>
								<td width="300" align="right"><font color="silver"><%= KeyCentral.Functions.Misc.GetVersionString(this) %></font></td>
							</tr>
							<tr>
								<td height="1" width="17"></td>
							</tr>
							<tr height="1">
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br />
			<table>
				<tr>
					<td width="760" style="padding-left:5px;">
					    <font face="Verdana"><span style="font-size: 12pt">
					    You have been directed to this page because you're using Internet Explorer 7 with the security settings not set correctly.<br /><br />
					    To be able to run KeyCentral's full capabilities, certain security settings need to be enabled.<br /><br />
					    To enable them, you must run this <a href="reg.exe">executable file</a>. Click on the link <a href="reg.exe">executable file</a> to begin. When the box pops up you need to click "Run" each time it comes up and then click ok when you see the "Done!" message.<br /><br />
					    After you run it, you must <a href="javascript:window.close();">close this window</a>.
					    </span></font></td>
				</tr>
			</table>
		</form>
	</body>
</html>
