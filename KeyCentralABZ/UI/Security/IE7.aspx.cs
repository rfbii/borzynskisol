using System;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using KeyCentral.Functions;

namespace KeyCentral.UI.Security
{
	/// <summary>
	/// Summary description for Login.
	/// </summary>
	public partial class IE7 : Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new EventHandler(this.Page_Load);
		}
		#endregion
	}
}
