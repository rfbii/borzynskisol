<%@ Page language="c#" Inherits="KeyCentral.UI.Security.Login" Codebehind="Login.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Login</title>
		<script type="text/javascript" language="JAVASCRIPT">
			function fix()
			{
				document.title = s;	
			}
		</script>
		<script type="text/javascript">
			var newWin;
			function SetFocusToUserName()
			{
			
	            var userName = document.getElementById('userName');
	            
	            if(userName !=undefined )
	                userName.focus();
			}
			function LoadApp(theURL) 
			{
				var sizeX = screen.availWidth-12;
				var sizeY = screen.availHeight-37;
				var options = 'directories=no,location=no,menubar=no,status=no,toolbar=no,scrollbars=no,resizable=yes,left=0,top=0,width=' + sizeX + ',height=' + sizeY;
				newWin = window.open(theURL, '', options);
			}
			function ShowApp()
			{
				if(newWin != undefined && newWin.closed ==false)
					newWin.focus();
			}
			
			function PopUpTestWindow()
			{
			    if(newWin != undefined && newWin.closed ==false)
			    {
			        ShowApp();
			        return;
			    }
			    if (document.all)
                    var version=/MSIE \d+.\d+/;
			    //alert(navigator.appVersion);

			    if (navigator.appVersion.indexOf("MSIE 7") != -1 || navigator.appVersion.indexOf("MSIE 8") != -1) {
			        var sizeX = screen.availWidth-12;
				    var sizeY = screen.availHeight-37;
				    var options = 'directories=no,location=no,menubar=no,status=no,toolbar=no,scrollbars=no,resizable=yes,left=4000,top=4000,width=' + sizeX + ',height=' + sizeY;
				    newWin2 = window.open("about:blank", '', options);

				    if(newWin2 != undefined)
				    {
                        //Security setting "Allow script initiated windows with size or position constraints" is disabled
					    if (newWin2.screenLeft < 4000)
					    {
    				        newWin2.close();
    				        //alert("WARNING! Security settings not set correctly!");
    				        location.replace("../../../KeyCentralABZ/UI/Security/IE7.aspx");
    				    }
    				    else
    				    {
    				        //All's fine
    				        newWin2.close();
    				        SetFocusToUserName();
    				    }
				    }
				    else
				        alert("Pop-up blocker detected.");
				}
			}
		</script>
		<link rel="stylesheet" type="text/css" href="../../../StyleSheet1.css"/>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<meta http-equiv="CACHE-CONTROL" content="NO-CACHE"/>
		<meta http-equiv="PRAGMA" content="NO-CACHE"/>
	</head>
	<body onload="SetFocusToUserName();PopUpTestWindow();">
		<form id="Form1" method="post" runat="server">
			<table bgcolor="#2917A8" width="100%" cellpadding="0" cellspacing="0">
				<tr>
                    <td bgcolor="#CCFFFF">
						<table cellpadding="0" height="40" cellspacing="0">
							<tr>
                                <td height="40">&nbsp;&nbsp;&nbsp;<img src="../../../KeyCentralABZ/UI/Images/BorzynskiLogo32.png"/>&nbsp;&nbsp;&nbsp;<img src="../../../KeyCentralABZ/UI/Images/Key Central Logo ( 100x20 pix).png" />&nbsp;&nbsp;&nbsp;</td>
								<%--<td class="BaseHeaderLogo" valign="top" height="40" width="17" colspan="2" rowspan="1"><font size="5">R</font></td>
								<td class="BaseHeaderLogo" width="13" height="40"><font size="5">F</font></td>
								<td class="BaseHeaderLogo" valign="bottom" width="3" height="40"><font size="5">B</font></td>
								<td class="BaseHeaderLogoB" height="40"><font color="white" size="5">II Custom 
										Solutions </font>
								</td>
								<td width="13"></td>
								<td class="BaseHeaderLogo" height="40"><font size="5">- Key Central</font><font size="4"><sup> &reg;</sup></font></td>--%>
								<td width="300" align="right"><font color="#2917A8"><%= KeyCentral.Functions.Misc.GetVersionString(this) %></></font></td>
							</tr>
							<tr>
								<td height="1" width="17"></td>
							</tr>
							<tr height="1">
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table>
				<tr>
					<td width="760">
						<table align="center">
							<tr>
								<td height="100" style="WIDTH: 124px"></td>
							</tr>
							<tr>
								<td style="WIDTH: 124px">
									<p align="right"><font class="label">User Name</font></p>
								</td>
								<td><asp:textbox id="userName" runat="server" CssClass="text" EnterAsTab=""></asp:textbox></td>
							</tr>
							<tr>
								<td style="WIDTH: 124px">
									<p align="right"><font class="label">Password</font></p>
								</td>
								<td><asp:textbox id="password" runat="server" CssClass="text" TextMode="Password" EnterAsTab=""></asp:textbox></td>
							</tr>
							<tr>
								<td style="WIDTH: 124px; HEIGHT: 8px">
									<p align="right"><font class="label">Company</font></p>
								</td>
								<td style="HEIGHT: 8px"><asp:DropDownList id="DropDownList1" runat="server" Width="282px" DataTextField="company_Name" DataMember="report_Number"></asp:DropDownList></td>
							</tr>
							<tr>
								<td style="WIDTH: 124px"></td>
								<td>
									<asp:Button id="btnLogin" runat="server" Text="Login" Font-Names="Tahoma"></asp:Button></td>
							</tr>
							<!--<tr>
								<td style="WIDTH: 124px"></td>
								<td>
									<input type="button" onclick="PopUpTestWindow();" value="Test" />
								</td>
							</tr>-->
						</table>
						<asp:label id="Message" runat="server" ForeColor="Red"></asp:label></td>
				</tr>
			</table>
			<br/>
			&nbsp;
		</form>
	</body>
</html>
