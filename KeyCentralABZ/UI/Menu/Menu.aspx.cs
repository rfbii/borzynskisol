using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using KeyCentral.Functions;
using KeyCentral.Lookups;

namespace KeyCentralABZ.UI.Menu
{
    public partial class Menu : BasePage
    {
        #region Vars
        private int LinkCount = 30;
        //private int MenuOptionCount = 0;
        private string currentMenu;
        //private SortedList favoritesStl;
        private SortedList MenusStl;
        /*private string MenuStr
        {
            get { return Request["DisplayMenu"]; }
        }*/
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["LoginPage"] == "true")
            {
                //Count how many menu options this user has
                CountMenuOptions("");
                //If the user has less that 10 menu option, 
                //  remove current favorites and add all Menu options as favorites
                //If more than 10 menu options
                //  make sure they still have access to each favorite &
                //  make sure the favorites are still available
                //      if not remove favorite from SQL
                //if (MenusStl.Count > 0 && MenusStl.Count <= 10)
                //{
                //    RemoveFavorites();
                //    AddFavorites("");
                //}
                //else
                //{
                //    CheckFavorites();
                //}
            }
            if (ViewState["CurrentMenu"] == null)
            {
                string menuToShow = "";
                if (Request["DisplayMenu"] != null)
                    menuToShow = Request["DisplayMenu"];

                ViewState["CurrentMenu"] = menuToShow;
                currentMenu = menuToShow;
                DisplayMenu(menuToShow);
            }
            else
            {
                currentMenu = (string)ViewState["CurrentMenu"];
                IsAjaxPostBack = true;
            }

            //this.Repeater1.DataSource = GetFavoritesArl();

            //if (((ArrayList)this.Repeater1.DataSource).Count != 0)
            //{
            //    Repeater1.DataBind();
            //}
        }

        #region Private functions
        private void DisplayMenu(string menuStr)
        {
            ArrayList menus = menuMap.GetMenus("Menu", menuStr);

            for (int x = 0; x < LinkCount; x++)
            {
                HyperLink hyperLink = (HyperLink)this.FindControl("HyperLink" + (x + 1).ToString());

                //Clear everything first
                hyperLink.ToolTip = "";
                hyperLink.Text = " ";

                #region Show back to... button
                if (x == menus.Count)
                {
                    string prevMenuDesc = GetPrevMenuDesc();

                    if (prevMenuDesc == "Logoff")
                    {
                        hyperLink.Text = "Return to Login";
                        hyperLink.ToolTip = "Return to Login";
                        //btn.OnClientClick = "document.location='../../UI/Security/Logout.aspx';";
                        hyperLink.NavigateUrl = "javascript: " + "location.href='../../UI/Security/Logout.aspx';";
                    }
                    else if (prevMenuDesc.Trim() == "")
                    {
                        hyperLink.Text = "Return to Main menu";
                        hyperLink.ToolTip = GetPrevMenu();
                        hyperLink.NavigateUrl = "Menu.aspx?DisplayMenu=" + GetPrevMenu();
                    }
                    else
                    {
                        hyperLink.Text = "Return to " + prevMenuDesc;
                        hyperLink.ToolTip = GetPrevMenu();
                        hyperLink.NavigateUrl = "Menu.aspx?DisplayMenu=" + GetPrevMenu();
                    }
                    hyperLink.Visible = true;
                    continue;
                }  //Hide extra buttons
                else if (x > menus.Count)
                {
                    hyperLink.Visible = false;
                    continue;
                }
                #endregion

                MenuStruct menu = (MenuStruct)menus[x];

                //set nextMenu
                if (menu.JavaCode != null && menu.JavaCode != "")
                {
                    hyperLink.NavigateUrl = "javascript: " + menu.JavaCode;
                    hyperLink.ToolTip = " ";
                }
                else if (menu.Url != null)
                {
                    int startOfDisplayMenu = menu.Url.ToLower().IndexOf("menu.aspx?displaymenu=");
                    int startOfQuerryString = menu.Url.ToLower().IndexOf("?");
                    bool isMenu = true; ;

                    if (startOfDisplayMenu < 0)//displaymenu= not foubnd this can't be a menu
                        isMenu = false;

                    if (startOfQuerryString >= 0 && startOfQuerryString < startOfDisplayMenu)
                        isMenu = false;

                    if (isMenu)//It's a menu
                    {
                        hyperLink.NavigateUrl = menu.Url;
                        hyperLink.ToolTip = GetMenuPart(menu.Url);
                    }
                    else//It must be a page
                    {
                        hyperLink.NavigateUrl = menu.Url;
                        hyperLink.ToolTip = " ";
                    }
                }

                hyperLink.Text = menu.Description;
                hyperLink.Visible = true;
            }
        }

        private string GetPrevMenu()
        {
            if (ViewState["CurrentMenu"] == null || (string)ViewState["CurrentMenu"] == String.Empty || (string)ViewState["CurrentMenu"] == "/")
                return "";

            string menuStr = (string)ViewState["CurrentMenu"];
            return menuStr.Substring(0, menuStr.LastIndexOf("/"));
        }

        private string GetPrevMenuDesc()
        {
            //if (ViewState["CurrentMenu"] == null || (string)ViewState["CurrentMenu"] == String.Empty || (string)ViewState["CurrentMenu"] == "/")
            if (currentMenu == string.Empty || currentMenu == "" || currentMenu == "/")
                return "Logoff";

            string menuStr = (string)ViewState["CurrentMenu"];
            if (!menuStr.Contains("/"))
                return "";
            menuStr = menuStr.Substring(0, menuStr.LastIndexOf("/"));
            return menuStr.Substring(menuStr.LastIndexOf("/") + 1);
        }
        private int GetMenuDepth(string menu)
        {
            if (menu == null || menu.Trim() == string.Empty || menu == "/")
                return 0;

            string[] split = menu.Split('/');
            return split.Length;

        }

        private string GetLastMenu()
        {
            if (ViewState["LastMenu"] == null)
                return "";

            else return (string)ViewState["LastMenu"];

        }
        private string GetMenuPart(string url)
        {

            int start = url.ToLower().IndexOf("displaymenu=") + 12;
            int length = url.Length - start;
            return url.Substring(start, length);
        }
        private void MoveToMenu(string newMenu)
        {

            if (newMenu == "Return to Login")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirectMe", "location.href='../../UI/Security/Logout.aspx';", true);
                //Response.Redirect("../../UI/Security/Logout.aspx");
                return;
            }

            if (newMenu == " ")
                return;

            ViewState["LastMenu"] = ViewState["CurrentMenu"];
            ViewState["CurrentMenu"] = newMenu;
            currentMenu = newMenu;
            DisplayMenu(newMenu);
        }
        #endregion

        #region Button Clicks
        /*protected void Button1_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button1.ToolTip);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button2.ToolTip);
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button3.ToolTip);
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button4.ToolTip);
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button5.ToolTip);
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button6.ToolTip);
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button7.ToolTip);
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button8.ToolTip);
        }

        protected void Button9_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button9.ToolTip);
        }

        protected void Button10_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button10.ToolTip);
        }

        protected void Button11_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button11.ToolTip);

        }

        protected void Button12_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button12.ToolTip);

        }

        protected void Button13_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button13.ToolTip);

        }

        protected void Button14_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button14.ToolTip);

        }

        protected void Button15_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button15.ToolTip);

        }

        protected void Button16_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button16.ToolTip);

        }

        protected void Button17_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button17.ToolTip);

        }

        protected void Button18_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button18.ToolTip);

        }

        protected void Button19_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button19.ToolTip);

        }

        protected void Button20_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button20.ToolTip);

        }

        protected void Button21_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button21.ToolTip);

        }

        protected void Button22_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button22.ToolTip);

        }

        protected void Button23_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button23.ToolTip);

        }

        protected void Button24_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button24.ToolTip);

        }

        protected void Button25_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button25.ToolTip);

        }

        protected void Button26_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button26.ToolTip);

        }

        protected void Button27_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button27.ToolTip);

        }

        protected void Button28_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button28.ToolTip);

        }

        protected void Button29_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button29.ToolTip);

        }

        protected void Button30_Click(object sender, EventArgs e)
        {
            MoveToMenu(Button30.ToolTip);

        }*/
        #endregion

//        protected void SetupFavoritesBtn_Click(object sender, System.EventArgs e)
//        {
//            Response.Redirect("../../../KeyCentralABZ/UI/Favorites/InputSetupFavorites.aspx");
//        }
//        public ArrayList GetFavoritesArl()
//        {
//            ArrayList ret = new ArrayList();

//            SqlConnection Conn = GetSQLConn();
//            SqlCommand selectCMD = new SqlCommand(@"
//			Select
//				Report_Name,
//				Report_URL
//			From
//				Favorites
//			Where
//				User_Key=@User_Key
//			Order By
//				Report_Name", Conn);
//            selectCMD.Parameters.AddWithValue("@User_Key", Session["User_Key"].ToString());

//            SqlDataReader myReader = selectCMD.ExecuteReader();

//            while (myReader.Read())
//            {
//                ret.Add(new CriteriaBoxRecord(myReader.GetValue(1).ToString(), myReader.GetValue(0).ToString(), ""));
//            }

//            myReader.Close();
//            myReader.Dispose();
//            Misc.CleanUpSQL(Conn);

//            return ret;
//        }
//        public SortedList GetFavoritesStl()
//        {
//            SortedList ret = new SortedList();

//            SqlConnection Conn = GetSQLConn();
//            SqlCommand selectCMD = new SqlCommand(@"
//			Select
//				Report_Name,
//				Report_URL
//			From
//				Favorites
//			Where
//				User_Key=@User_Key
//			Order By
//				Report_Name", Conn);
//            selectCMD.Parameters.AddWithValue("@User_Key", Session["User_Key"].ToString());

//            SqlDataReader myReader = selectCMD.ExecuteReader();

//            while (myReader.Read())
//            {
//                ret.Add(myReader.GetValue(0).ToString(), myReader.GetValue(1).ToString());
//            }

//            myReader.Close();
//            myReader.Dispose();
//            Misc.CleanUpSQL(Conn);

//            return ret;
//        }
        //public string Detail(object oItem)
        //{
        //    string detail = "";

        //    CriteriaBoxRecord favorite = (CriteriaBoxRecord)((RepeaterItem)oItem).DataItem;

        //    detail += @"<a href=" + "\"" + favorite.key + "\"" + ">" + favorite.text + "</a>";

        //    return detail;
        //}
        private void CountMenuOptions(string menuStr)
        {
            ArrayList menus = menuMap.GetMenus("Menu", menuStr);

            MenusStl = new SortedList();

            //For each top level menu
            for (int x = 0; x < menus.Count; x++)
            {
                MenuStruct menu = (MenuStruct)menus[x];

                CountBuildMenuSubNodes(menu, menu.Description);
            }
        }
        private void CountBuildMenuSubNodes(MenuStruct menu, string parentNodeDescription)
        {
            foreach (MenuStruct subMenu in menu.SubMenus)
            {
                if (subMenu.SubMenus.Count == 0)
                {
                    string url = "";

                    if (subMenu.Url != null && subMenu.Url != "")
                    {
                        url = subMenu.Url;
                    }
                    else
                    {
                        url = "javascript: " + subMenu.JavaCode;
                    }

                    //MenuOptionCount = MenuOptionCount + 1;

                    MenusStl.Add(parentNodeDescription + "~" + subMenu.Description, url);
                }
                //Call recureivly to build all sub-sub menus
                CountBuildMenuSubNodes(subMenu, parentNodeDescription + "~" + subMenu.Description);
            }
        }
        //protected void RemoveFavorites()
        //{
        //    SqlConnection Conn = GetSQLConn();
        //    SqlCommand removeCMD = new SqlCommand("Delete from [Favorites] where User_Key=@User_Key", Conn);
        //    removeCMD.Parameters.Add("@User_Key", Session["User_Key"].ToString());
        //    removeCMD.ExecuteNonQuery();
        //    Misc.CleanUpSQL(Conn);
        //}
        //private void AddFavorites(string menuStr)
        //{
        //    ArrayList menus = menuMap.GetMenus("Menu", menuStr);

        //    //For each top level menu
        //    for (int x = 0; x < menus.Count; x++)
        //    {
        //        MenuStruct menu = (MenuStruct)menus[x];

        //        AddSubNodesFavorites(menu, menu.Description);
        //    }

        //}
        //private void AddSubNodesFavorites(MenuStruct menu, string parentNodeDescription)
        //{
        //    SqlConnection Conn = GetSQLConn();
        //    foreach (MenuStruct subMenu in menu.SubMenus)
        //    {
        //        if (subMenu.SubMenus.Count == 0)
        //        {
        //            string url = "";

        //            if (subMenu.Url != null && subMenu.Url != "")
        //            {
        //                url = subMenu.Url;
        //            }
        //            else
        //            {
        //                url = "javascript: " + subMenu.JavaCode;
        //            }

        //            SqlCommand insertCMD = new SqlCommand("Insert into [Favorites] (User_Key, Report_Name, Report_URL) values(@User_Key, @Report_Name, @Report_URL)", Conn);
        //            insertCMD.Parameters.AddWithValue("@User_Key", Session["User_Key"].ToString());
        //            insertCMD.Parameters.AddWithValue("@Report_Name", subMenu.Description);
        //            insertCMD.Parameters.AddWithValue("@Report_URL", url);

        //            insertCMD.ExecuteNonQuery();
        //        }

        //        //Call recureivly to build all sub-sub menus
        //        AddSubNodesFavorites(subMenu, parentNodeDescription + "~" + subMenu.Description);
        //    }
        //    Misc.CleanUpSQL(Conn);

        //}
        //private void CheckFavorites()
        //{
        //    favoritesStl = GetFavoritesStl();

        //    SqlConnection Conn = GetSQLConn();

        //    //For each top level menu
        //    for (int x = 0; x < favoritesStl.Count; x++)
        //    {
        //        if (!MenusStl.ContainsValue(favoritesStl.GetByIndex(x).ToString()))
        //        {
        //            SqlCommand removeCMD = new SqlCommand("Delete from [Favorites] where User_Key=@User_Key and Report_Name=@Report_Name", Conn);
        //            removeCMD.Parameters.Add("@User_Key", Session["User_Key"].ToString());
        //            removeCMD.Parameters.Add("@Report_Name", favoritesStl.GetKey(x).ToString());
        //            removeCMD.ExecuteNonQuery();
        //        }
        //    }

        //    Misc.CleanUpSQL(Conn);
        //}
    }
}
