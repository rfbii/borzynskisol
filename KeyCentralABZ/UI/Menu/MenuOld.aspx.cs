//using System;
//using System.Collections;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
//using System.Text;
//using System.Web;
//using System.Web.SessionState;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.HtmlControls;
//using KeyCentral.Functions;

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using KeyCentral.Functions;
using KeyCentral.Lookups;

namespace KeyCentral.UI.Menu
{
    /// <summary>
    /// Summary description for Menu.
    /// </summary>
    public partial class Menu : BasePage
    {
        protected System.Web.UI.WebControls.Panel Panel1;


        private string DisplayMenu
        {
            get { return Request["DisplayMenu"]; }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            //menuMap = new MenuMap(Page);

            ////ArrayList menuMapArrayList = menuMap.GetMenus("Menu", DisplayMenu);
            //ArrayList menuMapArrayList = menuMap.GetMenus("Menu", null);

            //BuildBaseMenu(MainMenu, menuMapArrayList);

            //MainMenu.Visible = true;
            //MainMenu.MaximumDynamicDisplayLevels = 20;
            //MainMenu.Orientation = Orientation.Horizontal;
            //MainMenu.DynamicEnableDefaultPopOutImage = false;
            //MainMenu.StaticEnableDefaultPopOutImage = false;
            //MainMenu.StaticMenuItemStyle.CssClass = "menuitem";
            //MainMenu.DynamicHoverStyle.CssClass = "mouseoverdynamic";
            //MainMenu.DynamicMenuStyle.CssClass = "menustyledynamic";
            //MainMenu.DynamicMenuItemStyle.CssClass = "menuitemdynamic";
            //MainMenu.StaticHoverStyle.CssClass = "mouseover";
            //MainMenu.StaticMenuStyle.CssClass = "menustyle";

            // Put user code to initialize the page here
            this.Repeater1.DataSource = menuMap.GetMenus("Menu", DisplayMenu);
            Repeater1.DataBind();
        }

        /// <summary>
        /// Builds the Display string to show this menu as a button
        /// </summary>
        /// <param name="menu"></param>
        /// <returns>string of the HTML code to display the button for thie menu</returns>
        public string DisplayButton(KeyCentral.Functions.MenuStruct menu)
        {
            //Target output
            //<INPUT type="button" value="Button" class="button" onclick="javascript:document.location='http://www.yahoo.com';">

            StringBuilder ret = new StringBuilder();

            string temp = menu.Description;

            //Don't display "Menu" button on the main menu
            if ((DisplayMenu == null || DisplayMenu != null || DisplayMenu.IndexOf("/") == -1) && menu.Description != "Menu")
            {
                ret.Append("<tr><td align=center>");
                ret.Append("<input type='Button' class='button' value='");
                ret.Append(menu.Description);
                if (menu.Url != null && menu.Url != "")
                {
                    ret.Append("' onclick=\"javascript:document.location='");
                    ret.Append(menu.Url);
                    ret.Append("';\">");
                }
                else
                {
                    ret.Append("' onclick=\"javascript:");
                    ret.Append(menu.JavaCode);
                    ret.Append("\">");
                }
                ret.Append("<br><br></td></tr>");
            }

            return ret.ToString();
        }

        /// <summary>
        /// Builds the Display string to show this menu as a button
        /// </summary>
        /// <param name="menu"></param>
        /// <returns>string of the HTML code to display the button for thie menu</returns>
        public string DisplayBackButton()
        {
            //Target output
            //<INPUT type="button" value="Button" class="button" onclick="javascript:document.location='http://www.yahoo.com';">
            StringBuilder ret = new StringBuilder();
            string menuDesc;
            string returnString = String.Empty;
            //Return to {Secuirty} menu
            //Add log off button

            if (DisplayMenu == null || DisplayMenu.IndexOf("/") == -1)
                return DisplayLogoffButton();
            else
                menuDesc = DisplayMenu.Substring(0, DisplayMenu.LastIndexOf("/"));

            string[] split = menuDesc.Split('/');

            if (split.Length <= 1)//main menu
                returnString = "Main";
            else
                returnString = split[split.Length - 1];

            ret.Append("<input type='Button' class='button' value='Return to ");
            ret.Append(returnString);

            ret.Append(" Menu' onclick=\"javascript:document.location='Menu.aspx?DisplayMenu=");
            ret.Append(menuDesc);
            ret.Append("';\">");

            return ret.ToString();
        }
        private string DisplayLogoffButton()
        {
            StringBuilder ret = new StringBuilder();

            ret.Append("<input type='Button' class='button' value='Return to Login");
            ret.Append("' onclick=\"javascript:document.location='../../../KeyCentralABZ/UI/Security/Logout.aspx';\">");



            return ret.ToString();

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }

        private void BuildBaseMenu(System.Web.UI.WebControls.Menu menu, ArrayList mainMenuArrayList)
        {
            for (int i = 0; i < mainMenuArrayList.Count; i++)
            {
                MenuItem newMenuItem = new MenuItem();
                newMenuItem.Text = ((MenuStruct)mainMenuArrayList[i]).Description;
                newMenuItem.NavigateUrl = ((MenuStruct)mainMenuArrayList[i]).Url;
                newMenuItem.Value = ((MenuStruct)mainMenuArrayList[i]).Security;

                newMenuItem = AddSubMenus(newMenuItem, ((MenuStruct)mainMenuArrayList[i]).SubMenus);

                menu.Items.Add(newMenuItem);
            }
        }

        private MenuItem AddSubMenus(MenuItem menuItem, ArrayList mainMenu)
        {
            for (int i = 0; i < mainMenu.Count; i++)
            {
                MenuItem newMenuItem = new MenuItem();
                newMenuItem.Text = ((MenuStruct)mainMenu[i]).Description;
                newMenuItem.NavigateUrl = ((MenuStruct)mainMenu[i]).Url;
                //newMenuItem.NavigateUrl = "javascript:document.location='http://www.yahoo.com/'";
                //newMenuItem.NavigateUrl = "javascript: BaseRFB_HideHeader(false); PrintThisPage(); BaseRFB_HideHeader(true);";
                newMenuItem.Value = ((MenuStruct)mainMenu[i]).Security;

                newMenuItem = AddSubMenus(newMenuItem, ((MenuStruct)mainMenu[i]).SubMenus);

                menuItem.ChildItems.Add(newMenuItem);
            }

            return menuItem;
        }
        #endregion
    }
}