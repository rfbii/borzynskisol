<%@ Page language="c#" Inherits="KeyCentralABZ.UI.Menu.CriteriaInstallPrint" Codebehind="CriteriaInstallPrint.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Year Start Date</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
		</style>-->
        <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body onload="SetupPrinting();"><object id="factory" style="display:none"
  classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814"
  codebase="../../../KeyCentralABZ/UI/Include/smsx.cab#Version=6,5,439,01">
</object>
<script language="javascript">
    function SetupPrinting() {
        if (factory.printing == undefined) {
            alert('Warning: The software needed to print has not been installed on your computer.');
            return;
        }
        else
        {
        alert('The software needed to print is installed on your computer.');
        window.location.replace('../../../KeyCentralABZ/UI/Menu/Menu.aspx');
        }

        factory.printing.footer = "Printed: &D &t&bPage &p of &P&bType Security Report";
        factory.printing.header = "";
        factory.printing.leftMargin = .5;
        factory.printing.rightMargin = .5;
        factory.printing.topMargin = .5;
        factory.printing.bottomMargin = .5;
    }  
        </script>
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
			 <table width='600'>
			    <tr>
			        <td>The software needed to print has not been installed on your computer.'
			        You need to install software to be able and print from KeyCentral. 
			        </td>
			    </tr>
			    <tr>
			    <td>
			    </td>
			    </tr>
			    <tr>
			        <td>There should be white bar at the top of your screen click on that bar and install the software. 
			        </td>
			    </tr>
			    <tr>
			        <td>If you do not see white bar then you need to have administrator rights so you can install the software.
			       </td>
			    </tr>
			 </table>
				<asp:button id="Return" style="Z-INDEX: 103; LEFT: 338px; POSITION: absolute; TOP: 216px" tabIndex="3"
					runat="server" EnterAsTab="" Width="160px" Height="24px" Text="Return to Menu" onclick="Return_Click"></asp:button>
			</div>
			<!-- #include file=../../../KeyCentralABZ/UI/Include/ScreenFooter.html"--></form>
	</body>
</html>
