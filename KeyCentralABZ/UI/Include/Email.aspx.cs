using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace KeyCentral.UI.Include
{
	/// <summary>
	/// Summary description for Email.
	/// </summary>
	public partial class Email : KeyCentral.Functions.BasePage
	{
		protected System.Web.UI.WebControls.TextBox TextBox1;
		protected System.Web.UI.WebControls.TextBox Textbox2;
		protected System.Web.UI.HtmlControls.HtmlInputHidden Test;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void send_Click(object sender, System.EventArgs e)
		{
			KeyCentral.Functions.JavaStatusMessage status = new KeyCentral.Functions.JavaStatusMessage(this);
			status.ShowMessage("Sending E-mail");
			
			//Build the to Address.
			string toAddress = to.Text;
			if(this.copyMyself.Checked)
				toAddress += ";" + this.from.Text;

			KeyCentral.Functions.Email.SendEMail( this.from.Text,toAddress,this.subject.Text,"The report is attached.",Request["attachment"] + ".html",FixOutput(Request["EmailForm"]),this);
			//Request["EmailForm"] = "";
			Response.Clear();
			
			this.RegisterStartupScript("Close","<script language=\"javascript\">window.close();</script>");
		}
		
		protected void cancel_Click(object sender, System.EventArgs e)
		{
			this.RegisterStartupScript("Close","<script language=\"javascript\">window.close();</script>");
		}
		private string FixOutput(string str)
		{
			//str  = FixScriptX(str);
			str = CleanExtraStuff(str);
			return str;
		}
		private string CleanExtraStuff(string str)
		{
			string temp = str.ToLower();
			string cleanStr="";
			int emailStart; 
			int emailEnd;
			int lastEnd=0;


			emailStart = temp.IndexOf("<!-- email start -->",0);
			emailEnd = temp.IndexOf("<!-- email end -->",0);
			while(emailStart!=-1 && emailEnd !=-1)
			{
				cleanStr += str.Substring(emailStart+20,(emailEnd-emailStart)-20);
				//temp = str.Remove(emailStart,(emailEnd-emailStart)+17).ToLower();

				lastEnd = emailEnd+1;
				emailStart = temp.IndexOf("<!-- email start -->",lastEnd);
				emailEnd = temp.IndexOf("<!-- email end -->",lastEnd);
			}



			return "<HTML><Body>" + cleanStr + @"</Body></HTML>";
		}
		private string FixScriptX(string str)
		{
			string temp = str.ToLower();
			int start = temp.IndexOf("<object id=factory",0);
			int count;

			if(start==-1)
				return str;
			
			count= temp.IndexOf("</object>",start)-start;

			return str.Remove(start,count).Insert(start,"<OBJECT id=\"factory\" style=\"DISPLAY: none\" codeBase=\"http://www.meadroid.com/scriptx/ScriptX.cab#Version=6,1,431,2\" classid=\"clsid:1663ed61-23eb-11d2-b92f-008048fdd814\" VIEWASTEXT></OBJECT>");
		}

	}
}
