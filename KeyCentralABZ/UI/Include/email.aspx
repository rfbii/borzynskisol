<%@ Page language="c#" validateRequest="false" Inherits="KeyCentral.UI.Include.Email" Codebehind="Email.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
		<title>Email</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1"/>
		<meta name="CODE_LANGUAGE" content="C#"/>
		<meta name="vs_defaultClientScript" content="JavaScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<base target="_self"/>
  </head>
	<body onload="LoadValues();">

		<script type="text/javascript" language="javascript">
		//window.onbeforeunload = BeforeUnload;
		function BeforeUnload()
		{
			mess = "You will lose all information for this E-mail\nand need to run the report again.";
			return mess;
		}
		function LoadValues()
		{
			//document.getElementById("EmailForm").value =  self.opener.document.documentElement.outerHTML;
			document.getElementById("EmailForm").value = window.dialogArguments.document.documentElement.outerHTML;
			
		}
		</script>
		<form id="Form1" method="post" runat="server">
			<input id="EmailForm" type="hidden" name="EmailForm" />
			<table>
				<tr>
					<td>From:</td>
					<td><asp:TextBox id="from" runat="server"></asp:TextBox></td>
				</tr>
				<tr>
					<td>To:</td>
					<td><asp:TextBox id="to" runat="server"></asp:TextBox></td>
				</tr>
				<tr>
					<td>Subject:</td>
					<td><asp:TextBox id="subject" runat="server"></asp:TextBox></td>
				</tr>
				<tr>
					<td>Attachment Name:</td>
					<td><asp:TextBox id="attachment" runat="server">Famous Report</asp:TextBox></td>
				</tr>
				<tr>
					<td>Copy Myself:</td>
					<td><asp:CheckBox id="copyMyself" runat="server"></asp:CheckBox></td>
				</tr>
				<tr>
					<td></td>
					<td align="center"><asp:Button id="send" runat="server" Text="Send" onclick="send_Click"></asp:Button>&nbsp;&nbsp;&nbsp;<asp:Button id="cancel" runat="server" Text="Cancel" onclick="cancel_Click"></asp:Button></td>
				</tr>
			</table>
		</form>
	</body>
</html>
