

$(document).ready(function () {
    // Fix up GridView to support THEAD tags            
    $("#LotDataGrid tbody").before("<thead></thead>");
    $("#LotDataGrid thead ").append($("#LotDataGrid tbody tr:first"));
    //$("#LotDataGrid tbody tr:first").remove();

    $("#LotDataGrid thead tr").find('td').each(function () {
        $(this).replaceWith($('<th>').html($(this).html()))

    });

    $("#LotDataGrid").fixedHeader({
        height: 670
    });

    if ($("#LotDataGrid_header").find("thead").length == 2) {
        var i = 0;
        $("#LotDataGrid_header").find('thead').each(function () {

            i = i + 1;
            if (i == 2) {
                $(this).remove();
            }
        });
    }

});


/*********************************  Printing Functions  ************************************/
var LastBodyDivClass = 'scroll';
var boxType, leftClickLookup;
var singleLookup = "false";
var LookupSearchParam0 = "";
var LookupSearchParam1 = "";
function BaseRFB_HideHeader(display) {
    var HeaderDiv = document.getElementById('BaseRFBHeaderDiv');
    var BodyDiv = document.getElementById('BodyDiv');

    if (HeaderDiv != null && HeaderDiv.style != null) {
        if (display == true)
            HeaderDiv.style.display = 'inline';
        else
            HeaderDiv.style.display = 'none';
    }
    else
        alert('HeaderDiv not found');

    if (BodyDiv != null && BodyDiv.style != null) {
        if (display == true)
            BodyDiv.className = LastBodyDivClass;
        else {
            LastBodyDivClass = BodyDiv.className;
            BodyDiv.className = '';
        }
    }
}

function PrintThisPage() {
    HideButtons();

    if (factory.printing == undefined) {
        alert('Warning: The software needed to print has not been installed on your computer.');
    }
    else {
        factory.printing.Print();
    }

    ShowButtons();
}
function HideButtons() {
    var ButtonsDiv = document.getElementById('ButtonsDiv');

    if (ButtonsDiv != undefined && ButtonsDiv != null && ButtonsDiv.style != null) {
        document.all.ButtonsDiv.style.visibility = 'hidden';
    }
}

function ShowButtons() {
    var ButtonsDiv = document.getElementById('ButtonsDiv');

    if (ButtonsDiv != undefined && ButtonsDiv != null && ButtonsDiv.style != null) {
        document.all.ButtonsDiv.style.visibility = '';
    }
}
function PrintThisPageLandscape() {
    var oldPortrait = factory.printing.portrait;
    BaseRFB_HideHeader(false);
    factory.printing.portrait = false;
    factory.printing.Print();
    factory.printing.portrait = oldPortrait;
    BaseRFB_HideHeader(true);
}

var PopPrintScreenMessageFunc = null;
var newPrintWin = null;
function PopPrintScreen(theURL) {
    var options = 'directories=no,location=no,menubar=no,status=no,toolbar=no,scrollbars=no,height=600,width=1000,left=2000,top=100';
    newPrintWin = window.open(theURL, '', options);
    ShowStatusMessage('Preparing report for printing.');
    PopPrintScreenMessageFunc = window.setInterval('PopPrintScreenStopMessage()', 100);

}
function PopPrintScreenStopMessage() {
    if (newPrintWin != undefined && newPrintWin != null && newPrintWin.closed == true) {
        window.clearInterval(PopPrintScreenMessageFunc);
        PopPrintScreenMessageFunc = null;

        HideStatusMessage();
        newPrintWin = null;
    }

}
/*********************************  End Hid Header Functions *********************************/

/**********************************  E-mail Functions **********************************/
function EmailThisPage() {
    var myRand = rand(50000);
    window.showModalDialog("../../../KeyCentralABZ/UI/Include/Email.aspx?rand=" + myRand, window, "dialogHeight: 240px; dialogWidth: 300px; dialogTop: px; dialogLeft: px; edge: Raised; center: Yes; help: No; resizable: No; status: No;");
    //Email = window.open("../../../BaseRFB/BaseRFB/Include/Email.aspx");
}
function rnd() {
    today = new Date();
    jran = today.getTime();

    ia = 9301;
    ic = 49297;
    im = 233280;
    jran = (jran * ia + ic) % im;
    return jran / (im * 1.0);
}

function rand(number) {
    return Math.ceil(rnd() * number);
}
var ExcelWindow = null
function ExcelThisPage() {

    if (ExcelWindow != null) {
        ExcelWindow.close();
        ExcelWindow = null;
    }
    var sizeX = 300;
    var sizeY = 150;
    var theURL = "../../../KeyCentralABZ/UI/Include/Excel.aspx";
    var options = 'directories=no,location=no,menubar=no,status=no,toolbar=no,scrollbars=no,resizable=yes,left=0,top=0,width=' + sizeX + ',height=' + sizeY;
    ExcelWindow = window.open(theURL, '', options);

    //window.showModelessDialog("../../../BaseRFB/BaseRFB/Include/Excel.aspx",window,"dialogHeight: 150px; dialogWidth: 300px; dialogTop: px; dialogLeft: px; edge: Raised; center: Yes; help: No; resizable: No; status: No;");		
    //window.open(,"dialogHeight: 150px; dialogWidth: 300px; dialogTop: px; dialogLeft: px; edge: Raised; center: Yes; help: No; resizable: No; status: No;");		
    //window.showModalDialog("../../../BaseRFB/BaseRFB/Include/Excel.aspx",window,"dialogHeight: 150px; dialogWidth: 300px; dialogTop: px; dialogLeft: px; edge: Raised; center: Yes; help: No; resizable: No; status: No;");		
}
/*********************************  End E-mail Functions *********************************/

/**********************************  Enter As Tab Functions **********************************/
var keyLog = "";
var keyLogOn = false
function OnKeyDown() {
    if (keyLogOn)
        keyLog = keyLog.concat(String.fromCharCode(event.keyCode));

    if (keyLog == "LOG") {
        var LogDiv = document.getElementById('LogDiv');
        if (LogDiv.style.left == "-1000px") {
            LogDiv.style.left = 350;
            LogDiv.style.top = 10;
        }
        else {
            LogDiv.style.left = -1000;
            LogDiv.style.top = -1000;
        }
        keyLogOn = !keyLogOn;
        keyLog = "";
    }
    if (keyLog == "TEST") {
        document.location = "../../../KeyCentralABZ/UI/Test/Test.aspx";
        keyLogOn = !keyLogOn;
        keyLog = "";
    }
    if (keyLog == "SCREEN800") {
        window.resizeTo(785, 600);
        keyLogOn = !keyLogOn;
        keyLog = "";
    }
    if (keyLog == "SHOWPRINT") {
        if (newPrintWin != undefined && newPrintWin != null && newPrintWin.closed == false) {
            newPrintWin.moveTo(0, 0);
            newPrintWin.setFocus();
        }
        else
            alert('Print screen can not be found');

        keyLogOn = !keyLogOn;
        keyLog = "";
    }

    if (event.keyCode == 192) {
        if (keyLogOn) {
            alert(keyLog)
            keyLog = "";
        }
        keyLogOn = !keyLogOn;
    }

    if (document.all[document.activeElement.sourceIndex].IgnoreEnter != undefined && event.keyCode == 13) {
        event.keyCode = 0;
        return false;
    }

    if (event.keyCode == 13 && document.all[document.activeElement.sourceIndex].TabFinish == undefined && document.all[document.activeElement.sourceIndex].EnterAsTab != undefined)
        SetFocusToNext(document.activeElement.sourceIndex);

}

function SetFocusToNext(iId) {
    var iMinTab = -1;
    var iMinId = -1;
    bFound = false;

    //Check for next highest with this tab
    for (x = iId + 1; x < document.all.length && iMinId == -1 ; x++) {
        if (document.all[x].EnterAsTab != undefined && document.all[x].disabled == false)
            if (document.all[x].EnterAsTab == document.all[iId].EnterAsTab)
                iMinId = x;
    }

    if (iMinId != -1)
        bFound = true;

    //Check for next highest tab
    for (x = 0; x < document.all.length && !bFound ; x++) {
        if (document.all[x].EnterAsTab != undefined && document.all[x].EnterAsTab > document.all[iId].EnterAsTab && document.all[x].disabled == false)
            if (document.all[x].EnterAsTab < iMinTab | iMinTab == -1) {
                iMinTab = document.all[x].EnterAsTab;
                iMinId = x;
            }
    }

    if (iMinId != -1)
        bFound = true;

    //Check for lowest tab with lowest id
    for (x = 0; x < document.all.length && !bFound ; x++) {
        if (document.all[x].EnterAsTab != undefined && document.all[x].disabled == false)
            if (document.all[x].EnterAsTab < iMinTab | iMinTab == -1) {
                iMinTab = document.all[x].EnterAsTab;
                iMinId = x;
            }
    }

    if (iMinId != -1)
        bFound = true;

    if (bFound) {
        event.returnValue = false;
        document.all[iMinId].focus();
    }
}
document.onkeydown = OnKeyDown;
/********************************* End Enter As Tab Functions ********************************/

/********************************** Context Menu  Functions **********************************/
/**************************  Posible Context Menus are as follows  ***********************
<control id='name' LookupSearch='type' ></control>
Ex: <asp:TextBox id="TextBox1" runat="server" LookupSearch="Lot"></asp:TextBox>

<control id='name' LookupSearch='typeSingle' SingleLookup></control>
Ex: <asp:TextBox id="TextBox1" runat="server" LookupSearch="CustomerSingle" SingleLookup></asp:TextBox>

<control id='name' LookupSearch='typeSingle' SingleLookup LeftClickLookup></control>
Ex: <asp:TextBox id="TextBox1" runat="server" LookupSearch="CustomerSingle" SingleLookup LeftClickLookup></asp:TextBox>


<control id='name' CalendarPopUp ></control>
Ex: <asp:TextBox id="TextBox1" runat="server" CalendarPopUp></asp:TextBox>


***********************************************************************************************/
var LookupSearchPage = "../../../KeyCentralABZ/UI/ContextMenu/LookupSearch.aspx";
var CurrentContextMenu = "";
var CalendarClass = null;
var ControlToUpdate;

document.onmousedown = OnMouseDown;
document.oncontextmenu = OnContextMenu;
document.onclick = OnClick;

function OnContextMenu() {
    //if(window.event.srcElement.ContextMenu!= undefined)
    if (GetContextMenu(window.event.srcElement) != "") {
        window.event.returnValue = false;
        window.event.cancelBubble = true;
    }
}
function GetContextMenu(control) {
    for (var i = 0; i <= control.attributes.length - 1; i++) {
        if (control.attributes[i].name == 'lookupsearch') {
            return "LookupSearch";
        }
        else if (control.attributes[i].name == 'calendarpopup') {
            return "CalendarPopUp";
        }
    }
    if (control.LookupSearch != undefined)
        return "LookupSearch";
    if (control.CalendarPopUp != undefined)
        return "CalendarPopUp";
    return "";
}

function ShowLookupSearch(startOn) {
    var val;
    ContextMenuDiv = document.getElementById("LookupSearchDiv");

    if (ControlToUpdate.SingleLookup != undefined)
        singleLookup = "true";

    if (ControlToUpdate.LookupSearchParam0 != undefined)
        LookupSearchParam0 = document.getElementById(ControlToUpdate.LookupSearchParam0).value;

    if (ControlToUpdate.LookupSearchParam1 != undefined)
        LookupSearchParam1 = document.getElementById(ControlToUpdate.LookupSearchParam1).value;

    if (ContextMenuDiv != undefined) {
        if (ContextMenuDiv.LookupType != "" && ContextMenuDiv.LookupType != undefined) {
            val = window.showModalDialog(LookupSearchPage + "?LookupType=" + ContextMenuDiv.LookupType + "&&StartOn=" + startOn + "&&SingleLookup=" + singleLookup + "&&LookupSearchParam0=" + LookupSearchParam0 + "&&LookupSearchParam1=" + LookupSearchParam1, "", "dialogHeight: 450px; dialogWidth: 780px; dialogTop: px; dialogLeft: px; edge: Raised; center: Yes; help: No; resizable: No; status: No;");
        }
        else {
            for (var i = 0; i <= ContextMenuDiv.attributes.length - 1; i++) {
                if (ContextMenuDiv.attributes[i].name == 'lookuptype') {
                    if (boxType != '') {
                        val = window.showModalDialog(LookupSearchPage + "?LookupType=" + boxType + "&&StartOn=" + startOn + "&&SingleLookup=" + singleLookup + "&&LookupSearchParam0=" + LookupSearchParam0 + "&&LookupSearchParam1=" + LookupSearchParam1, "", "dialogHeight: 450px; dialogWidth: 780px; dialogTop: px; dialogLeft: px; edge: Raised; center: Yes; help: No; resizable: No; status: No;");
                    }
                }
            }
        }
    }
    if (ControlToUpdate.SingleLookup != undefined && val != undefined) {
        if (val.indexOf("!") != -1) {
            if (document.getElementById(ControlToUpdate.SingleLookup) == undefined)
                alert('Lookup returned keys and text but no SingleLookup hidden field was given.');
            else {
                var temp = document.getElementById(ControlToUpdate.SingleLookup);
                temp.value = val.substring(0, val.indexOf("!"));
                ControlToUpdate.value = val.substring(val.indexOf("!") + 1, val.length);
            }
        }
        else {
            if (ControlToUpdate.SingleLookup != "" && document.getElementById(ControlToUpdate.SingleLookup) != undefined) {
                var temp = document.getElementById(ControlToUpdate.SingleLookup);
                temp.value = ""
            }
            ControlToUpdate.value = val;
        }
    }
    else if (singleLookup != undefined && val != undefined) {
        if (val.indexOf("!") != -1) {
            if (singleLookup == undefined)
                alert('Lookup returned keys and text but no SingleLookup hidden field was given.');
            else {
                var temp = singleLookup;
                temp.value = val.substring(0, val.indexOf("!"));
                ControlToUpdate.value = val.substring(val.indexOf("!") + 1, val.length);
            }
        }
        else {
            DoPostBack();
        }
        //else {
        //    if (singleLookup != "" && singleLookup != undefined) {
        //        var temp = singleLookup;
        //        temp.value = ""
        //    }
        //    ControlToUpdate.value = val;
        //}
    }
    else
        DoPostBack();
}

function ShowContextMenu(x, y, control) {
    ControlToUpdate = control;
    switch (GetContextMenu(control)) {
        case "LookupSearch":
            contextMenuDiv = document.getElementById("LookupSearchDiv");
            contextStyle = contextMenuDiv.style;
            contextStyle.left = x;
            contextStyle.top = y;
            contextMenuDiv.LookupType = control.LookupSearch;
            break;
        case "CalendarPopUp":
            ShowCalendar(x, y, control);

            break;
    }

    //Save witch ContextMenu is up
    CurrentContextMenu = GetContextMenu(control);
}
function HideContextMenu() {
    switch (CurrentContextMenu) {
        case "LookupSearch":
            contextMenuDiv = document.getElementById("LookupSearchDiv");
            contextStyle = contextMenuDiv.style;
            contextStyle.left = -1000;
            contextStyle.top = -1000;
            contextMenuDiv.LookupType = "";
            break;
    }
}

function ShowCalendar(x, y, control) {
    //Init if it hasn't already been done
    if (CalendarClass == null) {
        oDate = new Date();
        CalendarClass = new CalendarFrame.DatePicker("CalendarDiv", oDate.getFullYear() - 10, oDate.getFullYear() + 10, true);
    }
    CalendarClass.open(control);

    CalendarDiv.style.left = x;
    CalendarDiv.style.top = y;
}
function HideCalendar() {
    calendarDiv = document.getElementById("CalendarDiv");


    if (CalendarDiv != undefined) {
        calendarStyle = calendarDiv.style;
        calendarStyle.visibility = 'hidden';
    }
}
function DoPostBack() {
    var theform;

    if (window.navigator.appName.toLowerCase().indexOf("netscape") > -1)
        theform = document.forms[document.forms.item(0).name];
    else
        theform = eval("document." + document.forms.item(0).name);

    theform.submit();
}
function OnMouseDown() {
    if (window.event.button == 2 && GetContextMenu(window.event.srcElement) != "") {
        if (window.event.srcElement.id.toLowerCase().indexOf("growerid", 0) != -1) {
            boxType = 'Grower';
        }
        else if (window.event.srcElement.id.toLowerCase().indexOf("warehouseid", 0) != -1) {
            boxType = 'Warehouse';
        }
        else if (window.event.srcElement.id.toLowerCase().indexOf("growerblockid", 0) != -1) {
            boxType = 'GrowerBlock';
        }
        else if (window.event.srcElement.id.toLowerCase().indexOf("vendorid", 0) != -1) {
            boxType = 'VendorName';
        }
        else if (window.event.srcElement.id.toLowerCase().indexOf("commoditylookup", 0) != -1) {
            boxType = 'Commodity';
        }
        else if (window.event.srcElement.id.toLowerCase().indexOf("lotid", 0) != -1) {
            boxType = 'Lot';
        }
        else if (window.event.srcElement.id.toLowerCase().indexOf("phaseid", 0) != -1) {
            boxType = 'Phase';
        }
        else if (window.event.srcElement.id.toLowerCase().indexOf("costcenterid", 0) != -1) {
            boxType = 'CostCenter';
        }
        else if (window.event.srcElement.id.toLowerCase().indexOf("txtvariety", 0) != -1) {
            boxType = 'VarietySingle';
            //singleLookup = 'variety_Key';
            singleLookup = true;
            lookupSearchParam1 = 'txtCommodity';
        }
        else if (window.event.srcElement.id.toLowerCase().indexOf("txtcommodity", 0) != -1) {
            boxType = 'CommoditySingle';
            //singleLookup = 'commodity_Key';            
            singleLookup = true;
        }
        else if (window.event.srcElement.id.toLowerCase().indexOf("txtcustomer", 0) != -1) {
            boxType = 'CustomerSingle';
            //singleLookup = 'commodity_Key';            
            singleLookup = true;
        }
        else if (window.event.srcElement.id.toLowerCase().indexOf("txtsalesperson", 0) != -1) {
            boxType = 'SalespersonSingle';
            //singleLookup = 'salesperson_Key';
            singleLookup = true;
        }
        else if (window.event.srcElement.id.toLowerCase().indexOf("txtsalesman", 0) != -1) {
            boxType = 'SalespersonSingleFreightAllocation';
            //singleLookup = 'salesperson_Key';
            singleLookup = true;
        }

        if (window.event.stopPropagation) { window.event.stopPropagation(); }
        ShowContextMenu(window.event.clientX, window.event.clientY, window.event.srcElement);
        //if (window.event.preventDefault) {
        //    window.event.preventDefault();
        //}
        //else {
        //    window.event.returnValue = false;
        //    window.event.cancelBubble = true;
        //}

    }
    if (window.event.button == 1 && window.event.srcElement.LeftClickLookup != undefined) {
        ControlToUpdate = window.event.srcElement;
        contextMenuDiv = document.getElementById("LookupSearchDiv");
        contextMenuDiv.LookupType = window.event.srcElement.LookupSearch;
        ShowLookupSearch('Lookup');
    }
}

function OnClick() {
    HideContextMenu();
    HideCalendar();
}
/********************************* End Context Functions ********************************/

/********************************** Calendar Functions ****************************************/

var oDP = null;
function init() {
    oDate = new Date();
    //oDP   = new frameDatePicker.DatePicker("divDatePicker", oDate.getFullYear()-2, oDate.getFullYear()+5);

    // If you want Sunday as first day of the week use this construction call instead:
    oDP = new frameDatePicker.DatePicker("divDatePicker", oDate.getFullYear() - 10, oDate.getFullYear() + 10, true);

    // Use another init year/month than todays. 0=Jan, 11=Dec
    //oDP.setInitDate(2003, 0);
}

/********************************* End Calendar Functions ********************************/
/********************************* Date Functions*****************************************/
function DaysInMonth(WhichMonth, WhichYear) {
    var DaysInMonth = 31;
    if (WhichMonth == "Apr" || WhichMonth == "Jun" || WhichMonth == "Sep" || WhichMonth == "Nov") DaysInMonth = 30;
    if (WhichMonth == "Feb" && (WhichYear / 4) != Math.floor(WhichYear / 4)) DaysInMonth = 28;
    if (WhichMonth == "Feb" && (WhichYear / 4) == Math.floor(WhichYear / 4)) DaysInMonth = 29;
    return DaysInMonth;
}
function CheckDateField(control) {
    CleanDateField(control);
    var theDate = new Date();

    switch (control.value.split('/').length) {
        case 1:
            if (control.value > 0 && control.value < DaysInMonth(theDate.getMonth(), theDate.getFullYear())) {
                theDate.setDate(control.value);
                SetFormatedDate(control, theDate);
            }
            //else
            //DateError(control);
            break;
        case 2:
            IsMonthDay(control);
            break;
        case 3:
            IsMonthDayShortYear(control);
            break;
    }

}
function DateError(control) {
    //document.getElementById(control.id).style.background ='#ff0033';
    control.focus();
    setTimeout('setFocus("' + control.id + '");', 10);
    alert(control.value + ' is not a valid date.');
}
function setFocus(control) {
    if (control != undefined)
        document.getElementById(control).focus();
}
function CleanDateField(control) {
    var str = new String();

    //Trim and store the text
    str = Trim(control.value);
    //replace .-\ with /
    str = str.replace(/\./g, "/");
    str = str.replace(/-/g, "/");
    str = str.replace(/\\/g, "/");

    control.value = str;
}

function CheckForShortDate(control) {
    var theDate = new Date();
    if (control.value > 0 && control.value <= DaysInMonth(theDate.getMonth(), theDate.getFullYear)) {
        theDate.setDate(control.value);
        SetFormatedDate(control, theDate);
    }
    //var blah = asdf;

    IsMonthDay(control)
    IsMonthDayShortYear(control)
    //if((theDate.getMonth()+1) + "-" + theDate.getDate() == control.value)
    //	SetFormatedDate(control,theDate);	
}
function IsMonthDayShortYear(control) {
    var theDate2 = new Date();
    var inputValue = control.value;
    var month = "";
    var day = "";
    var year = "";

    //Get Month
    if (inputValue.indexOf("-") > -1) {
        month = inputValue.substring(0, control.value.indexOf("-"));
        inputValue = inputValue.substring(inputValue.indexOf("-") + 1, inputValue.length);
    }
    else if (inputValue.indexOf("/") > -1) {
        month = inputValue.substring(0, control.value.indexOf("/"));
        inputValue = inputValue.substring(inputValue.indexOf("/") + 1, inputValue.length);
    }

    //Get Day
    if (inputValue.indexOf("-") > -1) {
        day = inputValue.substring(0, inputValue.indexOf("-"));
        inputValue = inputValue.substring(inputValue.indexOf("-") + 1, inputValue.length);
    }
    else if (inputValue.indexOf("/") > -1) {
        day = inputValue.substring(0, inputValue.indexOf("/"));
        inputValue = inputValue.substring(inputValue.indexOf("/") + 1, inputValue.length);
    }

    //Get Year
    if (inputValue.length == 2) {
        year = inputValue;
    }

    if (year.length == 2) {

        if (year < 50)
            year = "20" + year;

        //			theDate2.setYear(year);
        //			theDate2.setMonth(month-1);
        //			theDate2.setDate(day); 
        //			SetFormatedDate(control,theDate2);
        control.value = (month + "/" + day + "/" + year);
    }
    else {

    }


}
function IsMonthDay(control) {
    var theDate2 = new Date();
    var month;
    var day;
    var year = theDate2.getFullYear();
    if (control.value.indexOf("/") > -1) {
        month = control.value.substring(0, control.value.indexOf("/"));
        day = control.value.substring(control.value.indexOf("/") + 1, control.value.length);
        if (day.indexOf("/") == -1) {
            if (month > 0 && month < 13 && day > 0 && day <= DaysInMonth(month, theDate2.getFullYear)) {
                //					theDate2.setMonth(month-1); 
                //					theDate2.setDate(day); 
                //                  SetFormatedDate(control,theDate2);
                control.value = (month + "/" + day + "/" + year);

            }
        }
    }
}
function SetFormatedDate(control, theDate) {
    control.value = (theDate.getMonth() + 1) + "/" + theDate.getDate() + "/" + theDate.getFullYear();
}

function FirstMondayOfTheYearOffSet(varDate) {
    var jan1 = new Date(varDate.getYear(), 0, 1);

    if (jan1.getDay() == 0)
        return 1;
    else if (jan1.getDay() < 5)
        return -((jan1.getDay()) - 1);
    else
        return 8 - (jan1.getDay());
}
function FirstMondayOfTheYear(varDate) {
    var firstMonday = new Date(varDate.getFullYear(), 0, 1);
    firstMonday.setDate(firstMonday.getDate() + FirstMondayOfTheYearOffSet(varDate));
    return firstMonday;
}
function WeekToStartDate(week, year) {
    var date = new Date(year, 0, 1);

    date.setDate(date.getDate() + FirstMondayOfTheYearOffSet(date));
    date.setDate(date.getDate() + ((week - 1) * 7));

    return date;
}
function WeekToEndDate(week, year) {
    var date = WeekToStartDate(week, year);

    date.setDate(date.getDate() + 6);

    return date;
}
function DateToWeek(inDate) {
    firstMondayUTC = FirstMondayOfTheYear(inDate).getTime() / 86400000;
    inDateUTC = inDate.getTime() / 86400000;
    dayOffset = (inDateUTC - firstMondayUTC);

    //alert(dayOffset);
    if (dayOffset < 0) {
        inDate.setDate(inDate.getDate() - (inDate.getDate() + 1))
        return DateToWeek(inDate)
    }
    else if (inDate.getDay() != 0 && inDate.getDay() < 4) {
        inDate.setDate(inDate.getDate() + (6 - inDate.getDay()));
        return DateToWeek(inDate);
    }
    else {
        //salert(dayOffset);
        return Math.floor(dayOffset / 7) + 1;
    }
}

function Trim(s) {
    s = s.replace(/ /g, "");
    s = s.replace(/\n/g, "");

    return s;
}
function GetShortDate(date) {
    return (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
}

function SetStartDate(weekField, yearField, dateField) {
    yearText = Trim(document.getElementsByName(yearField).item(0).value);
    weekText = Trim(document.getElementsByName(weekField).item(0).value);

    if (yearText != '' && weekText != '') {
        date = WeekToStartDate(weekText, yearText);
        document.getElementsByName(dateField).item(0).value = GetShortDate(date);
    }
}

function SetEndDate(weekField, yearField, dateField) {
    yearText = Trim(document.getElementsByName(yearField).item(0).value);
    weekText = Trim(document.getElementsByName(weekField).item(0).value);

    if (yearText != '' && weekText != '') {
        date = WeekToEndDate(weekText, yearText);
        document.getElementsByName(dateField).item(0).value = GetShortDate(date);
    }
}

function ClearWeekYear(weekField, YearField) {
    document.getElementsByName(weekField).item(0).value = '';
    document.getElementsByName(YearField).item(0).value = '';
}
function TestDateToWeek() {
    var testDate = new Date('12/29/2003');

    testDate = new Date('12/29/2003');
    document.writeln('1: ' + DateToWeek(testDate) + '<br>');
    testDate = new Date('1/1/2004');
    document.writeln('1: ' + DateToWeek(testDate) + '<br>');
    testDate = new Date('1/5/2004');
    document.writeln('2: ' + DateToWeek(testDate) + '<br>');
    testDate = new Date('1/1/2005');
    document.writeln('53: ' + DateToWeek(testDate) + '<br>');
    testDate = new Date('1/1/2006');
    document.writeln('52: ' + DateToWeek(testDate) + '<br>');
    testDate = new Date('1/2/2006');
    document.writeln('1: ' + DateToWeek(testDate) + '<br>');
    testDate = new Date('1/8/2006');
    document.writeln('1: ' + DateToWeek(testDate) + '<br>');
    testDate = new Date('1/9/2006');
    document.writeln('2: ' + DateToWeek(testDate) + '<br>');
    testDate = new Date('1/2/2006');
}
function TestWeekToStartDate() {
    document.writeln('Week 1 2001 =' + GetShortDate(WeekToStartDate(1, 2001)) + '<br>');
    document.writeln('Week 2 2001 =' + GetShortDate(WeekToStartDate(2, 2001)) + '<br>');
    document.writeln('Week 3 2001 =' + GetShortDate(WeekToStartDate(3, 2001)) + '<br>');

    document.writeln('Week 1 2002 =' + GetShortDate(WeekToStartDate(1, 2002)) + '<br>');
    document.writeln('Week 2 2002 =' + GetShortDate(WeekToStartDate(2, 2002)) + '<br>');
    document.writeln('Week 3 2002 =' + GetShortDate(WeekToStartDate(3, 2002)) + '<br>');

    document.writeln('Week 1 2003 =' + GetShortDate(WeekToStartDate(1, 2003)) + '<br>');
    document.writeln('Week 2 2003 =' + GetShortDate(WeekToStartDate(2, 2003)) + '<br>');
    document.writeln('Week 3 2003 =' + GetShortDate(WeekToStartDate(3, 2003)) + '<br>');

    document.writeln('Week 1 2004 =' + GetShortDate(WeekToStartDate(1, 2004)) + '<br>');
    document.writeln('Week 2 2004 =' + GetShortDate(WeekToStartDate(2, 2004)) + '<br>');
    document.writeln('Week 3 2004 =' + GetShortDate(WeekToStartDate(3, 2004)) + '<br>');

    document.writeln('Week 1 2005 =' + GetShortDate(WeekToStartDate(1, 2005)) + '<br>');
    document.writeln('Week 2 2005 =' + GetShortDate(WeekToStartDate(2, 2005)) + '<br>');
    document.writeln('Week 3 2005 =' + GetShortDate(WeekToStartDate(3, 2005)) + '<br>');

    document.writeln('Week 1 2006 =' + GetShortDate(WeekToStartDate(1, 2006)) + '<br>');
    document.writeln('Week 2 2006 =' + GetShortDate(WeekToStartDate(2, 2006)) + '<br>');
    document.writeln('Week 3 2006 =' + GetShortDate(WeekToStartDate(3, 2006)) + '<br>');

    document.writeln('Week 1 2007 =' + GetShortDate(WeekToStartDate(1, 2007)) + '<br>');
    document.writeln('Week 2 2007 =' + GetShortDate(WeekToStartDate(2, 2007)) + '<br>');
    document.writeln('Week 3 2007 =' + GetShortDate(WeekToStartDate(3, 2007)) + '<br>');

    document.writeln('Week 1 2008 =' + GetShortDate(WeekToStartDate(1, 2008)) + '<br>');
    document.writeln('Week 2 2008 =' + GetShortDate(WeekToStartDate(2, 2008)) + '<br>');
    document.writeln('Week 3 2008 =' + GetShortDate(WeekToStartDate(3, 2008)) + '<br>');
}
/********************************* End Date Functions ************************************/

/********************************** Resize  Functions **********************************/
function SetScreenSize() {
    //alert(document.location);//
    if (document.location.toString().indexOf("/MayanDoc.aspx") != -1 || document.location.toString().indexOf("AccountingGrid.aspx") != -1 || document.location.toString().indexOf("YellowInforma/ReportYellow.aspx") != -1 || document.location.toString().indexOf("YellowAdvanceReport/ReportYellow.aspx?AdvanceDays=40") != -1)
        ResizeScreenToMax();
    else
        ResizeScreenToNormal();
}
function ResizeScreenToNormal() {
    if (window != null) {
        try {
            window.resizeTo(790, 547);
            window.moveTo((screen.availWidth / 2) - (790 / 2), (screen.availHeight / 2) - (547 / 2));
        }
        catch (exception) {
        }
    }
}
function ResizeScreenToMax() {
    try {
        //alert(screen.width + 'x' + screen.height);
        switch (screen.width + 'x' + screen.height) {
            case '800x600':
                EnableStyleSheet('800x600');
                SetControlWidth('BaseRFBHeaderDiv', 800);

                window.resizeTo(800, 547);
                window.moveTo(0, (screen.availHeight / 2) - (547 / 2));
                break;
            case '1024x768':
                EnableStyleSheet('1024x768');
                SetControlWidth('BaseRFBHeaderDiv', 1024);

                window.resizeTo(1024, 720);
                window.moveTo(0, (screen.availHeight / 2) - (720 / 2));
                break;
            case '1280x1024':
                EnableStyleSheet('1280x1024');
                SetControlWidth('BaseRFBHeaderDiv', 1280);

                window.resizeTo(1280, 974);
                window.moveTo(0, (screen.availHeight / 2) - (974 / 2));
                break;
            case '1600x1200':
                EnableStyleSheet('1600x1200');
                SetControlWidth('BaseRFBHeaderDiv', 1600);

                window.resizeTo(1600, 1150);
                window.moveTo(0, (screen.availHeight / 2) - (1150 / 2));
                break;

            default:
                EnableStyleSheet('800x600');
                SetControlWidth('BaseRFBHeaderDiv', 800);

                window.resizeTo(800, 547);
                window.moveTo(0, (screen.availHeight / 2) - (547 / 2));
                break;
        }
    }
    catch (exception) {
    }
}
function SetControlClass(control, Class) {
    var Div = document.getElementById(control);
    if (Div != null)
        Div.className = Class;
}
function SetControlWidth(control, width) {
    var Div = document.getElementById(control);
    if (Div != null)
        Div.width = width;
}

function EnableStyleSheet(sheet) {
    var x;
    for (x = 0; x < document.styleSheets.length; x++) {
        if (document.styleSheets.item(x).title == sheet)
            document.styleSheets.item(x).disabled = false;
        else {
            if (document.styleSheets.item(x).title != undefined && document.styleSheets.item(x).title != "")
                document.styleSheets.item(x).disabled = true;
        }
    }
}

/********************************* End Resize Functions ************************************/

/**********************************  Misc Functions **********************************/
function SetFocusToControl(fieldName) {
    var field = document.getElementById(fieldName);
    if (field != undefined)
        field.focus();
}
function Fahrenheit2Celsius(fahrenheit) {
    return 5 / 9 * (fahrenheit - 32);
}
function Celsius2Fahrenheit(celsius) {
    return 9 / 5 * celsius + 32
}
function OpenStatsWindows() {
    var sizeX = screen.availWidth - 12;
    var sizeY = screen.availHeight - 126;
    var theURL = "../../../Stats/";
    var options = 'directories=yes,location=yes,menubar=yes,toolbar=yes,status=yes,scrollbars=yes,resizable=yes,left=0,top=0,width=' + sizeX + ',height=' + sizeY;
    newWin = window.open(theURL, '', options);
}
/********************************* End Misc Functions ************************************/

/********************************** Encode/Decode QueryString Functions **********************************/


function EncodeQueryString(queryString) {
    var str = new String();
    str = queryString;

    //Make sure all & are a pair and only a pair
    while (str.indexOf("&&") > -1)
        str = str.replace("&&", "&");
    str = str.replace(/&/g, "&&");

    // !->$ ~->@ &&->! ?->~
    str = str.replace(/\!/g, "$");
    str = str.replace(/~/g, "@");
    str = str.replace(/&&/g, "!");
    str = str.replace(/\?/g, "~");

    return str;
}
function DecodeQueryString(queryString) {
    var str = new String();
    str = queryString;

    //Make sure all & are a pair and only a pair
    while (str.indexOf("&&") > -1)
        str = str.replace("&&", "&");
    str = str.replace(/&/g, "&&");

    // ~->? !->&& @->~ $->!
    str = str.replace(/~/g, "?");
    str = str.replace(/!/g, "&&");
    str = str.replace(/@/g, "~");
    str = str.replace(/\$/g, "!");

    return str;
}
/********************************* End Encode/Decode QueryString Functions ************************************/

/**********************************   Functions **********************************/
