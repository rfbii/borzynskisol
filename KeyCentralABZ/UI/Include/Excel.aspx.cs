using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KeyCentral.UI.Include
{
	/// <summary>
	/// Summary description for Excel.
	/// </summary>
	public partial class Excel : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.Button send;
		protected bool firstTime = true;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if(this.IsPostBack && ViewState["firstTime"] == null && firstTime == true)
			{
				ViewState["firstTime"] = false;
				firstTime = false;
				string excelStuff = Request["EmailForm"];
				excelStuff = CleanExtraStuff(excelStuff);
				Session["ExcelDoc"] = excelStuff;
				RegisterStartupScript("ExcelAndClose",@"<script language='javascript'>window.open('Excel2.aspx', '', 'directories=no,location=yes,menubar=yes,status=yes,toolbar=yes,scrollbars=yes,resizable=yes');window.close();</script>");
				/*
				Response.ContentType = @"application/vnd.ms-excel";
				Response.ClearContent();
				Response.Write( CleanExtraStuff(Request["EmailForm"]));
				Session.Remove("ExcelDoc");
				Response.End();
*/
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
		
		private string CleanExtraStuff(string str)
		{
			string temp = str.ToLower();
			string cleanStr="";
			int emailStart; 
			int emailEnd;
			int lastEnd=0;

			emailStart = temp.IndexOf("<!-- email start -->",0);
			emailEnd = temp.IndexOf("<!-- email end -->",0);
			while(emailStart!=-1 && emailEnd !=-1)
			{
				cleanStr += str.Substring(emailStart+20,(emailEnd-emailStart)-20);
				//temp = str.Remove(emailStart,(emailEnd-emailStart)+17).ToLower();

				lastEnd = emailEnd+1;
				emailStart = temp.IndexOf("<!-- email start -->",lastEnd);
				emailEnd = temp.IndexOf("<!-- email end -->",lastEnd);
			}

			return "<HTML><Body>" + cleanStr + @"</Body></HTML>";
		}
	}
}
