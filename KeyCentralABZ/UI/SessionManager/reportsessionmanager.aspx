<%@ Page language="c#" Inherits="KeyCentral.UI.ReportSessionManager" EnableViewState="false" Codebehind="ReportSessionManager.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
  <head runat="server">
    <title>SessionManager</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1"/>
    <meta name="CODE_LANGUAGE" content="C#"/>
    <meta name="vs_defaultClientScript" content="JavaScript"/>
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
    <meta http-equiv="PRAGMA" content="NO-CACHE"/>
    <script type="text/javascript">stmh = setTimeout('window.location = window.location',55000);</script>
	<script type="text/javascript">
		function Disconnect(sessionID)
		{
			document.getElementById('SessionDisconnect').src ='ReportSessionManager.aspx?Disconnect=' + sessionID;
			//	setTimeout("document.getElementById('SessionDisconnect').src='';",300);
		}
	</script>
	<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
</head>
  <body>
    <form id="Form1" method="post" runat="server">
        <!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
		<div id="BodyDiv"  class="scroll">
			<%= GetDisplayTable() %>
			<asp:Button id="Button2" runat="server" Text="Run Cleanup" onclick="Button2_Click"></asp:Button>
			<iframe name="SessionDisconnect" style="WIDTH: 0px; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 0px; BORDER-BOTTOM-STYLE: none"></iframe>
		</div>
	<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
     </form>





  </body>
</html>
