using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KeyCentral.Functions;

namespace KeyCentral.UI
{
	/// <summary>
	/// Summary description for SessionPing.
	/// </summary>
	public partial class SessionPing : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User_Name"] != null)
			{
				SessionManager sessionManager = new SessionManager();
				
				sessionManager.RegisterPing(Session.SessionID,(string)Session["User_Name"],Request.QueryString["LastDoc"],Request);

				if(sessionManager.ForceLogout())
					Response.Write("<script>window.parent.HandleServerMessage('Logout');</script>");
			}

			//Response.Write(DateTime.Now.ToString());
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

	}
}
