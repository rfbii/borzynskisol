using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using KeyCentral.Functions;

namespace KeyCentral.UI
{
	public partial class ReportSessionManager :  BasePage
	{
		protected System.Web.UI.WebControls.Button Button1;
		private bool IsDisconnectPage=false;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			//Security Check
			if (!this.IsAdmin())
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx");

			//Handle disconnect request
			if(Request.QueryString["Disconnect"] != null && Request.QueryString["Disconnect"] != "")
			{
				Response.Write("Disconnecting:" + Request.QueryString["Disconnect"] + ":");
				SessionManager sessionManager = new SessionManager();
				if(!sessionManager.RegisterLogout(Request.QueryString["Disconnect"]))
					Response.Write("Session not found!");
				else
					Response.Write("Done!");

				IsDisconnectPage = true;
			}

			if(!IsDisconnectPage)
				Response.Write(SessionManager.GetPingScript());


			//DisplayTable();

			// Put user code to initialize the page here
		}
		public void DisplayTable()
		{
			if(IsDisconnectPage)
				return;

			Response.Write(GetDisplayTable());
		}
		public string GetDisplayTable()
		{
			if(IsDisconnectPage)
				return String.Empty;

			StringBuilder builder = new StringBuilder();

			SessionManager sessionManager = new SessionManager();
			ArrayList sessionList = sessionManager.GetSessionList();

            builder.Append("<table width=\"300\"><tr><td style='FONT-SIZE: 15px'>Session ID</td><td style='FONT-SIZE: 15px'>User Name</td><td width='150' style='FONT-SIZE: 15px'>Last Doc</td><td style='FONT-SIZE: 15px'>First Ping</td><td style='FONT-SIZE: 15px'>LastPing</td><td style='FONT-SIZE: 15px'>IP</td><td style='FONT-SIZE: 15px'>Disconnecting</td><td style='FONT-SIZE: 15px'>SessionID</td></tr>");
			int loopLimit = sessionList.Count;
			for(int x=0;x<loopLimit;x++)
			{
				SessionManagerCBO tempRecord = (SessionManagerCBO)sessionList[x];


                builder.Append("<tr><td style='FONT-SIZE: 15px'><input type='button' onClick=\"Disconnect('" + tempRecord.SessionID + "');\" value='Disconnect' >");

                builder.Append("</td><td style='FONT-SIZE: 15px'>" + tempRecord.UserName + "</td><td width='150' style='FONT-SIZE: 15px'>" + tempRecord.LastDoc/*.Replace("\\"," \\").Replace("/"," /")*/ + "</td><td style='FONT-SIZE: 15px'>" + tempRecord.FirstPing.ToString() + "</td><td style='FONT-SIZE: 15px'>" + tempRecord.LastPing.ToString() + "</td><td style='FONT-SIZE: 15px'>" + tempRecord.ClientIPAddress + "</td><td style='FONT-SIZE: 15px'>" + tempRecord.ForceLogout.ToString() + "</td><td style='FONT-SIZE: 15px'>" + tempRecord.SessionID + "</td></tr>");

			}
			builder.Append("</table>");

			return builder.ToString();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void Button2_Click(object sender, System.EventArgs e)
		{
			SessionManager sessionManager = new SessionManager();

			Response.Write(sessionManager.RunCleanupNow());
		
		}

	}
}
