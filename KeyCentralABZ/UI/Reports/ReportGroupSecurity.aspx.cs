using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Data.OleDb;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Text;
using KeyCentral.Functions;
using KeyCentral.Lookups;

namespace KeyCentral.UI.Reports
{
	public partial class ReportGroupSecurity : KeyCentral.Functions.BasePage
	{
		public DateTime dt1;
		public DateTime dt2;
		public DateTime dt3;
		public DateTime dt4;
		protected System.Web.UI.HtmlControls.HtmlGenericControl CommentLine;
		protected ReportTotalsHelper totalHelper;
	
		#region Totals
		#region Vars
		private string strName="";
		private bool nameChanged= false;
		#endregion

		public string CheckForChanges(object oItem)
		{
			bool firstRecord = false;
			if (strName == "")
			{
				strName = GetField(oItem,"Name");
				firstRecord = true;
			}
			if(strName != GetField(oItem,"Name"))
			{
				nameChanged = true;
				strName = GetField(oItem,"Name");
			}
			else
				nameChanged= false;

			if (firstRecord)
				nameChanged = true;
		
			return "";
		}
		public string NewRecord(object oItem)
		{
			return "";
		}
		
		public string Detail(object oItem)
		{
			string strRet="";
			if (nameChanged)
			{
				strRet = @"
					<tr>
						<td class='DetailLineNumber' width='5'>&nbsp;</td>
						<td width='150' class='DetailLineNumber'>" + GetField(oItem,"Name") + @"&nbsp;</td>
						<td width='75' class='DetailLineNumber'>" + GetField(oItem,"Type") + @"&nbsp;</td>
						<td width='295' class='DetailLineNumber'>" + GetField(oItem,"Detail") + @"&nbsp;</td>
						</tr>";
			}
			else
			{
				strRet = @"
					<tr>
						<td class='DetailLine' width='5'>&nbsp;</td>
						<td width='150' class='DetailLine'>&nbsp;</td>
						<td width='75' class='DetailLine'>&nbsp;</td>
						<td width='295' class='DetailLine'>" + GetField(oItem,"Detail") + @"</td>
						</tr>";
			}
			return strRet;
		}
		public string OrderFooter()
		{
			string strRet="";
			strRet = @"
					<tr>
						<td class='OrderHeader2' width='0'>&nbsp;</td>
					</tr>";
			return strRet;
		}	
		private string GetField(object oItem,string strField)
		{
			return DataBinder.Eval(oItem,"DataItem." + strField).ToString();
		}
		private string GetDateField(object oItem,string strField)
		{
			string str = DataBinder.Eval(oItem,"DataItem." + strField).ToString();
			if(str.IndexOf(" ",0)>0)
				return str.Substring(0,str.IndexOf(" ",0));
			else
				return str;
		}
		#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//Security Check
			if (!this.IsAdmin())
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx");

			KeyCentral.Functions.JavaStatusMessage status = new KeyCentral.Functions.JavaStatusMessage(this);
			status.ShowMessage("Computing");
			
			GetReportData();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}

		private void InitializeComponent()
		{    
		}
		#endregion

		private void GetReportData()
		{
			SqlConnection Conn = GetSQLConn("GetReportData");
			OracleConnection OracleConn = GetOracleConn();

			//Get SQL Data
			DataSet SQLData =GetSQLData(Conn);
			
		}
		private DataSet GetSQLData(SqlConnection Conn)
		{
			OracleConnection OracleConn = GetOracleConn();

			DataSet retData = new DataSet();
			SqlDataAdapter selectCMD = new SqlDataAdapter("select SType_Group.Description As Name, SType.Description As Type, SType_Detail.Detail from SType left join SType_Group on SType_Group.SType_Key = SType.SType_Key left join SType_Group_Detail on SType_Group_Detail.SType_Group_Key = SType_Group.SType_Group_Key left join SType_Detail on SType_Detail.SType_Detail_Key = SType_Group_Detail.SType_Detail_Key Order by 2,1,3",Conn);
			selectCMD.Fill(retData);

			SortedList growerData =GetGrowerData(OracleConn,GetSTypeArray(retData,1,"Grower",2));
			SortedList warehousesData =GetWarehouseData(OracleConn,GetSTypeArray(retData,1,"Warehouse",2));
			
			ReplaceField(retData,1,"Grower",2,growerData);
			ReplaceField(retData,1,"Warehouse",2,warehousesData);

			if (retData.Tables[0].DefaultView.Count != 0)
			{
				retData.Tables[0].DefaultView.Sort = "Type, Name, Detail";
				Repeater1.DataSource = retData.Tables[0].DefaultView;
				Repeater1.DataBind();
			}

			Conn.Close();
			Conn.Dispose();	
						
			return retData;
		}
		private void ReplaceField(DataSet data,int testField,string testValue,int replaceField,SortedList replaceList)
		{
			string temp;
			for(int x=0;x<data.Tables[0].Rows.Count;x++)
			{
				if(data.Tables[0].Rows[x].ItemArray.GetValue(testField).ToString() == testValue && data.Tables[0].Rows[x].ItemArray.GetValue(2).ToString()!= "")
				{
					//Handle all warehouses/growers
					if(data.Tables[0].Rows[x][replaceField].ToString() == "-1")
					{
						if(testValue == "Grower")
						{
							data.Tables[0].Rows[x][replaceField] = "ALL GROWERS";
						}
						else if(testValue == "Warehouse")
						{
							data.Tables[0].Rows[x][replaceField] = "ALL WAREHOUSES";
						}
					}
					else
					{
						temp = (string)replaceList[data.Tables[0].Rows[x].ItemArray.GetValue(replaceField).ToString()];
						data.Tables[0].Rows[x][replaceField] = temp;
					}
				}
			}
		}
		private ArrayList GetSTypeArray(DataSet data,int field,string test,int value)
		{
			ArrayList ret = new ArrayList();

			for(int x=0;x<data.Tables[0].Rows.Count;x++)
			{
				if (data.Tables[0].Rows[x].ItemArray.GetValue(field).ToString() == test && data.Tables[0].Rows[x].ItemArray.GetValue(2).ToString()!= "")
					ret.Add( new CriteriaBoxRecord(data.Tables[0].Rows[x].ItemArray.GetValue(value).ToString(),"",""));
			}
			return ret;
		}
		private SortedList GetGrowerData(OracleConnection OracleConn,ArrayList growerlist)
		{
			System.Collections.SortedList ret = new SortedList();
			string reportCommand = FormatOracle("Select Fc_Name.NameIdx,Concat(rpad(Fc_Name.Id,6,'_'),lpad(Fc_Name.LastCoName,40)) as GrowerID  From #COMPANY_NUMBER#.Fc_Name where 1 = 1 #WhereClause:Fc_Name.NameIdx#");
			WhereClauseBuilder whereClause = new WhereClauseBuilder();

			//build the functions
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32,"Fc_Name.NameIdx",(ArrayList)(growerlist));
			OracleCommand OracleData = new OracleCommand(whereClause.BuildWhereClause(reportCommand), OracleConn);

			Oracle.DataAccess.Client.OracleDataReader myReader =OracleData.ExecuteReader();
			while (myReader.Read()) 
			{
				ret.Add(myReader.GetInt32(0).ToString(),myReader.GetString(1));
			}
			myReader.Dispose();
			OracleData.Dispose();
			return ret;
		}
		private SortedList GetWarehouseData(OracleConnection OracleConn,ArrayList list)
		{
			for(int i = 0; i < list.Count; i++)
			{
				CriteriaBoxRecord temp = (KeyCentral.Lookups.CriteriaBoxRecord) list[i];
				if(temp.key == "" && temp.text == "")
					return null;
			}
			System.Collections.SortedList ret = new SortedList();
			string reportCommand = FormatOracle("select Ic_Warehouse.WarehouseIdx, Fc_Name_Location.Descr from #COMPANY_NUMBER#.Ic_Warehouse,#COMPANY_NUMBER#.Fc_Name_Location where Ic_Warehouse.NameLocationSeq  = Fc_Name_Location.NameLocationSeq and Ic_Warehouse.NameIdx  = Fc_Name_Location.NameIdx #WhereClause:Ic_Warehouse.WarehouseIdx#");
			WhereClauseBuilder whereClause = new WhereClauseBuilder();

			//build the functions
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32,"Ic_Warehouse.WarehouseIdx",(ArrayList)(list));
			OracleCommand OracleData = new OracleCommand(whereClause.BuildWhereClause(reportCommand), OracleConn);

			Oracle.DataAccess.Client.OracleDataReader myReader =OracleData.ExecuteReader();
			while (myReader.Read()) 
			{
				ret.Add(myReader.GetInt32(0).ToString(),myReader.GetString(1));
			}
			myReader.Dispose();
			OracleData.Dispose();
			return ret;
		}
	}
}
