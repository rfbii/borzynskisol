using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Data.OleDb;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Text;
using KeyCentral.Functions;
using KeyCentral.Lookups;

namespace KeyCentral.UI.Reports
{
	public partial class ReportUserSecurity : KeyCentral.Functions.BasePage
	{
		public DateTime dt1;
		public DateTime dt2;
		public DateTime dt3;
		public DateTime dt4;
		protected System.Web.UI.HtmlControls.HtmlGenericControl CommentLine;
		protected ReportTotalsHelper totalHelper;
	

		#region Totals
		#region Vars
		private string strLastName="";
		private bool bNameChanged= false;
		#endregion

		public string CheckForChanges(object oItem)
		{
			if (strLastName == "")
				strLastName = GetField(oItem,"UserName");
			if(strLastName != GetField(oItem,"UserName"))
			{
				bNameChanged = true;
				strLastName = GetField(oItem,"UserName");
			}
			else
				bNameChanged= false;
		return "";
		}
		public string NewRecord(object oItem)
		{
			return "";
		}
		
		public string Detail(object oItem)
		{
			string strRet="";
			if (bNameChanged)
			{
				strRet = @"
					<tr>
						<td class='DetailLineNumber' width='5'>&nbsp;</td>
						<td width='75' class='DetailLineNumber'>" + GetField(oItem,"UserName")+ @"</td>
						<td width='75' class='DetailLineNumber'>" + GetField(oItem,"Type") + @"</td>
						<td width='75' class='DetailLineNumber'>" + GetField(oItem,"SType") + @"</td>
						<td width='295' class='DetailLineNumber'>" + GetField(oItem,"Detail") + @"</td>
						</tr>";
			}
			else
			{
				strRet = @"
					<tr>
						<td class='DetailLine' width='5'>&nbsp;</td>
						<td width='75' class='DetailLine'>" + GetField(oItem,"UserName")+ @"</td>
						<td width='75' class='DetailLine'>" + GetField(oItem,"Type") + @"</td>
						<td width='75' class='DetailLine'>" + GetField(oItem,"SType") + @"</td>
						<td width='295' class='DetailLine'>" + GetField(oItem,"Detail") + @"</td>
						</tr>";
			}
				return strRet;
		}
		public string OrderFooter()
		{
			string strRet="";
			strRet = @"
					<tr>
						<td class='OrderHeader2' width='0'>&nbsp;</td>
					</tr>";
			return strRet;
		}	
		private string GetField(object oItem,string strField)
		{
			return DataBinder.Eval(oItem,"DataItem." + strField).ToString();
		}
		private string GetDateField(object oItem,string strField)
		{
			string str = DataBinder.Eval(oItem,"DataItem." + strField).ToString();
			if(str.IndexOf(" ",0)>0)
				return str.Substring(0,str.IndexOf(" ",0));
			else
				return str;
		}
		#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//Security Check
			if (!this.IsAdmin())
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx");
			
			KeyCentral.Functions.JavaStatusMessage status = new KeyCentral.Functions.JavaStatusMessage(this);
			status.ShowMessage("Computing");
			
			GetReportData();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}
		
		private void InitializeComponent()
		{    

		}
		#endregion
		
		
		private void GetReportData()
		{
			SqlConnection Conn = GetSQLConn("GetReportData");
			OracleConnection OracleConn = GetOracleConn();

			//Get SQL Data
			DataSet SQLData =GetSQLData(Conn);
			
		}
		private DataSet GetSQLData(SqlConnection Conn)
		{
			OracleConnection OracleConn = GetOracleConn();

			DataSet retData = new DataSet();
			//	select  [User].UserName,'Group' as Type,SType.Description as Stype,SType_Group.Description as Detail from [User_SType_Group] join [SType_Group] on SType_Group.SType_Group_Key = User_SType_Group.SType_Group_Key join [User] on [User].User_Key = User_SType_Group.User_Key join [SType] on [SType].SType_Key = SType_Group.SType_Key Union All select  [User].UserName,'Detail' as Type, SType.Description as Stype, SType_Detail.Detail as Detail from [User_SType] join [SType_Detail] on SType_detail.SType_Detail_Key = User_SType.SType_Detail_Key join [User] on [User].User_Key = User_SType.User_Key join [SType] on [SType].SType_Key = SType_Detail.SType_Key Order by [User].UserName, Type, SType, Detail
			SqlDataAdapter selectCMD = new SqlDataAdapter("select [User].UserName,'Group' as Type,SType.Description as Stype,SType_Group.Description as Detail from [User_SType_Group] join [SType_Group] on SType_Group.SType_Group_Key = User_SType_Group.SType_Group_Key join [User] on [User].User_Key = User_SType_Group.User_Key join [SType] on [SType].SType_Key = SType_Group.SType_Key Union All select  [User].UserName,'Detail' as Type, SType.Description as Stype, SType_Detail.Detail as Detail from [User_SType] join [SType_Detail] on SType_detail.SType_Detail_Key = User_SType.SType_Detail_Key join [User] on [User].User_Key = User_SType.User_Key join [SType] on [SType].SType_Key = SType_Detail.SType_Key Order by [User].UserName, Type, SType, Detail",Conn);
			selectCMD.Fill(retData);
			
			SortedList growerData =GetGrowerData(OracleConn,GetSTypeArray(retData,2,"Grower",3));
			SortedList warehousesData =GetWarehouseData(OracleConn,GetSTypeArray(retData,2,"Warehouse",3));
			
			ReplaceField(retData,2,"Grower",3,growerData);
			ReplaceField(retData,2,"Warehouse",3,warehousesData);

			if (retData.Tables[0].DefaultView.Count != 0)
			{
				this.Repeater1.DataSource = retData.Tables[0].DefaultView;
					Repeater1.DataBind();
			}
			OracleConn.Close();
			OracleConn.Dispose();

			Conn.Close();
			Conn.Dispose();	
						
			return retData;
		}
		private void ReplaceField(DataSet data,int testField,string testValue,int replaceField,SortedList replaceList)
		{
			string temp;
			for(int x=0;x<data.Tables[0].Rows.Count;x++)
			{
				if(data.Tables[0].Rows[x].ItemArray.GetValue(testField).ToString() == testValue && data.Tables[0].Rows[x].ItemArray.GetValue(1).ToString()== "Detail")
				{
					//Handle all warehouses/growers
					if(data.Tables[0].Rows[x][replaceField].ToString() == "-1")
					{
						if(testValue == "Grower")
						{
							data.Tables[0].Rows[x][replaceField] = "ALL GROWERS";
						}
						else if(testValue == "Warehouse")
						{
							data.Tables[0].Rows[x][replaceField] = "ALL WAREHOUSES";
						}
					}
					else
					{
						temp = (string)replaceList[data.Tables[0].Rows[x].ItemArray.GetValue(replaceField).ToString()];
						data.Tables[0].Rows[x][replaceField] = temp;
					}
				}
			}

		}
		private ArrayList GetSTypeArray(DataSet data,int field,string test,int value)
		{
			ArrayList ret = new ArrayList();
			for(int x=0;x<data.Tables[0].Rows.Count;x++)
			{
				if (data.Tables[0].Rows[x].ItemArray.GetValue(field).ToString() == test && data.Tables[0].Rows[x].ItemArray.GetValue(1).ToString()== "Detail")
                    ret.Add( new CriteriaBoxRecord(data.Tables[0].Rows[x].ItemArray.GetValue(value).ToString(),"",""));
			}
			return ret;
		}
		private SortedList GetGrowerData(OracleConnection OracleConn,ArrayList growerlist)
		{
			System.Collections.SortedList ret = new SortedList();
			string reportCommand = FormatOracle("Select Fc_Name.NameIdx,Concat(rpad(Fc_Name.Id,6,'_'),lpad(Fc_Name.LastCoName,40)) as GrowerID  From #COMPANY_NUMBER#.Fc_Name where 1 = 1 #WhereClause:Fc_Name.NameIdx#");
			WhereClauseBuilder whereClause = new WhereClauseBuilder();

			//build the functions
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32,"Fc_Name.NameIdx",(ArrayList)(growerlist));
			OracleCommand OracleData = new OracleCommand(whereClause.BuildWhereClause(reportCommand), OracleConn);

			Oracle.DataAccess.Client.OracleDataReader myReader =OracleData.ExecuteReader();
			while (myReader.Read()) 
			{
				ret.Add(myReader.GetInt32(0).ToString(),myReader.GetString(1));
			}
			myReader.Dispose();
			OracleData.Dispose();
			return ret;
		}
		private SortedList GetWarehouseData(OracleConnection OracleConn,ArrayList list)
		{
			System.Collections.SortedList ret = new SortedList();
			string reportCommand = FormatOracle("select Ic_Warehouse.WarehouseIdx, Fc_Name_Location.Descr from #COMPANY_NUMBER#.Ic_Warehouse,#COMPANY_NUMBER#.Fc_Name_Location where Ic_Warehouse.NameLocationSeq  = Fc_Name_Location.NameLocationSeq and Ic_Warehouse.NameIdx  = Fc_Name_Location.NameIdx #WhereClause:Ic_Warehouse.WarehouseIdx#");
			WhereClauseBuilder whereClause = new WhereClauseBuilder();

			//build the functions
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32,"Ic_Warehouse.WarehouseIdx",(ArrayList)(list));
			OracleCommand OracleData = new OracleCommand(whereClause.BuildWhereClause(reportCommand), OracleConn);

			Oracle.DataAccess.Client.OracleDataReader myReader =OracleData.ExecuteReader();
			while (myReader.Read()) 
			{
				ret.Add(myReader.GetInt32(0).ToString(),myReader.GetString(1));
			}
			myReader.Dispose();
			OracleData.Dispose();
			return ret;
		}
		
	}
}
