<%@ Page language="c#" Inherits="KeyCentral.UI.Reports.ReportUserSecurity" Codebehind="ReportUserSecurity.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>User Security Report</title>
		<style type="text/css">.ReportTable.TD { FONT-SIZE: 8pt }
	.ReportTable.TD.OrderHeader1 { BORDER-TOP: #000000 1px solid; FONT-SIZE: 8pt }
	.ReportTable.TD.OrderHeader2 { FONT-SIZE: 8pt }
	.ReportTable.TD.OrderHeader2Right { BORDER-RIGHT: lightgrey 1px solid; FONT-SIZE: 8pt }
	.ReportTable.TD.OrderHeader2Top { BORDER-TOP: lightgrey 1px solid; FONT-SIZE: 8pt }
	.ReportTable.TD.OrderHeader2TopRight { BORDER-RIGHT: lightgrey 1px solid; BORDER-TOP: lightgrey 1px solid; FONT-SIZE: 8pt }
	.ReportTable.TD.OrderHeader3 { BORDER-TOP: lightgrey 1px solid; FONT-SIZE: 8pt }
	.ReportTable.TD.OrderHeader4 { FONT-SIZE: 8pt }
	.ReportTable.TD.OrderHeader4Number { BORDER-LEFT: lightgrey 1px solid; FONT-SIZE: 8pt }
	.ReportTable.TD.DetailLine { BORDER-TOP: lightgrey 1px solid; FONT-WEIGHT: bold; FONT-SIZE: 8pt }
	.ReportTable.TD.DetailLineNumber { BORDER-TOP: lightgrey 3px double; FONT-WEIGHT: bold; FONT-SIZE: 8pt }
	.ReportTable.TD.SubTotalSoldTop { BORDER-TOP: #000000 1px solid; FONT-SIZE: 8pt }
	.ReportTable.TD.SubTotalSold { FONT-SIZE: 8pt }
	.ReportTable.TD.SubTotalShipTop { BORDER-TOP: #000000 1px solid; FONT-SIZE: 8pt }
	.ReportTable.TD.SubTotalShip { FONT-SIZE: 8pt }
	.ReportTable.TD.OrderFooter { BORDER-TOP: #000000 3px double; FONT-SIZE: 8pt }
	.Title { FONT-WEIGHT: bold; FONT-SIZE: 12pt; COLOR: black; FONT-FAMILY: Arial }
	.Title1 { FONT-WEIGHT: bold; FONT-SIZE: 10pt; FONT-FAMILY: Arial }
		</style>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="/KeystoneCustomerStatus/Inc/_styles.css" type="text/css" rel="stylesheet"/>
	<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body bgcolor="#ffffff" onload="SetupPrinting();">
		<object id="factory" style="DISPLAY: none" codebase="http://www.meadroid.com/scriptx/ScriptX.cab#Version=6,1,431,2"
			classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814" viewastext="">
		</object>
		<script type="text/javascript" language="javascript">
		function SetupPrinting()
		{
			factory.printing.footer = "Printed: &D &t&bPage &p of &P&bUser Security Report";
			factory.printing.header = "";
			factory.printing.leftMargin = .5;
			factory.printing.rightMargin = .5;
			factory.printing.topMargin = .5;
			factory.printing.bottomMargin = .5;
		}  
		</script>
<form id="Form1" method="post" runat="server">
		  <!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
				<table cellspacing="0" cellpadding="0" width="720" border="0" class="ReportTable">
					<tr>
						<td class="Title" style="HEIGHT: 7px" align="center" width="720">User Security Report</td>
					</tr>
					<tr>
						<td height="20"><font size="2" face="Arial">&nbsp;</font></td>
					</tr>
				</table>
				<table style="COLOR: black" bgcolor="white" class="ReportTable">
					<tr>
						<td></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="720" border="0" class="ReportTable">
					<tr>
						<td width='5'><font size="2" face="Arial">&nbsp;</font></td>
						<td width='75'><font size="2"><b>USER</b></font></td>
						<td width='75'><font size="2"><b>KIND</b></font></td>
						<td width='75'><font size="2"><b>TYPE</b></font></td>
						<td width='295'><font size="2"><b>SECURITY</b></font></td>
					</tr>
					<asp:repeater id="Repeater1" runat="server">
						<ItemTemplate>
							<%# CheckForChanges(Container)%>
							<%# Detail(Container)%>
						</ItemTemplate>
						<FooterTemplate>
							<%# OrderFooter() %>
						</FooterTemplate>
					</asp:repeater></table>
			</div>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
		</form>
	</body>
</html>
