<%@ Register TagPrefix="cc1" Namespace="Cet" Assembly="Cet.ConfirmButton" %>
<%@ Page language="c#" Inherits="KeyCentral.UI.Inputs.InputUsers" Codebehind="InputUsers.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>User Input</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
        <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
        <%--<style type="text/css">
			DIV.scroll { OVERFLOW: auto; WIDTH: 900px; HEIGHT: 600px }
		</style>--%>
		<%--<style type="text/css">DIV.scroll { OVERFLOW: auto;  WIDTH: expression(document.body.clientWidth); HEIGHT: expression(screen.availHeight-(screen.availHeight * .9)) }
		</style>--%>
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
			<script type="text/vbscript" language='VBScript'>
					Function ExistingUser(msg,User_Key,UserName)
						if MsgBox(msg,vbYesNo +vbDefaultButton1) = vbYes then
							ExistingUser = true
						else
							Document.location = "InputUsers.aspx?User_Key=" & User_Key & "&&RecreateUser=true&&UserName=" & UserName
						end if
					End function
			</script>
                <table width="775px">
                    <tr>
                        <td align="Left">
                            <asp:label id="UsersLabel" runat="server" Height="16px" Width="56px" Font-Names="Tahoma" Font-Size="X-Small">Users</asp:label>
                            <asp:TextBox ID="userKey" runat="server" ForeColor="Black" Height="26px" Visible="False" Width="33px"></asp:TextBox>
                            <asp:listbox id="UserList" runat="server" Height="130px" Width="176px" AutoPostBack="True" onselectedindexchanged="UserList_SelectedIndexChanged"></asp:listbox>
                        </td>
                        <td valign="bottom" align="right">
                            <asp:label id="Label4" runat="server" Height="28px" Width="154px" Font-Names="Tahoma" Font-Size="Small">Add/Edit User Name:</asp:label>
                            <asp:label id="Label3" runat="server" Height="30px" Width="154px" Font-Names="Tahoma" Font-Size="Small">Password:</asp:label>
                            <asp:label id="Label2" runat="server" Height="32px" Width="154px" Font-Names="Tahoma" Font-Size="Small">Confirm Password:</asp:label>
                            <asp:label id="Label7" runat="server" Height="24px" Width="154px" Font-Names="Tahoma" Font-Size="Small">Administrator:</asp:label>                
                        </td>
                         <td valign="bottom" align="left">
                            <asp:textbox id="userName" onblur="__doPostBack('userName','')" runat="server" Height="28px" Width="200px" Font-Size="Small" CssClass="text" EnterAsTab="" AutoPostBack="True" ontextchanged="userName_TextChanged"></asp:textbox>
                            <asp:textbox id="password" runat="server" Height="28px" Width="200px" CssClass="text" EnterAsTab="" TextMode="Password"></asp:textbox>
                            <asp:textbox id="password2" onblur="__doPostBack('password2','')" runat="server" Height="28px" Width="200px" AutoPostBack="True" CssClass="text" EnterAsTab="" TextMode="Password" ontextchanged="password2_TextChanged"></asp:textbox>
                            <asp:radiobuttonlist id="AdministratorList" runat="server" Height="28px" Width="200px" AutoPostBack="True" RepeatDirection="Horizontal" onselectedindexchanged="AdministratorList_SelectedIndexChanged"><asp:ListItem Value="Yes">Yes</asp:ListItem><asp:ListItem Value="No" Selected="True">No</asp:ListItem></asp:radiobuttonlist>
                        </td>
                        <td align="Left">
                            <asp:label id="securityTypeLabel" runat="server" Height="16px" Width="88px" Font-Names="Tahoma" Font-Size="X-Small">Security Types</asp:label>
                            <asp:TextBox ID="sTypeKey" runat="server" ForeColor="Black" Height="26px" Visible="False" Width="33px"></asp:TextBox>
                            <asp:listbox id="SecurityTypeList" runat="server" Height="130px" Width="176px" AutoPostBack="True" onselectedindexchanged="SecurityTypeList_SelectedIndexChanged"></asp:listbox>
                        </td>
                    </tr>
                </table>
                <table width="792px">
                    <tr>
                         <td align="center">
                             <cc1:confirmbutton id="deleteUser" runat="server" Height="24px" Width="160px" Text="Delete User" Enabled="False" TabFinish="" onclick="deleteUser_Click"></cc1:confirmbutton>
                        </td>
                    </tr>
                </table>

                <%-- <table width="800px">
                    <tr>
                        <td align="center">
                            <asp:label id="Label14" runat="server" Height="19" Width="792px" Font-Names="Tahoma" Font-Size="Medium">TALK TO BOB BROWN BEFORE YOU CHANGE SECURITY</asp:label>
                        </td>
                    </tr>
                </table>--%>
                <table width="800px">
                    <tr>
                        <td>
                            <asp:label id="Label6" runat="server" Width="168px" Font-Names="Tahoma" Font-Size="X-Small">Security Groups Available</asp:label>
                            <asp:listbox id="availableGroupList" runat="server" Height="120px" Width="300px" Font-Names="Courier New" ForeColor="Black" DataValueField="SType_Group_Key" DataTextField="Description"></asp:listbox>
                        </td>
                        <td>
                            <asp:button id="addGroup" runat="server" Height="24" Width="75px" Font-Names="Tahoma" Text=">" onclick="addGroup_Click"></asp:button>
			                <asp:button id="removeGroup" runat="server" Height="24" Width="75" Text="<" onclick="removeGroup_Click"></asp:button>
			                <asp:button id="addAllGroups" runat="server" Height="24" Width="75px" Text=">>" CausesValidation="False" onclick="addAllGroups_Click"></asp:button>
			                <asp:button id="removeAllGroups" runat="server" Height="24" Width="75" Text="<<" CausesValidation="False" onclick="RemoveAllGroups_Click"></asp:button>
                        </td>
                        <td>
                            <asp:label id="Label5" runat="server" Height="16px" Width="176px" Font-Names="Tahoma" Font-Size="X-Small">Security Groups Selected</asp:label>
                            <asp:listbox id="selectedGroupList" runat="server" Height="120px" Width="300px" Font-Names="Courier New" DataValueField="SType_Group_Key" DataTextField="Description"></asp:listbox>
                        </td>
                    </tr>
                </table>
                <table width="800px">
                    <tr>
                        <td>
                            <asp:label id="availableLabel" runat="server" Width="168px" Font-Names="Tahoma" Font-Size="X-Small">Security Details Available</asp:label>
                            <asp:listbox id="availableDetailList" runat="server" Height="120px" Width="300px" Font-Names="Courier New" DataValueField="SType_Detail_Key" DataTextField="Detail"></asp:listbox>
                        </td>
                        <td>
                            <asp:button id="addDetail" runat="server" Height="24" Width="75px" Font-Names="Tahoma" Text=">" onclick="addDetail_Click"></asp:button>
                            <asp:button id="removeDetail" runat="server" Height="24px" Width="75px" Text="<" onclick="removeDetail_Click"></asp:button>
			                <asp:button id="addAllDetails" runat="server" Height="24px" Width="75px" Text=">>" CausesValidation="False" onclick="addAllDetails_Click"></asp:button>
			                <asp:button id="removeAllDetails" runat="server" Height="24px" Width="75px" Text="<<" CausesValidation="False" onclick="RemoveAllDetails_Click"></asp:button>
                        </td>
			            <td>
                            <asp:label id="selectedLabel" runat="server" Height="16px" Width="176px" Font-Names="Tahoma" Font-Size="X-Small">Security Details Selected</asp:label>
                            <asp:listbox id="selectedDetailList" runat="server" Height="120px" Width="300px" Font-Names="Courier New" DataValueField="SType_Detail_Key" DataTextField="Detail"></asp:listbox>
			            </td>
                    </tr>
                </table>
                <table width="800px">
                    <tr>
                        <td><asp:label id="Label8" runat="server" Height="16px" Width="150px"></asp:label></td>
                        <td><asp:Button id="userSecurityReport" runat="server" Width="160px" Height="24px" Text="User Security Report" onclick="userSecurityReport_Click"></asp:Button></td>
                        <td><asp:Button id="typeSecurityReport" runat="server" Width="160px" Height="24px" Text="Type Security Report" onclick="typeSecurityReport_Click"></asp:Button></td>
                        <td><asp:Button id="groupSecurityReport" runat="server" Width="160px" Height="24px" Text="Group Security Report" onclick="groupSecurityReport_Click"></asp:Button></td>
                        <td><asp:label id="Label9" runat="server" Height="16px" Width="150px"></asp:label></td>
                    </tr>
                    <tr>
                         <td colspan="2"><asp:label id="Label12" runat="server" Height="4px" Width="310px"></asp:label></td>
                    </tr>
                    <tr>
                        <td colspan="2"><asp:label id="Label10" runat="server" Height="16px" Width="310px"></asp:label></td>
                        <td><asp:button id="returnToMenu" runat="server" Width="160px" Height="24px" Text="Return to Menu" onclick="Return_Click"></asp:button></td>
                        <td><asp:label id="Label11" runat="server" Height="16px" Width="150px"></asp:label></td>
                    </tr>
                </table>
                <asp:label id="message" runat="server" ForeColor="Red"></asp:label>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
			</div>
		</form>
	</body>
</html>
