<%@ Register TagPrefix="cc1" Namespace="Cet" Assembly="Cet.ConfirmButton" %>
<%@ Page language="c#" Inherits="KeyCentral.UI.Inputs.InputSecurityType" Codebehind="InputSecurityType.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Security Type</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body bgcolor="white">
	<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
			<cc1:confirmbutton id="deleteSecurityType" style="Z-INDEX: 113; LEFT: 264px; POSITION: absolute; TOP: 176px"
				runat="server" Text="Delete Security Type" Enabled="False" Width="160px" onclick="deleteSecurityType_Click"></cc1:confirmbutton>
			<div style="DISPLAY: inline; Z-INDEX: 106; LEFT: 48px; WIDTH: 168px; POSITION: absolute; TOP: 72px; HEIGHT: 24px"
				align="center"><font face="Tahoma" size="2">Security 
					Types</font></div>
			<asp:label id="Message" style="Z-INDEX: 100; LEFT: 8px; POSITION: absolute; TOP: 344px" runat="server"
				ForeColor="Red"></asp:label><asp:textbox id="STypeKey" style="Z-INDEX: 101; LEFT: 480px; POSITION: absolute; TOP: 152px"
				runat="server" Width="84px" Visible="False"></asp:textbox><asp:listbox id="SecurityTypeList" style="Z-INDEX: 105; LEFT: 8px; POSITION: absolute; TOP: 88px"
				runat="server" Width="251px" AutoPostBack="True" Height="120px" onselectedindexchanged="SecurityTypeList_SelectedIndexChanged"></asp:listbox>
			<div style="DISPLAY: inline; Z-INDEX: 108; LEFT: 272px; WIDTH: 152px; POSITION: absolute; TOP: 128px; HEIGHT: 24px"
				align="right"><font face="Tahoma" size="2">Add New 
					Security Type:</font></div>
			<asp:textbox id="NewSecurityType"  onblur="__doPostBack('NewSecurityType','')" style="Z-INDEX: 109; LEFT: 424px; POSITION: absolute; TOP: 128px"
				tabIndex="1" runat="server" Width="328px" AutoPostBack="True" EnterAsTab="" ontextchanged="NewSecurityType_TextChanged"></asp:textbox><asp:button id="Return" style="Z-INDEX: 111; LEFT: 264px; POSITION: absolute; TOP: 368px" tabIndex="4"
				runat="server" Text="Return to Menu" Width="160px" Height="24px" onclick="Return_Click"></asp:button><cc1:confirmbutton id="deleteSecurityDetail" style="Z-INDEX: 114; LEFT: 264px; POSITION: absolute; TOP: 312px"
				runat="server" Text="Delete Security Detail" Enabled="False" Width="160px" onclick="deleteSecurityDetail_Click"></cc1:confirmbutton>
			<div style="DISPLAY: inline; Z-INDEX: 115; LEFT: 48px; WIDTH: 168px; POSITION: absolute; TOP: 208px; HEIGHT: 24px"
				align="center"><font face="Tahoma" size="2">Security 
					Details</font></div>
			<asp:textbox id="STypeDetail" style="Z-INDEX: 116; LEFT: 480px; POSITION: absolute; TOP: 288px"
				runat="server" Width="84px" Visible="False"></asp:textbox><asp:listbox id="SecurityDetailList" style="Z-INDEX: 117; LEFT: 8px; POSITION: absolute; TOP: 224px"
				tabIndex="2" runat="server" Width="251px" AutoPostBack="True" Height="120px" onselectedindexchanged="SecurityDetailList_SelectedIndexChanged"></asp:listbox>
			<div style="DISPLAY: inline; Z-INDEX: 118; LEFT: 272px; WIDTH: 152px; POSITION: absolute; TOP: 264px; HEIGHT: 24px"
				align="right"><font face="Tahoma" size="2">Add New 
					Security Detail:</font></div>
			<asp:textbox id="NewSecurityDetail" onblur="__doPostBack('NewSecurityDetail','')" style="Z-INDEX: 119; LEFT: 424px; POSITION: absolute; TOP: 264px"
				tabIndex="3" runat="server" Width="328px" AutoPostBack="True" EnterAsTab="" ontextchanged="NewSecurityDetail_TextChanged"></asp:textbox>
				</div>
				<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
				</form>
	</body>
</html>
