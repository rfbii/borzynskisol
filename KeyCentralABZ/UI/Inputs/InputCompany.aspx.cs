using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;

namespace KeyCentral.UI.Inputs
{
	/// <summary>
	/// Summary description for Company.
	/// </summary>
	public partial class InputCompany : KeyCentral.Functions.BasePage
	{
		#region Declares
		#endregion

		#region events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			//Security Check
			if (!this.IsAdmin())
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx");
		}
		protected void companyList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			newCompany.Text = companyList.SelectedItem.Text;
			companyKey.Text = companyList.SelectedItem.Value;
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("select Report_Number from [Company] where Company_Name=@Company_Name", Conn);
			selectCMD.Parameters.Add("@Company_Name",newCompany.Text);
			
			SqlDataReader myReader = selectCMD.ExecuteReader();
			if(myReader.Read()==true) //Existing Company
			{
				oracleReportNo.Text = myReader.GetString(0).PadRight(10).Substring(8,2);
				myReader.Close();
			}
			Conn.Close();
			Conn.Dispose();
			Message.Text = "";
			//Fill ListBoxs
			deleteCompany.Message= "Do you want to delete " + companyList.SelectedItem.Text + " Company?";
			deleteCompany.Enabled=true;
			newCompany.Enabled= true;
			oracleReportNo.Enabled= true;
			newCompany.AutoPostBack= true;
		}
		protected void deleteCompany_Click(object sender, System.EventArgs e)
		{
			SqlConnection Conn = GetSQLConn();
			
			SqlCommand removeCMD = new SqlCommand("delete from [Company] where Company_key = @Company_Key ", Conn);
			removeCMD.Parameters.Add("@Company_Key",companyKey.Text);
			removeCMD.ExecuteNonQuery();
			FillcompanyList();
			companyKey.Text = "";
			newCompany.Text = "";
			oracleReportNo.Text = "";
			Message.Text = "Company Deleted.";	
			deleteCompany.Enabled= false;
			Conn.Close();
			Conn.Dispose();
			newCompany.Enabled= false;
			oracleReportNo.Enabled= false;
		}
		
		private void clearTextFields_Click(object sender, System.EventArgs e)
		{
			FillcompanyList();
			companyKey.Text = "";
			newCompany.Text = "";
			oracleReportNo.Text = "";
			deleteCompany.Enabled= false;
			newCompany.Enabled= false;
			oracleReportNo.Enabled= false;
		}
		protected void newCompany_TextChanged(object sender, System.EventArgs e)
		{
			Message.Text = companyKey.Text;
			if(companyKey.Text.Trim() != "")
			{
				SqlConnection Conn = GetSQLConn();
				SqlCommand updateCMD = new SqlCommand("update [Company] Set Company_Name=@newCompany where Company_Key=@companyKey", Conn);
				updateCMD.Parameters.Add("@newCompany",newCompany.Text);
				updateCMD.Parameters.Add("@companyKey",companyKey.Text);
				
				
				updateCMD.ExecuteNonQuery();
				Message.Text = "Company Name Updated.";	
				FillcompanyList();
				Conn.Close();
				Conn.Dispose();	
			}
		}
	
		protected void oracleReportNo_TextChanged(object sender, System.EventArgs e)
		{
			//Make sure a company has been entered
			if(newCompany.Text == "")
				if(oracleReportNo.Text != "")
			{
				Message.Text = "Please enter a Company Name.";
				return;
			}
			Message.Text = "";
		{
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("select Company_Key,Report_Number from [Company] where Company_Name=@Company_Name", Conn);
			SqlCommand insertCMD = new SqlCommand("insert into [Company] (Company_Name,Report_Number) values(@Company_Name,@Report_Number) Select @@Identity", Conn);
			SqlCommand updateCMD = new SqlCommand("update [Company] Set Report_Number=@Report_Number where Company_Key=@companyKey", Conn);
			selectCMD.Parameters.Add("@Company_Name",newCompany.Text);
			insertCMD.Parameters.Add("@Company_Name",newCompany.Text);
			insertCMD.Parameters.Add("@Report_Number","Company_" + oracleReportNo.Text.Trim());
			updateCMD.Parameters.Add("@companyKey",companyKey.Text);
			updateCMD.Parameters.Add("@Report_Number","Company_" + oracleReportNo.Text.Trim());
			
			SqlDataReader myReader = selectCMD.ExecuteReader();
			if(myReader.Read()==true) //Existing Company
			{
				companyKey.Text = myReader.GetValue(0).ToString();
				if (myReader.GetString(1).ToString()== "Company_" + oracleReportNo.Text.Trim())
				{
					myReader.Close();
					Message.Text = "This Company already exists.";
				}
				else
				{
					myReader.Close();
					updateCMD.ExecuteNonQuery();
					Message.Text = "Report Number Updated.";
					deleteCompany.Enabled=false;		
				}
			
			
			}
			else  //New Company 
			{
				myReader.Close();
				companyKey.Text= insertCMD.ExecuteScalar().ToString();
				Message.Text = "Company Added.";
				newCompany.Text = "";
				oracleReportNo.Text = "";
				FillcompanyList();
				newCompany.Enabled= false;
				oracleReportNo.Enabled= false;
			}
			Conn.Close();
			Conn.Dispose();		
		}
	}	
		protected void addNewCompany_Click(object sender, System.EventArgs e)
		{
			newCompany.Text = "";
			oracleReportNo.Text = "";
			companyKey.Text	= "";
			Message.Text = "";
			deleteCompany.Enabled= false;
			newCompany.Enabled= true;
			oracleReportNo.Enabled= true;
			newCompany.AutoPostBack= false;
		}

		protected void Return_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Setup");			
		}
		#endregion
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			FillcompanyList();
			newCompany.Enabled= false;
			oracleReportNo.Enabled= false;
		}
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
		#region Private helpers
		private void FillcompanyList()
		{
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("select * from [Company] order by Company_Name", Conn);
			
			SqlDataReader myReader = selectCMD.ExecuteReader();
			companyList.DataSource = myReader;
			companyList.DataTextField ="Company_Name";
			companyList.DataValueField = "Company_Key";
			companyList.DataBind();	
			Conn.Close();
			Conn.Dispose();	  		
		}
		#endregion	

		
	}
}
