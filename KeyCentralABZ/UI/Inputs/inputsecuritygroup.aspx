<%@ Register TagPrefix="cc1" Namespace="Cet" Assembly="Cet.ConfirmButton" %>
<%@ Page language="c#" Inherits="KeyCentral.UI.Inputs.InputSecurityGroup" Codebehind="InputSecurityGroup.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Security Group</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body bgcolor="white">
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
			<cc1:confirmbutton id="deleteSTypeGroup" style="Z-INDEX: 114; LEFT: 424px; POSITION: absolute; TOP: 176px"
				runat="server" Text="Delete  Security Group" Enabled="False" Width="160px" onclick="deleteSecurityGroup_Click"></cc1:confirmbutton>
			<asp:Label id="securityGroupLabel" style="Z-INDEX: 117; LEFT: 272px; POSITION: absolute; TOP: 72px"
				runat="server" Width="96px" Height="16px" Font-Names="Tahoma" Font-Size="X-Small">Security Groups</asp:Label>
			<asp:listbox id="SecurityTypeList" style="Z-INDEX: 106; LEFT: 32px; POSITION: absolute; TOP: 88px"
				runat="server" Width="170px" Height="120px" AutoPostBack="True" onselectedindexchanged="SecurityTypeList_SelectedIndexChanged"></asp:listbox><asp:listbox id="availableList" style="Z-INDEX: 105; LEFT: 8px; POSITION: absolute; TOP: 224px"
				runat="server" Width="328px" Height="120px" DataValueField="SType_Detail_Key" Font-Names="Courier New" DataTextField="Detail"></asp:listbox>
			<asp:button id="add" style="Z-INDEX: 102; LEFT: 344px; POSITION: absolute; TOP: 224px" runat="server"
				Text=">" Width="75px" Height="24" Font-Names="Tahoma" onclick="Add_Click"></asp:button><asp:button id="remove" style="Z-INDEX: 104; LEFT: 344px; POSITION: absolute; TOP: 256px" runat="server"
				Text="<" Width="75" Height="24" onclick="Remove_Click"></asp:button><asp:button id="addAll" style="Z-INDEX: 108; LEFT: 344px; POSITION: absolute; TOP: 288px" runat="server"
				Text=">>" Width="75px" Height="24" CausesValidation="False" onclick="AddAll_Click"></asp:button><asp:button id="removeAll" style="Z-INDEX: 103; LEFT: 344px; POSITION: absolute; TOP: 320px"
				runat="server" Text="<<" Width="75" Height="24" CausesValidation="False" onclick="RemoveAll_Click"></asp:button><asp:listbox id="selectedList" style="Z-INDEX: 101; LEFT: 424px; POSITION: absolute; TOP: 224px"
				runat="server" Width="328px" Height="120px" DataValueField="SType_Detail_Key" Font-Names="Courier New" DataTextField="Detail"></asp:listbox><asp:label id="message" style="Z-INDEX: 107; LEFT: 8px; POSITION: absolute; TOP: 344px" runat="server"
				ForeColor="Red"></asp:label><asp:textbox id="sTypeGroupKey" style="Z-INDEX: 109; LEFT: 592px; POSITION: absolute; TOP: 152px"
				runat="server" Width="85" Height="24" Visible="False"></asp:textbox>
			<asp:listbox id="STypeGroupList" style="Z-INDEX: 111; LEFT: 232px; POSITION: absolute; TOP: 88px"
				runat="server" Width="170px" Height="120px" AutoPostBack="True" onselectedindexchanged="STypeGroupList_SelectedIndexChanged"></asp:listbox>
			<asp:textbox id="newSTypeGroup"  onblur="__doPostBack('newSTypeGroup','')" style="Z-INDEX: 112; LEFT: 560px; POSITION: absolute; TOP: 120px"
				runat="server" Width="170px" AutoPostBack="True" ontextchanged="newSTypeGroup_TextChanged"></asp:textbox><asp:button id="returnToMenu" style="Z-INDEX: 113; LEFT: 272px; POSITION: absolute; TOP: 368px"
				runat="server" Text="Return to Menu" Width="160px" Height="24px" onclick="Return_Click"></asp:button>
			<asp:TextBox id="sTypeKey" style="Z-INDEX: 115; LEFT: 536px; POSITION: absolute; TOP: 152px"
				runat="server" Width="85px" Height="24px" Visible="False"></asp:TextBox>
			<asp:Label id="securityTypeLabel" style="Z-INDEX: 116; LEFT: 72px; POSITION: absolute; TOP: 72px"
				runat="server" Width="88px" Height="16px" Font-Names="Tahoma" Font-Size="X-Small">Security Types</asp:Label>
			<asp:Label id="newGroupLabel" style="Z-INDEX: 118; LEFT: 408px; POSITION: absolute; TOP: 120px"
				runat="server" Width="152px" Height="16px" Font-Names="Tahoma" Font-Size="X-Small">Add New Security Group:</asp:Label>
			<asp:Label id="availableLabel" style="Z-INDEX: 119; LEFT: 112px; POSITION: absolute; TOP: 208px"
				runat="server" Width="168px" Font-Names="Tahoma" Font-Size="X-Small">Security Details Available</asp:Label>
			<asp:Label id="selectedLabel" style="Z-INDEX: 120; LEFT: 520px; POSITION: absolute; TOP: 208px"
				runat="server" Width="176px" Height="16px" Font-Names="Tahoma" Font-Size="X-Small">Security Details Selected</asp:Label>
				</div>
				<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
				</form>
	</body>
</html>
