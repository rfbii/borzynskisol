<%@ Register TagPrefix="cc1" Namespace="Cet" Assembly="Cet.ConfirmButton" %>
<%@ Page language="c#" Inherits="KeyCentral.UI.Inputs.InputCompany" Codebehind="InputCompany.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
		<title>Company</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
	<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body bgcolor="white">
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->			
		<div class="scroll" id="BodyDiv">
		    <table>
                <tr>
                    <td align="left" ><font face="Tahoma" size="2">Company</font>
		            </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                      <asp:listbox id="companyList" style="Z-INDEX: 103;"	runat="server" Width="300px" Height="204px" AutoPostBack="True" onselectedindexchanged="companyList_SelectedIndexChanged"></asp:listbox>
                </td>
                <td>
			        <br />
			        <font face="Tahoma" size="2">Add/Edit Company:</font>
			        
			
			<asp:textbox id="newCompany" onblur="__doPostBack('newCompany','')" style="Z-INDEX: 106;"
				tabIndex="1" runat="server" Width="300px" EnterAsTab="" AutoPostBack="True" ontextchanged="newCompany_TextChanged"></asp:textbox>
			<asp:textbox id="companyKey" style="Z-INDEX: 102;" runat="server" Width="100px" Height="19" Visible="False"></asp:textbox><br />
			<asp:label id="Label1" style="Z-INDEX: 109;" runat="server"	Width="134px"><font face="Tahoma" size="2">Oracle Report Number:</font></asp:label>
			<asp:textbox id="oracleReportNo" onblur="__doPostBack('oracleReportNo','')" style="Z-INDEX: 110;" tabIndex="2" runat="server" Width="100px" EnterAsTab="" AutoPostBack="True" ontextchanged="oracleReportNo_TextChanged"></asp:textbox>
			
                        </td>
                    </tr>
                <tr>
                    <td colspan="2">
                        <asp:label id="Message" style="Z-INDEX: 101;" runat="server"
				 Height="19" ForeColor="Red"></asp:label>
                        
                    </td>
                </tr>
             </table>
            <table>
                <tr>
                    <td>
				<cc1:confirmbutton id="deleteCompany" style="Z-INDEX: 108;" tabIndex="3" runat="server" Text="Delete Company" Enabled="False" Width="176" Height="22" onclick="deleteCompany_Click"></cc1:confirmbutton>
                        <asp:Button id="addNewCompany" style="Z-INDEX: 111;" runat="server" Width="144px" Text="Add New Company" onclick="addNewCompany_Click"></asp:Button>
                        <asp:button id="Return" style="Z-INDEX: 107;" tabIndex="5" runat="server" Text="Return to Menu" Width="176" Height="22" onclick="Return_Click"></asp:button>
                    </td>
                </tr>
                </table>
			</div>
		<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
		</form>
	</body>
</html>

