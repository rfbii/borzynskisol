<%@ Page language="c#" Inherits="KeyCentral.UI.Inputs.InputCommodityCategory" Codebehind="InputCommodityCategory.aspx.cs" %>
<%@ Register TagPrefix="cc1" Namespace="Cet" Assembly="Cet.ConfirmButton" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
  <head runat="server">
		<title>Input Commodity-Category</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
  </head>
	<body bgcolor="white">
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
				<table width="800">
					<tbody>
					</tbody>
				</table>
				<table width="800">
					<tr>
						<td width="800" align="center"><div id="TitleLine" runat="server"></div>
						</td>
					</tr>
				</table>
				<table height="20">
					<tr>
						<td></td>
					</tr>
				</table>
				<table width="800">
					<tr>
						<td width="235" align="left"><font face="Tahoma" size="2">Commodity</font></td>
						<td width="565" align="left" colspan="3"><asp:textbox id="commodityKey" runat="server" Width="100px" Height="19" Visible="False"></asp:textbox></td>
					</tr>
					<tr>
						<td width="235" align="left"><asp:listbox id="CommodityList" runat="server" Width="232px" Height="136px" Font-Names="Tahoma" AutoPostBack="True" Enabled="True" onselectedindexchanged="CommodityList_SelectedIndexChanged" tabIndex="1"></asp:listbox></td>
						<td width="565" valign="top" align="left" colspan="3"><asp:label id="commodityName" runat="server" Width="192px" Font-Names="Tahoma" Font-Size="X-Small"></asp:label></td>
					</tr>
					<tr>
						<td align="left"><font face="Tahoma" size="2">Category</font></td>
						<td colspan="3"><asp:textbox id="CategoryKey" runat="server" Width="84px" Visible="False"></asp:textbox></td>
					</tr>
					<tr>
						<td rowspan="4" align="left"><asp:listbox id="CategoryList" runat="server" Width="232px" Height="136px" AutoPostBack="True" tabIndex="2" Enabled="True" onselectedindexchanged="CategoryList_SelectedIndexChanged"></asp:listbox></td>
						<td align="center" colspan="3">&nbsp;</td>
					</tr>
					<tr>
						
					</tr>
					<tr>
						<td align="center" colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td align="left" colspan="3"><cc1:confirmbutton id="deleteCategory" runat="server" Width="160px" Enabled="False" Text="Delete Category" tabIndex="90" onclick="deleteCategory_Click"></cc1:confirmbutton></td>
					</tr>
					<tr>
						<td align="left" colspan="4">
						    <table border="0" cellpadding="0" cellspacing="0">
					            <tr>
					                 <td align="left" width="380">Category:&nbsp;<asp:textbox id="NewCategory" runat="server" Width="300px" Enabled="True" EnterAsTab="" tabIndex="3"></asp:textbox></td>
					                 <td><asp:Button runat="server" Height="22" Width="176" id="Included" Text="Included in Category" Enabled="False" tabIndex="3" OnClick="Included_Click" ></asp:Button></td>
					            </tr>
					            <tr>
						            <td align="left">Size Search Keyword:&nbsp;<asp:textbox id="sizeSearch" runat="server" Width="100px" Enabled="True" EnterAsTab="" tabIndex="4" MaxLength="8"></asp:textbox><br />
						            Grade Search Keyword:&nbsp;<asp:textbox id="gradeSearch" runat="server" Width="100px" Enabled="True" EnterAsTab="" tabIndex="4" MaxLength="8"></asp:textbox><br />
						            <asp:Button runat="server" Height="22" Width="176" id="Save1" Text="Save" Enabled="False" tabIndex="5" onclick="save_Click"></asp:Button>&nbsp;&nbsp;&nbsp;<asp:Button runat="server" Height="22" Width="160" id="newCategoryBtn" Text="New Category" Enabled="False" tabIndex="5" onclick="newCategory_Click"></asp:Button><br />
						            </td>
						            <td><asp:listbox id="IncludedListBox" runat="server" Width="390px" Height="136px" AutoPostBack="False" tabIndex="8" Enabled="True"></asp:listbox></td>
					            </tr>
					        </table>
						</td>
					</tr>
					<tr>
						<td align="left" colspan="4">
						    <table border="0" cellpadding="0" cellspacing="0">
						        <tr>
						            <td align="left" colspan="2" style="BORDER-TOP: black 1px solid" ><asp:Button runat="server" id="sizeProblemsButton" Text="Check For Size Problems" Enabled="False" tabIndex="6" onclick="sizeProblemsButton_Click"></asp:Button></td>
						        </tr>
						        <tr>
						            <td><font face="Tahoma" size="2">Included in Other</font></td>
						            <td><font face="Tahoma" size="2">Multiple Included</font></td>
						        </tr>
						        <tr>
						            <td><asp:listbox id="SizeNotIncludedListBox" runat="server" Width="390px" Height="136px" AutoPostBack="False" tabIndex="7" Enabled="True"></asp:listbox></td>
						            <td><asp:listbox id="MultipleSizesIncludedListBox" runat="server" Width="390px" Height="136px" AutoPostBack="False" tabIndex="8" Enabled="True"></asp:listbox></td>
						        </tr>
						    </table>
						</td>
					</tr>
					<tr>
						<td align="left" colspan="4" height="25"><asp:label id="Message" runat="server" ForeColor="Red"></asp:label></td>
					</tr>
				</table>
				<table width="800">
					<tr>
						<td align="right"><asp:button id="save" runat="server" Height="22" Width="176" Text="Save" EnterAsTab="" Visible="True" onclick="save_Click" tabIndex="9"></asp:button></td>
						<td align="center"><asp:button id="cancel" runat="server" Width="160px" Enabled="True" Text="Cancel" EnterAsTab="" tabIndex="10" onclick="cancel_Click"></asp:button></td>
						<td align="left"><asp:button id="Return" tabIndex="10" runat="server" Width="160px" Text="Return to Menu" Height="24px" EnterAsTab="" tabstop="" onclick="Return_Click"></asp:button></td>
					</tr>
				</table>
				<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
			</div>
		</form>
	</body>
</html>
