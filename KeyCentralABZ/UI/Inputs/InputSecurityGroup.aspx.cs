using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;

namespace KeyCentral.UI.Inputs
{
	/// <summary>
	/// Summary description for Security Group.
	/// </summary>
	public partial class InputSecurityGroup : KeyCentral.Functions.BasePage
	{
		#region Declares

		protected Cet.ConfirmButton deleteSecurityGroup;
		#endregion

		#region events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			//Security Check
			if (!this.IsAdmin())
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx");			
		}
		protected void SecurityTypeList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			sTypeKey.Text = SecurityTypeList.SelectedItem.Value;
			FillSecurityGroupList();
			EnableScreen();
			availableList.DataSource = "";
			availableList.DataBind();
			selectedList.DataSource = "";
			selectedList.DataBind();
			newSTypeGroup.Text = "";
		}
		
		protected void STypeGroupList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			sTypeGroupKey.Text = STypeGroupList.SelectedItem.Value;
			//Fill ListBoxs
			SqlConnection Conn = GetSQLConn();

			DataBindLists(Conn,sTypeGroupKey.Text,sTypeKey.Text);
			newSTypeGroup.Text = STypeGroupList.SelectedItem.Text;
			EnableAddRemove();
			Conn.Close();
			Conn.Dispose();
			deleteSTypeGroup.Message= "Do you want to delete " + STypeGroupList.SelectedItem.Text + " Security Group?";
			deleteSTypeGroup.Enabled=true;
		}
		protected void newSTypeGroup_TextChanged(object sender, System.EventArgs e)
		{
			//Make sure a Security Group has been entered
			if(newSTypeGroup.Text.Trim() == "")
			{				
				availableList.DataSource = "";
				availableList.DataBind();
				selectedList.DataSource = "";
				selectedList.DataBind();
				message.Text = "Please enter a Security Group.";
				return;
			}
			message.Text = "";
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("select SType_Group_Key from [SType_Group] where Description=@Description and SType_Key =@STypeKey ", Conn);
			SqlCommand insertCMD = new SqlCommand("insert into [SType_Group] (SType_Key,Description) values(@STypeKey,@Description) Select @@Identity", Conn);
			selectCMD.Parameters.Add("@Description",newSTypeGroup.Text.ToLower());
			selectCMD.Parameters.Add("@STypeKey",sTypeKey.Text);
			insertCMD.Parameters.Add("@Description",newSTypeGroup.Text);
			insertCMD.Parameters.Add("@STypeKey",sTypeKey.Text);
			
			SqlDataReader myReader = selectCMD.ExecuteReader();
			if(myReader.Read()==true) //Existing Group
			{
				sTypeGroupKey.Text = myReader.GetValue(0).ToString();
				myReader.Close();
				//Fill ListBoxs
				DataBindLists(Conn,sTypeGroupKey.Text,sTypeKey.Text);
				message.Text = "Security Group already exists.";
				EnableAddRemove();	
			}
			else  //New Security Group
			{
				myReader.Close();
				sTypeGroupKey.Text = insertCMD.ExecuteScalar().ToString();
				message.Text = "Security Group Added.";
				DataBindLists(Conn,sTypeGroupKey.Text,sTypeKey.Text);
				EnableAddRemove();
				FillSecurityGroupList();
			}
			
			Conn.Close();
			Conn.Dispose();		
		}

		protected void Add_Click(object sender, System.EventArgs e)
		{			
			if(availableList.SelectedIndex!=-1)
			{
				SqlConnection Conn = GetSQLConn();
				SqlCommand insertCMD = new SqlCommand("insert into [SType_Group_Detail] (SType_Group_Key,SType_Detail_Key) values(@SType_Group_Key,@SType_Detail_Key)", Conn);
				insertCMD.Parameters.Add("@SType_Group_Key",sTypeGroupKey.Text);
				insertCMD.Parameters.Add("@SType_Detail_Key",availableList.SelectedItem.Value);		
				
				insertCMD.ExecuteNonQuery();
				DataBindLists(Conn,sTypeGroupKey.Text,sTypeKey.Text);				
				message.Text = "Security Detail Added.";
				Conn.Close();
				Conn.Dispose();
			}
			else
			{
				message.Text = "Please select a Security Detail to add to this Security Group.";
			}			
		}

		protected void Remove_Click(object sender, System.EventArgs e)
		{
			if(selectedList.SelectedIndex!=-1)
			{
				SqlConnection Conn = GetSQLConn();
				SqlCommand removeCMD = new SqlCommand("delete from [SType_Group_Detail] where SType_Group_Key = @SType_Group_Key and SType_Detail_Key =@SType_Detail_Key ", Conn);
				removeCMD.Parameters.Add("@SType_Group_Key",sTypeGroupKey.Text);
				removeCMD.Parameters.Add("@SType_Detail_Key",selectedList.SelectedItem.Value);		
				
				removeCMD.ExecuteNonQuery();
				DataBindLists(Conn,sTypeGroupKey.Text,sTypeKey.Text);
				message.Text = "Security Detail Removed.";				
				Conn.Close();
				Conn.Dispose();
			}
			else
			{
				message.Text = "Please select a Security Detail to remove from this Security Group.";
			}
		}

		protected void AddAll_Click(object sender, System.EventArgs e)
		{	
			SqlConnection Conn = GetSQLConn();
			SqlCommand insertCMD = new SqlCommand("INSERT INTO SType_Group_Detail  (SType_Group_Key,SType_Detail_Key ) SELECT @SType_Group_Key,SType_Detail_Key  FROM SType_Detail Where SType_Key = @STypeKey", Conn);
			insertCMD.Parameters.Add("@SType_Group_Key",sTypeGroupKey.Text);
			insertCMD.Parameters.Add("@STypeKey",sTypeKey.Text);
			
			RemoveAllDetails(Conn ,sTypeGroupKey.Text);
			insertCMD.ExecuteNonQuery();
			DataBindLists(Conn,sTypeGroupKey.Text,sTypeKey.Text);				
			message.Text = "All Security Details Added.";
			Conn.Close();
			Conn.Dispose();		
		}

		protected void RemoveAll_Click(object sender, System.EventArgs e)
		{
			SqlConnection Conn = GetSQLConn();

			RemoveAllDetails(Conn ,sTypeGroupKey.Text);
			DataBindLists(Conn,sTypeGroupKey.Text,sTypeKey.Text);
			message.Text = "All Security Details Removed.";				
			Conn.Close();
			Conn.Dispose();				
		}
		
		protected void Return_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Setup/Security");			
		}
		protected void deleteSecurityGroup_Click(object sender, System.EventArgs e)
		{
			SqlConnection Conn = GetSQLConn();
			

			//Remove User SType Group 
			SqlCommand removeCMDUserGroup = new SqlCommand("delete from [User_SType_Group] where SType_Group_Key In (Select  SType_Group_Key from [SType_Group] where SType_Group.SType_Group_Key=@SType_Group_Key) ", Conn);
			removeCMDUserGroup.Parameters.Add("@SType_Group_Key",sTypeGroupKey.Text);
			removeCMDUserGroup.ExecuteNonQuery();
			
			RemoveAllDetails(Conn ,sTypeGroupKey.Text);
			SqlCommand removeCMD = new SqlCommand("delete from [SType_Group] where SType_Group_Key = @SType_Group_Key ", Conn);
			removeCMD.Parameters.Add("@SType_Group_Key",sTypeGroupKey.Text);
			removeCMD.ExecuteNonQuery();
			DataBindLists(Conn,sTypeGroupKey.Text,sTypeKey.Text);
			FillSecurityGroupList();			
			availableList.DataSource = "";
			availableList.DataBind();
			selectedList.DataSource = "";
			selectedList.DataBind();
			newSTypeGroup.Text = "";
			message.Text = "Security Group Deleted.";	
			deleteSTypeGroup.Enabled= false;
		}

		#endregion
			
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			FillSecurityTypeList();
			DisableScreen();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Private helpers
		private void DataBindLists(SqlConnection Conn ,string SecurityKey,string TypeKey)
		{
			SqlConnection SConn = GetSQLConn();
			
			SqlCommand selectCMD = new SqlCommand("select SType_Key,Description from [SType] where SType_Key=@TypeKey", Conn);
			selectCMD.Parameters.Add("@TypeKey",sTypeKey.Text);
			SqlDataReader myReader2 = selectCMD.ExecuteReader();
			myReader2.Read(); 			
				switch(myReader2.GetString(1).ToString().ToLower())
				{
					case "grower":
						myReader2.Close();
						DataBindSecuritysIncluded(Conn,SecurityKey,TypeKey);
						DataBindSecuritysNotIncluded(Conn,SecurityKey,TypeKey);
						KeyCentral.Functions.Misc.GetGrowerNames(Session,this.selectedList,this.availableList);
						break;
					case "warehouse":
						myReader2.Close();
						DataBindSecuritysIncluded(Conn,SecurityKey,TypeKey);
						DataBindSecuritysNotIncluded(Conn,SecurityKey,TypeKey);
						KeyCentral.Functions.Misc.GetWarehouseNames(Session,this.selectedList,this.availableList);
						break;
					default:
						myReader2.Close();
						message.Text = "";
						DataBindSecuritysIncluded(Conn,SecurityKey,TypeKey);
						DataBindSecuritysNotIncluded(Conn,SecurityKey,TypeKey);
						break;
				}
			SConn.Close();
			SConn.Dispose();						
		}
		private void DataBindSecuritysIncluded(SqlConnection Conn ,string SecurityKey,string TypeKey)
		{
			SqlCommand selectCMD = new SqlCommand("select SType_Detail.Detail,SType_Detail.SType_Detail_Key from [SType_Group_Detail] join [SType_Detail] on SType_Detail.SType_Detail_Key = SType_Group_Detail.SType_Detail_Key where SType_Group_Key=@SecurityGroupKey and SType_Detail.SType_Key = @TypeKey Order by Detail", Conn);
			selectCMD.Parameters.Add("@SecurityGroupKey",SecurityKey);
			selectCMD.Parameters.Add("@TypeKey",TypeKey);
			
			SqlDataReader myReader = selectCMD.ExecuteReader();
			selectedList.DataSource = myReader;
			selectedList.DataBind();

			myReader.Close();

			//Handle All Warehouses/Growers
			if(selectedList.Items.Count > 0)
			{
				selectCMD = new SqlCommand("select SType.Description from Stype where SType_Key=@SType_Key", Conn);
				selectCMD.Parameters.Add("@SType_Key",TypeKey);

				myReader = selectCMD.ExecuteReader();
				if(myReader.Read()==true)
				{
					switch(myReader.GetString(0).ToString().ToLower())
					{
						case "grower":
							myReader.Close();
							if(selectedList.Items[0].Text == "-1")
								selectedList.Items[0].Text = "ALL GROWERS";
							break;
						case "warehouse":
							myReader.Close();
							if(selectedList.Items[0].Text == "-1")
								selectedList.Items[0].Text = "ALL WAREHOUSES";
							break;
						default:
							myReader.Close();
							break;
					}
				}
			}
		}
		private void DataBindSecuritysNotIncluded(SqlConnection Conn ,string SecurityKey,string TypeKey)
		{
			SqlCommand selectCMD = new SqlCommand("select SType_Detail.Detail,SType_Detail.SType_Detail_key from [SType_Detail] where SType_Detail.SType_Key = @TypeKey and SType_Detail.SType_Detail_key not in (select SType_Detail_key from [SType_Group_Detail] where SType_Group_Key=@STypeGroupKey) order by Detail" , Conn);
			selectCMD.Parameters.Add("@STypeGroupKey",SecurityKey);
			selectCMD.Parameters.Add("@TypeKey",TypeKey);
			SqlDataReader myReader = selectCMD.ExecuteReader();
			availableList.DataSource = myReader;			
			availableList.DataBind();
			myReader.Close();

			//Handle All Warehouses/Growers
			if(availableList.Items.Count > 0)
			{
				selectCMD = new SqlCommand("select SType.Description from Stype where SType_Key=@SType_Key", Conn);
				selectCMD.Parameters.Add("@SType_Key",TypeKey);

				myReader = selectCMD.ExecuteReader();
				if(myReader.Read()==true)
				{
					switch(myReader.GetString(0).ToString().ToLower())
					{
						case "grower":
							myReader.Close();
							if(availableList.Items[0].Text == "-1")
								availableList.Items[0].Text = "ALL GROWERS";
							break;
						case "warehouse":
							myReader.Close();
							if(availableList.Items[0].Text == "-1")
								availableList.Items[0].Text = "ALL WAREHOUSES";
							break;
						default:
							myReader.Close();
							break;
					}
				}
			}
		}
		private void RemoveAllDetails(SqlConnection Conn ,string SecurityKey)
		{
			SqlCommand removeCMD = new SqlCommand("delete from [SType_Group_Detail] where SType_group_key = @Security_Group_Key ", Conn);
			removeCMD.Parameters.Add("@Security_Group_Key",sTypeGroupKey.Text);
			removeCMD.ExecuteNonQuery();
		}
		private void FillSecurityTypeList()
		{
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("select * from [SType] order by description", Conn);
		
			SqlDataReader myReader = selectCMD.ExecuteReader();
			if(myReader.HasRows ==true)
			{			 
				SecurityTypeList.DataSource = myReader;
				SecurityTypeList.DataTextField = "Description";
				SecurityTypeList.DataValueField = "SType_Key";
				SecurityTypeList.DataBind();
			}
			Conn.Close();
			Conn.Dispose();	  		
					
		}	
		private void FillSecurityGroupList()
		{
			STypeGroupList.DataSource ="";
			STypeGroupList.DataBind();
	
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("select * from [SType_Group] Where SType_Key=@STypeKey order by description", Conn);
			selectCMD.Parameters.Add("@STypeKey",sTypeKey.Text);
			
			SqlDataReader myReader = selectCMD.ExecuteReader();
			
			if(myReader.HasRows ==true)
			{			
				STypeGroupList.DataSource = myReader;
				STypeGroupList.DataTextField = "Description";
				STypeGroupList.DataValueField = "SType_Group_Key";
				STypeGroupList.DataBind();
	
			}
			Conn.Close();
			Conn.Dispose();	  		
					
		}
		private void DisableScreen()
		{
			STypeGroupList.Enabled = false;
			newSTypeGroup.Enabled = false;
			availableList.Enabled = false;
			selectedList.Enabled = false;
			add.Enabled = false;
			remove.Enabled = false;
			addAll.Enabled = false;
			removeAll.Enabled = false;
			securityGroupLabel.Enabled = false;
			newGroupLabel.Enabled = false;
			availableLabel.Enabled = false;
			selectedLabel.Enabled = false;
		}
		private void EnableScreen()
		{
			STypeGroupList.Enabled = true;
			newSTypeGroup.Enabled = true;
			availableList.Enabled = true;
			selectedList.Enabled = true;
			securityGroupLabel.Enabled = true;
			newGroupLabel.Enabled = true;
			availableLabel.Enabled = true;
			selectedLabel.Enabled = true;
		}	
		private void DisableAddRemove()
		{
			add.Enabled = false;
			remove.Enabled = false;
			addAll.Enabled = false;
			removeAll.Enabled = false;
		}

		private void EnableAddRemove()
		{
			add.Enabled = true;
			remove.Enabled = true;
			addAll.Enabled = true;
			removeAll.Enabled = true;
		}
		#endregion			
	}
}
