using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;

namespace KeyCentral.UI.Inputs
{
	/// <summary>
	/// Summary description for Security Type.
	/// </summary>
	public partial class InputSecurityType : KeyCentral.Functions.BasePage
	{
		#region Declares

		#endregion

		#region events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			//Security Check
			if (!this.IsAdmin())
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx");
		}
		protected void SecurityTypeList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			STypeKey.Text = SecurityTypeList.SelectedItem.Value;
			NewSecurityType.Text = SecurityTypeList.SelectedItem.Text.ToString();
			deleteSecurityType.Message= "Do you want to delete " + SecurityTypeList.SelectedItem.Text + "  Security Type?";
			deleteSecurityType.Enabled=true;
			EnableDetail();
			FillSecurityDetailList();
		}
		protected void SecurityDetailList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			STypeDetail.Text = SecurityDetailList.SelectedItem.Value;
			deleteSecurityDetail.Message= "Do you want to delete " + SecurityDetailList.SelectedItem.Text + "  Security Detail?";
			deleteSecurityDetail.Enabled=true;
		}
		
		protected void NewSecurityType_TextChanged(object sender, System.EventArgs e)
		{
			//Make sure a  Security Type has been entered
			if(NewSecurityType.Text.Trim() == "")
			{				
				Message.Text = "Please enter a  Security Type.";
				return;
			}
			if(NewSecurityType.Text.ToLower().Trim() == "grower" ||NewSecurityType.Text.ToLower().Trim() == "growers")
			 {				
				 Message.Text = "Please use the Famous Security Type screen to select Growers.";
				 return;
			 }			
			if(NewSecurityType.Text.ToLower().Trim() == "warehouse" || NewSecurityType.Text.ToLower().Trim() == "warehouses")
			{				
				Message.Text = "Please use the Famous Security Type screen to select Warehouses.";
				return;
			}			
			Message.Text = "";
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("select SType_Key from [SType] where Description=@Description", Conn);
			SqlCommand insertCMD = new SqlCommand("insert into [SType] (Description) values(@Description) Select @@Identity", Conn);
			selectCMD.Parameters.Add("@Description",NewSecurityType.Text.ToLower());
			insertCMD.Parameters.Add("@Description",NewSecurityType.Text);
			
			SqlDataReader myReader = selectCMD.ExecuteReader();
			if(myReader.Read()==true) //Existing Header
			{
				STypeKey.Text = myReader.GetValue(0).ToString();
				myReader.Close();
				//Fill ListBoxs
				Message.Text = "This  Security Type already exists.";					
			}
			else  //New  Security Type
			{
				EnableDetail();
				myReader.Close();
				STypeKey.Text = insertCMD.ExecuteScalar().ToString();
				Message.Text = " Security Type Added.";
				EnableDetail();
				FillSecurityTypeList();
				SecurityDetailList.DataSource = "";
				SecurityDetailList.DataBind();					
			}
			
			Conn.Close();
			Conn.Dispose();		
		}
		protected void NewSecurityDetail_TextChanged(object sender, System.EventArgs e)
		{
			//Make sure a  Security Detail has been entered
			if(NewSecurityDetail.Text.Trim() == "")
			{				
				Message.Text = "Please enter a  Security Detail.";
				return;
			}
			Message.Text = "";
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("select SType_Detail_Key from [SType_Detail] where Detail=@Detail and SType_Key=@STypeKey", Conn);
			SqlCommand insertDetailCMD = new SqlCommand("insert into [SType_Detail] (SType_Key,Detail) values(@STypeKey,@Detail) Select @@Identity", Conn);
			selectCMD.Parameters.Add("@Detail",NewSecurityDetail.Text.ToLower());
			selectCMD.Parameters.Add("@STypeKey",STypeKey.Text.ToLower());
			insertDetailCMD.Parameters.Add("@Detail",NewSecurityDetail.Text);
			insertDetailCMD.Parameters.Add("@STypeKey",STypeKey.Text);
				
			SqlDataReader myReader = selectCMD.ExecuteReader();
			if(myReader.Read()==true) //Existing Header
			{
				STypeDetail.Text = myReader.GetValue(0).ToString();
				myReader.Close();
				//Fill ListBoxs
				Message.Text = "This  Security Detail already exists.";					
			}
			else  //New  Security Detail
			{
				myReader.Close();
				STypeDetail.Text = insertDetailCMD.ExecuteScalar().ToString();
				Message.Text = " Security Detail Added.";
				NewSecurityDetail.Text = "";
				FillSecurityDetailList();
			}
			
			Conn.Close();
			Conn.Dispose();		
		}
	
		protected void Return_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Setup/Security");			
		}
		protected void deleteSecurityType_Click(object sender, System.EventArgs e)
		{
			SqlConnection Conn = GetSQLConn();
			
			//Remove User SType  
			SqlCommand removeCMDUser = new SqlCommand("delete from [User_SType] where SType_Detail_Key In (Select  SType_Detail_Key from [SType_detail] where SType_Detail.SType_Key=@SType_Key) ", Conn);
			removeCMDUser.Parameters.Add("@SType_Key",STypeKey.Text);
			removeCMDUser.ExecuteNonQuery();
			
			//Remove User SType Group 
			SqlCommand removeCMDUserGroup = new SqlCommand("delete from [User_SType_Group] where SType_Group_Key In (Select  SType_Group_Key from [SType_Group] where SType_Group.SType_Key=@SType_Key) ", Conn);
			removeCMDUserGroup.Parameters.Add("@SType_Key",STypeKey.Text);
			removeCMDUserGroup.ExecuteNonQuery();
			
			//Remove SType Group Detail
			SqlCommand removeCMDGroupDetail = new SqlCommand("delete from [SType_Group_Detail] where SType_Group_Key In (Select  SType_Group_Key from [SType_Group] where SType_Group.SType_Key=@SType_Key) ", Conn);
			removeCMDGroupDetail.Parameters.Add("@SType_Key",STypeKey.Text);
			removeCMDGroupDetail.ExecuteNonQuery();
			
			//Remove SType Group
			SqlCommand removeCMDGroup = new SqlCommand("delete from [SType_Group] where SType_key = @SType_Key ", Conn);
			removeCMDGroup.Parameters.Add("@SType_Key",STypeKey.Text);
			removeCMDGroup.ExecuteNonQuery();
			
			//Remove SType Detail
			SqlCommand removeCMDDetail = new SqlCommand("delete from [SType_Detail] where SType_key = @SType_Key ", Conn);
			removeCMDDetail.Parameters.Add("@SType_Key",STypeKey.Text);
			removeCMDDetail.ExecuteNonQuery();

			//Remove SType 
			SqlCommand removeCMD = new SqlCommand("delete from [SType] where SType_key = @SType_Key ", Conn);
			removeCMD.Parameters.Add("@SType_Key",STypeKey.Text);
			removeCMD.ExecuteNonQuery();
			NewSecurityType.Text = "";
			FillSecurityTypeList();
			FillSecurityDetailList();
			Message.Text = " Security Type Deleted.";	
			deleteSecurityType.Enabled= false;
		}
		protected void deleteSecurityDetail_Click(object sender, System.EventArgs e)
		{
			SqlConnection Conn = GetSQLConn();
			

			//Remove User SType 
			SqlCommand removeCMDUser = new SqlCommand("delete from [User_SType] where SType_Detail_Key =@SType_Detail ", Conn);
			removeCMDUser.Parameters.Add("@SType_Detail",STypeDetail.Text);
			removeCMDUser.ExecuteNonQuery();
			
			//Remove SType Group Detail
			SqlCommand removeCMDGroupDetail = new SqlCommand("delete from [SType_Group_Detail] where SType_Detail_Key =@SType_Detail ", Conn);
			removeCMDGroupDetail.Parameters.Add("@SType_Detail",STypeDetail.Text);
			removeCMDGroupDetail.ExecuteNonQuery();
			
			// Remove SType Detail
			SqlCommand removeCMD = new SqlCommand("delete from [SType_Detail] where SType_Detail_key = @SType_Detail ", Conn);
			removeCMD.Parameters.Add("@SType_Detail",STypeDetail.Text);
			removeCMD.ExecuteNonQuery();
			FillSecurityDetailList();
			Message.Text = " Security Detail Deleted.";	
			deleteSecurityDetail.Enabled= false;
		}

		#endregion
			
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			FillSecurityTypeList();
			DisableDetail();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Private helpers
		private void FillSecurityTypeList()
		{
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("select * from [SType] where Description != 'Grower' and Description <>'Warehouse' order by description", Conn);
		
			SqlDataReader myReader = selectCMD.ExecuteReader();
			
			if(myReader.HasRows ==true)
			{				 
				SecurityTypeList.DataSource = myReader;
				SecurityTypeList.DataTextField = "Description";
				SecurityTypeList.DataValueField = "SType_Key";
				SecurityTypeList.DataBind();	
			}
			Conn.Close();
			Conn.Dispose();	  		
					
		}	
		private void FillSecurityDetailList()
			{
				SqlConnection Conn = GetSQLConn();
				SqlCommand selectCMD = new SqlCommand("select * from [SType_Detail] where SType_Key=@STypeKey", Conn);
				selectCMD.Parameters.Add("@STypeKey",STypeKey.Text.ToLower());
				
				SqlDataReader myReader = selectCMD.ExecuteReader();
			 
				SecurityDetailList.DataSource = myReader;
				SecurityDetailList.DataTextField = "Detail";
				SecurityDetailList.DataValueField = "SType_Detail_Key";
				SecurityDetailList.DataBind();	
				Conn.Close();
				Conn.Dispose();	  		
					
		}	
		private void DisableDetail()
		{
			NewSecurityDetail.Enabled = false;
			SecurityDetailList.Enabled = false;					
		}
		private void EnableDetail()
		 {
			 NewSecurityDetail.Enabled = true;
			 SecurityDetailList.Enabled = true;					
		 }
		
		#endregion	
		
	}
}
