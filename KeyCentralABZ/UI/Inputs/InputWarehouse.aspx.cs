using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb; 
using Oracle.DataAccess.Client;
	

namespace KeyCentral.UI.Inputs
{
	/// <summary>
	/// Summary description for Security Type.
	/// </summary>
	public partial class InputWarehouse : KeyCentral.Functions.BasePage
	{
		#region Declares

		protected string warehouseName = "";
		
		#endregion

		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			//Security Check
			if (!this.IsAdmin())
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx");			
		}
		protected void Add_Click(object sender, System.EventArgs e)
		{			
			if(availableList.SelectedIndex!=-1)
			{
				SqlConnection Conn = GetSQLConn();
				SqlCommand insertCMD = new SqlCommand("insert into [SType_Detail] (SType_Key,Detail) values(@STypeKey,@Detail)" , Conn);
				insertCMD.Parameters.Add("@STypeKey",sTypeKey.Text);
				insertCMD.Parameters.Add("@Detail",availableList.SelectedItem.Value);		

				insertCMD.ExecuteNonQuery();
				message.Text = "Warehouse Added.";
				FillSelectedList();
				FillAvailableList();
				Conn.Close();
				Conn.Dispose();
			}
			else
			{
				message.Text = "Please select a Warehouse to add.";
			}			
		}
		protected void Remove_Click(object sender, System.EventArgs e)
		{
			if(selectedList.SelectedIndex!=-1)
			{
				if(selectedList.SelectedIndex!=0)
				{
					SqlConnection Conn = GetSQLConn();
				
					message.Text = selectedList.SelectedItem.Value;
					//Remove entries from SType_Group_Detail where this is a detail.
					SqlCommand removeCMDGroupDetail = new SqlCommand("delete from [SType_Group_Detail] where SType_Detail_Key = @SType_Detail_Key", Conn);
					removeCMDGroupDetail.Parameters.Add("@SType_Detail_Key",selectedList.SelectedItem.Value);
					removeCMDGroupDetail.ExecuteNonQuery();
				
					//Remove entries from User_SType where this is a detail.
					SqlCommand removeCMDUserDetail = new SqlCommand("delete from [User_SType] where SType_Detail_Key = @SType_Detail_Key", Conn);
					removeCMDUserDetail.Parameters.Add("@SType_Detail_Key",selectedList.SelectedItem.Value);
					removeCMDUserDetail.ExecuteNonQuery();
			
					//Remove entry from SType_Detail.
					SqlCommand removeCMD = new SqlCommand("delete from [SType_Detail] where SType_Key = @SType_Key and SType_Detail_Key =@SType_Detail_Key", Conn);
					removeCMD.Parameters.Add("@SType_Key",sTypeKey.Text);
					removeCMD.Parameters.Add("@SType_Detail_Key",selectedList.SelectedItem.Value);		
					removeCMD.ExecuteNonQuery();
					message.Text = "Warehouse Removed.";
					FillSelectedList();
					FillAvailableList();
					Conn.Close();
					Conn.Dispose();
				}
				else
				{
					message.Text = "You cannot remove \"ALL WAREHOUSES\".";
				}
			}
			else
			{
				message.Text = "Please select a Warehouse to remove.";
			}
		}
		protected void addAll_Click(object sender, System.EventArgs e)
		{	
			FillSelectedList();
			
			SqlConnection Conn = GetSQLConn();
			
					
			OracleConnection OracleConn = GetOracleConn();
			OracleCommand OracleData = new OracleCommand(FormatOracle("Select Ic_Warehouse.WarehouseIdx,Fc_Name_Location.Descr From #COMPANY_NUMBER#.Ic_Warehouse, #COMPANY_NUMBER#.Fc_Name_Location Where Ic_Warehouse.NameIdx = Fc_Name_Location.NameIdx and Ic_Warehouse.NameLocationSeq = Fc_Name_Location.NameLocationSeq Order by Descr"),OracleConn);
		
			DataSet dsRet = new DataSet();
			OracleData.BindByName = true;
			//DataSet dsRet = new DataSet();
			int x=0;
	
			availableList.Items.Clear();
			Oracle.DataAccess.Client.OracleDataReader myReader =OracleData.ExecuteReader();
			while (myReader.Read()) 
			{
				x= KeyCentral.Functions.Misc.TextIsInListBox(this.selectedList,myReader.GetValue(0).ToString());
				if (x==-1)
				{
					selectedList.Items.Add( new ListItem(myReader.GetString(1),myReader.GetValue(0).ToString()));
					SqlCommand insertCMD = new SqlCommand("insert into [SType_Detail] (SType_Key,Detail) values(@STypeKey,@Detail)" , Conn);
					insertCMD.Parameters.Add("@STypeKey",sTypeKey.Text);
					insertCMD.Parameters.Add("@Detail",myReader.GetValue(0));		
					insertCMD.ExecuteNonQuery();
				}
				else
				{
					selectedList.Items.Add(new ListItem(myReader.GetString(1),selectedList.Items[x].Value));
					selectedList.Items.Remove(selectedList.Items[x]);
				}
			}
			message.Text = "All Warehouses Added.";
			myReader.Close();
			myReader.Dispose();
			OracleData.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();
			Conn.Close();				
		}

		protected void removeAll_Click(object sender, System.EventArgs e)
		{
			SqlConnection Conn = GetSQLConn();
			
			RemoveAllDetails(Conn ,sTypeKey.Text);
			FillSelectedList();
			FillAvailableList();
			message.Text = "All Warehouses Removed.";				
			Conn.Close();
			Conn.Dispose();				
		}
		
		protected void Return_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Setup/Security/Famous Security Type");			
		}
		#endregion
			
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			addWarehouseSType();
			FillSelectedList();
			FillAvailableList();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
		
		#region Private helpers
		private void addWarehouseSType()
		{ 
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("select SType_Key from [SType] where Description='Warehouse'", Conn);
			SqlCommand insertCMD = new SqlCommand("insert into [SType] (Description) values('Warehouse') Select @@Identity", Conn);
			
			SqlDataReader myReader = selectCMD.ExecuteReader();
			if(myReader.Read()==true)
			{
				sTypeKey.Text = myReader.GetValue(0).ToString();
				myReader.Close();
			}
			else
			{
				myReader.Close();
				sTypeKey.Text = insertCMD.ExecuteScalar().ToString();
			}			
			Conn.Close();
			Conn.Dispose();
		}
		private void FillAvailableList()
		{
			OracleConnection OracleConn = GetOracleConn();
			OracleCommand OracleData = new OracleCommand(FormatOracle("Select Ic_Warehouse.WarehouseIdx,Fc_Name_Location.Descr From #COMPANY_NUMBER#.Ic_Warehouse, #COMPANY_NUMBER#.Fc_Name_Location Where Ic_Warehouse.NameIdx = Fc_Name_Location.NameIdx and Ic_Warehouse.NameLocationSeq = Fc_Name_Location.NameLocationSeq Order by Descr"), OracleConn);
			
			DataSet dsRet = new DataSet();
			int x=0;

			OracleData.BindByName = true;
		
			availableList.Items.Clear();
			Oracle.DataAccess.Client.OracleDataReader myReader =OracleData.ExecuteReader();
			while (myReader.Read()) 
			{
				x= KeyCentral.Functions.Misc.TextIsInListBox(this.selectedList,myReader.GetValue(0).ToString());
				if (x==-1)
					availableList.Items.Add( new ListItem(myReader.GetString(1),myReader.GetValue(0).ToString()));
				else
				{
					selectedList.Items.Add(new ListItem(myReader.GetString(1),selectedList.Items[x].Value));
					selectedList.Items.Remove(selectedList.Items[x]);
				}
			}
			myReader.Close();
			myReader.Dispose();
			OracleData.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();
		}

		private void FillSelectedList()
		{
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("select * from [SType_Detail] where SType_Key=@STypeKey Order By Detail", Conn);
			selectCMD.Parameters.Add("@STypeKey",sTypeKey.Text.ToLower());

			SqlDataReader myReader = selectCMD.ExecuteReader();

			selectedList.DataSource = myReader;
			selectedList.DataTextField = "Detail";
			selectedList.DataValueField = "SType_Detail_Key";
			selectedList.DataBind();
			Conn.Close();
			Conn.Dispose();

			//Handle All Warehouses
			if(selectedList.Items.Count > 0)
			{
				if(selectedList.Items[0].Text == "-1")
					selectedList.Items[0].Text = "ALL WAREHOUSES";
			}
		}	
		private void RemoveAllDetails(SqlConnection Conn ,string SecurityKey)
		{
			//Remove User SType 
			SqlCommand removeCMDUser = new SqlCommand("delete from [User_SType] where SType_Detail_Key In (Select  SType_Detail_Key from [SType_Detail] where SType_Detail.SType_Key=@STypeKey) ", Conn);
			removeCMDUser.Parameters.Add("@STypeKey",sTypeKey.Text.ToLower());
			removeCMDUser.ExecuteNonQuery();
			
			//Remove SType Group Detail
			SqlCommand removeCMDGroupDetail = new SqlCommand("delete from [SType_Group_Detail] where SType_Group_Key In (Select  SType_Group_Key from [SType_Group] where SType_Group.SType_Key=@STypeKey) ", Conn);
			removeCMDGroupDetail.Parameters.Add("@STypeKey",sTypeKey.Text.ToLower());
			removeCMDGroupDetail.ExecuteNonQuery();
			
			// Remove SType Detail except the "ALL WAREHOUSES"
			SqlCommand removeCMD = new SqlCommand("delete from [SType_Detail] where SType_key = @STypeKey and Detail <> '-1'", Conn);
			removeCMD.Parameters.Add("@STypeKey",sTypeKey.Text.ToLower());
			removeCMD.ExecuteNonQuery();
		}
		#endregion
	}
}
