<%@ Page language="c#" Inherits="KeyCentral.UI.Inputs.InputCompanyDisclaimer" Codebehind="InputCompanyDisclaimer.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Company Disclaimer</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
		</style>-->
        <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body bgcolor="white" onload="document.Form1.pageDisclaimerTextBox.focus()">
	<script type="text/javascript" language="javascript">
    function Count(text,long) 
    {
	    var maxlength = new Number(long); // Change number to your max length.
	    if (text.value.length > maxlength)
	    {
		    text.value = text.value.substring(0,maxlength);
	    }
    }
    </script>

		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
			    <p>&nbsp;</p>
			    <p>&nbsp;</p>
			    <p>&nbsp;</p>
			    <table border="0" cellpadding="0" cellspacing="0">
			        <tr>
			            <td align="center" colspan="2">Company Disclaimer</td>
			        </tr>
			        <tr>
			            <td>&nbsp;</td>
			        </tr>
			        <tr>
			            <td align="left" colspan="2">&nbsp;Page Disclaimer</td>
			        </tr>
			        <tr>
			            <td align="left" colspan="2">&nbsp;<asp:textbox id="pageDisclaimerTextBox" tabIndex="1" runat="server" EnterAsTab="" Width="800px" MaxLength="130" Font-Names="Arial" Font-Size="8pt"></asp:textbox></td>
			        </tr>
			        <tr>
			            <td>&nbsp;</td>
			        </tr>
			        <tr>
			            <td align="left" colspan="2">&nbsp;Report Disclaimer - Portrait</td>
			        </tr>
			        <tr>
			            <td align="left" colspan="2">&nbsp;<asp:textbox id="reportDisclaimerTextBox1" tabIndex="2" runat="server" EnterAsTab="" Width="800px" MaxLength="100" Font-Names="Arial" Font-Size="10pt"></asp:textbox></td>
			        </tr>
			        <tr>
			            <td align="left" colspan="2">&nbsp;<asp:textbox id="reportDisclaimerTextBox2" tabIndex="3" runat="server" EnterAsTab="" Width="800px" MaxLength="100" Font-Names="Arial" Font-Size="10pt"></asp:textbox></td>
			        </tr>
			        <tr>
			            <td align="left" colspan="2">&nbsp;<asp:textbox id="reportDisclaimerTextBox3" tabIndex="4" runat="server" EnterAsTab="" Width="800px" MaxLength="100" Font-Names="Arial" Font-Size="10pt"></asp:textbox></td>
			        </tr>
			        <tr>
			            <td align="left" colspan="2">&nbsp;<asp:textbox id="reportDisclaimerTextBox4" tabIndex="5" runat="server" EnterAsTab="" Width="800px" MaxLength="100" Font-Names="Arial" Font-Size="10pt"></asp:textbox></td>
			        </tr>
			        <tr>
			            <td align="left" colspan="2">&nbsp;<asp:textbox id="reportDisclaimerTextBox5" tabIndex="6" runat="server" EnterAsTab="" Width="800px" MaxLength="100" Font-Names="Arial" Font-Size="10pt"></asp:textbox></td>
			        </tr>
			        <tr>
			            <td>&nbsp;</td>
			        </tr>
			        <tr>
			            <td align="left" colspan="2">&nbsp;Report Disclaimer - Landscape</td>
			        </tr>
			        <tr>
			            <td align="left" colspan="2">&nbsp;<asp:textbox id="reportDisclaimerTextBox6" tabIndex="7" runat="server" EnterAsTab="" Width="800px" MaxLength="125" Font-Names="Arial" Font-Size="10pt"></asp:textbox></td>
			        </tr>
			        <tr>
			            <td align="left" colspan="2">&nbsp;<asp:textbox id="reportDisclaimerTextBox7" tabIndex="8" runat="server" EnterAsTab="" Width="800px" MaxLength="125" Font-Names="Arial" Font-Size="10pt"></asp:textbox></td>
			        </tr>
			        <tr>
			            <td align="left" colspan="2">&nbsp;<asp:textbox id="reportDisclaimerTextBox8" tabIndex="9" runat="server" EnterAsTab="" Width="800px" MaxLength="125" Font-Names="Arial" Font-Size="10pt"></asp:textbox></td>
			        </tr>
			        <tr>
			            <td align="left" colspan="2">&nbsp;<asp:textbox id="reportDisclaimerTextBox9" tabIndex="10" runat="server" EnterAsTab="" Width="800px" MaxLength="125" Font-Names="Arial" Font-Size="10pt"></asp:textbox></td>
			        </tr>
			        <tr>
			            <td align="left" colspan="2">&nbsp;<asp:textbox id="reportDisclaimerTextBox10" tabIndex="11" runat="server" EnterAsTab="" Width="800px" MaxLength="125" Font-Names="Arial" Font-Size="10pt"></asp:textbox></td>
			        </tr>
			        <tr>
			            <td>&nbsp;</td>
			        </tr>
    			    <tr>
    			        <td align="center"><asp:Button runat="server" ID="SaveExitButton" Text="Save & Exit" TabIndex="7" OnClick="SaveExitButton_Click" Width="100px" /></td>
    			        <td align="center"><asp:Button runat="server" ID="ExitButton" Text="Exit" TabIndex="8" OnClick="ExitButton_Click" Width="100px" /></td>
    			    </tr>
			    </table>
			</div>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"--></form>
	</body>
</html>