using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb; 
using Oracle.DataAccess.Client;
using KeyCentral.Functions;

namespace KeyCentral.UI.Inputs
{

	/// <summary>
	/// Summary description for Expense Type.
	/// </summary>
    public partial class InputCompanyDisclaimer : BasePage
	{
		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
            if (!this.IsAdmin())
			{
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx");
			}

            if(!IsPostBack)
                FillDisclaimers();
		}

        protected void SaveExitButton_Click(object sender, System.EventArgs e)
		{
            disclaimer.UpdateDisclaimer(
                reportDisclaimerTextBox1.Text,
                reportDisclaimerTextBox2.Text,
                reportDisclaimerTextBox3.Text,
                reportDisclaimerTextBox4.Text,
                reportDisclaimerTextBox5.Text,
                reportDisclaimerTextBox6.Text,
                reportDisclaimerTextBox7.Text,
                reportDisclaimerTextBox8.Text,
                reportDisclaimerTextBox9.Text,
                reportDisclaimerTextBox10.Text,
                pageDisclaimerTextBox.Text);

            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Setup");
		}

        protected void ExitButton_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Setup");
		}
		
		#endregion
			
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Private helpers
        private void FillDisclaimers()
		{
            if (disclaimer.DisclaimerList.Count != 0)
            {
                pageDisclaimerTextBox.Text = disclaimer.DisclaimerList[0].ToString().Trim();
                reportDisclaimerTextBox1.Text = disclaimer.DisclaimerList[1].ToString().Trim();
                reportDisclaimerTextBox2.Text = disclaimer.DisclaimerList[2].ToString().Trim();
                reportDisclaimerTextBox3.Text = disclaimer.DisclaimerList[3].ToString().Trim();
                reportDisclaimerTextBox4.Text = disclaimer.DisclaimerList[4].ToString().Trim();
                reportDisclaimerTextBox5.Text = disclaimer.DisclaimerList[5].ToString().Trim();
            }

            if (disclaimer.DisclaimerListLandscape.Count != 0)
            {
                pageDisclaimerTextBox.Text = disclaimer.DisclaimerListLandscape[0].ToString().Trim();
                reportDisclaimerTextBox6.Text = disclaimer.DisclaimerListLandscape[1].ToString().Trim();
                reportDisclaimerTextBox7.Text = disclaimer.DisclaimerListLandscape[2].ToString().Trim();
                reportDisclaimerTextBox8.Text = disclaimer.DisclaimerListLandscape[3].ToString().Trim();
                reportDisclaimerTextBox9.Text = disclaimer.DisclaimerListLandscape[4].ToString().Trim();
                reportDisclaimerTextBox10.Text = disclaimer.DisclaimerListLandscape[5].ToString().Trim();
            }
		}
		#endregion
	}
}
