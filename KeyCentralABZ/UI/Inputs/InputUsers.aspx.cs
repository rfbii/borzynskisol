using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;

namespace KeyCentral.UI.Inputs
{
	/// <summary>
	/// Summary description for Users.
	/// </summary>
	public partial class InputUsers : KeyCentral.Functions.BasePage
	{
		#region Declares
		protected System.Web.UI.WebControls.Button submit;
	#endregion
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);	
			FillUserList();
			DisableAddRemoves();
			
	}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
		#region events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			//Security Check
			if (!this.IsAdmin())
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx");

			if(!IsPostBack && Request.QueryString["RecreateUser"] != null && Request.QueryString["RecreateUser"] == "true")
			{	
				if(Request.QueryString["User_Key"] != "")
				{
					DeleteUser(Request.QueryString["User_Key"]);
					userName.Text =Request.QueryString["UserName"];		
				}
			}
		}
		protected void UserList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			LoadUser(UserList.SelectedItem.Text,UserList.SelectedItem.Value);
		}		
		protected void userName_TextChanged(object sender, System.EventArgs e)
		{
			string u_Key="";
			AdministratorList.SelectedIndex = AdministratorList.Items.IndexOf(AdministratorList.Items.FindByText("No"));
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("select User_Key,Password,Administrator from [user] where UserName=@UserName", Conn);
			selectCMD.Parameters.Add("@UserName",userName.Text.ToLower());
			
			SqlDataReader myReader = selectCMD.ExecuteReader();
			if(myReader.Read()==true)
			{
				u_Key = myReader.GetInt32(0).ToString();
				this.RegisterStartupScript("RecreateUser","<script lang='js'>ExistingUser('This user already exists, do you want to edit this user?  Select Yes to edit existing user, select No to delete existing user and create a new user.','" + myReader.GetInt32(0).ToString() +"','" + userName.Text +"')</script>");
			}
			myReader.Close();
			Conn.Close();
			Conn.Dispose();		
			
			if(u_Key != "")
				LoadUser(userName.Text,u_Key);
		}

		protected void SecurityTypeList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			sTypeKey.Text = SecurityTypeList.SelectedItem.Value;
			//Fill ListBoxs
			SqlConnection Conn = GetSQLConn();

			DataBindLists(Conn,userKey.Text,sTypeKey.Text);
			message.Text = "";
			EnableAddRemoves();
			Conn.Close();
			Conn.Dispose();
		}
		protected void password2_TextChanged(object sender, System.EventArgs e)
		{ 
			//Make sure a user has been entered
			if(userName.Text.Trim() == "")
			{
				message.Text = "Please enter a user.";
				return;
			}
			//Make sure the passwords are the same
			if(password.Text != password2.Text)
			{
				message.Text = "Passwords are not the same.";
				return;
			}
			//Make sure the passwords are the same
			if(password.Text.Trim() == "")
			{
				message.Text = "Please enter a Password.";
				return;
			}
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("select User_Key,Password,Administrator from [user] where UserName=@UserName", Conn);
			SqlCommand insertCMD = new SqlCommand("insert into [user] (UserName,Password,Administrator) values(@UserName,@Password,@Administrator) Select @@Identity", Conn);
			SqlCommand updateCMD = new SqlCommand("update [user] Set Password=@Password where UserName=@UserName", Conn);
			selectCMD.Parameters.Add("@UserName",userName.Text.ToLower());
			insertCMD.Parameters.Add("@UserName",userName.Text);
			insertCMD.Parameters.Add("@Password",password.Text);
			insertCMD.Parameters.Add("@Administrator",AdministratorList.SelectedItem.Value.ToString());
			updateCMD.Parameters.Add("@UserName",userName.Text);
			updateCMD.Parameters.Add("@Password",password.Text);
			
			SqlDataReader myReader = selectCMD.ExecuteReader();
			if(myReader.Read()==true)
			{
				if (myReader.GetString(1).ToString()== password.Text)
				{
					myReader.Close();
					message.Text = "This User already exists.";
				}
				else
				{
					myReader.Close();
					updateCMD.ExecuteNonQuery();
					message.Text = "Password Updated.";	
				}
			}
			else
			{
				myReader.Close();
				userKey.Text = insertCMD.ExecuteScalar().ToString();
				message.Text = "User Added.";
				FillUserList();
				FillSecurityTypeList();
				deleteUser.Message= "Do you want to delete " + userName.Text + " User?";
				deleteUser.Enabled=true;
				//EnableScreen();
				availableGroupList.DataSource = "";
				availableGroupList.DataBind();
				selectedGroupList.DataSource = "";
				selectedGroupList.DataBind();
				availableDetailList.DataSource = "";
				availableDetailList.DataBind();
				selectedDetailList.DataSource = "";
				selectedDetailList.DataBind();		
			}
			
			Conn.Close();
			Conn.Dispose();
		}
		protected void AdministratorList_SelectedIndexChanged(object sender, System.EventArgs e)
		{ 
			//Make sure a user has been entered
			if(userName.Text.Trim() == "")
			{
				message.Text = "Please enter a user.";
				return;
			}
			//Make sure the passwords are the same
			if(password.Text != password2.Text)
			{
				message.Text = "Passwords are not the same.";
				return;
			}
			
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("select User_Key,Password,Administrator from [user] where UserName=@UserName", Conn);
			SqlCommand insertCMD = new SqlCommand("insert into [user] (UserName,Password,Administrator) values(@UserName,@Password,@Administrator) Select @@Identity", Conn);
			SqlCommand updateCMD = new SqlCommand("update [user] Set Administrator=@Administrator where UserName=@UserName", Conn);
			selectCMD.Parameters.Add("@UserName",userName.Text.ToLower());
			insertCMD.Parameters.Add("@UserName",userName.Text);
			insertCMD.Parameters.Add("@Password",password.Text);
			insertCMD.Parameters.Add("@Administrator",AdministratorList.SelectedItem.Value.ToString());
			updateCMD.Parameters.Add("@UserName",userName.Text);
			updateCMD.Parameters.Add("@Administrator",AdministratorList.SelectedItem.Value.ToString());
			
			SqlDataReader myReader = selectCMD.ExecuteReader();
			if(myReader.Read()==true)
			{
				if (myReader.GetString(2).ToString()== AdministratorList.SelectedItem.Value.ToString())
				{
					myReader.Close();
					message.Text = "This User already exists.";
				}
				else
				{
					myReader.Close();
					updateCMD.ExecuteNonQuery();
					message.Text = "Administrator Updated.";						
				}
			}
			else
			{
				//Make sure the passwords are the same
				if(password.Text.Trim() == "")
				{
					message.Text = "Please enter a Password.";
					return;
				}
				myReader.Close();
				userKey.Text = insertCMD.ExecuteScalar().ToString();
				message.Text = "User Added.";
				FillUserList();
				FillSecurityTypeList();
				deleteUser.Message= "Do you want to delete " + userName.Text + " User?";
				deleteUser.Enabled=true;
				//EnableScreen();
				availableGroupList.DataSource = "";
				availableGroupList.DataBind();
				selectedGroupList.DataSource = "";
				selectedGroupList.DataBind();
				availableDetailList.DataSource = "";
				availableDetailList.DataBind();
				selectedDetailList.DataSource = "";
				selectedDetailList.DataBind();		
			}
			
			Conn.Close();
			Conn.Dispose();
		}
		
		protected void deleteUser_Click(object sender, System.EventArgs e)
		{
			if(userKey.Text != "")
				DeleteUser(userKey.Text);
			userName.Text = "";
		}

		protected void addGroup_Click(object sender, System.EventArgs e)
		{			
			if(availableGroupList.SelectedIndex!=-1)
			{
				SqlConnection Conn = GetSQLConn();
				SqlCommand insertCMD = new SqlCommand("insert into [User_SType_Group] (User_Key,SType_Group_Key) values(@User_Key,@SType_Group_Key)", Conn);
				insertCMD.Parameters.Add("@User_Key",userKey.Text);
				insertCMD.Parameters.Add("@SType_Group_Key",availableGroupList.SelectedItem.Value);		
			
				insertCMD.ExecuteNonQuery();
				message.Text = "Security Group Added.";
				DataBindGroupLists(Conn,userKey.Text,sTypeKey.Text);				
				Conn.Close();
				Conn.Dispose();
			}
			else
			{
				message.Text = "Please select a Security Group to add to this User.";
			}			
		}
		protected void removeGroup_Click(object sender, System.EventArgs e)
		{
			if(selectedGroupList.SelectedIndex!=-1)
			{
				SqlConnection Conn = GetSQLConn();
				SqlCommand removeCMD = new SqlCommand("delete from [User_SType_Group] where User_Key =@User_Key and SType_Group_Key = @SType_Group_Key ", Conn);
				removeCMD.Parameters.Add("@User_Key",userKey.Text);		
				removeCMD.Parameters.Add("@SType_Group_Key",selectedGroupList.SelectedItem.Value);

				removeCMD.ExecuteNonQuery();
				DataBindGroupLists(Conn,userKey.Text,sTypeKey.Text);				
				message.Text = "Security Group Removed.";				
				Conn.Close();
				Conn.Dispose();
			}
			else
			{
				message.Text = "Please select a Security Group to remove from this User.";
			}
		}
		protected void addAllGroups_Click(object sender, System.EventArgs e)
		{	
			SqlConnection Conn = GetSQLConn();
			SqlCommand insertCMD = new SqlCommand("INSERT INTO User_SType_Group (User_Key ,SType_Group_Key ) SELECT @User_Key,SType_Group_Key  FROM SType_Group Where SType_Key = @SType_Key", Conn);
			insertCMD.Parameters.Add("@User_Key",userKey.Text);
			insertCMD.Parameters.Add("@SType_Key",sTypeKey.Text);
			
			RemoveAllGroups(Conn,userKey.Text,sTypeKey.Text);
			insertCMD.ExecuteNonQuery();
			message.Text = "All Security Groups Added.";
			DataBindGroupLists(Conn,userKey.Text,sTypeKey.Text);
			Conn.Close();
			Conn.Dispose();		
		}

		protected void RemoveAllGroups_Click(object sender, System.EventArgs e)
		{
			SqlConnection Conn = GetSQLConn();
			
			RemoveAllGroups(Conn,userKey.Text,sTypeKey.Text);
			DataBindGroupLists(Conn,userKey.Text,sTypeKey.Text);
			message.Text = "All Security Groups Removed.";				
			Conn.Close();
			Conn.Dispose();				
		}
		
		protected void addDetail_Click(object sender, System.EventArgs e)
		{			
			if(availableDetailList.SelectedIndex!=-1)
			{
				SqlConnection Conn = GetSQLConn();
				SqlCommand insertCMD = new SqlCommand("insert into [User_SType] (User_Key,SType_Detail_Key) values(@User_Key,@SType_Detail_Key)", Conn);
				insertCMD.Parameters.Add("@User_Key",userKey.Text);
				insertCMD.Parameters.Add("@SType_Detail_Key",availableDetailList.SelectedItem.Value);		
				
				insertCMD.ExecuteNonQuery();
				DataBindDetailLists(Conn,userKey.Text,sTypeKey.Text);				
				message.Text = "Security Detail Added.";
				Conn.Close();
				Conn.Dispose();
			}
			else
			{
				message.Text = "Please select a Security Detail to add to this User.";
			}			
		}
		protected void removeDetail_Click(object sender, System.EventArgs e)
		{
			if(selectedDetailList.SelectedIndex!=-1)
			{
				SqlConnection Conn = GetSQLConn();
				SqlCommand removeCMD = new SqlCommand("delete from [User_SType] where User_Key =@User_Key and SType_Detail_Key = @SType_Detail_Key ", Conn);
				removeCMD.Parameters.Add("@User_Key",userKey.Text);		
				removeCMD.Parameters.Add("@SType_Detail_Key",selectedDetailList.SelectedItem.Value);
				
				removeCMD.ExecuteNonQuery();
				DataBindDetailLists(Conn,userKey.Text,sTypeKey.Text);				
				message.Text = "Security Detail Removed.";				
				Conn.Close();
				Conn.Dispose();
			}
			else
			{
				message.Text = "Please select a Security Detail to remove from this User.";
			}
		}
		protected void addAllDetails_Click(object sender, System.EventArgs e)
		{	
			SqlConnection Conn = GetSQLConn();
			SqlCommand insertCMD = new SqlCommand("INSERT INTO User_SType (User_Key ,SType_Detail_Key ) SELECT @User_Key,SType_Detail_Key  FROM SType_Detail Where SType_Key = @SType_Key", Conn);
			insertCMD.Parameters.Add("@User_Key",userKey.Text);
			insertCMD.Parameters.Add("@SType_Key",sTypeKey.Text);
			
			RemoveAllDetails(Conn,userKey.Text,sTypeKey.Text);
			insertCMD.ExecuteNonQuery();
			DataBindDetailLists(Conn,userKey.Text,sTypeKey.Text);
			message.Text = "All Security Details Added.";
			Conn.Close();
			Conn.Dispose();		
		}

		protected void RemoveAllDetails_Click(object sender, System.EventArgs e)
		{
			SqlConnection Conn = GetSQLConn();
			
			RemoveAllDetails(Conn,userKey.Text,sTypeKey.Text);
			DataBindDetailLists(Conn,userKey.Text,sTypeKey.Text);
			message.Text = "All Security Details Removed.";				
			Conn.Close();
			Conn.Dispose();				
		}
		
		protected void Return_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Setup/Security");			
		}
		
		protected void userSecurityReport_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Reports/ReportUserSecurity.aspx?" + "&&LastPage=" + "../../../KeyCentralABZ/UI/Inputs/InputUsers.aspx");			
		
		}
		protected void typeSecurityReport_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Reports/ReportTypeSecurity.aspx?" + "&&LastPage=" + "../../../KeyCentralABZ/UI/Inputs/InputUsers.aspx");			
		
		}
		protected void groupSecurityReport_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Reports/ReportGroupSecurity.aspx?" + "&&LastPage=" + "../../../KeyCentralABZ/UI/Inputs/InputUsers.aspx");			
		
		}
		#endregion
		#region Private helpers	
		private void LoadUser(string uName, string u_Key)
		{
			userKey.Text = u_Key;
			userName.Text = uName;
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("select Administrator from [User] where UserName=@UserName", Conn);
			selectCMD.Parameters.Add("@UserName",uName.ToLower());
			
			SqlDataReader myReader = selectCMD.ExecuteReader();
			myReader.Read();
			AdministratorList.SelectedIndex = AdministratorList.Items.IndexOf(AdministratorList.Items.FindByText(myReader.GetString(0).ToString()));
			myReader.Close();
			Conn.Close();
			Conn.Dispose();
			
			FillSecurityTypeList();
			deleteUser.Message= "Do you want to delete " + uName + " User?";
			deleteUser.Enabled=true;
			DisableAddRemoves();
			message.Text = "";
			availableGroupList.DataSource = "";
			availableGroupList.DataBind();
			selectedGroupList.DataSource = "";
			selectedGroupList.DataBind();
			availableDetailList.DataSource = "";
			availableDetailList.DataBind();
			selectedDetailList.DataSource = "";
			selectedDetailList.DataBind();

		}
		private void DeleteUser(string u_Key)
		{
			SqlConnection Conn = GetSQLConn();
			
			SqlCommand deleteFavCMD = new SqlCommand("use CustomerStatus; delete from [Customer_Status_Fav] where User_Key = @User_Key ", Conn);
			deleteFavCMD.Parameters.Add("@User_Key",u_Key);
			deleteFavCMD.ExecuteNonQuery();
			SqlCommand deleteGroupCMD = new SqlCommand("use KeyCentral;delete from [User_SType_Group] where User_Key = @User_Key ", Conn);
			deleteGroupCMD.Parameters.Add("@User_Key",u_Key);
			deleteGroupCMD.ExecuteNonQuery();
			SqlCommand deleteDetailCMD = new SqlCommand("use KeyCentral;delete from [User_SType] where User_Key = @User_Key ", Conn);
			deleteDetailCMD.Parameters.Add("@User_Key",u_Key);
			deleteDetailCMD.ExecuteNonQuery();
			SqlCommand deleteUserCMD = new SqlCommand("use KeyCentral;delete from [User] where User_Key = @User_Key ", Conn);
			deleteUserCMD.Parameters.Add("@User_Key",u_Key);
			deleteUserCMD.ExecuteNonQuery();
			UserList.DataSource = "";
			UserList.DataBind();
			SecurityTypeList.DataSource = "";
			SecurityTypeList.DataBind();
			availableGroupList.DataSource = "";
			availableGroupList.DataBind();
			selectedGroupList.DataSource = "";
			selectedGroupList.DataBind();
			availableDetailList.DataSource = "";
			availableDetailList.DataBind();
			selectedDetailList.DataSource = "";
			selectedDetailList.DataBind();		
			message.Text = "User Deleted.";	
			password.Text = "";
			password2.Text = "";
			userKey.Text = "";
			sTypeKey.Text = "";
			FillUserList();			
			deleteUser.Enabled= false;
		}
		private void FillUserList()
		{			
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("select * from [User] order by UserName", Conn);
			
			SqlDataReader myReader = selectCMD.ExecuteReader();
			if(myReader.HasRows ==true)
			{
				UserList.DataSource = myReader;
				UserList.DataTextField = "UserName";
				UserList.DataValueField = "User_Key";
				UserList.DataBind();
			}
			Conn.Close();
			Conn.Dispose();	 						
		}			
		private void FillSecurityTypeList()
		{
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("select * from [SType] order by description", Conn);
		
			SqlDataReader myReader = selectCMD.ExecuteReader();
			 
			SecurityTypeList.DataSource = myReader;
			SecurityTypeList.DataTextField = "Description";
			SecurityTypeList.DataValueField = "SType_Key";
			SecurityTypeList.DataBind();	
			Conn.Close();
			Conn.Dispose();	  		
					
		}	
		private void DataBindLists(SqlConnection Conn ,string UserKey,string TypeKey)
		{
			DataBindGroupLists(Conn,UserKey,TypeKey);
			DataBindDetailLists(Conn,UserKey,TypeKey);		
		}
		private void DataBindGroupLists(SqlConnection Conn ,string UserKey,string TypeKey)
		 {
			 DataBindAvailableGroupList(Conn,UserKey,TypeKey);
			 DataBindSelectedGroupList(Conn,UserKey,TypeKey);
		 }
		private void DataBindDetailLists(SqlConnection Conn ,string UserKey,string TypeKey)
		{
			SqlConnection SConn = GetSQLConn();
			
			SqlCommand selectCMD = new SqlCommand("select SType_Key,Description from [SType] where SType_Key=@TypeKey", Conn);
			selectCMD.Parameters.Add("@TypeKey",sTypeKey.Text);
			SqlDataReader myReader2 = selectCMD.ExecuteReader();
			myReader2.Read(); 			
			switch(myReader2.GetString(1).ToString().ToLower())
			{
				case "grower":
					myReader2.Close();
					DataBindAvailableDetailList(Conn,UserKey,TypeKey);
					DataBindSelectedDetailList(Conn,UserKey,TypeKey);
					KeyCentral.Functions.Misc.GetGrowerNames(Session,this.selectedDetailList,this.availableDetailList);
					break;
				case "warehouse":
					myReader2.Close();
					DataBindAvailableDetailList(Conn,UserKey,TypeKey);
					DataBindSelectedDetailList(Conn,UserKey,TypeKey);
					KeyCentral.Functions.Misc.GetWarehouseNames(Session,this.selectedDetailList,this.availableDetailList);
					break;
				default:
					myReader2.Close();
					message.Text = "";
					DataBindAvailableDetailList(Conn,UserKey,TypeKey);
					DataBindSelectedDetailList(Conn,UserKey,TypeKey);
					break;
			}
			SConn.Close();
			SConn.Dispose();						
		}
		
		
		private void DataBindAvailableGroupList(SqlConnection Conn ,string UserKey,string TypeKey)
		{
			SqlCommand selectCMD = new SqlCommand("select SType_Group.Description,SType_Group.SType_Group_Key from [SType_Group] where SType_Group.SType_Key = @TypeKey and SType_Group.SType_Group_key not in (select SType_Group_key from [User_SType_Group] where User_Key=@UserKey) order by Description", Conn);
			selectCMD.Parameters.Add("@UserKey",UserKey);
			selectCMD.Parameters.Add("@TypeKey",TypeKey);
			SqlDataReader myReader = selectCMD.ExecuteReader();
			availableGroupList.DataSource = myReader;			
			availableGroupList.DataBind();
			myReader.Close();
		}
		
		private void DataBindSelectedGroupList(SqlConnection Conn ,string UserKey,string TypeKey)
		{
			SqlCommand selectCMD = new SqlCommand("select SType_Group.Description,SType_Group.SType_Group_Key from [User_SType_Group] join [SType_Group] on SType_Group.SType_Group_Key = User_SType_Group.SType_Group_Key where User_Key=@UserKey and SType_Group.SType_Key = @TypeKey Order by Description", Conn);
			selectCMD.Parameters.Add("@UserKey",UserKey);
			selectCMD.Parameters.Add("@TypeKey",TypeKey);
			
			SqlDataReader myReader = selectCMD.ExecuteReader();
			selectedGroupList.DataSource = myReader;
			selectedGroupList.DataBind();

			myReader.Close();
		}
		
		private void DataBindAvailableDetailList(SqlConnection Conn ,string UserKey,string TypeKey)
		{
			SqlCommand selectCMD = new SqlCommand("select SType_Detail.Detail,SType_Detail.SType_Detail_key from [SType_Detail] where SType_Detail.SType_Key = @TypeKey and SType_Detail.SType_Detail_key not in (select SType_Detail_key from [User_SType] where User_Key=@UserKey) order by Detail", Conn);
			selectCMD.Parameters.Add("@UserKey",UserKey);
			selectCMD.Parameters.Add("@TypeKey",TypeKey);
			SqlDataReader myReader = selectCMD.ExecuteReader();
			availableDetailList.DataSource = myReader;			
			availableDetailList.DataBind();
			myReader.Close();

			//Handle All Warehouses/Growers
			if(availableDetailList.Items.Count > 0)
			{
				selectCMD = new SqlCommand("select SType.Description from Stype where SType_Key=@SType_Key", Conn);
				selectCMD.Parameters.Add("@SType_Key",TypeKey);

				myReader = selectCMD.ExecuteReader();
				if(myReader.Read()==true)
				{
					switch(myReader.GetString(0).ToString().ToLower())
					{
						case "grower":
							myReader.Close();
							if(availableDetailList.Items[0].Text == "-1")
								availableDetailList.Items[0].Text = "ALL GROWERS";
							break;
						case "warehouse":
							myReader.Close();
							if(availableDetailList.Items[0].Text == "-1")
								availableDetailList.Items[0].Text = "ALL WAREHOUSES";
							break;
						default:
							myReader.Close();
							break;
					}
				}
			}
		}
		
		private void DataBindSelectedDetailList(SqlConnection Conn ,string UserKey,string TypeKey)
		{
			SqlCommand selectCMD = new SqlCommand("select SType_Detail.Detail,SType_Detail.SType_Detail_Key from [User_SType] join [SType_Detail] on SType_Detail.SType_Detail_Key = User_SType.SType_Detail_Key where User_Key=@UserKey and SType_Detail.SType_Key = @TypeKey Order by Detail", Conn);
			selectCMD.Parameters.Add("@UserKey",UserKey);
			selectCMD.Parameters.Add("@TypeKey",TypeKey);
			
			SqlDataReader myReader = selectCMD.ExecuteReader();
			selectedDetailList.DataSource = myReader;
			selectedDetailList.DataBind();

			myReader.Close();

			//Handle All Warehouses/Growers
			if(selectedDetailList.Items.Count > 0)
			{
				selectCMD = new SqlCommand("select SType.Description from Stype where SType_Key=@SType_Key", Conn);
				selectCMD.Parameters.Add("@SType_Key",TypeKey);

				myReader = selectCMD.ExecuteReader();
				if(myReader.Read()==true)
				{
					switch(myReader.GetString(0).ToString().ToLower())
					{
						case "grower":
							myReader.Close();
							if(selectedDetailList.Items[0].Text == "-1")
								selectedDetailList.Items[0].Text = "ALL GROWERS";
							break;
						case "warehouse":
							myReader.Close();
							if(selectedDetailList.Items[0].Text == "-1")
								selectedDetailList.Items[0].Text = "ALL WAREHOUSES";
							break;
						default:
							myReader.Close();
							break;
					}
				}
			}
		}
		private void RemoveAllGroups(SqlConnection Conn ,string UserKey,string TypeKey)
		{
			SqlCommand removeCMD = new SqlCommand("delete from [User_SType_Group] where User_Key = @UserKey and SType_Group_Key In(Select  SType_Group_Key from [SType_Group] where SType_Group.SType_Key=@TypeKey) ", Conn);
			removeCMD.Parameters.Add("@UserKey",UserKey);
			removeCMD.Parameters.Add("@TypeKey",TypeKey);
			removeCMD.ExecuteNonQuery();
		}
		
		private void RemoveAllDetails(SqlConnection Conn ,string UserKey,string TypeKey)
		{
			SqlCommand removeCMD = new SqlCommand("delete from [User_SType] where User_Key = @UserKey and SType_Detail_Key In(Select  SType_Detail_Key from [SType_Detail] where SType_Detail.SType_Key=@TypeKey) ", Conn);
			removeCMD.Parameters.Add("@UserKey",UserKey);
			removeCMD.Parameters.Add("@TypeKey",TypeKey);
			removeCMD.ExecuteNonQuery();
		}
		private void DisableAddRemoves()
		{
			addGroup.Enabled = false;
			removeGroup.Enabled = false;
			addAllGroups.Enabled = false;
			removeAllGroups.Enabled = false;
			addDetail.Enabled = false;
			removeDetail.Enabled = false;
			addAllDetails.Enabled = false;
			removeAllDetails.Enabled = false;
		}
		private void EnableAddRemoves()
		{
			addGroup.Enabled = true;
			removeGroup.Enabled = true;
			addAllGroups.Enabled = true;
			removeAllGroups.Enabled = true;
			addDetail.Enabled = true;
			removeDetail.Enabled = true;
			addAllDetails.Enabled = true;
			removeAllDetails.Enabled = true;
		}
		
		#endregion
	}
}
