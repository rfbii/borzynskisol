using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb; 
using Oracle.DataAccess.Client;
using System.Text;
using KeyCentral.Functions;
using KeyCentral.Lookups;

namespace KeyCentral.UI.Inputs
{

	/// <summary>
    /// Summary description for InputCommodityCategory.
	/// </summary>
    public partial class InputCommodityCategory : BasePage
    {
        #region Vars
        private ArrayList CommodityNameArrayList { get { return (ArrayList)Session["CommodityName"]; } }
        private ArrayList CommoditySizeSearchArrayList { get { return (ArrayList)Session["CommoditySizeSearch"]; } }
        private KeyCentral.Functions.ReportSessionManager reportSessionManager;
        private string ReportName;
        private string InputName;
        private string Security;
        private string Database;
        private string CommodityKey;
        private string ReturnURL;
        #endregion

        #region Events
        protected void Page_Load(object sender, System.EventArgs e)
		{
            ReportName = "";
            InputName = "";
            Security = "";
            Database = "";
            CommodityKey = "";
            ReturnURL = "";

            if (Request.QueryString["ReportName"] != "" || Request.QueryString["ReportName"] != null)
                ReportName = Request.QueryString["ReportName"];

            switch (ReportName)
            {
                case "WWInputCommodityCategory0607":
                    InputName = "Walla Walla";
                    Security = "WWSRP Setup";
                    CommodityKey = "5";
                    Database = "WWSRP0607";
                    ReturnURL = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/SRP/WW SRP 06-07/Inputs";
                    break;

                case "WeeklySalesInputCommodityCategory":
                    InputName = "Weekly Sales";
                    Security = "Weekly Sales Setup";
                    CommodityKey = "";
                    Database = "WeeklySales";
                    ReturnURL = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Reports/Sales Reports";
                    break;

                default:
                    ReturnURL = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=";
                    break;
            }

            if (CheckSecurity(Security) == false)
			{
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=");
            }

            reportSessionManager = new KeyCentral.Functions.ReportSessionManager(this, Request["ReportName"].ToString());

            if (!IsPostBack)
            {
                SetTitleLine();
                SetupCommodity();
            }
		}

		protected void CategoryList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
            CategoryKey.Text = "";
            NewCategory.Text = "";
            Message.Text = "";
            NewCategory.Text = CategoryList.SelectedItem.Text.ToString();
            CategoryKey.Text = CategoryList.SelectedItem.Value;
            Message.Text = "";
            IncludedListBox.Items.Clear();
            IncludedListBox.ClearSelection();

            if (CategoryKey.Text == "-1" && NewCategory.Text == "Other")
            {
                NewCategory.Enabled = false;
                Save1.Enabled = false;
                save.Enabled = false;
                sizeSearch.Text = "";
                sizeSearch.Enabled = false;
                gradeSearch.Text = "";
                gradeSearch.Enabled = false;
                deleteCategory.Enabled = false;
                newCategoryBtn.Enabled = true;
                Included.Enabled = false;
            }
            else
            {
                deleteCategory.Message = "Do you want to delete Category " + CategoryList.SelectedItem.Text + "?";
                deleteCategory.Enabled = true;
                newCategoryBtn.Enabled = true;
                Included.Enabled = true;
                NewCategory.Enabled = true;
                save.Enabled = true;
                Save1.Enabled = true;
                sizeSearch.Text = "";
                sizeSearch.Enabled = true;
                gradeSearch.Text = "";
                gradeSearch.Enabled = true;

                if (commodityKey.Text != "" && CategoryKey.Text != "")
                {
                    SqlConnection Conn = GetSQLConn();
                    SqlCommand selectCMD = new SqlCommand("use " + Database + "; select Size_Search, Grade_Search from [Commodity_Category] where Commodity_Key=@Commodity_Key and Category_Key=@Category_Key", Conn);
                    selectCMD.Parameters.Add("@Commodity_Key", commodityKey.Text);
                    selectCMD.Parameters.Add("@Category_Key", CategoryKey.Text);

                    SqlDataReader myReader = selectCMD.ExecuteReader();
                    if (myReader.Read() == true) //Existing Header
                    {
                        sizeSearch.Text = myReader.GetValue(0).ToString().ToUpper();
                        gradeSearch.Text = myReader.GetValue(1).ToString().ToUpper();
                        Included_Click(null,null);
                    }

                    myReader.Close();
                    Conn.Close();
                    Conn.Dispose();
                }
            }
		}

		protected void CommodityList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			commodityKey.Text = "";
			commodityName.Text = "";
			Message.Text = "";
			commodityName.Text = CommodityList.SelectedItem.Text;
			commodityKey.Text = CommodityList.SelectedItem.Value;
            FillCategoryList(CommodityList.SelectedItem.Value);
            sizeProblemsButton.Enabled = true;
           

            SizeNotIncludedListBox.Items.Clear();
            SizeNotIncludedListBox.ClearSelection();

            MultipleSizesIncludedListBox.Items.Clear();
            MultipleSizesIncludedListBox.ClearSelection();

            IncludedListBox.Items.Clear();
            IncludedListBox.ClearSelection();

            NewCategory.Text = "";
            gradeSearch.Text = "";
            sizeSearch.Text = "";

            NewCategory.Enabled = true;
            Save1.Enabled = true;
            save.Enabled = true;
            sizeSearch.Enabled = true;
            gradeSearch.Enabled = true;
            deleteCategory.Enabled = false;
            newCategoryBtn.Enabled = true;
            Included.Enabled = false;
		}

		protected void Return_Click(object sender, System.EventArgs e)
		{
            Response.Redirect(ReturnURL);			
		}

		protected void deleteCategory_Click(object sender, System.EventArgs e)
		{
			SqlConnection Conn = GetSQLConn();

			//Remove Category 
            SqlCommand removeCMD = new SqlCommand("use " + Database + ";delete from [Commodity_Category] where Commodity_Key=@Commodity_Key and Category_Key = @Category_Key ", Conn);
            removeCMD.Parameters.Add("@Commodity_Key", commodityKey.Text);
			removeCMD.Parameters.Add("@Category_Key", CategoryKey.Text);
			removeCMD.ExecuteNonQuery();
			NewCategory.Text = "";
			sizeSearch.Text = "";
            gradeSearch.Text = "";

            CategoryList.ClearSelection();
            CategoryList.Items.Clear();
            CategoryList.SelectedIndex = -1;
            CategoryKey.Text = "";

            if(commodityKey.Text != "")
                FillCategoryList(commodityKey.Text);

            Message.Text = "Commodity-Category Deleted.";

            //CategoryList.SelectedIndex = -1;
            //CategoryList.ClearSelection();
            //CategoryList.Items.Clear();
            deleteCategory.Enabled = false;
            newCategoryBtn.Enabled = true;
            Included.Enabled = false;

            //CommodityList.ClearSelection();
            //CommodityList.SelectedIndex = -1;

            //commodityKey.Text = "";
            //commodityName.Text = "";
            //sizeProblemsButton.Enabled = false;

            SizeNotIncludedListBox.Items.Clear();
            SizeNotIncludedListBox.ClearSelection();

            MultipleSizesIncludedListBox.Items.Clear();
            MultipleSizesIncludedListBox.ClearSelection();

            IncludedListBox.Items.Clear();
            IncludedListBox.ClearSelection();

            string tempMessage = Message.Text;

            SetupCommodity();

            Message.Text = tempMessage;
		}

        protected void newCategory_Click(object sender, System.EventArgs e)
        {
            CategoryList.SelectedIndex = -1;
            NewCategory.Text = "";
            sizeSearch.Text = "";
            gradeSearch.Text = "";
            CategoryKey.Text = "";
            Message.Text = "";
            deleteCategory.Enabled = false;
            Included.Enabled = false;

            SizeNotIncludedListBox.Items.Clear();
            SizeNotIncludedListBox.ClearSelection();

            MultipleSizesIncludedListBox.Items.Clear();
            MultipleSizesIncludedListBox.ClearSelection();

            IncludedListBox.Items.Clear();
            IncludedListBox.ClearSelection();
        }

		protected void cancel_Click(object sender, System.EventArgs e)
		{
			NewCategory.Text = "";
			//NewCategory.Enabled = false;
			sizeSearch.Text = "";
			//sizeSearch.Enabled = false;
			CategoryList.SelectedIndex = -1;
            CategoryList.ClearSelection();
            CategoryList.Items.Clear();
			deleteCategory.Enabled = false;
            newCategoryBtn.Enabled = false;
            Included.Enabled = false;
            gradeSearch.Text = "";
            CategoryKey.Text = "";
            Message.Text = "";

            NewCategory.Enabled = true;
            Save1.Enabled = true;
            save.Enabled = true;
            sizeSearch.Enabled = true;
            gradeSearch.Enabled = true;

			CommodityList.ClearSelection();
			CommodityList.SelectedIndex = -1;

			commodityKey.Text = "";
            commodityName.Text = "";
            sizeProblemsButton.Enabled = false;

            SizeNotIncludedListBox.Items.Clear();
            SizeNotIncludedListBox.ClearSelection();

            MultipleSizesIncludedListBox.Items.Clear();
            MultipleSizesIncludedListBox.ClearSelection();

            IncludedListBox.Items.Clear();
            IncludedListBox.ClearSelection();

            SetupCommodity();
		}

		protected void save_Click(object sender, System.EventArgs e)
		{
			if(NewCategory.Text.Trim() == "" || commodityKey.Text == "")
			{
                Message.Text = "Please choose a commodity, then fill in a category.";
				return;
			}
			else if(sizeSearch.Text.Trim() == "")
			{
				Message.Text = "Please fill in a size search keyword.";
				return;
			}
            else if (gradeSearch.Text.Trim() == "")
            {
                Message.Text = "Please fill in a grade search keyword.";
                return;
            }

			KeyCentral.Functions.JavaStatusMessage status = new KeyCentral.Functions.JavaStatusMessage(this);
			status.ShowMessage("Saving");

			Message.Text = "";
			SqlConnection Conn = GetSQLConn();
            SqlCommand selectCMD = new SqlCommand("use " + Database + "; select Category_Key from [Commodity_Category] where Description=@Description and Commodity_Key=@Commodity_Key and Size_Search=@Size_Search and Grade_Search=@Grade_Search", Conn);
			selectCMD.Parameters.Add("@Description",NewCategory.Text.ToLower());
			selectCMD.Parameters.Add("@Commodity_Key",commodityKey.Text);
            selectCMD.Parameters.Add("@Size_Search", sizeSearch.Text.Trim().ToUpper());
            selectCMD.Parameters.Add("@Grade_Search", gradeSearch.Text.Trim().ToUpper());

			SqlDataReader myReader = selectCMD.ExecuteReader();
			if(myReader.Read()==true) //Existing Header
			{
				CategoryKey.Text = myReader.GetValue(0).ToString();
				myReader.Close();

                Message.Text = "Commodity-Category already exists.";
			}
			else if (CategoryList.SelectedIndex != -1) //category exists and is selected
			{
				myReader.Close();
                SqlCommand updateCMD = new SqlCommand("use " + Database + "; update Commodity_Category set Description=@Description, Size_Search=@Size_Search, Grade_Search=@Grade_Search where Commodity_Key=@Commodity_Key and Category_Key=@Category_Key", Conn);
				updateCMD.Parameters.Add("@Commodity_Key",commodityKey.Text);
				updateCMD.Parameters.Add("@Category_Key",CategoryKey.Text);
                updateCMD.Parameters.Add("@Description", NewCategory.Text);
                updateCMD.Parameters.Add("@Size_Search", sizeSearch.Text.Trim().ToUpper());
                updateCMD.Parameters.Add("@Grade_Search", gradeSearch.Text.Trim().ToUpper());

				updateCMD.ExecuteNonQuery();
				updateCMD.Dispose();
				this.CleanUpSQL(Conn);

				Message.Text = "Commodity-Category updated.";
				NewCategory.Text = "";
                sizeSearch.Text = "";
                gradeSearch.Text = "";
                CategoryKey.Text = "";

                CategoryList.ClearSelection();
                CategoryList.Items.Clear();
                CategoryList.SelectedIndex = -1;

                FillCategoryList(commodityKey.Text);

			}
			else  //New Category
			{
				myReader.Close();
                SqlCommand insertCMD = new SqlCommand("use " + Database + "; insert into [Commodity_Category] (Commodity_Key,Size_Search,Grade_Search,Description) values(@Commodity_Key,@Size_Search,@Grade_Search,@Description) Select @@Identity", Conn);
				insertCMD.Parameters.Add("@Description", NewCategory.Text);
				insertCMD.Parameters.Add("@Commodity_Key", commodityKey.Text);
                insertCMD.Parameters.Add("@Size_Search", sizeSearch.Text.Trim().ToUpper());
                insertCMD.Parameters.Add("@Grade_Search", gradeSearch.Text.Trim().ToUpper());

				CategoryKey.Text = insertCMD.ExecuteScalar().ToString();
                Message.Text = "Commodity-Category Added.";
				NewCategory.Text = "";
                sizeSearch.Text = "";
                gradeSearch.Text = "";
                CategoryKey.Text = "";

                CategoryList.ClearSelection();
                CategoryList.Items.Clear();
                CategoryList.SelectedIndex = -1;

                FillCategoryList(commodityKey.Text);
			}

			Conn.Close();
            Conn.Dispose();

            deleteCategory.Enabled = false;
            newCategoryBtn.Enabled = true;
            Included.Enabled = false;

            IncludedListBox.Items.Clear();
            IncludedListBox.ClearSelection();

            SizeNotIncludedListBox.Items.Clear();
            SizeNotIncludedListBox.ClearSelection();

            MultipleSizesIncludedListBox.Items.Clear();
            MultipleSizesIncludedListBox.ClearSelection();

            string tempMessage = Message.Text;

            SetupCommodity();

            Message.Text = tempMessage;
		}
        protected void Included_Click(object sender, EventArgs e)
        {
            FillCommodityNameArrayList(CommodityList.SelectedItem.Value);
            
            IncludedListBox.Items.Clear();
            IncludedListBox.ClearSelection();

            for (int i = 0; i < CommodityNameArrayList.Count; i++)
            {
                if ((((CriteriaBoxRecord)CommodityNameArrayList[i]).description.ToLower().IndexOf(gradeSearch.Text.ToLower()) != -1) && (((CriteriaBoxRecord)CommodityNameArrayList[i]).text.ToLower().IndexOf(sizeSearch.Text.ToLower()) != -1))
                    {
                        IncludedListBox.Items.Add(new ListItem(((CriteriaBoxRecord)CommodityNameArrayList[i]).key));
                    }
               
            }
        }
        protected void sizeProblemsButton_Click(object sender, System.EventArgs e)
        {
            FillCommodityNameArrayList(CommodityList.SelectedItem.Value);
            FillCommoditySizeSearchArrayList(CommodityList.SelectedItem.Value);
            int count = 0;
            bool found = false;

            SizeNotIncludedListBox.Items.Clear();
            SizeNotIncludedListBox.ClearSelection();

            MultipleSizesIncludedListBox.Items.Clear();
            MultipleSizesIncludedListBox.ClearSelection();

            for (int i = 0; i < CommodityNameArrayList.Count; i++)
            {
                count = 0;
                found = false;

                for (int j = 0; j < CommoditySizeSearchArrayList.Count; j++)
                {
                    if ((((CriteriaBoxRecord)CommodityNameArrayList[i]).description.ToLower().IndexOf(((CriteriaBoxRecord)CommoditySizeSearchArrayList[j]).description.ToLower()) != -1) && (((CriteriaBoxRecord)CommodityNameArrayList[i]).text.ToLower().IndexOf(((CriteriaBoxRecord)CommoditySizeSearchArrayList[j]).text.ToLower()) != -1))
                    {
                        found = true;
                        count++;
                    }
                }

                if (found == false)
                {
                    SizeNotIncludedListBox.Items.Add(new ListItem(((CriteriaBoxRecord)CommodityNameArrayList[i]).key));
                }

                if (count > 1)
                {
                    MultipleSizesIncludedListBox.Items.Add(new ListItem(((CriteriaBoxRecord)CommodityNameArrayList[i]).key));
                }
            }
        }
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
            FillCommodityList();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Private helpers
        private void FillCategoryList(string commodityIndex)
		{
			SqlConnection Conn = Misc.GetSQLConn();
            SqlCommand selectCMD = new SqlCommand("use " + Database + "; select * from [Commodity_Category] where Commodity_Key=@Commodity_Key order by description", Conn);
            selectCMD.Parameters.Add("Commodity_Key", commodityIndex);

			SqlDataReader myReader = selectCMD.ExecuteReader();

			if(myReader.HasRows == true)
			{				 
				CategoryList.DataSource = myReader;
				CategoryList.DataTextField = "Description";
				CategoryList.DataValueField = "Category_Key";
				CategoryList.DataBind();
			}
			else
			{
				CategoryList.ClearSelection();
				CategoryList.Items.Clear();
			}

            CategoryList.Items.Add(new ListItem("Other", "-1"));

			Conn.Close();
			Conn.Dispose();
		}

		private void FillCommodityList()
		{
			OracleConnection OracleConn = GetOracleConn();
			string oracleSQL = FormatOracle("Select Name, Cmtyidx From #COMPANY_NUMBER#.Ic_Ps_Commodity Order by Name");
			OracleCommand OracleCmd = new OracleCommand(oracleSQL, OracleConn);
			OracleCmd.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader2 = OracleCmd.ExecuteReader();

			while (myReader2.Read())
			{
				CommodityList.Items.Add(new ListItem(myReader2.GetValue(0).ToString(),myReader2.GetValue(1).ToString()));
			}

			myReader2.Close();
			myReader2.Dispose();
			OracleCmd.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();
		}

        private void FillCommodityNameArrayList(string commodityIndex)
        {
            ArrayList tempCommodityNameArrayList = new ArrayList();

            OracleConnection OracleConn = GetOracleConn();

            string oracleCommand = @"
            SELECT
	            /*Concat(Ic_Ps_Size.NameInvc, Concat(' ', Concat(Ic_Ps_Style.NameInvc, Concat(' ', Ic_Ps_Grade.NameInvc)))) as NameInvc,*/
	            Ic_Ps_Size.Name as SizeName,
				Ic_Ps_Style.Name as StyleName,
				Ic_Ps_Grade.Name as GradeName
            From
	            Ic_Ps_Style,
	            Ic_Ps_Size,
				Ic_Ps_Grade,
				Ic_Product_Produce
            Where
				Ic_Ps_Size.SizeIdx = Ic_Product_Produce.SizeIdx and
				Ic_Ps_Style.StyleIdx = Ic_Product_Produce.StyleIdx and
				Ic_Ps_Grade.GradeIdx = Ic_Product_Produce.GradeIdx and
				Ic_Product_Produce.CmtyIdx = '" + commodityIndex + @"'

            Group By
	            Ic_Ps_Size.Name,
	            Ic_Ps_Style.Name,
	            Ic_Ps_Grade.Name

            Order By Ic_Ps_Style.Name";

            OracleCommand OracleData = new OracleCommand(FormatOracle(oracleCommand), OracleConn);

            OracleData.BindByName = true;
            Oracle.DataAccess.Client.OracleDataReader myReader = OracleData.ExecuteReader();

            while (myReader.Read())
            {
                //SizeStyleList.Items.Add(new ListItem(myReader.GetString(2),myReader.GetValue(3).ToString()));
                //SizeStyleList.Items.Add(new ListItem(myReader.GetString(4) + "-" + myReader.GetString(2), myReader.GetValue(5).ToString()));
                tempCommodityNameArrayList.Add(new CriteriaBoxRecord(myReader.GetString(0) + " " + myReader.GetString(1) + " " + myReader.GetString(2), myReader.GetString(0), myReader.GetString(2)));
            }

            myReader.Close();
            myReader.Dispose();
            OracleData.Dispose();
            OracleConn.Close();
            OracleConn.Dispose();

            reportSessionManager.AddSession("CommodityName", tempCommodityNameArrayList);
        }

        private void FillCommoditySizeSearchArrayList(string commodityIndex)
        {
            ArrayList tempCommoditySizeSearchArrayList = new ArrayList();

            SqlConnection Conn = Misc.GetSQLConn();
            SqlCommand selectCMD = new SqlCommand("use " + Database + "; select Size_Search, Grade_Search from [Commodity_Category] where Commodity_Key=@Commodity_Key Order by Size_Search", Conn);
            selectCMD.Parameters.Add("Commodity_Key", commodityIndex);

            SqlDataReader myReader = selectCMD.ExecuteReader();

            while (myReader.Read())
            {
                tempCommoditySizeSearchArrayList.Add(new CriteriaBoxRecord("", myReader.GetString(0), myReader.GetString(1)));
            }

            myReader.Close();
            Conn.Close();
            Conn.Dispose();

            reportSessionManager.AddSession("CommoditySizeSearch", tempCommoditySizeSearchArrayList);
        }

        private void SetupCommodity()
        {
            if (CommodityKey != "")
            {
                for (int i = 0; i < CommodityList.Items.Count; i++)
                {
                    if (CommodityList.Items[i].Value == CommodityKey)
                    {
                        CommodityList.SelectedIndex = i;
                        CommodityList_SelectedIndexChanged(null, null);
                        CommodityList.Enabled = false;
                    }
                }
            }
        }

        private void SetTitleLine()
        {
            StringBuilder title = new StringBuilder();

            if (InputName != "" || InputName != null)
            {
                title.Append(InputName);
                title.Append(" ");
            }

            title.Append("Input Commodity-Category");

            TitleLine.InnerText = title.ToString();
        }
		#endregion
       
}
}