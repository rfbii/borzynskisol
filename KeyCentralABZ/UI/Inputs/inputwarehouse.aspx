<%@ Page language="c#" Inherits="KeyCentral.UI.Inputs.InputWarehouse" Codebehind="InputWarehouse.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Security Type</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body bgcolor="white">
			<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
				<div class="scroll" id="BodyDiv">
				<asp:label id="message" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 224px" runat="server"
				ForeColor="Red"></asp:label><asp:listbox id="availableList" style="Z-INDEX: 111; LEFT: 8px; POSITION: absolute; TOP: 96px"
				runat="server" Font-Names="Courier New" Height="120px" Width="322px"></asp:listbox><asp:button id="add" style="Z-INDEX: 103; LEFT: 352px; POSITION: absolute; TOP: 96px" runat="server"
				Font-Names="Tahoma" Height="24" Width="75px" Text=">" onclick="Add_Click"></asp:button><asp:button id="remove" style="Z-INDEX: 105; LEFT: 352px; POSITION: absolute; TOP: 128px" runat="server"
				Height="24" Width="75" Text="<" onclick="Remove_Click"></asp:button><asp:button id="addAll" style="Z-INDEX: 106; LEFT: 352px; POSITION: absolute; TOP: 160px" runat="server"
				Height="24" Width="75px" Text=">>" CausesValidation="False" onclick="addAll_Click"></asp:button><asp:button id="removeAll" style="Z-INDEX: 104; LEFT: 352px; POSITION: absolute; TOP: 192px"
				runat="server" Height="24" Width="75" Text="<<" CausesValidation="False" onclick="removeAll_Click"></asp:button><asp:label id="availableLabel" style="Z-INDEX: 110; LEFT: 112px; POSITION: absolute; TOP: 80px"
				runat="server" Font-Names="Tahoma" Width="168px" Font-Size="X-Small">Warehouses Available</asp:label><asp:textbox id="sTypeKey" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 72px" runat="server"
				Width="84px" Visible="False"></asp:textbox><asp:button id="Return" style="Z-INDEX: 107; LEFT: 312px; POSITION: absolute; TOP: 256px" tabIndex="4"
				runat="server" Height="24px" Width="160px" Text="Return to Menu" onclick="Return_Click"></asp:button>
			<div style="DISPLAY: inline; Z-INDEX: 108; LEFT: 504px; WIDTH: 168px; POSITION: absolute; TOP: 80px; HEIGHT: 24px"
				align="center"><font face="Tahoma" size="2">Warehouses <font face="Tahoma" size="2">
						Selected </font></font>
			</div>
			<asp:listbox id="selectedList" style="Z-INDEX: 109; LEFT: 448px; POSITION: absolute; TOP: 96px"
				tabIndex="2" runat="server" Font-Names="Courier New" Height="120px" Width="322px"></asp:listbox>
			</div>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
		</form>
	</body>
</html>
