using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using KeyCentral.Functions;
using Oracle.DataAccess.Client;

namespace KeyCentral.UI.Inputs
{
	/// <summary>
	/// Summary description for Security Type.
	/// </summary>
	/// 
	
	public partial class inputRemoveCheckWritingUser : BasePage
	{
		#region Declares

		#endregion

		#region events
		protected void Page_Load(object sender, System.EventArgs e)
		{
            deleteAllUsers.Message = "Do you want to delete all users?";
        }

        protected void userList_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            Company_Key.Text = Misc.ParseTildaString(userList.SelectedItem.Value, 0);
            Bank_Name_Key.Text = Misc.ParseTildaString(userList.SelectedItem.Value, 1);
            SelectedUser.Text = userList.SelectedItem.Text.ToString();
            deleteSelectedUser.Message = "Do you want to delete " + userList.SelectedItem.Text;
            deleteSelectedUser.Enabled = true;
        }

        protected void Return_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Setup");
		}
        protected void deleteSelectedUser_Click(object sender, System.EventArgs e)
		{
            SqlConnection Conn = GetSQLConn();
            SqlCommand removeCMD = new SqlCommand("use BankBalance;delete from [Check_Writing_User] where Company_Key = @Company_Key and Bank_Name_Key = @Bank_Name_Key", Conn);
            removeCMD.Parameters.AddWithValue("@Company_Key", Company_Key.Text);
            removeCMD.Parameters.AddWithValue("@Bank_Name_Key", Bank_Name_Key.Text);
            removeCMD.ExecuteNonQuery();
            SelectedUser.Text = "";
            Company_Key.Text = "";
            Bank_Name_Key.Text = "";
            FillUserList();
            Message.Text = "Selected user has been deleted.";
            deleteSelectedUser.Enabled = false;
		}
        protected void deleteAllUsers_Click(object sender, System.EventArgs e)
        {
            SqlConnection Conn = GetSQLConn();
            SqlCommand removeCMD = new SqlCommand("use BankBalance; delete from [Check_Writing_User]", Conn);
            removeCMD.ExecuteNonQuery();
            SelectedUser.Text = "";
            Company_Key.Text = "";
            Bank_Name_Key.Text = "";
            FillUserList();
            Message.Text = "All users have been deleted.";
        }
		

		#endregion
			
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			FillUserList();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Private helpers


        private void FillUserList()
        {
            string companyStr = "";

            //Clears list...
            userList.ClearSelection();
            userList.Items.Clear();
            //... and defaults no-selection on new list items.
            userList.SelectedIndex = -1;

            //SQL pull (w/Oracle in the get-functions) to Retrive the Company and Bank name & keys.
            SqlConnection Conn = GetSQLConn();
            SqlCommand selectCMD = new SqlCommand("use BankBalance;select * from [Check_Writing_User]", Conn);

            SqlDataReader myReader = selectCMD.ExecuteReader();

            while (myReader.Read())
            {
                companyStr = GetCompany(myReader.GetInt32(0).ToString());
                userList.Items.Add(new ListItem(myReader.GetString(2) + "~" + Misc.ParseTildaString(companyStr, 0) + "~" + GetBankName(Misc.ParseTildaString(companyStr, 1), myReader.GetInt32(1).ToString()), myReader.GetInt32(0).ToString() + "~" + myReader.GetInt32(1).ToString()));
            }

            Conn.Close();
            Conn.Dispose();
        }

        private string GetBankName(string CompanyKey, string BankKey)
        {
            string returnBank = "";

            OracleConnection OracleConn = Misc.GetOracleConn(CompanyKey);
		    DataSet bankData = new DataSet();
			
            string selectCMD = Misc.FormatOracle(CompanyKey + "_rpt", @"SELECT 
		    Fc_Name.LastCoName,
            Bank.BankNameidx
            from Bank,
                 Fc_Name
            Where Bank.BankNameidx = Fc_Name.Nameidx and Bank.BankNameidx = :BankKey
            ");

            OracleCommand OracleCmd = new OracleCommand(selectCMD, OracleConn);
            OracleCmd.Parameters.Add(":BankKey", BankKey);
            OracleCmd.BindByName = true;
            OracleDataReader myReader = OracleCmd.ExecuteReader();

            while (myReader.Read())
            {
                returnBank = myReader.GetValue(0).ToString();
                
            }

            myReader.Close();
            Misc.CleanUpOracle(OracleConn);

            return returnBank;
        }

        private string GetCompany(string companyKey)
        {
            string returnCompany = "";
           
            SqlConnection Conn = GetSQLConn();
            SqlCommand selectCMD = new SqlCommand("use KeyCentral;select [Company].Company_Name, [Company].Report_Number from [Company] where Company_Key=@Company_Key", Conn);
            selectCMD.Parameters.Add("@Company_Key", companyKey);

            SqlDataReader myReader = selectCMD.ExecuteReader();

            while (myReader.Read())
            {
                returnCompany = myReader.GetValue(0).ToString() + "~" + myReader.GetValue(1).ToString();
            }

            myReader.Close();
            myReader.Dispose();

            return returnCompany;

        }
		
		#endregion	
		
	}
}
