using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using KeyCentral.Functions;

namespace KeyCentralLib
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:GrowerId runat=server></{0}:GrowerId>")]
    public class GrowerId : WebControl
    {
        protected DynamicCriteriaBox GrowerIdBox;

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                String s = (String)ViewState["Text"];
                return ((s == null) ? String.Empty : s);
            }

            set
            {
                ViewState["Text"] = value;
            }
        }

        public ArrayList Keys
        {
            get { return GrowerIdBox.Keys; }
            set { Page.Session.Add("Grower:Criteria", value); }
        }

        protected override void OnInit(EventArgs e)
        {
            CreateControls();
            base.OnInit(e);
        }
        protected override void RenderContents(HtmlTextWriter output)
        {
            output.Write(@"<table>
	<tr>
		<td>Grower ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Name
		</td>
	</tr>
	<tr>
		<td>
			");
            GrowerIdBox.RenderControl(output);
            //output.Write("render Dynamic Criteria Box");

            output.Write(@"
		</td>
	</tr>
</table>");
        }

        private void CreateControls()
        {
            GrowerIdBox = new DynamicCriteriaBox();
            GrowerIdBox.ID = this.UniqueID + this.ClientIDSeparator + "GrowerIdBox";

            GrowerIdBox.MaxTextLength = 6;
            GrowerIdBox.OracleCommand = "Select NameIdx,LastCoName From #COMPANY_NUMBER#.Ga_Grower,#COMPANY_NUMBER#.Fc_Name where lower(Fc_Name.Id) = lower(:value) and Fc_Name.NameIdx = GA_Grower.GrowerNameIdx";
            GrowerIdBox.OracleCommandLookup = "Select NameIdx,LastCoName,Fc_Name.Id From #COMPANY_NUMBER#.Fc_Name where Fc_Name.NameIdx = :value";
            GrowerIdBox.NotFoundMessage = "The Grower ID '#input#' could not be found.";
            GrowerIdBox.AlreadyEntered = "The Grower ID '#input#' is already in the list.";
            GrowerIdBox.LookupSearch = "Grower";

            Security.BuildLimitedToKeys(Page.Session, "grower", "GrowerLimit");
            GrowerIdBox.LimitedToKeys = "GrowerLimit";
            this.Controls.Add(GrowerIdBox);
        }
    }
}
