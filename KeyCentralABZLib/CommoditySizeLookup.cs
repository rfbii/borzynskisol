using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using KeyCentral.Functions;

namespace KeyCentralLib
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:CommoditySizeLookup runat=server></{0}:CommoditySizeLookup>")]
    public class CommoditySizeLookup : WebControl
    {
        protected DynamicCriteriaBox CommoditySizeLookupBox;
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                String s = (String)ViewState["Text"];
                return ((s == null) ? String.Empty : s);
            }

            set
            {
                ViewState["Text"] = value;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            CreateControls();
            base.OnInit(e);
        }
        protected override void RenderContents(HtmlTextWriter output)
        {
            output.Write(@"<table>
	<tr>
		<td>Commodity - Style - Size
		</td>
	</tr>
	<tr>
		<td>");
            CommoditySizeLookupBox.RenderControl(output);
            //output.Write("render Dynamic Criteria Box");

            output.Write(@"</td>
	</tr>
</table>");
        }
        public ArrayList Keys
        {
            get { return CommoditySizeLookupBox.Keys; }
            set
            {
                Page.Session.Add("Commodity:Criteria", value);

            }
        }

        private void CreateControls()
        {
            CommoditySizeLookupBox = new DynamicCriteriaBox();
            CommoditySizeLookupBox.ID = this.UniqueID + this.ClientIDSeparator + "CommoditySizeLookupBox";

            CommoditySizeLookupBox.MaxTextLength = 20;
            //CommoditySizeLookupBox.OracleCommand = "Select Ic_Warehouse.WarehouseIdx,Fc_Name_Location.City,Fc_Name_Location.Descr From #COMPANY_NUMBER#.Ic_Warehouse,#COMPANY_NUMBER#.Fc_Name_Location where Fc_Name_Location.Descr = :value and Ic_Warehouse.NameIdx = Fc_Name_Location.NameIdx and Ic_Warehouse.NameLocationSeq = Fc_Name_Location.NameLocationSeq";
            CommoditySizeLookupBox.OracleCommandLookup = "select Ic_Ps_Size.SizeIdx, Ic_Ps_Commodity.Name || ' ' || Ic_Ps_Style.Name || ' ' || Ic_Ps_Size.Name from Ic_Ps_Commodity ,Ic_Ps_Size ,Ic_Ps_Style where Ic_Ps_Size.CmtyIdx = Ic_Ps_Commodity.CmtyIdx and Ic_Ps_Style.StyleIdx = Ic_Ps_Size.StyleIdx and Ic_Ps_Size.SizeIdx=:value";
            CommoditySizeLookupBox.NotFoundMessage = "The Size '#input#' could not be found.";
            CommoditySizeLookupBox.AlreadyEntered = "The Size '#input#' is already in the list.";
            CommoditySizeLookupBox.LookupSearch = "CommoditySize";
            CommoditySizeLookupBox.TextWidth = 0;
            CommoditySizeLookupBox.DescriptionWidth = 200;
            CommoditySizeLookupBox.DisplayListSize = 3;

            Security.BuildLimitedToKeys(Page.Session, "CommoditySize", "CommoditySizeLimit");
            CommoditySizeLookupBox.LimitedToKeys = "CommoditySizeLimit";
            this.Controls.Add(CommoditySizeLookupBox);
        }
    }
}
