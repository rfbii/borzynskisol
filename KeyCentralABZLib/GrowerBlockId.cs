using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

namespace KeyCentralLib
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:GrowerBlockId runat=server></{0}:GrowerBlockId>")]
    public class GrowerBlockId : WebControl
    {
        protected DynamicCriteriaBox GrowerBlockIdBox;

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                String s = (String)ViewState["Text"];
                return ((s == null) ? String.Empty : s);
            }

            set
            {
                ViewState["Text"] = value;
            }
        }

        public ArrayList Keys
        {
            get { return GrowerBlockIdBox.Keys; }
            set { Page.Session.Add("GrowerBlock:Criteria", value); }
        }

        protected override void OnInit(EventArgs e)
        {
            CreateControls();
            base.OnInit(e);
        }
        protected override void RenderContents(HtmlTextWriter output)
        {
            output.Write(@"<table>
	<tr>
		<td>GrowerBlock  
			ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Description
		</td>
	</tr>
	<tr>
		<td>
			");
            GrowerBlockIdBox.RenderControl(output);
            //output.Write("render Dynamic Criteria Box");

            output.Write(@"
		</td>
	</tr>
</table>");
        }

        private void CreateControls()
        {
            GrowerBlockIdBox = new DynamicCriteriaBox();
            GrowerBlockIdBox.ID = this.UniqueID + this.ClientIDSeparator + "GrowerBlockIdBox";

            GrowerBlockIdBox.OracleCommand = "Select GaBlockIdx,NVL (Name,' ') From #COMPANY_NUMBER#.Ga_Block where lower(Ga_Block.Id) = Lower(:value)";
            GrowerBlockIdBox.NotFoundMessage = "The Grower Block ID '#input#' could not be found.";
            GrowerBlockIdBox.AlreadyEntered = "The Grower Block ID '#input#' is already in the list.";
            GrowerBlockIdBox.OracleCommandLookup = "Select GaBlockIdx,NVL (Name,' '),NVL (Ga_Block.Id,' ') From #COMPANY_NUMBER#.Ga_Block where Ga_Block.GaBlockIdx = :value";

            GrowerBlockIdBox.LookupSearch = "GrowerBlock";
            this.Controls.Add(GrowerBlockIdBox);
        }
    }
}
