using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

namespace KeyCentralLib
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:VendorId runat=server></{0}:VendorId>")]
    public class VendorId : WebControl
    {
        protected DynamicCriteriaBox VendorIdBox;

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                String s = (String)ViewState["Text"];
                return ((s == null) ? String.Empty : s);
            }

            set
            {
                ViewState["Text"] = value;
            }
        }

        public ArrayList Keys
        {
            get { return VendorIdBox.Keys; }
            set { Page.Session.Add("VendorName:Criteria", value); }
        }

        protected override void OnInit(EventArgs e)
        {
            CreateControls();
            base.OnInit(e);
        }
        protected override void RenderContents(HtmlTextWriter output)
        {
            output.Write(@"<table>
	<tr>
		<td>Vendor 
			ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Description
		</td>
	</tr>
	<tr>
		<td>
			");
            VendorIdBox.RenderControl(output);
            //output.Write("render Dynamic Criteria Box");

            output.Write(@"
		</td>
	</tr>
</table>");
        }

        private void CreateControls()
        {
            VendorIdBox = new DynamicCriteriaBox();
            VendorIdBox.ID = this.UniqueID + this.ClientIDSeparator + "VendorIdBox";

            VendorIdBox.OracleCommand = "Select NameIdx,NVL (LastCoName,' ') From #COMPANY_NUMBER#.Fc_Name where lower(Fc_Name.Id) = Lower(:value)";
            VendorIdBox.NotFoundMessage = "The Vendor ID '#input#' could not be found.";
            VendorIdBox.AlreadyEntered = "The Vendor ID '#input#' is already in the list.";
            VendorIdBox.OracleCommandLookup = "Select NameIdx,NVL (LastCoName,' '),NVL (Fc_Name.Id,' ') From #COMPANY_NUMBER#.Fc_Name where Fc_Name.NameIdx = :value";
            VendorIdBox.LookupSearch = "VendorName";
            this.Controls.Add(VendorIdBox);
        }
    }
}
