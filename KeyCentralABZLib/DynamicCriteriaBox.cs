using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Web.UI.HtmlControls;
using Oracle.DataAccess.Client;
using System.Data;
using System.Drawing;
using System.Data.OleDb;
using System.Configuration;
using KeyCentral.Lookups;
using KeyCentral.Functions;

namespace KeyCentralLib
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:DynamicCriteriaBox runat=server></{0}:DynamicCriteriaBox>")]
    public class DynamicCriteriaBox : WebControl
    {
        #region declares
        protected System.Web.UI.HtmlControls.HtmlGenericControl mainDiv;
        protected ArrayList criteria;
        protected int maxTextLength = 13;
        protected int textWidth = 100;
        protected int descriptionWidth = 300;
        protected int displayListSize = 4;
        protected string oracleCommand = String.Empty;
        protected string oracleCommandLookup = String.Empty;
        protected string sqlCommand = String.Empty;
        protected string notFoundMessage = String.Empty;
        protected string alreadyEntered = String.Empty;
        protected string rowFocus = String.Empty;
        protected string lookupSearch = String.Empty;
        protected bool dataGood = true;
        protected string limitedToKeys = String.Empty;
        protected string validKeys = String.Empty;
        #endregion

        #region public Paramerters
        public bool DataGood
        {
            get { return dataGood; }
        }
        public ArrayList Keys
        {
            get
            {
                /*
                ArrayList keys = new ArrayList();
                foreach( CriteriaBoxRecord key in criteria)
                    keys.Add(key.key);

                return keys;*/
                return criteria;
            }
        }
        public string NotFoundMessage
        {
            get { return notFoundMessage; }
            set { notFoundMessage = value.Replace("\"", "'"); }
        }
        public string AlreadyEntered
        {
            get { return alreadyEntered; }
            set { alreadyEntered = value.Replace("\"", "'"); }
        }
        public string OracleCommand
        {
            get { return oracleCommand; }
            set { oracleCommand = value; }
        }
        /// <summary>
        /// select Key,Desc,TextBox where key=:value
        /// </summary>
        public string OracleCommandLookup
        {
            get { return oracleCommandLookup; }
            set { oracleCommandLookup = value; }
        }
        public string SqlCommand
        {
            get { return sqlCommand; }
            set { sqlCommand = value; }
        }
        public string LookupSearch
        {
            get { return lookupSearch; }
            set { lookupSearch = value; }
        }
        public int MaxTextLength
        {
            get { return maxTextLength; }
            set { maxTextLength = value; }
        }
        public int TextWidth
        {
            get { return textWidth; }
            set { textWidth = value; }
        }
        public int DescriptionWidth
        {
            get { return descriptionWidth; }
            set { descriptionWidth = value; }
        }
        public int DisplayListSize
        {
            get { return displayListSize; }
            set { displayListSize = value; }
        }
        public string LimitedToKeys
        {
            get { return limitedToKeys; }
            set { limitedToKeys = value; }
        }

        /// <summary>
        /// Sets or Gets the session var that holds the Valid Keys ArrayList.
        /// </summary>
        public string ValidKeys
        {
            get { return validKeys; }
            set { validKeys = value; }
        }

        #endregion
        private System.Web.SessionState.HttpSessionState Session { get { return Context.Session; } }

        #region Events
        protected override void OnLoad(EventArgs e)
        {
            criteria.Clear();

            if (Session[this.lookupSearch + ":Criteria"] != null)//check for Values to load
            {
                if (((ArrayList)Session[this.lookupSearch + ":Criteria"]).Count > 0)
                    criteria = (ArrayList)Session[this.lookupSearch + ":Criteria"];

                //We want to remove it even if it is empty
                Session.Remove(this.lookupSearch + ":Criteria");
            }
            else if (Session[this.lookupSearch + ":Lookup"] != null)//Check for LookupValues
                LoadFromLookup();
            else//Get user input
                ReadTextBoxes(mainDiv, criteria);

            if (Session[limitedToKeys] != null && ((ArrayList)Session[limitedToKeys]).Count > 0)
            {
                //If we don't have anything selected load it from their limitation
                if (criteria.Count == 0)
                    LoadFromLimit();
                else
                    CheckInputsVsLimit(criteria, (ArrayList)Session[limitedToKeys]);
            }
            if (Session[ValidKeys] != null && ((ArrayList)Session[ValidKeys]).Count > 0)
            {
                CheckInputsVsLimit(criteria, (ArrayList)Session[ValidKeys]);
            }

            mainDiv.Controls.Clear();
            CreateTextBoxes(mainDiv, criteria);

            SetMainDivAtributes();


            if (Page.IsPostBack == true)
                ViewState.Clear();

            WriteViewState(criteria);

            //Sets the focus 
            if (Page.IsPostBack == true)
            {
                if (rowFocus != "")
                    SetFocus(rowFocus);
                else
                    if (Page.Request["__EVENTTARGET"].IndexOf(this.UniqueID) > -1)
                        if (Page.Request["__EVENTTARGET"].IndexOf("_Input:") > -1)
                            SetFocus(GetNextTextBoxID(Page.Request["__EVENTTARGET"]));
            }

            base.OnLoad(e);
        }
        protected override void OnInit(EventArgs e)
        {
            criteria = new ArrayList();

            CreateControls();
            base.OnInit(e);
        }
        protected override void OnPreRender(System.EventArgs e)
        {

        }
        protected override void RenderContents(HtmlTextWriter output)
        {
            mainDiv.RenderControl(output);
            //output.Write("Rendering DynamicCriteriabox");
        }
        #endregion

        #region private Helpers
        private void CreateControls()
        {
            mainDiv = new HtmlGenericControl();
            mainDiv.ID = this.UniqueID + this.ClientIDSeparator + "mainDiv";
            mainDiv.Style.Add("BORDER-RIGHT", "1px solid");
            mainDiv.Style.Add("BORDER-TOP", "1px solid");
            mainDiv.Style.Add("BORDER-LEFT", "1px solid");
            mainDiv.Style.Add("BORDER-BOTTOM", "1px solid");
            mainDiv.Style.Add("TABLE-LAYOUT", "fixed");
            mainDiv.Style.Add("OVERFLOW", "auto");
            mainDiv.Style.Add("WIDTH", "430px");
            mainDiv.Style.Add("BORDER-COLLAPSE", "separate");
            mainDiv.Style.Add("HEIGHT", "100px");
            this.Controls.Add(mainDiv);
        }

        private void LoadFromLimit()
        {
            criteria = (ArrayList)Session[limitedToKeys];

            for (int x = 0; x < criteria.Count; x++)
            {
                if (((CriteriaBoxRecord)criteria[x]).text == "")
                {
                    criteria[x] = OracleLookupValue((CriteriaBoxRecord)criteria[x]);
                }
            }
        }

        private void LoadFromLookup()
        {
            ArrayList newCriteria = new ArrayList();

            foreach (string key in (ArrayList)Session[this.lookupSearch + ":Lookup"])
            {
                newCriteria.Add(OracleLookupValue(new CriteriaBoxRecord(key, "", "")));
            }

            Session.Remove(this.lookupSearch + ":Lookup");
            if (newCriteria.Count > 0)
                criteria = newCriteria;
        }

        private void CheckInputsVsLimit(ArrayList criteria, ArrayList limited)
        {
            for (int x = criteria.Count - 1; x >= 0; x--)
            {
                if (RecordInArray(limited, (CriteriaBoxRecord)criteria[x]) == false)
                    criteria.RemoveAt(x);
            }
        }

        private bool RecordInArray(ArrayList list, CriteriaBoxRecord value)
        {
            foreach (CriteriaBoxRecord record in list)
                if (value.key == record.key)
                    return true;

            return false;
        }

        private void CreateTextBoxes(HtmlGenericControl parentDiv, ArrayList values)
        {
            int controlIndex = 0;
            foreach (CriteriaBoxRecord criteriaBoxRecord in values)
                controlIndex = AddTextBox(parentDiv, criteriaBoxRecord, controlIndex);

            //Add the two empty TextBoxes
            AddTextBox(parentDiv, new CriteriaBoxRecord("", "", ""), controlIndex);
            AddTextBox(parentDiv, new CriteriaBoxRecord("", "", ""), controlIndex + 1);
        }

        private int AddTextBox(HtmlGenericControl control, CriteriaBoxRecord criteriaBoxRecord, int controlIndex)
        {
            TextBox box = new TextBox();

            box.Text = criteriaBoxRecord.text;
            box.ID = this.UniqueID + this.ClientIDSeparator +  "Input:" + controlIndex.ToString();
            box.AutoPostBack = true;
            box.MaxLength = maxTextLength;
            box.Width = this.textWidth;
            box.Attributes.Add("EnterAsTab", "");

            //Add the right click menu if needed
            if (lookupSearch != "")
                box.Attributes.Add("LookupSearch", lookupSearch);

            //Add the TextBox we just created
            control.Controls.Add(new System.Web.UI.LiteralControl("\n"));
            control.Controls.Add(box);

            //<textarea id="" wrap="off" readonly style="BORDER-RIGHT: gray 1px inset; TABLE-LAYOUT: fixed; BORDER-TOP: gray 1px inset; OVERFLOW: hidden; BORDER-LEFT: gray 1px inset; WIDTH: 50px; BORDER-BOTTOM: gray 1px inset; HEIGHT: 20px">text area one</textarea>
            //Add the description field
            System.Web.UI.LiteralControl desc = new System.Web.UI.LiteralControl();

            //Add the right click menu if needed
            string descLookupAttribute = "";
            if (lookupSearch != "")
                descLookupAttribute = "LookupSearch='" + lookupSearch + "'";

            //desc.Text = "<div nowrap style='WIDTH: 100px;OVERFLOW: hidden'>"+ criteriaBoxRecord.description+"</div>";
            desc.Text = "<textarea id='" + UniqueID + ":Description:" + controlIndex.ToString() + "' wrap='off' readonly style='BORDER-RIGHT: gray 1px inset; TABLE-LAYOUT: fixed; BORDER-TOP: gray 1px inset; OVERFLOW: hidden; BORDER-LEFT: gray 1px inset; WIDTH: " + descriptionWidth + "px; BORDER-BOTTOM: gray 1px inset; HEIGHT: 20px';font-family: 'Arial' " + descLookupAttribute + " >" + criteriaBoxRecord.description + "</textarea>\n";

            //Add the TextBox we just created
            control.Controls.Add(new System.Web.UI.LiteralControl("\n"));
            control.Controls.Add(desc);

            control.Controls.Add(new System.Web.UI.LiteralControl("<BR>\n"));

            return controlIndex + 1;
        }

        private void ReadTextBoxes(HtmlGenericControl parentDiv, ArrayList values)
        {
            for (int x = 0; Page.Request[UniqueID + "_Input:" + x.ToString()] != null; x++)
            {
                if (ViewState["DynamicCriteriaBox:" + x.ToString()] == null || (Page.Request[UniqueID + "_Input:" + x.ToString()] != ((CriteriaBoxRecord)ViewState["DynamicCriteriaBox:" + x.ToString()]).text))
                    CheckChange(values, Page.Request[UniqueID + "_Input:" + x.ToString()], x);
                else
                    values.Add(((CriteriaBoxRecord)ViewState["DynamicCriteriaBox:" + x.ToString()]));
            }
        }

        private void SetMainDivAtributes()
        {
            if (lookupSearch != "")
                mainDiv.Attributes.Add("LookupSearch", lookupSearch);

            //Adjust the size depending on the size of the inner guys
            if ((criteria.Count + 2) > displayListSize)
                mainDiv.Style["WIDTH"] = (TextWidth + DescriptionWidth + 30).ToString() + "px";
            else
                mainDiv.Style["WIDTH"] = (TextWidth + DescriptionWidth + 20).ToString() + "px";

            mainDiv.Style["HEIGHT"] = (displayListSize * 25).ToString() + "px";
        }

        private void CheckChange(ArrayList values, string text, int row)
        {
            CriteriaBoxRecord criteriaBoxRecord;

            //Make sure it isn't blank
            if (text.Trim() == "")
            {
                if (ViewState["DynamicCriteriaBox:" + row.ToString()] != null)
                    if (((CriteriaBoxRecord)ViewState["DynamicCriteriaBox:" + row.ToString()]).text.Trim() != "")
                        rowFocus = UniqueID + "_Input:" + row.ToString();

                return;
            }

            //Make sure it isn't in the list already
            if (TextIsPresent(values, text) == true)
            {
                if (alreadyEntered != "")
                {
                    SetAlert(alreadyEntered.Replace("#input#", text), UniqueID + "_Input:" + row.ToString());
                    return;
                }
                else
                    throw (new Exception("'alreadyEntered' not provided for control " + UniqueID));
            }

            criteriaBoxRecord = new CriteriaBoxRecord(text);

            if (this.oracleCommand != "")
            {
                values.Add(OracleCheckValue(criteriaBoxRecord, row));
            }
            else if (sqlCommand != "")
            {

            }
            else
            {
                throw (new Exception("You must give an Oracle or SQL command!"));
                //ERROR
            }
        }

        private void WriteViewState(ArrayList values)
        {
            for (int x = 0; x < values.Count; x++)
                ViewState.Add("DynamicCriteriaBox:" + x.ToString(), values[x]);
        }

        private CriteriaBoxRecord OracleCheckValue(CriteriaBoxRecord record, int row)
        {
            OracleConnection OracleConn = KeyCentral.Functions.Misc.GetOracleConn(Session);
            OracleCommand OracleData = new OracleCommand(KeyCentral.Functions.Misc.FormatOracle(Session, oracleCommand), OracleConn);
            DataSet dsRet = new DataSet();

            OracleData.BindByName = true;
            OracleData.Parameters.Add(":value", record.text);

            Oracle.DataAccess.Client.OracleDataReader reader = OracleData.ExecuteReader();
            if (reader.Read())
            {
                record.key = reader.GetInt32(0).ToString();
                record.description = reader.GetString(1);
            }
            else
            {
                if (notFoundMessage != "")
                    SetAlert(notFoundMessage.Replace("#input#", record.text), this.UniqueID + "_Input:" + row.ToString());
                else
                    throw (new Exception("'notFoundMessage' not provided for control " + this.UniqueID));
            }

            //cleanup
            reader.Close();
            reader.Dispose();
            OracleData.Dispose();
            OracleConn.Close();
            OracleConn.Dispose();
            return record;
        }

        private CriteriaBoxRecord OracleLookupValue(CriteriaBoxRecord record)
        {
            OracleConnection OracleConn = Misc.GetOracleConn(Session);
            OracleCommand OracleData = new OracleCommand(Misc.FormatOracle(Session, oracleCommandLookup), OracleConn);
            DataSet dsRet = new DataSet();

            try
            {
                OracleData.BindByName = true;
                OracleData.Parameters.Add(":value", record.key);

                Oracle.DataAccess.Client.OracleDataReader reader = OracleData.ExecuteReader();
                if (reader.Read())
                {
                    record.key = reader.GetInt32(0).ToString();
                    if (!reader.IsDBNull(1))
                        record.description = reader.GetString(1);
                    else
                        record.description = "";

                    if (reader.FieldCount > 2)
                        if (!reader.IsDBNull(2))
                            record.text = reader.GetString(2);
                }
                else
                {
                    if (notFoundMessage != "")
                        SetAlert(notFoundMessage.Replace("#input#", record.text), this.UniqueID + ":LimitedToKeys");
                    else
                        throw (new Exception("'notFoundMessage' not provided for control " + this.UniqueID));
                }
                //cleanup
                reader.Close();
                reader.Dispose();
                OracleData.Dispose();
                OracleConn.Close();
                OracleConn.Dispose();
            }
            catch (Exception ex)
            {
                Page.Response.Write("Command = " + KeyCentral.Functions.Misc.FormatOracle(Session, oracleCommandLookup) + "<br> key=" + record.key + "<BR> Error=" + ex.Message);
            }

            return record;
        }

        private bool TextIsPresent(ArrayList values, string text)
        {
            for (int x = 0; x < values.Count; x++)
                if (((CriteriaBoxRecord)values[x]).text.Trim() == text.Trim())
                    return true;

            return false;
        }

        private string GetNextTextBoxID(string changingTextBox)
        {
            return UniqueID + "_Input:" + (Int32.Parse(changingTextBox.Remove(0, changingTextBox.IndexOf("_Input:") + 7)) + 1).ToString();
        }
        #endregion

        #region private Javascript functions
        private void SetFocus(string ctrl)
        {
            string s = "<SCRIPT language=\"javascript\">" +
                "document.getElementById('" + ctrl + "').focus() </SCRIPT>";

            Page.RegisterStartupScript("focus", s);
        }
        private void SetAlert(string message, string controlToSetFocus)
        {
            string s = "<SCRIPT language=\"javascript\">" +
                "alert(\"" + message + "\"); </SCRIPT>";

            Page.RegisterStartupScript("NotFoundAlert", s);

            rowFocus = controlToSetFocus;
            dataGood = false;
        }
        private void SetAlert(string message)
        {
            SetAlert(message, "");
        }
        #endregion

        /*
         <LINK href="DynamicCriteriaBox.css" type="text/css" rel="stylesheet">
<div id="mainDiv" style="BORDER-RIGHT: 1px solid; TABLE-LAYOUT: fixed; BORDER-TOP: 1px solid; OVERFLOW: auto; BORDER-LEFT: 1px solid; WIDTH: 430px; BORDER-BOTTOM: 1px solid; BORDER-COLLAPSE: separate; HEIGHT: 100px"
	runat="server"></div>*/
    }
}
