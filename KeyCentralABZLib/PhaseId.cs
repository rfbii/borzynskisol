using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

namespace KeyCentralLib
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:PhaseId runat=server></{0}:PhaseId>")]
    public class PhaseId : WebControl
    {
        protected DynamicCriteriaBox PhaseIdBox;

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                String s = (String)ViewState["Text"];
                return ((s == null) ? String.Empty : s);
            }

            set
            {
                ViewState["Text"] = value;
            }
        }

        public ArrayList Keys
        {
            get { return PhaseIdBox.Keys; }
            set { Page.Session.Add("Phase:Criteria", value); }
        }

        protected override void OnInit(EventArgs e)
        {
            CreateControls();
            base.OnInit(e);
        }
        protected override void RenderContents(HtmlTextWriter output)
        {
            output.Write(@"<table>
	<tr>
		<td>Phase 
			ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Description
		</td>
	</tr>
	<tr>
		<td>
			");
            PhaseIdBox.RenderControl(output);
            //output.Write("render Dynamic Criteria Box");

            output.Write(@"
		</td>
	</tr>
</table>");
        }

        private void CreateControls()
        {
            PhaseIdBox = new DynamicCriteriaBox();
            PhaseIdBox.ID = this.UniqueID + this.ClientIDSeparator + "PhaseIdBox";

            PhaseIdBox.OracleCommand = "Select PhaseIdx,NVL (Name,' ') From #COMPANY_NUMBER#.Ca_Phase where lower(Ca_Phase.Id) = Lower(:value)";
            PhaseIdBox.NotFoundMessage = "The Phase ID '#input#' could not be found.";
            PhaseIdBox.AlreadyEntered = "The Phase ID '#input#' is already in the list.";
            PhaseIdBox.OracleCommandLookup = "Select PhaseIdx,NVL (Name,' '),NVL (Ca_Phase.id,' ') From #COMPANY_NUMBER#.Ca_Phase where Ca_Phase.PhaseIdx = :value";
            PhaseIdBox.LookupSearch = "Phase";
            this.Controls.Add(PhaseIdBox);
        }
    }
}
