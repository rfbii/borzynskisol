using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using KeyCentral.Functions;

namespace KeyCentralLib
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:CommodityLookup runat=server></{0}:CommodityLookup>")]
    public class CommodityLookup : WebControl
    {

        protected DynamicCriteriaBox CommodityLookupBox;

        public ArrayList Keys
        {
            get { return CommodityLookupBox.Keys; }
            set
            {
                Page.Session.Add("Commodity:Criteria", value);

            }
        }
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                String s = (String)ViewState["Text"];
                return ((s == null) ? String.Empty : s);
            }

            set
            {
                ViewState["Text"] = value;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            CreateControls();
            base.OnInit(e);
        }
        protected override void RenderContents(HtmlTextWriter output)
        {
            output.Write(@"<table>
	<tr>
		<td>Commodity
		</td>
	</tr>
	<tr>
		<td>");
            CommodityLookupBox.RenderControl(output);
            //output.Write("render Dynamic Criteria Box");

            output.Write(@"</td>
	</tr>
</table>");
        }

        private void CreateControls()
        {
            CommodityLookupBox = new DynamicCriteriaBox();
            CommodityLookupBox.ID = this.UniqueID + this.ClientIDSeparator + "CommodityLookupBox";

            CommodityLookupBox.MaxTextLength = 20;
            CommodityLookupBox.OracleCommand = "Select Ic_Ps_Commodity.CmtyIdx as \"Keys\",Ic_Ps_Commodity.Name,Ic_Ps_Commodity.Name from	#COMPANY_NUMBER#.Ic_Ps_Commodity where Ic_Ps_Commodity.Name = :value  order by 2";
            CommodityLookupBox.NotFoundMessage = "The Commodity '#input#' could not be found.";
            CommodityLookupBox.OracleCommandLookup = "Select Ic_Ps_Commodity.CmtyIdx ,Ic_Ps_Commodity.Name,Ic_Ps_Commodity.Name from	#COMPANY_NUMBER#.Ic_Ps_Commodity where Ic_Ps_Commodity.CmtyIdx = :value";
            CommodityLookupBox.AlreadyEntered = "The Commodity '#input#' is already in the list.";
            CommodityLookupBox.LookupSearch = "Commodity";
            CommodityLookupBox.TextWidth = 150;
            CommodityLookupBox.DescriptionWidth = 0;
            CommodityLookupBox.DisplayListSize = 3;
            Security.BuildLimitedToKeys(Page.Session, "Commodity", "CommodityLimit");
            CommodityLookupBox.LimitedToKeys = "CommodityLimit";
            this.Controls.Add(CommodityLookupBox);
        }
    }
}
