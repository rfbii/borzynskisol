using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

namespace KeyCentralLib
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:LotId runat=server></{0}:LotId>")]
    public class LotId : WebControl
    {
        protected DynamicCriteriaBox LotIdBox;

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                String s = (String)ViewState["Text"];
                return ((s == null) ? String.Empty : s);
            }

            set
            {
                ViewState["Text"] = value;
            }
        }

        public ArrayList Keys
        {
            get { return LotIdBox.Keys; }
            set { Page.Session.Add("Lot:Criteria", value); }
        }

        protected override void OnInit(EventArgs e)
        {
            CreateControls();
            base.OnInit(e);
        }
        protected override void RenderContents(HtmlTextWriter output)
        {
            output.Write(@"<table>
	<tr>
		<td>Lot 
			ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Description
		</td>
	</tr>
	<tr>
		<td>
			");
            LotIdBox.RenderControl(output);
            //output.Write("render Dynamic Criteria Box");

            output.Write(@"
		</td>
	</tr>
</table>");
        }

        private void CreateControls()
        {
            LotIdBox = new DynamicCriteriaBox();
            LotIdBox.ID = this.UniqueID + this.ClientIDSeparator + "LotIdBox";

            LotIdBox.OracleCommand = "Select GaLotIdx,NVL (Descr,' ') From #COMPANY_NUMBER#.Ga_Lot where lower(Ga_Lot.Id) = Lower(:value)";
            LotIdBox.NotFoundMessage = "The Lot ID '#input#' could not be found.";
            LotIdBox.AlreadyEntered = "The Lot ID '#input#' is already in the list.";
            LotIdBox.OracleCommandLookup = "Select GaLotIdx,NVL (Descr,' '),NVL (Ga_Lot.id,' ') From #COMPANY_NUMBER#.Ga_Lot where Ga_Lot.GaLotIdx = :value";
            LotIdBox.LookupSearch = "Lot";
            this.Controls.Add(LotIdBox);
        }
    }
}
