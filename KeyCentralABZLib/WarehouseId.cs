using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using KeyCentral.Lookups;
using KeyCentral.Functions;

namespace KeyCentralLib
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:WarehouseId runat=server></{0}:WarehouseId>")]
    public class WarehouseId : WebControl
    {
        protected DynamicCriteriaBox WarehouseIdBox;
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                String s = (String)ViewState["Text"];
                return ((s == null) ? String.Empty : s);
            }

            set
            {
                ViewState["Text"] = value;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            CreateControls();
            base.OnInit(e);
        }
        protected override void OnPreRender(EventArgs e)
        {
            

            base.OnPreRender(e);
        }
        protected override void OnLoad(EventArgs e)
        {
            Security.BuildLimitedToKeys(Page.Session, "Warehouse", "WarehouseLimit");

            //Clear out old data
            Page.Session.Remove("WarehouseValid");
            if (ViewState[WarehouseIdBox.ValidKeys] != null)
                selectableWarehouses = (ArrayList)ViewState[WarehouseIdBox.ValidKeys];
            else
                selectableWarehouses = new ArrayList();

            LoadSelectableWarehouses();

            base.OnLoad(e);
        }
        protected override void RenderContents(HtmlTextWriter output)
        {
            output.Write(@"<table>
	<tr>
		<td>Warehouse ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; City
		</td>
	</tr>
	<tr>
		<td>");
            WarehouseIdBox.RenderControl(output);
            //output.Write("render Dynamic Criteria Box");

            output.Write(@"</td>
	</tr>
</table>");
        }
        private ArrayList selectableWarehouses;
        /// <summary>
        /// Must be an ArrayList of (strings or ints) of WarehouseIdx 
        /// </summary>
        public ArrayList SelectableWarehouses
        {
            get
            {
                return selectableWarehouses;
            }
        }
        public void SetSelectableWarehousesInt(ArrayList value)
        {
            // don't do anythign if null or empty ArrayList
            if (value == null || value.Count == 0)
                return;

            selectableWarehouses = new ArrayList();
            foreach (int tempKey in value)
                selectableWarehouses.Add(tempKey.ToString());

            ViewState.Add(WarehouseIdBox.ValidKeys, selectableWarehouses);

            LoadSelectableWarehouses();

            //Make sure we postback so the data can go through out Page_Load
            //Page.RegisterStartupScript("PostBack","<script lang='js'>" +  Page.GetPostBackClientEvent(WarehouseIdBox,"") + ";</script>");
        }
        public void SetSelectableWarehousesString(ArrayList value)
        {
            // don't do anythign if null or empty ArrayList
            if (value == null || value.Count == 0)
                return;

            selectableWarehouses = value;
            ViewState.Add(WarehouseIdBox.ValidKeys, selectableWarehouses);

            LoadSelectableWarehouses();

            //Make sure we postback so the data can go through out Page_Load
            Page.RegisterStartupScript("PostBack", "<script lang='js'>" + Page.GetPostBackClientEvent(WarehouseIdBox, "") + ";</script>");
        }
        private void CreateControls()
        { 
            WarehouseIdBox = new DynamicCriteriaBox();
            WarehouseIdBox.ID = this.UniqueID + this.ClientIDSeparator + "WarehouseIdBox";

            WarehouseIdBox.MaxTextLength = 20;
            WarehouseIdBox.OracleCommand = "Select Ic_Warehouse.WarehouseIdx,Fc_Name_Location.City,Fc_Name_Location.Descr From #COMPANY_NUMBER#.Ic_Warehouse,#COMPANY_NUMBER#.Fc_Name_Location where Fc_Name_Location.Descr = :value and Ic_Warehouse.NameIdx = Fc_Name_Location.NameIdx and Ic_Warehouse.NameLocationSeq = Fc_Name_Location.NameLocationSeq";
            WarehouseIdBox.OracleCommandLookup = "Select Ic_Warehouse.WarehouseIdx,Fc_Name_Location.City,Fc_Name_Location.Descr From #COMPANY_NUMBER#.Ic_Warehouse,#COMPANY_NUMBER#.Fc_Name_Location  where Ic_Warehouse.NameIdx = Fc_Name_Location.NameIdx and Ic_Warehouse.NameLocationSeq = Fc_Name_Location.NameLocationSeq and Ic_Warehouse.WarehouseIdx = :value ";
            WarehouseIdBox.NotFoundMessage = "The Warehouse ID '#input#' could not be found.";
            WarehouseIdBox.AlreadyEntered = "The Warehouse ID '#input#' is already in the list.";

            /*Security.BuildLimitedToKeys(Page.Session, "Warehouse", "WarehouseLimit");*/
            WarehouseIdBox.LookupSearch = "Warehouse";
            WarehouseIdBox.LimitedToKeys = "WarehouseLimit";
            WarehouseIdBox.ValidKeys = "WarehouseValid";

            //Clear out old data
            /*Page.Session.Remove("WarehouseValid");
            if (ViewState[WarehouseIdBox.ValidKeys] != null)
                selectableWarehouses = (ArrayList)ViewState[WarehouseIdBox.ValidKeys];
            else
                selectableWarehouses = new ArrayList();

            LoadSelectableWarehouses();*/
            this.Controls.Add(WarehouseIdBox);
        }
        public ArrayList Keys
        {
            get { return WarehouseIdBox.Keys; }
            set
            {
                Page.Session.Add("Warehouse:Criteria", value);

            }
        }
        public void ReLoad()
        {

        }
        private void LoadSelectableWarehouses()
        {
            //We don't do anything if there isn't any selectableWarehouses
            if (selectableWarehouses == null || selectableWarehouses.Count == 0)
                return;

            //Exceptions
            if (WarehouseIdBox.LimitedToKeys == "")
                throw new Exception("LimitedToKeys must be defined");
            if (WarehouseIdBox.ValidKeys == "")
                throw new Exception("ValidKeys must be defined");

            // -1~Error ~ No Valid Warehouses
            ArrayList goodWarehouses = new ArrayList();
            ArrayList limitedTo = (ArrayList)Page.Session[WarehouseIdBox.LimitedToKeys];

            //If we have security limted to and selectable warehouses, get a inner join of them.
            if ((limitedTo != null && limitedTo.Count != 0) && selectableWarehouses.Count != 0)
            {
                //If this warehosue is in the list add it.
                foreach (CriteriaBoxRecord tempRecord in limitedTo)
                    if (selectableWarehouses.Contains(tempRecord.key))
                        goodWarehouses.Add(tempRecord);

                //Make sure if no good ones found we add an error warehouse.
                if (goodWarehouses.Count == 0)
                    goodWarehouses.Add(new CriteriaBoxRecord("-1", "ERROR", "No Valid Warehouses"));

                Page.Session[WarehouseIdBox.LimitedToKeys] = goodWarehouses;

                //Clear the keys out so they get reset.
                Keys = new ArrayList();
            }//if they dont' have secuirty limitation, load the selectable warehouses
            else if (selectableWarehouses.Count > 0 && Keys.Count > 0)
            {
                //If this warehosue is in the list add it.
                foreach (CriteriaBoxRecord tempRecord in Keys)
                    if (selectableWarehouses.Contains(tempRecord.key))
                        goodWarehouses.Add(tempRecord);

                Keys = goodWarehouses;
            }
            else if (selectableWarehouses.Count != 0)
            {
                Page.Session[WarehouseIdBox.ValidKeys] = CriteriaBoxRecord.GetArrayOfCriteriaRecords(selectableWarehouses);
                //Session[WarehouseIdBox.LimitedToKeys] = selectableWarehouses;
                //Session[WarehouseIdBox.LookupSearch + ":Lookup"] = selectableWarehouses;

            }
            //else // we don't care to do anything if there is no selectable warehouses.

            //Make sure to reload after setting the lookup. CAUSES LOOP!!
            //ReLoad();
        }
    }
}