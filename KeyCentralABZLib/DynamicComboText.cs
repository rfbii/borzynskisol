using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Web.UI.HtmlControls;
using KeyCentral.Lookups;

namespace KeyCentralLib
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:DynamicComboText runat=server></{0}:DynamicComboText>")]
    public class DynamicComboText : WebControl
    {

        #region Declares

        private ArrayList values;
        private int maxTextLength = 20;
        private int textWidth = 50;
        private int comboWidth = 50;
        private string defaultComboText = "";
        private ListItemCollection items;
        protected HtmlGenericControl DynamicComboTextDiv;

        #endregion

        #region Properties
        public int Width
        {
            set { DynamicComboTextDiv.Style["width"] = value.ToString(); }
            get { return Int32.Parse(DynamicComboTextDiv.Attributes["width"]); }
        }
        public int Height
        {
            set { DynamicComboTextDiv.Style["height"] = value.ToString(); }
            get { return Int32.Parse(DynamicComboTextDiv.Style["height"]); }
        }
        public int MaxTextLength { set { maxTextLength = value; } get { return maxTextLength; } }
        public int TextWidth { set { textWidth = value; } get { return textWidth; } }
        public int ComboWidth { set { comboWidth = value; } get { return comboWidth; } }
        public ListItemCollection Items { set { items = value; } get { return items; } }
        public string DefaultComboText { set { defaultComboText = value; } get { return defaultComboText; } }
        public ArrayList Keys { get { return values; } set { values = value; } }

        #endregion
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                String s = (String)ViewState["Text"];
                return ((s == null) ? String.Empty : s);
            }

            set
            {
                ViewState["Text"] = value;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            CreateControls();
            base.OnInit(e);
        }
        public static Control FindControlByID(ControlCollection controls, string ID, int depth)
        {
            int loopLimit = controls.Count;
            for (int x = 0; x < loopLimit; x++)
            {
                //Check this ID
                if (((Control)controls[x]).ID == ID)
                    return controls[x];

                //Go recursive to see if we can find it
                if (depth > 0)
                {
                    Control tempControl = FindControlByID(((Control)controls[x]).Controls, ID, depth - 1);

                    if (tempControl != null)
                        return tempControl;
                }
            }

            return null;
        }
        protected override void OnPreRender(EventArgs e)
        {
            RenderControls(DynamicComboTextDiv, values);

            base.OnPreRender(e);
        }
        protected override void RenderContents(HtmlTextWriter output)
        {
            output.Write(@"<table>
	<tr>
		<td>");
            DynamicComboTextDiv.RenderControl(output);

            output.Write(@"</td>
	</tr>
</table>");
        }

        private void CreateControls()
        {
            DynamicComboTextDiv = new HtmlGenericControl();
            DynamicComboTextDiv.ID = this.UniqueID + this.ClientIDSeparator + "DynamicComboTextDiv";
            DynamicComboTextDiv.Style.Add("BORDER-RIGHT", "1px solid");
            DynamicComboTextDiv.Style.Add("TABLE-LAYOUT", "fixed");
            DynamicComboTextDiv.Style.Add("BORDER-TOP", "1px solid");
            DynamicComboTextDiv.Style.Add("OVERFLOW", "auto");
            DynamicComboTextDiv.Style.Add("BORDER-LEFT", "1px solid");
            DynamicComboTextDiv.Style.Add("WIDTH", "450px");
            DynamicComboTextDiv.Style.Add("BORDER-BOTTOM", "1px solid");
            DynamicComboTextDiv.Style.Add("BORDER-COLLAPSE", "separate");
            DynamicComboTextDiv.Style.Add("HEIGHT", "100px");
            this.Controls.Add(DynamicComboTextDiv);

            values = new ArrayList();
            ReadEnteredData(DynamicComboTextDiv, values);

            //RenderControls(DynamicComboTextDiv, values);

            //Sets the focus 
            if (Page.IsPostBack == true)
                if (Page.Request["__EVENTTARGET"].IndexOf("_Input:") > -1 && Page.Request["__EVENTTARGET"].IndexOf(this.UniqueID) > -1)
                    SetFocus(GetNextTextBoxID(Page.Request["__EVENTTARGET"]));
        }

        #region Private Render Functions

        private void RenderControls(HtmlGenericControl parentDiv, ArrayList values)
        {
            parentDiv.Controls.Add(new System.Web.UI.LiteralControl("\n"));
            int controlIndex = 0;
            int blankLines = 0;
            foreach (CriteriaBoxRecord criteriaBoxRecord in values)
            {
                controlIndex = RenderLine(parentDiv, criteriaBoxRecord, controlIndex);
                if (criteriaBoxRecord.text.Trim() == "")
                    blankLines++;
            }

            //Add the blank lines
            for (int x = 2; x > blankLines; x--)
                controlIndex = RenderLine(parentDiv, new CriteriaBoxRecord("", "", defaultComboText), controlIndex);
        }

        private int RenderLine(HtmlGenericControl control, CriteriaBoxRecord criteriaBoxRecord, int controlIndex)
        {
            RenderDropDownList(control, criteriaBoxRecord, controlIndex);
            //control.Controls.Add(new System.Web.UI.LiteralControl("&nbsp;"));
            RenderTextBox(control, criteriaBoxRecord, controlIndex);
            control.Controls.Add(new System.Web.UI.LiteralControl("<BR>\n"));

            return controlIndex + 1;
        }

        private void RenderDropDownList(HtmlGenericControl control, CriteriaBoxRecord criteriaBoxRecord, int controlIndex)
        {
            //Add the DropDownList
            System.Web.UI.WebControls.DropDownList dropDown = new DropDownList();

            if (items != null)
                foreach (ListItem item in items)
                    dropDown.Items.Add(new ListItem(item.Text, item.Value));


            dropDown.ID = this.UniqueID + this.ClientIDSeparator + "dropDown" + "_Key:" + controlIndex.ToString();
            dropDown.Width = comboWidth;
            //dropDown.AutoPostBack=true;

            if (criteriaBoxRecord.key != "")
                dropDown.SelectedValue = criteriaBoxRecord.key;
            else if (criteriaBoxRecord.description != "")
                for (int x = 0; x < dropDown.Items.Count; x++)
                    if (dropDown.Items[x].Text == criteriaBoxRecord.description)
                        dropDown.Items[x].Selected = true;

            control.Controls.Add(dropDown);
            control.Controls.Add(new System.Web.UI.LiteralControl("\n"));
        }
        private void RenderTextBox(HtmlGenericControl control, CriteriaBoxRecord criteriaBoxRecord, int controlIndex)
        {
            TextBox box = new TextBox();

            box.Text = criteriaBoxRecord.text;
            box.ID = this.UniqueID + this.ClientIDSeparator + "textBox" + "_Input:" + controlIndex.ToString();
            box.AutoPostBack = true;
            box.MaxLength = maxTextLength;
            box.Width = this.textWidth;
            box.Attributes.Add("EnterAsTab", "");

            //if(lookupSearch !="")
            //	box.Attributes.Add("LookupSearch",lookupSearch);

            //Add the TextBox we just created
            control.Controls.Add(box);
            control.Controls.Add(new System.Web.UI.LiteralControl("\n"));
        }
        #endregion

        #region Private Helpers
        private void ReadEnteredData(HtmlGenericControl parentDiv, ArrayList values)
        {
            string key;
            string text;
            for (int x = 0; Page.Request[UniqueID + this.ClientIDSeparator + "textBox" + "_Input:" + x.ToString()] != null; x++)
                if (Page.Request[UniqueID + this.ClientIDSeparator + "textBox" + "_Input:" + x.ToString()].ToString() != "")
                {
                    key = Page.Request[UniqueID + this.ClientIDSeparator + "dropDown" + "_Key:" + x.ToString()];
                    text = Page.Request[UniqueID + this.ClientIDSeparator + "textBox" + "_Input:" + x.ToString()];
                    values.Add(new CriteriaBoxRecord(key, text, ""));
                }
        }
        private void SetFocus(string ctrl)
        {
            string s = "<SCRIPT language=\"javascript\">" +
                "document.getElementById('" + ctrl + "').focus(); </SCRIPT>";

            Page.RegisterStartupScript("focus", s);
        }
        private string GetNextTextBoxID(string changingTextBox)
        {
            //return this.UniqueID + "_Input:" + (Int32.Parse(changingTextBox.Remove(0, changingTextBox.IndexOf(":Input:") + 7)) + 1).ToString();
            return this.UniqueID + this.ClientIDSeparator + "textBox" + "_Input:" + (Int32.Parse(changingTextBox.Remove(0, changingTextBox.IndexOf("_Input:") + 7)) + 1).ToString();
        }
        #endregion
    }
}
