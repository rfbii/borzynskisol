using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

namespace KeyCentralLib
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:CostCenterId runat=server></{0}:CostCenterId>")]
    public class CostCenterId : WebControl
    {
        protected DynamicCriteriaBox CostCenterIdBox;

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                String s = (String)ViewState["Text"];
                return ((s == null) ? String.Empty : s);
            }

            set
            {
                ViewState["Text"] = value;
            }
        }

        public ArrayList Keys
        {
            get { return CostCenterIdBox.Keys; }
            set { Page.Session.Add("CostCenter:Criteria", value); }
        }

        protected override void OnInit(EventArgs e)
        {
            CreateControls();
            base.OnInit(e);
        }
        protected override void RenderContents(HtmlTextWriter output)
        {
            output.Write(@"<table>
	<tr>
		<td>Cost Center 
			ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Description
		</td>
	</tr>
	<tr>
		<td>
			");
            CostCenterIdBox.RenderControl(output);
            //output.Write("render Dynamic Criteria Box");

            output.Write(@"
		</td>
	</tr>
</table>");
        }

        private void CreateControls()
        {
            CostCenterIdBox = new DynamicCriteriaBox();
            CostCenterIdBox.ID = this.UniqueID + this.ClientIDSeparator + "CostCenterIdBox";

            CostCenterIdBox.OracleCommand = "Select CostCenterIdx,NVL (Name,' ') From #COMPANY_NUMBER#.Ca_Cost_Center where lower(Ca_Cost_Center.Id) = Lower(:value)";
            CostCenterIdBox.NotFoundMessage = "The Cost Center ID '#input#' could not be found.";
            CostCenterIdBox.AlreadyEntered = "The Cost Center ID '#input#' is already in the list.";
            CostCenterIdBox.OracleCommandLookup = "Select CostCenterIdx,NVL (Name,' '),NVL (Ca_Cost_Center.id,' ') From #COMPANY_NUMBER#.Ca_Cost_Center where Ca_Cost_Center.CostCenterIdx = :value";
            CostCenterIdBox.LookupSearch = "CostCenter";
            this.Controls.Add(CostCenterIdBox);
        }
    }
}
