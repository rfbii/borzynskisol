using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;

namespace KeyCentral.Functions
{
    public class Disclaimer : BasePage
    {
        #region Declares
        //private SortedList disclaimerList;
        #endregion

        #region Properties
        public SortedList AllDisclaimerList
        {
            get
            {
                //If we are multi-Thread we should to lock the Disclaimer object
                //Lazy loading (If not in Application then put it there.)
                if(HttpContext.Current.Application["Disclaimer"] == null)
                    HttpContext.Current.Application["Disclaimer"] = GetDisclaimerList();

                return (SortedList)HttpContext.Current.Application["Disclaimer"];
            }
        }
        public SortedList AllDisclaimerListLandscape
        {
            get
            {
                //If we are multi-Thread we should to lock the Disclaimer object
                //Lazy loading (If not in Application then put it there.)
                if (HttpContext.Current.Application["DisclaimerLandscape"] == null)
                    HttpContext.Current.Application["DisclaimerLandscape"] = GetDisclaimerListLandscape();

                return (SortedList)HttpContext.Current.Application["DisclaimerLandscape"];
            }
        }
        /// <summary>
        /// Returns an ArrayList of Disclaimers; DisclaimerList[0] is the page disclaimer. 
        /// An empty arraylist if nodisclaimer has been setup for this company.
        /// </summary>
        public ArrayList DisclaimerList
        {
            get
            {
                SortedList disclaimerList = AllDisclaimerList;

                if (disclaimerList.ContainsKey((string)HttpContext.Current.Session["Company_Key"]) == false)
                    return new ArrayList();
                else
                    return (ArrayList)disclaimerList[(string)HttpContext.Current.Session["Company_Key"]];
            }
        }
        public ArrayList DisclaimerListLandscape
        {
            get
            {
                SortedList disclaimerList = AllDisclaimerListLandscape;

                if (disclaimerList.ContainsKey((string)HttpContext.Current.Session["Company_Key"]) == false)
                    return new ArrayList();
                else
                    return (ArrayList)disclaimerList[(string)HttpContext.Current.Session["Company_Key"]];
            }
        }
        public int LineCount
        {
            get
            {
                ArrayList list = DisclaimerList;
                int count = 0;

                //only counting the report disclaimers, not the page disclaimer(Page disclaimer is at index 0)
                for(int i = 1; i < list.Count; i++)
                {
                    if(list[i].ToString() != "")
                        count++;
                }

                return count;
            }
        }
        public int LineCountLandscape
        {
            get
            {
                ArrayList list = DisclaimerListLandscape;
                int count = 0;

                //only counting the report disclaimers, not the page disclaimer(Page disclaimer is at index 0)
                for (int i = 1; i < list.Count; i++)
                {
                    if (list[i].ToString() != "")
                        count++;
                }

                return count;
            }
        }
        #endregion

        #region Constructor
        public Disclaimer()
        {
        }
        #endregion

        #region Private Helpers
        private SortedList GetDisclaimerList()
        {
            string companyKey = (string)HttpContext.Current.Session["Company_Key"];

            SortedList disclaimerList = new SortedList();

            SqlConnection Conn = Misc.GetSQLConn();
            SqlCommand selectCMD = new SqlCommand(@"
use KeyCentral;
Select
    COALESCE([Disclaimer].Company_Key,'') as Company_Key,
    COALESCE([Disclaimer].Report_Disclaimer1,'') as Report_Disclaimer1,
    COALESCE([Disclaimer].Report_Disclaimer2,'') as Report_Disclaimer2,
    COALESCE([Disclaimer].Report_Disclaimer3,'') as Report_Disclaimer3,
    COALESCE([Disclaimer].Report_Disclaimer4,'') as Report_Disclaimer4,
    COALESCE([Disclaimer].Report_Disclaimer5,'') as Report_Disclaimer5,
    COALESCE([Disclaimer].Page_Disclaimer,'') as Page_Disclaimer
From
    Disclaimer", Conn);

            SqlDataReader myReader = selectCMD.ExecuteReader();

            while (myReader.Read())
            {
                ArrayList disclaimersArrayList = new ArrayList();
                disclaimersArrayList.Add(myReader.GetValue(6).ToString().Trim());
                disclaimersArrayList.Add(myReader.GetValue(1).ToString().Trim());
                disclaimersArrayList.Add(myReader.GetValue(2).ToString().Trim());
                disclaimersArrayList.Add(myReader.GetValue(3).ToString().Trim());
                disclaimersArrayList.Add(myReader.GetValue(4).ToString().Trim());
                disclaimersArrayList.Add(myReader.GetValue(5).ToString().Trim());

                disclaimerList.Add(myReader.GetValue(0).ToString(), disclaimersArrayList);
            }

            myReader.Close();
            myReader.Dispose();
            Conn.Close();
            Conn.Dispose();

            return disclaimerList;
        }
        private SortedList GetDisclaimerListLandscape()
        {
            string companyKey = (string)HttpContext.Current.Session["Company_Key"];

            SortedList disclaimerList = new SortedList();

            SqlConnection Conn = Misc.GetSQLConn();
            SqlCommand selectCMD = new SqlCommand(@"
use KeyCentral;
Select
    COALESCE([Disclaimer].Company_Key,'') as Company_Key,
    COALESCE([Disclaimer].Report_Disclaimer6,'') as Report_Disclaimer1,
    COALESCE([Disclaimer].Report_Disclaimer7,'') as Report_Disclaimer2,
    COALESCE([Disclaimer].Report_Disclaimer8,'') as Report_Disclaimer3,
    COALESCE([Disclaimer].Report_Disclaimer9,'') as Report_Disclaimer4,
    COALESCE([Disclaimer].Report_Disclaimer10,'') as Report_Disclaimer5,
    COALESCE([Disclaimer].Page_Disclaimer,'') as Page_Disclaimer
From
    Disclaimer", Conn);

            SqlDataReader myReader = selectCMD.ExecuteReader();

            while (myReader.Read())
            {
                ArrayList disclaimersArrayList = new ArrayList();
                disclaimersArrayList.Add(myReader.GetValue(6).ToString().Trim());
                disclaimersArrayList.Add(myReader.GetValue(1).ToString().Trim());
                disclaimersArrayList.Add(myReader.GetValue(2).ToString().Trim());
                disclaimersArrayList.Add(myReader.GetValue(3).ToString().Trim());
                disclaimersArrayList.Add(myReader.GetValue(4).ToString().Trim());
                disclaimersArrayList.Add(myReader.GetValue(5).ToString().Trim());

                disclaimerList.Add(myReader.GetValue(0).ToString(), disclaimersArrayList);
            }

            myReader.Close();
            myReader.Dispose();
            Conn.Close();
            Conn.Dispose();

            return disclaimerList;
        }
        public void UpdateDisclaimer(string reportDisclaimerTextBox1, string reportDisclaimerTextBox2, string reportDisclaimerTextBox3, string reportDisclaimerTextBox4, string reportDisclaimerTextBox5, string reportDisclaimerTextBox6, string reportDisclaimerTextBox7, string reportDisclaimerTextBox8, string reportDisclaimerTextBox9, string reportDisclaimerTextBox10, string pageDisclaimerTextBox)
        {
            if (!CheckSecurity("ADMINONLY"))
                throw new Exception("You do not have permission to update the disclaimer.");

            SqlConnection Conn = this.GetSQLConn();

            //insert/update Company Disclaimer
            SqlCommand selectCMD = new SqlCommand("use KeyCentral; select * from [Disclaimer] where company_key=@company_key", Conn);
            selectCMD.Parameters.AddWithValue("@company_key", Session["Company_Key"].ToString());

            SqlDataReader myReader = selectCMD.ExecuteReader();

            bool shouldUpdate = myReader.HasRows;
            myReader.Close();
            myReader.Dispose();


            if (shouldUpdate)
            {
                SqlCommand updateCMD = new SqlCommand(@"use KeyCentral; update [Disclaimer] Set Report_Disclaimer1=@Report_Disclaimer1, Report_Disclaimer2=@Report_Disclaimer2, Report_Disclaimer3=@Report_Disclaimer3, Report_Disclaimer4=@Report_Disclaimer4, Report_Disclaimer5=@Report_Disclaimer5, Report_Disclaimer6=@Report_Disclaimer6, Report_Disclaimer7=@Report_Disclaimer7, Report_Disclaimer8=@Report_Disclaimer8, Report_Disclaimer9=@Report_Disclaimer9, Report_Disclaimer10=@Report_Disclaimer10, Page_Disclaimer=@Page_Disclaimer where Company_Key=@Company_Key", Conn);
                updateCMD.Parameters.AddWithValue("@Report_Disclaimer1", reportDisclaimerTextBox1);
                updateCMD.Parameters.AddWithValue("@Report_Disclaimer2", reportDisclaimerTextBox2);
                updateCMD.Parameters.AddWithValue("@Report_Disclaimer3", reportDisclaimerTextBox3);
                updateCMD.Parameters.AddWithValue("@Report_Disclaimer4", reportDisclaimerTextBox4);
                updateCMD.Parameters.AddWithValue("@Report_Disclaimer5", reportDisclaimerTextBox5);
                updateCMD.Parameters.AddWithValue("@Report_Disclaimer6", reportDisclaimerTextBox6);
                updateCMD.Parameters.AddWithValue("@Report_Disclaimer7", reportDisclaimerTextBox7);
                updateCMD.Parameters.AddWithValue("@Report_Disclaimer8", reportDisclaimerTextBox8);
                updateCMD.Parameters.AddWithValue("@Report_Disclaimer9", reportDisclaimerTextBox9);
                updateCMD.Parameters.AddWithValue("@Report_Disclaimer10", reportDisclaimerTextBox10);
                updateCMD.Parameters.AddWithValue("@Page_Disclaimer", pageDisclaimerTextBox);
                updateCMD.Parameters.AddWithValue("@Company_Key", Session["Company_Key"].ToString());

                updateCMD.ExecuteNonQuery();
                updateCMD.Dispose();
            }
            else
            {
                SqlCommand insertCMD = new SqlCommand("use KeyCentral;insert into [Disclaimer] (Company_Key,Report_Disclaimer1,Report_Disclaimer2,Report_Disclaimer3,Report_Disclaimer4,Report_Disclaimer5,Report_Disclaimer6,Report_Disclaimer7,Report_Disclaimer8,Report_Disclaimer9,Report_Disclaimer10,Page_Disclaimer) values(@Company_Key,@Report_Disclaimer1,@Report_Disclaimer2,@Report_Disclaimer3,@Report_Disclaimer4,@Report_Disclaimer5,@Report_Disclaimer6,@Report_Disclaimer7,@Report_Disclaimer8,@Report_Disclaimer9,@Report_Disclaimer10,@Page_Disclaimer)", Conn);
                insertCMD.Parameters.AddWithValue("@Company_Key", Session["Company_Key"].ToString());
                insertCMD.Parameters.AddWithValue("@Report_Disclaimer1", reportDisclaimerTextBox1);
                insertCMD.Parameters.AddWithValue("@Report_Disclaimer2", reportDisclaimerTextBox2);
                insertCMD.Parameters.AddWithValue("@Report_Disclaimer3", reportDisclaimerTextBox3);
                insertCMD.Parameters.AddWithValue("@Report_Disclaimer4", reportDisclaimerTextBox4);
                insertCMD.Parameters.AddWithValue("@Report_Disclaimer5", reportDisclaimerTextBox5);
                insertCMD.Parameters.AddWithValue("@Report_Disclaimer6", reportDisclaimerTextBox6);
                insertCMD.Parameters.AddWithValue("@Report_Disclaimer7", reportDisclaimerTextBox7);
                insertCMD.Parameters.AddWithValue("@Report_Disclaimer8", reportDisclaimerTextBox8);
                insertCMD.Parameters.AddWithValue("@Report_Disclaimer9", reportDisclaimerTextBox9);
                insertCMD.Parameters.AddWithValue("@Report_Disclaimer10", reportDisclaimerTextBox10);
                insertCMD.Parameters.AddWithValue("@Page_Disclaimer", pageDisclaimerTextBox);

                insertCMD.ExecuteNonQuery();
                insertCMD.Dispose();
            }

            this.CleanUpSQL(Conn);

            //Reload the disclaimer into the Application
            HttpContext.Current.Application["Disclaimer"] = GetDisclaimerList();
        }
        #endregion
    }
}
