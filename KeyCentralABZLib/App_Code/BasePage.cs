//#define LogDBConnection 

namespace KeyCentral.Functions
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Data.SqlClient;
    using Oracle.DataAccess.Client;
    using System.Configuration;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.IO;
    using System.Data;
    using System.Collections;

    public class BasePage : System.Web.UI.Page
    {
        protected Menu MainMenu;
        //protected skmMenu.Menu Menu1;
        protected System.Web.UI.HtmlControls.HtmlGenericControl LogDiv;
        protected MenuMap menuMap;
        protected SessionManager sessionManager;
        private bool _useScrollPersistence = true;
        private string _bodyID;
        protected Disclaimer disclaimer;
        protected bool IsAjaxPostBack = false;

        protected void JavaScriptMessage(string message)
        {
            message = message.Replace("'", "\\'");
            this.RegisterStartupScript("DisplayMessage", "<script>alert('" + message + "');</script>");
        }

        #region Properties
        public bool UseScrollPersistence
        {
            get { return this._useScrollPersistence; }
            set { this._useScrollPersistence = value; }
        }
        public string BodyID
        {
            get { return this._bodyID; }
            set { this._bodyID = value; }
        }
        #endregion

        #region override functions
        protected override void OnPreRender(EventArgs e)
        {
            if (UseScrollPersistence)
                RetainScrollPosition();

            base.OnPreRender(e);
        }
        protected override void Render(HtmlTextWriter writer)
        {
            if (UseScrollPersistence && BodyID == null)
            {
                TextWriter tempWriter = new StringWriter();
                base.Render(new HtmlTextWriter(tempWriter));
                writer.Write(Regex.Replace(tempWriter.ToString(), "<body", "<body id=\"thebody\" ", RegexOptions.IgnoreCase));
            }
            else
            {
                base.Render(writer);
            }
        }
        protected override void OnInit(EventArgs e)
        {
            if (Session["Company"] != null)
                SetTitle(Session["Company"].ToString());
            //else //Handle this in page load
            //	Response.Redirect("../../../KeyCentralABZ/UI/Security/Login.aspx");

            menuMap = new MenuMap(Page);
            base.OnInit(e);
        }
        public static Control FindControlByID(ControlCollection controls, string ID, int depth)
        {
            int loopLimit = controls.Count;
            for (int x = 0; x < loopLimit; x++)
            {
                //Check this ID
                if (((Control)controls[x]).ID == ID)
                    return controls[x];

                //Go recursive to see if we can find it
                if (depth > 0)
                {
                    Control tempControl = FindControlByID(((Control)controls[x]).Controls, ID, depth - 1);

                    if (tempControl != null)
                        return tempControl;
                }
            }

            return null;
        }
        protected override void OnLoad(EventArgs e)
        {
            DateTime pageRequest = DateTime.Now;


            //Only load the menu on new pages
            MainMenu = (Menu)FindControlByID(Controls, "MainMenu", 2);
            this.LogDiv = (System.Web.UI.HtmlControls.HtmlGenericControl)FindControlByID(Controls, "LogDiv", 2);

            disclaimer = new Disclaimer();

            //Only show on Non-PostBack and when not on MobilePhone
            if (this.IsPostBack != true)// && ((string)Session["MobileClientPlatform"]).ToUpper() == "NO")
            {
                if (MainMenu != null)
                {
                    //menuMap.BuildMenu(MainMenu, GetPageType(Request.RawUrl).ToString());
                    RenderMenu(GetPageType(Request.RawUrl).ToString());
                }
                else
                    //Response.Write("menu was null!");
                    Response.Write("");
            }


            base.OnLoad(e);

            if (Session["Company"] != null)
                SetTitle(Session["Company"].ToString());
            else
                if (!IsAjaxPostBack)
                    Response.Redirect("../../../KeyCentralABZ/UI/Security/Login.aspx");


            //Put in the Session Ping code. (Exclude AjaxCalls)
            if (!IsAjaxPostBack)
                Response.Write(SessionManager.GetPingScript());

            sessionManager = new SessionManager();

            if (LogDiv != null)
                Log("This page took " + ((TimeSpan)(DateTime.Now - pageRequest)).TotalSeconds.ToString("##.###") + " seconds to run.");

        }

        #endregion

		#region Scroll Position Functions
		#region JavaScript code
		private static string saveScrollPosition = @"
<script language='javascript'>
function saveScrollPosition() 
{{
	document.forms[0].__ScrollPosX.value = {0}.scrollTop;
	document.forms[0].__ScrollPosY.value = {0}.scrollLeft;
	if(document.getElementById('BodyDiv') != null)
	{{
		document.forms[0].__ScrollPosBodyDivX.value = BodyDiv.scrollTop;
		document.forms[0].__ScrollPosBodyDivY.value = BodyDiv.scrollLeft;
	}}

	if(document.getElementById('HeaderDiv') != null)
		if(document.getElementById('HeaderDiv').scrollwithbody != undefined)
			HeaderDiv.scrollLeft = BodyDiv.scrollLeft;
	if(document.getElementById('FooterDiv') != null)
		if(document.getElementById('FooterDiv').scrollwithbody != undefined)
			FooterDiv.scrollLeft = BodyDiv.scrollLeft;

	if(document.getElementById('LeftDiv') != null)
		if(document.getElementById('LeftDiv').scrollwithbody != undefined)
			LeftDiv.scrollTop = BodyDiv.scrollTop;
}}
function ScrollBodyDivWithLeftDiv() 
{{
	
	if(document.getElementById('LeftDiv') != null)
		if(document.getElementById('LeftDiv').scrollwithbody != undefined)
			BodyDiv.scrollTop = LeftDiv.scrollTop;
}}
{0}.onscroll=saveScrollPosition;
if(document.getElementById('BodyDiv') != null)
{{
	BodyDiv.onscroll=saveScrollPosition;
}}
if(document.getElementById('LeftDiv') != null)
{{
	LeftDiv.onscroll=ScrollBodyDivWithLeftDiv;
}}
</script>";
		private static string setScrollPosition = @"
<script language='javascript'>
function setScrollPosition() 
{{
	{0}.scrollTop =" + "\"" +"{1}" +"\"" + @";
	{0}.scrollLeft =" + "\"" +"{2}" +"\"" + @";
	if(document.getElementById('BodyDiv') != null)
	{{
		BodyDiv.scrollTop =" + "\"" +"{3}" +"\"" + @";
		BodyDiv.scrollLeft =" + "\"" +"{4}" +"\"" + @";
	}}
}}
{0}.onload=setScrollPosition;
</script>";
		#endregion
		private void RetainScrollPosition()          
		{
			//If the menu isn't on this page there isn't the header.
			if(MainMenu == null)
				return;
			RegisterHiddenField("__ScrollPosX", "0");
			RegisterHiddenField("__ScrollPosY", "0");
			RegisterHiddenField("__ScrollPosBodyDivX", "0");
			RegisterHiddenField("__ScrollPosBodyDivY", "0");
			string __bodyID = BodyID == null ? "thebody" : BodyID;
			RegisterStartupScript("saveScroll", string.Format(saveScrollPosition,__bodyID));
		
			if(Page.IsPostBack)
			{
				RegisterStartupScript("setScroll", string.Format(setScrollPosition,__bodyID, Request.Form["__ScrollPosX"], Request.Form["__ScrollPosY"],Request.Form["__ScrollPosBodyDivX"],Request.Form["__ScrollPosBodyDivY"]));
			}
		}

		#endregion

		public BasePage(){}

		public void SetFocus(string fieldName)
		{
			this.RegisterStartupScript("Focus","<script>SetFocusToControl('"+ fieldName+ "');</script>");
		}
		public bool CheckSecurity(string Name)
		{   
			return Security.CheckSecurity(Session,Name);
		}
		public bool IsAdmin()
		{
			if (Session["Administrator"].ToString() == "Yes")
				return true;
			else
				return false;
		}
		public bool CheckReportSecurity()
		{
			bool ret=false;
			if (Session["Administrator"].ToString() == "Yes")
			{
				ret=true;
			}
			else
				if (((CheckSecurity("Customer Status Report") != false) ||(CheckSecurity("Weekly Sales Report") != false) || (CheckSecurity("Weekly Sales Per Year Report") != false) ||(CheckSecurity("Weekly Sales Per Year Summary Report") != false)||(CheckSecurity("Walla Walla Aged Packout") != false) ||(CheckSecurity("Walla Walla Product Flow") != false) ||(CheckSecurity("Walla Walla Packout") != false)||(CheckSecurity("Walla Walla Repack") != false) ||(CheckSecurity("Freight Allocation") != false)))			
				{			
					ret=true;
				}
			return ret;
		
		}
		public bool CheckMayan0405ReportSecurity()
		{
			bool ret=false;
			if (Session["Administrator"].ToString() == "Yes")
			{
				ret=true;
			}
			else
				if (((CheckSecurity("Mayan Data Check Report0405") != false)||(CheckSecurity("Mayan Packout Report0405") != false)||(CheckSecurity("Mayan Repack Report0405") != false)))			
			{			
				ret=true;
			}
			return ret;
		}

		public bool CheckInforma0405ReportSecurity()
		{
			bool ret=false;
			if (Session["Administrator"].ToString() == "Yes")
			{
				ret=true;
			}
			else
				if (((CheckSecurity("Mayan Price Report0405") != false) ||(CheckSecurity("Mayan Charge Report0405") != false) || (CheckSecurity("Informa Reports0405") != false)||(CheckSecurity("Informa Grower Payment Reports0405") != false)||(CheckSecurity("Settlement Reports0405") != false)||(CheckSecurity("Mayan Repack Charges Report0405") != false)))			
			{			
				ret=true;
			}
			return ret;
		}

		public bool CheckMayan0506ReportSecurity()
		{
			bool ret=false;
			if (Session["Administrator"].ToString() == "Yes")
			{
				ret=true;
			}
			else
				if (((CheckSecurity("Mayan Data Check Report0506") != false)||(CheckSecurity("Mayan Packout Report0506") != false)||(CheckSecurity("Mayan Repack Report0506") != false)))			
			{			
				ret=true;
			}
			return ret;
		}

		public bool CheckInforma0506ReportSecurity()
		{
			bool ret=false;
			if (Session["Administrator"].ToString() == "Yes")
			{
				ret=true;
			}
			else
				if (((CheckSecurity("Mayan Price Report0506") != false) ||(CheckSecurity("Mayan Charge Report0506") != false) || (CheckSecurity("Informa Reports0506") != false)||(CheckSecurity("Informa Grower Payment Reports0506") != false)||(CheckSecurity("Settlement Reports0506") != false)||(CheckSecurity("Mayan Repack Charges Report0506") != false)))			
			{			
				ret=true;
			}
			return ret;
		}

		public bool CheckMayan0607ReportSecurity()
		{
			bool ret=false;
			if (Session["Administrator"].ToString() == "Yes")
			{
				ret=true;
			}
			else
				if (((CheckSecurity("Mayan Data Check Report0607") != false)||(CheckSecurity("Mayan Packout Report0607") != false)||(CheckSecurity("Mayan Repack Report0607") != false)))			
			{			
				ret=true;
			}
			return ret;
		}

		public bool CheckInforma0607ReportSecurity()
		{
			bool ret=false;
			if (Session["Administrator"].ToString() == "Yes")
			{
				ret=true;
			}
			else
				if (((CheckSecurity("Mayan Price Report0607") != false) ||(CheckSecurity("Mayan Charge Report0607") != false) || (CheckSecurity("Informa Reports0607") != false)||(CheckSecurity("Informa Grower Payment Reports0607") != false)||(CheckSecurity("Settlement Reports0607") != false)||(CheckSecurity("Mayan Repack Charges Report0607") != false)))			
			{			
				ret=true;
			}
			return ret;
		}

		public bool CheckMayanDataCentral0405Security()
		{
			bool ret=false;
			if (Session["Administrator"].ToString() == "Yes")
			{
				ret=true;
			}
			else
				if (((CheckSecurity("Mayan Data Central0405") != false || CheckSecurity("Mayan Data Central Read Only0405")!= false)|| CheckSecurity("Mayan Data Central Accounting0405")!= false))			
				{			
					ret=true;
				}
			return ret;
		}

		public bool CheckMayanDataCentral0506Security()
		{
			bool ret=false;
			if (Session["Administrator"].ToString() == "Yes")
			{
				ret=true;
			}
			else
				if (((CheckSecurity("Mayan Data Central0506") != false || CheckSecurity("Mayan Data Central Read Only0506")!= false)|| CheckSecurity("Mayan Data Central Accounting0506")!= false))			
			{			
				ret=true;
			}
			return ret;
		}

		public bool CheckMayanDataCentral0607Security()
		{
			bool ret=false;
			if (Session["Administrator"].ToString() == "Yes")
			{
				ret=true;
			}
			else
				if (((CheckSecurity("Mayan Data Central0607") != false || CheckSecurity("Mayan Data Central Read Only0607")!= false)|| CheckSecurity("Mayan Data Central Accounting0607")!= false))			
			{			
				ret=true;
			}
			return ret;
		}

		public string FormatOracle(string Name)
		{   
			return Misc.FormatOracle(Session,Name);
		}
		public string GetQueryString(string field)
		{
			return Misc.GetQueryString(this.Request,field);
		}

		protected void SetTitle(string title)
		{
			this.RegisterClientScriptBlock("Create title","<script lang=js>document.title='" + title + "'</script>");
		}
		public OracleConnection GetOracleConn()
		{
			return GetOracleConn("Generic");
		}

		public OracleConnection GetOracleConn(string reason)
		{
			#if (LogDBConnection)
						Log("OracleConnect:" + reason );
			#endif

			return Misc.GetOracleConn(Session);
		}

		public SqlConnection GetSQLConn()
		{
			return Misc.GetSQLConn("Generic");
		}
		public SqlConnection GetSQLConn(string reason)
		{
			#if (LogDBConnection)
				Log("SQLConnect:" + reason );
			#endif

			return Misc.GetSQLConn(reason);

		}
		public void Log(string logValue)
		{
			LogDiv.InnerHtml += logValue +"<BR>";
		}
		public void CleanUpSQL(SqlConnection Conn)
		{
			Conn.Close();
			Conn.Dispose();
		}

		public void DataBindListBox(ListBox listBox,SqlCommand command,string textField,string valueField)
		{
			SqlDataReader myReader = command.ExecuteReader();
			
			listBox.DataSource = myReader;
			listBox.DataTextField = textField;
			listBox.DataValueField = valueField;
			listBox.DataBind();	
		}
		public void JavaScriptRedirect(string url)
		{
			Response.Write("<script>location.replace('" + url + "');</script>");
		}
		public void ShowNowLoading()
		{
			Response.Write("<div id='NowLoadingDiv' style='LEFT: 250px; POSITION: absolute; TOP: 100px' >");
			Response.Write("<img src='../../../BaseRFB/BaseRFB/Include/Computing.gif'>");
			Response.Write("</div>");

			Response.Write("<script language=javascript>");

			Response.Write("function StartLoading(){NowLoadingDiv.style.visibility = 'visible';}");
			Response.Write("function DoneLoading(){NowLoadingDiv.style.visibility = 'hidden';}");
			Response.Write("StartLoading();</script>");
			Response.Flush();

			if(!this.IsClientScriptBlockRegistered("DoneLoading"))
				this.RegisterStartupScript("DoneLoading","<script>DoneLoading();</script>");
		}

		public enum PageType { Menu, Input, Criteria, Report, Report_NoExcel, Report_BackOnly, PrintExcel, MayanDoc, Accounting, Unknown, MayanDocInspection }

		protected PageType GetPageType(string page)
		{
			//Remove a quary string if present
			if(page.IndexOf("?")!=-1)
				page = page.Substring(0,page.IndexOf("?"));

			//If the path is passed in too, remove it.
			if(page.IndexOf("/")!=-1)
				page = page.Remove(0,page.LastIndexOf("/")+1);

			//Make sure this isn't a case sensitive check.
			page = page.ToLower();

			if(page.StartsWith("menu"))
				return PageType.Menu;

			if(page.StartsWith("input"))
				return PageType.Input;

			if(page.StartsWith("criteria"))
				return PageType.Criteria;
			
			if(page.StartsWith("mayandoc.aspx"))
				return PageType.MayanDoc;
			
			if(page.StartsWith("mayandocsearch.aspx"))
				return PageType.Criteria;

			if(page.StartsWith("accountinggrid.aspx"))
				return PageType.Accounting;

			if(page.StartsWith("report_noexcel"))
				return PageType.Report_NoExcel;

			if(page.StartsWith("report_backonly"))
				return PageType.Report_BackOnly;

			if (page.StartsWith("printexcel"))
				return PageType.PrintExcel;

			if(page.StartsWith("report"))
				return PageType.Report;

			if(page.StartsWith("mayandocinspection"))
				return PageType.MayanDocInspection;
			
			return PageType.Unknown;
		}
		protected void SetPopPrinting(string sort)
		{
            //TODO: uncomment later
            //Menu1.Items["Print"].SubItems["Receive"].JavascriptCommand = "PopPrintScreen('../../../MayanDoc/MayanDoc/Print/PrintReceive.aspx?MinShipDate=" + this.Request.QueryString["MinShipDate"] + "&&MaxShipDate=" + this.Request.QueryString["MaxShipDate"] + "&&MaxSchedDate=" + this.Request.QueryString["MaxSchedDate"] + "&&MinSchedDate=" + this.Request.QueryString["MinSchedDate"] + "&&Destination=" + this.Request.QueryString["Destination"] + "&&ShippingLine=" + this.Request.QueryString["ShippingLine"] + "&&Container=" + this.Request.QueryString["Container"] + "&&Lot=" + this.Request.QueryString["Lot"] + "&&MinUSDADate=" + this.Request.QueryString["MinUSDADate"] + "&&MaxUSDADate=" + this.Request.QueryString["MaxUSDADate"] + "&&Sort=" + sort + "');";
            //Menu1.Items["Print"].SubItems["Shipping"].JavascriptCommand = "PopPrintScreen('../../../MayanDoc/MayanDoc/Print/PrintShipping.aspx?MinShipDate=" + this.Request.QueryString["MinShipDate"] + "&&MaxShipDate=" + this.Request.QueryString["MaxShipDate"] + "&&MaxSchedDate=" + this.Request.QueryString["MaxSchedDate"] + "&&MinSchedDate=" + this.Request.QueryString["MinSchedDate"] + "&&Destination=" + this.Request.QueryString["Destination"] + "&&ShippingLine=" + this.Request.QueryString["ShippingLine"] + "&&Container=" + this.Request.QueryString["Container"] + "&&Lot=" + this.Request.QueryString["Lot"] + "&&MinUSDADate=" + this.Request.QueryString["MinUSDADate"] + "&&MaxUSDADate=" + this.Request.QueryString["MaxUSDADate"] + "&&Sort=" + sort + "');";
            //Menu1.Items["Print"].SubItems["Packer"].JavascriptCommand = "PopPrintScreen('../../../MayanDoc/MayanDoc/Print/PrintPacker.aspx?MinShipDate=" + this.Request.QueryString["MinShipDate"] + "&&MaxShipDate=" + this.Request.QueryString["MaxShipDate"] + "&&MaxSchedDate=" + this.Request.QueryString["MaxSchedDate"] + "&&MinSchedDate=" + this.Request.QueryString["MinSchedDate"] + "&&Destination=" + this.Request.QueryString["Destination"] + "&&ShippingLine=" + this.Request.QueryString["ShippingLine"] + "&&Container=" + this.Request.QueryString["Container"] + "&&Lot=" + this.Request.QueryString["Lot"] + "&&MinUSDADate=" + this.Request.QueryString["MinUSDADate"] + "&&MaxUSDADate=" + this.Request.QueryString["MaxUSDADate"] + "&&Sort=" + sort + "');";
            //Menu1.Items["Print"].SubItems["Ocean"].JavascriptCommand = "PopPrintScreen('../../../MayanDoc/MayanDoc/Print/PrintOcean.aspx?MinShipDate=" + this.Request.QueryString["MinShipDate"] + "&&MaxShipDate=" + this.Request.QueryString["MaxShipDate"] + "&&MaxSchedDate=" + this.Request.QueryString["MaxSchedDate"] + "&&MinSchedDate=" + this.Request.QueryString["MinSchedDate"] + "&&Destination=" + this.Request.QueryString["Destination"] + "&&ShippingLine=" + this.Request.QueryString["ShippingLine"] + "&&Container=" + this.Request.QueryString["Container"] + "&&Lot=" + this.Request.QueryString["Lot"] + "&&MinUSDADate=" + this.Request.QueryString["MinUSDADate"] + "&&MaxUSDADate=" + this.Request.QueryString["MaxUSDADate"] + "&&Sort=" + sort + "');";
		}
		
		protected void SetPopPrintingAccounting(string sort)
		{
            //TODO: uncomment later
            //Menu1.Items["Print"].JavascriptCommand = "PopPrintScreen('../../../MayanDoc/MayanDoc/Print/PrintAccounting.aspx?MinShipDate=" + this.Request.QueryString["MinShipDate"] + "&&MaxShipDate=" + this.Request.QueryString["MaxShipDate"] + "&&MaxSchedDate=" + this.Request.QueryString["MaxSchedDate"] + "&&MinSchedDate=" + this.Request.QueryString["MinSchedDate"] + "&&Destination=" + this.Request.QueryString["Destination"] + "&&ShippingLine=" + this.Request.QueryString["ShippingLine"] + "&&Container=" + this.Request.QueryString["Container"] + "&&Lot=" + this.Request.QueryString["Lot"]  + "&&Sort=" + sort + "');";
		}
		
		/// <summary>
		/// If the data is vaild it sets the DataSource and calls DataBind.
		/// If the data is not vaild calls JavaScriptRedirect to the param url
		/// </summary>
		/// <param name="data">Data to verify</param>
		/// <param name="repeater">Repeater to bind to</param>
		/// <param name="url">Url to redirect if data isn't valid</param>
		protected void DataBindOrRedirect(DataSet data,Repeater repeater,string url)
		{
			if(data == null || data.Tables.Count ==0 || data.Tables[0].DefaultView.Count == 0)
				JavaScriptRedirect(url);

			repeater.DataSource = data.Tables[0].DefaultView;
			repeater.DataBind();
		}

        private void RenderMenu(string pageType)
        {
            ArrayList menuMapArrayList = menuMap.GetMenus(pageType, null);

            BuildBaseMenu(MainMenu, menuMapArrayList);

            MainMenu.Visible = true;
            MainMenu.MaximumDynamicDisplayLevels = 20;
            MainMenu.Orientation = Orientation.Horizontal;
            MainMenu.DynamicEnableDefaultPopOutImage = false;
            MainMenu.DynamicHoverStyle.CssClass = "mouseoverdynamic";
            MainMenu.DynamicMenuStyle.CssClass = "menustyledynamic";
            MainMenu.DynamicMenuItemStyle.CssClass = "menuitemdynamic";
            MainMenu.StaticEnableDefaultPopOutImage = false;
            MainMenu.StaticMenuItemStyle.CssClass = "menuitem";
            MainMenu.StaticHoverStyle.CssClass = "mouseover";
            MainMenu.StaticMenuStyle.CssClass = "menustyle";
        }

        private void BuildBaseMenu(System.Web.UI.WebControls.Menu menu, ArrayList mainMenuArrayList)
        {
            for (int i = 0; i < mainMenuArrayList.Count; i++)
            {
                MenuItem newMenuItem = new MenuItem();
                newMenuItem.Text = ((MenuStruct)mainMenuArrayList[i]).Description;

                if (((MenuStruct)mainMenuArrayList[i]).Url == null || ((MenuStruct)mainMenuArrayList[i]).Url == string.Empty)
                    newMenuItem.NavigateUrl = "javascript: " + ((MenuStruct)mainMenuArrayList[i]).JavaCode;
                else
                    newMenuItem.NavigateUrl = ((MenuStruct)mainMenuArrayList[i]).Url;

                newMenuItem.Value = ((MenuStruct)mainMenuArrayList[i]).Security;

                newMenuItem = AddSubMenus(newMenuItem, ((MenuStruct)mainMenuArrayList[i]).SubMenus);

                menu.Items.Add(newMenuItem);
            }
        }

        private MenuItem AddSubMenus(MenuItem menuItem, ArrayList mainMenu)
        {
            for (int i = 0; i < mainMenu.Count; i++)
            {
                MenuItem newMenuItem = new MenuItem();
                newMenuItem.Text = ((MenuStruct)mainMenu[i]).Description;
                if (((MenuStruct)mainMenu[i]).Url == null || ((MenuStruct)mainMenu[i]).Url == string.Empty)
                    newMenuItem.NavigateUrl = "javascript: " + ((MenuStruct)mainMenu[i]).JavaCode;
                else
                    newMenuItem.NavigateUrl = ((MenuStruct)mainMenu[i]).Url;

                //newMenuItem.NavigateUrl = "javascript:document.location='http://www.yahoo.com/'";
                //newMenuItem.NavigateUrl = "javascript: BaseRFB_HideHeader(false); PrintThisPage(); BaseRFB_HideHeader(true);";
                newMenuItem.Value = ((MenuStruct)mainMenu[i]).Security;

                newMenuItem = AddSubMenus(newMenuItem, ((MenuStruct)mainMenu[i]).SubMenus);

                menuItem.ChildItems.Add(newMenuItem);
            }

            return menuItem;
        }

	}
}
