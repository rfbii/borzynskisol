//====================================================================
// This file is generated as part of Web project conversion.
// The extra class 'CriteriaBoxRecord' in the code behind file in 'lookups\misc\DynamicCriteriaBox.ascx.cs' is moved to this file.
//====================================================================




namespace KeyCentral.Lookups
 {

	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Collections;
	using System.Data.OleDb; 
	using Oracle.DataAccess.Client;
	using System.Configuration;
	using KeyCentral.Functions;
    using System.Xml.Serialization;
    
     [Serializable]
     [XmlInclude(typeof(KeyCentral.Lookups.CriteriaBoxRecord))] 
    public class CriteriaBoxRecord
	{
		public string key;
		public string text;
		public string description;
        public CriteriaBoxRecord()
        {
            key = string.Empty;
            text = string.Empty;
            description = string.Empty;
        }
    	public CriteriaBoxRecord(string _key,string _text,string _description)
		{
			key = _key;
			text = _text;
			description = _description;
		}
		public CriteriaBoxRecord(string _text)
		{
			key = "";
			text = _text;
			description = "";
        }
        #region Public Static Functions
        public static ArrayList GetArrayOfKeys(ArrayList list)
        {
            ArrayList ret = new ArrayList();

            if (list == null)
                return ret;

            foreach (CriteriaBoxRecord record in list)
                ret.Add(record.key);

            return ret;
        }
        public static ArrayList GetArrayOfCriteriaRecords(ArrayList list)
        {
            ArrayList ret = new ArrayList();

            int loopLimit = list.Count;
            for (int x = 0; x < loopLimit; x++)
                ret.Add(new CriteriaBoxRecord(list[x].ToString(), "", ""));

            return ret;

        }
        #endregion
	}

}