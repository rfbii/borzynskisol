using System;
using System.Web.Mail;
using System.IO;
using System.Configuration;

namespace KeyCentral.Functions
{
	public class Email
	{
		public Email(){}
		public static bool SendEMail(string from,string to,string subject,string body,string attachmentName,string attachmentData,System.Web.UI.Page page)
		{
			bool ret =true;
			string path = page.Server.MapPath(page.Request.ApplicationPath) +"\\Temp\\";
			string fileName ="";
			try
			{
				MailMessage mailMsg = new MailMessage(); 
				mailMsg.From=from;
				mailMsg.To=to;
				mailMsg.Subject=subject;
				mailMsg.Body=body;

				fileName = GetAvailbeFileName(path + attachmentName);
				SaveToFile(fileName,attachmentData);

				MailAttachment attachment = new MailAttachment(fileName);
				mailMsg.Attachments.Add(attachment);
			
				SmtpMail.SmtpServer = ConfigurationSettings.AppSettings["EmailServer"];//"ks-exchange1";
				SmtpMail.Send(mailMsg);

			}
			catch(Exception e)
			{
				ret = false;
				Misc.TraceMessage("Email error",e.Message);
			}
			finally
			{
				if(fileName!="")
					File.Delete(fileName);
			
			}
			return ret;
		}

		private static void SaveToFile(string fileName,string fileData)
		{
			FileStream file = new FileStream(fileName,System.IO.FileMode.CreateNew);
			StreamWriter writer = new StreamWriter(file);
			
			writer.Write(fileData);
			writer.Close();
			file.Close();
		}
		private static string GetAvailbeFileName(string fileName)
		{
			if(!File.Exists(fileName))
				return fileName;

			string fileBase = fileName.Substring(0,fileName.LastIndexOf("."));
			string fileExt =fileName.Remove(0,fileName.LastIndexOf("."));

			int x;
			for(x=1;File.Exists(fileBase+x.ToString()+fileExt) && x<5000;x++){}
			
			return fileBase+x.ToString()+fileExt;
		}
		
	}
}
