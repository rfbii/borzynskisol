using System;
using System.Web.UI;

namespace KeyCentral.Functions
{
	public class JavaStatusMessage
	{
		private Page page;

		public JavaStatusMessage(Page clientPage)
		{
			page = clientPage;

			WriteStatusDiv();
			WriteJavaCode();
			page.Response.Flush();
		}
		public void ShowMessage(string message)
		{
			page.Response.Write("<script language='javascript'>");
			page.Response.Write("ShowStatusMessage('" + message + "');");				
			page.Response.Write("</script>\n");
			page.Response.Flush();

		}
		private void WriteStatusDiv()
		{
			page.Response.Write(@"
				<font size='10' face='Comic Sans MS' color='maroon'>
				<div id='StatusDiv'  align='center' 
				style='POSITION: absolute; LEFT: 100px;TOP: 100px'>
				&nbsp;</div></font>");
		}
		private void WriteJavaCode()
		{
			#region JavaScripts
			string vars = @"
				var statusMessageDots = 0;
				var statusMessageDotMax = 10;
				var statusMessage;
				var statusMessageFunc=null;";
			string showStatusMessageTick = @"
				function ShowStatusMessageTick()
				{
					var output;
					output = statusMessage;
					statusMessageDots++;
					if(statusMessageDots>=statusMessageDotMax)
						statusMessageDots=1;

					for(var x = 0;x < statusMessageDots;x++)
						output += '.';
					StatusDiv.innerText =  output;
				}";
			string showStatusMessage = @"
				function ShowStatusMessage(text)
				{
					statusMessage=text;
					statusMessageDots=0;
					ShowStatusMessageTick();

					if(StatusDiv.style != null)
						StatusDiv.style.visibility = 'visible';

					if(statusMessageFunc==null)
						statusMessageFunc = window.setInterval('ShowStatusMessageTick()',500);
				}";
			string hideStatusMessage = @"
				function HideStatusMessage()
				{
					if(StatusDiv.style != null)
						StatusDiv.style.visibility = 'hidden';

					window.clearInterval(statusMessageFunc);
				}";
			#endregion

			page.Response.Write("<script language='javascript'>");
			page.Response.Write(vars);
			page.Response.Write(showStatusMessageTick);
			page.Response.Write(showStatusMessage);
			page.Response.Write(hideStatusMessage);
			page.Response.Write("</script>");

			page.RegisterStartupScript("HideStatusMessage","<script language='javascript'>HideStatusMessage();</script>");	
		}

	}
}
