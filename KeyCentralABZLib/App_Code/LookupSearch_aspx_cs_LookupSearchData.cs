//====================================================================
// This file is generated as part of Web project conversion.
// The extra class 'LookupSearchData' in the code behind file in 'ui\contextmenu\LookupSearch.aspx.cs' is moved to this file.
//====================================================================


using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Data.SqlClient;
using KeyCentral.Functions;
using KeyCentral.Lookups;


namespace KeyCentral.UI.ContextMenu
 {


	public struct LookupSearchData
	{
		public ArrayList SearchItems;
		/// <summary>
		/// Select Idx as \"Keys\",	display field1, display field2 where 1=1 #WhereClause:Table.Field#
		/// </summary>
		public string LookupSQL;
		public string ToSaveVar;
	}

}