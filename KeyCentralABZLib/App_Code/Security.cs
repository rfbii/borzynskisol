using System;
using System.Web.SessionState;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using KeyCentral.Lookups;
using System.Collections.Generic;

namespace KeyCentral.Functions
{
	public class Security
	{
		public Security(){}

		public static bool CheckSecurity(HttpSessionState Session,string Name)
		{
			bool ret=false;

            VerifySecurtyTypeExists(Name);

			if (Session["Administrator"].ToString() != "Yes")
			{
				SqlConnection Conn = Misc.GetSQLConn("CheckSecurity");
				SqlCommand selectCMDGroup = new SqlCommand("Select SType_Detail.Stype_Detail_Key From User_Stype_Group,SType_Group_Detail,SType_Detail Where User_Stype_Group.SType_Group_Key =  SType_Group_Detail.SType_Group_Key and SType_Group_Detail.Stype_Detail_Key = SType_Detail.Stype_Detail_Key and User_Stype_Group.User_Key = @User and SType_Detail.Detail = @ReportName", Conn);
				selectCMDGroup.Parameters.Add("@ReportName",Name);
				selectCMDGroup.Parameters.Add("@User",Session["User_Key"]);
			
				SqlCommand selectCMDUser = new SqlCommand("select SType_Detail.Stype_Detail_Key from User_Stype,SType_Detail where User_Stype.SType_Detail_Key = SType_Detail. SType_Detail_Key and User_Stype.User_Key = @User and SType_Detail.Detail = @ReportName", Conn);
				selectCMDUser.Parameters.Add("@ReportName",Name);
				selectCMDUser.Parameters.Add("@User",Session["User_Key"]);	

				if(selectCMDGroup.ExecuteScalar()!=null)
					ret=true;
				if(selectCMDUser.ExecuteScalar()!=null)
					ret=true;

				Conn.Close();
				Conn.Dispose();
			}
			else
				ret=true;
			return ret;
		}
        private static void VerifySecurtyTypeExists(string Name)
        {
            List<string> SecurityTypeList;

            //If no list creat it (We could do a lookup and get all that exist now.....)
            if (System.Web.HttpContext.Current.Application["SecurityTypeList"] == null)
            {
                List<string> tempList = new List<string>();
                tempList.Add("ADMINONLY");
                System.Web.HttpContext.Current.Application["SecurityTypeList"] = tempList;
            }

            SecurityTypeList = (List<string>)System.Web.HttpContext.Current.Application["SecurityTypeList"];

            if (SecurityTypeList.Contains(Name) == false)
            {
                AddSecurityDetail("Module",Name);
                SecurityTypeList.Add(Name);
                SecurityTypeList.Sort();
            }
           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Name"></param>
        /// <returns>SecurityType_Detail_Key</returns>
        private static int AddSecurityDetail(string securityType,string securityTypeDetail)
        {
            if (securityTypeDetail == string.Empty)
                throw new Exception("Unable to add a blank SecurityType_Detail.");

            int? SType_Key = GetSType_Key(securityType);

            if (SType_Key == null)
                throw new Exception("Unable to add Security Detail to a SType that does not exists.");

            return AddSecurityDetail(SType_Key.Value, securityTypeDetail);
        }
        private static int AddSecurityDetail(int sType_Key, string securityTypeDetail)
        {
            if (securityTypeDetail == string.Empty)
                throw new Exception("Unable to add a blank SecurityType_Detail.");

            int? STypeDetail_Key = GetSType_Detail_Key(sType_Key, securityTypeDetail);

            //If the Detail already exists just return it
            if (STypeDetail_Key != null)
                return STypeDetail_Key.Value;

            SqlConnection Conn = Misc.GetSQLConn();
            SqlCommand insertDetailCMD = new SqlCommand("insert into [SType_Detail] (SType_Key,Detail) values(@STypeKey,@Detail) Select @@Identity", Conn);
            insertDetailCMD.Parameters.AddWithValue("@Detail", securityTypeDetail);
            insertDetailCMD.Parameters.AddWithValue("@STypeKey", sType_Key);

            STypeDetail_Key = (int?)(decimal?)insertDetailCMD.ExecuteScalar();

            insertDetailCMD.Dispose();
            Misc.CleanUpSQL(Conn);

            return STypeDetail_Key.Value;
        }

        public static int? GetSType_Key(string description)
        {
            SqlConnection Conn = Misc.GetSQLConn();
            SqlCommand sTypeCMD = new SqlCommand("select SType_Key from SType where [Description] = @Description", Conn);
            sTypeCMD.Parameters.AddWithValue("@Description", description);

            int? SType_Key = (int?)sTypeCMD.ExecuteScalar();

            sTypeCMD.Dispose();
            Misc.CleanUpSQL(Conn);

            return SType_Key;
        }

        public static int? GetSType_Detail_Key(int SType_Key, string description)
        {
            SqlConnection Conn = Misc.GetSQLConn();
            SqlCommand lookupCMD = new SqlCommand("select SType_Detail_Key from SType_Detail where SType_Key = @SType_Key and Detail= @Detail", Conn);
            lookupCMD.Parameters.AddWithValue("@SType_Key", SType_Key);
            lookupCMD.Parameters.AddWithValue("@Detail", description);

            int? SType_Detail_Key = (int?)lookupCMD.ExecuteScalar();

            lookupCMD.Dispose();
            Misc.CleanUpSQL(Conn);

            return SType_Detail_Key;
        }


		public static void BuildLimitedToKeys(HttpSessionState Session,string sType,string sessionString)
		{
			ArrayList values = new ArrayList();
			SqlConnection Conn = Misc.GetSQLConn("BuildLimitedToKeys");

			SqlCommand selectCMDGroup = new SqlCommand("select SType_Detail.Detail from User_SType_Group,SType_Group_Detail, SType_Detail,SType where User_SType_Group.SType_Group_Key = SType_Group_Detail.SType_Group_Key and SType_Group_Detail.SType_Detail_Key = SType_Detail.SType_Detail_Key and SType_Detail.SType_Key = SType.SType_Key and SType.Description = @SType and  User_SType_Group.User_Key = @User_Key", Conn);
			selectCMDGroup.Parameters.Add("@SType",sType);
			selectCMDGroup.Parameters.Add("@User_Key",Session["User_Key"]);
			
			SqlCommand selectCMDUser = new SqlCommand("select SType_Detail.Detail from User_SType, SType_Detail,SType where  User_SType.SType_Detail_Key = SType_Detail.SType_Detail_Key and SType_Detail.SType_Key = SType.SType_Key and SType.Description = @SType and User_SType.User_Key = @User_Key", Conn);
			selectCMDUser.Parameters.Add("@SType",sType);
			selectCMDUser.Parameters.Add("@User_Key",Session["User_Key"]);	

			SqlDataReader myReader = selectCMDGroup.ExecuteReader();
			while(myReader.Read())
				values.Add(new CriteriaBoxRecord(myReader.GetString(0),"",""));
			myReader.Close();
			
			myReader = selectCMDUser.ExecuteReader();
			while(myReader.Read())
				values.Add(new CriteriaBoxRecord(myReader.GetString(0),"",""));
			myReader.Close();
			
			Conn.Close();
			Conn.Dispose();

			if(sType.ToLower() == "grower" || sType.ToLower() == "warehouse")
			{
				if(values.Count == 0)
				{
					if(sType.ToLower() == "grower")
						values.Add(new CriteriaBoxRecord("-2","ERROR","No Valid Growers"));
					else if(sType.ToLower() == "warehouse")
						values.Add(new CriteriaBoxRecord("-2","ERROR","No Valid Warehouses"));
				}
				else if(values.Count > 0)
				{
					for(int i = 0; i < values.Count; i++)
					{
						if(((CriteriaBoxRecord)values[i]).key == "-1")
						{
							i = values.Count;
							values = new ArrayList();
						}
					}
				}
			}

			Session.Add(sessionString,values);
		}
	}
}
