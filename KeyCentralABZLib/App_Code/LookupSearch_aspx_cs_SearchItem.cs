//====================================================================
// This file is generated as part of Web project conversion.
// The extra class 'SearchItem' in the code behind file in 'ui\contextmenu\LookupSearch.aspx.cs' is moved to this file.
//====================================================================


using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Data.SqlClient;
using KeyCentral.Functions;
using KeyCentral.Lookups;


namespace KeyCentral.UI.ContextMenu
 {


	public struct SearchItem
	{
		public string Description;
		public string FieldName;
		public SearchType Type;
		public SearchItem(string description,string fieldName,SearchType type)
		{
			Description = description;
			FieldName = fieldName;
			Type = type;
		}
	}

}