using System;
using System.Collections;
using System.Data;

namespace KeyCentral.Functions
{
	public class ReportTotalsHelper
	{
		#region private Structs and classes
		private struct SubTotalFieldDef
		{
			public string field;
			public int index;
			public SubTotalFieldDef(int i,string field){index=i;this.field=field;}
		}
		private struct SubTotalDef
		{
			public ArrayList byFields; //of string
			public ArrayList totalFields;// of SubTotalFieldDef
			public SubTotalDef(ArrayList by,ArrayList total){byFields=by;totalFields=total;}
		}
		public class TotalRecord
		{
			public string fields;
			public double value;
			public TotalRecord(string fields,double val){this.fields=fields;value=val;}
		}
		public class ReportTotal
		{
			public ArrayList totalRecords;//of TotalRecord
			public ReportTotal(){totalRecords = new ArrayList();}
			public void Update(string fields,double val)
			{
				bool found=false;
				for(int x=0;x<totalRecords.Count;x++)
				{
					if(((TotalRecord)totalRecords[x]).fields==fields)
					{
						((TotalRecord)totalRecords[x]).value += val;
						found=true;
					}
				}
				if(found==false)
					totalRecords.Add(new TotalRecord(fields,val));
			}
		}
		#endregion

		#region Declares
		private ArrayList subTotalDefs; // of SubTotalDef
		private ArrayList reportTotals; // of ReportTotal
		#endregion

		public ArrayList ReportTotals{get{return reportTotals;}}

		#region public Functions
		#region AddSubTotal functions
		public void AddSubTotal2And1(string by1,string by2,string total1)
		{
			ArrayList byFields = new ArrayList();
			ArrayList totalFields = new ArrayList();

			byFields.Add(by1);
			byFields.Add(by2);

			int index = reportTotals.Add(new ReportTotal());

			totalFields.Add(new SubTotalFieldDef(index,total1));
			AddSubTotal(byFields,totalFields);
		}
		public void AddSubTotal3And1(string by1,string by2,string by3,string total1)
		{
			ArrayList byFields = new ArrayList();
			ArrayList totalFields = new ArrayList();

			byFields.Add(by1);
			byFields.Add(by2);
			byFields.Add(by3);

			int index = reportTotals.Add(new ReportTotal());

			totalFields.Add(new SubTotalFieldDef(index,total1));
			AddSubTotal(byFields,totalFields);
		}
		public void AddSubTotal4And1(string by1,string by2,string by3,string by4,string total1)
		{
			ArrayList byFields = new ArrayList();
			ArrayList totalFields = new ArrayList();

			byFields.Add(by1);
			byFields.Add(by2);
			byFields.Add(by3);
			byFields.Add(by4);

			int index = reportTotals.Add(new ReportTotal());

			totalFields.Add(new SubTotalFieldDef(index,total1));
			AddSubTotal(byFields,totalFields);
		}
		public void AddSubTotal5And1(string by1,string by2,string by3,string by4,string by5,string total1)
		{
			ArrayList byFields = new ArrayList();
			ArrayList totalFields = new ArrayList();

			byFields.Add(by1);
			byFields.Add(by2);
			byFields.Add(by3);
			byFields.Add(by4);
			byFields.Add(by5);

			int index = reportTotals.Add(new ReportTotal());

			totalFields.Add(new SubTotalFieldDef(index,total1));
			AddSubTotal(byFields,totalFields);
		}
		
		public void AddSubTotal1And1(string by1,string total1)
		{
			ArrayList byFields = new ArrayList();
			ArrayList totalFields = new ArrayList();

			byFields.Add(by1);

			int index = reportTotals.Add(new ReportTotal());
			totalFields.Add(new SubTotalFieldDef(index,total1));

			AddSubTotal(byFields,totalFields);
		}
		public void AddSubTotal1And2(string by1,string total1,string total2)
		{
			ArrayList byFields = new ArrayList();
			ArrayList totalFields = new ArrayList();

			byFields.Add(by1);

			int index = reportTotals.Add(new ReportTotal());
			totalFields.Add(new SubTotalFieldDef(index,total1));
			
			index = reportTotals.Add(new ReportTotal());
			totalFields.Add(new SubTotalFieldDef(index,total2));

			AddSubTotal(byFields,totalFields);
		}
		public void AddSubTotal1And3(string by1,string total1,string total2,string total3)
		{
			ArrayList byFields = new ArrayList();
			ArrayList totalFields = new ArrayList();

			byFields.Add(by1);

			int index = reportTotals.Add(new ReportTotal());
			totalFields.Add(new SubTotalFieldDef(index,total1));

			index = reportTotals.Add(new ReportTotal());
			totalFields.Add(new SubTotalFieldDef(index,total2));

			index = reportTotals.Add(new ReportTotal());
			totalFields.Add(new SubTotalFieldDef(index,total3));

			AddSubTotal(byFields,totalFields);
		}
		public void AddSubTotal1And4(string by1,string total1,string total2,string total3,string total4)
		{
			ArrayList byFields = new ArrayList();
			ArrayList totalFields = new ArrayList();

			byFields.Add(by1);

			int index = reportTotals.Add(new ReportTotal());
			totalFields.Add(new SubTotalFieldDef(index,total1));

			index = reportTotals.Add(new ReportTotal());
			totalFields.Add(new SubTotalFieldDef(index,total2));
			
			index = reportTotals.Add(new ReportTotal());
			totalFields.Add(new SubTotalFieldDef(index,total3));
			
			index = reportTotals.Add(new ReportTotal());
			totalFields.Add(new SubTotalFieldDef(index,total4));

			AddSubTotal(byFields,totalFields);
		}
		#endregion
		public ReportTotalsHelper()
		{
			subTotalDefs = new ArrayList();
			reportTotals = new ArrayList();
		}

		private void AddSubTotal(ArrayList byFields,ArrayList totalFields)
		{
			subTotalDefs.Add(new SubTotalDef(byFields,totalFields));
		}

		public void CalcTotals(DataSet data)
		{
			CheckFieldNames(data);
			foreach(DataRow row in  data.Tables[0].Rows)//Foreach row in the dataset
				foreach(SubTotalDef totalDef in subTotalDefs)//Foreach of the defined subTotals
					foreach(SubTotalFieldDef field in totalDef.totalFields)//Foreach datafield in the subTotal
						AddTotal(BuildFieldsList(totalDef.byFields,row),field.index,row[field.field ]);
		}
		public double GetTotal(int reportIndex,string byFields)
		{
			ReportTotal total = (ReportTotal)reportTotals[reportIndex];
			for(int x=0;x<total.totalRecords.Count;x++)
			{
				if(((TotalRecord)total.totalRecords[x]).fields==byFields)
					return ((TotalRecord)total.totalRecords[x]).value;
			}
			return 0;
		}
		public ArrayList GetTotalFields(int reportIndex)
		{
			ArrayList ret= new ArrayList();

			ReportTotal total = (ReportTotal)reportTotals[reportIndex];
			for(int x=0;x<total.totalRecords.Count;x++)
			{
				ret.Add(((TotalRecord)total.totalRecords[x]).fields);
			}

			return ret;
		}
		
		public double GetTotal(int reportIndex,int fieldIndex,string byFields)
		{
			ReportTotal total = (ReportTotal)reportTotals[reportIndex];
			for(int x=0;x<total.totalRecords.Count;x++)
			{
				if(((TotalRecord)total.totalRecords[x]).fields==byFields)
					return ((TotalRecord)total.totalRecords[x]).value;
			}
			return 0;
		}
		#endregion

		#region private helpers
		private void CheckFieldNames(DataSet data)
		{
			foreach(SubTotalDef totalDef in subTotalDefs)//Foreach of the defined subTotals
			{
				foreach(SubTotalFieldDef field in totalDef.totalFields)//Foreach datafield in the subTotal
					if(FieldExistsInDataSet(data,field.field)==false)
						throw new Exception("Report Helper Error: Field " + field.field + " Does not exist in the DataSet.");

				foreach(string fieldName in totalDef.byFields)
					if(FieldExistsInDataSet(data,fieldName)==false)
						throw new Exception("Report Helper Error: Field " + fieldName + " Does not exist in the DataSet.");
			}
		}
		private bool FieldExistsInDataSet(DataSet data,string fieldName)
		{
			foreach(System.Data.DataColumn column in data.Tables[0].Columns)
				if(column.ColumnName.ToLower() == fieldName.ToLower())
					return true;

			return false;
		}
		private void AddTotal(string byFields,int index,object value)
		{
			try
			{
				if(value.ToString().Trim() != "")
					((ReportTotal)reportTotals[index]).Update(byFields,double.Parse(value.ToString()));
			}
			catch(Exception ex){ex=ex;}
		}
		private string BuildFieldsList(ArrayList fields,DataRow row)
		{
			string ret="";
			for(int x=0;x<fields.Count;x++)
			{
				if(x!=(fields.Count-1))
					ret += row[fields[x].ToString()] + ",";
				else
					ret += row[fields[x].ToString()];
			}
			return ret;
		}
		#endregion
	}
}
