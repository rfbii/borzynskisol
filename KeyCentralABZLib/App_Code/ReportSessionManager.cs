//#define DebugSessionManager
using System;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Collections;

namespace KeyCentral.Functions
{
	/// <summary>
	/// This Class is responible for managing the session var for reports
	/// </summary>
	public class ReportSessionManager
	{
		BasePage page;
		ArrayList sessionVars;
		string reportName;


		public ReportSessionManager(BasePage page,string reportName)
		{
			this.page = page;
			this.reportName = reportName;
			sessionVars = new ArrayList();
			
			#if (DebugSessionManager)
				page.Log("SessionMananger:" + reportName);
			#endif
		}
		public void SaveSessions()
		{
			string sessionString = CreateSessionVar();
			page.Session.Add("ReportSessionManager",sessionString);

#if (DebugSessionManager)
			page.Log("SessionMananger:" + sessionString);
#endif
		}

		public bool CheckReportVars()
		{
			string lastReportName = GetLastReportName();

            if (lastReportName != reportName && lastReportName != "")
            {
                ClearLastReportVars();
            }

			if (lastReportName != reportName)
				return false;
			else
				return true;
		}

		public void AddSession(string name,object value)
		{
			page.Session.Add(name,value);
			sessionVars.Add(name);
		}

		private void ClearLastReportVars()
		{
			
#if (DebugSessionManager)
			page.Log("Clearing Last Report Vars.");
#endif
			ArrayList lastVars = GetLastVars();

			foreach(string str in lastVars)
			{
				page.Session.Remove(str);
#if (DebugSessionManager)
				page.Log("Removed:" + str);
#endif
			}

			page.Session.Remove("ReportSessionManager");
		}

		private string CreateSessionVar()
		{
			string ret = this.reportName;
			
			foreach(string str in this.sessionVars)
				ret += "~" + str;

			return ret; 
		}
		private ArrayList GetLastVars()
		{
			ArrayList lastVars = new ArrayList();
			string lastSessionVar = (string)page.Session["ReportSessionManager"];

			if(reportName != null && reportName != "")
			{
				string[] varSplit = lastSessionVar.Split('~');

				for(int x=1;x<varSplit.Length;x++)//I mean to start at 1, I want to skip the report name.
					lastVars.Add(varSplit[x]);
			}

			return lastVars;
		}
		private string GetLastReportName()
		{
			string reportName;
			reportName = (string)page.Session["ReportSessionManager"];

			if(reportName == null || reportName == "")
				return "";
			else if(reportName.IndexOf("~")==-1)
				return reportName;
			else
				return reportName.Substring(0,reportName.IndexOf("~"));
		}
	}
}
