using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace KeyCentral.Functions
{
	public class SessionWrapper
	{
		static Hashtable _session = new Hashtable();

		private static object getValue(string key)
		{
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
			{
				return _session[key];
			}
			else
			{
				return System.Web.HttpContext.Current.Session[key];
			}
		}
		private static void setValue(string key, object value)
		{
            if (System.Web.HttpContext.Current == null || System.Web.HttpContext.Current.Session == null)
			{
				_session[key] = value;
			}
			else
			{
				System.Web.HttpContext.Current.Session[key] = value;
			}
		}

		public static string Company_Key
		{
			get { return (string)getValue("Company_Key"); }
			set { setValue("Company_Key", value); }
		}


		public static string Report_Number
		{
			get { return (string)getValue("Report_Number"); }
			set { setValue("Report_Number", value); }
		}

		public static string User_Name
		{
			get { return (string)getValue("User_Name"); }
			set { setValue("User_Name", value); }
		}

	}
}