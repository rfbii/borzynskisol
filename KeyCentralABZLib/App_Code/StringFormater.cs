using System;
using System.Text.RegularExpressions;

namespace KeyCentral.Functions
{
	public class StringFormater
	{
		public StringFormater(){}

		//Check
		public static void CheckDate(string str,string desc)
		{
			if(!IsDate(str))
				throw new Exception( desc + " is not formated correctly. Value=" + str);
		}
		public static bool IsDate(string date)
		{
			if(date.Trim() == "" || Regex.IsMatch(date,@"^( |1[0-2]|0[1-9]|[1-9])(/|-)(3[0-1]|[1-2][0-9]|[1-9]|0[1-9])(/|-)((199[0-9]|200[0-9])|(9[0-9]|0[0-9]))$"))
				return true;
			else
				return false;
		}
		//Format
		public static string FormatString(string inValue,int length)
		{
			if(inValue.Length >length)
				return inValue.Substring(0,length);
			else
				return inValue;
		}

		public static string FormatString(object inValue,int length)
		{
			if(((string)inValue).Length >length)
				return ((string)inValue).Substring(0,length);
			else
				return (string)inValue;
		}

		public static string FormatPercentage(float inValue,int decimals)
		{
			if(decimals==2)
				return decimal.Round((decimal) inValue,4).ToString("###.00%");
			else if(decimals==1)
				return decimal.Round((decimal) inValue,3).ToString("###.0%");
			else
				return decimal.Round((decimal) inValue,2).ToString("###%");
		}
		public static string FormatPercentage(double inValue,int decimals)
		{
			try
			{
				if(decimals==2)
					return decimal.Round((decimal) inValue,4).ToString("###.00%");
				else if(decimals==1)
					return decimal.Round((decimal) inValue,3).ToString("###.0%");
				else
					return decimal.Round((decimal) inValue,2).ToString("###%");
			}
			catch(Exception ex)
			{
				ex=ex;
				return "0.00%";
			}
		}
		/// <summary>
		/// Format string for use in a CSV file
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static string FormatForCSV(string str)
		{
			return str.Replace("\"","''");
		}
	}
}
