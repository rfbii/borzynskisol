#define TraceDBConnection
using System;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using System.Configuration;
using System.Data.OleDb; 
using Oracle.DataAccess.Client;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Collections;
using KeyCentral.Lookups;
using System.Reflection;

namespace KeyCentral.Functions
{
    public enum SqlNeededAction{Insert,Update,Delete,Nothing};
    public class Misc
    {
        public Misc(){}
        public static string GetUniqueLot(string lot)
        {
            return  lot.Substring(0,2) + lot.Substring(3,4);
        }
        public static double Round(double val,int decimalPlaces)
        {
            double multiplier = Math.Pow(10,decimalPlaces);
            val = val * multiplier;
            val +=.5;
            val = Math.Floor(val);
            val = val / multiplier;

            return val;
        }

        #region Version functions
        public static string GetVersionString(Page caller)
        {
            string ret = "";
            string modual = "";
            string[] pathList = caller.Request.Url.AbsolutePath.Split('/');
            modual = pathList[pathList.Length-4];

            if (System.Web.HttpContext.Current.Application["VersionInfo:" + modual] == null)
            {
                ret = GetVersionString(modual);
                System.Web.HttpContext.Current.Application["VersionInfo:" + modual] = ret;
            }
            else
                ret = (string)System.Web.HttpContext.Current.Application["VersionInfo:" + modual];
             

            return ret;
        }
        public static string GetVersionString(string modual)
        {
            string ret = string.Empty;

            AppDomain currentDomain = AppDomain.CurrentDomain;
            Assembly[] loadedAssembly = currentDomain.GetAssemblies();

            foreach (Assembly assembly in loadedAssembly)
            {
                if (assembly.GetName().Name.ToLower() == modual.ToLower())
                {
                    object[] types = assembly.GetCustomAttributes(typeof(AssemblyInformationalVersionAttribute), false);

                    ret += ((AssemblyInformationalVersionAttribute)types[0]).InformationalVersion;
                }
            }

            return ret;
        }
        #endregion

        #region Display Formating
        public static string DisplayHTMLForCurency(decimal value)
        {

            if(value<= -0.01m)
                return "<font color='red'>" + value.ToString("C") + "</font>";
            else
                return value.ToString("C");
        }
        public static string DisplayHTMLForCurency(double value)
        {
            if (value <= -0.01d)
            {
                return "<font color='red'>" + value.ToString("C") + "</font>";
            }
            else
            {
                return value.ToString("C");
            }
        }
        public static string DisplayCurency(double value)
        {
            return value.ToString("C");
        }
        public static string DisplayFormat(decimal value)
        {
            if (value == 0)
                return value.ToString();
            else
                return decimal.Round(value,0).ToString("###,###");
        }
        public static string DisplayFormat2(decimal value)
        {
            if (value == 0)
                return value.ToString();
            else
                return decimal.Round(value,2).ToString("###,###.00");
        }
        public static string DisplayFormat2(double value)
        {
            if (value == 0)
                return value.ToString();
            else
                return Round(value,2).ToString("###,###.00");
        }
        public static string DisplayFormat4dec(double value)
        {
            if(value<= -0.00001d)
            {
                value = value*-1;
                return "<font color='red'>" +"(" + Round(value, 4).ToString("##.0000")+")" + "</font>";
            }
            else
                return Round(value,4).ToString("##.0000");
        }
        public static string DisplayFormat0dec(double value)
        {
            if (value<=-0.00001d)
            {
                value = value*-1;
                return "<font color='red'>" +"(" + Round(value,0).ToString("###,###")+")" + "</font>";
            }
            else if (value == 0)
                return value.ToString();
            else
                return Round(value,0).ToString("###,###");
        }
        public static string DisplayFormat(double value)
        {
            if (value == 0)
                return value.ToString();
            else
                return Round(value, 0).ToString("###,###");
        }
        public static string FormatDateTime(string dateTime)
        {
            if (dateTime == null)
                return null;
            if (dateTime.IndexOf(" ") > -1)
                dateTime = dateTime.Substring(0, dateTime.IndexOf(" "));

            return dateTime;
        }
        #endregion

        #region SQL and Oracle Functions
        public static OracleConnection GetOracleConn(string famousCompanyKey)
        {
            string server = System.Configuration.ConfigurationSettings.AppSettings["FamousServer"];
            string report = famousCompanyKey;
            OracleConnection OracleConn = new OracleConnection("Data Source=" + server + ";User ID=" + report + "_RPT;Password=FAMOUS");
            OracleConn.Open();
            return OracleConn;
        }
        public static OracleConnection GetOracleConn(HttpSessionState Session)
        {
            return GetOracleConn(Session["Report_Number"].ToString());
        }
        public static SqlConnection GetSQLConn()
        {
            return GetSQLConn("Generic");
        }
        public static SqlConnection GetSQLConn(string reason)
        {
#if(TraceDBConnection && DEBUG)
            TraceMessage("GetSQLConn", reason);
#endif
            SqlConnection Conn = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["CONNECTION_STRING_SQLdb"]);
            Conn.Open();
            return Conn;
        }
        public static void CleanUpSQL(SqlConnection Conn)
        {
            Conn.Close();
            Conn.Dispose();
        }
        public static void CleanUpOracle(OracleConnection Conn)
        {
            Conn.Close();
            Conn.Dispose();
        }


        public static SqlNeededAction GetNeededAction(string orgValue, string newValue)
        {
            if (orgValue == newValue)
                return SqlNeededAction.Nothing;
            else if (orgValue == "")//We know they aren't the same, so if org is "" it must be an insert
                return SqlNeededAction.Insert;
            else if (newValue == "")//we know they aren't the same, so if new is "" it must be a delete
                return SqlNeededAction.Delete;
            else//they aren't the same and neither are empty, must be an update
                return SqlNeededAction.Update;
        }

        public static string FormatOracle(HttpSessionState Session, string Oracle)
        {
            return FormatOracle(Session["Report_Number"].ToString(), Oracle);
        }
        public static string FormatOracle(string report_Name, string Oracle)
        {
            return Oracle.Replace("#COMPANY_NUMBER#", report_Name);
        }
        #endregion

        #region Query String functions
        public static string GetQueryString(System.Web.HttpRequest Request, string field)
        {
            if (Request[field] == null)
                return "";
            else
                return Request[field].ToString();
        }
        public static string DecodeQueryString(string queryString)
        {
            if (queryString == null)
                return "";
            else
                return queryString.Replace("~0", "?").Replace("!0", "&&").Replace("~1", "~0").Replace("!1", "!0").Replace("~2", "~1").Replace("!2", "!1").Replace("~3", "~2").Replace("!3", "!2");
        }
        public static string EncodeQueryString(string queryString)
        {
            while (queryString.IndexOf("&&") != -1)
                queryString = queryString.Replace("&&", "&");
            queryString = queryString.Replace("&", "&&");
            return queryString.Replace("!2", "!3").Replace("~2", "~3").Replace("!1", "!2").Replace("~1", "~2").Replace("!0", "!1").Replace("~0", "~1").Replace("&&", "!0").Replace("?", "~0");
        }
        #endregion

        #region Listbox functions
        public static int GetWarehouseNames(HttpSessionState Session, ListBox list, ListBox list2)
        {
            OracleConnection OracleConn = GetOracleConn(Session);
            OracleCommand OracleData = new OracleCommand(FormatOracle(Session, "Select Ic_Warehouse.WarehouseIdx,Fc_Name_Location.Descr From #COMPANY_NUMBER#.Ic_Warehouse, #COMPANY_NUMBER#.Fc_Name_Location Where Ic_Warehouse.NameIdx = Fc_Name_Location.NameIdx and Ic_Warehouse.NameLocationSeq = Fc_Name_Location.NameLocationSeq Order by Descr"), OracleConn);

            int x = 0;

            OracleData.BindByName = true;

            Oracle.DataAccess.Client.OracleDataReader myReader = OracleData.ExecuteReader();
            while (myReader.Read())
            {
                x = TextIsInListBox(list, myReader.GetValue(0).ToString());
                if (x != -1)
                {
                    list.Items.Add(new ListItem(myReader.GetString(1), list.Items[x].Value));
                    list.Items.Remove(list.Items[x]);
                }
                x = TextIsInListBox(list2, myReader.GetValue(0).ToString());
                if (x != -1)
                {
                    list2.Items.Add(new ListItem(myReader.GetString(1), list2.Items[x].Value));
                    list2.Items.Remove(list2.Items[x]);
                }

            }
            myReader.Close();
            myReader.Dispose();
            OracleData.Dispose();
            OracleConn.Close();
            OracleConn.Dispose();
            return 1;
        }
        public static int GetGrowerNames(HttpSessionState Session, ListBox list, ListBox list2)
        {
            OracleConnection OracleConn = GetOracleConn(Session);
            OracleCommand OracleData = new OracleCommand(FormatOracle(Session, "Select NameIdx,Concat(rpad(Id,6,'_'),lpad(LastCoName,40)) as GrowerID  From #COMPANY_NUMBER#.Ga_Grower,#COMPANY_NUMBER#.Fc_Name where Fc_Name.NameIdx = GA_Grower.GrowerNameIdx Order by Id"), OracleConn);

            int x = 0;

            OracleData.BindByName = true;

            Oracle.DataAccess.Client.OracleDataReader myReader = OracleData.ExecuteReader();
            while (myReader.Read())
            {
                x = TextIsInListBox(list, myReader.GetValue(0).ToString());
                if (x != -1)
                {
                    list.Items.Add(new ListItem(myReader.GetString(1), list.Items[x].Value));
                    list.Items.Remove(list.Items[x]);
                }
                x = TextIsInListBox(list2, myReader.GetValue(0).ToString());
                if (x != -1)
                {
                    list2.Items.Add(new ListItem(myReader.GetString(1), list2.Items[x].Value));
                    list2.Items.Remove(list2.Items[x]);
                }

            }
            myReader.Close();
            myReader.Dispose();
            OracleData.Dispose();
            OracleConn.Close();
            OracleConn.Dispose();
            return 1;
        }
        public static int GetGrowerNameList(HttpSessionState Session, ListBox list)
        {
            OracleConnection OracleConn = GetOracleConn(Session);
            OracleCommand OracleData = new OracleCommand(FormatOracle(Session, "Select NameIdx,Concat(rpad(Id,6,'_'),lpad(LastCoName,40)) as GrowerID  From #COMPANY_NUMBER#.Ga_Grower,#COMPANY_NUMBER#.Fc_Name where Fc_Name.NameIdx = GA_Grower.GrowerNameIdx Order by Id"), OracleConn);

            int x = 0;

            OracleData.BindByName = true;

            Oracle.DataAccess.Client.OracleDataReader myReader = OracleData.ExecuteReader();
            while (myReader.Read())
            {
                x = TextIsInListBox(list, myReader.GetValue(0).ToString());
                if (x != -1)
                {
                    list.Items.Add(new ListItem(myReader.GetString(1), list.Items[x].Value));
                    list.Items.Remove(list.Items[x]);
                }
            }
            myReader.Close();
            myReader.Dispose();
            OracleData.Dispose();
            OracleConn.Close();
            OracleConn.Dispose();
            return 1;
        }
        public static int GetCommodityNames(HttpSessionState Session, ListBox list, ListBox list2)
        {
            OracleConnection OracleConn = GetOracleConn(Session);
            OracleCommand OracleData = new OracleCommand(FormatOracle(Session, @"
				Select 
                    Ic_Ps_Commodity.Name as Commodity, 
                    Ic_Ps_Commodity.CmtyIdx as CommodityKey
                From 
                    #COMPANY_NUMBER#.Ic_Ps_Commodity
                Order By 1,2"), OracleConn);

            int x = 0;

            OracleData.BindByName = true;

            Oracle.DataAccess.Client.OracleDataReader myReader = OracleData.ExecuteReader();
            while (myReader.Read())
            {
                x = TextIsInListBox(list, myReader.GetValue(1).ToString());
                if (x != -1)
                {
                    list.Items.Add(new ListItem(myReader.GetString(0), list.Items[x].Value));
                    list.Items.Remove(list.Items[x]);
                }
                x = TextIsInListBox(list2, myReader.GetValue(1).ToString());
                if (x != -1)
                {
                    list2.Items.Add(new ListItem(myReader.GetString(0), list2.Items[x].Value));
                    list2.Items.Remove(list2.Items[x]);
                }

            }
            myReader.Close();
            myReader.Dispose();
            OracleData.Dispose();
            OracleConn.Close();
            OracleConn.Dispose();
            return 1;
        }
        public static int TextIsInListBox(ListBox list, string textToFind)
        {
            for (int x = 0; x < list.Items.Count; x++)
                if (list.Items[x].Text.Trim() == textToFind.Trim())
                    return x;

            return -1;
        }
        public static int ValueIsInListBox(ListBox list, string valueToFind)
        {
            for (int x = 0; x < list.Items.Count; x++)
                if (list.Items[x].Value.Trim() == valueToFind.Trim())
                    return x;

            return -1;
        }
        public static int ValueIsInDropBox(DropDownList list, string valueToFind)
        {
            for (int x = 0; x < list.Items.Count; x++)
                if (list.Items[x].Value.Trim() == valueToFind.Trim())
                    return x;

            return -1;
        }
        #endregion

        #region InverDataSet
        //Inverts on the first field, the first column's data must be sorted
        //First field is the key, second is the new rows 3rd is data
        public static DataSet InvertDataSet(DataSet data)
        {
            string lastKey = "";
            DataSet ret = new DataSet();
            ret.Tables.Add("Table");
            AddColumninDataSet(ret, data.Tables[0].Columns[0].ColumnName, "System.Int32", true, true);

            DataView myDataView = data.Tables[0].DefaultView;
            myDataView.Sort = "Lot_Key, prod";

            foreach (System.Data.DataRowView row in myDataView)
            {
                if (row.Row.ItemArray.GetValue(0).ToString() != lastKey)
                {
                    InvertDataSetKeyChanged(ret, row.Row);
                    lastKey = row.Row.ItemArray.GetValue(0).ToString();
                }
                else
                    InvertDataSetKeyNoChanged(ret, row.Row);
            }
            return ret;
        }
        #region InvertDataSet Helpers
        //****************************** Start InvertDatSet helpers *********************//
        private static void InvertDataSetKeyNoChanged(DataSet data, DataRow row)
        {
            int dataColumn = FindorAddColumninDataSet(data, row.ItemArray.GetValue(1).ToString(), "System.Double");
            DataRow dataRow = data.Tables[0].Rows[data.Tables[0].Rows.Count - 1];

            double dataValue = (double)DataColumnIsnull(dataRow.ItemArray.GetValue(dataColumn), 0.0D);

            dataRow[dataColumn] = dataValue + Double.Parse(row.ItemArray.GetValue(2).ToString());
        }
        public static object DataColumnIsnull(object value, object replaceValue)
        {
            if (value == null || value == System.DBNull.Value)
                return replaceValue;
            else
                return value;
        }

        private static void InvertDataSetKeyChanged(DataSet data, DataRow row)
        {
            int dataColumn = FindorAddColumninDataSet(data, row.ItemArray.GetValue(1).ToString(), "System.Double");
            DataRow newRow = data.Tables[0].NewRow();

            newRow[0] = row.ItemArray.GetValue(0).ToString();
            newRow[dataColumn] = row.ItemArray.GetValue(2).ToString();

            data.Tables[0].Rows.Add(newRow);
        }

        public static int FindorAddColumninDataSet(DataSet data, string fieldName, string type)
        {
            int findValue = FindFieldinDataSet(data, fieldName);
            if (findValue != -1)
                return findValue;
            else
                return AddColumninDataSet(data, fieldName, type);
        }
        //Returns the coloumn index or -1
        public static int FindFieldinDataSet(DataSet data, string fieldName)
        {
            for (int x = 0; x < data.Tables[0].Columns.Count; x++)
                if (data.Tables[0].Columns[x].ColumnName.ToLower() == fieldName.ToLower())
                    return x;
            return -1;
        }
        public static int AddColumninDataSet(DataSet data, string fieldName, string type)
        {
            return AddColumninDataSet(data, fieldName, type, false, false);
        }
        public static int AddColumninDataSet(DataSet data, string fieldName, string type, bool unique, bool readOnly)
        {
            DataColumn dataColumn = new DataColumn();
            dataColumn.ColumnName = fieldName;
            dataColumn.DataType = System.Type.GetType(type);
            dataColumn.ReadOnly = readOnly;
            dataColumn.Unique = unique;

            data.Tables[0].Columns.Add(dataColumn);

            return dataColumn.Ordinal;
        }
        //************************************* End InvertDatSet helpers *********************//
        #endregion
        #endregion

        #region Covert functions
        public static string ConvertToCelsius(string string1)
        {
            int CTemp;
            int CTempCalc;
            if (string1 != "NULL" && string1 != "")
            {
                CTempCalc = int.Parse(string1);
                CTemp = (((CTempCalc - 32) * 5) / 9);
                return CTemp.ToString();
            }
            else
                return string1;
        }
        public static string ConvertToLB(string string1)
        {
            double NetLB;
            double NetLBCalc;
            if (string1 != "NULL" && string1 != "")
            {
                NetLBCalc = double.Parse(string1);
                NetLB = (NetLBCalc * 2.2);
                return NetLB.ToString();
            }
            else
                return string1;
        }
        #endregion

        #region Parse Functions
        public static string ParseTildaString(string stringToParse, int field)
        {
            string[] stringSplit = stringToParse.Split('~');

            if (stringSplit.Length <= field)
                return "";
            else
                return stringSplit[field];
        }
        #endregion

        public static string StringTest(string string1, string string2)
        {
            string returnString;
            if (string1 == "")
            {
                if (string2 == "")
                    returnString = "0";
                else
                    returnString = string2;
            }
            else
                returnString = string1;
            return returnString;
        }
        public static double GetDouble(object value)
        {
            if (value == null || value.ToString() == "")
                return 0;
            else
                return double.Parse(value.ToString());

        }
        public static string GetValueFromDataGridItem(DataGridItem data, int index)
        {
            for (int x = 0; x < data.Cells[index].Controls.Count; x++)
            {
                if (data.Cells[index].Controls[x].ToString() == "System.Web.UI.WebControls.TextBox")
                    return ((TextBox)data.Cells[index].Controls[x]).Text;
                if (data.Cells[index].Controls[x].ToString() == "System.Web.UI.WebControls.Label")
                    return ((Label)data.Cells[index].Controls[x]).Text;
                if (data.Cells[index].Controls[x].ToString() == "System.Web.UI.WebControls.DropDownList")
                    return ((DropDownList)data.Cells[index].Controls[x]).SelectedItem.Value;
                if (data.Cells[index].Controls[x].ToString() == "System.Web.UI.HtmlControls.HtmlTextArea")
                    return ((HtmlTextArea)data.Cells[index].Controls[x]).Value.Replace("\n", "").Replace("\r", "");
                if (data.Cells[index].Controls[x].ToString() == "System.Web.UI.LiteralControl")
                    if (((LiteralControl)data.Cells[index].Controls[x]).ID != null)
                        return ((LiteralControl)data.Cells[index].Controls[x]).Text;
            }

            return "";
        }

        #region DataBase Functions

        public static ArrayList BuildArrayListFromDataSet(DataSet data, int field)
        {
            ArrayList ret = new ArrayList();
            if (data.Tables.Count == 0)
                return ret;

            for (int x = 0; x < data.Tables[0].Rows.Count; x++)
            {
                if (data.Tables[0].Rows[x].ItemArray.GetValue(field).ToString().Trim() != "")
                    ret.Add(new CriteriaBoxRecord(data.Tables[0].Rows[x].ItemArray.GetValue(field).ToString(), "", ""));
            }
            return ret;
        }
        public static ArrayList BuildSortedArrayListFromDataSet(DataSet data, int field)
        {
            ArrayList ret = new ArrayList();
            if (data.Tables.Count == 0)
                return ret;

            for (int x = 0; x < data.Tables[0].Rows.Count; x++)
            {
                string tempValue = data.Tables[0].Rows[x].ItemArray.GetValue(field).ToString().Trim();
                if (tempValue != "")
                {
                    int tempIndex = ret.BinarySearch(tempValue);
                    if (tempIndex < 0)
                        ret.Insert(Math.Abs(tempIndex) - 1, tempValue);
                    //ret.Add( new CriteriaBoxRecord(data.Tables[0].Rows[x].ItemArray.GetValue(field).ToString(),"",""));
                }
            }
            return ret;
        }

        public static ArrayList BuildStringArrayListFromDataSet(DataSet data, int field)
        {
            ArrayList ret = new ArrayList();
            if (data.Tables.Count == 0)
                return ret;

            for (int x = 0; x < data.Tables[0].Rows.Count; x++)
            {
                if (data.Tables[0].Rows[x].ItemArray.GetValue(field).ToString().Trim() != "")
                    ret.Add(data.Tables[0].Rows[x].ItemArray.GetValue(field).ToString());
            }
            return ret;
        }
        public static void RoundField(DataSet data, string field)
        {
            for (int x = 1; x < data.Tables[0].Rows.Count; x++)
                data.Tables[0].Rows[x][field] = decimal.Floor((decimal)data.Tables[0].Rows[x][field] + .5M);
        }
        public static ArrayList BuildUniqueArrayList(ArrayList values)
        {
            SortedList tempList = new SortedList();

            for (int x = 0; x < values.Count; x++)
            {
                if (tempList.ContainsKey(((CriteriaBoxRecord)values[x]).key))
                {
                    values.RemoveAt(x);
                    x--;
                }
                else
                    tempList.Add(((CriteriaBoxRecord)values[x]).key, null);
            }

            return values;
        }


        public static object DBNullIfEmpty(string str)
        {
            if (str == null || str.Trim() == "" || str == "1/1/1900")
                return System.DBNull.Value;
            else
                return str;
        }
        public static object DBNullIfEmptyString(string str)
        {
            if (str == null)
                return null;
            if (str.Trim() == "" || str == "1/1/1900")
                return System.DBNull.Value;
            else
                return str;
        }
        #endregion

        #region Week and Date Functions
        public static string AddDays(string date, int days)
        {
            if (date == "")
                return "";

            date = DateTime.Parse(date).AddDays(days).ToShortDateString();

            return date;
        }
        public static string FormatDate(string Date)
        {
            if (Date != null && Date != "")
            {
                string day;
                string month;
                day = DateTime.Parse(Date).Day.ToString();
                month = DateTime.Parse(Date).Month.ToString();
                switch (month)
                {
                    case "1":
                        return day + "-Jan";

                    case "2":
                        return day + "-Feb";

                    case "3":
                        return day + "-Mar";

                    case "4":
                        return day + "-Apr";

                    case "5":
                        return day + "-May";

                    case "6":
                        return day + "-Jun";

                    case "7":
                        return day + "-Jul";

                    case "8":
                        return day + "-Aug";

                    case "9":
                        return day + "-Sep";

                    case "10":
                        return day + "-Oct";

                    case "11":
                        return day + "-Nov";

                    case "12":
                        return day + "-Dec";

                }
            }
            return "";
        }
        /*
         * Notes: StartDate always on Monday, even if it's last year
         *		EndDate is always on Sunday, even if it's in next year.
         */
        public static DateTime WeekToStartDate(int week, int year)
        {
            DateTime date = new DateTime(year, 1, 1);

            date = date.AddDays(FirstMondayOfTheYear(date));
            date = date.AddDays((week - 1) * 7);

            return date;
        }
        public static DateTime WeekToEndDate(int week, int year)
        {
            return WeekToStartDate(week, year).AddDays(6);
        }
        public static string DateToWeek(DateTime date)
        {
            int firstMonday = FirstMondayOfTheYear(date);
            int dateOffset = ((date.DayOfYear - firstMonday) - 1);

            if (date.DayOfYear - firstMonday <= 0)//if the date is before the first monday of the year run again for the last day of last year.
                return DateToWeek(date.AddDays(-(date.DayOfYear)));
            else if (date.DayOfWeek != 0 && (int)date.DayOfWeek < 4)//if this isn't a sunday and it's after thursday run again for the next sunday
                return DateToWeek(date.AddDays((6 - (int)date.DayOfWeek)));
            else
                return ((dateOffset / 7) + 1).ToString();
        }

        public static string DateToWeekYear(DateTime date)
        {
            if (date.DayOfYear > 300 && DateToWeek(date) == "1")
                return (date.Year + 1).ToString();
            if (date.DayOfYear < 14 && Int32.Parse(DateToWeek(date)) > 4)
                return (date.Year - 1).ToString();

            return date.Year.ToString();

            /*
                        int firstMonday = FirstMondayOfTheYear(date);
                        int dateOffset = ((date.DayOfYear-firstMonday)-1);
			 
                        if(date.DayOfYear-firstMonday<=0)//if the date is before the first monday of the year run again for the last day of last year.
                            return (date.Year+1).ToString();
                        else if(date.DayOfWeek !=0 && (int)date.DayOfWeek < 4)//if this isn't a sunday and it's after thursday run again for the next sunday
                            return DateToWeekYear(date.AddDays((6-(int)date.DayOfWeek)));
                        else
                            return (date.Year).ToString();
                            */
        }

        public static string DateToWeek(string date)
        {
            if (date.Trim() == "")
                return "";
            else
                return DateToWeek(DateTime.Parse(FormatDateTime(date)));
        }
        public static int FirstMondayOfTheYear(DateTime date)
        {
            DateTime jan1 = date.AddDays(-(date.DayOfYear - 1));

            if (jan1.DayOfWeek == 0)
                return 1;
            else if (((int)jan1.DayOfWeek) < 5)
                return -(((int)jan1.DayOfWeek) - 1);
            else
                return 8 - ((int)jan1.DayOfWeek);
        }
        public static string GetMondayOfThisWeek(DateTime date)
        {
            if (date.DayOfWeek == 0)
                return FormatDateTime(date.AddDays(-6).ToShortDateString());
            else if ((int)date.DayOfWeek == 1)
                return FormatDateTime(date.ToShortDateString());
            else
                return FormatDateTime(date.AddDays(-((int)date.DayOfWeek - 1)).ToShortDateString());
        }
        public static string GetSundayOfThisWeek(DateTime date)
        {
            if (date.DayOfWeek == 0)
                return FormatDateTime(date.ToShortDateString());
            else
                return FormatDateTime(date.AddDays(7 - (int)date.DayOfWeek).ToShortDateString());
        }
        public static string TestDateToWeek()
        {
            string strRet = "";

            strRet += "mon- " + ((int)DateTime.Parse("1/5/04").DayOfWeek).ToString() + "<BR>";
            strRet += "tue- " + ((int)DateTime.Parse("1/6/04").DayOfWeek).ToString() + "<BR>";
            strRet += "we- " + ((int)DateTime.Parse("1/7/04").DayOfWeek).ToString() + "<BR>";
            strRet += "th- " + ((int)DateTime.Parse("1/8/04").DayOfWeek).ToString() + "<BR>";
            strRet += "fr- " + ((int)DateTime.Parse("1/9/04").DayOfWeek).ToString() + "<BR>";
            strRet += "sat- " + ((int)DateTime.Parse("1/10/04").DayOfWeek).ToString() + "<BR>";
            strRet += "sun- " + ((int)DateTime.Parse("1/11/04").DayOfWeek).ToString() + "<BR>";
            strRet += "1:" + DateToWeek(DateTime.Parse("12/29/2003")) + "Mon 12/29/2003" + "<BR>";
            strRet += "1:" + DateToWeek(DateTime.Parse("1/1/2004")) + "Thu 1/1/2004" + "<BR>";
            strRet += "2:" + DateToWeek(DateTime.Parse("1/5/04")) + "Mon 1/5/04" + "<BR>";
            strRet += "53:" + DateToWeek(DateTime.Parse("1/1/05")) + "Thu 1/1/05" + "<BR>";
            strRet += "52:" + DateToWeek(DateTime.Parse("1/1/06")) + "Sun 1/1/06" + "<BR>";
            strRet += "1:" + DateToWeek(DateTime.Parse("1/2/06")) + "Mon 1/2/06" + "<BR>";
            strRet += "1:" + DateToWeek(DateTime.Parse("1/8/06")) + "Sun 1/8/06" + "<BR>";
            strRet += "2:" + DateToWeek(DateTime.Parse("1/9/06")) + "Mon 1/9/06" + "<BR>";

            return strRet;
        }
        public static string TestWeekToStartDate()
        {
            string strRet = "";

            strRet += "Week 1 2001 =" + WeekToStartDate(1, 2001).ToShortDateString() + "<BR>";
            strRet += "Week 2 2001 =" + WeekToStartDate(2, 2001).ToShortDateString() + "<BR>";
            strRet += "Week 3 2001 =" + WeekToStartDate(3, 2001).ToShortDateString() + "<BR>";

            strRet += "Week 1 2002 =" + WeekToStartDate(1, 2002).ToShortDateString() + "<BR>";
            strRet += "Week 2 2002 =" + WeekToStartDate(2, 2002).ToShortDateString() + "<BR>";
            strRet += "Week 3 2002 =" + WeekToStartDate(3, 2002).ToShortDateString() + "<BR>";

            strRet += "Week 1 2003 =" + WeekToStartDate(1, 2003).ToShortDateString() + "<BR>";
            strRet += "Week 2 2003 =" + WeekToStartDate(2, 2003).ToShortDateString() + "<BR>";
            strRet += "Week 3 2003 =" + WeekToStartDate(3, 2003).ToShortDateString() + "<BR>";

            strRet += "Week 1 2004 =" + WeekToStartDate(1, 2004).ToShortDateString() + "<BR>";
            strRet += "Week 2 2004 =" + WeekToStartDate(2, 2004).ToShortDateString() + "<BR>";
            strRet += "Week 3 2004 =" + WeekToStartDate(3, 2004).ToShortDateString() + "<BR>";

            strRet += "Week 1 2005 =" + WeekToStartDate(1, 2005).ToShortDateString() + "<BR>";
            strRet += "Week 2 2005 =" + WeekToStartDate(2, 2005).ToShortDateString() + "<BR>";
            strRet += "Week 3 2005 =" + WeekToStartDate(3, 2005).ToShortDateString() + "<BR>";

            strRet += "Week 1 2006 =" + WeekToStartDate(1, 2006).ToShortDateString() + "<BR>";
            strRet += "Week 2 2006 =" + WeekToStartDate(2, 2006).ToShortDateString() + "<BR>";
            strRet += "Week 3 2006 =" + WeekToStartDate(3, 2006).ToShortDateString() + "<BR>";

            strRet += "Week 1 2007 =" + WeekToStartDate(1, 2007).ToShortDateString() + "<BR>";
            strRet += "Week 2 2007 =" + WeekToStartDate(2, 2007).ToShortDateString() + "<BR>";
            strRet += "Week 3 2007 =" + WeekToStartDate(3, 2007).ToShortDateString() + "<BR>";

            strRet += "Week 1 2008 =" + WeekToStartDate(1, 2008).ToShortDateString() + "<BR>";
            strRet += "Week 2 2008 =" + WeekToStartDate(2, 2008).ToShortDateString() + "<BR>";
            strRet += "Week 3 2008 =" + WeekToStartDate(3, 2008).ToShortDateString() + "<BR>";

            return strRet;
        }

        #region GetDateBasedWeek & WeekBase Funcation
        /// <summary>
        /// Struct for WeekBase Data
        /// </summary>
        public struct WeekBaseData
        {
            public DateTime FirstMonday;
            public int FirstWeekNumber;
        }

        /// <summary>
        /// Uses a WeekBaseData to get the week the day is in (this lines up weeks acrost years)
        /// </summary>
        /// <param name="date">Date to find the week of</param>
        /// <param name="baseDate">BaseDate </param>
        /// <param name="LeapYearAtEndOfSession">whether to put the leap day at the end of the session or in the week it happens</param>
        /// <returns></returns>
        public static int GetDateBasedWeek(DateTime date, WeekBaseData baseDate, bool LeapYearAtEndOfSession)
        {
            int dayDiff;
            int weekDiff;
            int retWeek;

            //Same crop year
            if ((date.Year == baseDate.FirstMonday.Year && date.DayOfYear >= baseDate.FirstMonday.DayOfYear) ||
                (date.Year == (baseDate.FirstMonday.Year + 1) && date.DayOfYear < baseDate.FirstMonday.DayOfYear))
            {

                dayDiff = (date - baseDate.FirstMonday).Days;

                if (dayDiff > 363)
                    dayDiff = 363;

                weekDiff = (int)Math.Floor(dayDiff / 7.0d);

                retWeek = baseDate.FirstWeekNumber + weekDiff;

                if (retWeek > 52)
                    retWeek -= 52;

                return retWeek;

                //return Int32.Parse(Misc.DateToWeek(date));

            }
            else
            {
                //Set the year
                DateTime tempDate = date.AddYears(baseDate.FirstMonday.Year - date.Year);

                //If the dayof the year is before the monday add one year
                if (tempDate.DayOfYear < baseDate.FirstMonday.DayOfYear)
                    tempDate = tempDate.AddYears(1);

                //Find the diffrence in days from our base date
                dayDiff = (tempDate - baseDate.FirstMonday).Days;


                //Addjust for Leap year (if leap year and after feb 28 add a day)
                if (LeapYearAtEndOfSession && date.Year % 4 == 0 && date.DayOfYear > 59 && date.DayOfYear < baseDate.FirstMonday.DayOfYear)
                    dayDiff++;

                //Make sure all the extra days stay in the last week.
                if (dayDiff > 363)
                    dayDiff = 363;

                weekDiff = (int)Math.Floor(dayDiff / 7.0d);

                retWeek = baseDate.FirstWeekNumber + weekDiff;

                if (retWeek > 52)
                    retWeek -= 52;

                return retWeek;
            }
        }

        /// <summary>
        /// Get the year of the date based in the WeekBaseData
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string GetDateBasedWeekYear(DateTime date, WeekBaseData baseDate)
        {
            if (date.DayOfYear > 300 && GetDateBasedWeek(date, baseDate, false) == 1)
                return (date.Year + 1).ToString();
            if (date.DayOfYear < 14 && GetDateBasedWeek(date, baseDate, false) > 4)
                return (date.Year - 1).ToString();

            return date.Year.ToString();
        }
        /// <summary>
        /// Builds the BaseWeekData Strutct to base all other GetWeek funcations off of
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static WeekBaseData GetWeekBase(DateTime date)
        {
            WeekBaseData weekData;

            weekData.FirstMonday = GetStartMonday(date);
            weekData.FirstWeekNumber = Int32.Parse(Misc.DateToWeek(weekData.FirstMonday));

            return weekData;
        }

        /// <summary>
        /// Uses the Thursday rule to get what should be the start monday for the given date
        /// </summary>
        /// <param name="date"></param>
        /// <returns>The Start Monday</returns>
        public static DateTime GetStartMonday(DateTime date)
        {
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    return date;
                case DayOfWeek.Tuesday:
                    return date.AddDays(-1);
                case DayOfWeek.Wednesday:
                    return date.AddDays(-2);
                case DayOfWeek.Thursday:
                    return date.AddDays(-3);
                case DayOfWeek.Friday:
                    return date.AddDays(3);
                case DayOfWeek.Saturday:
                    return date.AddDays(2);
                case DayOfWeek.Sunday:
                    return date.AddDays(1);
                default:
                    return date;
            }
        }
        public static DateTime GetDateBasedWeekStartDate(int week, WeekBaseData baseDate, bool LeapYearAtEndOfSession)
        {
            int daysToAdd;
            DateTime tempDate = baseDate.FirstMonday;
            DateTime prevDate;

            if (week >= baseDate.FirstWeekNumber)
                daysToAdd = (week - baseDate.FirstWeekNumber) * 7;
            else
                daysToAdd = ((week + 52) - baseDate.FirstWeekNumber) * 7;

            tempDate = tempDate.AddDays(daysToAdd);
            prevDate = tempDate.AddDays(-1);
            while (GetDateBasedWeek(tempDate, baseDate, LeapYearAtEndOfSession) == GetDateBasedWeek(prevDate, baseDate, LeapYearAtEndOfSession))
            {
                tempDate = prevDate;
                prevDate = prevDate.AddDays(-1);
            }

            return tempDate;
        }
        public static DateTime GetDateBasedWeekEndDate(int week, WeekBaseData baseDate, bool LeapYearAtEndOfSession)
        {
            int daysToAdd;
            DateTime tempDate = baseDate.FirstMonday;
            DateTime nextDate;

            if (week >= baseDate.FirstWeekNumber)
                daysToAdd = (week - baseDate.FirstWeekNumber) * 7;
            else
                daysToAdd = ((week + 52) - baseDate.FirstWeekNumber) * 7;

            tempDate = tempDate.AddDays(daysToAdd + 5);
            nextDate = tempDate.AddDays(1);
            while (GetDateBasedWeek(tempDate, baseDate, LeapYearAtEndOfSession) == GetDateBasedWeek(nextDate, baseDate, LeapYearAtEndOfSession))
            {
                tempDate = nextDate;
                nextDate = nextDate.AddDays(1);
            }

            return tempDate;
        }
        #endregion

        /// <summary>
        /// Gets the years the input date belongs in based on yearStart
        /// </summary>
        /// <param name="input">Date to find years for</param>
        /// <param name="yearStart">what month/date does the year start on (format 04/01)</param>
        /// <returns>the years (format 05-06)</returns>
        public static string GetYears(DateTime input, string yearStart)
        {
            DateTime start = DateTime.Parse(yearStart + "/" + input.Year);
            int firstYear, secondYear;

            //Figure out the first year
            if (input >= start)//date is after start it's [year]-[year+1]
                firstYear = start.Year;
            else
                firstYear = start.Year - 1;

            secondYear = firstYear + 1;

            //Change year to 2 digit
            firstYear -= (firstYear < 2000 ? 1900 : 2000);
            secondYear -= (secondYear < 2000 ? 1900 : 2000);

            return String.Format("{0:00}-{1:00}", firstYear, secondYear);
        }

        public static string GetYears(string input, string yearStart)
        {
            return GetYears(DateTime.Parse(input), yearStart);
        }
        #endregion

        public static void TraceMessage(string category, string message)
        {
            System.Web.HttpContext.Current.Trace.Write(category, message);
        }
    }
    /// <summary>
    /// Weeks are from Saturday to Friday. 
    /// First week of the year is the week that has Feburary 1 in it.
    /// </summary>
    public class WalmartWeek
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="week"></param>
        /// <param name="year"></param>
        /// <returns>Saturday of the year/week</returns>
        public static DateTime WeekToStartDate(int week, int year)
        {
            DateTime date = GetFirstDayOfTheYear(new DateTime(year, 2, 1));

            date = date.AddDays((week - 1) * 7);

            return date;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="week"></param>
        /// <param name="year"></param>
        /// <returns>Friday of the year/week</returns>
        public static DateTime WeekToEndDate(int week, int year)
        {
            return WeekToStartDate(week, year).AddDays(6);
        }

        public static int DateToWeek(DateTime date)
        {
            DateTime firstDay = GetFirstDayOfTheYear(date);

            //Calc the days diff
            int daysDiff = ((TimeSpan)(date - firstDay)).Days;

            //div by 7, the remander will be ignored, add one becase the first week is 1 not 0
            return (daysDiff / 7) + 1;
        }
        public static string DateToWeek(string date)
        {
            if (date.Trim() == "")
                return "";
            else
                return DateToWeek(DateTime.Parse(Misc.FormatDateTime(date))).ToString();
        }
        public static int DateToWeekYear(DateTime date)
        {
            return GetFirstDayOfTheYear(date).Year;
        }

        private static DateTime GetFirstDayOfTheYear(DateTime date)
        {
            //Get feb First of the specified year
            DateTime febFirst = new DateTime(date.Year, 2, 1);
            DateTime firstDay;

            //Move it back to the Sat that incluces Feb1
            if (febFirst.DayOfWeek != DayOfWeek.Saturday)
                firstDay = febFirst.AddDays(((int)(febFirst.DayOfWeek + 1)) * -1);
            else
                firstDay = febFirst;

            //if before the first day of the year, it's part of the year before
            if (date < firstDay)
            {
                febFirst = febFirst.AddYears(-1);

                if (febFirst.DayOfWeek != DayOfWeek.Saturday)
                    firstDay = febFirst.AddDays(((int)(febFirst.DayOfWeek + 1)) * -1);
                else
                    firstDay = febFirst;
            }

            return firstDay;
        }

    }
}
