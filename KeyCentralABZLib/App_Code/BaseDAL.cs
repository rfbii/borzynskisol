using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
//using NUnit.Framework;

namespace KeyCentral.Functions
{
    public class BaseDAL
    {
        protected string famousCompanyKey;		//SQL Company Key (ex:5 or 58)
        protected string famousReportNumber;	//Famous Report Number(ex: company_3 or Company_4)
        protected Page page;

        protected BaseDAL(Page page)
        {
            famousReportNumber = SessionWrapper.Report_Number;
            famousCompanyKey = SessionWrapper.Company_Key;
            this.page = page;
        }


        public static object ExecuteScalar(string sqlCommand)
        {
            object ret;
            SqlConnection conn = Misc.GetSQLConn("ExecuteScalar");
            SqlCommand cmd = new SqlCommand(sqlCommand, conn);

            ret = cmd.ExecuteScalar();
            conn.Close();
            conn.Dispose();

            return ret;
        }


        //public static void compareDataSets(DataSet baseLineDts, DataSet newDts)
        //{
        //    compareDataSets(baseLineDts, newDts, false);
        //}
        //public static void compareDataSets(DataSet baseLineDts, DataSet newDts, bool ignoreColumnCountBol)
        //{
        //    Assert.AreEqual(baseLineDts.Tables.Count, newDts.Tables.Count);

        //    for (int x = 0; x < baseLineDts.Tables.Count; x++)
        //    {
        //        #region Compare Columns
        //        if (!ignoreColumnCountBol)
        //            Assert.AreEqual(baseLineDts.Tables[x].Columns.Count, newDts.Tables[x].Columns.Count, "Compare Columns count");

        //        for (int y = 0; y < baseLineDts.Tables[x].Columns.Count; y++)
        //        {
        //            Assert.AreEqual(baseLineDts.Tables[x].Columns[y].ColumnName, newDts.Tables[x].Columns[y].ColumnName, "Compare Columns column name");
        //            Assert.AreEqual(baseLineDts.Tables[x].Columns[y].DataType, newDts.Tables[x].Columns[y].DataType, "Compare Columns data type");
        //        }
        //        #endregion

        //        #region Compare Rows
        //        Assert.AreEqual(baseLineDts.Tables[x].Rows.Count, newDts.Tables[x].Rows.Count, "Compare Rows count");

        //        for (int y = 0; y < baseLineDts.Tables[x].Rows.Count; y++)
        //        {
        //            for (int z = 0; z < baseLineDts.Tables[x].Rows[y].ItemArray.Length; z++)
        //                Assert.AreEqual(baseLineDts.Tables[x].Rows[y].ItemArray[z], newDts.Tables[x].Rows[y].ItemArray[z], "Compare Rows item array (row values)");
        //        }
        //        #endregion
        //    }

        //}
        //    <summary>
        ///    Converts a SqlDataReader to a DataSet
        ///    <param name='reader'>
        /// SqlDataReader to convert.</param>
        ///    <returns>
        /// DataSet filled with the contents of the reader.</returns>
        ///    </summary>
        public static DataSet convertDataReaderToDataSet(IDataReader reader)
        {
            DataSet dataSet = new DataSet();
            do
            {
                // Create new data table

                DataTable schemaTable = reader.GetSchemaTable();
                DataTable dataTable = new DataTable();

                if (schemaTable != null)
                {
                    // A query returning records was executed

                    for (int i = 0; i < schemaTable.Rows.Count; i++)
                    {
                        DataRow dataRow = schemaTable.Rows[i];
                        // Create a column name that is unique in the data table
                        string columnName = (string)dataRow["ColumnName"]; //+ "<C" + i + "/>";
                        // Add the column definition to the data table
                        DataColumn column = new DataColumn(columnName, (Type)dataRow["DataType"]);
                        dataTable.Columns.Add(column);
                    }

                    dataSet.Tables.Add(dataTable);

                    // Fill the data table we just created

                    while (reader.Read())
                    {
                        DataRow dataRow = dataTable.NewRow();

                        for (int i = 0; i < reader.FieldCount; i++)
                            dataRow[i] = reader.GetValue(i);

                        dataTable.Rows.Add(dataRow);
                    }
                }
                else
                {
                    // No records were returned

                    DataColumn column = new DataColumn("RowsAffected");
                    dataTable.Columns.Add(column);
                    dataSet.Tables.Add(dataTable);
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = reader.RecordsAffected;
                    dataTable.Rows.Add(dataRow);
                }
            }
            while (reader.NextResult());
            return dataSet;
        }
    }
}