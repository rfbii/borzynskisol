using System;
using System.Collections;
using System.Data;
using System.Web.UI;

namespace KeyCentral.Functions
{
	/// <summary>
	/// Summary description for BaseBLL.
	/// </summary>
	public abstract class BaseBLL
	{
		protected string famousCompanyKey;		//SQL Company Key (ex:5 or 58)
		protected string famousReportNumber;	//Famous Report Number(ex: company_3 or Company_4)
        public static string testDir = @"C:\Temp\Packout\";
		protected Page page;

		protected BaseBLL(Page page)
        {
            famousReportNumber = SessionWrapper.Report_Number;
            famousCompanyKey = SessionWrapper.Company_Key;
            this.page = page;
        }

        protected static void RoundField(DataSet data, string field)
        {
			for (int x = 1; x < data.Tables[0].Rows.Count; x++)
				data.Tables[0].Rows[x][field] = decimal.Floor((decimal) data.Tables[0].Rows[x][field] + .5M);
		}
	}
	public interface IBaseBLL
	{
		void ProcessData(IDataReader reader);

	}
}
