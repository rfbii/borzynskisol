namespace KeyCentral.Functions
{
	using System.Reflection;
	using System.Collections;
	using System;
	using System.Data.SqlClient;
	using System.Web;
	using System.Web.SessionState;
    using System.IO;


	[AttributeUsage(AttributeTargets.Class |
		 AttributeTargets.Method |
		 AttributeTargets.Property |
		AttributeTargets.Assembly,
		 AllowMultiple = true)]
	public class MenuMapAttribute : System.Attribute
	{
		#region Declares
		private string security;
		private string description;
		private string menu;
		private string url;
		private string javaCode;
		private string type;
		private int sortOrder;
		private bool displayIfEmpty;
		#endregion

		#region Properties
		public string Security{get{return security;}set{security = value;}}
		public string Description{get{return description;}set{description = value;}}
		public string Menu{get{return menu;}set{menu = value;}}
		public string Url{get{return url;}set{url = value;}}
		public string JavaCode{get{return javaCode;}set{javaCode = value;}}
		public string Type{get{return type;}set{type = value;}}
		public int SortOrder{get{return sortOrder;}set{sortOrder = value ;}}
		public bool DisplayIfEmpty{get{return displayIfEmpty;}set{displayIfEmpty = value ;}}

		#endregion

		#region Constructor
		public MenuMapAttribute()
		{
			security = "ADMIN ONLY";
			description = "";
			menu = "";
			sortOrder = 999;
			DisplayIfEmpty = true;
			type="";
		}
		
		public MenuMapAttribute(string security,string description,string menu,string url,string javaCode,int menuOrder,bool displayIfEmpty,string type)
		{
			this.security = security;
			this.description = description;
			this.Menu = menu;
			this.url = url;
			this.javaCode = javaCode;
			this.sortOrder = menuOrder;
			this.displayIfEmpty = displayIfEmpty;
			this.type = type;
		}
		#endregion
	}
	public class MenuStruct
	{
		#region Declares
		private ArrayList subMenus;
		private string security;
		private string description;
		private string url;
		private string javaCode;
		private int menuOrder;
		private bool displayIfEmpty;
		private string type;
		#endregion
		
		#region Properties
		public ArrayList SubMenus{get{return subMenus;}set{subMenus = value;}}
		public string Security{get{return security;}set{security = value;}}
		public string Description{get{return description;}set{description = value;}}
		public string Url{get{return url;}set{url = value;}}
		public string JavaCode{get{return javaCode;}set{javaCode = value;}}
		public int MenuOrder{get{return menuOrder;}set{menuOrder = value;}}

		public bool DisplayIfEmpty{get{return displayIfEmpty;}set{displayIfEmpty = value ;}}

		public string Type{get{return type;}set{type = value;}}

		#endregion

		#region Constructor
		public MenuStruct()
		{
			subMenus = new ArrayList();
		}
		public MenuStruct(string security,string description,string url,string javaCode,int menuOrder,bool displayIfEmpty,string type)
		{
			subMenus = new ArrayList();
			this.security = security;
			this.description = description;
			this.url = url;
			this.javaCode = javaCode;
			this.menuOrder = menuOrder;
			this.displayIfEmpty = displayIfEmpty;
			this.type = type;
		}

		#endregion

		#region Public Functions
        public MenuStruct DeepCopy(string userKey)
        {
            MenuStruct ret = new MenuStruct();

            if (description == "Archive")
            {
                string javaCodeStr = javaCode.Remove(javaCode.IndexOf("UserKey=")) + "UserKey=" + userKey + javaCode.Substring(javaCode.IndexOf("','ArchiveWindow'"));

                ret = new MenuStruct(security, description, url, javaCodeStr, menuOrder, displayIfEmpty, type);
            }
            else
            {
                ret = new MenuStruct(security, description, url, javaCode, menuOrder, displayIfEmpty, type);
            }

            //MenuStruct ret = new MenuStruct(security,description,url,javaCode,menuOrder,displayIfEmpty,type);

            foreach (MenuStruct menu in subMenus)
                ret.SubMenus.Add(menu.DeepCopy(userKey));

            return ret;
        }

        public static ArrayList DeepCopyMenuList(ArrayList menus, string userKey)
        {
            ArrayList ret = new ArrayList();

            foreach (MenuStruct menu in menus)
                ret.Add(menu.DeepCopy(userKey));

            return ret;
            /*
            //Shallow Copy this level
            ArrayList ret = (ArrayList)menus.Clone();// new ArrayList(menus);

            //Deep copy the rest of the levels
            for(int i=0;i<ret.Count;i++)
                ((MenuStruct)ret[i]).SubMenus = DeepCopyMenuList(((MenuStruct)menus[i]).SubMenus);
				
            return ret;
            */
        }
        #region Original Code lines 113-154
        //public MenuStruct DeepCopy()
        //{
        //    MenuStruct ret = new MenuStruct(security, description, url, javaCode, menuOrder, displayIfEmpty, type);

        //    foreach (MenuStruct menu in subMenus)
        //        ret.SubMenus.Add(menu.DeepCopy());

        //    return ret;
        //}

        //public static ArrayList DeepCopyMenuList(ArrayList menus)
        //{
        //    ArrayList ret = new ArrayList();

        //    foreach (MenuStruct menu in menus)
        //        ret.Add(menu.DeepCopy());

        //    return ret;
        //    /*
        //    //Shallow Copy this level
        //    ArrayList ret = (ArrayList)menus.Clone();// new ArrayList(menus);

        //    //Deep copy the rest of the levels
        //    for(int i=0;i<ret.Count;i++)
        //        ((MenuStruct)ret[i]).SubMenus = DeepCopyMenuList(((MenuStruct)menus[i]).SubMenus);
				
        //    return ret;
        //    */
        //} 
        #endregion
		#endregion
	}


    [MenuMap(Security = "", Description = "Menu", Menu = "", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", SortOrder = 1, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=")]
    [MenuMap(Security = "ADMINONLY", Description = "Setup", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "", SortOrder = 2, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Setup")]
    [MenuMap(Security = "ADMINONLY", Description = "Company", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Setup", SortOrder = 1, Url = "../../../KeyCentralABZ/UI/Inputs/InputCompany.aspx")]
    [MenuMap(Security = "ADMINONLY", Description = "Company Disclaimer", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Setup", SortOrder = 1, Url = "../../../KeyCentralABZ/UI/Inputs/InputCompanyDisclaimer.aspx")]
    [MenuMap(Security = "ADMINONLY", Description = "Remove Check Writing User", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Setup", SortOrder = 1, Url = "../../../KeyCentralABZ/UI/Inputs/inputRemoveCheckWritingUser.aspx")]
    [MenuMap(Security = "ADMINONLY", Description = "Security", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Setup", SortOrder = 2, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Setup/Security")]

    [MenuMap(Security = "ADMINONLY", Description = "Stats", JavaCode = "OpenStatsWindows();", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Setup", SortOrder = 2)]

    [MenuMap(Security = "ADMINONLY", Description = "User", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Setup/Security", SortOrder = 1, Url = "../../../KeyCentralABZ/UI/Inputs/InputUsers.aspx")]
    [MenuMap(Security = "ADMINONLY", Description = "Famous Security Type", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Setup/Security", SortOrder = 2, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Setup/Security/Famous Security Type")]
    [MenuMap(Security = "ADMINONLY", Description = "Grower", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Setup/Security/Famous Security Type", SortOrder = 1, Url = "../../../KeyCentralABZ/UI/Inputs/InputGrower.aspx")]
    [MenuMap(Security = "ADMINONLY", Description = "Warehouse", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Setup/Security/Famous Security Type", SortOrder = 2, Url = "../../../KeyCentralABZ/UI/Inputs/InputWarehouse.aspx")]
    [MenuMap(Security = "ADMINONLY", Description = "Security Type", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Setup/Security", SortOrder = 3, Url = "../../../KeyCentralABZ/UI/Inputs/InputSecurityType.aspx")]
    [MenuMap(Security = "ADMINONLY", Description = "Security Group", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Setup/Security", SortOrder = 4, Url = "../../../KeyCentralABZ/UI/Inputs/InputSecurityGroup.aspx")]
	[MenuMap(Security = "", Description = "Print", Type = "Report,Report_NoExcel,PrintExcel", Menu = "", SortOrder = 97, JavaCode = "BaseRFB_HideHeader(false);PrintThisPage();BaseRFB_HideHeader(true);")]
	//[MenuMap(Security="",Description="Email",Type="Report,Report_NoExcel",Menu="",SortOrder=98,JavaCode="EmailThisPage();")]
    //[MenuMap(Security = "", Description = "Excel", Type = "Report,PrintExcel", Menu = "", SortOrder = 99, JavaCode = "ExcelThisPage();")]
	[MenuMap(Security="",Description="Back",Type="Report,Report_NoExcel,Report_BackOnly",Menu="",SortOrder=100,Url="Misc.DecodeQueryString(Request.QueryString['LastPage'])")]
    [MenuMap(Security = "", Description = "Reports", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "", DisplayIfEmpty = false, SortOrder = 3, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Reports")]
    [MenuMap(Security = "", Description = "Mayan Data Central", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "", DisplayIfEmpty = false, SortOrder = 4, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Mayan Data Central")]
    [MenuMap(Security = "", Description = "SRP", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "", DisplayIfEmpty = false, SortOrder = 6, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/SRP")]
    [MenuMap(Security = "", Description = "Accounting", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "", DisplayIfEmpty = false, SortOrder = 7, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Accounting")]
    [MenuMap(Security = "ADMINONLY", Description = "Session Manager", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "/Setup/Security", SortOrder = 1, Url = "../../../KeyCentralABZ/UI/SessionManager/ReportSessionManager.aspx?ReportName=SessionManager")]
	[MenuMap(Security = "", Description = "Close", Type = "PrintExcel", Menu = "", SortOrder = 101, JavaCode = "window.close();")]
    
    public class MenuMap
	{
		#region Declares
		private System.Web.Caching.Cache Cache;
		private HttpSessionState Session;
		private HttpRequest Request;
		#endregion

		#region Properties
		#endregion

		#region Constructor
		public MenuMap(System.Web.Caching.Cache Cache,HttpSessionState Session,HttpRequest request)
		{
			this.Cache  = Cache;
			this.Session = Session;
			this.Request = request;
		}
		
		public MenuMap(System.Web.UI.Page page)
		{
			this.Cache  = page.Cache;
			this.Session = page.Session;
			this.Request = page.Request;
		}
		
		#endregion

		#region Public functions
		public ArrayList GetMenus(string menuType)
		{
			ArrayList menus = GetMenus();
			
			ApplySecurity(menus);
			ApplyType(menus,menuType);

			//Cleanup any menus that the sub items were removed
			DeleteNonVisableMenus(menus);

			ApplySpecailMenusSettings(menus);

			return menus;
		}

		public ArrayList GetMenus(string menuType,string menuLocation)
		{
			ArrayList menus = GetMenus(menuType);
			
			if(menuLocation == null || menuLocation == "")
				return menus;
 
			String[] menuStrings = menuLocation.ToLower().Split('/');
			int menuStringIndex = 1;

			while(menuStringIndex < menuStrings.Length)
			{
				bool found = false;
				foreach(MenuStruct menuStruct in menus)
					if(menuStruct.Description.ToLower() ==menuStrings[menuStringIndex])
					{
						menus = menuStruct.SubMenus;
						menuStringIndex++;
						found = true;
						break;
					}

				if(! found)
					throw new Exception("Menu for '" + menuLocation + "' can not be found.");
						
			}

			
			return menus;
		}

		#endregion

		#region Private Helper Functions
		private ArrayList GetMenus()
		{
			//Make sure the MenuMap is in the Cache
			while (System.Web.HttpContext.Current.Application["MenuMap"] == null)
			{
				ArrayList menu = new ArrayList();
				ArrayList flatMenu = GetFlatMenu();

				BuildMenuFromFlat("", flatMenu, menu);

				DeleteNonVisableMenus(menu);
				SortMenu(menu);

                System.Web.HttpContext.Current.Application["MenuMap"] = menu;
			}

			//Return the Cached MenuMap
			//return (ArrayList)((ArrayList)Cache["MenuMap"]).Clone();
            //return MenuStruct.DeepCopyMenuList((ArrayList)System.Web.HttpContext.Current.Application["MenuMap"]);
            return MenuStruct.DeepCopyMenuList((ArrayList)System.Web.HttpContext.Current.Application["MenuMap"], Session["User_Key"].ToString());
            //return new ArrayList((ArrayList)Cache["MenuMap"]);
			//return (ArrayList)Cache["MenuMap"];
		}

        /// <summary>
		/// Recursive function to remove anything that doesn't have the passed type
		/// </summary>
		/// <param name="menus"></param>
		/// <param name="type"></param>
		private void ApplyType(ArrayList menus,string type)
		{
			//If no type is passed don't filter anything!
			if(type == null || type == "")
				return;

			for(int i=menus.Count-1;i>=0;i--)
			{
				//Apply the typef or this menu
				if(((MenuStruct)menus[i]).Type.IndexOf(type) ==-1)
					menus.RemoveAt(i);
				else //Apply Security for the submenus
					ApplyType(((MenuStruct)menus[i]).SubMenus,type);
			}
		}
		
		private void ApplySecurity(ArrayList menus)
		{
			//If the user is an admin this doesn't need to be done.
			bool removed=false;

			for(int i=menus.Count-1;i>=0;i--)
			{
				removed= false;
				//Apply Security for this Menu
				if(((MenuStruct)menus[i]).Security != "")
				{
					bool permisionFound = false;
					string[] securityList = ((MenuStruct)menus[i]).Security.Split(',');

					for(int x=0;x<securityList.Length;x++)
						if(Security.CheckSecurity(Session,securityList[x]))
							permisionFound = true;

					if(permisionFound == false)
					{
						menus.RemoveAt(i);
						removed = true;
						//continue;
					}
				}
			

				//Apply Security for the submenus
				if(!removed)
					ApplySecurity(((MenuStruct)menus[i]).SubMenus);
			}
		}

		/// <summary>
		/// Recursive function to make sure all specail menus get thier settings
		/// </summary>
		/// <param name="menus"></param>
		private void ApplySpecailMenusSettings(ArrayList menus)
		{
			foreach(MenuStruct menuStruct in menus)
			{
				//Apply the settings for this menu
				ApplySpecailMenusSettings(menuStruct);

				//Do recursive funcation
				ApplySpecailMenusSettings(menuStruct.SubMenus);
			}
		}
		/// <summary>
		/// Apply Sepcail settings for some menus
		/// 'Back' = Misc.DecodeQueryString(Request.QueryString['LastPage'])
		/// </summary>
		/// <param name="menu"></param>
		private void ApplySpecailMenusSettings(MenuStruct menu)
		{
			if(menu.Description == "Back")
			{
				menu.Url = Functions.Misc.DecodeQueryString(Request.QueryString["LastPage"]);
			}
		}

		/// <summary>
		/// Recursive function to remove all no visuable menus
		/// </summary>
		/// <param name="menus"></param>
		private void DeleteNonVisableMenus(ArrayList menus)
		{
			for(int i=menus.Count-1;i>=0;i--)
			{
				//Do Recusive function first to clean out the submenus
				//if(((MenuStruct)menus[i]).SubMenus.Count ==0)
					DeleteNonVisableMenus(((MenuStruct)menus[i]).SubMenus);

				if(((MenuStruct)menus[i]).DisplayIfEmpty == false && ((MenuStruct)menus[i]).SubMenus.Count ==0)
					menus.RemoveAt(i);

			}
		}
		private void SortMenu(ArrayList menus)
		{
			//Sort this Menu
			for(int i=0;i<(menus.Count-1);i++)
			{
				if(((MenuStruct)menus[i]).MenuOrder > ((MenuStruct)menus[i+1]).MenuOrder)
				{
					menus.Reverse(i,2);
					i -=2;
				}
				else if(((MenuStruct)menus[i]).MenuOrder == ((MenuStruct)menus[i+1]).MenuOrder && ((MenuStruct)menus[i]).Description.CompareTo( ((MenuStruct)menus[i+1]).Description) > 0)
				{
					menus.Reverse(i,2);
					i -=2;
				}

				if(i<-1)
					i=-1;
			}

			//Sort the sub menus too
			foreach(MenuStruct menu in menus)
				SortMenu(menu.SubMenus);

		}
		private void BuildMenuFromFlat(string menu,ArrayList flatMenu,ArrayList newMenu)
		{
			ArrayList tempMenu = new ArrayList();
			//Get the Menus at this level
			GetMenusAtMenu(menu,flatMenu,tempMenu);

			//Get the SubMenus
			foreach(MenuStruct menuItem in tempMenu)
			{
				BuildMenuFromFlat(menu + "/" +  menuItem.Description,flatMenu,menuItem.SubMenus);
			}
			newMenu.AddRange(tempMenu);
		}
		private void GetMenusAtMenu(string menu,ArrayList flatMenu,ArrayList listToAddTo)
		{
			for(int i=flatMenu.Count-1;i>=0;i--)
			{
				MenuMapAttribute tempMenu = (MenuMapAttribute)flatMenu[i];
				if(tempMenu.Menu==menu)
				{
					listToAddTo.Add( new MenuStruct(tempMenu.Security,tempMenu.Description,tempMenu.Url,tempMenu.JavaCode,tempMenu.SortOrder,tempMenu.DisplayIfEmpty,tempMenu.Type));
					flatMenu.RemoveAt(i);
				}
			}
		}
		private ArrayList GetFlatMenu()
		{
			ArrayList flatMenu = new ArrayList();

			AppDomain currentDomain = AppDomain.CurrentDomain;
            LoadAllAssemblies(currentDomain);
			Assembly[] loadedAssembly=  currentDomain.GetAssemblies();

			foreach(Assembly assembly in loadedAssembly)
			{
                //System.Web.HttpContext.Current.Response.Write(assembly.Location + assembly.FullName  + " <br>");
				//Only look at Assemblies with the MenuMap Attribute
				//if(assembly.GetCustomAttributes(typeof(MenuMapAttribute),false).Length >0)
				{
					Type[] types = assembly.GetTypes();

					foreach(Type type in types)
					{
						//Look at each Propterity to see if it's a menumap
						System.Reflection.PropertyInfo[] properties = type.GetProperties();
						foreach(PropertyInfo info in properties)
						{
							MenuMapAttribute[] maps = (MenuMapAttribute[])info.GetCustomAttributes(typeof(MenuMapAttribute),false);

							if(maps.Length>0)//If they have the MenuMap
							{
								MethodInfo[] methods = info.GetAccessors();
								if(methods.Length>0)//If they have a Get accessor
									flatMenu.Add( new MenuMapAttribute(maps[0].Security,maps[0].Description,maps[0].Menu,methods[0].Invoke(null,null).ToString(),maps[0].JavaCode,maps[0].SortOrder,maps[0].DisplayIfEmpty,maps[0].Type));
							}
						}

						//Look for MenuMaps on the Class
						MenuMapAttribute[] classMaps = (MenuMapAttribute[])type.GetCustomAttributes(typeof(MenuMapAttribute),false);
						foreach (MenuMapAttribute map in classMaps)
                        {
                            if (map.Description == "Archive")
                            {
                                flatMenu.Add(new MenuMapAttribute(map.Security, map.Description, map.Menu, map.Url, map.JavaCode.Replace("UserKey=", "UserKey=" + Session["User_Key"].ToString()), map.SortOrder, map.DisplayIfEmpty, map.Type));
                            }
                            else
                            {
                                flatMenu.Add(new MenuMapAttribute(map.Security, map.Description, map.Menu, map.Url, map.JavaCode, map.SortOrder, map.DisplayIfEmpty, map.Type));
                            }
                        }
							
					}
				}
			}

			return flatMenu;
		}
        private void LoadAllAssemblies(AppDomain currentDomain)
        {

            System.IO.DirectoryInfo binInfo = new System.IO.DirectoryInfo(currentDomain.BaseDirectory + "/bin");

            foreach (FileInfo dll in binInfo.GetFiles())
                if(dll.Name.IndexOf(".dll")!=-1)
                currentDomain.Load(dll.Name.Replace(".dll",""));
        }

		#endregion
	}
}
