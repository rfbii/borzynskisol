using System;
using System.Collections;
using KeyCentral.Functions;
using KeyCentral.Lookups;

namespace KeyCentral.Functions
{


    public struct WhereClauseUnit
    {

        public enum UnitType
        {
            String,
            LikeString,
            LikeAndNotLikeString,
			InclusionsAndExclusionsString,
            Int32,
            DateRangeSQL,
            DateRangeOracle,
            NotInt32
        }

        UnitType type;
        string field;
		string field2;
        ArrayList values;
		ArrayList values2;
		ArrayList values3;
		ArrayList values4;

        public UnitType Type
        {
            get { return type; }
            set { type = value; }
        }
        public string Field
        {
            get { return field; }
            set { field = value; }
		}
		public string Field2
		{
			get { return field2; }
			set { field2 = value; }
		}
        public ArrayList Values
        {
            get { return values; }
            set { values = value; }
        }
        public ArrayList Values2
        {
            get { return values2; }
            set { values2 = value; }
		}
		public ArrayList Values3
		{
			get { return values3; }
			set { values3 = value; }
		}
		public ArrayList Values4
		{
			get { return values4; }
			set { values4 = value; }
		}

        public WhereClauseUnit(UnitType inType, string inField)
        {
            type = inType;
			field = inField;
			field2 = null;
            values = null;
			values2 = null;
			values3 = null;
			values4 = null;
        }
        public WhereClauseUnit(UnitType inType, string inField, string inValues)
        {
            type = inType;
			field = inField;
			field2 = null;
            values = new ArrayList();
			values2 = null;
			values3 = null;
			values4 = null;

            if (inValues.Trim() != "")
                values.Add(inValues);
        }
        public WhereClauseUnit(UnitType inType, string inField, ArrayList inValues)
        {
            type = inType;
			field = inField;
			field2 = null;
            values = inValues;
			values2 = null;
			values3 = null;
			values4 = null;
        }
        public WhereClauseUnit(UnitType inType, string inField, ArrayList inValues, ArrayList inValues2)
        {
            type = inType;
			field = inField;
			field2 = null;
            values = inValues;
			values2 = inValues2;
			values3 = null;
			values4 = null;
		}
		public WhereClauseUnit(UnitType inType, string inField, string inField2, ArrayList inValues, ArrayList inValues2, ArrayList inValues3, ArrayList inValues4)
		{
			type = inType;
			field = inField;
			field2 = inField2;
			values = inValues;
			values2 = inValues2;
			values3 = inValues3;
			values4 = inValues4;
		}
    }


    public class WhereClauseBuilder
    {
        private ArrayList values;

        #region constructors
        public WhereClauseBuilder()
        {
            values = new ArrayList();
        }
        #endregion

        #region Add function
        public void Add(WhereClauseUnit unit)
        {
            values.Add(unit);
        }
        public void Add(WhereClauseUnit.UnitType inType, string inField, ArrayList inValues)
        {
            Add(new KeyCentral.Functions.WhereClauseUnit(inType, inField, inValues));
        }
        public void AddCriteriaBox(WhereClauseUnit.UnitType inType, string inField, ArrayList inValues)
        {
            ArrayList vals = new ArrayList();
            foreach (CriteriaBoxRecord record in inValues)
                vals.Add(record.key);
            Add(new KeyCentral.Functions.WhereClauseUnit(inType, inField, vals));
        }
        public void AddCriteriaBox(WhereClauseUnit.UnitType inType, string inField, ArrayList inValues, ArrayList inValues2)
        {
            ArrayList vals = new ArrayList();
            ArrayList vals2 = new ArrayList();
            foreach (CriteriaBoxRecord record in inValues)
                vals.Add(record.key);
            foreach (CriteriaBoxRecord record in inValues2)
                vals2.Add(record.key);
            Add(new KeyCentral.Functions.WhereClauseUnit(inType, inField, vals, vals2));
		}
		public void AddCriteriaBox(WhereClauseUnit.UnitType inType, string inField, string inField2, ArrayList inValues, ArrayList inValues2, ArrayList inValues3, ArrayList inValues4)
		{
			ArrayList vals = new ArrayList();
			ArrayList vals2 = new ArrayList();
			ArrayList vals3 = new ArrayList();
			ArrayList vals4 = new ArrayList();
			foreach (CriteriaBoxRecord record in inValues)
				vals.Add(record.key);
			foreach (CriteriaBoxRecord record in inValues2)
				vals2.Add(record.key);
			foreach (CriteriaBoxRecord record in inValues3)
				vals3.Add(record.key);
			foreach (CriteriaBoxRecord record in inValues4)
				vals4.Add(record.key);
			Add(new KeyCentral.Functions.WhereClauseUnit(inType, inField, inField2, vals, vals2, vals3, vals4));
		}
        public void Add(WhereClauseUnit.UnitType inType, string inField, string inValue)
        {
            if (inValue == null)
                Add(new KeyCentral.Functions.WhereClauseUnit(inType, inField, ""));
            else
                Add(new KeyCentral.Functions.WhereClauseUnit(inType, inField, inValue));
        }
        public void Add(WhereClauseUnit.UnitType inType, string inField, string inValue, string inValue2)
        {
            if (inValue == null || inValue2 == null || inValue.Trim() == "" || inValue.Trim() == "")
                Add(new KeyCentral.Functions.WhereClauseUnit(inType, inField, ""));
            else
            {
                ArrayList values = new ArrayList();
                values.Add(inValue);
                values.Add(inValue2);
                Add(new KeyCentral.Functions.WhereClauseUnit(inType, inField, values));
            }
        }
        public void Add(WhereClauseUnit.UnitType inType, string inField)
        {
            Add(new KeyCentral.Functions.WhereClauseUnit(inType, inField));
        }
        #endregion

        public void CheckParams(string input)
        {
            int x = 0;
            int startIndex;
            int endIndex;

            while (input.IndexOf("#WhereClause:", x) > -1)
            {
                startIndex = input.IndexOf("#WhereClause:", x);
                endIndex = input.IndexOf("#", startIndex + 1);

                if (endIndex == -1)
                    throw (new Exception("#WhereClause does not have an end #"));

                if (FieldExists(input.Substring(startIndex + 13, (endIndex - startIndex) - 13)) == false)
                    throw (new Exception("Field " + input.Substring(startIndex + 13, (endIndex - startIndex) - 13) + " has not been provided to the WhereClauseBuilder."));

                x = startIndex + 1;
            }
        }

        private bool FieldExists(string field)
        {
			foreach (WhereClauseUnit unit in values)
			{
				if (unit.Field == field)
					return true;
				else if (unit.Field2 != null)
				{
					if (unit.Field2 == field)
						return true;
				}
			}

            return false;
        }

        public string BuildWhereClause(string input)
        {
            CheckParams(input);

            foreach (WhereClauseUnit unit in values)
            {
                int loopCount = 0;
                System.Text.StringBuilder whereClause = new System.Text.StringBuilder();

                if (unit.Values.Count == 1)
                {
                    whereClause.Append(" and ");

                    if (unit.Type == WhereClauseUnit.UnitType.LikeAndNotLikeString)
                    {
                        if (unit.Values2.Count > 0)
                        {
                            whereClause.Append(" (( ");
                        }
                        else
                        {
                            whereClause.Append(" ( ");
                        }

                        whereClause.Append(" lower(" + unit.Field + ")");
                        whereClause.Append(" like ");
                    }
                    else if (unit.Type == WhereClauseUnit.UnitType.LikeString)
                    {
                        whereClause.Append(" lower(" + unit.Field + ")");
                        whereClause.Append(" like ");
                    }
					else if (unit.Type == WhereClauseUnit.UnitType.InclusionsAndExclusionsString)
					{
						whereClause.Append(" ( ");

						whereClause.Append(" not ( ");

						if (unit.Values != null && unit.Values.Count > 0 && unit.Values2 != null && unit.Values2.Count > 0)
						{
							//Appened the data of the Criteria
							for (int i = 0; i < unit.Values.Count; i++)
							{
								//Break apart at 999 because Oracle has this hard limit.
								if (loopCount != 0 && loopCount % 999 == 0)
								{
									whereClause.Remove(whereClause.Length - 1, 1);//Chop off the last ,
									whereClause.Append(" ) or "); //Append ") or GaLot.GaLotIdx in ("
									whereClause.Append(unit.Field);
									whereClause.Append(" in (");
								}

								whereClause.Append(" ( ");
								whereClause.Append(" lower(" + unit.Field + ")");
								whereClause.Append(" like ");
								whereClause.Append(FormatForType(unit.Values[i].ToString(), unit.Type) + " and ");
								whereClause.Append(" lower(" + unit.Field2 + ")");
								whereClause.Append(" like ");
								whereClause.Append(FormatForType(unit.Values2[i].ToString(), unit.Type));
								whereClause.Append(" ) or ");

								loopCount++;
							}
						}
					}
                    else if (unit.Type == WhereClauseUnit.UnitType.NotInt32)
                    {
                        whereClause.Append(unit.Field);
                        whereClause.Append(" <> ");

                    }
                    else if (unit.Type == WhereClauseUnit.UnitType.DateRangeOracle || unit.Type == WhereClauseUnit.UnitType.DateRangeSQL)
                        throw new Exception("Two values must be given to do a DataRange.");
                    else
                    {
                        whereClause.Append(unit.Field);
                        whereClause.Append(" = ");
                    }

					if (unit.Type != WhereClauseUnit.UnitType.InclusionsAndExclusionsString)
					{
						whereClause.Append(FormatForType(unit.Values[0].ToString(), unit.Type));
					}

                    if (unit.Type == WhereClauseUnit.UnitType.LikeAndNotLikeString)
                    {
                        whereClause.Append(" ) ");

                        if (unit.Values2.Count > 0)
                        {
                            whereClause.Append(" or ( ");

                            for (int i = 0; i < unit.Values2.Count; i++)
                            {
                                whereClause.Append(" lower(" + unit.Field + ")");
                                whereClause.Append(" not like ");
                                whereClause.Append(FormatForType(unit.Values2[i].ToString(), unit.Type));
                                whereClause.Append(" and");
                            }

                            whereClause.Remove(whereClause.Length - 4, 4);//Chop off the last "and"
                            whereClause.Append(" )) ");
                        }
                    }
					else if (unit.Type == WhereClauseUnit.UnitType.InclusionsAndExclusionsString)
					{
						whereClause.Remove(whereClause.Length - 3, 3);

						whereClause.Append(" ) ");

						if (unit.Values3.Count > 0 && unit.Values4.Count > 0)
						{
							whereClause.Append(" or ( ");

							for (int i = 0; i < unit.Values3.Count; i++)
							{
								whereClause.Append("( lower(" + unit.Field + ")");
								whereClause.Append(" like ");
								whereClause.Append(FormatForType(unit.Values3[i].ToString(), unit.Type));
								whereClause.Append(" and");
								whereClause.Append(" lower(" + unit.Field2 + ")");
								whereClause.Append(" like ");
								whereClause.Append(FormatForType(unit.Values4[i].ToString(), unit.Type));
								whereClause.Append(") or");
							}

							whereClause.Remove(whereClause.Length - 3, 3);//Chop off the last "or"
							whereClause.Append(" ) ");
							whereClause.Append(" ) ");
						}
						else
						{
							//input = input.Replace("#WhereClause:" + unit.Field + "#", "");
							//input = input.Replace("#WhereClause:" + unit.Field2 + "#", "");
							whereClause.Append(" ) ");
						}
					}

                    whereClause.Append("\r\n");
                    input = input.Replace("#WhereClause:" + unit.Field + "#", whereClause.ToString());

					if (unit.Type == WhereClauseUnit.UnitType.InclusionsAndExclusionsString)
					{
						input = input.Replace("#WhereClause:" + unit.Field2 + "#", "");
					}
                }
                else if (unit.Values.Count > 1)
                {
                    whereClause.Append("and (");
                    if (unit.Type == WhereClauseUnit.UnitType.LikeAndNotLikeString)
                    {
                        if (unit.Values2.Count > 0)
                        {
                            whereClause.Append(" ( ");
                        }
					}
					else if (unit.Type == WhereClauseUnit.UnitType.InclusionsAndExclusionsString)
					{
						if (unit.Values.Count > 0)
						{
							whereClause.Append(" not ( ");
						}
					}
                    else if (unit.Type != WhereClauseUnit.UnitType.LikeString)
                    {
                        whereClause.Append(unit.Field);
					}

                    //TODO: Change to use (field like 'val1%' or field like 'val2%')
                    //Error Trapping
                    /*if(unit.Type == WhereClauseUnit.UnitType.LikeString)
                        throw new Exception("Multiple strings can not be given for a like string");
                    else */
                    if ((unit.Type == WhereClauseUnit.UnitType.DateRangeOracle || unit.Type == WhereClauseUnit.UnitType.DateRangeSQL) && unit.Values.Count != 2)
                        throw new Exception("Exacly 2 dates must be give for DateRanges.");

                    //Append the start of the criteria
                    else if (unit.Type == WhereClauseUnit.UnitType.DateRangeOracle || unit.Type == WhereClauseUnit.UnitType.DateRangeSQL)
                        whereClause.Append(" between ");
                    else if (unit.Type == WhereClauseUnit.UnitType.NotInt32)
                        whereClause.Append(" not in(");
					else if (unit.Type != WhereClauseUnit.UnitType.LikeString && unit.Type != WhereClauseUnit.UnitType.LikeAndNotLikeString && unit.Type != WhereClauseUnit.UnitType.InclusionsAndExclusionsString)
                        whereClause.Append(" in (");

					if (unit.Type != WhereClauseUnit.UnitType.InclusionsAndExclusionsString)
					{
						//Appened the data of the Criteria
						foreach (string val in unit.Values)
						{
							//Break apart at 999 because Oracle has this hard limit.
							if (loopCount != 0 && loopCount % 999 == 0)
							{
								whereClause.Remove(whereClause.Length - 1, 1);//Chop off the last ,
								whereClause.Append(" ) or "); //Append ") or GaLot.GaLotIdx in ("
								whereClause.Append(unit.Field);
								whereClause.Append(" in (");
							}

							if (unit.Type == WhereClauseUnit.UnitType.DateRangeOracle || unit.Type == WhereClauseUnit.UnitType.DateRangeSQL)
								whereClause.Append(FormatForType(val, unit.Type) + " and ");
							else if (unit.Type == WhereClauseUnit.UnitType.LikeString)
							{
								whereClause.Append(" lower(" + unit.Field + ")");
								whereClause.Append(" like ");
								whereClause.Append(FormatForType(val, unit.Type) + " or ");
							}
							else if (unit.Type == WhereClauseUnit.UnitType.LikeAndNotLikeString)
							{
								whereClause.Append(" lower(" + unit.Field + ")");
								whereClause.Append(" like ");
								whereClause.Append(FormatForType(val, unit.Type) + " or ");
							}
							else
								whereClause.Append(FormatForType(val, unit.Type) + ",");

							loopCount++;
						}
					}
					else if (unit.Type == WhereClauseUnit.UnitType.InclusionsAndExclusionsString)
					{
						if (unit.Values != null && unit.Values.Count > 0 && unit.Values2 != null && unit.Values2.Count > 0)
						{
							//Appened the data of the Criteria
							for (int i = 0; i < unit.Values.Count; i++)
							{
								//Break apart at 999 because Oracle has this hard limit.
								if (loopCount != 0 && loopCount % 999 == 0)
								{
									whereClause.Remove(whereClause.Length - 1, 1);//Chop off the last ,
									whereClause.Append(" ) or "); //Append ") or GaLot.GaLotIdx in ("
									whereClause.Append(unit.Field);
									whereClause.Append(" in (");
								}

								whereClause.Append(" ( ");
								whereClause.Append(" lower(" + unit.Field + ")");
								whereClause.Append(" like ");
								whereClause.Append(FormatForType(unit.Values[i].ToString(), unit.Type) + " and ");
								whereClause.Append(" lower(" + unit.Field2 + ")");
								whereClause.Append(" like ");
								whereClause.Append(FormatForType(unit.Values2[i].ToString(), unit.Type));
								whereClause.Append(" ) or ");

								loopCount++;
							}
						}
					}

                    //Append the end of the criteria
                    if (unit.Type == WhereClauseUnit.UnitType.DateRangeOracle || unit.Type == WhereClauseUnit.UnitType.DateRangeSQL)
                    {
                        whereClause.Remove(whereClause.Length - 4, 4);
                        whereClause.Append(") \r\n");
                    }
                    else if (unit.Type == WhereClauseUnit.UnitType.LikeString)
                    {
                        whereClause.Remove(whereClause.Length - 3, 3);
                        whereClause.Append(") \r\n");
                    }
                    else if (unit.Type == WhereClauseUnit.UnitType.LikeAndNotLikeString)
                    {
                        whereClause.Remove(whereClause.Length - 3, 3);
                        whereClause.Append(" ) ");

                        if (unit.Values2.Count > 0)
                        {
                            whereClause.Append(" or ( ");

                            for (int i = 0; i < unit.Values2.Count; i++)
                            {
                                whereClause.Append(" lower(" + unit.Field + ")");
                                whereClause.Append(" not like ");
                                whereClause.Append(FormatForType(unit.Values2[i].ToString(), unit.Type));
                                whereClause.Append(" and");
                            }

                            whereClause.Remove(whereClause.Length - 4, 4);//Chop off the last "and"
                            whereClause.Append(" )) ");
                        }

                        whereClause.Append("\r\n");
                    }
					else if (unit.Type == WhereClauseUnit.UnitType.InclusionsAndExclusionsString)
					{
						whereClause.Remove(whereClause.Length - 3, 3);
						whereClause.Append(" ) ");

						if (unit.Values3.Count > 0 && unit.Values4.Count > 0)
						{
							whereClause.Append(" or ( ");

							for (int i = 0; i < unit.Values3.Count; i++)
							{
								whereClause.Append(" lower(" + unit.Field + ")");
								whereClause.Append(" like ");
								whereClause.Append(FormatForType(unit.Values3[i].ToString(), unit.Type));
								whereClause.Append(" and");
								whereClause.Append(" lower(" + unit.Field2 + ")");
								whereClause.Append(" like ");
								whereClause.Append(FormatForType(unit.Values4[i].ToString(), unit.Type));
								whereClause.Append(") or");
							}

							whereClause.Remove(whereClause.Length - 3, 3);//Chop off the last "or"
							whereClause.Append(" ) ");
						}
						else
							whereClause.Append(" ) ");

						whereClause.Append("\r\n");
					}
					else
					{
						whereClause.Remove(whereClause.Length - 1, 1);
						whereClause.Append(") )\r\n");
					}
                    input = input.Replace("#WhereClause:" + unit.Field + "#", whereClause.ToString());

					if (unit.Type == WhereClauseUnit.UnitType.InclusionsAndExclusionsString)
					{
						input = input.Replace("#WhereClause:" + unit.Field2 + "#", "");
					}
                }
                else if (unit.Values2 != null && unit.Values.Count == 0)
                {
                    if (unit.Type == WhereClauseUnit.UnitType.LikeAndNotLikeString)
                    {
                        if (unit.Values2.Count > 0)
                        {
                            whereClause.Append(" and ");
                            whereClause.Append(" ( ");

                            for (int i = 0; i < unit.Values2.Count; i++)
                            {
                                whereClause.Append(" lower(" + unit.Field + ")");
                                whereClause.Append(" not like ");
                                whereClause.Append(FormatForType(unit.Values2[i].ToString(), unit.Type));
                                whereClause.Append(" and");
                            }

                            whereClause.Remove(whereClause.Length - 4, 4);//Chop off the last "and"
                            whereClause.Append(" ) ");

                            whereClause.Append("\r\n");
                            input = input.Replace("#WhereClause:" + unit.Field + "#", whereClause.ToString());
                        }
                        else
                            input = input.Replace("#WhereClause:" + unit.Field + "#", "");
                    }
					else if (unit.Type == WhereClauseUnit.UnitType.InclusionsAndExclusionsString && unit.Values3 != null)
					{
						if (unit.Values3.Count > 0)
						{
							whereClause.Append(" and ");
							whereClause.Append(" (");

							for (int i = 0; i < unit.Values3.Count; i++)
							{
								whereClause.Append(" ( lower(" + unit.Field + ")");
								whereClause.Append(" like ");
								whereClause.Append(FormatForType(unit.Values3[i].ToString(), unit.Type));
								whereClause.Append(" and");
								whereClause.Append(" lower(" + unit.Field2 + ")");
								whereClause.Append(" like ");
								whereClause.Append(FormatForType(unit.Values4[i].ToString(), unit.Type));
								whereClause.Append(") or");
							}

							whereClause.Remove(whereClause.Length - 3, 3);//Chop off the last "or"
							whereClause.Append(" ) ");

							whereClause.Append("\r\n");
							input = input.Replace("#WhereClause:" + unit.Field + "#", whereClause.ToString());
							input = input.Replace("#WhereClause:" + unit.Field2 + "#", "");
						}
						else
						{
							input = input.Replace("#WhereClause:" + unit.Field + "#", "");
							input = input.Replace("#WhereClause:" + unit.Field2 + "#", "");
						}
					}
					else
					{
						input = input.Replace("#WhereClause:" + unit.Field + "#", "");

						if (unit.Type == WhereClauseUnit.UnitType.InclusionsAndExclusionsString)
						{
							input = input.Replace("#WhereClause:" + unit.Field2 + "#", "");
						}
					}
					//else
					//    input = input.Replace("#WhereClause:" + unit.Field + "#", "");
                }
                else
                    input = input.Replace("#WhereClause:" + unit.Field + "#", "");
            }
            return input;
        }
        private string FormatForType(string value, WhereClauseUnit.UnitType type)
        {
            string ret = "";
            switch (type)
            {
                case WhereClauseUnit.UnitType.Int32:
                    ret = value;
                    break;
                case WhereClauseUnit.UnitType.String:
                    ret = "'" + value + "'";
                    break;
                case WhereClauseUnit.UnitType.LikeString:
                    ret = "'" + value.ToLower() + "%'";
                    break;
                case WhereClauseUnit.UnitType.LikeAndNotLikeString:
                    ret = "'" + value.ToLower() + "%'";
					break;
				case WhereClauseUnit.UnitType.InclusionsAndExclusionsString:
					ret = "'" + value.ToLower() + "%'";
					break;
                case WhereClauseUnit.UnitType.DateRangeSQL:
                    ret = "cast('" + value + "' as datetime)";
                    break;
                case WhereClauseUnit.UnitType.DateRangeOracle:
                    ret = "To_Date('" + value + "','MM/DD/YYYY')";
                    break;
                case WhereClauseUnit.UnitType.NotInt32:
                    ret = value;
                    break;
            }
            return ret;
        }
    }
}