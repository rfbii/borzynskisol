//#define DebugIFrames
using System;
using System.Collections;
using System.Web.SessionState;
using System.Web;
using System.Text;

namespace KeyCentral.Functions
{
	public class SessionManager
	{
		#region Properties
		ArrayList sessionList
		{
			get
			{
				//Make sure SessionList is there!
				if(System.Web.HttpContext.Current.Application["SessionList"] == null)
					System.Web.HttpContext.Current.Application["SessionList"] = new ArrayList();

				return (ArrayList)System.Web.HttpContext.Current.Application["SessionList"];
			}
		}

		HttpSessionState Session{get{return System.Web.HttpContext.Current.Session;}}
		HttpApplicationState Application{get{return System.Web.HttpContext.Current.Application;}}
		string CurrentSessionID{get{return Session.SessionID;}}
		#endregion


		public SessionManager()
		{

		}


		#region Private Functions
		private SessionManagerCBO FindOrCreateNewSession(string sessionID,string userName)
		{
			SessionManagerCBO record;

			//Find the record or create a record
			if(Application["Session:"+ sessionID] ==null)
			{
				record = new SessionManagerCBO(sessionID,userName);
				Application[ "Session:"+ sessionID] = record;

				//FIX: Save chagnes to SessionList as well!
				sessionList.Add(sessionID);

			}	
			else
				record = (SessionManagerCBO)Application["Session:"+ sessionID];

			return record;
		}

		private void CleanUpSession(string sessionID)
		{
			//Remove the SessionID from the sessionList
			sessionList.Remove(sessionID);

			//Remove the CBO
			Application.Remove("Session:"+ sessionID);
		}
		private string CleanUpOldSessions()
		{
			if(Application["Session:LastCheck"] == null)
			{
				Application["Session:LastCheck"] = DateTime.Now;
				Application["Session:LastMessage"] = String.Empty;
			}
            else if (((TimeSpan)(DateTime.Now - (DateTime)Application["Session:LastCheck"])).Minutes >0)
				return "Already ran this minute. Last Message was:" + (string)Application["Session:LastMessage"];

			

			ArrayList list = GetSessionList();
			StringBuilder builder = new StringBuilder();
			builder.Append("Last Cleanup Ran:");
			builder.Append(((DateTime)Application["Session:LastCheck"]).ToShortTimeString() + "<BR>");

			//Go through all sessions, and cleanup and over 5 minutes old.
			foreach(SessionManagerCBO record in list)
			{
				if(((TimeSpan)( DateTime.Now - record.LastPing)).Minutes > Session.Timeout)
				{
					builder.Append("Session is too old, run cleanup on it:");
					builder.Append(record.UserName + ":");
					builder.Append(record.SessionID + "<BR>");
					CleanUpSession(record.SessionID);
				}
				else
				{
					builder.Append("Session still active:");
					builder.Append(record.UserName + ":");
					builder.Append(record.SessionID + "<BR>");
				}
			}

			//Reset the lastCheck counter
			Application["Session:LastCheck"] = DateTime.Now;
			Application["Session:LastMessage"] = builder.ToString();

			return (string)Application["Session:LastMessage"];
		}
		
		#endregion
		
		#region Public Client functions
		public void RegisterPing(string sessionID,string userName,string lastDoc,HttpRequest Request)
		{
			SessionManagerCBO record = FindOrCreateNewSession(sessionID,userName);

			//Make changes
			record.LastPing = DateTime.Now;
			record.LastDoc = lastDoc;
			record.ClientIPAddress = Request.UserHostAddress;

			CleanUpOldSessions();
		}

		public void CleanupCurrentSession()
		{
			Session.Clear();
			Session.Abandon();

			CleanUpSession(CurrentSessionID);
		}
		#endregion

		#region Public Manager Functions
		public ArrayList GetSessionList()
		{
			ArrayList ret = new ArrayList();
			
			int loopLimit = sessionList.Count;
			for(int x=0;x<loopLimit;x++)
				ret.Add(Application["Session:"+ (string)sessionList[x]]);
			
			return ret;
		}
		public bool ForceLogout()
		{
			SessionManagerCBO record = FindOrCreateNewSession(Session.SessionID,"");

			if(record.ForceLogout)
				return true;
			else
				return false;
		}
		public bool RegisterLogout(string sessionID)
		{
			if(Application["Session:"+ sessionID] == null)
				return false;

			((SessionManagerCBO)Application["Session:"+ sessionID]).ForceLogout=true;

			return true;

			//record.ForceLogout=true;
		}

		public string RunCleanupNow()
		{
			return CleanUpOldSessions();
		}
		#endregion

		#region public static Functions
		public static string GetPingScript()
		{
#if (DebugIFrames)
			return @"<script>function HandleServerMessage(message){
				if(message =='Logout')
					location.href='../../../KeyCentralABZ/UI/Security/LogOut.aspx';
				if(message.substring(0,4) =='say:')
					alert(message.substring(4));
			}</script>" + "<IFRAME NAME=\"SessionPingFrame\" src='../../../KeyCentralABZ/UI/SessionManager/SessionPing.aspx'></IFRAME><script language=javascript>document.getElementById('SessionPingFrame').src ='../../../KeyCentralABZ/UI/SessionManager/SessionPing.aspx?LastDoc=' + document.location;</script>";
#else
            return @"<script>function HandleServerMessage(message){
				if(message =='Logout')
					location.href='../../../KeyCentralABZ/UI/Security/LogOut.aspx';
				if(message.substring(0,4) =='say:')
					alert(message.substring(4));
					}</script>" + "<IFRAME NAME=\"SessionPingFrame\" style=\"WIDTH: 0px; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 0px; BORDER-BOTTOM-STYLE: none\"></IFRAME><script language=javascript>if(document.getElementById('SessionPingFrame')!= null) { document.getElementById('SessionPingFrame').src ='../../../KeyCentralABZ/UI/SessionManager/SessionPing.aspx?LastDoc=' + document.location;}</script>";
#endif
        }

		#endregion
	}

	public class SessionManagerCBO :IComparable
	{
		public string SessionID;
		public string UserName;
		public string LastDoc;
		public string ClientIPAddress;
		public DateTime FirstPing;
		public DateTime LastPing;
		public bool ForceLogout;

		public SessionManagerCBO(string sessionID,string userName)
		{
			SessionID = sessionID;
			UserName = userName;
			FirstPing = DateTime.Now;
			LastPing = DateTime.Now;
			ForceLogout =false;
		}
		#region IComparable Members

		public int CompareTo(object obj)
		{
			int tempCompare;
			SessionManagerCBO Y = (SessionManagerCBO) obj;

			//Compare SessionID
			tempCompare = this.SessionID.CompareTo(Y.SessionID);
			if(tempCompare != 0)
				return tempCompare;


			return 0;
		}

		#endregion
	}
}
