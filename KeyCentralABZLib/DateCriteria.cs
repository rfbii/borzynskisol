using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KeyCentral.Functions;
using System.Collections;

namespace KeyCentralLib
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:DateCriteria runat=server></{0}:DateCriteria>")]
    public class DateCriteria : WebControl
    {
        #region Declares
        protected string description;
        protected string check;
        protected bool isValid = false;
        protected bool isRequired = false;
        protected bool useEnterAsTab = true;
        protected System.Web.UI.WebControls.TextBox startYearstartWeek;
        protected TextBox minDate;
        protected TextBox maxDate;
        protected TextBox startYear;
        protected TextBox startWeek;
        protected TextBox endYear;
        protected TextBox endWeek;
        protected TextBox selection;
        protected TextBox from;
        protected TextBox thru;
        protected Label weekError;
        protected RequiredFieldValidator RequiredFieldValidator1;
        protected RequiredFieldValidator RequiredFieldValidator2;
        protected RangeValidator RangeValidator1;
        protected RangeValidator RangeValidator2;
        protected CompareValidator CompareValidator1;
        protected CompareValidator CompareValidator2;
        protected CompareValidator CompareValidator3;
        protected DropDownList DropDownList1;
        #endregion

        #region Properties
        public string DateSelection
        {
            get
            {
                String s = (String)ViewState["DateSelection"];
                return ((s == null) ? String.Empty : s);
            }
            set { ViewState["DateSelection"] = value; }
        }
        public string MinDate
        {
            get
            {
                return minDate.Text;
            }
            set { minDate.Text = value; }
        }
        public string MinYear
        {
            get { return startYear.Text; }
            set { startYear.Text = value; }
        }
        public string MinWeek
        {
            get { return startWeek.Text; }
            set { startWeek.Text = value; }
        }
        public string MaxDate
        {
            get { return maxDate.Text; }
            set { maxDate.Text = value; }
        }
        public string MaxYear
        {
            get { return endYear.Text; }
            set { endYear.Text = value; }
        }
        public string MaxWeek
        {
            get { return endWeek.Text; }
            set { endWeek.Text = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public bool UseEnterAsTab
        {
            get { return useEnterAsTab; }
            set { useEnterAsTab = value; }
        }
        public bool IsValid
        {
            get { return isValid; }
        }
        public bool IsRequired
        {
            get { return isRequired; }
            set
            {
                isRequired = value;
                RequiredFieldValidator1.Enabled = isRequired;
                RequiredFieldValidator2.Enabled = isRequired;
            }
        }
        public DateTime MinimumDateToValidate
        {
            set
            {
                this.RangeValidator1.MinimumValue = value.ToShortDateString();
                this.RangeValidator2.MinimumValue = value.ToShortDateString();

                this.RangeValidator1.ErrorMessage = "From Date must be between " + RangeValidator1.MinimumValue + " and " + RangeValidator1.MaximumValue + ".";
                this.RangeValidator2.ErrorMessage = "Thru Date must be between " + RangeValidator2.MinimumValue + " and " + RangeValidator2.MaximumValue + ".";
            }
        }

        public DateTime MaximumDateToValidate
        {
            set
            {
                this.RangeValidator1.MaximumValue = value.ToShortDateString();
                this.RangeValidator2.MaximumValue = value.ToShortDateString();


                this.RangeValidator1.ErrorMessage = "From Date must be between " + RangeValidator1.MinimumValue + " and " + RangeValidator1.MaximumValue + ".";
                this.RangeValidator2.ErrorMessage = "Thru Date must be between " + RangeValidator2.MinimumValue + " and " + RangeValidator2.MaximumValue + ".";
            }
        }
        #endregion

        #region Events
        protected override void OnLoad(EventArgs e)
        {
            Page.Validate();
            isValid = Page.IsValid;
            base.OnLoad(e);
        }
        protected override void OnInit(EventArgs e)
        {
            CreateControls();
            base.OnInit(e);
        }
        protected override void OnPreRender(System.EventArgs e)
        {
            this.EnableViewState = true;

            if (Page.Request[this.UniqueID + this.ClientIDSeparator + "DropDownList1"] != DateSelection)
            {
                //DropDownList changed!
                DateSelection = Page.Request[this.UniqueID + this.ClientIDSeparator + "DropDownList1"];
                if (Page.IsPostBack == true && DateSelection != "Custom")
                    DropDownList1_SelectedIndexChanged(null, null);

            }


        }
        protected override void RenderContents(HtmlTextWriter output)
        {

            output.Write(@"<table border='1' bordercolor='black'>
	<tr>
		<td>
			<table style='WIDTH: 540px; HEIGHT: 53px'>
				<tr>
					<td style='WIDTH: 230px' align='center' width='230' colSpan='2'>
                        <asp:label id='selection' Width='237px' runat='server'>Date Selections for " + description + @" Date</asp:label></td>
					<td align='right' width='70'>Start Year</td>
					<td width='40'>Week</td>
					<td align='right' width='70'>End Year</td>
					<td width='40'>Week</td>
				</tr>
				<tr>
					<td style='WIDTH: 189px' align='center' colSpan='2'>");

            DropDownList1.RenderControl(output);

            output.Write(@"</td><td>");
            startYear.RenderControl(output);

            output.Write(@"</td><td>");
            startWeek.RenderControl(output);

            output.Write(@"</td><td>");
            endYear.RenderControl(output);

            output.Write(@"</td><td>");
            endWeek.RenderControl(output);

            output.Write(@"</td></tr></table>
			<table style='WIDTH: 540px; HEIGHT: 33px' width='540'>
				<tr>
					<td align='right' width='150'>");

            output.Write("From Date");
            //GetLabel("from","From Date").RenderControl(output);

            output.Write(@"</td><td align='left' width='110'>");

            //Render rfv1 then min date
            RequiredFieldValidator1.RenderControl(output);


            //minDate.Text = MinDate;
            minDate.RenderControl(output);

            output.Write(@"</td><td align='right' width='150'>");
            output.Write("Thru Date");
            //GetLabel("thru","Thru Date").RenderControl(output);

            output.Write(@"</td><td width='110'>");
            maxDate.RenderControl(output);
            //getRFValidator("RequiredFieldValidator2","maxDate","Thru Date must be provided.",false,false).RenderControl(output);

            output.Write(@"</td></tr><tr></tr></table>
			<table width='540'>
				<tr>
					<td width='266'>");
            CompareValidator1.RenderControl(output);
            RangeValidator1.RenderControl(output);
            //GetCValidator("CompareValidator1", "minDate", "Thru Date must be after From Date.", ValidatorDisplay.Dynamic, "maxDate", ValidationCompareOperator.LessThanEqual, ValidationDataType.Date).RenderControl(output);
            //GetRValidator("RangeValidator1", "minDate", ValidatorDisplay.Dynamic, "1/1/2099", "1/1/1900", ValidationDataType.Date).RenderControl(output);

            CompareValidator3.RenderControl(output);
            //GetCValidator("CompareValidator3", "minDate", "Invalid From Date.", ValidatorDisplay.Dynamic, "", ValidationCompareOperator.DataTypeCheck, ValidationDataType.Date).RenderControl(output);

            output.Write(@"</td><td>");

            CompareValidator2.RenderControl(output);
            RangeValidator2.RenderControl(output);
            //GetCValidator("CompareValidator2", "maxDate", "Invalid Thru Date.", ValidatorDisplay.Dynamic, "", ValidationCompareOperator.DataTypeCheck, ValidationDataType.Date).RenderControl(output);

            weekError.RenderControl(output);

            output.Write(@"</td></tr></table>
		</td>
	</tr>
</table>");
        }
        void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            minDate.Text = "";
            maxDate.Text = "";
            switch (DateSelection)
            {
                case "Today":
                    minDate.Text = System.DateTime.Now.ToShortDateString();
                    maxDate.Text = System.DateTime.Now.ToShortDateString();
                    break;
                case "Yesterday":
                    minDate.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
                    break;
                case "Tomorrow":
                    minDate.Text = (System.DateTime.Today.AddDays(+1)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(+1)).ToShortDateString();
                    break;
                case "This Week":
                    ThisWeek();
                    break;
                case "This Week Thru Today":
                    ThisWeekThruToday();
                    break;
                case "Last Week":
                    LastWeek();
                    break;
                case "Last 2 Weeks":
                    Last2Weeks();
                    break;
                case "Last 4 Weeks":
                    Last4Weeks();
                    break;
                case "Last 13 Weeks":
                    Last13Weeks();
                    break;
                case "Last 26 Weeks":
                    Last26Weeks();
                    break;
                case "Last 52 Weeks":
                    Last52Weeks();
                    break;
                case "This Month":
                    ThisMonth();
                    break;
                case "This Month Thru Today":
                    ThisMonthThruToday();
                    break;
                case "This Month Year Ago":
                    ThisMonthYearAgo();
                    break;
                case "Last Month":
                    LastMonth();
                    break;
                case "Last Month Year Ago":
                    LastMonthYearAgo();
                    break;
                case "This Year":
                    minDate.Text = "1/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "12/31/" + (System.DateTime.Today.Year);
                    break;
                case "This Year Thru Today":
                    minDate.Text = "1/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = System.DateTime.Now.ToShortDateString();
                    break;
                case "Last Year":
                    minDate.Text = "1/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "12/31/" + ((System.DateTime.Today.Year) - 1);
                    break;
            }
        }
        #endregion

        #region Private Helpers
        #region GetBox stuff
        private List<string> GetDateOptions()
        {
            List<string> myList = new List<string>();

            myList.Add("Custom");
            myList.Add("Today");
            myList.Add("Yesterday");
            myList.Add("Tomorrow");
            myList.Add("This Week");
            myList.Add("This Week Thru Today");
            myList.Add("Last Week");
            myList.Add("Last 2 Weeks");
            myList.Add("Last 4 Weeks");
            myList.Add("Last 13 Weeks");
            myList.Add("Last 26 Weeks");
            myList.Add("Last 52 Weeks");
            myList.Add("This Month");
            myList.Add("This Month Thru Today");
            myList.Add("This Month Year Ago");
            myList.Add("Last Month");
            myList.Add("Last Month Year Ago");
            myList.Add("This Year");
            myList.Add("This Year Thru Today");
            myList.Add("Last Year");

            return myList;
        }
        private DropDownList GetDateDropDown()
        {
            System.Web.UI.WebControls.DropDownList DropDownList1 = new DropDownList();
            DropDownList1.Width = System.Web.UI.WebControls.Unit.Pixel(237);
            //DropDownList1.ID = /*this.UniqueID + ":" +*/ "DropDownList1";
            DropDownList1.ID = this.UniqueID + this.ClientIDSeparator + "DropDownList1";

            List<string> myList = GetDateOptions();

            foreach (string item in myList)
                DropDownList1.Items.Add(item);

            /*
                        DropDownList1.Items.Add("Custom");
                        DropDownList1.Items.Add("Today");
                        DropDownList1.Items.Add("Yesterday");
                        DropDownList1.Items.Add("Tomorrow");
                        DropDownList1.Items.Add("This Week");
                        DropDownList1.Items.Add("This Week Thru Today");
                        DropDownList1.Items.Add("Last Week");
                        DropDownList1.Items.Add("Last 2 Weeks");
                        DropDownList1.Items.Add("Last 4 Weeks");
                        DropDownList1.Items.Add("Last 13 Weeks");
                        DropDownList1.Items.Add("Last 26 Weeks");
                        DropDownList1.Items.Add("Last 52 Weeks");
                        DropDownList1.Items.Add("This Month");
                        DropDownList1.Items.Add("This Month Thru Today");
                        DropDownList1.Items.Add("This Month Year Ago");
                        DropDownList1.Items.Add("Last Month");
                        DropDownList1.Items.Add("Last Month Year Ago");
                        DropDownList1.Items.Add("This Year");
                        DropDownList1.Items.Add("This Year Thru Today");
                        DropDownList1.Items.Add("Last Year");
                        */
            DropDownList1.AutoPostBack = true;


            //DropDownList1.SelectedIndexChanged += new EventHandler(DropDownList1_SelectedIndexChanged);

            this.Controls.Add(DropDownList1);
            return DropDownList1;

        }
        private TextBox GetTextBox(string id, int width)
        {
            TextBox box = new TextBox();
            box.ID = this.UniqueID + this.ClientIDSeparator + id;
            box.Width = System.Web.UI.WebControls.Unit.Pixel(width);

            this.Controls.Add(box);
            return box;
        }
        private Label GetLabel(string id, int width)
        {
            Label box = new Label();
            box.ID = this.UniqueID + this.ClientIDSeparator + id;
            box.Width = System.Web.UI.WebControls.Unit.Pixel(width);

            this.Controls.Add(box);
            return box;
        }
        private Label GetLabel(string id, string text)
        {
            Label box = new Label();
            box.ID = this.UniqueID + this.ClientIDSeparator + id;
            box.Text = text;

            this.Controls.Add(box);
            return box;
        }
        private RequiredFieldValidator getRFValidator(string id, string control, string message, bool display, bool enabled)
        {
            RequiredFieldValidator validator = new RequiredFieldValidator();
            validator.ID = this.UniqueID + this.ClientIDSeparator + id;
            validator.ControlToValidate = this.UniqueID + this.ClientIDSeparator + control;
            validator.ErrorMessage = message;
            if (display == false)
                validator.Display = ValidatorDisplay.Dynamic;
            validator.Enabled = enabled;

            this.Controls.Add(validator);
            return validator;
        }
        private CompareValidator GetCValidator(string id, string control, string errorMessage, ValidatorDisplay display, string controlToCompare, ValidationCompareOperator compareOperator, ValidationDataType dataType)
        {
            CompareValidator validator = new CompareValidator();
            validator.ID = this.UniqueID + this.ClientIDSeparator + id;
            validator.ControlToValidate = this.UniqueID + this.ClientIDSeparator + control;
            validator.ErrorMessage = errorMessage;
            validator.Display = display;
            validator.ControlToCompare = this.UniqueID + this.ClientIDSeparator + controlToCompare;
            validator.Operator = compareOperator;// ValidationCompareOperator.LessThanEqual;
            validator.Type = dataType;// ValidationDataType.Date;

            this.Controls.Add(validator);
            return validator;
        }
        private RangeValidator GetRValidator(string id, string control, ValidatorDisplay display, string minVal, string maxVal, ValidationDataType dataType)
        {
            RangeValidator validator = new RangeValidator();
            validator.ID = this.UniqueID + this.ClientIDSeparator + id;
            validator.ControlToValidate = this.UniqueID + this.ClientIDSeparator + control;
            validator.Display = display;
            validator.MinimumValue = minVal;
            validator.MaximumValue = maxVal;
            validator.Type = dataType;

            this.Controls.Add(validator);
            return validator;
        }
        #endregion

        private void CreateControls()
        {
            this.DropDownList1 = GetDateDropDown();

            //Create the controls
            startYear = GetTextBox("startYear", 70);
            startWeek = GetTextBox("startWeek", 40);
            endYear = GetTextBox("endYear", 70);
            endWeek = GetTextBox("endWeek", 40);
            minDate = GetTextBox("minDate", 95);
            maxDate = GetTextBox("maxDate", 95);

            //Add the Attributes we need
            startWeek.Attributes.Add("onblur", "SetStartDate('" + startWeek.ID + "','" + startYear.ID + "','" + minDate.ID + "');");
            startYear.Attributes.Add("onblur", "SetStartDate('" + startWeek.ID + "','" + startYear.ID + "','" + minDate.ID + "');");
            endYear.Attributes.Add("onblur", "SetEndDate('" + endWeek.ID + "','" + endYear.ID + "','" + maxDate.ID + "');");
            endWeek.Attributes.Add("onblur", "SetEndDate('" + endWeek.ID + "','" + endYear.ID + "','" + maxDate.ID + "');");

            minDate.Attributes.Add("CalendarPopUp", "");
            minDate.Attributes.Add("onblur", "CheckDateField(this);CheckForShortDate(this);ClearWeekYear('" + startWeek.ID + "','" + startYear.ID + "');if (typeof(Page_ClientValidate) == 'function') Page_ClientValidate();");

            maxDate.Attributes.Add("CalendarPopUp", "");
            maxDate.Attributes.Add("onblur", "CheckDateField(this);CheckForShortDate(this);ClearWeekYear('" + endWeek.ID + "','" + endYear.ID + "');if (typeof(Page_ClientValidate) == 'function') Page_ClientValidate();");

            //This needs to be moved to pre-Render
            if (UseEnterAsTab)
            {
                minDate.Attributes.Add("EnterAsTab", "");
                maxDate.Attributes.Add("EnterAsTab", "");
                startYear.Attributes.Add("EnterAsTab", "");
                startWeek.Attributes.Add("EnterAsTab", "");
                endYear.Attributes.Add("EnterAsTab", "");
                endWeek.Attributes.Add("EnterAsTab", "");
            }

            weekError = new Label();
            weekError.ID = "weekError";
            weekError.ForeColor = System.Drawing.Color.Red;

            RequiredFieldValidator1 = getRFValidator("RequiredFieldValidator1", "minDate", "From Date must be provided.", false, false);
            RequiredFieldValidator1.ControlToValidate = minDate.ID;

            CompareValidator1 = new CompareValidator();
            CompareValidator1.ID = this.UniqueID + this.ClientIDSeparator + "CompareValidator1";
            CompareValidator1.ControlToValidate = minDate.ID;
            CompareValidator1.ControlToCompare = maxDate.ID;
            CompareValidator1.ErrorMessage = "Thru Date must be after From Date.";
            CompareValidator1.Display = ValidatorDisplay.Dynamic;
            CompareValidator1.Operator = ValidationCompareOperator.LessThanEqual;
            CompareValidator1.Type = ValidationDataType.Date;
            this.Controls.Add(CompareValidator1);

            CompareValidator2 = new CompareValidator();
            CompareValidator2.ID = this.UniqueID + this.ClientIDSeparator + "CompareValidator2";
            CompareValidator2.ControlToValidate = maxDate.ID;
            CompareValidator2.ErrorMessage = "Invalid Thru Date.";
            CompareValidator2.Display = ValidatorDisplay.Dynamic;
            CompareValidator2.Operator = ValidationCompareOperator.DataTypeCheck;
            CompareValidator2.Type = ValidationDataType.Date;
            this.Controls.Add(CompareValidator2);

            //GetCValidator("CompareValidator1", "minDate", "Thru Date must be after From Date.", ValidatorDisplay.Dynamic, "maxDate", ValidationCompareOperator.LessThanEqual, ValidationDataType.Date).RenderControl(output);
            //GetCValidator("CompareValidator2", "maxDate", "Invalid Thru Date.", ValidatorDisplay.Dynamic, "", ValidationCompareOperator.DataTypeCheck, ValidationDataType.Date).RenderControl(output);

            //GetCValidator("CompareValidator3", "minDate", "Invalid From Date.", ValidatorDisplay.Dynamic, "", ValidationCompareOperator.DataTypeCheck, ValidationDataType.Date).RenderControl(output);
            CompareValidator3 = new CompareValidator();
            CompareValidator3.ID = this.UniqueID + this.ClientIDSeparator + "CompareValidator3";
            CompareValidator3.ControlToValidate = minDate.ID;
            CompareValidator3.ErrorMessage = "Invalid From Date.";
            CompareValidator3.Display = ValidatorDisplay.Dynamic;
            CompareValidator3.Operator = ValidationCompareOperator.DataTypeCheck;
            CompareValidator3.Type = ValidationDataType.Date;
            this.Controls.Add(CompareValidator3);

            RangeValidator1 = new RangeValidator();
            RangeValidator1.ID = this.UniqueID + this.ClientIDSeparator + "RangeValidator1";
            RangeValidator1.ControlToValidate = minDate.ID;
            RangeValidator1.ErrorMessage = "From Date is not within the range.";
            RangeValidator1.Display = ValidatorDisplay.Dynamic;
            RangeValidator1.Type = ValidationDataType.Date;
            RangeValidator1.MinimumValue = "1/1/1900";//DateTime.MinValue;
            RangeValidator1.MaximumValue = "1/1/2099";//DateTime.MaxValue;
            this.Controls.Add(RangeValidator1);

            RangeValidator2 = new RangeValidator();
            RangeValidator2.ID = this.UniqueID + this.ClientIDSeparator + "RangeValidator2";
            RangeValidator2.ControlToValidate = maxDate.ID;
            RangeValidator2.ErrorMessage = "Thru Date is not within the range.";
            RangeValidator2.Display = ValidatorDisplay.Dynamic;
            RangeValidator2.Type = ValidationDataType.Date;
            RangeValidator2.MinimumValue = "1/1/1900";//DateTime.MinValue;
            RangeValidator2.MaximumValue = "1/1/2099";//DateTime.MaxValue;
            this.Controls.Add(RangeValidator2);



            //Page.RegisterRequiresRaiseEvent(this);
            //Page.RegisterRequiresRaiseEvent(RequiredFieldValidator1);
        }
        private void SetStartDate()
        {
            try
            {
                int week = Int32.Parse(startWeek.Text);
                int year = Int32.Parse(startYear.Text);
                DateTime startDate = Misc.WeekToStartDate(week, year);

                if (week < 0)
                {
                    weekError.Text = "Week can not be less than 0.";
                }
                else if (week > 50 && year != Misc.WeekToStartDate(week, year).Year)
                {
                    weekError.Text = "There is not " + endWeek.Text + " weeks in " + endYear.Text;
                }
                else
                    minDate.Text = startDate.ToShortDateString();
            }
            catch
            {
                weekError.Text = "Error with start week/year.";
            }
        }
        private void SetEndDate()
        {
            try
            {
                int week = Int32.Parse(endWeek.Text);
                int year = Int32.Parse(endYear.Text);
                DateTime endDate = Misc.WeekToEndDate(week, year);

                if (week < 0)
                {
                    weekError.Text = "Week can not be less than 0.";
                }
                else if (week > 50 && year != Misc.WeekToStartDate(week, year).Year)
                {
                    weekError.Text = "There is not " + endWeek.Text + " weeks in " + endYear.Text;
                }
                else
                    maxDate.Text = endDate.ToShortDateString();
            }
            catch
            {
                weekError.Text = "Error with end week/year.";
            }
        }

        private void ThisWeek()
        {
            switch (System.DateTime.Today.DayOfWeek)
            {
                case System.DayOfWeek.Sunday:
                    minDate.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today).ToShortDateString();
                    break;
                case System.DayOfWeek.Monday:
                    minDate.Text = (System.DateTime.Today).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(+6)).ToShortDateString();
                    break;
                case System.DayOfWeek.Tuesday:
                    minDate.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(+5)).ToShortDateString();
                    break;
                case System.DayOfWeek.Wednesday:
                    minDate.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(+4)).ToShortDateString();
                    break;
                case System.DayOfWeek.Thursday:
                    minDate.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(+3)).ToShortDateString();
                    break;
                case System.DayOfWeek.Friday:
                    minDate.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(+2)).ToShortDateString();
                    break;
                case System.DayOfWeek.Saturday:
                    minDate.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(+1)).ToShortDateString();
                    break;
            }
        }
        private void ThisWeekThruToday()
        {
            switch (System.DateTime.Today.DayOfWeek)
            {
                case System.DayOfWeek.Sunday:
                    minDate.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today).ToShortDateString();
                    break;
                case System.DayOfWeek.Monday:
                    minDate.Text = (System.DateTime.Today).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today).ToShortDateString();
                    break;
                case System.DayOfWeek.Tuesday:
                    minDate.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today).ToShortDateString();
                    break;
                case System.DayOfWeek.Wednesday:
                    minDate.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today).ToShortDateString();
                    break;
                case System.DayOfWeek.Thursday:
                    minDate.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today).ToShortDateString();
                    break;
                case System.DayOfWeek.Friday:
                    minDate.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today).ToShortDateString();
                    break;
                case System.DayOfWeek.Saturday:
                    minDate.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today).ToShortDateString();
                    break;
            }
        }
        private void LastWeek()
        {
            switch (System.DateTime.Today.DayOfWeek)
            {
                case System.DayOfWeek.Sunday:
                    minDate.Text = (System.DateTime.Today.AddDays(-13)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
                    break;
                case System.DayOfWeek.Monday:
                    minDate.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
                    break;
                case System.DayOfWeek.Tuesday:
                    minDate.Text = (System.DateTime.Today.AddDays(-8)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
                    break;
                case System.DayOfWeek.Wednesday:
                    minDate.Text = (System.DateTime.Today.AddDays(-9)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
                    break;
                case System.DayOfWeek.Thursday:
                    minDate.Text = (System.DateTime.Today.AddDays(-10)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
                    break;
                case System.DayOfWeek.Friday:
                    minDate.Text = (System.DateTime.Today.AddDays(-11)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
                    break;
                case System.DayOfWeek.Saturday:
                    minDate.Text = (System.DateTime.Today.AddDays(-12)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
                    break;
            }
        }
        private void Last2Weeks()
        {
            switch (System.DateTime.Today.DayOfWeek)
            {
                case System.DayOfWeek.Sunday:
                    minDate.Text = (System.DateTime.Today.AddDays(-20)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
                    break;
                case System.DayOfWeek.Monday:
                    minDate.Text = (System.DateTime.Today.AddDays(-14)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
                    break;
                case System.DayOfWeek.Tuesday:
                    minDate.Text = (System.DateTime.Today.AddDays(-15)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
                    break;
                case System.DayOfWeek.Wednesday:
                    minDate.Text = (System.DateTime.Today.AddDays(-16)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
                    break;
                case System.DayOfWeek.Thursday:
                    minDate.Text = (System.DateTime.Today.AddDays(-17)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
                    break;
                case System.DayOfWeek.Friday:
                    minDate.Text = (System.DateTime.Today.AddDays(-18)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
                    break;
                case System.DayOfWeek.Saturday:
                    minDate.Text = (System.DateTime.Today.AddDays(-19)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
                    break;
            }
        }
        private void Last4Weeks()
        {
            switch (System.DateTime.Today.DayOfWeek)
            {
                case System.DayOfWeek.Sunday:
                    minDate.Text = (System.DateTime.Today.AddDays(-34)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
                    break;
                case System.DayOfWeek.Monday:
                    minDate.Text = (System.DateTime.Today.AddDays(-28)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
                    break;
                case System.DayOfWeek.Tuesday:
                    minDate.Text = (System.DateTime.Today.AddDays(-29)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
                    break;
                case System.DayOfWeek.Wednesday:
                    minDate.Text = (System.DateTime.Today.AddDays(-30)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
                    break;
                case System.DayOfWeek.Thursday:
                    minDate.Text = (System.DateTime.Today.AddDays(-31)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
                    break;
                case System.DayOfWeek.Friday:
                    minDate.Text = (System.DateTime.Today.AddDays(-32)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
                    break;
                case System.DayOfWeek.Saturday:
                    minDate.Text = (System.DateTime.Today.AddDays(-33)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
                    break;
            }
        }
        private void Last13Weeks()
        {
            switch (System.DateTime.Today.DayOfWeek)
            {
                case System.DayOfWeek.Sunday:
                    minDate.Text = (System.DateTime.Today.AddDays(-97)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
                    break;
                case System.DayOfWeek.Monday:
                    minDate.Text = (System.DateTime.Today.AddDays(-91)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
                    break;
                case System.DayOfWeek.Tuesday:
                    minDate.Text = (System.DateTime.Today.AddDays(-92)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
                    break;
                case System.DayOfWeek.Wednesday:
                    minDate.Text = (System.DateTime.Today.AddDays(-93)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
                    break;
                case System.DayOfWeek.Thursday:
                    minDate.Text = (System.DateTime.Today.AddDays(-94)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
                    break;
                case System.DayOfWeek.Friday:
                    minDate.Text = (System.DateTime.Today.AddDays(-95)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
                    break;
                case System.DayOfWeek.Saturday:
                    minDate.Text = (System.DateTime.Today.AddDays(-96)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
                    break;
            }
        }
        private void Last26Weeks()
        {
            switch (System.DateTime.Today.DayOfWeek)
            {
                case System.DayOfWeek.Sunday:
                    minDate.Text = (System.DateTime.Today.AddDays(-188)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
                    break;
                case System.DayOfWeek.Monday:
                    minDate.Text = (System.DateTime.Today.AddDays(-182)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
                    break;
                case System.DayOfWeek.Tuesday:
                    minDate.Text = (System.DateTime.Today.AddDays(-183)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
                    break;
                case System.DayOfWeek.Wednesday:
                    minDate.Text = (System.DateTime.Today.AddDays(-184)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
                    break;
                case System.DayOfWeek.Thursday:
                    minDate.Text = (System.DateTime.Today.AddDays(-184)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
                    break;
                case System.DayOfWeek.Friday:
                    minDate.Text = (System.DateTime.Today.AddDays(-186)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
                    break;
                case System.DayOfWeek.Saturday:
                    minDate.Text = (System.DateTime.Today.AddDays(-187)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
                    break;
            }
        }
        private void Last52Weeks()
        {
            switch (System.DateTime.Today.DayOfWeek)
            {
                case System.DayOfWeek.Sunday:
                    minDate.Text = (System.DateTime.Today.AddDays(-370)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
                    break;
                case System.DayOfWeek.Monday:
                    minDate.Text = (System.DateTime.Today.AddDays(-364)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
                    break;
                case System.DayOfWeek.Tuesday:
                    minDate.Text = (System.DateTime.Today.AddDays(-365)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
                    break;
                case System.DayOfWeek.Wednesday:
                    minDate.Text = (System.DateTime.Today.AddDays(-366)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
                    break;
                case System.DayOfWeek.Thursday:
                    minDate.Text = (System.DateTime.Today.AddDays(-367)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
                    break;
                case System.DayOfWeek.Friday:
                    minDate.Text = (System.DateTime.Today.AddDays(-368)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
                    break;
                case System.DayOfWeek.Saturday:
                    minDate.Text = (System.DateTime.Today.AddDays(-369)).ToShortDateString();
                    maxDate.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
                    break;
            }
        }
        private void ThisMonth()
        {
            switch (System.DateTime.Today.Month)
            {
                case 1:
                    minDate.Text = "1/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "1/31/" + (System.DateTime.Today.Year);
                    break;
                case 2:
                    if (System.DateTime.IsLeapYear(System.DateTime.Today.Year))
                    {
                        minDate.Text = "2/1/" + (System.DateTime.Today.Year);
                        maxDate.Text = "2/29/" + (System.DateTime.Today.Year);
                    }
                    else
                    {
                        minDate.Text = "2/1/" + (System.DateTime.Today.Year);
                        maxDate.Text = "2/28/" + (System.DateTime.Today.Year);
                    }
                    break;
                case 3:
                    minDate.Text = "3/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "3/31/" + (System.DateTime.Today.Year);
                    break;
                case 4:
                    minDate.Text = "4/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "4/30/" + (System.DateTime.Today.Year);
                    break;
                case 5:
                    minDate.Text = "5/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "5/31/" + (System.DateTime.Today.Year);
                    break;
                case 6:
                    minDate.Text = "6/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "6/30/" + (System.DateTime.Today.Year);
                    break;
                case 7:
                    minDate.Text = "7/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "7/31/" + (System.DateTime.Today.Year);
                    break;
                case 8:
                    minDate.Text = "8/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "8/31/" + (System.DateTime.Today.Year);
                    break;
                case 9:
                    minDate.Text = "9/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "9/30/" + (System.DateTime.Today.Year);
                    break;
                case 10:
                    minDate.Text = "10/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "10/31/" + (System.DateTime.Today.Year);
                    break;
                case 11:
                    minDate.Text = "11/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "11/30/" + (System.DateTime.Today.Year);
                    break;
                case 12:
                    minDate.Text = "12/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "12/31/" + (System.DateTime.Today.Year);
                    break;
            }
        }
        private void ThisMonthThruToday()
        {
            switch (System.DateTime.Today.Month)
            {
                case 1:
                    minDate.Text = "1/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = (System.DateTime.Today).ToShortDateString();
                    break;
                case 2:
                    minDate.Text = "2/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = (System.DateTime.Today).ToShortDateString();
                    break;
                case 3:
                    minDate.Text = "3/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = (System.DateTime.Today).ToShortDateString();
                    break;
                case 4:
                    minDate.Text = "4/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = (System.DateTime.Today).ToShortDateString();
                    break;
                case 5:
                    minDate.Text = "5/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = (System.DateTime.Today).ToShortDateString();
                    break;
                case 6:
                    minDate.Text = "6/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = (System.DateTime.Today).ToShortDateString();
                    break;
                case 7:
                    minDate.Text = "7/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = (System.DateTime.Today).ToShortDateString();
                    break;
                case 8:
                    minDate.Text = "8/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = (System.DateTime.Today).ToShortDateString();
                    break;
                case 9:
                    minDate.Text = "9/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = (System.DateTime.Today).ToShortDateString();
                    break;
                case 10:
                    minDate.Text = "10/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = (System.DateTime.Today).ToShortDateString();
                    break;
                case 11:
                    minDate.Text = "11/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = (System.DateTime.Today).ToShortDateString();
                    break;
                case 12:
                    minDate.Text = "12/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = (System.DateTime.Today).ToShortDateString();
                    break;
            }
        }
        private void ThisMonthYearAgo()
        {
            switch (System.DateTime.Today.Month)
            {
                case 1:
                    minDate.Text = "1/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "1/31/" + ((System.DateTime.Today.Year) - 1);
                    break;
                case 2:
                    if (System.DateTime.IsLeapYear((System.DateTime.Today.Year) - 1))
                    {
                        minDate.Text = "2/1/" + ((System.DateTime.Today.Year) - 1);
                        maxDate.Text = "2/29/" + ((System.DateTime.Today.Year) - 1);
                    }
                    else
                    {
                        minDate.Text = "2/1/" + ((System.DateTime.Today.Year) - 1);
                        maxDate.Text = "2/28/" + ((System.DateTime.Today.Year) - 1);
                    }
                    break;
                case 3:
                    minDate.Text = "3/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "3/31/" + ((System.DateTime.Today.Year) - 1);
                    break;
                case 4:
                    minDate.Text = "4/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "4/30/" + ((System.DateTime.Today.Year) - 1);
                    break;
                case 5:
                    minDate.Text = "5/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "5/31/" + ((System.DateTime.Today.Year) - 1);
                    break;
                case 6:
                    minDate.Text = "6/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "6/30/" + ((System.DateTime.Today.Year) - 1);
                    break;
                case 7:
                    minDate.Text = "7/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "7/31/" + ((System.DateTime.Today.Year) - 1);
                    break;
                case 8:
                    minDate.Text = "8/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "8/31/" + ((System.DateTime.Today.Year) - 1);
                    break;
                case 9:
                    minDate.Text = "9/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "9/30/" + ((System.DateTime.Today.Year) - 1);
                    break;
                case 10:
                    minDate.Text = "10/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "10/31/" + ((System.DateTime.Today.Year) - 1);
                    break;
                case 11:
                    minDate.Text = "11/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "11/30/" + ((System.DateTime.Today.Year) - 1);
                    break;
                case 12:
                    minDate.Text = "12/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "12/31/" + ((System.DateTime.Today.Year) - 1);
                    break;
            }
        }
        private void LastMonth()
        {
            switch (System.DateTime.Today.Month)
            {
                case 1:
                    minDate.Text = "12/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "12/31/" + ((System.DateTime.Today.Year) - 1);
                    break;
                case 2:
                    minDate.Text = "1/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "1/31/" + (System.DateTime.Today.Year);
                    break;
                case 3:
                    if (System.DateTime.IsLeapYear(System.DateTime.Today.Year))
                    {
                        minDate.Text = "2/1/" + (System.DateTime.Today.Year);
                        maxDate.Text = "2/29/" + (System.DateTime.Today.Year);
                    }
                    else
                    {
                        minDate.Text = "2/1/" + (System.DateTime.Today.Year);
                        maxDate.Text = "2/28/" + (System.DateTime.Today.Year);
                    }
                    break;
                case 4:
                    minDate.Text = "3/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "3/31/" + (System.DateTime.Today.Year);
                    break;
                case 5:
                    minDate.Text = "4/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "4/30/" + (System.DateTime.Today.Year);
                    break;
                case 6:
                    minDate.Text = "5/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "5/31/" + (System.DateTime.Today.Year);
                    break;
                case 7:
                    minDate.Text = "6/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "6/30/" + (System.DateTime.Today.Year);
                    break;
                case 8:
                    minDate.Text = "7/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "7/31/" + (System.DateTime.Today.Year);
                    break;
                case 9:
                    minDate.Text = "8/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "8/31/" + (System.DateTime.Today.Year);
                    break;
                case 10:
                    minDate.Text = "9/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "9/30/" + (System.DateTime.Today.Year);
                    break;
                case 11:
                    minDate.Text = "10/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "10/31/" + (System.DateTime.Today.Year);
                    break;
                case 12:
                    minDate.Text = "11/1/" + (System.DateTime.Today.Year);
                    maxDate.Text = "11/30/" + (System.DateTime.Today.Year);
                    break;
            }
        }
        private void LastMonthYearAgo()
        {
            switch (System.DateTime.Today.Month)
            {
                case 1:
                    minDate.Text = "12/1/" + ((System.DateTime.Today.Year) - 2);
                    maxDate.Text = "12/31/" + ((System.DateTime.Today.Year) - 2);
                    break;
                case 2:
                    minDate.Text = "1/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "1/31/" + ((System.DateTime.Today.Year) - 1);
                    break;
                case 3:
                    if (System.DateTime.IsLeapYear((System.DateTime.Today.Year) - 1))
                    {
                        minDate.Text = "2/1/" + ((System.DateTime.Today.Year) - 1);
                        maxDate.Text = "2/29/" + ((System.DateTime.Today.Year) - 1);
                    }
                    else
                    {
                        minDate.Text = "2/1/" + ((System.DateTime.Today.Year) - 1);
                        maxDate.Text = "2/28/" + ((System.DateTime.Today.Year) - 1);
                    }
                    break;
                case 4:
                    minDate.Text = "3/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "3/31/" + ((System.DateTime.Today.Year) - 1);
                    break;
                case 5:
                    minDate.Text = "4/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "4/30/" + ((System.DateTime.Today.Year) - 1);
                    break;
                case 6:
                    minDate.Text = "5/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "5/31/" + ((System.DateTime.Today.Year) - 1);
                    break;
                case 7:
                    minDate.Text = "6/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "6/30/" + ((System.DateTime.Today.Year) - 1);
                    break;
                case 8:
                    minDate.Text = "7/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "7/31/" + ((System.DateTime.Today.Year) - 1);
                    break;
                case 9:
                    minDate.Text = "8/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "8/31/" + ((System.DateTime.Today.Year) - 1);
                    break;
                case 10:
                    minDate.Text = "9/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "9/30/" + ((System.DateTime.Today.Year) - 1);
                    break;
                case 11:
                    minDate.Text = "10/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "10/31/" + ((System.DateTime.Today.Year) - 1);
                    break;
                case 12:
                    minDate.Text = "11/1/" + ((System.DateTime.Today.Year) - 1);
                    maxDate.Text = "11/30/" + ((System.DateTime.Today.Year) - 1);
                    break;
            }
        }
        #endregion
    }
}
