using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Web.UI.HtmlControls;
using Oracle.DataAccess.Client;
using System.Data;
using System.Drawing;
using System.Data.OleDb;
using System.Configuration;
using KeyCentral.Lookups;
using KeyCentral.Functions;

namespace KeyCentralLib
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:DynamicEntryBox runat=server></{0}:DynamicEntryBox>")]
    public class DynamicEntryBox : WebControl
    {
        #region declares
        private int postBackCount=0;
        private List<List<string>> values; //list of rows, containtng a list of vales

        protected System.Web.UI.HtmlControls.HtmlGenericControl mainDiv;

        protected string field0Header = "field0";
        protected string field1Header = "field1";
        protected int field0MaxLength = 13;
        protected int field1MaxLength = 13;
        protected int field0Width = 100;
        protected int field1Width = 100;
        protected int displayListSize = 4;
        protected string rowFocus = String.Empty;
        #endregion

        #region public Paramerters

        public List<List<string>> Values
        {
            get { return values; }
            set { values = value; }
        }

        public int Field0MaxLength
        {
            get { return field0MaxLength; }
            set { field0MaxLength = value; }
        }
        public int Field1MaxLength
        {
            get { return field1MaxLength; }
            set { field1MaxLength = value; }
        }

        public int Field0Width
        {
            get { return field0Width; }
            set { field0Width = value; }
        }
        public int Field1Width
        {
            get { return field1Width; }
            set { field1Width = value; }
        }

        public string Field0Header
        {
            get { return field0Header; }
            set { field0Header = value; }
        }
        public string Field1Header
        {
            get { return field1Header; }
            set { field1Header = value; }
        }

        public int DisplayListSize
        {
            get { return displayListSize; }
            set { displayListSize = value; }
        }

        #endregion
        private System.Web.SessionState.HttpSessionState Session { get { return Context.Session; } }

        #region Events
        protected override void OnLoad(EventArgs e)
        {
            CreateControls();
            //mainDiv.Controls.Clear();

            CreateTextBoxes(mainDiv.Controls);

            ProcessFocus();

            if (Page.IsPostBack == true)
                ViewState.Clear();
            
            base.OnLoad(e);

            return;

            /*
            criteria.Clear();

            if (Session[this.lookupSearch + ":Criteria"] != null)//check for Values to load
            {
                if (((ArrayList)Session[this.lookupSearch + ":Criteria"]).Count > 0)
                    criteria = (ArrayList)Session[this.lookupSearch + ":Criteria"];

                //We want to remove it even if it is empty
                Session.Remove(this.lookupSearch + ":Criteria");
            }
            else if (Session[this.lookupSearch + ":Lookup"] != null)//Check for LookupValues
                LoadFromLookup();
            else//Get user input
                ReadTextBoxes(mainDiv, criteria);

            if (Session[limitedToKeys] != null && ((ArrayList)Session[limitedToKeys]).Count > 0)
            {
                //If we don't have anything selected load it from their limitation
                if (criteria.Count == 0)
                    LoadFromLimit();
                else
                    CheckInputsVsLimit(criteria, (ArrayList)Session[limitedToKeys]);
            }
            if (Session[ValidKeys] != null && ((ArrayList)Session[ValidKeys]).Count > 0)
            {
                CheckInputsVsLimit(criteria, (ArrayList)Session[ValidKeys]);
            }

            mainDiv.Controls.Clear();
            CreateTextBoxes(mainDiv);

            SetMainDivAtributes();


            if (Page.IsPostBack == true)
                ViewState.Clear();

            WriteViewState(criteria);

          
             * */
        }

        private void ProcessFocus()
        {
            //Sets the focus 
            if (Page.IsPostBack == true)
            {
                if (rowFocus != "")
                    SetFocus(rowFocus);
                else
                    if (Page.Request["__EVENTTARGET"].IndexOf(this.UniqueID) > -1)
                            SetFocus(GetNextTextBoxID(Page.Request["__EVENTTARGET"]));
            }
        }

        protected override object SaveViewState()
        {
            return null;

        }
        protected override void LoadViewState(object savedState)
        {

        }
        protected override void OnInit(EventArgs e)
        {
            this.EnableViewState = false;

            //Keep track of postback counter...
            if (Page.IsPostBack)
                postBackCount = Int32.Parse(Page.Request[UniqueID + this.ClientIDSeparator + "PostBackCounter"]) + 1;

            values = new List<List<string>>();
            ReadTextBoxes(mainDiv);

 
            base.OnInit(e);
        }
        protected override void OnPreRender(System.EventArgs e)
        {

        }
        protected override void RenderContents(HtmlTextWriter output)
        {
            //this.RenderControl(output);
            mainDiv.RenderControl(output);
            //output.Write("Rendering DynamicCriteriabox");
        }
        #endregion

        #region private Helpers
        private void CreateControls()
        {
            mainDiv = new HtmlGenericControl();
            mainDiv.ID = this.UniqueID + this.ClientIDSeparator + "mainDiv";
            mainDiv.Style.Add("BORDER-RIGHT", "1px solid");
            mainDiv.Style.Add("BORDER-TOP", "1px solid");
            mainDiv.Style.Add("BORDER-LEFT", "1px solid");
            mainDiv.Style.Add("BORDER-BOTTOM", "1px solid");
            mainDiv.Style.Add("TABLE-LAYOUT", "fixed");
            mainDiv.Style.Add("OVERFLOW", "auto");
            mainDiv.Style.Add("WIDTH", "440px");
            mainDiv.Style.Add("BORDER-COLLAPSE", "separate");
            mainDiv.Style.Add("HEIGHT", "100px");
            mainDiv.EnableViewState = false;
            //this.Controls.Add(mainDiv);
        }


        /// <summary>
        /// Creates all the textBoxes for each rool
        /// </summary>
        /// <param name="parentDiv">Parent Control to add to</param>
        private void CreateTextBoxes(ControlCollection controls)
        {
            List<string> emptyRow = new List<string>();
            emptyRow.Add("");
            emptyRow.Add("");
            int rowIndex = 0;

            //Add headers
            AddLable(controls, field0Header, field0Width);
            AddLable(controls, field1Header, field1Width);
            controls.Add(new System.Web.UI.LiteralControl("<BR>\n"));

            foreach (List<string> rowValues in values)
                AddTextBox(controls, rowIndex++, rowValues);

            //Add the two empty TextBoxes
            AddTextBox(controls, rowIndex++, emptyRow);
            AddTextBox(controls, rowIndex++, emptyRow);

            //Add PostBackCounter hidden field
            HtmlInputHidden postBackCounter = new HtmlInputHidden();
            postBackCounter.ID = UniqueID + this.ClientIDSeparator + "PostBackCounter";
            postBackCounter.Value = (postBackCount).ToString();

            controls.Add(postBackCounter);


        }

        /// <summary>
        /// Addes the Lable to the collection
        /// </summary>
        /// <param name="controls">Control Collectino to add the Lable to</param>
        /// <param name="value">text for the Lable</param>
        /// <param name="rowValues">Width of the Lable</param>
        /// <returns></returns>
        private void AddLable(ControlCollection controls, string value, int width)
        {
            controls.Add(new System.Web.UI.LiteralControl("&nbsp;"));   
            Label lb = new Label();
            lb.Width = width;
            lb.Text = value;
            controls.Add(lb);
            controls.Add(new System.Web.UI.LiteralControl("\n"));   


            return;
        }

        /// <summary>
        /// Addes the textBoxes for this Row to the parrent control
        /// </summary>
        /// <param name="control">Parent control to add these to</param>
        /// <param name="controlIndex">row Index</param>
        /// <param name="rowValues">row Values</param>
        /// <returns></returns>
        private void AddTextBox(ControlCollection controls, int rowIndex, List<string> rowValues)
        {
            for (int x = 0; x < rowValues.Count; x++)
            {
                TextBox box = new TextBox();
                box.EnableViewState = false;

                box.Text = rowValues[x];
                box.ID = FieldName(rowIndex, x,false);// this.UniqueID + this.ClientIDSeparator + "Field" + this.ClientIDSeparator + x.ToString() + this.ClientIDSeparator + rowIndex.ToString();
                box.AutoPostBack = true;
                box.MaxLength = x==0? field0MaxLength:field1MaxLength;
                box.Width = x ==0 ? field0Width : field1Width;
                box.Attributes.Add("OnChange", "javascript:setTimeout('__doPostBack(\\'" + box.ID + "\\',\\'\\')', 0)");
                box.Attributes.Add("EnterAsTab", "");


                //Add the TextBox we just created
                controls.Add(new System.Web.UI.LiteralControl("\n"));
                controls.Add(box);
            }
            controls.Add(new System.Web.UI.LiteralControl("<BR>\n"));

            return;
        }

        /// <summary>
        /// Reads the data from the postback and fills in Values with all the values
        /// </summary>
        /// <param name="parentDiv"></param>
        private void ReadTextBoxes(HtmlGenericControl parentDiv)
        {
            for (int x = 0; Page.Request[FieldName(x,0,true)] != null; x++)//while the first field != null keep going
            {
                List<string> rowData = new List<string>();

                for (int y = 0; Page.Request[FieldName(x, y, true)] != null; y++)//while the field in this row isn't null keep going
                    rowData.Add(Page.Request[FieldName(x, y, true)]);

                //Add to the values List
                bool isEmpty = true;
                foreach (string str in rowData)
                    if (str.Trim() != "")
                        isEmpty = false;

                if (!isEmpty)
                    values.Add(rowData);
            }
        }

        private string FieldName(int row, int column, bool previous)
        {
            if (previous && postBackCount != 0)
                return UniqueID + this.ClientIDSeparator + "Field" + this.ClientIDSeparator.ToString() + (postBackCount - 1).ToString() + this.ClientIDSeparator + row.ToString() + this.ClientIDSeparator + column.ToString();
            else
                return UniqueID + this.ClientIDSeparator + "Field" + this.ClientIDSeparator.ToString() + this.postBackCount.ToString() + this.ClientIDSeparator + row.ToString() + this.ClientIDSeparator + column.ToString();
        }


        private void SetMainDivAtributes()
        {
            //Adjust the size depending on the size of the inner guys
            if ((values.Count + 2) > displayListSize)
                mainDiv.Style["WIDTH"] = (field0Width + field1Width + 30).ToString() + "px";
            else
                mainDiv.Style["WIDTH"] = (field0Width + field1Width + 20).ToString() + "px";

            mainDiv.Style["HEIGHT"] = (displayListSize * 25).ToString() + "px";
        }

        private string GetNextTextBoxID(string changingTextBox)
        {
            int fieldCount = 2;
            string[] split = changingTextBox.Split(this.ClientIDSeparator);

            int row = Int32.Parse(split[3]);
            int column = Int32.Parse(split[4]);

            if (column == (fieldCount - 1))
            {
                row++;
                column = 0;
            }
            else
                column++;

            return FieldName(row, column,false);

            return UniqueID + "_Input:" + (Int32.Parse(changingTextBox.Remove(0, changingTextBox.IndexOf("_Input:") + 7)) + 1).ToString();
        }
        #endregion

        #region private Javascript functions
        private void SetFocus(string ctrl)
        {
            string s = "<SCRIPT language=\"javascript\">" +
                "document.getElementById('" + ctrl + "').focus() </SCRIPT>";

            Page.ClientScript.RegisterStartupScript(this.GetType(),"SetFocus", s);
            //Page.RegisterStartupScript("focus", s);
        }
        #endregion
    }
}
