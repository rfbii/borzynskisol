﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Collections;
using KeyCentral.Functions;

namespace BankBalanceLib
{
	public class BankBalanceReportPDF
	{
		#region Vars
		private Document document;
		private PdfWriter PDFWriter;
		//private int lineCount = 0;
		//private int linesPerPage = 38;
		#endregion

		#region Constructor
		public BankBalanceReportPDF()
		{
		}
		#endregion

		#region Display Code
		public void Main(string saveFolderLocationStr, string fileNameStr, DataSet BankBalanceDataDts)
		{
			try
			{
				//if temp folder doesn't already exist on client's computer, create the folder
				DirectoryInfo directoryInfo = new DirectoryInfo(saveFolderLocationStr);

				if (!directoryInfo.Exists)
				{
					directoryInfo.Create();
				}

				document = new Document(PageSize.LETTER.Rotate(), 18, 18, 18, 18);

				// creation of the different writers
				PDFWriter = PdfWriter.GetInstance(document, new FileStream(saveFolderLocationStr + "\\" + fileNameStr, FileMode.Create));
				PDFWriter.ViewerPreferences = PdfWriter.PageModeUseOutlines;

				string ReportNameStr = "Bank Balance Report";

				#region Footer
				//HeaderFooter footer = new HeaderFooter(new Phrase("Page ", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)), true);
				//footer.Border = iTextSharp.text.Rectangle.NO_BORDER;
				//footer.Alignment = Element.ALIGN_CENTER;
				//document.Footer = footer;

				PDFCustomHeaderFooter pdfCustomHeaderFooter = new PDFCustomHeaderFooter(ReportNameStr);
				PDFWriter.PageEvent = pdfCustomHeaderFooter;
				#endregion

				document.Open();

				#region Header
				PdfPTable pdfPTable = Header(ReportNameStr);
				#endregion

				#region Detail
				BankBalanceDataDts.Tables[0].DefaultView.Sort = "DeptName, BankName, TN";

				double companyTotalLimit = 0;
				double companyTotalLoanBal = 0;
				double companyTotalLoanAvailable = 0;
				double companyTotalGLBalance = 0;
				double companyTotalLoan = 0;
				double companyTotalBalance = 0;

				double subLimit = 0;
				double subLoanBal = 0;
				double subLoanAvailable = 0;
				double subGLBalance = 0;
				double subLoan = 0;
				double subBalance = 0;

				double totalLimit = 0;
				double totalLoanBal = 0;
				double totalLoanAvailable = 0;
				double totalGLBalance = 0;
				double totalLoan = 0;
				double totalBalance = 0;

				string lastCompany = "";
				string lastBank = "";
				bool companyChanged = true;
				bool BankChanged = false;

				for (int i = 0; i < BankBalanceDataDts.Tables[0].DefaultView.Count; i++)
				{
					DataRowView dataRow = (DataRowView)BankBalanceDataDts.Tables[0].DefaultView[i];

					if (lastCompany != dataRow["Deptname"].ToString())
					{
						companyChanged = true;
					}
					if (lastBank != (dataRow["BankName"].ToString() + dataRow["Deptname"].ToString()))
					{
						if (lastBank == "")
						{
							lastBank = (dataRow["BankName"].ToString() + dataRow["Deptname"].ToString());
						}
						else
						{
							lastBank = (dataRow["BankName"].ToString() + dataRow["Deptname"].ToString());
							BankChanged = true;
						}
					}

					#region Subtotal
					if (BankChanged && lastBank != "")
					{
						PdfPCell detailCell = new PdfPCell(new Phrase("Subtotal", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
						detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthLeft = 2;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
						detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(subLimit), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(subLimit))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(subLoanBal), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(subLoanBal))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(subLoanAvailable), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(subLoanAvailable))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(subGLBalance), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(subGLBalance))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(subLoan), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(subLoan))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(subBalance), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(subBalance))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthRight = 2;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						BankChanged = false;
						companyTotalLimit += subLimit;
						companyTotalLoanBal += subLoanBal;
						companyTotalLoanAvailable += subLoanAvailable;
						companyTotalGLBalance += subGLBalance;
						companyTotalLoan += subLoan;
						companyTotalBalance += subBalance;
						subLimit = 0;
						subLoanBal = 0;
						subLoanAvailable = 0;
						subGLBalance = 0;
						subLoan = 0;
						subBalance = 0;
					}
					#endregion

					#region Company Total
					if (companyChanged && lastCompany != "")
					{
						PdfPCell detailCell = new PdfPCell(new Phrase("Total", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
						detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthLeft = 2;
						detailCell.BorderWidthBottom = 2;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
						detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthBottom = 2;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(companyTotalLimit), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(companyTotalLimit))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthBottom = 2;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(companyTotalLoanBal), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(companyTotalLoanBal))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthBottom = 2;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(companyTotalLoanAvailable), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(companyTotalLoanAvailable))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthBottom = 2;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(companyTotalGLBalance), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(companyTotalGLBalance))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthBottom = 2;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(companyTotalLoan), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(companyTotalLoan))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthBottom = 2;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(companyTotalBalance), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(companyTotalBalance))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthBottom = 2;
						detailCell.BorderWidthRight = 2;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						totalLimit += companyTotalLimit;
						totalLoanBal += companyTotalLoanBal;
						totalLoanAvailable += companyTotalLoanAvailable;
						totalGLBalance += companyTotalGLBalance;
						totalLoan += companyTotalLoan;
						totalBalance += companyTotalBalance;
						companyTotalLimit = 0;
						companyTotalLoanBal = 0;
						companyTotalLoanAvailable = 0;
						companyTotalGLBalance = 0;
						companyTotalLoan = 0;
						companyTotalBalance = 0;
					}
					#endregion

					#region Company Header
					if (companyChanged)
					{
						PdfPCell detailCell = new PdfPCell(new Phrase(dataRow["Deptname"].ToString(), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
						detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthLeft = 2;
						detailCell.BorderWidthRight = 0;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
						detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthLeft = 0;
						detailCell.BorderWidthRight = 0;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthLeft = 0;
						detailCell.BorderWidthRight = 0;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthLeft = 0;
						detailCell.BorderWidthRight = 0;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthLeft = 0;
						detailCell.BorderWidthRight = 0;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthLeft = 0;
						detailCell.BorderWidthRight = 0;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthLeft = 0;
						detailCell.BorderWidthRight = 0;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthLeft = 0;
						detailCell.BorderWidthRight = 2;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						companyChanged = false;
						lastCompany = dataRow["Deptname"].ToString();
					}
					#endregion

					#region Detail
					double availableBalance = 0;
					double detailTotalLoan = 0;
					double balance = 0;

					if (dataRow["LoanLimit"].ToString() != "0" || dataRow["LoanBalance"].ToString() != "0")
					{
						availableBalance = (double.Parse(dataRow["LoanLimit"].ToString()) - double.Parse(dataRow["LoanBalance"].ToString()));
						detailTotalLoan = (double.Parse(dataRow["LoanBalance"].ToString()) - double.Parse(dataRow["GLBalance"].ToString()));
						balance = (double.Parse(dataRow["LoanLimit"].ToString()) - detailTotalLoan);
					}
					else
					{
						balance = double.Parse(dataRow["GLBalance"].ToString());
					}

					if (dataRow["TN"].ToString().ToString() == "0")
					{
						PdfPCell detailCell = new PdfPCell(new Phrase(dataRow["BankName"].ToString().PadRight(28).Substring(0, 28), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
						detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthLeft = 2;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
						detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(double.Parse(dataRow["LoanLimit"].ToString())), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, GetColor(double.Parse(dataRow["LoanLimit"].ToString())))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(double.Parse(dataRow["LoanBalance"].ToString())), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, GetColor(double.Parse(dataRow["LoanBalance"].ToString())))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(availableBalance), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, GetColor(availableBalance))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(double.Parse(dataRow["GLBalance"].ToString())), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, GetColor(double.Parse(dataRow["GLBalance"].ToString())))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(detailTotalLoan), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, GetColor(detailTotalLoan))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(balance), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, GetColor(balance))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthRight = 2;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);
					}
					else
					{
						PdfPCell detailCell = new PdfPCell(new Phrase(dataRow["BankName"].ToString().PadRight(28).Substring(0, 28), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
						detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthLeft = 2;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(dataRow["TN"].ToString(), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
						detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(double.Parse(dataRow["LoanLimit"].ToString())), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, GetColor(double.Parse(dataRow["LoanLimit"].ToString())))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(double.Parse(dataRow["LoanBalance"].ToString())), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, GetColor(double.Parse(dataRow["LoanBalance"].ToString())))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(availableBalance), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, GetColor(availableBalance))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(double.Parse(dataRow["GLBalance"].ToString())), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, GetColor(double.Parse(dataRow["GLBalance"].ToString())))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(detailTotalLoan), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, GetColor(detailTotalLoan))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);

						detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(balance), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, GetColor(balance))));
						detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
						detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
						detailCell.NoWrap = true;
						detailCell.BorderWidthRight = 2;
						pdfPTable.DefaultCell.Padding = 0;
						pdfPTable.AddCell(detailCell);
					}

					subLimit += double.Parse(dataRow["LoanLimit"].ToString());
					subLoanBal += double.Parse(dataRow["LoanBalance"].ToString());
					subLoanAvailable += availableBalance;
					subGLBalance += double.Parse(dataRow["GLBalance"].ToString());
					subLoan += detailTotalLoan;
					subBalance += balance;
					#endregion
				}

				#region Subtotal
				if (true)
				{
					PdfPCell detailCell = new PdfPCell(new Phrase("Subtotal", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
					detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					detailCell.BorderWidthLeft = 2;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
					detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(subLimit), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(subLimit))));
					detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(subLoanBal), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(subLoanBal))));
					detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(subLoanAvailable), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(subLoanAvailable))));
					detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(subGLBalance), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(subGLBalance))));
					detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(subLoan), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(subLoan))));
					detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(subBalance), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(subBalance))));
					detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					detailCell.BorderWidthRight = 2;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					BankChanged = false;
					companyTotalLimit += subLimit;
					companyTotalLoanBal += subLoanBal;
					companyTotalLoanAvailable += subLoanAvailable;
					companyTotalGLBalance += subGLBalance;
					companyTotalLoan += subLoan;
					companyTotalBalance += subBalance;
					subLimit = 0;
					subLoanBal = 0;
					subLoanAvailable = 0;
					subGLBalance = 0;
					subLoan = 0;
					subBalance = 0;
				}
				#endregion

				#region Company Total
				if (true)
				{
					PdfPCell detailCell = new PdfPCell(new Phrase("Total", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
					detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					detailCell.BorderWidthLeft = 2;
					detailCell.BorderWidthBottom = 2;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
					detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					detailCell.BorderWidthBottom = 2;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(companyTotalLimit), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(companyTotalLimit))));
					detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					detailCell.BorderWidthBottom = 2;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(companyTotalLoanBal), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(companyTotalLoanBal))));
					detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					detailCell.BorderWidthBottom = 2;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(companyTotalLoanAvailable), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(companyTotalLoanAvailable))));
					detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					detailCell.BorderWidthBottom = 2;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(companyTotalGLBalance), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(companyTotalGLBalance))));
					detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					detailCell.BorderWidthBottom = 2;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(companyTotalLoan), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(companyTotalLoan))));
					detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					detailCell.BorderWidthBottom = 2;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(companyTotalBalance), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(companyTotalBalance))));
					detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					detailCell.BorderWidthBottom = 2;
					detailCell.BorderWidthRight = 2;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					totalLimit += companyTotalLimit;
					totalLoanBal += companyTotalLoanBal;
					totalLoanAvailable += companyTotalLoanAvailable;
					totalGLBalance += companyTotalGLBalance;
					totalLoan += companyTotalLoan;
					totalBalance += companyTotalBalance;
					companyTotalLimit = 0;
					companyTotalLoanBal = 0;
					companyTotalLoanAvailable = 0;
					companyTotalGLBalance = 0;
					companyTotalLoan = 0;
					companyTotalBalance = 0;
				}
				#endregion

				#region Grand Total
				if (true)
				{
					PdfPCell detailCell = new PdfPCell(new Phrase("Grand Total", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
					detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					detailCell.BorderWidthBottom = 2;
					detailCell.BorderWidthLeft = 2;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
					detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					detailCell.BorderWidthBottom = 2;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(totalLimit), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(totalLimit))));
					detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					detailCell.BorderWidthBottom = 2;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(totalLoanBal), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(totalLoanBal))));
					detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					detailCell.BorderWidthBottom = 2;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(totalLoanAvailable), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(totalLoanAvailable))));
					detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					detailCell.BorderWidthBottom = 2;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(totalGLBalance), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(totalGLBalance))));
					detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					detailCell.BorderWidthBottom = 2;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(totalLoan), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(totalLoan))));
					detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					detailCell.BorderWidthBottom = 2;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);

					detailCell = new PdfPCell(new Phrase(RemoveZeroCurrency(totalBalance), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, GetColor(totalBalance))));
					detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					detailCell.NoWrap = true;
					detailCell.BorderWidthBottom = 2;
					detailCell.BorderWidthRight = 2;
					pdfPTable.DefaultCell.Padding = 0;
					pdfPTable.AddCell(detailCell);
				}
				#endregion
				#endregion

				document.Add(pdfPTable);
			}
			catch (Exception ex)
			{
			
			}
			finally
			{
				if (document != null && document.IsOpen())
				{
					// we close the document
					document.Close();
				}
			}
		}
		private PdfPTable Header(string ReportNameStr)
		{
			#region Title
			Paragraph paragraph = new Paragraph();
			paragraph.Alignment = Element.ALIGN_CENTER;
			paragraph.Font = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD);
			paragraph.Add(ReportNameStr);
			paragraph.SpacingAfter = 2f;
			paragraph.SpacingBefore = 0f;
			paragraph.Leading = 10f;
			document.Add(paragraph);
			#endregion

			#region Header
			PdfPTable pdfPTable = new PdfPTable(8);

			float tableWidth = 0;

			float companyWidth = 250f;
			float transferNumberWidth = 20f;
			float loanLimitWidth = 70f;
			float loanBalanceWidth = 70f;
			float availableBalanceWidth = 70f;
			float glBalanceWidth = 70f;
			float totalLoanWidth = 70f;
			float balanceAvailableWidth = 70f;

			tableWidth = companyWidth + transferNumberWidth + loanLimitWidth + loanBalanceWidth + availableBalanceWidth + glBalanceWidth + totalLoanWidth + balanceAvailableWidth;

			pdfPTable.SetWidths(new float[] { companyWidth, transferNumberWidth, loanLimitWidth, loanBalanceWidth, availableBalanceWidth, glBalanceWidth, totalLoanWidth, balanceAvailableWidth });
			pdfPTable.TotalWidth = tableWidth;
			//table.KeepTogether = true;
			pdfPTable.SplitRows = true;
			pdfPTable.SplitLate = false;
			pdfPTable.LockedWidth = true;
			pdfPTable.DefaultCell.Padding = 0;
			//pdfPTable.WidthPercentage = 100; // percentage
			pdfPTable.DefaultCell.BorderWidth = 1f;
			pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
			pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

			PdfPCell detailCell = new PdfPCell(new Phrase("Company", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
			pdfPTable.DefaultCell.Padding = 0;
			pdfPTable.AddCell(detailCell);

			detailCell = new PdfPCell(new Phrase("TN", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
			pdfPTable.DefaultCell.Padding = 0;
			pdfPTable.AddCell(detailCell);

			detailCell = new PdfPCell(new Phrase("Loan Limit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
			pdfPTable.DefaultCell.Padding = 0;
			pdfPTable.AddCell(detailCell);

			detailCell = new PdfPCell(new Phrase("Loan Balance", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
			pdfPTable.DefaultCell.Padding = 0;
			pdfPTable.AddCell(detailCell);

			detailCell = new PdfPCell(new Phrase("Available Balance", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
			pdfPTable.DefaultCell.Padding = 0;
			pdfPTable.AddCell(detailCell);

			detailCell = new PdfPCell(new Phrase("G/L Balance", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
			pdfPTable.DefaultCell.Padding = 0;
			pdfPTable.AddCell(detailCell);

			detailCell = new PdfPCell(new Phrase("Total Loan", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
			pdfPTable.DefaultCell.Padding = 0;
			pdfPTable.AddCell(detailCell);

			detailCell = new PdfPCell(new Phrase("Balance Available", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
			pdfPTable.DefaultCell.Padding = 0;
			pdfPTable.AddCell(detailCell);

			pdfPTable.HeaderRows = 1; // this is the end of the table header
			#endregion

			return pdfPTable;
		}

		#region Helper Methods
		public string RemoveZero(string str)
		{
			if (str == "0")
				return "";

			return str;
		}
		public string RemoveZero(double input)
		{
			if (input == 0)
				return "";

			return DisplayFormat(input);
		}
		public string RemoveZero1(double input)
		{
			if (input == 0)
				return "";

			return DisplayFormat1(input);
		}
		private string RemoveZero2(double input)
		{
			if (input == 0)
				return "";

			return DisplayFormat2(input);
		}
		private string RemoveZeroCurrency(double input)
		{
			if (input == 0)
				return "";

			return input.ToString("C");
		}
		public string ReplaceWithZero(string str)
		{
			if (str == "")
				return "0";

			return str;
		}
		public string ReplaceWithZero(double input)
		{
			if (input == 0)
				return "";

			return input.ToString();
		}
		public string DisplayFormat(double value)
		{
			if (value == 0)
				return value.ToString();
			else
				return Misc.Round(value, 2).ToString("###,##0");
		}
		public string DisplayFormat1(double value)
		{
			if (value == 0)
				return value.ToString();
			else
				return Misc.Round(value, 2).ToString("###,##0.0");
		}
		public string DisplayFormat2(double value)
		{
			if (value == 0)
				return value.ToString();
			else
				return Misc.Round(value, 2).ToString("###,##0.00");
		}
		public string DisplayFormat2a(double value)
		{
			if (value == 0)
				return value.ToString();
			else
				return Misc.Round(value, 2).ToString("###,##0.##");
		}
		public string DisplayFormat3(double value)
		{
			if (value == 0)
				return value.ToString();
			else
				return Misc.Round(value, 3).ToString("###,##0.0##");
		}
		private Color GetColor(double value)
		{
			Color color = Color.BLACK;

			if (value < 0)
			{
				color = Color.RED;
			}

			return color;
		}
		#endregion
		#endregion
	}
}
