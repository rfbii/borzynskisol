using System;
using System.Collections.Generic;
using System.Text;
using KeyCentral.Functions;

namespace BankBalanceLib
{
	[MenuMap(Security = "Admin", Description = "Input Bank Account Type", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Bank", SortOrder = 10, Url = "../../../BankBalanceABZ/UI/Inputs/InputAccountType.aspx")]
	[MenuMap(Security = "Admin", Description = "Edit Bank Account", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Bank", SortOrder = 10, Url = "../../../BankBalanceABZ/UI/Inputs/InputBankAccount.aspx")]
	[MenuMap(Security = "Bank Balance Report", Description = "Bank Balance", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Bank", SortOrder = 10, Url = "../../../BankBalanceABZ/UI/Reports/CriteriaBankBalance.aspx?ReportName=BankBalanceReport")]
	[MenuMap(Security = "Check Writing Number", Description = "Check Writing Number", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Bank", SortOrder = 10, Url = "../../../BankBalanceABZ/UI/Inputs/InputCheckWritingUser.aspx?ReportName=CheckWritingUser&&FirstTime=True")]
	[MenuMap(Security = "", Description = "Bank", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "", SortOrder = 5, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Bank")]

    class Menumap
    {
    }
}
