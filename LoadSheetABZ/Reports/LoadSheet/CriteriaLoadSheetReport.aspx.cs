using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KeyCentral.Functions;

namespace LoadSheetABZ.Reports.LoadSheet
{
	/// <summary>
    /// Summary description for CriteriaLoadSheetReport.
	/// </summary>
	/// 

    public partial class CriteriaLoadSheetReport : BasePage
    {
        #region Declares
        protected ReportSessionManager reportSessionManager;
        #endregion

        #region Events
        protected void Page_Load(object sender, System.EventArgs e)
		{
            if (CheckSecurity("Load Sheet Report") == false)
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/MenuMain.aspx");

			#region SessionManager
			reportSessionManager = new ReportSessionManager(this,Request["ReportName"].ToString());
			
			//Load Vars from session if this report is in SessionManager
			if(IsPostBack ==false && reportSessionManager.CheckReportVars())
			{
				orderNumber.Text  = (string)Session["OrderNumber"];
			}
		
			#endregion

			if(IsPostBack ==false)
			{
				Message.Text ="";

				//Check for returned error messages
				if (Request.QueryString["Records"]== "None")
					Message.Text = "No Records Found.";
			}
		}
        protected void cmdReport_Click(object sender, System.EventArgs e)
        {
            {
                //Add Report Vars
                reportSessionManager.AddSession("OrderNumber", orderNumber.Text);

                //Let the reportSessionManager know that we are done
                reportSessionManager.SaveSessions();

                //Build next page string and redirect to it
                Response.Redirect("ReportLoadSheet.aspx" + "?LastPage=" + Misc.EncodeQueryString(Request.Path + "?ReportName=" + Request.QueryString["ReportName"]));
            }
        }
        protected void cmdMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Freight");
        }
        #endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
        #endregion
	}
}
