<%@ Page language="c#" Inherits="LoadSheetABZ.Reports.LoadSheet.ReportLoadSheetInline" Codebehind="ReportLoadSheetInline.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
			<!-- Email Start -->
		<title>Load Sheet Report</title>
		<style type="text/css">.ReportTable.TD { FONT-SIZE: 8pt }
	.Title { FONT-WEIGHT: bold; FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: Arial }
	.Header { FONT-WEIGHT: bold; FONT-SIZE: 12pt; COLOR: black; FONT-FAMILY: Arial; BORDER-Top: black 1px solid; }
	.Title1 {  FONT-SIZE: 11pt; FONT-FAMILY: Arial }
	.Title2 {  FONT-WEIGHT: bold; FONT-SIZE: 14pt; FONT-FAMILY: Arial }
	.Title3 { FONT-SIZE: 11pt; BORDER-BOTTOM: lightgrey 1px solid; FONT-FAMILY: Arial } 
	.Title4 { FONT-WEIGHT: bold; FONT-SIZE: 11pt; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Arial }
	.Title5 { FONT-WEIGHT: bold; FONT-SIZE: 11pt; BORDER-Top: black 1px solid; FONT-FAMILY: Arial }
	.Title6 { FONT-SIZE: 11pt; BORDER-Top: black 1px solid; FONT-FAMILY: Arial }
		</style>
				<!-- Email End -->
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C#" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<link href="/KeystoneCustomerStatus/Inc/_styles.css" type="text/css" rel="stylesheet"/>
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
		</style>-->
        <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body bgcolor="#ffffff" onload="SetupPrinting();">
		<object id="factory" style="DISPLAY: none" codebase="http://www.meadroid.com/scriptx/ScriptX.cab#Version=6,1,431,2"
			classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814" viewastext="">
		</object>
		<script type="text/javascript" language="javascript">
		function SetupPrinting()
		{
			factory.printing.footer = "Printed: &d &t&bPage &p of &P&bLoad Sheet Report";
			factory.printing.header = "";
			factory.printing.leftMargin = .5;
			factory.printing.rightMargin = .5;
			factory.printing.topMargin = .5;
			factory.printing.bottomMargin = .5;
		}  
		</script>
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div id="BodyDiv" class="scroll">
			<!-- Email Start -->
				<table cellspacing="0" cellpadding="0" border="0" width="720" class="ReportTable">
					<asp:repeater id="Repeater1" runat="server">
						<HeaderTemplate>
							<%# Header()%>
							<%# Detail()%>
						    <%# Footer()%>
							<%# Header2()%>
							<%# Detail2()%>
						    <%# Footer2()%>
						</HeaderTemplate>
					</asp:repeater>
				</table>
				<!-- Email End -->
			</div>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
		</form>
	</body>
</html>