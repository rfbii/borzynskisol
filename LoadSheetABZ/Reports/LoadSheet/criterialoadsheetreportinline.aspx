<%@ Page language="c#" Inherits="LoadSheetABZ.Reports.LoadSheet.CriteriaLoadSheetReportInline" Codebehind="CriteriaLoadSheetReportInline.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Criteria Load Sheet Report</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="C#" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
        <!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
		</style>-->
        <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
				<table width="720" cellpadding="0" cellspacing="0" border="0">
					<tr height="20"><td>&nbsp;</td></tr>
					<tr>
						<td align="center">Criteria Load Sheet Report Inline</td>
					</tr>
					<tr height="40"><td>&nbsp;</td></tr>
				</table>
				<table width="720" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td align="center"><asp:Label runat="server" id="Label1">Order Number:&nbsp;</asp:Label><asp:TextBox ID="orderNumber" EnterAsTab="" MaxLength="6" runat="server"></asp:TextBox></td>
					</tr>
				</table>
				<table width="720" cellpadding="0" cellspacing="0" border="0">
					<tr height="20"><td>&nbsp;</td></tr>
					<tr>
						<td width="360" align="center"><asp:Button id="cmdReport" width="175" runat="server" TabFinish="" EnterAsTab="" Text="Run Report" onclick="cmdReport_Click"></asp:Button></td>
						<td  width="360" align="center"><asp:button id="cmdMenu" width="175" runat="server" Text="Return to Freight Menu" OnClick="cmdMenu_Click"></asp:button></td>
					</tr>
					<tr>
						<td height="20">&nbsp;</td>
					</tr>
				</table>
				<asp:Label id="Message" runat="server" ForeColor="Red">Message</asp:Label>
			</div>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
		</form>
	</body>
</html>