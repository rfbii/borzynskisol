using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Text;
using KeyCentral.Functions;

namespace LoadSheetABZ.Reports.LoadSheet
{
	public partial class ReportLoadSheetInline : BasePage
	{
		#region Param Properties
		private string orderNumber { get { return Session["OrderNumber"].ToString(); } }
		#endregion

		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Load Sheet Report") == false)
				Response.Redirect("../../../KeyCentralABZ/UI/Menu/MenuMain.aspx");

			if (!IsPostBack)
			{
				JavaStatusMessage status = new JavaStatusMessage(this);
				status.ShowMessage("Computing");

				ArrayList WarehousesArl = FillWarehousesArl();
				Session.Add("WarehousesArl", WarehousesArl);

				SortedList WarehouseOrderStl = LoadWarehousesLtbOrderFromSQL();
				Session.Add("WarehouseOrderStl", WarehouseOrderStl);

				DataSet dsRet = new DataSet();
				dsRet = GetReportData(orderNumber.Trim());

				if (dsRet.Tables[0].DefaultView.Count == 0)
				{
					JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"]) + "&&Records=None");
				}
				//else if (GetIfOrderIsInvoiced(orderNumber.Trim()))
				//{
				//    Response.Write("<script>alert('This order has already been invoiced.');</script>");
				//    JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"]));
				//}
				else if (WarehouseOrderStl.Count == 0)
				{
					Response.Write("<script>alert('Please go to Freight/Input Load Sheet and enter\\r\\na Warehouse Load Order for this Order Number.');</script>");
					JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"]));
				}
				else
				{
					this.Repeater1.DataSource = dsRet.Tables[0].DefaultView;
					Repeater1.DataBind();
				}
			}
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}

		private void InitializeComponent()
		{

		}
		#endregion

		#region Display Code
		public string Header()
		{
			string header = "";

			if (((SortedList)Session["WarehouseOrderStl"]).Count != 1)
			{
				header = @"
				    <table cellspacing='0' cellpadding='0' border='0' width='720' class='ReportTable'>
					    <tr>
						    <td align='center' width='720' class='Title2'>Pick Up Report</td>
					    </tr>
				    </table>
					<table border='0' cellPadding='0' cellspacing='0'>
						<tr>
							<td class='Title1' width='340' height='30' align='right' valign='center'>For:&nbsp;&nbsp;</td>
						    <td class='Title2' width='380' height='30' align='left' valign='center'>" + orderNumber.Substring(0, 6) + @"</td>
                        </tr>
                        <tr>
						    <td colspan='2' align='center' valign='center' width='720' class='Title2'>First</td>
                        </tr>
				        <tr>
					        <td colspan='2' width=720 align=center height=10 ></td>
				        </tr>
					</table>";
			}

			return header;
		}
		public string Detail()
		{
			string detail = "";
			bool printWarehouseBorderSpaceBln = false;
			DataView tempDtv = (DataView)this.Repeater1.DataSource;
			SortedList WarehouseOrderStl = (SortedList)Session["WarehouseOrderStl"];

			if (WarehouseOrderStl.Count != 1)
			{
				for (int i = 0; i < WarehouseOrderStl.Count; i++)
				{
					ListItem tempListItem = (ListItem)WarehouseOrderStl[i];
					DataView tempWarehouseDtv = new DataView(tempDtv.ToTable(), "WarehouseIdx = '" + tempListItem.Value + "'", "SoNo DESC, LineSeq", DataViewRowState.CurrentRows);
					ArrayList dropsArl = new ArrayList();

					detail += @"
                <table border='0' cellPadding='0' cellspacing='0'>";

					if (printWarehouseBorderSpaceBln)
					{
						detail += @"
                        <tr>
	                        <td colspan='3' width=720 align=center height=35 ></td>
                        </tr>";
					}

					detail += @"		
                    <tr>
                        <td class='Header' width=100 align=left>>>>>>>>>>></td>
					    <td class='Header' width=520 align=center>" + tempListItem.Text + @"</td>
                        <td class='Header' width=100 align=right><<<<<<<<<<</td>
				    </tr>
			    </table>";

					if (Misc.ParseTildaString(tempListItem.Value, 0) == "2")
					{
						detail += @"
                                    <table border='0' cellPadding='0' cellspacing='0'>
					                    <td class='Title1' width=720 align=center>" + ((DataRowView)tempWarehouseDtv[0])["CommString"].ToString() + @"</td>
					                </table>";

						if (((DataRowView)tempWarehouseDtv[0])["Ref"].ToString() != "")
						{
							detail += @"
                                    <table border='0' cellPadding='0' cellspacing='0'>
					                    <td class='Title1' width=720 align=center>Pick&nbsp;Up&nbsp;Number:&nbsp;" + ((DataRowView)tempWarehouseDtv[0])["Ref"].ToString() + @"</td>
					                </table>";
						}
					}

					//Getting a list of SoNos for this Warehouse.
					for (int j = 0; j < tempWarehouseDtv.Count; j++)
					{
						DataRowView temp2Drv = tempWarehouseDtv[j];

						if (!dropsArl.Contains(temp2Drv["SoNo"].ToString()))
							dropsArl.Add(temp2Drv["SoNo"].ToString());
					}

					for (int j = 0; j < dropsArl.Count; j++)
					{
						bool headerPrintedBln = false;
						string tempSoNoStr = (string)dropsArl[j];
						DataView tempDropsDtv = new DataView(tempWarehouseDtv.ToTable(), "SoNo = '" + tempSoNoStr + "'", "SoNo DESC, LineSeq", DataViewRowState.CurrentRows);

						for (int k = 0; k < tempDropsDtv.Count; k++)
						{
							DataRowView tempDrv = tempDropsDtv[k];

							if (!headerPrintedBln)
							{
								double PalletsDbl = 0;

								for (int l = 0; l < tempDropsDtv.Count; l++)
								{
									DataRowView temp2Drv = tempDropsDtv[l];

									if (temp2Drv["UnitsPerPallet"].ToString() != "")
										PalletsDbl += Convert.ToDouble(temp2Drv["OrderQnt"]) / Convert.ToDouble(temp2Drv["UnitsPerPallet"]);
								}

								detail += @"
                                <table border='0' cellPadding='0' cellspacing='0'>
                                    <tr>
                                        <td width=720 align=center height=10 ></td>
                                    </tr>
					            </table>
					            <table border='0' cellPadding='0' cellspacing='0'>
						            <tr>
							            <td class='Title1' width=70 height=21 align=right valign=top>Customer:&nbsp;</td>
							            <td class='Title' width=235 height=21 align=left>" + tempDrv["tocust"].ToString().Replace("\r\n", "<br>") + @"</td>
							            <td class='Title1' width=65 height=21 align=right valign=top>Order:&nbsp;<br>Cust PO:&nbsp;<br>Pallets:&nbsp;</td>
							            <td class='Title' width=155 height=21 align=left valign=top>" + tempDrv["SoNo"].ToString() + "<br>" + tempDrv["custPoRef"].ToString() + "<br>" + Misc.DisplayFormat2(PalletsDbl) + @"</td>
							            <td class='Title1' width=95 height=21 align=right valign=top>Delivery Date:&nbsp;<br>Ship Date:&nbsp;</td>
							            <td class='Title' width=100 height=21 align=left valign=top>" + Misc.FormatDateTime(tempDrv["ToEtaDateTime"].ToString()) + @"<br>" + Misc.FormatDateTime(tempDrv["SoDateTime"].ToString()) + @"</td>
                                    </tr>
					            </table>
                                <table border='0' cellPadding='0' cellspacing='0'>
							        <tr>
								        <td class='Title3' width=80 height=21 align=right>Quantity</td>
								        <td class='Title3' width=50 height=21 align=right>&nbsp;</td>
								        <td class='Title3' width=375 height=21 align=center>Description</td>
								        <td class='Title3' width=215 height=21 align=center>Warehouse&nbsp;Location&nbsp;&nbsp;</td>
							        </tr>
						        </table>";

								headerPrintedBln = true;
							}

							detail += @"
						<table border='0' cellPadding='0' cellspacing='0'>
							<tr>
								<td class='Title4' width=80 height=21 align=right>" + tempDrv["OrderQnt"].ToString() + @"</td>
								<td class='Title4' width=50 height=21 align=right>&nbsp;</td>
								<td class='Title4' width=375 height=21 align=left>" + StringFormater.FormatString(tempDrv["Name"].ToString(), 30) + @"</td>
								<td class='Title4' width=215 height=21 align=left>" + StringFormater.FormatString(tempDrv["descr"].ToString(), 20) + @"</td>
							</tr>
						</table>";
						}
					}

					printWarehouseBorderSpaceBln = true;
				}
			}

			return detail;
		}
		public string Footer()
		{
			string total = "";

			if (((SortedList)Session["WarehouseOrderStl"]).Count != 1)
			{
				total = @"
				<table border='0' cellPadding='0' cellspacing='0'>
                    <tr>
					    <td align='center' height='45' valign='center' width='720' class='Title2'>Last</td>
                    </tr>
				</table>
				<table border='0' cellPadding='0' cellspacing='0'>
                    <tr>
                        <!-- email end -->
                        <div style='page-break-before:always'><span style='visibility: hidden;'><font style='font-size:0pt;'>&nbsp;</font></span></div>
                        <!-- email start -->
                    </tr>
                </table>";
			}

			return total;
		}
		public string Header2()
		{
			DataView tempDtv = (DataView)this.Repeater1.DataSource;
			DataRowView tempCarrierDrv = tempDtv[0];
			SortedList tempDropsStl = new SortedList();
			double PalletsDbl = 0;
			string header = "";
            string orderNumberStr = orderNumber;
            string dropNumberStr = "1";
            if (orderNumberStr.Length > 6)
            {
                orderNumberStr = orderNumber.Substring(0, 6);
            }
            header += @"
			    <table cellspacing='0' cellpadding='0' border='0' width='720' class='ReportTable'>
				    <tr>
					    <td align='center' width='720' class='Title2'>Load Sheet Report Inline</td>
				    </tr>
			    </table>
				<table border='0' cellPadding='0' cellspacing='0'>
					<tr>
						<td class='Title1' width='340' height='30' align='right' valign='center'>For:&nbsp;&nbsp;</td>
					    <td class='Title2' width='380' height='30' align='left' valign='center'>" + orderNumberStr + @"</td>
                    </tr>
                </table>";

			for (int i = 0; i < tempDtv.Count; i++)
			{
				DataRowView tempLoadSheetDrv = tempDtv[i];

				if (tempLoadSheetDrv["UnitsPerPallet"].ToString() != "")
					PalletsDbl += Convert.ToDouble(tempLoadSheetDrv["OrderQnt"]) / Convert.ToDouble(tempLoadSheetDrv["UnitsPerPallet"]);

				if (!tempDropsStl.Contains(tempLoadSheetDrv["SoNo"].ToString()))
					tempDropsStl.Add(tempLoadSheetDrv["SoNo"].ToString(), tempLoadSheetDrv["tocust"].ToString().Substring(0, tempLoadSheetDrv["tocust"].ToString().IndexOf("\r\n")));
			}

			header += @"
                    <table border='0' cellPadding='0' cellspacing='0'>";

			for (int i = tempDropsStl.Count - 1; i >= 0; i--)
			{
                if (tempDropsStl.GetKey(i).ToString().Length > 6)
                {
                   dropNumberStr = tempDropsStl.GetKey(i).ToString().Substring(7, 1);
                }
                header += @"
			            <tr>
                            <td align='right' valign='center' class='Title1'>Drop&nbsp;" + dropNumberStr + @":&nbsp;</td>
                            <td align='left' valign='center' class='Title'>" + tempDropsStl.GetByIndex(i).ToString() + @"</td>
                        </tr>";
			}

			header += @"
		            <tr>
                        <td align='right' valign='center' class='Title1'>Total&nbsp;Pallets:&nbsp;</td>
                        <td align='left' valign='center' class='Title'>" + Misc.DisplayFormat2(PalletsDbl) + @"</td>
                    </tr>
                    <tr>
                        <td align='right' valign='center' class='Title1'>Carrier:&nbsp;</td>
                        <td align='left' valign='center' class='Title'>" + tempCarrierDrv["Carrier"].ToString() + @"</td>
                    </tr>
                </table>
			    <table border='0' cellPadding='0' cellspacing='0'>
			        <tr>
				        <td width=720 align=center height=10 ></td>
			        </tr>
                    <tr>
				        <td align='center' valign='center' width='720' class='Title2'>Nose</td>
                    </tr>
			        <tr>
				        <td width=720 align=center height=10 ></td>
			        </tr>
			    </table>
                <table border='0' cellPadding='0' cellspacing='0'>
				    <tr>
					    <td class='Title3' width=80 height=21 align=right>Quantity</td>
					    <td class='Title3' width=50 height=21 align=right>&nbsp;</td>
					    <td class='Title3' width=375 height=21 align=center>Description</td>
					    <td class='Title3' width=215 height=21 align=center>Warehouse&nbsp;Location&nbsp;&nbsp;</td>
				    </tr>
			    </table>";

			return header;
		}
		public string Detail2()
		{
			string detail = "";
			DataView tempDtv = (DataView)this.Repeater1.DataSource;
			SortedList WarehouseOrderStl = (SortedList)Session["WarehouseOrderStl"];

			tempDtv.Sort = "SoNo DESC, LineSeq";

			int index = 0;

			for (int i = 0; i < tempDtv.Count; i++)
			{
				if (i == 0)
				{
					index = i;
				}
				else
				{
					i = index;
				}

				if (i >= tempDtv.Count)
				{
					break;
				}

				DataRowView tempLoadSheetDrv = tempDtv[i];

				for (int h = 0; h < WarehouseOrderStl.Count; h++)
				{
					ListItem tempListItem = (ListItem)WarehouseOrderStl[h];
					DataView tempWarehouseDtv = new DataView(tempDtv.ToTable(), "WarehouseIdx = '" + tempListItem.Value + "' and SoNo = '" + tempLoadSheetDrv["SoNo"].ToString() + "'", "SoNo DESC, LineSeq", DataViewRowState.CurrentRows);
					
					DataView temp2Dtv = new DataView(tempWarehouseDtv.ToTable(), "SoNo = '" + tempLoadSheetDrv["SoNo"].ToString() + "'", "SoNo DESC, LineSeq", DataViewRowState.CurrentRows);

					for (int j = 0; j < temp2Dtv.Count; j++)
					{
						index++;

						DataRowView temp2Drv = temp2Dtv[j];

						detail += @"
						<table border='0' cellPadding='0' cellspacing='0'>
							<tr>
								<td class='Title4' width=80 height=21 align=right>" + temp2Drv["OrderQnt"].ToString() + @"</td>
								<td class='Title4' width=50 height=21 align=right>&nbsp;</td>
								<td class='Title4' width=375 height=21 align=left>" + StringFormater.FormatString(temp2Drv["Name"].ToString(), 30) + @"</td>
								<td class='Title4' width=215 height=21 align=left>" + StringFormater.FormatString(temp2Drv["descr"].ToString(), 20) + @"</td>
							</tr>
						</table>";
					}
				}
			}

			return detail;
		}
		public string Footer2()
		{
			string total = "";

			total = @"
					<table border='0' cellPadding='0' cellspacing='0'>
                        <tr>
						    <td align='center' height='45' valign='center' width='720' class='Title2'>Tail</td>
                        </tr>
					</table>";

			return total;
		}
		#endregion

		#region Private Functions
		private DataSet GetOrderData(string orderNumber)
		{
			OracleConnection OracleConn = GetOracleConn();
			DataSet orderData = new DataSet();
			#region Oracle Code
			string reportCommand = Misc.FormatOracle(Session, @"
SELECT
			Ar_Trx_Header.SoNo, 
			Ar_Trx_Header.SoDateTime,
			Ar_Trx_Header.CustPoRef,
			CarrName.LastCoName as Carrier,
			Ar_So_Line.IcQnt,
			Ar_So_Line.OrderQnt,
			Ic_Product_Id.Name,
			Fc_Name_Location.Descr,
			Ar_So_Line.UnitsPerPallet,
			CustName.PrintAddress as tocust,
			LComment.TruckComment,
			Ar_Trx_Header.TruckBroker,
			Ar_Trx_Header.ToEtaDateTime,
			Ar_Trx_Header_Ship.FromPrintAddress as FromPrintAddress,
			Fc_Name_Comm.AreaCode custCode,
			Fc_Name_Comm.CommString as custPhone,
			CarrPhone.AreaCode as CarrCode,
			CarrPhone.CommString as CarrPhone,
			CarrFax.CommString as CarrFax,
            Ar_So_Line.ArSoLineSeq as LineSeq,
            AR_TRX_HEADER.ArTrxHdrIdx,
			concat('1~', Ic_Warehouse.WarehouseIdx) as WarehouseIdx,
			concat('2~', Ar_So_Product.FromNameIdx) as ShipperIdx,
			Ar_So_Product.FromLocSeq,
            Ar_So_Detail.Ref
			
	
    FROM    #COMPANY_NUMBER#.AR_TRX_HEADER,
			#COMPANY_NUMBER#.Fc_Name_Location CustName,
			#COMPANY_NUMBER#.Fc_Name CarrName,
			#COMPANY_NUMBER#.Ar_So_Line,
			#COMPANY_NUMBER#.Ar_So_Detail,
			#COMPANY_NUMBER#.Ar_Trx_Header_Ship,
		    #COMPANY_NUMBER#.Ic_Product_Id,
            #COMPANY_NUMBER#.Ic_Warehouse,
            #COMPANY_NUMBER#.Fc_Name_Location,
            #COMPANY_NUMBER#.Fc_Name_Comm,
            #COMPANY_NUMBER#.Fc_Name_Comm CarrPhone,
            #COMPANY_NUMBER#.Fc_Name_Comm CarrFax,
            #COMPANY_NUMBER#.Ar_So_Product,

		(
		Select 
					Ar_So_Comment.Cmnt as TruckComment,
					Ar_Trx_Header.ArTrxHdrIdx
			
		From
					#COMPANY_NUMBER#.AR_TRX_HEADER,
					#COMPANY_NUMBER#.Ar_So_Comment,
					#COMPANY_NUMBER#.Fc_Comment_Type
			
		Where
			    	lower(SubStr(AR_TRX_HEADER.SoNo,0,6)) = lower(:OrderNumber) and
					Ar_So_Comment.ArTrxHdrIdx             = Ar_Trx_Header.ArTrxHdrIdx  and
					Fc_Comment_Type.CommentTypeIdx        = Ar_So_Comment.CommentTypeIdx and
					Fc_Comment_Type.ID                    = 'Truck'
		) LComment
	
		Where
			 lower(SubStr(AR_TRX_HEADER.SoNo,0,6)) = lower(:OrderNumber) and
			CustName.NameIdx                      = Ar_Trx_Header.ToCustNameIdx and
			CustName.NameLocationSeq              = Ar_Trx_Header.ToCustLocSeq and
			CarrName.NameIdx                      (+)= Ar_Trx_Header.CarrierNameIdx and
			Ar_So_Line.ArTrxHdrIdx                = Ar_Trx_Header.ArTrxHdrIdx and
			Ar_So_Line.OrderQnt                   <> '0' and
			Ic_Product_Id.ProductIdx              = Ar_So_Line.ProductIdx and
			Ic_Warehouse.WarehouseIdx             = Ar_So_Line.WarehouseIdx and
			Fc_Name_Location.NameIdx              = Ic_Warehouse.NameIdx and
			Fc_Name_Location.NameLocationSeq      = Ic_Warehouse.NameLocationSeq  and
			Fc_Name_Comm.NameIdx                  (+)= CustName.NameIdx  and
			Fc_Name_Comm.NameLocationSeq          (+)= CustName.NameLocationSeq and
			Fc_Name_Comm.CommType                 (+)= '2' and
			Fc_Name_Comm.NameCommSeq              (+)= '1' and
			CarrPhone.NameIdx                     (+)= Ar_Trx_Header.CarrierNameIdx  and
			CarrPhone.NameLocationSeq             (+)= Ar_Trx_Header.CarrierLocationSeq and
			CarrPhone.CommType                    (+)= '2' and
			CarrPhone.NameCommSeq                 (+)= '1' and
			CarrFax.NameIdx                       (+)= Ar_Trx_Header.CarrierNameIdx  and
			CarrFax.NameLocationSeq               (+)= Ar_Trx_Header.CarrierLocationSeq and
			CarrFax.CommType                      (+)= '3' and
			Ar_So_Detail.ArTrxHdrIdx              = Ar_So_Line.ArTrxHdrIdx and
			Ar_So_Detail.ArSoLineTrxType          = Ar_So_Line.ArSoLineTrxType and
			Ar_So_Detail.ArSoLineSeq              = Ar_So_Line.ArSoLineSeq and
			Ar_Trx_Header_Ship.ArTrxHdrIdx        = Ar_So_Detail.ArTrxHdrIdx and
			Ar_Trx_Header_Ship.FromLocationSeq    = Ic_Warehouse.NameLocationSeq  and
			Ar_Trx_Header_Ship.FromNameIdx        = Ic_Warehouse.NameIdx and
            LComment.ArTrxHdrIdx                  (+)= Ar_Trx_Header.ArTrxHdrIdx and
			Ar_So_Product.ArTrxHdrIdx        	= Ar_So_Detail.ArTrxHdrIdx and
			Ar_So_Product.ArSoDtlSeq       		= Ar_So_Detail.ArSoDtlSeq
	   
    Order By
			Ar_Trx_Header.SoNo ");
			#endregion
			OracleDataAdapter OracleData = new OracleDataAdapter(reportCommand, OracleConn);

			OracleData.SelectCommand.BindByName = true;
			OracleData.SelectCommand.Parameters.Add(":OrderNumber", orderNumber);

			OracleData.Fill(orderData);
			OracleData.Dispose();
			Misc.CleanUpOracle(OracleConn);
			return orderData;
		}
		private void ProcessShipperAndWarehouseIdx(DataSet tempDts)
		{
			for (int i = 0; i < tempDts.Tables[0].Rows.Count; i++)
			{
				DataRow tempDtr = (DataRow)tempDts.Tables[0].Rows[i];

				//If a line has a ShipperIdx we use that, else we use WarehouseIdx.
				//Warehouse: 1~, Shipper: 2~.
				if (tempDtr["ShipperIdx"].ToString() != "" && tempDtr["FromLocSeq"].ToString() == "1")
					tempDtr["WarehouseIdx"] = tempDtr["ShipperIdx"];
				else
					tempDtr["WarehouseIdx"] = tempDtr["WarehouseIdx"];
			}
		}
		private bool GetIfOrderIsInvoiced(string orderNumber)
		{
			bool isInvoicedBln = false;
			OracleConnection OracleConn = GetOracleConn();
			#region Oracle Code
			string reportCommand = Misc.FormatOracle(Session, @"
                Select
	                Ar_Trx_Line.ArTrxHdrIdx

                From
	                #COMPANY_NUMBER#.Ar_Trx_Header,
	                #COMPANY_NUMBER#.Ar_Trx_Line

                Where
	                lower(SubStr(Ar_Trx_Header.SoNo,0,6)) = lower(:OrderNumber) and
	                Ar_Trx_Line.ArTrxHdrIdx = Ar_Trx_Header.ArTrxHdrIdx
                ");
			#endregion"
			OracleCommand OracleData = new OracleCommand(reportCommand, OracleConn);

			OracleData.BindByName = true;
			OracleData.Parameters.Add(":OrderNumber", orderNumber);
			Oracle.DataAccess.Client.OracleDataReader myReader = OracleData.ExecuteReader();

			if (myReader.Read())
			{
				isInvoicedBln = true;
			}

			myReader.Close();
			myReader.Dispose();
			OracleData.Dispose();
			Misc.CleanUpOracle(OracleConn);
			return isInvoicedBln;
		}
		private DataSet GetReportData(string orderNumber)
		{
			//Get Orders that are not in the invoice data
			DataSet orderData = GetOrderData(orderNumber);

			ProcessShipperAndWarehouseIdx(orderData);

			GetSQLData(orderData);

			GetShipperAddresses(orderData);

			return orderData;
		}
		private void GetSQLData(DataSet tempDts)
		{
			ArrayList WarehousesArl = (ArrayList)Session["WarehousesArl"];

			SqlConnection Conn = GetSQLConn();

			SqlCommand SelectCmd = new SqlCommand(@"use [Load_Sheet]; 
            Select
                Trx_Header, 
                Line_Sequence,
                Warehouse_Key,
                Type_Key
            From 
                [Load_Sheet_Input] 
            Where 
                1=1", Conn);

			SqlDataReader myReader = SelectCmd.ExecuteReader();

			while (myReader.Read())
			{
				for (int i = 0; i < tempDts.Tables[0].Rows.Count; i++)
				{
					DataRow tempDtr = tempDts.Tables[0].Rows[i];

					if (tempDtr["ArTrxHdrIdx"].ToString() == myReader.GetValue(0).ToString() && tempDtr["LineSeq"].ToString() == myReader.GetValue(1).ToString())
					{
						for (int j = 0; j < WarehousesArl.Count; j++)
						{
							ListItem tempListItem = (ListItem)WarehousesArl[j];

							if (tempListItem.Value == myReader.GetValue(3).ToString() + "~" + myReader.GetValue(2).ToString())
							{
								tempDtr["WarehouseIdx"] = myReader.GetValue(3).ToString() + "~" + myReader.GetValue(2).ToString();
								tempDtr["descr"] = tempListItem.Text;
							}
						}
					}
				}
			}

			Misc.CleanUpSQL(Conn);
		}
		private void GetShipperAddresses(DataSet tempDts)
		{
			OracleConnection OracleConn = GetOracleConn();
			OracleCommand OracleData = new OracleCommand(FormatOracle(@"

            Select 
                Ar_Shipper.ShipperNameIdx,
                Concat(Fc_Name_Location.Address, Concat(' (', Concat(Fc_Name_Comm.AreaCode, Concat(') ', Fc_Name_Comm.CommString)))) as CommString
            From 
                #COMPANY_NUMBER#.Ar_Shipper, 
                #COMPANY_NUMBER#.Fc_Name,
                #COMPANY_NUMBER#.Fc_Name_Location,
                #COMPANY_NUMBER#.Fc_Name_Comm
            Where 
                Ar_Shipper.ShipperNameIdx = Fc_Name_Location.NameIdx and 
                Fc_Name_Location.OrderBy = '1' and
                Ar_Shipper.ShipperNameIdx = Fc_Name.NameIdx and
                Fc_Name_Comm.NameIdx = Fc_Name_Location.NameIdx and 
                Fc_Name_Comm.NameLocationSeq = Fc_Name_Location.NameLocationSeq and
                Fc_Name_Comm.CommType = '2' and
				Fc_Name_Comm.OrderBy = '1'

            "), OracleConn);

			OracleData.BindByName = true;
			Oracle.DataAccess.Client.OracleDataReader myReader = OracleData.ExecuteReader();

			tempDts.Tables[0].Columns.Add("CommString", System.Type.GetType("System.String"));

			while (myReader.Read())
			{
				for (int i = 0; i < tempDts.Tables[0].Rows.Count; i++)
				{
					DataRow tempDtr = tempDts.Tables[0].Rows[i];

					if (Misc.ParseTildaString(tempDtr["WarehouseIdx"].ToString(), 0) == "2" && Misc.ParseTildaString(tempDtr["WarehouseIdx"].ToString(), 1) == myReader.GetValue(0).ToString())
						tempDtr["CommString"] = myReader.GetValue(1).ToString();
				}
			}

			myReader.Close();
			myReader.Dispose();
			OracleData.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();
		}
		private ArrayList FillWarehousesArl()
		{
			OracleConnection OracleConn = GetOracleConn();
			string oracleSQL = FormatOracle(@"
				Select
					Fc_Name_Location.Descr,
					Ic_Warehouse.WarehouseIdx
				From
					#COMPANY_NUMBER#.Ic_Warehouse,
					#COMPANY_NUMBER#.Fc_Name_Location
				Where
					Ic_Warehouse.NameIdx = Fc_Name_Location.NameIdx and
					Ic_Warehouse.NameLocationSeq = Fc_Name_Location.NameLocationSeq
				Order by Descr");
			OracleCommand OracleCmd = new OracleCommand(oracleSQL, OracleConn);
			OracleCmd.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader = OracleCmd.ExecuteReader();

			ArrayList retArl = new ArrayList();

			while (myReader.Read())
			{
				retArl.Add(new ListItem(myReader.GetValue(0).ToString(), "1~" + myReader.GetValue(1).ToString()));
			}

			myReader.Close();
			myReader.Dispose();
			OracleCmd.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();

			ArrayList tempValidShippersArl = new ArrayList();

			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand(@"use [Load_Sheet]; 
                select 
                    Shipper_Key 
                from 
                    [Valid_Shippers]
                where
                    1=1", Conn);
			SqlDataReader myReader2 = selectCMD.ExecuteReader();

			while (myReader2.Read())
			{
				tempValidShippersArl.Add(myReader2.GetValue(0).ToString());
			}

			Conn.Close();
			Conn.Dispose();
			myReader2.Close();

			OracleConnection OracleConn2 = GetOracleConn();
			string oracleSQL2 = FormatOracle(@"
                Select 
                    Ar_Shipper.ShipperNameIdx,
                    Concat(Fc_Name.LastCoName, Concat('~', Concat(Fc_Name_Location.City, Concat(' ', Fc_Name_Location.State)))) as Name
                From 
                    #COMPANY_NUMBER#.Ar_Shipper, 
                    #COMPANY_NUMBER#.Fc_Name,
                    #COMPANY_NUMBER#.Fc_Name_Location
                Where 
                    Ar_Shipper.ShipperNameIdx = Fc_Name_Location.NameIdx and 
                    Fc_Name_Location.OrderBy = 1 and
                    Ar_Shipper.ShipperNameIdx = Fc_Name.NameIdx
                Order by Name");
			OracleCommand OracleCmd2 = new OracleCommand(oracleSQL2, OracleConn2);
			OracleCmd2.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader3 = OracleCmd2.ExecuteReader();

			while (myReader3.Read())
			{
				if (tempValidShippersArl.Contains(myReader3.GetValue(0).ToString()))
					retArl.Add(new ListItem(myReader3.GetValue(1).ToString(), "2~" + myReader3.GetValue(0).ToString()));
			}

			myReader3.Close();
			myReader3.Dispose();
			OracleCmd2.Dispose();
			OracleConn2.Close();
			OracleConn2.Dispose();

			return retArl;
		}
		private SortedList LoadWarehousesLtbOrderFromSQL()
		{
			SortedList WarehouseOrderStl = new SortedList();
			ArrayList WarehousesArl = (ArrayList)Session["WarehousesArl"];

			SqlConnection Conn = GetSQLConn();
			SqlCommand SelectCmd = new SqlCommand(@"use [Load_Sheet]; 
            Select
                [Index],
                Warehouse_Key,
                Type_Key
            From 
                [Load_Sheet_Warehouse_Order] 
            Where 
                Order_Number=@Order_Number", Conn);

			SelectCmd.Parameters.Add("@Order_Number", orderNumber.Substring(0, 6));

			SqlDataReader myReader = SelectCmd.ExecuteReader();

			while (myReader.Read())
			{
				foreach (ListItem tempListItem in WarehousesArl)
				{
					if (tempListItem.Value == myReader.GetValue(2).ToString() + "~" + myReader.GetValue(1).ToString())
						WarehouseOrderStl.Add(Convert.ToInt32(myReader.GetValue(0)), tempListItem);
				}
			}

			myReader.Close();
			Misc.CleanUpSQL(Conn);

			return WarehouseOrderStl;
		}
		#endregion
	}
}