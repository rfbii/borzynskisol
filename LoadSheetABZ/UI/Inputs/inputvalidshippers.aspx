<%@ Page language="c#" Inherits="LoadSheetABZ.UI.Inputs.InputValidShippers" Codebehind="InputValidShippers.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Input Valid Shippers</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body bgcolor="white">
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
				<table width="920">
					<tbody>
					</tbody>
				</table>
				<table width="920">
					<tr>
						<td align="center">Input Valid Shippers</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
				<table width="920">
					<tr>
						<td width="250"><asp:textbox id="wareHouseKey" runat="server" Width="84px" Visible="False"></asp:textbox><asp:label id="availableLabel" runat="server" Width="168px" Font-Names="Tahoma" Font-Size="X-Small">Shippers Available</asp:label></td>
						<td width="50"></td>
						<td valign="bottom" width="250"><font face="Tahoma" size="2">Valid Shippers</font>
						</td>
					</tr>
					<tr>
						<td align="left"><asp:listbox id="availableList" runat="server" Width="435px" Height="120px" Font-Names="Courier New" onselectedindexchanged="availableList_SelectedIndexChanged"></asp:listbox></td>
						<td align="center" width="50"><asp:button id="add" runat="server" Width="50px" Height="24" Font-Names="Tahoma" Text=">" onclick="add_Click"></asp:button><asp:button id="remove" runat="server" Width="50" Height="24" Text="<" onclick="remove_Click"></asp:button><asp:button id="addAll" runat="server" Width="50px" Height="24" Text=">>" CausesValidation="False" onclick="addAll_Click"></asp:button><asp:button id="removeAll" runat="server" Width="50" Height="24" Text="<<" CausesValidation="False" onclick="removeAll_Click"></asp:button></td>
						<td align="left"><asp:listbox id="selectedList" tabIndex="2" runat="server" Width="435px" Height="120px" Font-Names="Courier New" onselectedindexchanged="selectedList_SelectedIndexChanged"></asp:listbox></td>
					</tr>
				</table>
				<table width="920">
					<tr>
						<td width="200"><asp:textbox id="validWarehouseKey" runat="server" Width="84px" Visible="False"></asp:textbox>
					</tr>
					
				</table>
				<table width="920">
					<tr>
						<td align="center" width="920"><asp:button id="Return" tabIndex="4" runat="server" Width="160px" Height="24px" Text="Return to Menu" onclick="Return_Click"></asp:button></td>
					</tr>
					<tr>						
					    <td align="left" width="920"><asp:label id="message" runat="server" ForeColor="Red"></asp:label></td>
                    </tr>
				</table>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
		    </div>
		</form>
	</body>
</html>
