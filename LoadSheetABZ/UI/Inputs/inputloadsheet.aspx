<%@ Page language="c#" Inherits="LoadSheetABZ.UI.Inputs.InputLoadSheet" Codebehind="InputLoadSheet.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
			<!-- Email Start -->
		<title>Load Sheet</title>
		<style type="text/css">.ReportTable.TD { FONT-SIZE: 8pt }
	.Title { FONT-WEIGHT: bold; FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: Arial }
	.Header { FONT-WEIGHT: bold; FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: Arial; BORDER-Top: black 1px solid; }
	.Title1 {  FONT-SIZE: 11pt; FONT-FAMILY: Arial }
	.Title2 {  FONT-WEIGHT: bold; FONT-SIZE: 14pt; FONT-FAMILY: Arial }
	.Title3 { FONT-SIZE: 11pt; BORDER-BOTTOM: lightgrey 1px solid; FONT-FAMILY: Arial } 
	.Title4 { FONT-WEIGHT: bold; FONT-SIZE: 11pt; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Arial }
	.Title5 { FONT-WEIGHT: bold; FONT-SIZE: 11pt; BORDER-Top: black 1px solid; FONT-FAMILY: Arial }
	.Title6 { FONT-SIZE: 11pt; BORDER-Top: black 1px solid; FONT-FAMILY: Arial }
	.Title7 { FONT-WEIGHT: bold; FONT-SIZE: 11pt; FONT-FAMILY: Arial }
	.Title8 { FONT-SIZE: 11pt; FONT-FAMILY: Arial }
		</style>
				<!-- Email End -->
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C#" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<link href="/KeystoneCustomerStatus/Inc/_styles.css" type="text/css" rel="stylesheet"/>
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body bgcolor="#ffffff" onload="SetupPrinting();">
		<object id="factory" style="DISPLAY: none" codebase="http://www.meadroid.com/scriptx/ScriptX.cab#Version=6,1,431,2"
			classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814" viewastext="">
		</object>
		<script type="text/javascript" language="javascript">
		function SetupPrinting()
		{
			factory.printing.footer = "Printed: &d &t&bPage &p of &P&bInput Load Sheet";
			factory.printing.header = "";
			factory.printing.leftMargin = .5;
			factory.printing.rightMargin = .5;
			factory.printing.topMargin = .5;
			factory.printing.bottomMargin = .5;
		}  
		</script>
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div id="MainDiv" runat="server">
			<!-- Email Start -->
				<table cellspacing="0" cellpadding="0" border="0" width="720" class="ReportTable">
					<tr>
						<td align="center" width="720" class='Title2'>Input Load Sheet</td>
					</tr>
					<tr>
					    <td style="height: 19px">&nbsp;</td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" width="720" class="ReportTable">
					<tr>
						<td align='left' width='240'><asp:button id="ReturnBtn" runat="server" width='150' Text="Return to Criteria" OnClick="ReturnBtn_Click"></asp:button></td>
					    <td align='center' width='240'><asp:button id="ClearBtn" runat="server" width='150' Text="Clear" OnClick="ClearBtn_Click"></asp:button></td>
					    <td align='right' width='240'><asp:button id="SaveBtn" runat="server" width='150' Text="Save" OnClick="SaveBtn_Click"></asp:button></td>
					</tr>
				</table>
			</div>
			<div id="BodyDiv" class="scroll">
				<table cellspacing="0" cellpadding="0" border="0" width="720" class="ReportTable">
					<asp:repeater  EnableViewState="false" id="Repeater1" runat="server">
						<HeaderTemplate>
							<%# Header(Container)%>
						</HeaderTemplate>
						<FooterTemplate>
						    <%# Total()%>
						</FooterTemplate>
					</asp:repeater>
				</table>
				<table width="720">
				<tr>
					<td height="30" class="Title1" width="260" align="center" valign="bottom">Warehouse Load Order</td>
				</tr>
				<tr>
					<td width="260" rowspan="2"><asp:ListBox id="WarehousesLtb" runat="server" Width="260px" Rows="6" Font-Names="Arial"></asp:ListBox></td>
				    <td width="100" valign="bottom"><asp:button runat="server" id="MoveUpBtn" Text="Move Up" width="100" onclick="MoveUpBtn_Click"></asp:button></td>
				    <td rowspan="2" width="360" valign="top">
				        <table width="360">
				            <tr>
				                <td align="right" valign="top"><asp:button runat="server" id="SaveAndRunWithCustBtn" Text="Save & Run Report With Cust." Width="210" onclick="SaveAndRunWithCustBtn_Click"></asp:button></td>
				            </tr>
				            <tr>
				                <td align="right" valign="top"><asp:button runat="server" id="SaveAndRunInlineBtn" Text="Save & Run Report Inline" Width="210" onclick="SaveAndRunInlineBtn_Click"></asp:button></td>
				            </tr>
				        </table>
				    </td>
				</tr>
				<tr>
				    <td width="100" valign="top"><asp:button runat="server" id="MoveDownBtn" Text="Move Down" width="100" onclick="MoveDownBtn_Click"></asp:button></td>
				</tr>
			    </table>
				<!-- Email End -->
			</div>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
		</form>
	</body>
</html>