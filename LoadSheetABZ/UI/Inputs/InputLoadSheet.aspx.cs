using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Text;
using KeyCentral.Functions;

namespace LoadSheetABZ.UI.Inputs
{
    public partial class InputLoadSheet : BasePage
    {
        #region Vars
        bool mainHeader = true;
        bool ChargeHeader = true;
        bool DetailHeader = true;
        bool dropChange = false;
        bool printWarehouseBorderSpaceBln = false;
        string lastOrder = "";
        double totalQnt = 0;
        #endregion

        #region Param Properties
        private string orderNumber { get { return Session["OrderNumber"].ToString(); } }
        #endregion

        #region Events
        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckSecurity("Load Sheet Report") == false)
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/MenuMain.aspx");

            if (!IsPostBack)
            {
                JavaStatusMessage status = new JavaStatusMessage(this);
                status.ShowMessage("Computing");

                if ((DataSet)Session["OriginalDataDts"] != null)
                    Session.Remove("OriginalDataDts");

                if ((DataSet)Session["NewDataDts"] != null)
                    Session.Remove("NewDataDts");

                ArrayList WarehousesArl = FillWarehousesArl();
                Session.Add("WarehousesArl", WarehousesArl);

                DataSet dsRet = new DataSet();
                dsRet = GetReportData(orderNumber.Trim());

                if (dsRet.Tables[0].DefaultView.Count == 0)
                {
                    JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"]) + "&&Records=None");
                }
                else if (GetIfOrderIsInvoiced(orderNumber.Trim()))
                {
                    Response.Write("<script>alert('This order has already been invoiced.');</script>");
                    JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"]));
                }
                else
                {
                    dsRet.Tables[0].DefaultView.Sort = "SoNo, LineSeq";
                    this.Repeater1.DataSource = dsRet.Tables[0].DefaultView;
                    Repeater1.DataBind();

                    Session.Add("OriginalDataDts", dsRet);
                    Session.Add("NewDataDts", ((DataSet)Session["OriginalDataDts"]).Copy());

                    LoadWarehousesLtbOrderFromSQL();
                    FillWarehousesLtb();
                    WarehousesLtb.SelectedIndex = 0;
                }
            }
            else
            {
                DataSet tempDataDts = (DataSet)Session["NewDataDts"];
                ArrayList WarehousesArl = (ArrayList)Session["WarehousesArl"];
                string dropNumberStr = "1";
                if(orderNumber.Length > 6)
                {
                    dropNumberStr = orderNumber.Substring(7, 1);
                }
                for (int i = 0; i < tempDataDts.Tables[0].Rows.Count; i++)
                {
                    DataRow tempDataRow = (DataRow)tempDataDts.Tables[0].Rows[i];

                    for (int j = 0; j < WarehousesArl.Count; j++)
                    {
                        WarehouseItem tempWarehouseItem = (WarehouseItem)WarehousesArl[j];
                        ListItem tempListItem = new ListItem(tempWarehouseItem.NameStr, tempWarehouseItem.IdxStr);

                        if (Request.Form["__EVENTTARGET"].ToString() == "WarehouseDdl" + this.ClientIDSeparator + dropNumberStr)
                        {
                            if (tempListItem.Value == Request.Form["WarehouseDdl" + this.ClientIDSeparator + dropNumberStr])
                            {
                                tempDataRow["descr"] = tempListItem.Text;
                                tempDataRow["WarehouseIdx"] = tempListItem.Value;
                            }
                        }
                        else
                        {
                            if (tempListItem.Value == Request.Form["DetailDdl" + this.ClientIDSeparator + dropNumberStr + this.ClientIDSeparator + tempDataRow["LineSeq"].ToString()].ToString())
                            {
                                tempDataRow["descr"] = tempListItem.Text;
                                tempDataRow["WarehouseIdx"] = tempListItem.Value;
                            }
                        }
                    }
                }

                Session.Add("NewDataDts", tempDataDts);

                FillWarehousesLtb();

                RefreshReport(tempDataDts);
            }
        }
        protected void ReturnBtn_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("../../../LoadSheetABZ/UI/Inputs/CriteriaLoadSheet.aspx?ReportName=InputLoadSheet"); ;
        }
        protected void ClearBtn_Click(object sender, System.EventArgs e)
        {
            DataSet dsRet = (DataSet)Session["OriginalDataDts"];
            Session.Add("NewDataDts", dsRet.Copy());

            RefreshReport(dsRet);

            WarehousesLtb.Items.Clear();
            LoadWarehousesLtbOrderFromSQL();
            FillWarehousesLtb();
            WarehousesLtb.SelectedIndex = 0;
        }
        protected void SaveBtn_Click(object sender, System.EventArgs e)
        {
            KeyCentral.Functions.JavaStatusMessage status = new KeyCentral.Functions.JavaStatusMessage(this);
            status.ShowMessage("Saving");

            DataSet tempDts = (DataSet)Session["NewDataDts"];

            SqlConnection Conn = GetSQLConn();

            for (int i = 0; i < tempDts.Tables[0].Rows.Count; i++)
            {
                DataRow tempDtr = tempDts.Tables[0].Rows[i];

                SqlCommand deleteCMD = new SqlCommand("use [Load_Sheet]; delete from [Load_Sheet_Input] where Trx_Header=@Trx_Header and Line_Sequence=@Line_Sequence", Conn);
                deleteCMD.Parameters.Add("@Trx_Header", tempDtr["ArTrxHdrIdx"].ToString());
                deleteCMD.Parameters.Add("@Line_Sequence", tempDtr["LineSeq"].ToString());
                deleteCMD.ExecuteNonQuery();
            }

            for (int j = 0; j < tempDts.Tables[0].Rows.Count; j++)
            {
                DataRow temp2Dtr = tempDts.Tables[0].Rows[j];

                SqlCommand insertCMD = new SqlCommand("use [Load_Sheet]; insert into [Load_Sheet_Input] (Trx_Header, Line_Sequence, Warehouse_Key, Type_Key) values(@Trx_Header, @Line_Sequence, @Warehouse_Key, @Type_Key)", Conn);
                insertCMD.Parameters.Add("@Trx_Header", temp2Dtr["ArTrxHdrIdx"].ToString());
                insertCMD.Parameters.Add("@Line_Sequence", temp2Dtr["LineSeq"].ToString());
                insertCMD.Parameters.Add("@Warehouse_Key", Misc.ParseTildaString(temp2Dtr["WarehouseIdx"].ToString(), 1));
                insertCMD.Parameters.Add("@Type_Key", Misc.ParseTildaString(temp2Dtr["WarehouseIdx"].ToString(), 0));
                insertCMD.ExecuteNonQuery();
            }

            SqlCommand deleteCMD2 = new SqlCommand("use [Load_Sheet]; delete from [Load_Sheet_Warehouse_Order] where Order_Number=@Order_Number", Conn);
            deleteCMD2.Parameters.Add("@Order_Number", orderNumber.Substring(0, 6));
            deleteCMD2.ExecuteNonQuery();

            foreach (ListItem tempListItem in WarehousesLtb.Items)
            {
                SqlCommand insertCMD2 = new SqlCommand("use [Load_Sheet]; insert into [Load_Sheet_Warehouse_Order] (Order_Number, [Index], Warehouse_Key, Type_Key) values(@Order_Number, @Index, @Warehouse_Key, @Type_Key)", Conn);
                insertCMD2.Parameters.Add("@Order_Number", orderNumber.Substring(0, 6));
                insertCMD2.Parameters.Add("@Index", WarehousesLtb.Items.IndexOf(tempListItem));
                insertCMD2.Parameters.Add("@Warehouse_Key", Misc.ParseTildaString(tempListItem.Value, 1));
                insertCMD2.Parameters.Add("@Type_Key", Misc.ParseTildaString(tempListItem.Value, 0));
                insertCMD2.ExecuteNonQuery();
            }

            Misc.CleanUpSQL(Conn);

            Session.Add("OriginalDataDts", tempDts.Copy());

            WarehousesLtb.SelectedIndex = 0;
        }
        protected void MoveUpBtn_Click(object sender, System.EventArgs e)
        {
            if (WarehousesLtb.SelectedIndex != 0)
            {
                int tempSelectedIndexInt = WarehousesLtb.SelectedIndex;
                ListItem tempListItem = WarehousesLtb.Items[tempSelectedIndexInt];

                WarehousesLtb.Items.RemoveAt(tempSelectedIndexInt);
                WarehousesLtb.Items.Insert(tempSelectedIndexInt - 1, tempListItem);

                WarehousesLtb.SelectedIndex = tempSelectedIndexInt - 1;
            }
        }
        protected void MoveDownBtn_Click(object sender, System.EventArgs e)
        {
            if (WarehousesLtb.SelectedIndex != WarehousesLtb.Items.Count - 1)
            {
                int tempSelectedIndexInt = WarehousesLtb.SelectedIndex;
                ListItem tempListItem = WarehousesLtb.Items[tempSelectedIndexInt];

                WarehousesLtb.Items.RemoveAt(tempSelectedIndexInt);
                WarehousesLtb.Items.Insert(tempSelectedIndexInt + 1, tempListItem);

                WarehousesLtb.SelectedIndex = tempSelectedIndexInt + 1;
            }
        }
        protected void SaveAndRunWithCustBtn_Click(object sender, System.EventArgs e)
        {
            SaveBtn_Click(sender, e);

            JavaScriptRedirect("../../../LoadSheetABZ/Reports/LoadSheet/ReportLoadSheet.aspx" + "?LastPage=" + Misc.EncodeQueryString(Request.Path + "?ReportName=" + Request.QueryString["ReportName"]));
        }
        protected void SaveAndRunInlineBtn_Click(object sender, System.EventArgs e)
        {
            SaveBtn_Click(sender, e);

            JavaScriptRedirect("../../../LoadSheetABZ/Reports/LoadSheet/ReportLoadSheetInline.aspx" + "?LastPage=" + Misc.EncodeQueryString(Request.Path + "?ReportName=" + Request.QueryString["ReportName"]));
        }
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {

        }
        #endregion

        #region Display Code
        public string Header(object oItem)
        {
            string header = "";
            bool printWarehouseBorderSpaceBln = false;
            DataView tempDtv = (DataView)this.Repeater1.DataSource;
            string lastLineSoNoStr = "";
            string dropNumberStr = "1";
            for (int i = 0; i < tempDtv.Count; i++)
            {
                DataRowView tempLoadSheetDrv = tempDtv[i];

                if (lastLineSoNoStr != tempLoadSheetDrv["SoNo"].ToString())
                {
                    DataView temp2Dtv = new DataView(tempDtv.ToTable(), "SoNo = '" + tempLoadSheetDrv["SoNo"].ToString() + "'", "SoNo, LineSeq", DataViewRowState.CurrentRows);
                    double PalletsDbl = 0;

                    for (int j = 0; j < temp2Dtv.Count; j++)
                    {
                        DataRowView temp2Drv = temp2Dtv[j];

                        if (temp2Drv["UnitsPerPallet"].ToString() != "")
                            PalletsDbl += Convert.ToDouble(temp2Drv["OrderQnt"]) / Convert.ToDouble(temp2Drv["UnitsPerPallet"]);
                    }

                    header += @"
                    <table border='0' cellPadding='0' cellspacing='0'>";

                    if (printWarehouseBorderSpaceBln)
                    {
                        header += @"
                        <tr>
	                        <td colspan='3' width=720 align=center height=35>&nbsp;</td>
                        </tr>";
                    }
                    string orderNumberStr = orderNumber;
                    dropNumberStr = "1";
                    if (orderNumberStr.Length > 6)
                    {
                        orderNumberStr = orderNumber.Substring(0, 6);
                        dropNumberStr = tempLoadSheetDrv["SoNo"].ToString().Substring(7, 1);
                    }
                    header += @"
						<tr>
                            <td class='Header' width=100 align=left>>>>>>>>>>></td>
							<td class='Header' width=520 align=center>" + "Drop " + dropNumberStr + @"</td>
						    <td class='Header' width=100 align=right><<<<<<<<<<</td>
                        </tr>
					</table>
					<table border='0' cellPadding='0' cellspacing='0'>
						<tr>
							<td class='Title1' width=70 height=21 align=right valign=top>Customer:&nbsp;</td>
							<td class='Title' width=235 height=21 align=left>" + tempLoadSheetDrv["tocust"].ToString().Replace("\r\n", "<br>") + @"</td>
							<td class='Title1' width=65 height=21 align=right valign=top>Order:&nbsp;<br>Cust PO:&nbsp;<br>Pallets:&nbsp;</td>
							<td class='Title' width=155 height=21 align=left valign=top>" + orderNumberStr + "<br>" + tempLoadSheetDrv["custPoRef"].ToString() + "<br>" + Misc.DisplayFormat2(PalletsDbl) + @"</td>
							<td class='Title1' width=95 height=21 align=right valign=top>Delivery Date:&nbsp;<br>Ship Date:&nbsp;</td>
							<td class='Title' width=100 height=21 align=left valign=top>" + Misc.FormatDateTime(tempLoadSheetDrv["ToEtaDateTime"].ToString()) + "<br>" + Misc.FormatDateTime(tempLoadSheetDrv["SoDateTime"].ToString()) + @"</td>
                        </tr>
					</table>
					<table border='0' cellPadding='0' cellspacing='0'>
                        <tr>
                            <td class='Title1' width=505 height=21 align=right>Warehouse:&nbsp;</td>
                            <td class='BottomRightBlack' align=center width=215>";

                    DropDownList WarehouseDdl = new DropDownList();
                    WarehouseDdl.ID = "WarehouseDdl" + this.ClientIDSeparator + dropNumberStr;
                    WarehouseDdl.Attributes.Add("onchange", @"javascript:setTimeout('__doPostBack(\'" + WarehouseDdl.ID + @"\',\'\')', 0)");
                    WarehouseDdl.Width = Unit.Pixel(215);
                    ArrayList itemsArl = (ArrayList)Session["WarehousesArl"];
                    WarehouseDdl.Items.Add(new ListItem("", "-1"));
                    for (int j = 0; j < itemsArl.Count; j++)
                    {
                        WarehouseItem tempWarehouseItem = (WarehouseItem)itemsArl[j];
                        ListItem tempListItem = new ListItem(tempWarehouseItem.NameStr, tempWarehouseItem.IdxStr);
                        WarehouseDdl.Items.Add(tempListItem);
                    }

                    WarehouseDdl.SelectedValue = "-1";

                    System.IO.StringWriter textWriter = new System.IO.StringWriter();
                    HtmlTextWriter writer = new HtmlTextWriter(textWriter);
                    WarehouseDdl.RenderControl(writer);
                    header += textWriter.ToString();

                    header += @"
                            </td>
                        </tr>
                    </table>
                    <table border='0' cellPadding='0' cellspacing='0'>
							<tr>
								<td class='Title3' width=80 height=21 align=right>Quantity</td>
								<td class='Title3' width=50 height=21 align=right>&nbsp;</td>
								<td class='Title3' width=375 height=21 align=center>Description</td>
								<td class='Title3' width=215 height=21 align=center>Warehouse&nbsp;Location&nbsp;&nbsp;</td>
							</tr>
						</table>";

                    lastLineSoNoStr = tempLoadSheetDrv["SoNo"].ToString();
                    printWarehouseBorderSpaceBln = true;
                }

                header += @"
                <table border='0' cellPadding='0' cellspacing='0'>
							<tr>
								<td class='Title4' width=80 height=21 align=right>" + tempLoadSheetDrv["OrderQnt"].ToString() + @"</td>
								<td class='Title4' width=50 height=21 align=right>&nbsp;</td>
								<td class='Title4' width=375 height=21 align=left>" + StringFormater.FormatString(tempLoadSheetDrv["Name"].ToString(), 30) + @"</td>
								<td class='Title4' width=215 height=21 align=left>";

                DropDownList DetailDdl = new DropDownList();
                DetailDdl.ID = "DetailDdl" + this.ClientIDSeparator + dropNumberStr + this.ClientIDSeparator + tempLoadSheetDrv["LineSeq"];
                DetailDdl.Attributes.Add("onchange", @"javascript:setTimeout('__doPostBack(\'" + DetailDdl.ID + @"\',\'\')', 0)");
                DetailDdl.Width = Unit.Pixel(215);

                ArrayList itemsArl2 = (ArrayList)Session["WarehousesArl"];

                for (int j = 0; j < itemsArl2.Count; j++)
                {
                    WarehouseItem temp2WarehouseItem = (WarehouseItem)itemsArl2[j];
                    ListItem temp2ListItem = new ListItem(temp2WarehouseItem.NameStr, temp2WarehouseItem.IdxStr);
                    DetailDdl.Items.Add(temp2ListItem);
                }

                DetailDdl.ClearSelection();
                DetailDdl.SelectedValue = tempLoadSheetDrv["WarehouseIdx"].ToString();

                System.IO.StringWriter textWriter2 = new System.IO.StringWriter();
                HtmlTextWriter writer2 = new HtmlTextWriter(textWriter2);
                DetailDdl.RenderControl(writer2);
                header += textWriter2.ToString();

                header += @"    
                                </td>
							</tr>
						</table>";

                totalQnt += double.Parse(tempLoadSheetDrv["OrderQnt"].ToString());
            }

            return header;
        }
        public string Total()
        {
            string total = "";

            total = @"
					<table border='0' cellPadding='0' cellspacing='0'>
						<tr>
							<td class='Title7' width=80 height=21 align=right valign=center>" + totalQnt + @"</td>
							<td class='Title8' width=50 height=21 align=center valign=center>Total</td>
					</table>";

            return total;
        }
        #endregion

        #region Private Functions
        private DataSet GetOrderData(string orderNumber)
        {
            OracleConnection OracleConn = GetOracleConn();
            DataSet orderData = new DataSet();
            #region Oracle Code
            string reportCommand = Misc.FormatOracle(Session, @"
    SELECT
			Ar_Trx_Header.SoNo, 
			Ar_Trx_Header.SoDateTime,
			Ar_Trx_Header.CustPoRef,
			CarrName.LastCoName as Carrier,
			Ar_So_Line.IcQnt,
			Ar_So_Line.OrderQnt,
			Ic_Product_Id.Name,
			Fc_Name_Location.Descr,
			Ar_So_Line.UnitsPerPallet,
			CustName.PrintAddress as tocust,
			LComment.TruckComment,
			Ar_Trx_Header.TruckBroker,
			Ar_Trx_Header.ToEtaDateTime,
			Ar_Trx_Header_Ship.FromPrintAddress as FromPrintAddress,
			Fc_Name_Comm.AreaCode custCode,
			Fc_Name_Comm.CommString as custPhone,
			CarrPhone.AreaCode as CarrCode,
			CarrPhone.CommString as CarrPhone,
			CarrFax.CommString as CarrFax,
            Ar_So_Line.ArSoLineSeq as LineSeq,
            AR_TRX_HEADER.ArTrxHdrIdx,
			concat('1~', Ic_Warehouse.WarehouseIdx) as WarehouseIdx,
			concat('2~', Ar_So_Product.FromNameIdx) as ShipperIdx,
			Ar_So_Product.FromLocSeq
			
	
    FROM    #COMPANY_NUMBER#.AR_TRX_HEADER,
			#COMPANY_NUMBER#.Fc_Name_Location CustName,
			#COMPANY_NUMBER#.Fc_Name CarrName,
			#COMPANY_NUMBER#.Ar_So_Line,
			#COMPANY_NUMBER#.Ar_So_Detail,
			#COMPANY_NUMBER#.Ar_Trx_Header_Ship,
		    #COMPANY_NUMBER#.Ic_Product_Id,
            #COMPANY_NUMBER#.Ic_Warehouse,
            #COMPANY_NUMBER#.Fc_Name_Location,
            #COMPANY_NUMBER#.Fc_Name_Comm,
            #COMPANY_NUMBER#.Fc_Name_Comm CarrPhone,
            #COMPANY_NUMBER#.Fc_Name_Comm CarrFax,
            #COMPANY_NUMBER#.Ar_So_Product,

		(
		Select 
					Ar_So_Comment.Cmnt as TruckComment,
					Ar_Trx_Header.ArTrxHdrIdx
			
		From
					#COMPANY_NUMBER#.AR_TRX_HEADER,
					#COMPANY_NUMBER#.Ar_So_Comment,
					#COMPANY_NUMBER#.Fc_Comment_Type
			
		Where
			    	lower(SubStr(AR_TRX_HEADER.SoNo,0,6)) = lower(:OrderNumber) and
					Ar_So_Comment.ArTrxHdrIdx             = Ar_Trx_Header.ArTrxHdrIdx  and
					Fc_Comment_Type.CommentTypeIdx        = Ar_So_Comment.CommentTypeIdx and
					Fc_Comment_Type.ID                    = 'Truck'
		) LComment
	
		Where
			 lower(SubStr(AR_TRX_HEADER.SoNo,0,6)) = lower(:OrderNumber) and
			CustName.NameIdx                      = Ar_Trx_Header.ToCustNameIdx and
			CustName.NameLocationSeq              = Ar_Trx_Header.ToCustLocSeq and
			CarrName.NameIdx                      (+)= Ar_Trx_Header.CarrierNameIdx and
			Ar_So_Line.ArTrxHdrIdx                = Ar_Trx_Header.ArTrxHdrIdx and
			Ar_So_Line.OrderQnt                   <> '0' and
			Ic_Product_Id.ProductIdx              = Ar_So_Line.ProductIdx and
			Ic_Warehouse.WarehouseIdx             = Ar_So_Line.WarehouseIdx and
			Fc_Name_Location.NameIdx              = Ic_Warehouse.NameIdx and
			Fc_Name_Location.NameLocationSeq      = Ic_Warehouse.NameLocationSeq  and
			Fc_Name_Comm.NameIdx                  (+)= CustName.NameIdx  and
			Fc_Name_Comm.NameLocationSeq          (+)= CustName.NameLocationSeq and
			Fc_Name_Comm.CommType                 (+)= '2' and
			Fc_Name_Comm.NameCommSeq              (+)= '1' and
			CarrPhone.NameIdx                     (+)= Ar_Trx_Header.CarrierNameIdx  and
			CarrPhone.NameLocationSeq             (+)= Ar_Trx_Header.CarrierLocationSeq and
			CarrPhone.CommType                    (+)= '2' and
			CarrPhone.NameCommSeq                 (+)= '1' and
			CarrFax.NameIdx                       (+)= Ar_Trx_Header.CarrierNameIdx  and
			CarrFax.NameLocationSeq               (+)= Ar_Trx_Header.CarrierLocationSeq and
			CarrFax.CommType                      (+)= '3' and
			Ar_So_Detail.ArTrxHdrIdx              = Ar_So_Line.ArTrxHdrIdx and
			Ar_So_Detail.ArSoLineTrxType          = Ar_So_Line.ArSoLineTrxType and
			Ar_So_Detail.ArSoLineSeq              = Ar_So_Line.ArSoLineSeq and
			Ar_Trx_Header_Ship.ArTrxHdrIdx        = Ar_So_Detail.ArTrxHdrIdx and
			Ar_Trx_Header_Ship.FromLocationSeq    = Ic_Warehouse.NameLocationSeq  and
			Ar_Trx_Header_Ship.FromNameIdx        = Ic_Warehouse.NameIdx and
            LComment.ArTrxHdrIdx                  (+)= Ar_Trx_Header.ArTrxHdrIdx and
			Ar_So_Product.ArTrxHdrIdx        	= Ar_So_Detail.ArTrxHdrIdx and
			Ar_So_Product.ArSoDtlSeq       		= Ar_So_Detail.ArSoDtlSeq
	   
    Order By
			Ar_Trx_Header.SoNo 
            ");
            #endregion"
            OracleDataAdapter OracleData = new OracleDataAdapter(reportCommand, OracleConn);

            OracleData.SelectCommand.BindByName = true;
            OracleData.SelectCommand.Parameters.Add(":OrderNumber", orderNumber);

            OracleData.Fill(orderData);
            OracleData.Dispose();
            Misc.CleanUpOracle(OracleConn);
            return orderData;
        }
        private void ProcessShipperAndWarehouseIdx(DataSet tempDts)
        {
            for (int i = 0; i < tempDts.Tables[0].Rows.Count; i++)
            {
                DataRow tempDtr = (DataRow)tempDts.Tables[0].Rows[i];

                //If a line has a ShipperIdx we use that, else we use WarehouseIdx.
                //Warehouse: 1~, Shipper: 2~.
                if (tempDtr["ShipperIdx"].ToString() != "" && tempDtr["FromLocSeq"].ToString() == "1")
                    tempDtr["WarehouseIdx"] = tempDtr["ShipperIdx"];
                else
                    tempDtr["WarehouseIdx"] = tempDtr["WarehouseIdx"];
            }
        }
        private void GetSQLData(DataSet tempDts)
        {
            ArrayList WarehousesArl = (ArrayList)Session["WarehousesArl"];

            SqlConnection Conn = GetSQLConn();

            SqlCommand SelectCmd = new SqlCommand(@"use [Load_Sheet]; 
            Select
                Trx_Header, 
                Line_Sequence,
                Warehouse_Key,
                Type_Key
            From 
                [Load_Sheet_Input] 
            Where 
                1=1", Conn);

            SqlDataReader myReader = SelectCmd.ExecuteReader();

            while (myReader.Read())
            {
                for (int i = 0; i < tempDts.Tables[0].Rows.Count; i++)
                {
                    DataRow tempDtr = tempDts.Tables[0].Rows[i];

                    if (tempDtr["ArTrxHdrIdx"].ToString() == myReader.GetValue(0).ToString() && tempDtr["LineSeq"].ToString() == myReader.GetValue(1).ToString())
                    {
                        for (int j = 0; j < WarehousesArl.Count; j++)
                        {
                            WarehouseItem tempWarehouseItem = (WarehouseItem)WarehousesArl[j];

                            if (tempWarehouseItem.IdxStr == myReader.GetValue(3).ToString() + "~" + myReader.GetValue(2).ToString())
                            {
                                tempDtr["WarehouseIdx"] = myReader.GetValue(3).ToString() + "~" + myReader.GetValue(2).ToString();
                                tempDtr["descr"] = tempWarehouseItem.NameStr;
                            }
                        }
                    }
                }
            }

            Misc.CleanUpSQL(Conn);
        }
        private DataSet GetReportData(string orderNumber)
        {
            //Get Orders that are not in the invoice data
            DataSet orderData = GetOrderData(orderNumber);

            ProcessShipperAndWarehouseIdx(orderData);

            GetSQLData(orderData);

            return orderData;
        }
        private bool GetIfOrderIsInvoiced(string orderNumber)
        {
            bool isInvoicedBln = false;
            OracleConnection OracleConn = GetOracleConn();
            #region Oracle Code
            string reportCommand = Misc.FormatOracle(Session, @"
                Select
	                Ar_Trx_Line.ArTrxHdrIdx

                From
	                #COMPANY_NUMBER#.Ar_Trx_Header,
	                #COMPANY_NUMBER#.Ar_Trx_Line

                Where
	                lower(SubStr(Ar_Trx_Header.SoNo,0,6)) = lower(:OrderNumber) and
	                Ar_Trx_Line.ArTrxHdrIdx = Ar_Trx_Header.ArTrxHdrIdx
                ");
            #endregion"
            OracleCommand OracleData = new OracleCommand(reportCommand, OracleConn);

            OracleData.BindByName = true;
            OracleData.Parameters.Add(":OrderNumber", orderNumber);
            Oracle.DataAccess.Client.OracleDataReader myReader = OracleData.ExecuteReader();

            if (myReader.Read())
            {
                isInvoicedBln = true;
            }

            myReader.Close();
            myReader.Dispose();
            OracleData.Dispose();
            Misc.CleanUpOracle(OracleConn);
            return isInvoicedBln;
        }
        private ArrayList FillWarehousesArl()
        {
            ArrayList retArl = new ArrayList();

            OracleConnection OracleConn = GetOracleConn();
            string oracleSQL = FormatOracle(@"
				Select
					Fc_Name_Location.Descr,
					Ic_Warehouse.WarehouseIdx
				From
					#COMPANY_NUMBER#.Ic_Warehouse,
					#COMPANY_NUMBER#.Fc_Name_Location
				Where
					Ic_Warehouse.NameIdx = Fc_Name_Location.NameIdx and
					Ic_Warehouse.NameLocationSeq = Fc_Name_Location.NameLocationSeq
				Order by Descr");
            OracleCommand OracleCmd = new OracleCommand(oracleSQL, OracleConn);
            OracleCmd.BindByName = true;

            Oracle.DataAccess.Client.OracleDataReader myReader = OracleCmd.ExecuteReader();

            while (myReader.Read())
            {
                if (myReader.GetValue(1).ToString() != "5") //If it's not "OUTSIDE PURCHASE".
                {
                    FindOrAddWarehouseItem(retArl, "1~" + myReader.GetValue(1).ToString(), myReader.GetValue(0).ToString());
                }
            }

            myReader.Close();
            myReader.Dispose();
            OracleCmd.Dispose();
            OracleConn.Close();
            OracleConn.Dispose();

            ArrayList tempValidShippersArl = new ArrayList();

            SqlConnection Conn = GetSQLConn();
            SqlCommand selectCMD = new SqlCommand(@"use [Load_Sheet]; 
                select 
                    Shipper_Key 
                from 
                    [Valid_Shippers]
                where
                    1=1", Conn);
            SqlDataReader myReader2 = selectCMD.ExecuteReader();

            while (myReader2.Read())
            {
                tempValidShippersArl.Add(myReader2.GetValue(0).ToString());
            }

            Conn.Close();
            Conn.Dispose();
            myReader2.Close();

            OracleConnection OracleConn2 = GetOracleConn();
            string oracleSQL2 = FormatOracle(@"
                Select 
                    Ar_Shipper.ShipperNameIdx,
                    Concat(Fc_Name.LastCoName, Concat('~', Concat(Fc_Name_Location.City, Concat(' ', Fc_Name_Location.State)))) as Name
                From 
                    #COMPANY_NUMBER#.Ar_Shipper, 
                    #COMPANY_NUMBER#.Fc_Name,
                    #COMPANY_NUMBER#.Fc_Name_Location
                Where 
                    Ar_Shipper.ShipperNameIdx = Fc_Name_Location.NameIdx and 
                    Fc_Name_Location.OrderBy = 1 and
                    Ar_Shipper.ShipperNameIdx = Fc_Name.NameIdx
                Order by Name");
            OracleCommand OracleCmd2 = new OracleCommand(oracleSQL2, OracleConn2);
            OracleCmd2.BindByName = true;

            Oracle.DataAccess.Client.OracleDataReader myReader3 = OracleCmd2.ExecuteReader();

            while (myReader3.Read())
            {
                if (tempValidShippersArl.Contains(myReader3.GetValue(0).ToString()))
                {
                    FindOrAddWarehouseItem(retArl,  "2~" + myReader3.GetValue(0).ToString(), myReader3.GetValue(1).ToString());
                }
            }

            myReader3.Close();
            myReader3.Dispose();
            OracleCmd2.Dispose();
            OracleConn2.Close();
            OracleConn2.Dispose();

            return retArl;
        }
        private void FillWarehousesLtb()
        {
            DataSet tempDts = (DataSet)Session["NewDataDts"];
            ArrayList tempWarehousesArl = (ArrayList)Session["WarehousesArl"];

            for (int i = 0; i < tempDts.Tables[0].Rows.Count; i++)
            {
                DataRow tempDtr = tempDts.Tables[0].Rows[i];

                for (int j = 0; j < tempWarehousesArl.Count; j++)
                {
                    WarehouseItem tempWarehouseItem = (WarehouseItem)tempWarehousesArl[j];
                    ListItem tempListItem = new ListItem(tempWarehouseItem.NameStr, tempWarehouseItem.IdxStr);

                    if (tempListItem.Value == tempDtr["WarehouseIdx"].ToString() && !WarehousesLtb.Items.Contains(tempListItem))
                        WarehousesLtb.Items.Add(tempListItem);
                }
            }

            for (int j = WarehousesLtb.Items.Count - 1; j >= 0; j--)
            {
                ListItem temp2ListItem = WarehousesLtb.Items[j];
                int tempSelectedIndexInt = WarehousesLtb.SelectedIndex;
                bool deleteItemBln = true;

                for (int i = 0; i < tempDts.Tables[0].Rows.Count; i++)
                {
                    DataRow temp2Dtr = tempDts.Tables[0].Rows[i];

                    if (temp2ListItem.Value == temp2Dtr["WarehouseIdx"].ToString())
                    {
                        deleteItemBln = false;
                        break;
                    }
                }

                if (deleteItemBln && WarehousesLtb.Items.IndexOf(temp2ListItem) == WarehousesLtb.Items.Count - 1)
                {
                    WarehousesLtb.Items.Remove(temp2ListItem);
                    WarehousesLtb.SelectedIndex = WarehousesLtb.Items.Count - 1;
                }
                else if (deleteItemBln && WarehousesLtb.Items.IndexOf(temp2ListItem) == tempSelectedIndexInt)
                {
                    WarehousesLtb.Items.Remove(temp2ListItem);
                    WarehousesLtb.SelectedIndex = tempSelectedIndexInt;
                }
                else 
                if (deleteItemBln)
                {
                    WarehousesLtb.Items.Remove(temp2ListItem);
                }
            }
        }
        private void LoadWarehousesLtbOrderFromSQL()
        {
            SortedList tempWarehouseOrderStl = new SortedList();
            ArrayList WarehousesArl = (ArrayList)Session["WarehousesArl"];

            SqlConnection Conn = GetSQLConn();
            SqlCommand SelectCmd = new SqlCommand(@"use [Load_Sheet]; 
            Select
                [Index],
                Warehouse_Key,
                Type_Key
            From 
                [Load_Sheet_Warehouse_Order] 
            Where 
                Order_Number=@Order_Number", Conn);

            SelectCmd.Parameters.Add("@Order_Number", orderNumber.Substring(0, 6));

            SqlDataReader myReader = SelectCmd.ExecuteReader();

            while (myReader.Read())
            {
                tempWarehouseOrderStl.Add(Convert.ToInt32(myReader.GetValue(0)), myReader.GetValue(2).ToString() + "~" + myReader.GetValue(1).ToString());
            }

            myReader.Close();

            Misc.CleanUpSQL(Conn);

            for (int k = 0; k < tempWarehouseOrderStl.Count; k++)
            {
                string tempWarehouseOrderStlListItem = (string)tempWarehouseOrderStl[k];

                for (int j = 0; j < WarehousesArl.Count; j++)
                {
                    WarehouseItem tempWarehouseItem = (WarehouseItem)WarehousesArl[j];
                    ListItem tempWarehousesArlListItem = new ListItem(tempWarehouseItem.NameStr, tempWarehouseItem.IdxStr);

                    if (tempWarehouseOrderStlListItem == tempWarehousesArlListItem.Value)
                        WarehousesLtb.Items.Add(tempWarehousesArlListItem);
                }
            }
        }
        private void RefreshReport(DataSet dsRet)
        {
            if (dsRet.Tables[0].DefaultView.Count != 0)
            {
                printWarehouseBorderSpaceBln = false;

                dsRet.Tables[0].DefaultView.Sort = "SoNo, LineSeq";
                this.Repeater1.DataSource = dsRet.Tables[0].DefaultView;
                Repeater1.DataBind();
            }
            else
                JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"]) + "&&Records=None");
        }
        private WarehouseItem FindOrAddWarehouseItem(ArrayList list, string idxStr, string nameStr)
        {
            WarehouseItem tempItem = new WarehouseItem(idxStr, nameStr);
            int tempWarehouseIdx = list.BinarySearch(tempItem);

            if (tempWarehouseIdx < 0)
                list.Insert(Math.Abs(tempWarehouseIdx) - 1, tempItem);
            else
                tempItem = (WarehouseItem)list[tempWarehouseIdx];

            return tempItem;
        }
        #endregion

        #region CBO
        public class WarehouseItem : IComparable
        {
            #region Declares
            public string IdxStr;
            public string NameStr;
            #endregion

            #region Constructor/Dispose
            public WarehouseItem(string idxStr, string nameStr)
            {
                IdxStr = idxStr;
                NameStr = nameStr;
            }
            #endregion

            #region IComparable Members
            public int CompareTo(object obj)
            {
                int tempCompare;
                WarehouseItem Y = (WarehouseItem)obj;

                if ((tempCompare = this.NameStr.CompareTo(Y.NameStr)) != 0)
                    return tempCompare;
                else if ((tempCompare = this.IdxStr.CompareTo(Y.IdxStr)) != 0)
                    return tempCompare;

                return 0;
            }
            #endregion
        }
        #endregion
    }
}