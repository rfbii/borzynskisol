using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb; 
using Oracle.DataAccess.Client;
using KeyCentral.Functions;

namespace LoadSheetABZ.UI.Inputs
{
	/// <summary>
    /// Summary description for InputValidShippers
	/// </summary>
    public partial class InputValidShippers : KeyCentral.Functions.BasePage
	{
		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
            if (CheckSecurity("Input Valid Shippers") == false)
            {
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/MenuMain.aspx");
			}
			if(!IsPostBack)
			{
                FillSelectedList();
                FillAvailableList();
                SortListBoxes();
			}
		}
		protected void add_Click(object sender, System.EventArgs e)
		{			
			if(availableList.SelectedIndex!=-1)
			{
				SqlConnection Conn = GetSQLConn();
                SqlCommand insertCMD = new SqlCommand("use [Load_Sheet]; insert into [Valid_Shippers] (Shipper_Key) values(@Shipper_Key)", Conn);
				insertCMD.Parameters.Add("@Shipper_Key",wareHouseKey.Text);

				insertCMD.ExecuteNonQuery();
				message.Text = "Warehouse Added.";
				FillSelectedList();
				FillAvailableList();
				Conn.Close();
				Conn.Dispose();

                SortListBoxes();
			}
			else
			{
				message.Text = "Please select a Warehouse to add.";
			}
		}
		protected void remove_Click(object sender, System.EventArgs e)
		{
			if(selectedList.SelectedIndex!=-1)
			{
				SqlConnection Conn = GetSQLConn();
				
				message.Text = selectedList.SelectedItem.Value;
				//Remove entry from Valid_Warehouse.
                SqlCommand removeCMD = new SqlCommand("use [Load_Sheet]; delete from [Valid_Shippers] where Shipper_Key = @Shipper_Key", Conn);
                removeCMD.Parameters.Add("@Shipper_Key", wareHouseKey.Text);

				removeCMD.ExecuteNonQuery();
				message.Text = "Warehouse Removed.";
				FillSelectedList();
				FillAvailableList();
				Conn.Close();
				Conn.Dispose();

                SortListBoxes();
			}
			else
			{
				message.Text = "Please select a Warehouse to remove.";
			}
		}
		protected void addAll_Click(object sender, System.EventArgs e)
		{	
			FillSelectedList();

			SqlConnection Conn = GetSQLConn();

			OracleConnection OracleConn = GetOracleConn();
			OracleCommand OracleData = new OracleCommand(FormatOracle(@"

Select 
    Ar_Shipper.ShipperNameIdx,
                    Concat(Fc_Name.LastCoName, Concat('~', Concat(Fc_Name_Location.City, Concat(' ', Fc_Name_Location.State)))) as Name
From 
    #COMPANY_NUMBER#.Ar_Shipper, 
    #COMPANY_NUMBER#.Fc_Name,
    #COMPANY_NUMBER#.Fc_Name_Location
Where 
    Ar_Shipper.ShipperNameIdx = Fc_Name_Location.NameIdx and 
    Fc_Name_Location.OrderBy = '1' and
Ar_Shipper.ShipperNameIdx = Fc_Name.NameIdx"), OracleConn);

			DataSet dsRet = new DataSet();
			OracleData.BindByName = true;
			int x=0;

			availableList.Items.Clear();
			Oracle.DataAccess.Client.OracleDataReader myReader =OracleData.ExecuteReader();
			while (myReader.Read()) 
			{
				x= KeyCentral.Functions.Misc.TextIsInListBox(this.selectedList,myReader.GetValue(0).ToString());
				if (x==-1)
				{
					selectedList.Items.Add( new ListItem(myReader.GetString(1),myReader.GetValue(0).ToString()));
                    SqlCommand insertCMD = new SqlCommand("use [Load_Sheet]; insert into [Valid_Shippers] (Shipper_Key) values(@Shipper_Key)", Conn);
                    insertCMD.Parameters.Add("@Shipper_Key", myReader.GetInt32(0));
					insertCMD.ExecuteNonQuery();
				}
				else
				{
					selectedList.Items.Add(new ListItem(myReader.GetString(1),selectedList.Items[x].Value));
					selectedList.Items.Remove(selectedList.Items[x]);
				}
			}
			message.Text = "All Warehouses Added.";
			myReader.Close();
			myReader.Dispose();
			OracleData.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();
			Conn.Close();

            SortListBoxes();
		}
		protected void removeAll_Click(object sender, System.EventArgs e)
		{
			SqlConnection Conn = GetSQLConn();
			
			SqlCommand removeCMD = new SqlCommand("use [Load_Sheet]; delete from [Valid_Shippers]", Conn);
			removeCMD.ExecuteNonQuery();
			FillSelectedList();
			FillAvailableList();
			message.Text = "All Warehouses Removed.";				
			Conn.Close();
			Conn.Dispose();

            SortListBoxes();
		}
		protected void Return_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Freight");		
		}
		protected void availableList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			wareHouseKey.Text = availableList.SelectedItem.Value;
		}
		protected void selectedList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			wareHouseKey.Text = selectedList.SelectedItem.Value;
		}
		#endregion
			
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
		
		#region Private helpers
		private void FillAvailableList()
		{
			OracleConnection OracleConn = GetOracleConn();
			OracleCommand OracleData = new OracleCommand(FormatOracle(@"

Select 
    Ar_Shipper.ShipperNameIdx,
                    Concat(Fc_Name.LastCoName, Concat('~', Concat(Fc_Name_Location.City, Concat(' ', Fc_Name_Location.State)))) as Name
From 
    #COMPANY_NUMBER#.Ar_Shipper, 
    #COMPANY_NUMBER#.Fc_Name,
    #COMPANY_NUMBER#.Fc_Name_Location
Where 
    Ar_Shipper.ShipperNameIdx = Fc_Name_Location.NameIdx and 
    Fc_Name_Location.OrderBy = '1' and
Ar_Shipper.ShipperNameIdx = Fc_Name.NameIdx"), OracleConn);
			
			DataSet dsRet = new DataSet();
			int x=0;

			OracleData.BindByName = true;
		
			availableList.Items.Clear();
			Oracle.DataAccess.Client.OracleDataReader myReader =OracleData.ExecuteReader();
			while (myReader.Read()) 
			{
				x= KeyCentral.Functions.Misc.TextIsInListBox(this.selectedList,myReader.GetValue(0).ToString());
				if (x==-1)
					availableList.Items.Add(new ListItem(myReader.GetString(1),myReader.GetValue(0).ToString()));
				else
				{
					selectedList.Items.Add(new ListItem(myReader.GetString(1),selectedList.Items[x].Value));
					selectedList.Items.Remove(selectedList.Items[x]);
				}
			}
			myReader.Close();
			myReader.Dispose();
			OracleData.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();
		}
		private void FillSelectedList()
		{
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("use [Load_Sheet]; select * from [Valid_Shippers]", Conn);
			SqlDataReader myReader = selectCMD.ExecuteReader();
			
			selectedList.DataSource = myReader;
			selectedList.DataValueField = "Shipper_Key";
			selectedList.DataBind();
			Conn.Close();
            Conn.Dispose();
            myReader.Close();
		}
        private void SortListBoxes()
        {
            ArrayList tempAvailableArl = new ArrayList();
            ArrayList tempSelectedArl = new ArrayList();

            for (int i = availableList.Items.Count - 1; i >= 0; i--)
            {
                ListItem tempAvailableListItem = (ListItem)availableList.Items[i];

                tempAvailableArl.Add(tempAvailableListItem);
                availableList.Items.Remove(tempAvailableListItem);
            }

            for (int i = selectedList.Items.Count - 1; i >= 0; i--)
            {
                ListItem tempSelectedListItem = (ListItem)selectedList.Items[i];

                tempSelectedArl.Add(tempSelectedListItem);
                selectedList.Items.Remove(tempSelectedListItem);
            }

            SortListItemsByText(tempAvailableArl);
            SortListItemsByText(tempSelectedArl);

            for (int i = 0; i < tempAvailableArl.Count; i++)
            {
                availableList.Items.Add((ListItem)tempAvailableArl[i]);
            }

            for (int i = 0; i < tempSelectedArl.Count; i++)
            {
                selectedList.Items.Add((ListItem)tempSelectedArl[i]);
            }
        }
        private void SortListItemsByText(ArrayList ret)
        {
            for (int j = 0; j < ret.Count; j++)
            {
                for (int k = j + 1; k < ret.Count; k++)
                {
                    ListItem tempListItem = (ListItem)ret[j]; 
                    ListItem temp2ListItem = (ListItem)ret[k];

                    if (temp2ListItem.Text.CompareTo(tempListItem.Text) < 0)
                    {
                        ret[j] = temp2ListItem;
                        ret[k] = tempListItem;
                    }
                }
            }
        }
        #endregion
    }
}

