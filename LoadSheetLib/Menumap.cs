using System;
using System.Collections.Generic;
using System.Text;
using KeyCentral.Functions;

namespace LoadSheetLib
{
	[MenuMap(Security = "Load Sheet Report", Description = "Input Load Sheet", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Freight", SortOrder = 11, Url = "../../../LoadSheetABZ/UI/Inputs/CriteriaLoadSheet.aspx?ReportName=InputLoadSheet")]
	[MenuMap(Security = "Load Sheet Report", Description = "Load Sheet Report With Customer", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Freight", SortOrder = 12, Url = "../../../LoadSheetABZ/Reports/LoadSheet/CriteriaLoadSheetReport.aspx?ReportName=ReportLoadSheet")]
	[MenuMap(Security = "Load Sheet Report", Description = "Input Valid Shippers", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Freight", SortOrder = 10, Url = "../../../LoadSheetABZ/UI/Inputs/InputValidShippers.aspx?ReportName=InputValidShippers")]
	[MenuMap(Security = "Load Sheet Report", Description = "Load Sheet Report Inline", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Freight", SortOrder = 13, Url = "../../../LoadSheetABZ/Reports/LoadSheet/CriteriaLoadSheetReportInline.aspx?ReportName=ReportLoadSheetInline")]

    class Menumap
    {
    }
}