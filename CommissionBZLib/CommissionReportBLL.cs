using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Diagnostics;
using System.Reflection;
using System.Web.UI;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;
using KeyCentral.Functions;
using KeyCentral.Lookups;
using WeeklySales.App_Code;

namespace CommissionBZLib
{
	/// <summary>
	/// Summary description for CommissionReportBLL.
	/// </summary>
	public class CommissionReportBLL
	{
		#region Vars
		//Some globals so I don't have to pass the data all the time
		private string MinShipDate;
		private string MaxShipDate;
		private string SalesPerson;
		private ArrayList Commodities;
		private ArrayList ExcludedCommodities;
		private ArrayList ExcludedCustomers;
		private ArrayList Varieties;
		private ArrayList SalesmanCommissionPaidArrayList;
		private ArrayList FamousCommissionArrayList;
		public double OutsideSaleCommissionRateDbl;
		public double InsideSaleCommissionRateDbl;
		private Page page;

		public ArrayList SalesPersonArl;
		public ArrayList InsideSalesArl;
		public ArrayList OutsideSalesArl;
        public ArrayList CommissionOrderArl;
        public SortedList salesPersonStl;

		private System.Web.SessionState.HttpSessionState Session {get{return System.Web.HttpContext.Current.Session;}}
		#endregion

		#region Constructor/Dispose
		public CommissionReportBLL()
		{
		}
		#endregion

		#region Public Get Functions
		public ArrayList GetData(ArrayList Commodities, ArrayList ExcludedCommodities, ArrayList ExcludedCustomers, ArrayList Varieties, string SalesPerson, string MinShipDate, string MaxShipDate, Page page)
		{
			//Set some global data
			this.Commodities = Commodities;
			this.ExcludedCommodities = ExcludedCommodities;
			this.ExcludedCustomers = ExcludedCustomers;
			this.Varieties = Varieties;
			this.MinShipDate = MinShipDate;
			this.MaxShipDate = MaxShipDate;
			this.SalesPerson = SalesPerson;
			this.page = page;

			ArrayList ret = new ArrayList();

			SalesmanCommissionPaidArrayList = GetSalesmanCommissionPaid();
			FamousCommissionArrayList = GetFamousCommissionAmounts();

			FillCommissionRates();

			RawSalesBLL rawSalesBLL = new RawSalesBLL(page);

			rawSalesBLL.GetRawSalesData("", "", "", Commodities, Varieties, SalesPerson, new ArrayList(), new ArrayList(), MinShipDate, MaxShipDate);

			for (int i = 0; i < rawSalesBLL.RawDetails.Count; i++)
			{
				RawSalesBLL.ItemDetail tempItemDetail = (RawSalesBLL.ItemDetail)rawSalesBLL.RawDetails[i];

				//Filter out excluded commodities nd excluded customers
				if (!FindExcludedCommodity(ExcludedCommodities, Convert.ToInt32(tempItemDetail.CommodityId)) && !FindExcludedCustomer(ExcludedCustomers, Convert.ToInt32(tempItemDetail.CustomerKey)))
				{
					CommissionItem commissionItem = FindOrAddCommissionItem(ret, tempItemDetail.SoNo, tempItemDetail.CustName, tempItemDetail.ArTrxHdrIdx);
					CommissionItem_Detail commissionItemDetail = FindOrAddCommissionItemDetail(commissionItem.CommissionItemDetails, tempItemDetail.ShipDateTime, tempItemDetail.Invdescr, tempItemDetail.SoNo, tempItemDetail.ShipToName, tempItemDetail.ShipToCity, tempItemDetail.ShipToState, tempItemDetail.ShipToZip, tempItemDetail.Type, tempItemDetail.ShipToDescr, tempItemDetail.SalesPerson, tempItemDetail.CustName, tempItemDetail.CustCity, tempItemDetail.CustState, tempItemDetail.Style, tempItemDetail.Size, tempItemDetail.Grade, tempItemDetail.Label, tempItemDetail.Variety, tempItemDetail.Commodity, tempItemDetail.Warehouse, tempItemDetail.LotId, tempItemDetail.GrowerName, tempItemDetail.CommodityId, tempItemDetail.VarietyId, tempItemDetail.WarehouseId, tempItemDetail.FillType);
					commissionItemDetail.Qnt += tempItemDetail.Qnt;
					commissionItemDetail.FOBQnt += tempItemDetail.FOBQnt;
					commissionItemDetail.EquivQnt += tempItemDetail.EquivQnt;
					commissionItemDetail.SalesQnt += tempItemDetail.SalesQnt;
					commissionItemDetail.Cost += tempItemDetail.Cost;
					commissionItemDetail.OutsideSaleCommissionRateDbl = OutsideSaleCommissionRateDbl;
					commissionItemDetail.InsideSaleCommissionRateDbl = InsideSaleCommissionRateDbl;

					if (commissionItemDetail.FillType == "3")
						commissionItemDetail.OutSideSale = "Y";
					else
						commissionItemDetail.OutSideSale = "N";

					CommissionPaid commissionPaid = FindCommissionPaid(SalesmanCommissionPaidArrayList, tempItemDetail.SoNo);
					commissionItem.Paid = commissionPaid.Paid;
					commissionItem.CheckNumber = commissionPaid.CheckNumber;

					FamousCommission famousCommission = FindFamousCommission(FamousCommissionArrayList, tempItemDetail.SoNo);
					commissionItem.FamousCommissionAmt = famousCommission.Amount;

					//if (famousCommission.VendorId != "68~1")
					//    commissionItemDetail.OutSideSale = "Y";
					//else
					//    commissionItemDetail.OutSideSale = "N";
				}
			}

			return ret;
		}
		public void GetNewPendingData(ArrayList Commodities, ArrayList ExcludedCommodities, ArrayList ExcludedCustomers, ArrayList Varieties, ArrayList HouseSalesPersons, string MinShipDate, string MaxShipDate, Page page, bool PendingBln, string PaidStr)
		{
			//Set some global data
			this.Commodities = Commodities;
			this.ExcludedCommodities = ExcludedCommodities;
			this.ExcludedCustomers = ExcludedCustomers;
			this.Varieties = Varieties;
			this.MinShipDate = MinShipDate;
			this.MaxShipDate = MaxShipDate;
			this.page = page;
            salesPersonStl = GetSalesman();
            //FillNewTable();// Only for original loading of table.
			SalesPersonArl = new ArrayList();
            ArrayList paidDetailsArl = GetPaidDetails();
            CommissionOrderArl = new ArrayList();
            ArrayList CommissionInsideOrderArl = new ArrayList();

            for (int i = 0; i < HouseSalesPersons.Count; i++)
			{
				this.SalesPersonArl.Add(HouseSalesPersons[i]);
			}

			SalesmanCommissionPaidArrayList = GetSalesmanCommissionPaid();
			FamousCommissionArrayList = GetFamousCommissionAmounts();

			FillCommissionRates();

			RawSalesBLL rawSalesBLL = new RawSalesBLL(page);

			rawSalesBLL.GetRawSalesData("", "", "", Commodities, Varieties, "", new ArrayList(), new ArrayList(), MinShipDate, MaxShipDate);

			for (int i = 0; i < rawSalesBLL.RawDetails.Count; i++)
			{
				RawSalesBLL.ItemDetail tempItemDetail = (RawSalesBLL.ItemDetail)rawSalesBLL.RawDetails[i];

				//Only add invoices that starts with "W" or "B" (real invoices). Filter out excluded commodities and excluded customers
				//if (tempItemDetail.SoNo.StartsWith("W") || tempItemDetail.SoNo.StartsWith("B"))
				//{
					if (tempItemDetail.Type.ToUpper() == "INVOICED" && tempItemDetail.SalesPerson.ToUpper() != "HOUSE" &&
					!FindExcludedCommodity(ExcludedCommodities, Convert.ToInt32(tempItemDetail.CommodityId)) && !FindExcludedCustomer(ExcludedCustomers, Convert.ToInt32(tempItemDetail.CustomerKey)))
					{
						if (tempItemDetail.FillType == "3")
						{
							#region Outside Sale
							Int32 tempSalesman = Convert.ToInt32(salesPersonStl.GetKey(salesPersonStl.IndexOfValue(tempItemDetail.SalesPerson)));
							Commission_Order tempCommissionOrder = (Commission_Order)FindOrAddCommissionOrder(CommissionOrderArl, Convert.ToInt32(tempItemDetail.ArTrxHdrIdx), tempSalesman);
							if (tempItemDetail.ArAmt == tempItemDetail.ReceiptAmt)
							{
								tempCommissionOrder.InvoicePaidBln = true;
							}
							else
							{
								tempCommissionOrder.InvoicePaidBln = false;
							}
							Commission_Order_Detail tempCommissionOrderDetail = new Commission_Order_Detail();
							tempCommissionOrderDetail.DateStr = tempItemDetail.ShipDateTime.ToString();
							tempCommissionOrderDetail.LoadStr = tempItemDetail.SoNo;
							tempCommissionOrderDetail.CustomerStr = tempItemDetail.CustName;
							tempCommissionOrderDetail.QntDbl = tempItemDetail.Qnt;
							tempCommissionOrderDetail.FOBDbl = tempItemDetail.FOBQnt;
							tempCommissionOrderDetail.CostDbl = tempItemDetail.Cost;
							tempCommissionOrderDetail.ItemStr = tempItemDetail.Commodity;
							tempCommissionOrderDetail.InBln = false;
							tempCommissionOrder.CommissionOrderDetailArl.Add(tempCommissionOrderDetail);
							#endregion
						}
						else
						{
							#region Inside Sale
							Commission_Order tempCommissionOrder = (Commission_Order)FindOrAddCommissionOrder(CommissionInsideOrderArl, Convert.ToInt32(tempItemDetail.ArTrxHdrIdx), -1);
							Commission_Order_Detail tempCommissionOrderDetail = new Commission_Order_Detail();
							if (tempItemDetail.ArAmt == tempItemDetail.ReceiptAmt)
							{
								tempCommissionOrder.InvoicePaidBln = true;
							}
							else
							{
								tempCommissionOrder.InvoicePaidBln = false;
							}
							tempCommissionOrderDetail.DateStr = tempItemDetail.ShipDateTime.ToString();
							tempCommissionOrderDetail.LoadStr = tempItemDetail.SoNo;
							tempCommissionOrderDetail.CustomerStr = tempItemDetail.CustName;
							tempCommissionOrderDetail.QntDbl = tempItemDetail.Qnt;
							tempCommissionOrderDetail.FOBDbl = tempItemDetail.FOBQnt;
							tempCommissionOrderDetail.CostDbl = tempItemDetail.Cost;
							tempCommissionOrderDetail.ItemStr = tempItemDetail.Commodity;
							tempCommissionOrderDetail.InBln = true;
							tempCommissionOrder.CommissionOrderDetailArl.Add(tempCommissionOrderDetail);
							#endregion
						}
					}
				//}
			}
            //Remove Orders that are not ready to pay commissions on.
            #region Outside Sales
            for (int k = (CommissionOrderArl.Count -1); k > 0; k--)
            {
                Commission_Order tempCommissionOrder = (Commission_Order)CommissionOrderArl[k];

                if (tempCommissionOrder.InvoicePaidBln == false)
                {
                    CommissionOrderArl.RemoveAt(k);
                }
            }
            #endregion
            #region Inside Sales
            for (int k = (CommissionInsideOrderArl.Count - 1); k > 0; k--)
            {
                Commission_Order tempCommissionOrder = (Commission_Order)CommissionInsideOrderArl[k];

                if (tempCommissionOrder.InvoicePaidBln == false)
                {
                    CommissionInsideOrderArl.RemoveAt(k);
                }
            }
            #endregion

            //Add Commissions paid to Outside Orders
            #region Add Commissions paid
            for (int k = 0; k < CommissionOrderArl.Count; k++)
            {
                Commission_Order tempCommissionOrder = (Commission_Order)CommissionOrderArl[k];
                for (int l = 0; l < paidDetailsArl.Count; l++)
                {
                    CommissionTable tempPaid = (CommissionTable)paidDetailsArl[l];

                    if (tempPaid.SoNo_Key == tempCommissionOrder.OrderKeyInt)
                    {
                        if (tempPaid.Salesman_Key == tempCommissionOrder.SalesmanInt)
                        {
                            tempCommissionOrder.OutPaidDbl += tempPaid.OutPaid;
                            //tempCommissionOrder.InPaidDbl += tempPaid.InPaid;
                            CommissionPaidDetails tempPaidDetails = new CommissionPaidDetails();
                            tempPaidDetails.ReportNo = tempPaid.Report_Number_Key;
                            tempPaidDetails.OutPaid = tempPaid.OutPaid;
                            //tempPaidDetails.InPaid = tempPaid.InPaid;
                            tempCommissionOrder.CommissionPaidDetailArl.Add(tempPaidDetails);
                        }
                        //else
                        //{
                        //    Commission_Order tempCommissionOrder1 = (Commission_Order)FindOrAddCommissionOrder(CommissionOrderArl, tempPaid.SoNo_Key, tempPaid.Salesman_Key);
                        //    tempCommissionOrder1.OutPaidDbl += tempPaid.OutPaid;
                        //    tempCommissionOrder1.InPaidDbl += tempPaid.InPaid;
                        //    CommissionPaidDetails tempPaidDetails = new CommissionPaidDetails();
                        //    tempPaidDetails.ReportNo = tempPaid.Report_Number_Key;
                        //    tempPaidDetails.OutPaid = tempPaid.OutPaid;
                        //    tempPaidDetails.InPaid = tempPaid.InPaid;
                        //    tempCommissionOrder1.CommissionPaidDetailArl.Add(tempPaidDetails);
                        //}
                    }
                }
            }
            #endregion

            //Set Commission on Outside Sales
            #region Outside Sales
            
            for (int k = 0; k < CommissionOrderArl.Count; k++)
            {
                Commission_Order tempCommissionOrder = (Commission_Order)CommissionOrderArl[k];
                double tempCommission = 0;
                for (int l = 0; l < tempCommissionOrder.CommissionOrderDetailArl.Count; l++)
                {
                    Commission_Order_Detail tempDetail = (Commission_Order_Detail)tempCommissionOrder.CommissionOrderDetailArl[l];
			        tempCommission += ((tempDetail.FOBDbl - tempDetail.CostDbl) * OutsideSaleCommissionRateDbl);
                }
                tempCommissionOrder.OutCurrentDbl = tempCommission;
            }
            #endregion

            //Set Commission on Inside Sales
            #region Inside Sales
            for (int k = 0; k < CommissionInsideOrderArl.Count; k++)
            {
                Commission_Order tempCommissionOrder = (Commission_Order)CommissionInsideOrderArl[k];
                double tempCommission = 0;
                       for (int l = 0; l < tempCommissionOrder.CommissionOrderDetailArl.Count; l++)
                {
                    Commission_Order_Detail tempDetail = (Commission_Order_Detail)tempCommissionOrder.CommissionOrderDetailArl[l];
                    tempCommission += (tempDetail.FOBDbl * InsideSaleCommissionRateDbl);
                }
                double tempCommissionPaid = 0;
                for (int l = 0; l < paidDetailsArl.Count; l++)
                {
                    CommissionTable tempPaid = (CommissionTable)paidDetailsArl[l];

                    if (tempPaid.SoNo_Key == tempCommissionOrder.OrderKeyInt)
                    {
                        tempCommissionPaid += tempPaid.InPaid;
                  
                    }
                }
                double tempNetCommission = (tempCommission - tempCommissionPaid);
                if (tempNetCommission != 0)
                {
                    // Need to add lines for all Salesman.
                    double ratioDbl = (1/Convert.ToDouble(SalesPersonArl.Count));
                    for(int m = 0; m < SalesPersonArl.Count; m++)
                    {
                        SalesPerson tempSalesman = (SalesPerson)SalesPersonArl[m];
                    if( tempSalesman.SalesPersonNameStr.ToUpper() != "HOUSE")
                        {
                            Commission_Order tempCommissionOrder1 = (Commission_Order)FindOrAddCommissionOrder(CommissionOrderArl, tempCommissionOrder.OrderKeyInt, Convert.ToInt32(tempSalesman.SalesPersonKeyStr));
                            tempCommissionOrder1.InvoicePaidBln = tempCommissionOrder.InvoicePaidBln;
                            tempCommissionOrder1.InCurrentDbl = tempCommission;
                            for (int n = 0; n < tempCommissionOrder.CommissionOrderDetailArl.Count; n++)
                            {
                                Commission_Order_Detail tempCommissionOrderDetail = (Commission_Order_Detail)tempCommissionOrder.CommissionOrderDetailArl[n];
                                Commission_Order_Detail tempCommissionOrderDetail1 = new Commission_Order_Detail();
                                tempCommissionOrderDetail1.ItemStr = tempCommissionOrderDetail.ItemStr;
                                tempCommissionOrderDetail1.DateStr = tempCommissionOrderDetail.DateStr;
                                tempCommissionOrderDetail1.LoadStr = tempCommissionOrderDetail.LoadStr;
                                tempCommissionOrderDetail1.CustomerStr = tempCommissionOrderDetail.CustomerStr;
                                tempCommissionOrderDetail1.QntDbl = (tempCommissionOrderDetail.QntDbl);
                                tempCommissionOrderDetail1.FOBDbl = (tempCommissionOrderDetail.FOBDbl);
                                tempCommissionOrderDetail1.CostDbl = (tempCommissionOrderDetail.CostDbl);
                                tempCommissionOrderDetail1.InBln = true;
                                tempCommissionOrder1.CommissionOrderDetailArl.Add(tempCommissionOrderDetail1);
                            }
                            for (int n = 0; n < paidDetailsArl.Count; n++)
                            {
                                CommissionTable tempPaid = (CommissionTable)paidDetailsArl[n];

                                if (tempPaid.SoNo_Key == tempCommissionOrder1.OrderKeyInt)
                                {
                                    tempCommissionOrder1.InPaidDbl += tempPaid.InPaid;
                                    CommissionPaidDetails tempPaidDetails = new CommissionPaidDetails();
                                    tempPaidDetails.ReportNo = tempPaid.Report_Number_Key;
                                    tempPaidDetails.InPaid = tempPaid.InPaid;
                                    tempCommissionOrder1.CommissionPaidDetailArl.Add(tempPaidDetails);

                                    
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            
            //Set InNet and OutNet in CommissionOrderArl (InNet = InCurrent - InPaid) same for Out
            for (int p = 0; p < CommissionOrderArl.Count; p++)
            {
                Commission_Order tempInNet = (Commission_Order)CommissionOrderArl[p];

                tempInNet.InNetDbl = tempInNet.InCurrentDbl - tempInNet.InPaidDbl;
            }

            for (int p = 0; p < CommissionOrderArl.Count; p++)
            {
                Commission_Order tempOutNet = (Commission_Order)CommissionOrderArl[p];

                tempOutNet.OutNetDbl = tempOutNet.OutCurrentDbl - tempOutNet.OutPaidDbl;
            }

                // Remove lines by PaidRadioList selection
                if (PaidStr != "B")
                {
                    if (PaidStr == "N")
                    {
                        #region Remove lines that are paid
                        //Todone Only one loop PaidStr == 'N'
                       
                        for (int j = CommissionOrderArl.Count -1; j >= 0; j--)
                        {
                            Commission_Order tempSalesman = (Commission_Order)CommissionOrderArl[j];
                            //Todone if(InPaid !=0 && InNet == 0 && OutNet == 0 || OutPaid !=0 && InNet == 0 && OutNet == 0)
                            if (tempSalesman.InPaidDbl != 0 && tempSalesman.InNetDbl == 0 && tempSalesman.OutNetDbl == 0 || tempSalesman.OutPaidDbl != 0 && tempSalesman.InNetDbl == 0 && tempSalesman.OutNetDbl == 0)
                            {
                                CommissionOrderArl.RemoveAt(j);
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Remove lines that are not paid
                        //Todone new loop to remove not paid lines
                        
                         for (int j = CommissionOrderArl.Count - 1; j >= 0; j--)
                            {
                                Commission_Order tempSalesman = (Commission_Order)CommissionOrderArl[j];
                                //Todone if(InPaid == 0 && OutPaid == 0)
                                if (tempSalesman.InPaidDbl == 0 && tempSalesman.OutPaidDbl == 0)
                                {
                                    CommissionOrderArl.RemoveAt(j);
                                }
                            }
                        #endregion
                    }
                }
			
		
		}
		private void FillCommissionRates()
		{
			OutsideSaleCommissionRateDbl = 0;
			InsideSaleCommissionRateDbl = 0;

			SqlConnection Conn = Misc.GetSQLConn();

			SqlCommand selectCMD = new SqlCommand("use Commission; select Outside_Commission_Rate, Inside_Commission_Rate from Commission_Rates", Conn);

			SqlDataReader myReader = selectCMD.ExecuteReader();

			if (myReader.Read())
			{
				OutsideSaleCommissionRateDbl = Convert.ToDouble(myReader.GetValue(0).ToString());
				InsideSaleCommissionRateDbl = Convert.ToDouble(myReader.GetValue(1).ToString());
			}

			myReader.Close();

			Misc.CleanUpSQL(Conn);
		}
		#endregion

		#region Private Helper Functions
        private void FillNewTable()
        {
            ArrayList tempTable = new ArrayList();
            SqlConnection Conn = Misc.GetSQLConn();
            SqlCommand selectCMD = new SqlCommand("use Commission; SELECT * FROM New_Commission_Report", Conn);
            
            SqlDataReader myReader = selectCMD.ExecuteReader();

            if (myReader.Read())
            {
                while (myReader.Read())
                {
                    if (myReader.GetValue(1).ToString() == "Y")
                    {
                            CommissionTable tempItem = FindOrAddSalesCommissionTable(tempTable, myReader.GetInt32(0), myReader.GetInt32(3), GetOrderSalesman(myReader.GetValue(0).ToString()));
                            tempItem.OutPaid = myReader.GetDouble(2);
                    }
                    else
                    {
                        CommissionTable tempItem = FindOrAddSalesCommissionTable(tempTable, myReader.GetInt32(0), myReader.GetInt32(3), 2);
                        tempItem.InPaid = (myReader.GetDouble(2) / 2);
                        CommissionTable tempItem1 = FindOrAddSalesCommissionTable(tempTable, myReader.GetInt32(0), myReader.GetInt32(3), 96);
                        tempItem1.InPaid = (myReader.GetDouble(2) / 2);
                    }
                    
                    
                }
            }
            myReader.Close();
            Misc.CleanUpSQL(Conn);
            //Add data into new table
            SqlConnection Conn1 = Misc.GetSQLConn();
            //For loop through temptable to add each row

            for (int j = 0; j < tempTable.Count; j++)
            {
                CommissionTable tempItem = (CommissionTable)tempTable[j];
                SqlCommand insertCMD = new SqlCommand("use Commission;insert into [Commission_Report] (ArTrxHdrIdx,Report_Number,Salesman_Key,Inpaid,OutPaid)  values(@ArTrxHdrIdx,@Report_Number,@Salesman_Key,@Inpaid,@OutPaid)", Conn1);
                insertCMD.Parameters.AddWithValue("@ArTrxHdrIdx", tempItem.SoNo_Key);
                insertCMD.Parameters.AddWithValue("@Report_Number", tempItem.Report_Number_Key);
                insertCMD.Parameters.AddWithValue("@Salesman_Key", tempItem.Salesman_Key);
                insertCMD.Parameters.AddWithValue("@InPaid", tempItem.InPaid);
                insertCMD.Parameters.AddWithValue("@OutPaid", tempItem.OutPaid);
                
                insertCMD.ExecuteNonQuery();
            }
            Misc.CleanUpSQL(Conn1);
           
        } // prove the table load properly then remove.
        private ArrayList GetPaidDetails()
        {
            ArrayList tempTable = new ArrayList();
            SqlConnection Conn = Misc.GetSQLConn();
            SqlCommand selectCMD = new SqlCommand("use Commission; SELECT * FROM Commission_Report", Conn);

            SqlDataReader myReader = selectCMD.ExecuteReader();

            if (myReader.Read())
            {
                while (myReader.Read())
                {
                    CommissionTable tempItem = FindOrAddSalesCommissionTable(tempTable, myReader.GetInt32(0), myReader.GetInt32(1), myReader.GetInt32(2));
                    tempItem.InPaid = myReader.GetDouble(3); 
                    tempItem.OutPaid = myReader.GetDouble(4);
                }
            }
            myReader.Close();
            Misc.CleanUpSQL(Conn);
           return tempTable;

        } // prove the table load properly then remove.
        public Int32 GetOrderSalesman(string soNo)
        {
            int ret = 0;

             WhereClauseBuilder whereClause = new WhereClauseBuilder();

            OracleConnection OracleConn = Misc.GetOracleConn(HttpContext.Current.Session);
            #region oracle code for Ordered and Shipped
            string oracleSQL = Misc.FormatOracle(HttpContext.Current.Session, @" 
	SELECT 	Ar_Trx_Header.SalespersonNameIdx
	FROM	Ar_Trx_Header
	WHERE  1=1
           #WhereClause:Ar_Trx_Header.ArTrxHdrIdx#");
            #endregion

            //build the functions
            whereClause.Add(WhereClauseUnit.UnitType.String, "Ar_Trx_Header.ArTrxHdrIdx", soNo);

            OracleCommand OracleCmd = new OracleCommand(whereClause.BuildWhereClause(oracleSQL), OracleConn);
            OracleCmd.BindByName = true;

            Oracle.DataAccess.Client.OracleDataReader myReader = OracleCmd.ExecuteReader();

            while (myReader.Read())
            {
                ret= Convert.ToInt32(myReader["SalespersonNameIdx"]);
             }

            myReader.Close();
            Misc.CleanUpOracle(OracleConn);

            return ret;
        }
        private SortedList GetSalesman()
        {
            SortedList ret = new SortedList();

            OracleConnection OracleConn = Misc.GetOracleConn(HttpContext.Current.Session);
            #region oracle code for Ordered and Shipped
            string oracleSQL = Misc.FormatOracle(HttpContext.Current.Session, @" 
	SELECT 	Fc_Saleperson.SalespersonNameIdx,
            Fc_Name.LastCoName
	FROM	Fc_Saleperson,
            Fc_Name
	WHERE   Fc_Name.NameIdx  = FC_Saleperson.SalespersonNameIdx");
            #endregion

            OracleCommand OracleCmd = new OracleCommand(oracleSQL, OracleConn);
            OracleCmd.BindByName = true;

            Oracle.DataAccess.Client.OracleDataReader myReader = OracleCmd.ExecuteReader();

            while (myReader.Read())
            {
                ret.Add(myReader.GetInt32(0), myReader.GetString(1));
            }

            myReader.Close();
            Misc.CleanUpOracle(OracleConn);

            return ret;
        }
		public ArrayList GetFamousCommissionAmounts()
		{
			ArrayList ret = new ArrayList();

			#region Command Text
			string reportCommand = Misc.FormatOracle(HttpContext.Current.Session, @" 
SELECT /*+ CHOOSE */
	Ar_Trx_Header.SONO,
	Sum(Ar_Trx_Charge.Qnt) AS Qnt,
	Sum(Ar_Trx_Detail.ForeignAmt) AS Amount,
	Fc_Charge_Code.Id,
	Ar_Trx_Charge.NameIdx,
	Ar_Trx_Charge.NameLocationSeq
	
FROM
	Ar_Trx_Header,
	FC_NAME Cust_Name,
	Ar_Trx_Charge,
	Ar_Trx_Detail,
	Fc_Charge_Code
	
WHERE
	AR_TRX_HEADER.SOTYPE					IN ('2','5') AND
	Ar_Trx_Header.SoStatus					IN ('6','7','C','P') AND
	Cust_Name.NameIdx						= Ar_Trx_Header.CUSTNAMEIDX AND
	Ar_Trx_Charge.ArTrxHdrIdx				= Ar_Trx_Header.ArTrxHdrIdx AND
	Ar_Trx_Detail.ArTrxHdrIdx				= Ar_Trx_Charge.ArTrxHdrIdx AND
	Ar_Trx_Detail.ArTrxDtlSeq				= Ar_Trx_Charge.ArTrxDtlSeq AND
	Ar_Trx_Detail.TrxType					= '2' AND
	Fc_Charge_Code.FcChargeIdx				= Ar_Trx_Charge.FcChargeIdx AND
	lower(SUBSTR(Fc_Charge_Code.Id,1,6))	= 'apcmsn'
	
GROUP BY
	Ar_Trx_Header.SONO,
	Fc_Charge_Code.Id,
	Ar_Trx_Charge.NameIdx,
	Ar_Trx_Charge.NameLocationSeq
");
			#endregion

			OracleConnection Conn = Misc.GetOracleConn(HttpContext.Current.Session);

			OracleCommand OracleCmd = new OracleCommand(reportCommand, Conn);

			OracleDataReader myReader = OracleCmd.ExecuteReader();

			while (myReader.Read())
			{
				FamousCommission famousCommission = FindOrAddFamousCommission(ret, myReader["SoNo"].ToString());
				famousCommission.Qnt += Convert.ToDouble(myReader["Qnt"]);
				famousCommission.Amount += Convert.ToDouble(myReader["Amount"]);
				famousCommission.VendorId = myReader["NameIdx"].ToString() + "~" + myReader["NameLocationSeq"].ToString();
			}

			myReader.Close();
			//Misc.CleanUpOracle(Conn);

			return ret;
		}
		private ArrayList GetSalesmanCommissionPaid()
		{
			ArrayList ret = new ArrayList();

			#region Command Text
			string reportCommand = Misc.FormatOracle(HttpContext.Current.Session, @" 
SELECT
	Distinct AR_TRX_HEADER.SONO as INVOICENO,
	AP_VOUCHER_INQUIRY_VIEW.Descr,
	AP_VOUCHER_INQUIRY_VIEW.ForeignAmt,
	AP_VOUCHER_VIEW.PaymentRefNo,
	AP_VOUCHER_VIEW.PaidAmt

FROM
	AP_VOUCHER_INQUIRY_VIEW,
	AP_VOUCHER_VIEW,
	AR_TRX_HEADER

WHERE
	AP_VOUCHER_INQUIRY_VIEW.PONO = AR_TRX_HEADER.SONO and
	AP_VOUCHER_INQUIRY_VIEW.ACRUCASHTYPE = 'A' AND 
	AP_VOUCHER_VIEW.VOUCHERHDRIDX = AP_VOUCHER_INQUIRY_VIEW.VOUCHERHDRIDX and
	lower(SUBSTR(AP_VOUCHER_INQUIRY_VIEW.Descr,1,13)) = 'salesman comm'
");
			#endregion

			OracleConnection Conn = Misc.GetOracleConn(HttpContext.Current.Session);

			OracleCommand OracleCmd = new OracleCommand(reportCommand, Conn);

			OracleDataReader myReader = OracleCmd.ExecuteReader();

			while (myReader.Read())
			{
				//if PaymentRefNo is not null and PaidAmt is not 0, then fill in the paid amount and the check number
				if (!myReader.IsDBNull(3) && Convert.ToDouble(myReader["PaidAmt"]) != 0)
				{
					CommissionPaid commissionPaid = FindOrAddCommissionPaid(ret, myReader["InvoiceNo"].ToString());
					commissionPaid.Paid += Convert.ToDouble(myReader["ForeignAmt"]);
					commissionPaid.CheckNumber = myReader["PaymentRefNo"].ToString();
				}
			}

			myReader.Close();
			//Misc.CleanUpOracle(Conn);

			return ret;
		}
        private CommissionItem_SalesPerson FindOrAddCommissionItemSalesPerson(ArrayList list, string SalesPerson)
        {
            CommissionItem_SalesPerson tempCommissionItemSalesPerson = new CommissionItem_SalesPerson(SalesPerson);
            int tempCommissionItemSalesPersonIdx = list.BinarySearch(tempCommissionItemSalesPerson);

            if (tempCommissionItemSalesPersonIdx < 0)
                list.Insert(Math.Abs(tempCommissionItemSalesPersonIdx) - 1, tempCommissionItemSalesPerson);
            else
                tempCommissionItemSalesPerson = (CommissionItem_SalesPerson)list[tempCommissionItemSalesPersonIdx];

            return tempCommissionItemSalesPerson;
        }
        private CommissionTable FindOrAddSalesCommissionTable(ArrayList list, int SoNo_Key, int Report_Number_Key, int Salesman_Key)
        {
            CommissionTable tempSalesCommissionTable = new CommissionTable(SoNo_Key, Report_Number_Key, Salesman_Key);
            int tempSalesCommissionTableIdx = list.BinarySearch(tempSalesCommissionTable);

            if (tempSalesCommissionTableIdx < 0)
                list.Insert(Math.Abs(tempSalesCommissionTableIdx) - 1, tempSalesCommissionTable);
            else
                tempSalesCommissionTable = (CommissionTable)list[tempSalesCommissionTableIdx];

            return tempSalesCommissionTable;
        }
		private CommissionItem FindOrAddCommissionItem(ArrayList list, string SoNo, string CustomerName, string ArTrxHdrIdx)
		{
			CommissionItem tempCommissionItem = new CommissionItem(SoNo, CustomerName, ArTrxHdrIdx);
			int tempCommissionItemIdx = list.BinarySearch(tempCommissionItem);

			if (tempCommissionItemIdx < 0)
				list.Insert(Math.Abs(tempCommissionItemIdx) - 1, tempCommissionItem);
			else
				tempCommissionItem = (CommissionItem)list[tempCommissionItemIdx];

			return tempCommissionItem;
		}
		private CommissionPaid FindOrAddCommissionPaid(ArrayList list, string SoNo)
		{
			CommissionPaid tempCommissionPaid = new CommissionPaid(SoNo);
			int tempCommissionPaidIdx = list.BinarySearch(tempCommissionPaid);

			if (tempCommissionPaidIdx < 0)
				list.Insert(Math.Abs(tempCommissionPaidIdx) - 1, tempCommissionPaid);
			else
				tempCommissionPaid = (CommissionPaid)list[tempCommissionPaidIdx];

			return tempCommissionPaid;
		}
		private CommissionPaid FindCommissionPaid(ArrayList list, string SoNo)
		{
			CommissionPaid tempCommissionPaid = new CommissionPaid(SoNo);
			int tempCommissionPaidIdx = list.BinarySearch(tempCommissionPaid);

			if (tempCommissionPaidIdx >= 0)
				tempCommissionPaid = (CommissionPaid)list[tempCommissionPaidIdx];

			return tempCommissionPaid;
		}
		private FamousCommission FindOrAddFamousCommission(ArrayList list, string SoNo)
		{
			FamousCommission tempFamousCommission = new FamousCommission(SoNo);
			int tempFamousCommissionIdx = list.BinarySearch(tempFamousCommission);

			if (tempFamousCommissionIdx < 0)
				list.Insert(Math.Abs(tempFamousCommissionIdx) - 1, tempFamousCommission);
			else
				tempFamousCommission = (FamousCommission)list[tempFamousCommissionIdx];

			return tempFamousCommission;
		}
		private FamousCommission FindFamousCommission(ArrayList list, string SoNo)
		{
			FamousCommission tempFamousCommission = new FamousCommission(SoNo);
			int tempFamousCommissionIdx = list.BinarySearch(tempFamousCommission);

			if (tempFamousCommissionIdx >= 0)
				tempFamousCommission = (FamousCommission)list[tempFamousCommissionIdx];

			return tempFamousCommission;
		}
		private CommissionItem_Detail FindOrAddCommissionItemDetail(ArrayList list, DateTime shipdatetime, string invdescr, string sono, string shiptoname, string shiptocity, string shiptostate, string shiptozip, string type, string shiptodescr, string salesperson, string custname, string custcity, string custstate, string style, string size, string grade, string label, string variety, string commodity, string warehouse, string lotid, string growername, string commodityId, string varietyId, string warehouseId, string fillType)
		{
			CommissionItem_Detail tempCommissionItemDetail = new CommissionItem_Detail(shipdatetime, invdescr, sono, shiptoname, shiptocity, shiptostate, shiptozip, type, shiptodescr, salesperson, custname, custcity, custstate, style, size, grade, label, variety, commodity, warehouse, lotid, growername, commodityId, varietyId, warehouseId, fillType);
			int tempCommissionItemDetailIdx = list.BinarySearch(tempCommissionItemDetail);

			if (tempCommissionItemDetailIdx < 0)
				list.Insert(Math.Abs(tempCommissionItemDetailIdx) - 1, tempCommissionItemDetail);
			else
				tempCommissionItemDetail = (CommissionItem_Detail)list[tempCommissionItemDetailIdx];

			return tempCommissionItemDetail;
		}
        private Commission_Order FindOrAddCommissionOrder(ArrayList list, int OrderKey, int SalesmanKey)
        {
            Commission_Order tempCommissionOrder = new Commission_Order(OrderKey, SalesmanKey);
            int tempCommissionOrderIdx = list.BinarySearch(tempCommissionOrder);

            if (tempCommissionOrderIdx < 0)
                list.Insert(Math.Abs(tempCommissionOrderIdx) - 1, tempCommissionOrder);
            else
                tempCommissionOrder = (Commission_Order)list[tempCommissionOrderIdx];

            return tempCommissionOrder;
        }
        private Commission_Order_Detail FindOrAddCommissionOrderDetail(ArrayList list, string Item, bool InBln)
        {
            Commission_Order_Detail tempCommissionOrderDetail = new Commission_Order_Detail(Item, InBln);
            int tempCommissionOrderDetailIdx = list.BinarySearch(tempCommissionOrderDetail);

            if (tempCommissionOrderDetailIdx < 0)
                list.Insert(Math.Abs(tempCommissionOrderDetailIdx) - 1, tempCommissionOrderDetail);
            else
                tempCommissionOrderDetail = (Commission_Order_Detail)list[tempCommissionOrderDetailIdx];

            return tempCommissionOrderDetail;
        }
		private void AddExcludedCommodity(ArrayList list, int CommodityKey)
		{
			ExcludedCommodity tempExcludedCommodity = new ExcludedCommodity(CommodityKey);
			int tempExcludedCommodityIdx = list.BinarySearch(tempExcludedCommodity);

			if (tempExcludedCommodityIdx < 0)
				list.Insert(Math.Abs(tempExcludedCommodityIdx) - 1, tempExcludedCommodity);
		}
		private bool FindExcludedCommodity(ArrayList list, int CommodityKey)
		{
			bool found = false;

			ExcludedCommodity tempExcludedCommodity = new ExcludedCommodity(CommodityKey);
			int tempExcludedCommodityIdx = list.BinarySearch(tempExcludedCommodity);

			if (tempExcludedCommodityIdx >= 0)
				found = true;

			return found;
		}
		private void AddExcludedCustomer(ArrayList list, int CustomerKey)
		{
			ExcludedCustomer tempExcludedCustomer = new ExcludedCustomer(CustomerKey);
			int tempExcludedCustomerIdx = list.BinarySearch(tempExcludedCustomer);

			if (tempExcludedCustomerIdx < 0)
				list.Insert(Math.Abs(tempExcludedCustomerIdx) - 1, tempExcludedCustomer);
		}
		private bool FindExcludedCustomer(ArrayList list, int CustomerKey)
		{
			bool found = false;

			ExcludedCustomer tempExcludedCustomer = new ExcludedCustomer(CustomerKey);
			int tempExcludedCustomerIdx = list.BinarySearch(tempExcludedCustomer);

			if (tempExcludedCustomerIdx >= 0)
				found = true;

			return found;
		}
		#endregion
	}

	#region CBO
	public class CommissionItem_SalesPerson : IComparable
	{
		#region Vars
		public string SalesPerson;
		public bool NewCommissionReportBln;
		public ArrayList CommissionItems;

		#region Properties
		public double SumOfUnits
		{
			get
			{
				double ret = 0;

				for (int i = 0; i < CommissionItems.Count; i++)
				{
					CommissionItem commissionItem = (CommissionItem)CommissionItems[i];
					ret += commissionItem.SumOfUnits;
				}

				return ret;
			}
		}
		public double SumOfFOBs
		{
			get
			{
				double ret = 0;

				for (int i = 0; i < CommissionItems.Count; i++)
				{
					CommissionItem commissionItem = (CommissionItem)CommissionItems[i];
					ret += commissionItem.SumOfFOBs;
				}

				return ret;
			}
		}
		public double SumOfCommissions
		{
			get
			{
				double ret = 0;

				for (int i = 0; i < CommissionItems.Count; i++)
				{
					CommissionItem commissionItem = (CommissionItem)CommissionItems[i];
					ret += commissionItem.SumOfCommissions;
				}

				return ret;
			}
		}
		public double SumOfPaid
		{
			get
			{
				double ret = 0;

				for (int i = 0; i < CommissionItems.Count; i++)
				{
					CommissionItem commissionItem = (CommissionItem)CommissionItems[i];
					ret += commissionItem.Paid;
				}

				return ret;
			}
		}
		public double SumOfFamousCommissionAmt
		{
			get
			{
				double ret = 0;

				for (int i = 0; i < CommissionItems.Count; i++)
				{
					CommissionItem commissionItem = (CommissionItem)CommissionItems[i];
					ret += commissionItem.FamousCommissionAmt;
				}

				return ret;
			}
		}
		public double SumOfInvoiceAmount
		{
			get
			{
				double ret = 0;

				for (int i = 0; i < CommissionItems.Count; i++)
				{
					CommissionItem commissionItem = (CommissionItem)CommissionItems[i];
					ret += commissionItem.InvoiceAmount;
				}

				return ret;
			}
		}
		public double SumOfInvoicePaid
		{
			get
			{
				double ret = 0;

				for (int i = 0; i < CommissionItems.Count; i++)
				{
					CommissionItem commissionItem = (CommissionItem)CommissionItems[i];
					ret += commissionItem.InvoicePaid;
				}

				return ret;
			}
		}
		public double Difference
		{
			get
			{
				return (SumOfCommissions - SumOfFamousCommissionAmt);
			}
		}
		public double Due
		{
			get
			{
				double ret = (SumOfCommissions - SumOfPaid);

				if (CustomerDue != 0 && NewCommissionReportBln)
				{
					ret = 0;
				}

				return ret;
			}
		}
		public double CustomerDue//New Commission Report (invoice due)
		{
			get
			{
				return SumOfInvoiceAmount - SumOfInvoicePaid;
			}
		}
		#endregion
		#endregion

		#region Constructor
		public CommissionItem_SalesPerson()
		{
			SalesPerson = string.Empty;
			CommissionItems = new ArrayList();
		}
		public CommissionItem_SalesPerson(string salesPerson)
		{
			SalesPerson = salesPerson;
			CommissionItems = new ArrayList();
		}
		#endregion

		#region IComparable Members
		public int CompareTo(object obj)
		{
			int tempCompare;
			CommissionItem_SalesPerson Y = (CommissionItem_SalesPerson)obj;

			//Compare SalesPerson
			tempCompare = this.SalesPerson.CompareTo(Y.SalesPerson);
			if (tempCompare != 0)
				return tempCompare;

			return 0;
		}
		#endregion
	}
	public class CommissionItem : IComparable
	{
		#region Vars
		public string SoNo;
		public string CustomerName;
		public string ArTrxHdrIdx;
		public string CheckNumber;
		public string ReportNumber;//New Commission Report
		public double Paid;
		public double FamousCommissionAmt;
		public double InvoiceAmount;
		public double InvoicePaid;
		public bool NewCommissionReportBln;
		public ArrayList CommissionItemDetails;

		#region Properties
		public double SumOfUnits
		{
			get
			{
				double ret = 0;

				for (int i = 0; i < CommissionItemDetails.Count; i++)
				{
					CommissionItem_Detail commissionItem_Detail = (CommissionItem_Detail)CommissionItemDetails[i];
					ret += commissionItem_Detail.Qnt;
				}

				return ret;
			}
		}
		public double SumOfFOBs
		{
			get
			{
				double ret = 0;

				for (int i = 0; i < CommissionItemDetails.Count; i++)
				{
					CommissionItem_Detail commissionItem_Detail = (CommissionItem_Detail)CommissionItemDetails[i];
					ret += commissionItem_Detail.FOBQnt;
				}

				return ret;
			}
		}
		public double SumOfCommissions
		{
			get
			{
				double ret = 0;

				for (int i = 0; i < CommissionItemDetails.Count; i++)
				{
					CommissionItem_Detail commissionItem_Detail = (CommissionItem_Detail)CommissionItemDetails[i];
					ret += commissionItem_Detail.Commission;
				}

				return ret;
			}
		}
		public double Difference
		{
			get
			{
				return (SumOfCommissions - FamousCommissionAmt);
			}
		}
		public double Due
		{
			get
			{
				double ret = (SumOfCommissions - Paid);

				if (CustomerDue != 0 && NewCommissionReportBln)
				{
					ret = 0;
				}

				return ret;
			}
		}
		public double CustomerDue//New Commission Report (invoice due)
		{
			get
			{
				return InvoiceAmount - InvoicePaid;
			}
		}
		#endregion
		#endregion

		#region Constructor
		public CommissionItem()
		{
			SoNo = string.Empty;
			CustomerName = string.Empty;
			ArTrxHdrIdx = string.Empty;
			CheckNumber = string.Empty;
			ReportNumber = string.Empty;
			Paid = 0;
			FamousCommissionAmt = 0;
			InvoiceAmount = 0;
			InvoicePaid = 0;
			NewCommissionReportBln = false;
			CommissionItemDetails = new ArrayList();
		}
		public CommissionItem(string soNo, string customerName, string arTrxHdrIdx)
		{
			SoNo = soNo;
			CustomerName = customerName;
			ArTrxHdrIdx = arTrxHdrIdx;
			CheckNumber = string.Empty;
			ReportNumber = string.Empty;
			Paid = 0;
			FamousCommissionAmt = 0;
			InvoiceAmount = 0;
			InvoicePaid = 0;
			NewCommissionReportBln = false;
			CommissionItemDetails = new ArrayList();
		}
		#endregion

		#region IComparable Members
		public int CompareTo(object obj)
		{
			int tempCompare;
			CommissionItem Y = (CommissionItem)obj;

			//Compare SoNo
			tempCompare = this.SoNo.CompareTo(Y.SoNo);
			if (tempCompare != 0)
				return tempCompare;

			//Compare CustomerName
			tempCompare = this.CustomerName.CompareTo(Y.CustomerName);
			if (tempCompare != 0)
				return tempCompare;

			//Compare ArTrxHdrIdx
			tempCompare = this.ArTrxHdrIdx.CompareTo(Y.ArTrxHdrIdx);
			if (tempCompare != 0)
				return tempCompare;

			return 0;
		}
		#endregion
	}
	public class CommissionPaid : IComparable
	{
		#region Vars
		public string SoNo;
		public double Paid;
		public string CheckNumber;
		#endregion

		#region Constructor
		public CommissionPaid(string soNo)
		{
			SoNo = soNo;
			Paid = 0;
			CheckNumber = string.Empty;
		}
		#endregion

		#region IComparable Members
		public int CompareTo(object obj)
		{
			int tempCompare;
			CommissionPaid Y = (CommissionPaid)obj;

			//Compare SoNo
			tempCompare = this.SoNo.CompareTo(Y.SoNo);
			if (tempCompare != 0)
				return tempCompare;

			return 0;
		}
		#endregion
	}
    public class CommissionTable : IComparable
    {
        #region Vars
        public int SoNo_Key;
        public int Report_Number_Key;
        public int Salesman_Key;
        public double InPaid;
        public double OutPaid;
        #endregion

        #region Constructor
        public CommissionTable()
        {
            SoNo_Key = 0;
            Report_Number_Key = 0;
            Salesman_Key = 0;
            InPaid = 0;
            OutPaid = 0;
        }
        public CommissionTable(int soNo_Key, int report_Number_Key, int salesman_Key)
        {
            SoNo_Key = soNo_Key;
            Report_Number_Key = report_Number_Key;
            Salesman_Key = salesman_Key;
            InPaid = 0;
            OutPaid = 0;
        }
        #endregion

        #region IComparable Members
        public int CompareTo(object obj)
        {
            int tempCompare;
            CommissionTable Y = (CommissionTable)obj;

            //Compare SoNo_Key
            tempCompare = this.SoNo_Key.CompareTo(Y.SoNo_Key);
            if (tempCompare != 0)
                return tempCompare;

            //Compare Report_Number_Key
            tempCompare = this.Report_Number_Key.CompareTo(Y.Report_Number_Key);
            if (tempCompare != 0)
                return tempCompare;

            //Compare Salesman_Key
            tempCompare = this.Salesman_Key.CompareTo(Y.Salesman_Key);
            if (tempCompare != 0)
                return tempCompare;

            return 0;
        }
        #endregion
    }
    public class FamousCommission : IComparable
	{
		#region Vars
		public string SoNo;
		public double Qnt;
		public double Amount;
		public string VendorId;
		#endregion

		#region Constructor
		public FamousCommission(string soNo)
		{
			SoNo = soNo;
			Qnt = 0;
			Amount = 0;
			VendorId = string.Empty;
		}
		#endregion

		#region IComparable Members
		public int CompareTo(object obj)
		{
			int tempCompare;
			FamousCommission Y = (FamousCommission)obj;

			//Compare SoNo
			tempCompare = this.SoNo.CompareTo(Y.SoNo);
			if (tempCompare != 0)
				return tempCompare;

			return 0;
		}
		#endregion
	}
	public class CommissionItem_Detail : IComparable
	{
		#region Vars
		public DateTime ShipDateTime;
		public string Invdescr;
		public double FOBQnt;
		public double SalesQnt;
		public double Qnt;
		public double EquivQnt;
		public double Weight;
		public string SoNo;
		public string ShipToName;
		public string ShipToCity;
		public string ShipToState;
		public string ShipToZip;
		public string Type;
		public string ShipToDescr;
		public string SalesPerson;
		public string CustName;
		public string CustCity;
		public string CustState;
		public string Style;
		public string Size;
		public string Grade;
		public string Label;
		public string Variety;
		public string Commodity;
		public string Warehouse;
		public string LotId;
		public string GrowerName;
		public double FreightAmt;
		public string CommodityId;
		public string VarietyId;
		public string WarehouseId;
		public double Cost;
		public string FillType;
		public string OutSideSale;
		public double OutsideSaleCommissionRateDbl;
		public double InsideSaleCommissionRateDbl;

		#region Properties
		public double Profit
		{
			get
			{
				return (FOBQnt - Cost);
			}
		}
		public double TotalFOBPerUnit
		{
			get
			{
				if (Qnt == 0)
					return 0;

				return (FOBQnt / Qnt);
			}
		}
		public double Commission
		{
			get
			{
				double ret = 0;

				switch (OutSideSale.ToUpper())
				{
					case "Y":
						ret = (Profit * OutsideSaleCommissionRateDbl);
						break;

					case "N":
						ret = (FOBQnt * InsideSaleCommissionRateDbl);
						break;

					default:
						ret = (FOBQnt * InsideSaleCommissionRateDbl);
						break;
				}

				return ret;
			}
		}
		public double CommissionPerUnit
		{
			get
			{
				if (Qnt == 0)
					return 0;

				return (Commission / Qnt);
			}
		}
		#endregion
		#endregion

		#region Constructor
		public CommissionItem_Detail(DateTime shipdatetime, string invdescr, string sono, string shiptoname, string shiptocity, string shiptostate, string shiptozip, string type, string shiptodescr, string salesperson, string custname, string custcity, string custstate, string style, string size, string grade, string label, string variety, string commodity, string warehouse, string lotid, string growername, string commodityId, string varietyId, string warehouseId, string fillType)
		{
			ShipDateTime = shipdatetime;
			Invdescr = invdescr;
			FOBQnt = 0;
			SalesQnt = 0;
			Qnt = 0;
			EquivQnt = 0;
			Weight = 0;
			SoNo = sono;
			ShipToName = shiptoname;
			ShipToCity = shiptocity;
			ShipToState = shiptostate;
			ShipToZip = shiptozip;
			Type = type;
			ShipToDescr = shiptodescr;
			SalesPerson = salesperson;
			CustName = custname;
			CustCity = custcity;
			CustState = custstate;
			Style = style;
			Size = size;
			Grade = grade;
			Label = label;
			Variety = variety;
			Commodity = commodity;
			Warehouse = warehouse;
			LotId = lotid;
			GrowerName = growername;
			CommodityId = commodityId;
			VarietyId = varietyId;
			WarehouseId = warehouseId;
			Cost = 0;
			FillType = fillType;
			OutSideSale = string.Empty;
			OutsideSaleCommissionRateDbl = 0;
			InsideSaleCommissionRateDbl = 0;
		}
		#endregion

		#region IComparable Members
		public int CompareTo(object obj)
		{
			int tempCompare;
			CommissionItem_Detail Y = (CommissionItem_Detail)obj;

			if ((tempCompare = this.ShipDateTime.CompareTo(Y.ShipDateTime)) != 0)
				return tempCompare;
			else if ((tempCompare = this.SoNo.CompareTo(Y.SoNo)) != 0)
				return tempCompare;
			else if ((tempCompare = this.Invdescr.CompareTo(Y.Invdescr)) != 0)
				return tempCompare;
			else if ((tempCompare = this.ShipToName.CompareTo(Y.ShipToName)) != 0)
				return tempCompare;
			else if ((tempCompare = this.ShipToCity.CompareTo(Y.ShipToCity)) != 0)
				return tempCompare;
			else if ((tempCompare = this.ShipToState.CompareTo(Y.ShipToState)) != 0)
				return tempCompare;
			else if ((tempCompare = this.ShipToZip.CompareTo(Y.ShipToZip)) != 0)
				return tempCompare;
			else if ((tempCompare = this.Type.CompareTo(Y.Type)) != 0)
				return tempCompare;
			else if ((tempCompare = this.ShipToDescr.CompareTo(Y.ShipToDescr)) != 0)
				return tempCompare;
			else if ((tempCompare = this.SalesPerson.CompareTo(Y.SalesPerson)) != 0)
				return tempCompare;
			else if ((tempCompare = this.CustName.CompareTo(Y.CustName)) != 0)
				return tempCompare;
			else if ((tempCompare = this.CustCity.CompareTo(Y.CustCity)) != 0)
				return tempCompare;
			else if ((tempCompare = this.CustState.CompareTo(Y.CustState)) != 0)
				return tempCompare;
			else if ((tempCompare = this.Style.CompareTo(Y.Style)) != 0)
				return tempCompare;
			else if ((tempCompare = this.Size.CompareTo(Y.Size)) != 0)
				return tempCompare;
			else if ((tempCompare = this.Grade.CompareTo(Y.Grade)) != 0)
				return tempCompare;
			else if ((tempCompare = this.Label.CompareTo(Y.Label)) != 0)
				return tempCompare;
			else if ((tempCompare = this.Variety.CompareTo(Y.Variety)) != 0)
				return tempCompare;
			else if ((tempCompare = this.Commodity.CompareTo(Y.Commodity)) != 0)
				return tempCompare;
			else if ((tempCompare = this.Warehouse.CompareTo(Y.Warehouse)) != 0)
				return tempCompare;
			else if ((tempCompare = this.LotId.CompareTo(Y.LotId)) != 0)
				return tempCompare;
			else if ((tempCompare = this.GrowerName.CompareTo(Y.GrowerName)) != 0)
				return tempCompare;
			else if ((tempCompare = this.CommodityId.CompareTo(Y.CommodityId)) != 0)
				return tempCompare;
			else if ((tempCompare = this.VarietyId.CompareTo(Y.VarietyId)) != 0)
				return tempCompare;
			else if ((tempCompare = this.WarehouseId.CompareTo(Y.WarehouseId)) != 0)
				return tempCompare;
			else if ((tempCompare = this.FillType.CompareTo(Y.FillType)) != 0)
				return tempCompare;

			return 0;
		}
		#endregion
	}
    public class Commission_Order : IComparable
    {
        #region Vars
        public int OrderKeyInt;
        public int SalesmanInt;
        public double InPaidDbl;
        public double OutPaidDbl;
        public double InCurrentDbl;
        public double OutCurrentDbl;
        public double InNetDbl;
        public double OutNetDbl;
        public bool InvoicePaidBln;
        public ArrayList CommissionOrderDetailArl;
        public ArrayList CommissionPaidDetailArl;
        #endregion
        
        #region Constructor
        public Commission_Order()
        {
            OrderKeyInt = 0;
            SalesmanInt = 0;
            InPaidDbl = 0;
            OutPaidDbl = 0;
            InCurrentDbl = 0;
            OutCurrentDbl = 0;
            InNetDbl = 0;
            OutNetDbl = 0;
            InvoicePaidBln = false;
            CommissionOrderDetailArl = new ArrayList();
            CommissionPaidDetailArl = new ArrayList();
        }
        public Commission_Order(int orderKeyInt, int salesmanInt)
        {
            OrderKeyInt = orderKeyInt;
            SalesmanInt = salesmanInt;
            InPaidDbl = 0;
            OutPaidDbl = 0;
            InCurrentDbl = 0;
            OutCurrentDbl = 0;
            InNetDbl = 0;
            OutNetDbl = 0;
            InvoicePaidBln = false;
            CommissionOrderDetailArl = new ArrayList();
            CommissionPaidDetailArl = new ArrayList();
        }
        #endregion

        #region IComparable Members
        public int CompareTo(object obj)
        {
            int tempCompare;
            Commission_Order Y = (Commission_Order)obj;

            //Compare OrderKey
            tempCompare = this.OrderKeyInt.CompareTo(Y.OrderKeyInt);
            if (tempCompare != 0)
                return tempCompare;

            //Compare SalesmanInt
            tempCompare = this.SalesmanInt.CompareTo(Y.SalesmanInt);
            if (tempCompare != 0)
                return tempCompare;

            return 0;
        }
        #endregion
    }
    public class Commission_Order_Detail : IComparable
    {
        #region Vars
        public string DateStr;
        public string LoadStr;
        public string CustomerStr;
        public string ItemStr;
        public double QntDbl;
        public double FOBDbl;
        public double CostDbl;
        public double CommissionAmtDbl;
        public bool InBln;
        #endregion

        #region Constructor
        public Commission_Order_Detail()
        {
            DateStr = "";
            LoadStr = "";
            CustomerStr = "";
            ItemStr = "";
            QntDbl = 0;
            FOBDbl = 0;
            CostDbl = 0;
            CommissionAmtDbl = 0;
            InBln = false;
        }
        public Commission_Order_Detail(string itemStr, bool inBln)
        {
            DateStr = "";
            LoadStr = "";
            CustomerStr = "";
            ItemStr = itemStr;
            QntDbl = 0;
            FOBDbl = 0;
            CostDbl = 0;
            CommissionAmtDbl = 0;
            InBln = inBln;

        }
        #endregion

        #region IComparable Members
        public int CompareTo(object obj)
        {
            int tempCompare;
            Commission_Order_Detail Y = (Commission_Order_Detail)obj;

            //Compare Item
            tempCompare = this.ItemStr.CompareTo(Y.ItemStr);
            if (tempCompare != 0)
                return tempCompare;

            //Compare InBln
            tempCompare = this.InBln.CompareTo(Y.InBln);
            if (tempCompare != 0)
                return tempCompare;
            
            return 0;
        }
        #endregion
    }
    public class CommissionPaidDetails
    {
        #region Vars
        public int ReportNo;
        public double InPaid;
        public double OutPaid;
        #endregion

        #region Constructor
        public CommissionPaidDetails()
        {
            ReportNo = 0;
            InPaid = 0;
            OutPaid = 0;
        }
        #endregion
    }
	public class ExcludedCommodity : IComparable
	{
		#region Vars
		public int CommodityKey;
		#endregion

		#region Constructor
		public ExcludedCommodity(int commodityKey)
		{
			CommodityKey = commodityKey;
		}
		#endregion

		#region IComparable Members
		public int CompareTo(object obj)
		{
			int tempCompare;
			ExcludedCommodity Y = (ExcludedCommodity)obj;

			//Compare CommodityKey
			tempCompare = this.CommodityKey.CompareTo(Y.CommodityKey);
			if (tempCompare != 0)
				return tempCompare;

			return 0;
		}
		#endregion
	}
	public class ExcludedCustomer : IComparable
	{
		#region Vars
		public int CustomerKey;
		#endregion

		#region Constructor
		public ExcludedCustomer(int customerKey)
		{
			CustomerKey = customerKey;
		}
		#endregion

		#region IComparable Members
		public int CompareTo(object obj)
		{
			int tempCompare;
			ExcludedCustomer Y = (ExcludedCustomer)obj;

			//Compare CustomerKey
			tempCompare = this.CustomerKey.CompareTo(Y.CustomerKey);
			if (tempCompare != 0)
				return tempCompare;

			return 0;
		}
		#endregion
	}
	public struct SalesPerson
	{
		#region Vars
		public string SalesPersonNameStr;
		public string SalesPersonKeyStr;
		public double FOBQntDbl;
		public double SalesQntDbl;
		public double QntDbl;
		public double EquivQntDbl;
		public double DueDbl;
		public string PDFFileNameStr;
		#endregion

		#region Constructor
		public SalesPerson(string SalesPersonNameStr, string SalesPersonKeyStr)
		{
			this.SalesPersonNameStr = SalesPersonNameStr;
			this.SalesPersonKeyStr = SalesPersonKeyStr;
			this.FOBQntDbl = 0;
			this.SalesQntDbl = 0;
			this.QntDbl = 0;
			this.EquivQntDbl = 0;
			this.DueDbl = 0;
			this.PDFFileNameStr = "";
		}
		#endregion
	}
	#endregion
}
