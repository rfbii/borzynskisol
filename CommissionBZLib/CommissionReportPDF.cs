﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Collections;
using KeyCentral.Functions;

namespace CommissionBZLib
{
    public class CommissionReportPDF
    {
        #region Vars
        private Document document;
        private PdfWriter PDFWriter;
        private int lineCount = 0;
        private int linesPerPage = 38;

        double SubTotalInAllFOBPerQntDbl = 0;
        double SubTotalInCommissionAmtDbl = 0;
        double SubTotalInCommissionAmtPerQntDbl = 0;
        #endregion

        #region Constructor
        public CommissionReportPDF()
        {
        }
        #endregion

        #region Display Code
        public void Main(string saveFolderLocationStr, string fileNameStr, CommissionReportBLL CommissionReportDataBLL, string salesPersonNameStr, string reportNumberStr, string CommentLineStr, bool PendingBln)
        {
            try
            {
                //if temp folder doesn't already exist on client's computer, create the folder
                DirectoryInfo directoryInfo = new DirectoryInfo(saveFolderLocationStr);

                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }

                document = new Document(PageSize.LETTER.Rotate(), 18, 18, 18, 18);

                // creation of the different writers
                PDFWriter = PdfWriter.GetInstance(document, new FileStream(saveFolderLocationStr + "\\" + fileNameStr, FileMode.Create));
                PDFWriter.ViewerPreferences = PdfWriter.PageModeUseOutlines;

                #region Footer
                //HeaderFooter footer = new HeaderFooter(new Phrase("Page ", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)), true);
                //footer.Border = iTextSharp.text.Rectangle.NO_BORDER;
                //footer.Alignment = Element.ALIGN_CENTER;
                //document.Footer = footer;

                string ReportNameStr = "";

                if (!PendingBln)
                {
                    ReportNameStr = "New Commission Report";
                }
                else if (PendingBln)
                {
                    ReportNameStr = "Pending Commission Report";
                }

                PDFCustomHeaderFooter pdfCustomHeaderFooter = new PDFCustomHeaderFooter(ReportNameStr);
                PDFWriter.PageEvent = pdfCustomHeaderFooter;
                #endregion

                document.Open();

                PdfPTable pdfPTable = new PdfPTable(1);
                PdfPCell detailCell = new PdfPCell();

                float tableWidth = 0;

                float dateWidth = 55f;
                float loadNumberWidth = 55f;
                float customerWidth = 100f;
                float itemWidth = 55f;
                float unitWidth = 45f;
                float totalFOBWidth = 65f;
                float fobWidth = 45f;
                float commissionWidth = 60f;
                float perUnitWidth = 45f;
                float paidWidth = 55f;
                float dueWidth = 55f;
                float reportNumberWidth = 30f;
                float ossWidth = 25f;

                //Loop for each Saleman.
                bool firstTimeBln = true;
                string GrandTotalSalesmanInQntStr = "";
                string GrandTotalSalesmanInFOBStr = "";
                string GrandTotalSalesmanInCommissionStr = "";
                string GrandTotalSalesmanInNetStr = "";
                double SubTotalInQntDbl = 0;
                double SubTotalInFOBDbl = 0;
                //double SubTotalTotalFOBPerQntDbl = 0;
                double SubTotalInCommissionDbl = 0;
                //double SubTotalInCommissionPerUnitDbl = 0;
                double SubTotalInPaidDbl = 0;
                double SubTotalInNetDbl = 0;

                for (int x = 0; x < CommissionReportDataBLL.SalesPersonArl.Count; x++)
                {

                    SalesPerson tempSalesman = (SalesPerson)CommissionReportDataBLL.SalesPersonArl[x];

                    #region Report Header
                    Paragraph paragraph = new Paragraph();
                    paragraph.Alignment = Element.ALIGN_CENTER;
                    paragraph.Font = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD);
                    paragraph.Add(ReportNameStr);
                    paragraph.SpacingAfter = 2f;
                    paragraph.SpacingBefore = 0f;
                    paragraph.Leading = 10f;
                    document.Add(paragraph);

                    paragraph = new Paragraph();
                    paragraph.Alignment = Element.ALIGN_CENTER;
                    paragraph.Font = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD);

                    string commentLineStr = CommentLineStr.Trim();
                    //Marty Changed
                    //if (tempSalesman.SalesPersonKeyStr == tempCommissionOrder.SalesmanInt.ToString() && (tempCommissionOrder.InCurrentDbl != 0 || tempCommissionOrder.InNetDbl != 0 || tempCommissionOrder.InPaidDbl != 0))
                    //{
                    //    if (tempCommissionOrder.CommissionPaidDetailArl.Count == 1)
                    //    {
                    //        CommissionPaidDetails tempPaidDetails = (CommissionPaidDetails)tempCommissionOrder.CommissionPaidDetailArl[0];
                    //        if (tempPaidDetails.InPaid != 0)
                    //        {
                    //            reportNumberStr = Convert.ToString(tempPaidDetails.ReportNo);
                    //        }
                    //    }
                    //    commentLineStr += " ; Report Number: " + reportNumberStr + " ; Sales Person: " + tempSalesman.SalesPersonNameStr;
                    //}
                    commentLineStr += " ; Report Number: " + reportNumberStr; //Removed 6/26/2012 -- Alan // +" ; Sales Person: " + tempSalesman.SalesPersonNameStr;

                    paragraph.Add(commentLineStr);
                    paragraph.SpacingAfter = 2f;
                    paragraph.SpacingBefore = 0f;
                    paragraph.Leading = 10f;
                    document.Add(paragraph);
                    #endregion

                    #region Report
                    #region Column Header
                    pdfPTable = new PdfPTable(13);
                    pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                    tableWidth = 0;

                    dateWidth = 55f;
                    loadNumberWidth = 55f;
                    customerWidth = 100f;
                    itemWidth = 55f;
                    unitWidth = 45f;
                    totalFOBWidth = 65f;
                    fobWidth = 45f;
                    commissionWidth = 60f;
                    perUnitWidth = 45f;
                    paidWidth = 55f;
                    dueWidth = 55f;
                    reportNumberWidth = 30f;
                    ossWidth = 25f;

                    tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                    pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                    pdfPTable.TotalWidth = tableWidth;
                    //table.KeepTogether = true;
                    pdfPTable.SplitRows = true;
                    pdfPTable.SplitLate = false;
                    pdfPTable.LockedWidth = true;
                    pdfPTable.DefaultCell.Padding = 0;
                    //pdfPTable.WidthPercentage = 100; // percentage
                    pdfPTable.DefaultCell.BorderWidth = 1f;
                    pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                    detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    pdfPTable.HeaderRows = 1; // this is the end of the table header
                    #endregion
                    /*
                    for (int z = 0; z < CommissionReportDataBLL.CommissionOrderArl.Count; z++)
                    {
                        Commission_Order tempCommissionOrder = (Commission_Order)CommissionReportDataBLL.CommissionOrderArl[z];
                    */
                  

                    if (firstTimeBln) //if around inside sales
                    {
                        firstTimeBln = false;

                        // SalesPerson tempSalesPerson = (SalesPerson)CommissionReportDataBLL.SalesPersonArl[i];
                        #region Inside Sales
                        for (int y = 0; y < CommissionReportDataBLL.CommissionOrderArl.Count; y++)
                        {
                            Commission_Order tempCommissionOrder = (Commission_Order)CommissionReportDataBLL.CommissionOrderArl[y];

                            if (tempSalesman.SalesPersonKeyStr == tempCommissionOrder.SalesmanInt.ToString())
                            {
                                double totalOrderQnt = 0;
                                double totalFOBAmt = 0;
                                string LoadNumberDetail = "";
                                string CustomerNameDetail = "";
                                for (int j = 0; j < tempCommissionOrder.CommissionOrderDetailArl.Count; j++)
                                {
                                    Commission_Order_Detail tempCommissionOrderDetail = (Commission_Order_Detail)tempCommissionOrder.CommissionOrderDetailArl[j];
                                    if (tempCommissionOrderDetail.InBln)
                                    {
                                        #region Print Details

                                        string ShipDateDetail = Misc.FormatDateTime(tempCommissionOrderDetail.DateStr);
                                        if (tempCommissionOrderDetail.LoadStr != "")
                                            LoadNumberDetail = StringFormater.FormatString(tempCommissionOrderDetail.LoadStr, 10);
                                        if (tempCommissionOrderDetail.CustomerStr != "")
                                            CustomerNameDetail = StringFormater.FormatString(tempCommissionOrderDetail.CustomerStr, 15);

                                        string ItemDetail = "";

                                        if (tempCommissionOrderDetail.ItemStr != "")
                                            ItemDetail = StringFormater.FormatString(tempCommissionOrderDetail.ItemStr, 12);


                                        string QntDetail = Misc.DisplayFormat(tempCommissionOrderDetail.QntDbl);
                                        totalOrderQnt += tempCommissionOrderDetail.QntDbl;

                                        string TotalFOBDetail = RemoveZeroCurrency(tempCommissionOrderDetail.FOBDbl);
                                        string FOBDetail = RemoveZeroCurrency(tempCommissionOrderDetail.FOBDbl / tempCommissionOrderDetail.QntDbl);
                                        totalFOBAmt += tempCommissionOrderDetail.FOBDbl;

                                        string CommissionDetail = RemoveZeroCurrency(tempCommissionOrderDetail.FOBDbl * CommissionReportDataBLL.InsideSaleCommissionRateDbl);
                                        string CommissionPerUnitDetail = RemoveZeroCurrency((tempCommissionOrderDetail.FOBDbl * CommissionReportDataBLL.InsideSaleCommissionRateDbl) / tempCommissionOrderDetail.QntDbl);

                                        string OutSideSaleDetail = "N";

                                        detailCell = new PdfPCell(new Phrase(ShipDateDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        detailCell.NoWrap = true;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase(LoadNumberDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        detailCell.NoWrap = true;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase(CustomerNameDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        detailCell.NoWrap = true;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase(ItemDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        detailCell.NoWrap = true;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase(QntDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        detailCell.NoWrap = true;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase(TotalFOBDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        detailCell.NoWrap = true;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase(FOBDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        detailCell.NoWrap = true;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase(CommissionDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        detailCell.NoWrap = true;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase(CommissionPerUnitDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        detailCell.NoWrap = true;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        detailCell.NoWrap = true;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        detailCell.NoWrap = true;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        detailCell.NoWrap = true;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase(OutSideSaleDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        detailCell.NoWrap = true;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        lineCount += 1;

                                        if (lineCount > linesPerPage)
                                        {
                                            document.Add(pdfPTable);
                                            lineCount = 0;
                                            document.NewPage();

                                            #region Column Header
                                            pdfPTable = new PdfPTable(13);
                                            pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                                            tableWidth = 0;

                                            dateWidth = 55f;
                                            loadNumberWidth = 55f;
                                            customerWidth = 100f;
                                            itemWidth = 55f;
                                            unitWidth = 45f;
                                            totalFOBWidth = 65f;
                                            fobWidth = 45f;
                                            commissionWidth = 60f;
                                            perUnitWidth = 45f;
                                            paidWidth = 55f;
                                            dueWidth = 55f;
                                            reportNumberWidth = 30f;
                                            ossWidth = 25f;

                                            tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                                            pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                                            pdfPTable.TotalWidth = tableWidth;
                                            //table.KeepTogether = true;
                                            pdfPTable.SplitRows = true;
                                            pdfPTable.SplitLate = false;
                                            pdfPTable.LockedWidth = true;
                                            pdfPTable.DefaultCell.Padding = 0;
                                            //pdfPTable.WidthPercentage = 100; // percentage
                                            pdfPTable.DefaultCell.BorderWidth = 1f;
                                            pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                            pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                                            detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            pdfPTable.DefaultCell.Padding = 0;
                                            pdfPTable.AddCell(detailCell);

                                            detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            pdfPTable.DefaultCell.Padding = 0;
                                            pdfPTable.AddCell(detailCell);

                                            detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            pdfPTable.DefaultCell.Padding = 0;
                                            pdfPTable.AddCell(detailCell);

                                            detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            pdfPTable.DefaultCell.Padding = 0;
                                            pdfPTable.AddCell(detailCell);

                                            detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            pdfPTable.DefaultCell.Padding = 0;
                                            pdfPTable.AddCell(detailCell);

                                            detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            pdfPTable.DefaultCell.Padding = 0;
                                            pdfPTable.AddCell(detailCell);

                                            detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            pdfPTable.DefaultCell.Padding = 0;
                                            pdfPTable.AddCell(detailCell);

                                            detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            pdfPTable.DefaultCell.Padding = 0;
                                            pdfPTable.AddCell(detailCell);

                                            detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            pdfPTable.DefaultCell.Padding = 0;
                                            pdfPTable.AddCell(detailCell);

                                            detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            pdfPTable.DefaultCell.Padding = 0;
                                            pdfPTable.AddCell(detailCell);

                                            detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            pdfPTable.DefaultCell.Padding = 0;
                                            pdfPTable.AddCell(detailCell);

                                            detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            pdfPTable.DefaultCell.Padding = 0;
                                            pdfPTable.AddCell(detailCell);

                                            detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            pdfPTable.DefaultCell.Padding = 0;
                                            pdfPTable.AddCell(detailCell);

                                            pdfPTable.HeaderRows = 1; // this is the end of the table header
                                            #endregion
                                        }

                                        #endregion
                                    }
                                }
                                if (totalOrderQnt != 0)
                                {
                                    #region Print Total
                                    string LoadNumberItem = LoadNumberDetail;
                                    string CustomerNameItem = CustomerNameDetail;
                                    string SubInQntDetail = DisplayFormat(totalOrderQnt);
                                    string SubInFOBDetail = RemoveZeroCurrency(totalFOBAmt);
                                    string SubInCommissionDetail = RemoveZeroCurrency(tempCommissionOrder.InCurrentDbl);
                                    string SubInPaid = RemoveZeroCurrency(tempCommissionOrder.InPaidDbl);
                                    string SubInNet = RemoveZeroCurrency(tempCommissionOrder.InNetDbl);

                                    string ReportNumberItem = "";

                                    if (tempCommissionOrder.CommissionPaidDetailArl.Count != 0)
                                    {
                                        CommissionPaidDetails tempPaidDetails = (CommissionPaidDetails)tempCommissionOrder.CommissionPaidDetailArl[0];
                                        if (tempPaidDetails.InPaid != 0)
                                        {
                                            ReportNumberItem = Convert.ToString(tempPaidDetails.ReportNo);
                                        }

                                        #region not used now print paid details
                                        //for (int t = 0; t < tempCommissionOrder.CommissionPaidDetailArl.Count; t++)
                                        //{
                                        //    CommissionPaidDetails tempPaidDetails = (CommissionPaidDetails)tempCommissionOrder.CommissionPaidDetailArl[t];

                                        //    if (tempPaidDetails.InPaid != 0)
                                        //    {
                                        //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        //        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        //        detailCell.NoWrap = true;
                                        //        pdfPTable.DefaultCell.Padding = 0;
                                        //        pdfPTable.AddCell(detailCell);

                                        //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        //        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                        //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        //        detailCell.NoWrap = true;
                                        //        pdfPTable.DefaultCell.Padding = 0;
                                        //        pdfPTable.AddCell(detailCell);

                                        //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        //        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                        //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        //        detailCell.NoWrap = true;
                                        //        pdfPTable.DefaultCell.Padding = 0;
                                        //        pdfPTable.AddCell(detailCell);

                                        //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        //        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        //        detailCell.NoWrap = true;
                                        //        pdfPTable.DefaultCell.Padding = 0;
                                        //        pdfPTable.AddCell(detailCell);

                                        //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                        //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        //        detailCell.NoWrap = true;
                                        //        pdfPTable.DefaultCell.Padding = 0;
                                        //        pdfPTable.AddCell(detailCell);

                                        //        detailCell = new PdfPCell(new Phrase(SubInFOBDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                        //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        //        detailCell.NoWrap = true;
                                        //        pdfPTable.DefaultCell.Padding = 0;
                                        //        pdfPTable.AddCell(detailCell);

                                        //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                        //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        //        detailCell.NoWrap = true;
                                        //        pdfPTable.DefaultCell.Padding = 0;
                                        //        pdfPTable.AddCell(detailCell);

                                        //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                        //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        //        detailCell.NoWrap = true;
                                        //        pdfPTable.DefaultCell.Padding = 0;
                                        //        pdfPTable.AddCell(detailCell);

                                        //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                        //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        //        detailCell.NoWrap = true;
                                        //        pdfPTable.DefaultCell.Padding = 0;
                                        //        pdfPTable.AddCell(detailCell);

                                        //        detailCell = new PdfPCell(new Phrase(SubInPaid, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                        //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        //        detailCell.NoWrap = true;
                                        //        pdfPTable.DefaultCell.Padding = 0;
                                        //        pdfPTable.AddCell(detailCell);

                                        //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                        //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        //        detailCell.NoWrap = true;
                                        //        pdfPTable.DefaultCell.Padding = 0;
                                        //        pdfPTable.AddCell(detailCell);

                                        //        detailCell = new PdfPCell(new Phrase(ReportNumberItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        //        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                        //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        //        detailCell.NoWrap = true;
                                        //        pdfPTable.DefaultCell.Padding = 0;
                                        //        pdfPTable.AddCell(detailCell);

                                        //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                        //        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                        //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        //        detailCell.NoWrap = true;
                                        //        pdfPTable.DefaultCell.Padding = 0;
                                        //        pdfPTable.AddCell(detailCell);
                                        //    }

                                        //}
                                        #endregion
                                    }

                                    detailCell = new PdfPCell(new Phrase("Total", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(LoadNumberItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(CustomerNameItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Total", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(SubInQntDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(SubInFOBDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(SubInCommissionDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(SubInPaid, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(SubInNet, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(ReportNumberItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    lineCount += 1;

                                    if (lineCount > linesPerPage)
                                    {
                                        document.Add(pdfPTable);
                                        lineCount = 0;
                                        document.NewPage();

                                        #region Column Header
                                        pdfPTable = new PdfPTable(13);
                                        pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                                        tableWidth = 0;

                                        dateWidth = 55f;
                                        loadNumberWidth = 55f;
                                        customerWidth = 100f;
                                        itemWidth = 55f;
                                        unitWidth = 45f;
                                        totalFOBWidth = 65f;
                                        fobWidth = 45f;
                                        commissionWidth = 60f;
                                        perUnitWidth = 45f;
                                        paidWidth = 55f;
                                        dueWidth = 55f;
                                        reportNumberWidth = 30f;
                                        ossWidth = 25f;

                                        tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                                        pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                                        pdfPTable.TotalWidth = tableWidth;
                                        //table.KeepTogether = true;
                                        pdfPTable.SplitRows = true;
                                        pdfPTable.SplitLate = false;
                                        pdfPTable.LockedWidth = true;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        //pdfPTable.WidthPercentage = 100; // percentage
                                        pdfPTable.DefaultCell.BorderWidth = 1f;
                                        pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                                        detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        pdfPTable.HeaderRows = 1; // this is the end of the table header
                                        #endregion
                                    }

                                    SubTotalInQntDbl += totalOrderQnt;

                                    SubTotalInFOBDbl += totalFOBAmt;

                                    if (SubTotalInFOBDbl != 0)
                                    {
                                        SubTotalInAllFOBPerQntDbl = SubTotalInFOBDbl / SubTotalInQntDbl;
                                    }

                                    SubTotalInCommissionDbl += tempCommissionOrder.InCurrentDbl;

                                    if (SubTotalInQntDbl != 0)
                                    {
                                        SubTotalInCommissionAmtPerQntDbl = SubTotalInCommissionDbl / SubTotalInQntDbl;
                                    }

                                    SubTotalInPaidDbl += tempCommissionOrder.InPaidDbl;
                                    SubTotalInNetDbl += tempCommissionOrder.InNetDbl;

                                    #endregion
                                }

                            }
                        }


                        #region Sub Total
                        string GrandTotalInQntStr = DisplayFormat(SubTotalInQntDbl);
                        string GrandTotalInFOBStr = RemoveZeroCurrency(SubTotalInFOBDbl);
                        string GrandTotalInCommissionStr = RemoveZeroCurrency(SubTotalInCommissionDbl);
                        string GrandTotalInNetStr = RemoveZeroCurrency(SubTotalInNetDbl);

                        detailCell = new PdfPCell(new Phrase("Grand Total Inside Sales", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        detailCell.NoWrap = true;
                        detailCell.Colspan = 3;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(GrandTotalInQntStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        detailCell.NoWrap = true;
                        detailCell.Colspan = 2;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(GrandTotalInFOBStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        detailCell.NoWrap = true;
                        detailCell.Colspan = 2;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(GrandTotalInCommissionStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        detailCell.NoWrap = true;
                        detailCell.Colspan = 2;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(GrandTotalInNetStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        detailCell.NoWrap = true;
                        detailCell.Colspan = 2;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(reportNumberStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        detailCell.NoWrap = true;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        detailCell.NoWrap = true;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        lineCount += 1;

                        if (lineCount > linesPerPage)
                        {
                            document.Add(pdfPTable);
                            lineCount = 0;
                            document.NewPage();

                            #region Column Header
                            pdfPTable = new PdfPTable(13);
                            pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                            tableWidth = 0;

                            dateWidth = 55f;
                            loadNumberWidth = 55f;
                            customerWidth = 100f;
                            itemWidth = 55f;
                            unitWidth = 45f;
                            totalFOBWidth = 65f;
                            fobWidth = 45f;
                            commissionWidth = 60f;
                            perUnitWidth = 45f;
                            paidWidth = 55f;
                            dueWidth = 55f;
                            reportNumberWidth = 30f;
                            ossWidth = 25f;

                            tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                            pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                            pdfPTable.TotalWidth = tableWidth;
                            //table.KeepTogether = true;
                            pdfPTable.SplitRows = true;
                            pdfPTable.SplitLate = false;
                            pdfPTable.LockedWidth = true;
                            pdfPTable.DefaultCell.Padding = 0;
                            //pdfPTable.WidthPercentage = 100; // percentage
                            pdfPTable.DefaultCell.BorderWidth = 1f;
                            pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                            detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            pdfPTable.HeaderRows = 1; // this is the end of the table header
                            #endregion
                        }
                        #endregion

                        #region Subtotal Salesman
                        GrandTotalSalesmanInQntStr = DisplayFormat(SubTotalInQntDbl / CommissionReportDataBLL.SalesPersonArl.Count);
                        GrandTotalSalesmanInFOBStr = RemoveZeroCurrency(SubTotalInFOBDbl / CommissionReportDataBLL.SalesPersonArl.Count);
                        GrandTotalSalesmanInCommissionStr = RemoveZeroCurrency(SubTotalInCommissionDbl / CommissionReportDataBLL.SalesPersonArl.Count);
                        GrandTotalSalesmanInNetStr = RemoveZeroCurrency(SubTotalInNetDbl / CommissionReportDataBLL.SalesPersonArl.Count);

                        for (int j = 0; j < CommissionReportDataBLL.SalesPersonArl.Count; j++)
                        {
                            SalesPerson temp1Salesman = (SalesPerson)CommissionReportDataBLL.SalesPersonArl[j];

                            detailCell = new PdfPCell(new Phrase(temp1Salesman.SalesPersonNameStr + " Share of Inside Sales", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            detailCell.NoWrap = true;
                            detailCell.Colspan = 3;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(GrandTotalSalesmanInQntStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                            detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            detailCell.NoWrap = true;
                            detailCell.Colspan = 2;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(GrandTotalSalesmanInFOBStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                            detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            detailCell.NoWrap = true;
                            detailCell.Colspan = 2;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(GrandTotalSalesmanInCommissionStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                            detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            detailCell.NoWrap = true;
                            detailCell.Colspan = 2;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(GrandTotalSalesmanInNetStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                            detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            detailCell.NoWrap = true;
                            detailCell.Colspan = 2;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(reportNumberStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            detailCell.NoWrap = true;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                            detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            detailCell.NoWrap = true;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            lineCount += 1;

                        }

                        if (lineCount > linesPerPage)
                        {
                            document.Add(pdfPTable);
                            lineCount = 0;
                            document.NewPage();

                            #region Column Header
                            pdfPTable = new PdfPTable(13);
                            pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                            tableWidth = 0;

                            dateWidth = 55f;
                            loadNumberWidth = 55f;
                            customerWidth = 100f;
                            itemWidth = 55f;
                            unitWidth = 45f;
                            totalFOBWidth = 65f;
                            fobWidth = 45f;
                            commissionWidth = 60f;
                            perUnitWidth = 45f;
                            paidWidth = 55f;
                            dueWidth = 55f;
                            reportNumberWidth = 30f;
                            ossWidth = 25f;

                            tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                            pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                            pdfPTable.TotalWidth = tableWidth;
                            //table.KeepTogether = true;
                            pdfPTable.SplitRows = true;
                            pdfPTable.SplitLate = false;
                            pdfPTable.LockedWidth = true;
                            pdfPTable.DefaultCell.Padding = 0;
                            //pdfPTable.WidthPercentage = 100; // percentage
                            pdfPTable.DefaultCell.BorderWidth = 1f;
                            pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                            detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.DefaultCell.Padding = 0;
                            pdfPTable.AddCell(detailCell);

                            pdfPTable.HeaderRows = 1; // this is the end of the table header
                            #endregion
                        }
                        #endregion
                        if (lineCount != 0)
                        {
                            document.Add(pdfPTable);
                        }

                        #endregion
                    }
                    #region Outside Sales
                    #region Header
                    pdfPTable = new PdfPTable(13);
                    pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                    tableWidth = 0;

                    dateWidth = 55f;
                    loadNumberWidth = 55f;
                    customerWidth = 100f;
                    itemWidth = 55f;
                    unitWidth = 45f;
                    totalFOBWidth = 65f;
                    fobWidth = 45f;
                    commissionWidth = 60f;
                    perUnitWidth = 45f;
                    paidWidth = 55f;
                    dueWidth = 55f;
                    reportNumberWidth = 30f;
                    ossWidth = 25f;

                    tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                    pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                    pdfPTable.TotalWidth = tableWidth;
                    //table.KeepTogether = true;
                    pdfPTable.SplitRows = true;
                    pdfPTable.SplitLate = false;
                    pdfPTable.LockedWidth = true;
                    pdfPTable.DefaultCell.Padding = 0;
                    //pdfPTable.WidthPercentage = 100; // percentage
                    pdfPTable.DefaultCell.BorderWidth = 1f;
                    pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    #endregion


                    #region Header
                    detailCell = new PdfPCell(new Phrase(tempSalesman.SalesPersonNameStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 13;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    lineCount += 1;

                    if (lineCount > linesPerPage)
                    {
                        document.Add(pdfPTable);
                        lineCount = 0;
                        document.NewPage();

                        #region Column Header
                        pdfPTable = new PdfPTable(13);
                        pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                        tableWidth = 0;

                        dateWidth = 55f;
                        loadNumberWidth = 55f;
                        customerWidth = 100f;
                        itemWidth = 55f;
                        unitWidth = 45f;
                        totalFOBWidth = 65f;
                        fobWidth = 45f;
                        commissionWidth = 60f;
                        perUnitWidth = 45f;
                        paidWidth = 55f;
                        dueWidth = 55f;
                        reportNumberWidth = 30f;
                        ossWidth = 25f;

                        tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                        pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                        pdfPTable.TotalWidth = tableWidth;
                        //table.KeepTogether = true;
                        pdfPTable.SplitRows = true;
                        pdfPTable.SplitLate = false;
                        pdfPTable.LockedWidth = true;
                        pdfPTable.DefaultCell.Padding = 0;
                        //pdfPTable.WidthPercentage = 100; // percentage
                        pdfPTable.DefaultCell.BorderWidth = 1f;
                        pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                        detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        pdfPTable.HeaderRows = 1; // this is the end of the table header
                        #endregion
                    }

                    detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    lineCount += 1;

                    if (lineCount > linesPerPage)
                    {
                        document.Add(pdfPTable);
                        lineCount = 0;
                        document.NewPage();

                        #region Column Header
                        pdfPTable = new PdfPTable(13);
                        pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                        tableWidth = 0;

                        dateWidth = 55f;
                        loadNumberWidth = 55f;
                        customerWidth = 100f;
                        itemWidth = 55f;
                        unitWidth = 45f;
                        totalFOBWidth = 65f;
                        fobWidth = 45f;
                        commissionWidth = 60f;
                        perUnitWidth = 45f;
                        paidWidth = 55f;
                        dueWidth = 55f;
                        reportNumberWidth = 30f;
                        ossWidth = 25f;

                        tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                        pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                        pdfPTable.TotalWidth = tableWidth;
                        //table.KeepTogether = true;
                        pdfPTable.SplitRows = true;
                        pdfPTable.SplitLate = false;
                        pdfPTable.LockedWidth = true;
                        pdfPTable.DefaultCell.Padding = 0;
                        //pdfPTable.WidthPercentage = 100; // percentage
                        pdfPTable.DefaultCell.BorderWidth = 1f;
                        pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                        detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        pdfPTable.HeaderRows = 1; // this is the end of the table header
                        #endregion
                    }
                    #endregion

                    double totalOutOrderQnt = 0;
                    double totalOutFOBAmt = 0;
                    double SubTotalOutQntDbl = 0;
                    double SubTotalOutFOBDbl = 0;
                    double SubTotalOutTotalFOBPerUnitDbl = 0;
                    double SubTotalOutCommissionDbl = 0;
                    double SubTotalOutCommissionPerUnitDbl = 0;
                    double SubTotalOutPaidDbl = 0;
                    double SubTotalOutNetDbl = 0;

                    for (int y = 0; y < CommissionReportDataBLL.CommissionOrderArl.Count; y++)
                    {
                        Commission_Order tempCommissionOrder = (Commission_Order)CommissionReportDataBLL.CommissionOrderArl[y];

                        if (tempSalesman.SalesPersonKeyStr == tempCommissionOrder.SalesmanInt.ToString())
                        {
                            double totalOrderQnt = 0;
                            double totalFOBAmt = 0;
                            string LoadNumberDetail = "";
                            string CustomerNameDetail = "";
                            for (int j = 0; j < tempCommissionOrder.CommissionOrderDetailArl.Count; j++)
                            {
                                Commission_Order_Detail tempCommissionOrderDetail = (Commission_Order_Detail)tempCommissionOrder.CommissionOrderDetailArl[j];

                                #region Print Details
                                if (!tempCommissionOrderDetail.InBln)
                                {
                                    string ShipDateDetail = Misc.FormatDateTime(tempCommissionOrderDetail.DateStr);

                                    if (tempCommissionOrderDetail.LoadStr != "")
                                        LoadNumberDetail = StringFormater.FormatString(tempCommissionOrderDetail.LoadStr, 10);

                                    if (tempCommissionOrderDetail.CustomerStr != "")
                                        CustomerNameDetail = StringFormater.FormatString(tempCommissionOrderDetail.CustomerStr, 15);

                                    string ItemDetail = "";

                                    if (tempCommissionOrderDetail.ItemStr != "")
                                        ItemDetail = StringFormater.FormatString(tempCommissionOrderDetail.ItemStr, 12);

                                    string QntDetail = Misc.DisplayFormat(tempCommissionOrderDetail.QntDbl);
                                    totalOrderQnt += tempCommissionOrderDetail.QntDbl;
                                    string TotalFOBDetail = RemoveZeroCurrency(tempCommissionOrderDetail.FOBDbl);
                                    string FOBDetail = RemoveZeroCurrency(tempCommissionOrderDetail.FOBDbl / tempCommissionOrderDetail.QntDbl);
                                    totalFOBAmt += tempCommissionOrderDetail.FOBDbl;
                                    string CommissionDetail = RemoveZeroCurrency((tempCommissionOrderDetail.FOBDbl - tempCommissionOrderDetail.CostDbl) * CommissionReportDataBLL.OutsideSaleCommissionRateDbl);
                                    string CommissionPerUnitDetail = RemoveZeroCurrency(((tempCommissionOrderDetail.FOBDbl - tempCommissionOrderDetail.CostDbl) * CommissionReportDataBLL.OutsideSaleCommissionRateDbl) / tempCommissionOrderDetail.QntDbl);
                                    string OutSideSaleDetail = "Y";

                                    detailCell = new PdfPCell(new Phrase(ShipDateDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(LoadNumberDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(CustomerNameDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(ItemDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(QntDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(TotalFOBDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(FOBDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(CommissionDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(CommissionPerUnitDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(OutSideSaleDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    lineCount += 1;
                                }

                                if (lineCount > linesPerPage)
                                {
                                    document.Add(pdfPTable);
                                    lineCount = 0;
                                    document.NewPage();

                                    #region Column Header
                                    pdfPTable = new PdfPTable(13);
                                    pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                                    tableWidth = 0;

                                    dateWidth = 55f;
                                    loadNumberWidth = 55f;
                                    customerWidth = 100f;
                                    itemWidth = 55f;
                                    unitWidth = 45f;
                                    totalFOBWidth = 65f;
                                    fobWidth = 45f;
                                    commissionWidth = 60f;
                                    perUnitWidth = 45f;
                                    paidWidth = 55f;
                                    dueWidth = 55f;
                                    reportNumberWidth = 30f;
                                    ossWidth = 25f;

                                    tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                                    pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                                    pdfPTable.TotalWidth = tableWidth;
                                    //table.KeepTogether = true;
                                    pdfPTable.SplitRows = true;
                                    pdfPTable.SplitLate = false;
                                    pdfPTable.LockedWidth = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    //pdfPTable.WidthPercentage = 100; // percentage
                                    pdfPTable.DefaultCell.BorderWidth = 1f;
                                    pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                                    detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    pdfPTable.HeaderRows = 1; // this is the end of the table header
                                    #endregion
                                }

                                #endregion

                            }
                            if (totalOrderQnt != 0)
                            {
                                #region Print Total
                                string LoadNumberItem = LoadNumberDetail;
                                string CustomerNameItem = CustomerNameDetail;

                                string UnitItem = DisplayFormat(totalOrderQnt);

                                string TotalFOBItem = RemoveZeroCurrency(totalFOBAmt);
                                string CommissionItem = RemoveZeroCurrency(tempCommissionOrder.OutCurrentDbl);
                                string PaidItem = RemoveZeroCurrency(tempCommissionOrder.OutPaidDbl);
                                string DueItem = RemoveZeroCurrency(tempCommissionOrder.OutNetDbl);
                                SubTotalOutQntDbl += totalOrderQnt;
                                SubTotalOutFOBDbl += totalFOBAmt;
                                SubTotalOutCommissionDbl += (tempCommissionOrder.OutCurrentDbl);
                                SubTotalOutNetDbl += tempCommissionOrder.OutNetDbl;
                                string ReportNumberItem = "";
                                if (tempCommissionOrder.CommissionPaidDetailArl.Count != 0)
                                {
                                    CommissionPaidDetails tempPaidDetails = (CommissionPaidDetails)tempCommissionOrder.CommissionPaidDetailArl[0];
                                    if (tempPaidDetails.InPaid != 0)
                                    {
                                        ReportNumberItem = Convert.ToString(tempPaidDetails.ReportNo);
                                    }

                                    #region not used now print paid details
                                    //for (int t = 0; t < tempCommissionOrder.CommissionPaidDetailArl.Count; t++)
                                    //{
                                    //    CommissionPaidDetails tempPaidDetails = (CommissionPaidDetails)tempCommissionOrder.CommissionPaidDetailArl[t];

                                    //    if (tempPaidDetails.InPaid != 0)
                                    //    {
                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase(SubInFOBDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase(SubInPaid, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase(ReportNumberItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);
                                    //    }

                                    //}
                                    #endregion
                                }

                                detailCell = new PdfPCell(new Phrase("Total", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(LoadNumberItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(CustomerNameItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase("Total", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(UnitItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(TotalFOBItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(CommissionItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(PaidItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(DueItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(ReportNumberItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                lineCount += 1;

                                if (lineCount > linesPerPage)
                                {
                                    document.Add(pdfPTable);
                                    lineCount = 0;
                                    document.NewPage();

                                    #region Column Header
                                    pdfPTable = new PdfPTable(13);
                                    pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                                    tableWidth = 0;

                                    dateWidth = 55f;
                                    loadNumberWidth = 55f;
                                    customerWidth = 100f;
                                    itemWidth = 55f;
                                    unitWidth = 45f;
                                    totalFOBWidth = 65f;
                                    fobWidth = 45f;
                                    commissionWidth = 60f;
                                    perUnitWidth = 45f;
                                    paidWidth = 55f;
                                    dueWidth = 55f;
                                    reportNumberWidth = 30f;
                                    ossWidth = 25f;

                                    tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                                    pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                                    pdfPTable.TotalWidth = tableWidth;
                                    //table.KeepTogether = true;
                                    pdfPTable.SplitRows = true;
                                    pdfPTable.SplitLate = false;
                                    pdfPTable.LockedWidth = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    //pdfPTable.WidthPercentage = 100; // percentage
                                    pdfPTable.DefaultCell.BorderWidth = 1f;
                                    pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                                    detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    pdfPTable.HeaderRows = 1; // this is the end of the table header
                                    #endregion
                                }
                                #endregion
                            }


                        }
                    }
                    #region Total
                    string TotalOutQntStr = DisplayFormat(SubTotalOutQntDbl);
                    string TotalOutFOBStr = RemoveZeroCurrency(SubTotalOutFOBDbl);
                    string TotalOutCommissionStr = RemoveZeroCurrency(SubTotalOutCommissionDbl);
                    string TotalOutNetStr = RemoveZeroCurrency(SubTotalOutNetDbl);

                    detailCell = new PdfPCell(new Phrase(tempSalesman.SalesPersonNameStr + @" Total Outside", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 3;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(TotalOutQntStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(TotalOutFOBStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(TotalOutCommissionStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(TotalOutNetStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    lineCount += 1;

                    if (lineCount > linesPerPage)
                    {
                        document.Add(pdfPTable);
                        lineCount = 0;
                        document.NewPage();

                        #region Column Header
                        pdfPTable = new PdfPTable(13);
                        pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                        tableWidth = 0;

                        dateWidth = 55f;
                        loadNumberWidth = 55f;
                        customerWidth = 100f;
                        itemWidth = 55f;
                        unitWidth = 45f;
                        totalFOBWidth = 65f;
                        fobWidth = 45f;
                        commissionWidth = 60f;
                        perUnitWidth = 45f;
                        paidWidth = 55f;
                        dueWidth = 55f;
                        reportNumberWidth = 30f;
                        ossWidth = 25f;

                        tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                        pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                        pdfPTable.TotalWidth = tableWidth;
                        //table.KeepTogether = true;
                        pdfPTable.SplitRows = true;
                        pdfPTable.SplitLate = false;
                        pdfPTable.LockedWidth = true;
                        pdfPTable.DefaultCell.Padding = 0;
                        //pdfPTable.WidthPercentage = 100; // percentage
                        pdfPTable.DefaultCell.BorderWidth = 1f;
                        pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                        detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        pdfPTable.HeaderRows = 1; // this is the end of the table header
                        #endregion
                    }
                    #region Subtotal Salesman
                    //string GrandTotalSalesmanInQntStr = DisplayFormat(SubTotalInQntDbl / CommissionReportDataBLL.SalesPersonArl.Count);
                    //string GrandTotalSalesmanInFOBStr = RemoveZeroCurrency(SubTotalInFOBDbl / CommissionReportDataBLL.SalesPersonArl.Count);
                    //string GrandTotalSalesmanInCommissionStr = RemoveZeroCurrency(SubTotalInCommissionDbl / CommissionReportDataBLL.SalesPersonArl.Count);
                    //string GrandTotalSalesmanInNetStr = RemoveZeroCurrency(SubTotalInNetDbl / CommissionReportDataBLL.SalesPersonArl.Count);
                    detailCell = new PdfPCell(new Phrase(tempSalesman.SalesPersonNameStr + " Share of Inside Sales", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 3;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(GrandTotalSalesmanInQntStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(GrandTotalSalesmanInFOBStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(GrandTotalSalesmanInCommissionStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(GrandTotalSalesmanInNetStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(reportNumberStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);



                    lineCount += 1;

                    if (lineCount > linesPerPage)
                    {
                        document.Add(pdfPTable);
                        lineCount = 0;
                        document.NewPage();

                        #region Column Header
                        pdfPTable = new PdfPTable(13);
                        pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                        tableWidth = 0;

                        dateWidth = 55f;
                        loadNumberWidth = 55f;
                        customerWidth = 100f;
                        itemWidth = 55f;
                        unitWidth = 45f;
                        totalFOBWidth = 65f;
                        fobWidth = 45f;
                        commissionWidth = 60f;
                        perUnitWidth = 45f;
                        paidWidth = 55f;
                        dueWidth = 55f;
                        reportNumberWidth = 30f;
                        ossWidth = 25f;

                        tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                        pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                        pdfPTable.TotalWidth = tableWidth;
                        //table.KeepTogether = true;
                        pdfPTable.SplitRows = true;
                        pdfPTable.SplitLate = false;
                        pdfPTable.LockedWidth = true;
                        pdfPTable.DefaultCell.Padding = 0;
                        //pdfPTable.WidthPercentage = 100; // percentage
                        pdfPTable.DefaultCell.BorderWidth = 1f;
                        pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                        detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        pdfPTable.HeaderRows = 1; // this is the end of the table header
                        #endregion
                    }
                    #endregion
                    #region Total Salesman
                    string TotalSalesmanInQntStr = DisplayFormat((SubTotalInQntDbl / CommissionReportDataBLL.SalesPersonArl.Count) + SubTotalOutQntDbl);
                    string TotalSalesmanInFOBStr = RemoveZeroCurrency((SubTotalInFOBDbl / CommissionReportDataBLL.SalesPersonArl.Count) + SubTotalOutFOBDbl);
                    string TotalSalesmanInCommissionStr = RemoveZeroCurrency((SubTotalInCommissionDbl / CommissionReportDataBLL.SalesPersonArl.Count) + SubTotalOutCommissionDbl);
                    string TotalSalesmanInNetStr = RemoveZeroCurrency((SubTotalInNetDbl / CommissionReportDataBLL.SalesPersonArl.Count) + SubTotalOutNetDbl);

                    detailCell = new PdfPCell(new Phrase(tempSalesman.SalesPersonNameStr + " Grand Total", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 3;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(TotalSalesmanInQntStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(TotalSalesmanInFOBStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(TotalSalesmanInCommissionStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(TotalSalesmanInNetStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(reportNumberStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);



                    lineCount += 1;

                    if (lineCount > linesPerPage)
                    {
                        document.Add(pdfPTable);
                        lineCount = 0;
                        document.NewPage();

                        #region Column Header
                        pdfPTable = new PdfPTable(13);
                        pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                        tableWidth = 0;

                        dateWidth = 55f;
                        loadNumberWidth = 55f;
                        customerWidth = 100f;
                        itemWidth = 55f;
                        unitWidth = 45f;
                        totalFOBWidth = 65f;
                        fobWidth = 45f;
                        commissionWidth = 60f;
                        perUnitWidth = 45f;
                        paidWidth = 55f;
                        dueWidth = 55f;
                        reportNumberWidth = 30f;
                        ossWidth = 25f;

                        tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                        pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                        pdfPTable.TotalWidth = tableWidth;
                        //table.KeepTogether = true;
                        pdfPTable.SplitRows = true;
                        pdfPTable.SplitLate = false;
                        pdfPTable.LockedWidth = true;
                        pdfPTable.DefaultCell.Padding = 0;
                        //pdfPTable.WidthPercentage = 100; // percentage
                        pdfPTable.DefaultCell.BorderWidth = 1f;
                        pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                        detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        pdfPTable.HeaderRows = 1; // this is the end of the table header
                        #endregion
                    }
                    #endregion
                    if (lineCount != 0)
                    {
                        document.Add(pdfPTable);
                    }

                    #endregion
                    #endregion

                    #endregion

                    //create a new page for every new sales person
                    //commented out 6/27/12 as not needed.
                    //if (x != CommissionReportDataBLL.SalesPersonArl.Count - 1 && salesPersonNameStr == "")
                    //{
                    //    document.Add(pdfPTable);
                    //    lineCount = 0;
                    //    document.NewPage();
                    //}
                } //end of x for loop 
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (document != null && document.IsOpen())
                {
                    // we close the document
                    document.Close();
                }
            }
        }

        public void Salesman(string saveFolderLocationStr, string fileNameStr, CommissionReportBLL CommissionReportDataBLL, string salesPersonNameStr, string reportNumberStr, string CommentLineStr, bool PendingBln)
        {
            try
            {
                //if temp folder doesn't already exist on client's computer, create the folder
                DirectoryInfo directoryInfo = new DirectoryInfo(saveFolderLocationStr);

                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }

                document = new Document(PageSize.LETTER.Rotate(), 18, 18, 18, 18);

                // creation of the different writers
                PDFWriter = PdfWriter.GetInstance(document, new FileStream(saveFolderLocationStr + "\\" + fileNameStr, FileMode.Create));
                PDFWriter.ViewerPreferences = PdfWriter.PageModeUseOutlines;

                #region Footer
                //HeaderFooter footer = new HeaderFooter(new Phrase("Page ", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)), true);
                //footer.Border = iTextSharp.text.Rectangle.NO_BORDER;
                //footer.Alignment = Element.ALIGN_CENTER;
                //document.Footer = footer;

                string ReportNameStr = "";

                if (!PendingBln)
                {
                    ReportNameStr = "New Commission Report";
                }
                else if (PendingBln)
                {
                    ReportNameStr = "Pending Commission Report";
                }

                PDFCustomHeaderFooter pdfCustomHeaderFooter = new PDFCustomHeaderFooter(ReportNameStr);
                PDFWriter.PageEvent = pdfCustomHeaderFooter;
                #endregion

                document.Open();

                PdfPTable pdfPTable = new PdfPTable(1);
                PdfPCell detailCell = new PdfPCell();

                float tableWidth = 0;

                float dateWidth = 55f;
                float loadNumberWidth = 55f;
                float customerWidth = 100f;
                float itemWidth = 55f;
                float unitWidth = 45f;
                float totalFOBWidth = 65f;
                float fobWidth = 45f;
                float commissionWidth = 60f;
                float perUnitWidth = 45f;
                float paidWidth = 55f;
                float dueWidth = 55f;
                float reportNumberWidth = 30f;
                float ossWidth = 25f;
                //Loop for each Saleman.
                //for (int x = 0; x < CommissionReportDataBLL.SalesPersonArl.Count; x++)
                //{
                SalesPerson tempSalesman = new SalesPerson();
                for (int x = 0; x < CommissionReportDataBLL.SalesPersonArl.Count; x++)
                {
                    tempSalesman = (SalesPerson)CommissionReportDataBLL.SalesPersonArl[x];

                    if (tempSalesman.SalesPersonNameStr == salesPersonNameStr)
                    {
                        break;
                    }
                } 

                
                    #region Report Header
                    Paragraph paragraph = new Paragraph();
                    paragraph.Alignment = Element.ALIGN_CENTER;
                    paragraph.Font = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD);
                    paragraph.Add(ReportNameStr);
                    paragraph.SpacingAfter = 2f;
                    paragraph.SpacingBefore = 0f;
                    paragraph.Leading = 10f;
                    document.Add(paragraph);

                    paragraph = new Paragraph();
                    paragraph.Alignment = Element.ALIGN_CENTER;
                    paragraph.Font = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD);

                    string commentLineStr = CommentLineStr.Trim();
                    //Marty Changed
                    //if (tempSalesman.SalesPersonKeyStr == tempCommissionOrder.SalesmanInt.ToString() && (tempCommissionOrder.InCurrentDbl != 0 || tempCommissionOrder.InNetDbl != 0 || tempCommissionOrder.InPaidDbl != 0))
                    //{
                    //    if (tempCommissionOrder.CommissionPaidDetailArl.Count == 1)
                    //    {
                    //        CommissionPaidDetails tempPaidDetails = (CommissionPaidDetails)tempCommissionOrder.CommissionPaidDetailArl[0];
                    //        if (tempPaidDetails.InPaid != 0)
                    //        {
                    //            reportNumberStr = Convert.ToString(tempPaidDetails.ReportNo);
                    //        }
                    //    }
                    //    commentLineStr += " ; Report Number: " + reportNumberStr + " ; Sales Person: " + tempSalesman.SalesPersonNameStr;
                    //}
                    commentLineStr += " ; Report Number: " + reportNumberStr + " ; Sales Person: " + tempSalesman.SalesPersonNameStr;
                    paragraph.Add(commentLineStr);
                    paragraph.SpacingAfter = 2f;
                    paragraph.SpacingBefore = 0f;
                    paragraph.Leading = 10f;
                    document.Add(paragraph);
                    #endregion

                    #region Report

                    #region Column Header
                    pdfPTable = new PdfPTable(13);
                    pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                    tableWidth = 0;

                    dateWidth = 55f;
                    loadNumberWidth = 55f;
                    customerWidth = 100f;
                    itemWidth = 55f;
                    unitWidth = 45f;
                    totalFOBWidth = 65f;
                    fobWidth = 45f;
                    commissionWidth = 60f;
                    perUnitWidth = 45f;
                    paidWidth = 55f;
                    dueWidth = 55f;
                    reportNumberWidth = 30f;
                    ossWidth = 25f;

                    tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                    pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                    pdfPTable.TotalWidth = tableWidth;
                    //table.KeepTogether = true;
                    pdfPTable.SplitRows = true;
                    pdfPTable.SplitLate = false;
                    pdfPTable.LockedWidth = true;
                    pdfPTable.DefaultCell.Padding = 0;
                    //pdfPTable.WidthPercentage = 100; // percentage
                    pdfPTable.DefaultCell.BorderWidth = 1f;
                    pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                    detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    pdfPTable.HeaderRows = 1; // this is the end of the table header
                    #endregion
                    /*
                    for (int z = 0; z < CommissionReportDataBLL.CommissionOrderArl.Count; z++)
                    {
                        Commission_Order tempCommissionOrder = (Commission_Order)CommissionReportDataBLL.CommissionOrderArl[z];
                    */
                    double SubTotalInQntDbl = 0;
                    double SubTotalInFOBDbl = 0;
                    //double SubTotalTotalFOBPerQntDbl = 0;
                    double SubTotalInCommissionDbl = 0;
                    //double SubTotalInCommissionPerUnitDbl = 0;
                    double SubTotalInPaidDbl = 0;
                    double SubTotalInNetDbl = 0;


                    // SalesPerson tempSalesPerson = (SalesPerson)CommissionReportDataBLL.SalesPersonArl[i];
                    #region Inside Sales
                    for (int y = 0; y < CommissionReportDataBLL.CommissionOrderArl.Count; y++)
                    {
                        Commission_Order tempCommissionOrder = (Commission_Order)CommissionReportDataBLL.CommissionOrderArl[y];

                        if (tempSalesman.SalesPersonKeyStr == tempCommissionOrder.SalesmanInt.ToString())
                        {
                            double totalOrderQnt = 0;
                            double totalFOBAmt = 0;
                            string LoadNumberDetail = "";
                            string CustomerNameDetail = "";
                            for (int j = 0; j < tempCommissionOrder.CommissionOrderDetailArl.Count; j++)
                            {
                                Commission_Order_Detail tempCommissionOrderDetail = (Commission_Order_Detail)tempCommissionOrder.CommissionOrderDetailArl[j];
                                if (tempCommissionOrderDetail.InBln)
                                {
                                    #region Print Details

                                    string ShipDateDetail = Misc.FormatDateTime(tempCommissionOrderDetail.DateStr);
                                    if (tempCommissionOrderDetail.LoadStr != "")
                                        LoadNumberDetail = StringFormater.FormatString(tempCommissionOrderDetail.LoadStr, 10);
                                    if (tempCommissionOrderDetail.CustomerStr != "")
                                        CustomerNameDetail = StringFormater.FormatString(tempCommissionOrderDetail.CustomerStr, 15);

                                    string ItemDetail = "";

                                    if (tempCommissionOrderDetail.ItemStr != "")
                                        ItemDetail = StringFormater.FormatString(tempCommissionOrderDetail.ItemStr, 12);


                                    string QntDetail = Misc.DisplayFormat(tempCommissionOrderDetail.QntDbl);
                                    totalOrderQnt += tempCommissionOrderDetail.QntDbl;

                                    string TotalFOBDetail = RemoveZeroCurrency(tempCommissionOrderDetail.FOBDbl);
                                    string FOBDetail = RemoveZeroCurrency(tempCommissionOrderDetail.FOBDbl / tempCommissionOrderDetail.QntDbl);
                                    totalFOBAmt += tempCommissionOrderDetail.FOBDbl;

                                    string CommissionDetail = RemoveZeroCurrency(tempCommissionOrderDetail.FOBDbl * CommissionReportDataBLL.InsideSaleCommissionRateDbl);
                                    string CommissionPerUnitDetail = RemoveZeroCurrency((tempCommissionOrderDetail.FOBDbl * CommissionReportDataBLL.InsideSaleCommissionRateDbl) / tempCommissionOrderDetail.QntDbl);

                                    string OutSideSaleDetail = "N";

                                    detailCell = new PdfPCell(new Phrase(ShipDateDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(LoadNumberDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(CustomerNameDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(ItemDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(QntDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(TotalFOBDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(FOBDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(CommissionDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(CommissionPerUnitDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(OutSideSaleDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    lineCount += 1;

                                    if (lineCount > linesPerPage)
                                    {
                                        document.Add(pdfPTable);
                                        lineCount = 0;
                                        document.NewPage();

                                        #region Column Header
                                        pdfPTable = new PdfPTable(13);
                                        pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                                        tableWidth = 0;

                                        dateWidth = 55f;
                                        loadNumberWidth = 55f;
                                        customerWidth = 100f;
                                        itemWidth = 55f;
                                        unitWidth = 45f;
                                        totalFOBWidth = 65f;
                                        fobWidth = 45f;
                                        commissionWidth = 60f;
                                        perUnitWidth = 45f;
                                        paidWidth = 55f;
                                        dueWidth = 55f;
                                        reportNumberWidth = 30f;
                                        ossWidth = 25f;

                                        tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                                        pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                                        pdfPTable.TotalWidth = tableWidth;
                                        //table.KeepTogether = true;
                                        pdfPTable.SplitRows = true;
                                        pdfPTable.SplitLate = false;
                                        pdfPTable.LockedWidth = true;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        //pdfPTable.WidthPercentage = 100; // percentage
                                        pdfPTable.DefaultCell.BorderWidth = 1f;
                                        pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                                        detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        pdfPTable.DefaultCell.Padding = 0;
                                        pdfPTable.AddCell(detailCell);

                                        pdfPTable.HeaderRows = 1; // this is the end of the table header
                                        #endregion
                                    }

                                    #endregion
                                }
                            }
                            if (totalOrderQnt != 0)
                            {
                                #region Print Total
                                string LoadNumberItem = LoadNumberDetail;
                                string CustomerNameItem = CustomerNameDetail;
                                string SubInQntDetail = DisplayFormat(totalOrderQnt);
                                string SubInFOBDetail = RemoveZeroCurrency(totalFOBAmt);
                                string SubInCommissionDetail = RemoveZeroCurrency(tempCommissionOrder.InCurrentDbl);
                                string SubInPaid = RemoveZeroCurrency(tempCommissionOrder.InPaidDbl);
                                string SubInNet = RemoveZeroCurrency(tempCommissionOrder.InNetDbl);

                                string ReportNumberItem = "";

                                if (tempCommissionOrder.CommissionPaidDetailArl.Count != 0)
                                {
                                    CommissionPaidDetails tempPaidDetails = (CommissionPaidDetails)tempCommissionOrder.CommissionPaidDetailArl[0];
                                    if (tempPaidDetails.InPaid != 0)
                                    {
                                        ReportNumberItem = Convert.ToString(tempPaidDetails.ReportNo);
                                    }

                                    #region not used now print paid details
                                    //for (int t = 0; t < tempCommissionOrder.CommissionPaidDetailArl.Count; t++)
                                    //{
                                    //    CommissionPaidDetails tempPaidDetails = (CommissionPaidDetails)tempCommissionOrder.CommissionPaidDetailArl[t];

                                    //    if (tempPaidDetails.InPaid != 0)
                                    //    {
                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase(SubInFOBDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase(SubInPaid, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase(ReportNumberItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);
                                    //    }

                                    //}
                                    #endregion
                                }

                                detailCell = new PdfPCell(new Phrase("Total", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(LoadNumberItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(CustomerNameItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase("Total", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(SubInQntDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(SubInFOBDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(SubInCommissionDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(SubInPaid, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(SubInNet, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(ReportNumberItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                lineCount += 1;

                                if (lineCount > linesPerPage)
                                {
                                    document.Add(pdfPTable);
                                    lineCount = 0;
                                    document.NewPage();

                                    #region Column Header
                                    pdfPTable = new PdfPTable(13);
                                    pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                                    tableWidth = 0;

                                    dateWidth = 55f;
                                    loadNumberWidth = 55f;
                                    customerWidth = 100f;
                                    itemWidth = 55f;
                                    unitWidth = 45f;
                                    totalFOBWidth = 65f;
                                    fobWidth = 45f;
                                    commissionWidth = 60f;
                                    perUnitWidth = 45f;
                                    paidWidth = 55f;
                                    dueWidth = 55f;
                                    reportNumberWidth = 30f;
                                    ossWidth = 25f;

                                    tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                                    pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                                    pdfPTable.TotalWidth = tableWidth;
                                    //table.KeepTogether = true;
                                    pdfPTable.SplitRows = true;
                                    pdfPTable.SplitLate = false;
                                    pdfPTable.LockedWidth = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    //pdfPTable.WidthPercentage = 100; // percentage
                                    pdfPTable.DefaultCell.BorderWidth = 1f;
                                    pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                                    detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    pdfPTable.HeaderRows = 1; // this is the end of the table header
                                    #endregion
                                }

                                SubTotalInQntDbl += totalOrderQnt;

                                SubTotalInFOBDbl += totalFOBAmt;

                                if (SubTotalInFOBDbl != 0)
                                {
                                    SubTotalInAllFOBPerQntDbl = SubTotalInFOBDbl / SubTotalInQntDbl;
                                }

                                SubTotalInCommissionDbl += tempCommissionOrder.InCurrentDbl;

                                if (SubTotalInQntDbl != 0)
                                {
                                    SubTotalInCommissionAmtPerQntDbl = SubTotalInCommissionDbl / SubTotalInQntDbl;
                                }

                                SubTotalInPaidDbl += tempCommissionOrder.InPaidDbl;
                                SubTotalInNetDbl += tempCommissionOrder.InNetDbl;

                                #endregion
                            }

                        }
                    }
                    #region Sub Total
                    string GrandTotalInQntStr = DisplayFormat(SubTotalInQntDbl);
                    string GrandTotalInFOBStr = RemoveZeroCurrency(SubTotalInFOBDbl);
                    string GrandTotalInCommissionStr = RemoveZeroCurrency(SubTotalInCommissionDbl);
                    string GrandTotalInNetStr = RemoveZeroCurrency(SubTotalInNetDbl);

                    detailCell = new PdfPCell(new Phrase("Grand Total Inside Sales", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 3;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(GrandTotalInQntStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(GrandTotalInFOBStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(GrandTotalInCommissionStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(GrandTotalInNetStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(reportNumberStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);



                    lineCount += 1;

                    if (lineCount > linesPerPage)
                    {
                        document.Add(pdfPTable);
                        lineCount = 0;
                        document.NewPage();

                        #region Column Header
                        pdfPTable = new PdfPTable(13);
                        pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                        tableWidth = 0;

                        dateWidth = 55f;
                        loadNumberWidth = 55f;
                        customerWidth = 100f;
                        itemWidth = 55f;
                        unitWidth = 45f;
                        totalFOBWidth = 65f;
                        fobWidth = 45f;
                        commissionWidth = 60f;
                        perUnitWidth = 45f;
                        paidWidth = 55f;
                        dueWidth = 55f;
                        reportNumberWidth = 30f;
                        ossWidth = 25f;

                        tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                        pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                        pdfPTable.TotalWidth = tableWidth;
                        //table.KeepTogether = true;
                        pdfPTable.SplitRows = true;
                        pdfPTable.SplitLate = false;
                        pdfPTable.LockedWidth = true;
                        pdfPTable.DefaultCell.Padding = 0;
                        //pdfPTable.WidthPercentage = 100; // percentage
                        pdfPTable.DefaultCell.BorderWidth = 1f;
                        pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                        detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        pdfPTable.HeaderRows = 1; // this is the end of the table header
                        #endregion
                    }
                    #endregion

                    #region Subtotal Salesman
                    string GrandTotalSalesmanInQntStr = DisplayFormat(SubTotalInQntDbl / CommissionReportDataBLL.SalesPersonArl.Count);
                    string GrandTotalSalesmanInFOBStr = RemoveZeroCurrency(SubTotalInFOBDbl / CommissionReportDataBLL.SalesPersonArl.Count);
                    string GrandTotalSalesmanInCommissionStr = RemoveZeroCurrency(SubTotalInCommissionDbl / CommissionReportDataBLL.SalesPersonArl.Count);
                    string GrandTotalSalesmanInNetStr = RemoveZeroCurrency(SubTotalInNetDbl / CommissionReportDataBLL.SalesPersonArl.Count);

                    detailCell = new PdfPCell(new Phrase(tempSalesman.SalesPersonNameStr + " Share of Inside Sales", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 3;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(GrandTotalSalesmanInQntStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(GrandTotalSalesmanInFOBStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(GrandTotalSalesmanInCommissionStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(GrandTotalSalesmanInNetStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(reportNumberStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);



                    lineCount += 1;

                    if (lineCount > linesPerPage)
                    {
                        document.Add(pdfPTable);
                        lineCount = 0;
                        document.NewPage();

                        #region Column Header
                        pdfPTable = new PdfPTable(13);
                        pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                        tableWidth = 0;

                        dateWidth = 55f;
                        loadNumberWidth = 55f;
                        customerWidth = 100f;
                        itemWidth = 55f;
                        unitWidth = 45f;
                        totalFOBWidth = 65f;
                        fobWidth = 45f;
                        commissionWidth = 60f;
                        perUnitWidth = 45f;
                        paidWidth = 55f;
                        dueWidth = 55f;
                        reportNumberWidth = 30f;
                        ossWidth = 25f;

                        tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                        pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                        pdfPTable.TotalWidth = tableWidth;
                        //table.KeepTogether = true;
                        pdfPTable.SplitRows = true;
                        pdfPTable.SplitLate = false;
                        pdfPTable.LockedWidth = true;
                        pdfPTable.DefaultCell.Padding = 0;
                        //pdfPTable.WidthPercentage = 100; // percentage
                        pdfPTable.DefaultCell.BorderWidth = 1f;
                        pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                        detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        pdfPTable.HeaderRows = 1; // this is the end of the table header
                        #endregion
                    }
                    #endregion
                    if (lineCount != 0)
                    {
                        document.Add(pdfPTable);
                    }
                
                    #endregion

                    #region Outside Sales
                    #region Header
                    pdfPTable = new PdfPTable(13);
                    pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                    tableWidth = 0;

                    dateWidth = 55f;
                    loadNumberWidth = 55f;
                    customerWidth = 100f;
                    itemWidth = 55f;
                    unitWidth = 45f;
                    totalFOBWidth = 65f;
                    fobWidth = 45f;
                    commissionWidth = 60f;
                    perUnitWidth = 45f;
                    paidWidth = 55f;
                    dueWidth = 55f;
                    reportNumberWidth = 30f;
                    ossWidth = 25f;

                    tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                    pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                    pdfPTable.TotalWidth = tableWidth;
                    //table.KeepTogether = true;
                    pdfPTable.SplitRows = true;
                    pdfPTable.SplitLate = false;
                    pdfPTable.LockedWidth = true;
                    pdfPTable.DefaultCell.Padding = 0;
                    //pdfPTable.WidthPercentage = 100; // percentage
                    pdfPTable.DefaultCell.BorderWidth = 1f;
                    pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    #endregion


                    #region Header
                    detailCell = new PdfPCell(new Phrase(tempSalesman.SalesPersonNameStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 13;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    lineCount += 1;

                    if (lineCount > linesPerPage)
                    {
                        document.Add(pdfPTable);
                        lineCount = 0;
                        document.NewPage();

                        #region Column Header
                        pdfPTable = new PdfPTable(13);
                        pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                        tableWidth = 0;

                        dateWidth = 55f;
                        loadNumberWidth = 55f;
                        customerWidth = 100f;
                        itemWidth = 55f;
                        unitWidth = 45f;
                        totalFOBWidth = 65f;
                        fobWidth = 45f;
                        commissionWidth = 60f;
                        perUnitWidth = 45f;
                        paidWidth = 55f;
                        dueWidth = 55f;
                        reportNumberWidth = 30f;
                        ossWidth = 25f;

                        tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                        pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                        pdfPTable.TotalWidth = tableWidth;
                        //table.KeepTogether = true;
                        pdfPTable.SplitRows = true;
                        pdfPTable.SplitLate = false;
                        pdfPTable.LockedWidth = true;
                        pdfPTable.DefaultCell.Padding = 0;
                        //pdfPTable.WidthPercentage = 100; // percentage
                        pdfPTable.DefaultCell.BorderWidth = 1f;
                        pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                        detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        pdfPTable.HeaderRows = 1; // this is the end of the table header
                        #endregion
                    }

                    detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    lineCount += 1;

                    if (lineCount > linesPerPage)
                    {
                        document.Add(pdfPTable);
                        lineCount = 0;
                        document.NewPage();

                        #region Column Header
                        pdfPTable = new PdfPTable(13);
                        pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                        tableWidth = 0;

                        dateWidth = 55f;
                        loadNumberWidth = 55f;
                        customerWidth = 100f;
                        itemWidth = 55f;
                        unitWidth = 45f;
                        totalFOBWidth = 65f;
                        fobWidth = 45f;
                        commissionWidth = 60f;
                        perUnitWidth = 45f;
                        paidWidth = 55f;
                        dueWidth = 55f;
                        reportNumberWidth = 30f;
                        ossWidth = 25f;

                        tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                        pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                        pdfPTable.TotalWidth = tableWidth;
                        //table.KeepTogether = true;
                        pdfPTable.SplitRows = true;
                        pdfPTable.SplitLate = false;
                        pdfPTable.LockedWidth = true;
                        pdfPTable.DefaultCell.Padding = 0;
                        //pdfPTable.WidthPercentage = 100; // percentage
                        pdfPTable.DefaultCell.BorderWidth = 1f;
                        pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                        detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        pdfPTable.HeaderRows = 1; // this is the end of the table header
                        #endregion
                    }
                    #endregion

                    double totalOutOrderQnt = 0;
                    double totalOutFOBAmt = 0;
                    double SubTotalOutQntDbl = 0;
                    double SubTotalOutFOBDbl = 0;
                    double SubTotalOutTotalFOBPerUnitDbl = 0;
                    double SubTotalOutCommissionDbl = 0;
                    double SubTotalOutCommissionPerUnitDbl = 0;
                    double SubTotalOutPaidDbl = 0;
                    double SubTotalOutNetDbl = 0;

                    for (int y = 0; y < CommissionReportDataBLL.CommissionOrderArl.Count; y++)
                    {
                        Commission_Order tempCommissionOrder = (Commission_Order)CommissionReportDataBLL.CommissionOrderArl[y];

                        if (tempSalesman.SalesPersonKeyStr == tempCommissionOrder.SalesmanInt.ToString())
                        {
                            double totalOrderQnt = 0;
                            double totalFOBAmt = 0;
                            string LoadNumberDetail = "";
                            string CustomerNameDetail = "";
                            for (int j = 0; j < tempCommissionOrder.CommissionOrderDetailArl.Count; j++)
                            {
                                Commission_Order_Detail tempCommissionOrderDetail = (Commission_Order_Detail)tempCommissionOrder.CommissionOrderDetailArl[j];

                                #region Print Details
                                if (!tempCommissionOrderDetail.InBln)
                                {
                                    string ShipDateDetail = Misc.FormatDateTime(tempCommissionOrderDetail.DateStr);

                                    if (tempCommissionOrderDetail.LoadStr != "")
                                        LoadNumberDetail = StringFormater.FormatString(tempCommissionOrderDetail.LoadStr, 10);

                                    if (tempCommissionOrderDetail.CustomerStr != "")
                                        CustomerNameDetail = StringFormater.FormatString(tempCommissionOrderDetail.CustomerStr, 15);

                                    string ItemDetail = "";

                                    if (tempCommissionOrderDetail.ItemStr != "")
                                        ItemDetail = StringFormater.FormatString(tempCommissionOrderDetail.ItemStr, 12);

                                    string QntDetail = Misc.DisplayFormat(tempCommissionOrderDetail.QntDbl);
                                    totalOrderQnt += tempCommissionOrderDetail.QntDbl;
                                    string TotalFOBDetail = RemoveZeroCurrency(tempCommissionOrderDetail.FOBDbl);
                                    string FOBDetail = RemoveZeroCurrency(tempCommissionOrderDetail.FOBDbl / tempCommissionOrderDetail.QntDbl);
                                    totalFOBAmt += tempCommissionOrderDetail.FOBDbl;
                                    string CommissionDetail = RemoveZeroCurrency((tempCommissionOrderDetail.FOBDbl - tempCommissionOrderDetail.CostDbl) * CommissionReportDataBLL.OutsideSaleCommissionRateDbl);
                                    string CommissionPerUnitDetail = RemoveZeroCurrency(((tempCommissionOrderDetail.FOBDbl - tempCommissionOrderDetail.CostDbl) * CommissionReportDataBLL.OutsideSaleCommissionRateDbl) / tempCommissionOrderDetail.QntDbl);
                                    string OutSideSaleDetail = "Y";

                                    detailCell = new PdfPCell(new Phrase(ShipDateDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(LoadNumberDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(CustomerNameDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(ItemDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(QntDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(TotalFOBDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(FOBDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(CommissionDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(CommissionPerUnitDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase(OutSideSaleDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    detailCell.NoWrap = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    lineCount += 1;
                                }

                                if (lineCount > linesPerPage)
                                {
                                    document.Add(pdfPTable);
                                    lineCount = 0;
                                    document.NewPage();

                                    #region Column Header
                                    pdfPTable = new PdfPTable(13);
                                    pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                                    tableWidth = 0;

                                    dateWidth = 55f;
                                    loadNumberWidth = 55f;
                                    customerWidth = 100f;
                                    itemWidth = 55f;
                                    unitWidth = 45f;
                                    totalFOBWidth = 65f;
                                    fobWidth = 45f;
                                    commissionWidth = 60f;
                                    perUnitWidth = 45f;
                                    paidWidth = 55f;
                                    dueWidth = 55f;
                                    reportNumberWidth = 30f;
                                    ossWidth = 25f;

                                    tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                                    pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                                    pdfPTable.TotalWidth = tableWidth;
                                    //table.KeepTogether = true;
                                    pdfPTable.SplitRows = true;
                                    pdfPTable.SplitLate = false;
                                    pdfPTable.LockedWidth = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    //pdfPTable.WidthPercentage = 100; // percentage
                                    pdfPTable.DefaultCell.BorderWidth = 1f;
                                    pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                                    detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    pdfPTable.HeaderRows = 1; // this is the end of the table header
                                    #endregion
                                }

                                #endregion

                            }
                            if (totalOrderQnt != 0)
                            {
                                #region Print Total
                                string LoadNumberItem = LoadNumberDetail;
                                string CustomerNameItem = CustomerNameDetail;

                                string UnitItem = DisplayFormat(totalOrderQnt);

                                string TotalFOBItem = RemoveZeroCurrency(totalFOBAmt);
                                string CommissionItem = RemoveZeroCurrency(tempCommissionOrder.OutCurrentDbl);
                                string PaidItem = RemoveZeroCurrency(tempCommissionOrder.OutPaidDbl);
                                string DueItem = RemoveZeroCurrency(tempCommissionOrder.OutNetDbl);
                                SubTotalOutQntDbl += totalOrderQnt;
                                SubTotalOutFOBDbl += totalFOBAmt;
                                SubTotalOutCommissionDbl += (tempCommissionOrder.OutCurrentDbl);
                                SubTotalOutNetDbl += tempCommissionOrder.OutNetDbl;
                                string ReportNumberItem = "";
                                if (tempCommissionOrder.CommissionPaidDetailArl.Count != 0)
                                {
                                    CommissionPaidDetails tempPaidDetails = (CommissionPaidDetails)tempCommissionOrder.CommissionPaidDetailArl[0];
                                    if (tempPaidDetails.InPaid != 0)
                                    {
                                        ReportNumberItem = Convert.ToString(tempPaidDetails.ReportNo);
                                    }

                                    #region not used now print paid details
                                    //for (int t = 0; t < tempCommissionOrder.CommissionPaidDetailArl.Count; t++)
                                    //{
                                    //    CommissionPaidDetails tempPaidDetails = (CommissionPaidDetails)tempCommissionOrder.CommissionPaidDetailArl[t];

                                    //    if (tempPaidDetails.InPaid != 0)
                                    //    {
                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase(SubInFOBDetail, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase(SubInPaid, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase(ReportNumberItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);

                                    //        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                    //        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    //        detailCell.NoWrap = true;
                                    //        pdfPTable.DefaultCell.Padding = 0;
                                    //        pdfPTable.AddCell(detailCell);
                                    //    }

                                    //}
                                    #endregion
                                }

                                detailCell = new PdfPCell(new Phrase("Total", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(LoadNumberItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(CustomerNameItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase("Total", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(UnitItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(TotalFOBItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(CommissionItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(PaidItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(DueItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase(ReportNumberItem, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                                detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                detailCell.NoWrap = true;
                                pdfPTable.DefaultCell.Padding = 0;
                                pdfPTable.AddCell(detailCell);

                                lineCount += 1;

                                if (lineCount > linesPerPage)
                                {
                                    document.Add(pdfPTable);
                                    lineCount = 0;
                                    document.NewPage();

                                    #region Column Header
                                    pdfPTable = new PdfPTable(13);
                                    pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                                    tableWidth = 0;

                                    dateWidth = 55f;
                                    loadNumberWidth = 55f;
                                    customerWidth = 100f;
                                    itemWidth = 55f;
                                    unitWidth = 45f;
                                    totalFOBWidth = 65f;
                                    fobWidth = 45f;
                                    commissionWidth = 60f;
                                    perUnitWidth = 45f;
                                    paidWidth = 55f;
                                    dueWidth = 55f;
                                    reportNumberWidth = 30f;
                                    ossWidth = 25f;

                                    tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                                    pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                                    pdfPTable.TotalWidth = tableWidth;
                                    //table.KeepTogether = true;
                                    pdfPTable.SplitRows = true;
                                    pdfPTable.SplitLate = false;
                                    pdfPTable.LockedWidth = true;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    //pdfPTable.WidthPercentage = 100; // percentage
                                    pdfPTable.DefaultCell.BorderWidth = 1f;
                                    pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                                    detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    pdfPTable.DefaultCell.Padding = 0;
                                    pdfPTable.AddCell(detailCell);

                                    pdfPTable.HeaderRows = 1; // this is the end of the table header
                                    #endregion
                                }




                                /*
                                 if (GrandTotalQntDbl != 0)
                                 {
                                     GrandTotalTotalFOBPerUnitDbl = GrandTotalFOBQntDbl / GrandTotalQntDbl;
                                 }

                                 GrandTotalCommissionDbl += ;

                                 if (GrandTotalQntDbl != 0)
                                 {
                                     GrandTotalCommissionPerUnitDbl = GrandTotalCommissionDbl / GrandTotalQntDbl;
                                 }

                                 GrandTotalPaidDbl += commissionItem.Paid;
                                 GrandTotalDueDbl += commissionItem.Due;
                                 */

                                #endregion
                            }



                        }
                    }
                    #region Total
                    string TotalOutQntStr = DisplayFormat(SubTotalOutQntDbl);
                    string TotalOutFOBStr = RemoveZeroCurrency(SubTotalOutFOBDbl);
                    string TotalOutCommissionStr = RemoveZeroCurrency(SubTotalOutCommissionDbl);
                    string TotalOutNetStr = RemoveZeroCurrency(SubTotalOutNetDbl);

                    detailCell = new PdfPCell(new Phrase(tempSalesman.SalesPersonNameStr + @" Total Outside", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 3;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(TotalOutQntStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(TotalOutFOBStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(TotalOutCommissionStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(TotalOutNetStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    lineCount += 1;

                    if (lineCount > linesPerPage)
                    {
                        document.Add(pdfPTable);
                        lineCount = 0;
                        document.NewPage();

                        #region Column Header
                        pdfPTable = new PdfPTable(13);
                        pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                        tableWidth = 0;

                        dateWidth = 55f;
                        loadNumberWidth = 55f;
                        customerWidth = 100f;
                        itemWidth = 55f;
                        unitWidth = 45f;
                        totalFOBWidth = 65f;
                        fobWidth = 45f;
                        commissionWidth = 60f;
                        perUnitWidth = 45f;
                        paidWidth = 55f;
                        dueWidth = 55f;
                        reportNumberWidth = 30f;
                        ossWidth = 25f;

                        tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                        pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                        pdfPTable.TotalWidth = tableWidth;
                        //table.KeepTogether = true;
                        pdfPTable.SplitRows = true;
                        pdfPTable.SplitLate = false;
                        pdfPTable.LockedWidth = true;
                        pdfPTable.DefaultCell.Padding = 0;
                        //pdfPTable.WidthPercentage = 100; // percentage
                        pdfPTable.DefaultCell.BorderWidth = 1f;
                        pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                        detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        pdfPTable.HeaderRows = 1; // this is the end of the table header
                        #endregion
                    }
                    #region Subtotal Salesman
                    detailCell = new PdfPCell(new Phrase(tempSalesman.SalesPersonNameStr + " Share of Inside Sales", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 3;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(GrandTotalSalesmanInQntStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(GrandTotalSalesmanInFOBStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(GrandTotalSalesmanInCommissionStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(GrandTotalSalesmanInNetStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(reportNumberStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);



                    lineCount += 1;

                    if (lineCount > linesPerPage)
                    {
                        document.Add(pdfPTable);
                        lineCount = 0;
                        document.NewPage();

                        #region Column Header
                        pdfPTable = new PdfPTable(13);
                        pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                        tableWidth = 0;

                        dateWidth = 55f;
                        loadNumberWidth = 55f;
                        customerWidth = 100f;
                        itemWidth = 55f;
                        unitWidth = 45f;
                        totalFOBWidth = 65f;
                        fobWidth = 45f;
                        commissionWidth = 60f;
                        perUnitWidth = 45f;
                        paidWidth = 55f;
                        dueWidth = 55f;
                        reportNumberWidth = 30f;
                        ossWidth = 25f;

                        tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                        pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                        pdfPTable.TotalWidth = tableWidth;
                        //table.KeepTogether = true;
                        pdfPTable.SplitRows = true;
                        pdfPTable.SplitLate = false;
                        pdfPTable.LockedWidth = true;
                        pdfPTable.DefaultCell.Padding = 0;
                        //pdfPTable.WidthPercentage = 100; // percentage
                        pdfPTable.DefaultCell.BorderWidth = 1f;
                        pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                        detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        pdfPTable.HeaderRows = 1; // this is the end of the table header
                        #endregion
                    }
                    #endregion
                    #region Total Salesman
                    string TotalSalesmanInQntStr = DisplayFormat((SubTotalInQntDbl / CommissionReportDataBLL.SalesPersonArl.Count) + SubTotalOutQntDbl);
                    string TotalSalesmanInFOBStr = RemoveZeroCurrency((SubTotalInFOBDbl / CommissionReportDataBLL.SalesPersonArl.Count) + SubTotalOutFOBDbl);
                    string TotalSalesmanInCommissionStr = RemoveZeroCurrency((SubTotalInCommissionDbl / CommissionReportDataBLL.SalesPersonArl.Count) + SubTotalOutCommissionDbl);
                    string TotalSalesmanInNetStr = RemoveZeroCurrency((SubTotalInNetDbl / CommissionReportDataBLL.SalesPersonArl.Count) + SubTotalOutNetDbl);

                    detailCell = new PdfPCell(new Phrase(tempSalesman.SalesPersonNameStr + " Grand Total", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 3;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(TotalSalesmanInQntStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(TotalSalesmanInFOBStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(TotalSalesmanInCommissionStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(TotalSalesmanInNetStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    detailCell.Colspan = 2;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(reportNumberStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)));
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    detailCell.NoWrap = true;
                    pdfPTable.DefaultCell.Padding = 0;
                    pdfPTable.AddCell(detailCell);



                    lineCount += 1;

                    if (lineCount > linesPerPage)
                    {
                        document.Add(pdfPTable);
                        lineCount = 0;
                        document.NewPage();

                        #region Column Header
                        pdfPTable = new PdfPTable(13);
                        pdfPTable.HorizontalAlignment = Element.ALIGN_CENTER;

                        tableWidth = 0;

                        dateWidth = 55f;
                        loadNumberWidth = 55f;
                        customerWidth = 100f;
                        itemWidth = 55f;
                        unitWidth = 45f;
                        totalFOBWidth = 65f;
                        fobWidth = 45f;
                        commissionWidth = 60f;
                        perUnitWidth = 45f;
                        paidWidth = 55f;
                        dueWidth = 55f;
                        reportNumberWidth = 30f;
                        ossWidth = 25f;

                        tableWidth = dateWidth + loadNumberWidth + customerWidth + itemWidth + unitWidth + totalFOBWidth + fobWidth + commissionWidth + perUnitWidth + paidWidth + dueWidth + reportNumberWidth + ossWidth;

                        pdfPTable.SetWidths(new float[] { dateWidth, loadNumberWidth, customerWidth, itemWidth, unitWidth, totalFOBWidth, fobWidth, commissionWidth, perUnitWidth, paidWidth, dueWidth, reportNumberWidth, ossWidth });
                        pdfPTable.TotalWidth = tableWidth;
                        //table.KeepTogether = true;
                        pdfPTable.SplitRows = true;
                        pdfPTable.SplitLate = false;
                        pdfPTable.LockedWidth = true;
                        pdfPTable.DefaultCell.Padding = 0;
                        //pdfPTable.WidthPercentage = 100; // percentage
                        pdfPTable.DefaultCell.BorderWidth = 1f;
                        pdfPTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        pdfPTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

                        detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Load #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Total FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("FOB", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Cmsn.", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("/Unit", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Paid", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Due", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("RPT #", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("OSS", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.DefaultCell.Padding = 0;
                        pdfPTable.AddCell(detailCell);

                        pdfPTable.HeaderRows = 1; // this is the end of the table header
                        #endregion
                    }
                    #endregion
                    if (lineCount != 0)
                    {
                        document.Add(pdfPTable);
                    }

                    #endregion
                    #endregion                                   }

                    #endregion
               
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (document != null && document.IsOpen())
                {
                    // we close the document
                    document.Close();
                }
            }
        }

		public void Excel(string saveFolderLocationStr, string fileNameStr, CommissionReportBLL CommissionReportDataBLL, string salesPersonNameStr, string reportNumberStr, string CommentLineStr, bool PendingBln)
		{
			try
			{

				FileStream file = new FileStream(saveFolderLocationStr + "\\" + fileNameStr, System.IO.FileMode.Create);
				StreamWriter writer = new StreamWriter(file);


				string commentLineStr = CommentLineStr.Trim();
				string ReportNameStr = "";
				string columnHeader;

				if (!PendingBln)
				{
					ReportNameStr = "New Commission Report";
				}
				else if (PendingBln)
				{
					ReportNameStr = "Pending Commission Report";
				}
				//Loop for each Saleman.
				bool firstTimeBln = true;
				string GrandTotalSalesmanInQntStr = "";
				string GrandTotalSalesmanInFOBStr = "";
				string GrandTotalSalesmanInCommissionStr = "";
				string GrandTotalSalesmanInNetStr = "";
				double SubTotalInQntDbl = 0;
				double SubTotalInFOBDbl = 0;
				//double SubTotalTotalFOBPerQntDbl = 0;
				double SubTotalInCommissionDbl = 0;
				//double SubTotalInCommissionPerUnitDbl = 0;
				double SubTotalInPaidDbl = 0;
				double SubTotalInNetDbl = 0;

				for (int x = 0; x < CommissionReportDataBLL.SalesPersonArl.Count; x++)
				{
					SalesPerson tempSalesman = (SalesPerson)CommissionReportDataBLL.SalesPersonArl[x];

					#region Report
					#region First Time only
					if (firstTimeBln) //if around inside sales
					{
						firstTimeBln = false;

						#region Top Line
						commentLineStr += " ; Report Number: " + reportNumberStr;
						columnHeader = commentLineStr;
						writer.Write(columnHeader);
						writer.Write("\n");
						#endregion

						#region Column Header
						columnHeader = "Date,";
						columnHeader += "Load #,";
						columnHeader += "Customer,";
						columnHeader += "Item,";
						columnHeader += "Unit,";
						columnHeader += "Total FOB,";
						columnHeader += "FOB,";
						columnHeader += "Cmsn.,";
						columnHeader += "/Unit,";
						columnHeader += "Paid,";
						columnHeader += "Due,";
						columnHeader += "RPT #,";
						columnHeader += "OSS, Salesman,";
						writer.Write(columnHeader);
						writer.Write("\n");
						#endregion

						// SalesPerson tempSalesPerson = (SalesPerson)CommissionReportDataBLL.SalesPersonArl[i];
						#region Inside Sales
						for (int y = 0; y < CommissionReportDataBLL.CommissionOrderArl.Count; y++)
						{
							Commission_Order tempCommissionOrder = (Commission_Order)CommissionReportDataBLL.CommissionOrderArl[y];

							if (tempSalesman.SalesPersonKeyStr == tempCommissionOrder.SalesmanInt.ToString())
							{
								double totalOrderQnt = 0;
								double totalFOBAmt = 0;
								string LoadNumberDetail = "";
								string CustomerNameDetail = "";
								for (int j = 0; j < tempCommissionOrder.CommissionOrderDetailArl.Count; j++)
								{
									Commission_Order_Detail tempCommissionOrderDetail = (Commission_Order_Detail)tempCommissionOrder.CommissionOrderDetailArl[j];
									if (tempCommissionOrderDetail.InBln)
									{
										#region Print Details

										string ShipDateDetail = Misc.FormatDateTime(tempCommissionOrderDetail.DateStr);
										if (tempCommissionOrderDetail.LoadStr != "")
											LoadNumberDetail = StringFormater.FormatString(tempCommissionOrderDetail.LoadStr, 10);
										if (tempCommissionOrderDetail.CustomerStr != "")
											CustomerNameDetail = StringFormater.FormatString(tempCommissionOrderDetail.CustomerStr.Replace(",", ""), 15);

										string ItemDetail = "";

										if (tempCommissionOrderDetail.ItemStr != "")
											ItemDetail = StringFormater.FormatString(tempCommissionOrderDetail.ItemStr, 12);


										string QntDetail = (tempCommissionOrderDetail.QntDbl).ToString().Replace(",", "");
										totalOrderQnt += tempCommissionOrderDetail.QntDbl;

										string TotalFOBDetail = tempCommissionOrderDetail.FOBDbl.ToString().Replace(",", "");
										string FOBDetail = (tempCommissionOrderDetail.FOBDbl / tempCommissionOrderDetail.QntDbl).ToString().Replace(",", "");
										totalFOBAmt += tempCommissionOrderDetail.FOBDbl;

										string CommissionDetail = (tempCommissionOrderDetail.FOBDbl * CommissionReportDataBLL.InsideSaleCommissionRateDbl).ToString().Replace(",", "");
										string CommissionPerUnitDetail = Math.Round(((tempCommissionOrderDetail.FOBDbl * CommissionReportDataBLL.InsideSaleCommissionRateDbl) / tempCommissionOrderDetail.QntDbl), 2).ToString().Replace(",", "");

										string OutSideSaleDetail = "N";

										columnHeader = ShipDateDetail + ",";
										columnHeader += LoadNumberDetail + ",";
										columnHeader += CustomerNameDetail + ",";
										columnHeader += ItemDetail + ",";
										columnHeader += QntDetail + ",";
										columnHeader += TotalFOBDetail + ",";
										columnHeader += FOBDetail + ",";
										columnHeader += CommissionDetail + ",";
										columnHeader += CommissionPerUnitDetail + ",";
										columnHeader += ",";
										columnHeader += ",";
										columnHeader += ",";
										columnHeader += OutSideSaleDetail + ",,";
										writer.Write(columnHeader);
										writer.Write("\n");
										#endregion
									}
								}

							}
						}
						#endregion
					}
					#endregion

					#region Outside Sales
					

					for (int y = 0; y < CommissionReportDataBLL.CommissionOrderArl.Count; y++)
					{
						Commission_Order tempCommissionOrder = (Commission_Order)CommissionReportDataBLL.CommissionOrderArl[y];

						if (tempSalesman.SalesPersonKeyStr == tempCommissionOrder.SalesmanInt.ToString())
						{
							double totalOrderQnt = 0;
							double totalFOBAmt = 0;
							string LoadNumberDetail = "";
							string CustomerNameDetail = "";
							for (int j = 0; j < tempCommissionOrder.CommissionOrderDetailArl.Count; j++)
							{
								Commission_Order_Detail tempCommissionOrderDetail = (Commission_Order_Detail)tempCommissionOrder.CommissionOrderDetailArl[j];

								#region Print Details
								if (!tempCommissionOrderDetail.InBln)
								{
									string ShipDateDetail = Misc.FormatDateTime(tempCommissionOrderDetail.DateStr);

									if (tempCommissionOrderDetail.LoadStr != "")
										LoadNumberDetail = StringFormater.FormatString(tempCommissionOrderDetail.LoadStr, 10);

									if (tempCommissionOrderDetail.CustomerStr != "")
										CustomerNameDetail = StringFormater.FormatString(tempCommissionOrderDetail.CustomerStr.Replace(",",""), 15);

									string ItemDetail = "";

									if (tempCommissionOrderDetail.ItemStr != "")
										ItemDetail = StringFormater.FormatString(tempCommissionOrderDetail.ItemStr, 12);

									string QntDetail = (tempCommissionOrderDetail.QntDbl).ToString().Replace(",", "");
									totalOrderQnt += tempCommissionOrderDetail.QntDbl;
									string TotalFOBDetail = tempCommissionOrderDetail.FOBDbl.ToString().Replace(",", "");
									string FOBDetail = (tempCommissionOrderDetail.FOBDbl / tempCommissionOrderDetail.QntDbl).ToString().Replace(",", "");
									totalFOBAmt += tempCommissionOrderDetail.FOBDbl;
									string CommissionDetail = ((tempCommissionOrderDetail.FOBDbl - tempCommissionOrderDetail.CostDbl) * CommissionReportDataBLL.OutsideSaleCommissionRateDbl).ToString().Replace(",", "");
									string CommissionPerUnitDetail = Math.Round((((tempCommissionOrderDetail.FOBDbl - tempCommissionOrderDetail.CostDbl) * CommissionReportDataBLL.OutsideSaleCommissionRateDbl) / tempCommissionOrderDetail.QntDbl), 2).ToString().Replace(",","");
									string OutSideSaleDetail = "Y";

									columnHeader = ShipDateDetail + ",";
									columnHeader += LoadNumberDetail + ",";
									columnHeader += CustomerNameDetail + ",";
									columnHeader += ItemDetail + ",";
									columnHeader += QntDetail + ",";
									columnHeader += TotalFOBDetail + ",";
									columnHeader += FOBDetail + ",";
									columnHeader += CommissionDetail + ",";
									columnHeader += CommissionPerUnitDetail + ",";
									columnHeader += "" + ",";
									columnHeader += "" + ",";
									columnHeader += "" + ",";
									columnHeader += OutSideSaleDetail + ",";
									columnHeader += tempSalesman.SalesPersonNameStr + ",";
									writer.Write(columnHeader);
									writer.Write("\n");
								}
								#endregion
							}
						}
					} //end of y for loop 
					#endregion
					#endregion
				}
				writer.Close();
				file.Close();
			}
			catch (Exception ex)
			{

			}
			finally
			{
				
			}
		}
					#endregion
		#region Helper Methods
		public string RemoveZero(string str)
        {
            if (str == "0")
                return "";

            return str;
        }
        public string RemoveZero(double input)
        {
            if (input == 0)
                return "";

            return DisplayFormat(input);
        }
        public string RemoveZero1(double input)
        {
            if (input == 0)
                return "";

            return DisplayFormat1(input);
        }
        private string RemoveZero2(double input)
        {
            if (input == 0)
                return "";

            return DisplayFormat2(input);
        }
        private string RemoveZeroCurrency(double input)
        {
            if (input == 0)
                return "";
            return input.ToString("C");
        }
        public string ReplaceWithZero(string str)
        {
            if (str == "")
                return "0";

            return str;
        }
        public string ReplaceWithZero(double input)
        {
            if (input == 0)
                return "";

            return input.ToString();
        }
        public string DisplayFormat(double value)
        {
            if (value == 0)
                return value.ToString();
            else
                return Misc.Round(value, 2).ToString("###,##0");
        }
        public string DisplayFormat1(double value)
        {
            if (value == 0)
                return value.ToString();
            else
                return Misc.Round(value, 2).ToString("###,##0.0");
        }
        public string DisplayFormat2(double value)
        {
            if (value == 0)
                return value.ToString();
            else
                return Misc.Round(value, 2).ToString("###,##0.00");
        }
        public string DisplayFormat2a(double value)
        {
            if (value == 0)
                return value.ToString();
            else
                return Misc.Round(value, 2).ToString("###,##0.##");
        }
        public string DisplayFormat3(double value)
        {
            if (value == 0)
                return value.ToString();
            else
                return Misc.Round(value, 3).ToString("###,##0.0##");
        }
        #endregion
	}
}