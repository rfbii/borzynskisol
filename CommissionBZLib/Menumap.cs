using System;
using System.Collections.Generic;
using System.Text;
using KeyCentral.Functions;

namespace CommissionBZLib
{
	[MenuMap(Security = "", Description = "Commission", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "", SortOrder = 5, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Commission")]
	[MenuMap(Security = "Commission Reports", Description = "Reports", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "/Commission", DisplayIfEmpty = false, SortOrder = 1, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Commission/Reports")]
	[MenuMap(Security = "Commission Reports", Description = "Commission Report", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Commission/Reports", SortOrder = 1, Url = "../../../CommissionABZ/Reports/CommissionReport/CriteriaCommission.aspx?ReportName=CommissionReport")]
	[MenuMap(Security = "Commission Reports", Description = "New Commission Report", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Commission/Reports", SortOrder = 2, Url = "../../../CommissionABZ/Reports/NewCommissionReport/CriteriaNewCommission.aspx?ReportName=NewCommissionReport")]
	[MenuMap(Security = "Commission Reports", Description = "Lookup New Commission Report", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Commission/Reports", SortOrder = 3, Url = "../../../CommissionABZ/Reports/NewCommissionReport/CriteriaLookupNewCommissionReport.aspx?ReportName=LookupNewCommissionReport")]
	[MenuMap(Security = "Commission Reports", Description = "Pending Commission Report", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Commission/Reports", SortOrder = 4, Url = "../../../CommissionABZ/Reports/PendingCommissionReport/CriteriaPendingCommission.aspx?ReportName=PendingCommissionReport")]
	//[MenuMap(Security = "Commission Reports", Description = "Lookup Pending Commission Report", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Commission/Reports", SortOrder = 5, Url = "../../../CommissionABZ/Reports/PendingCommissionReport/CriteriaLookupPendingCommissionReport.aspx?ReportName=LookupPendingCommissionReport")]
	[MenuMap(Security = "Commission Inputs", Description = "Inputs", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "/Commission", DisplayIfEmpty = false, SortOrder = 1, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Commission/Inputs")]
	[MenuMap(Security = "Commission Inputs", Description = "Excluded Commodities", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Commission/Inputs", SortOrder = 1, Url = "../../../CommissionABZ/UI/Inputs/InputExcludedCommodities.aspx?ReportName=InputExcludedCommodities")]
	[MenuMap(Security = "Commission Inputs", Description = "Excluded Customers", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Commission/Inputs", SortOrder = 1, Url = "../../../CommissionABZ/UI/Inputs/InputExcludedCustomers.aspx?ReportName=InputExcludedCustomers")]
   
    class MenuMap
    {
    }
}
