using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using KeyCentral.Functions;
using KeyCentral.Lookups;
using WeeklySales.App_Code;

namespace WeeklySales.Reports.YearWeeklySummary
{
	public partial class ReportYearWeeklySummary : BasePage
	{
		#region delcares
		protected string QuantityStyle ="";
		protected string priceHeader = "";
		protected Repeater Repeater2;
		protected Repeater Repeater1;
		protected HtmlGenericControl CommentLine;
		protected ReportTotalsHelper totalHelper;
		protected WeeklySalesPerYearBLL weeklySalesPerYearBLL;
		protected DateTime minDate;
		protected DateTime maxDate;
		#endregion
		
		#region Param Properties
		private string Price{get{return Session["Price"].ToString();}}
		private string Quantity{get{return Session["Quantity"].ToString();}}
		private string Customer { get { return Session["Customer"].ToString(); } }
		private string CustomerName { get { return Session["CustomerName"].ToString(); } }
		private string ShipTo{get{return Session["ShipTo"].ToString();}}
		private string ShipToIdx{get{return Session["ShipToIdx"].ToString().Trim();}}
		private string ShipToLocation{get{return Session["ShipToLocation"].ToString().Trim();}}
		private string Variety{get{return Session["Variety"].ToString();}}
		private string VarietyIdx{get{return Session["VarietyIdx"].ToString();}}
		private string SalesPerson{get{return Session["SalesPerson"].ToString();}}
		private DateTime YearStartDate{get{return DateTime.Parse(Session["YearStartDate"].ToString());}}//
		private ArrayList WarehouseIdx{get{return (ArrayList)Session["WarehouseIdx"];}}
		private ArrayList CommodityIdx{get{return (ArrayList)Session["CommodityIdx"];}}
		private ArrayList CommoditySizeIdx{get{return (ArrayList)Session["CommoditySizeIdx"];}}
		#endregion

		#region Vars
		private ArrayList Qnts = new ArrayList();
		private ArrayList Product = new ArrayList();
		private ArrayList Amount = new ArrayList();
		private ArrayList shipTo = new ArrayList();
		private ArrayList SubQnts = new ArrayList();
		private ArrayList SubProduct = new ArrayList();
		private ArrayList SubAmount = new ArrayList();
		private string commentLineString = "";
		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
            if (CheckSecurity("Weekly Sales Report") == false)
            {
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Sales Reports");
            }

			SetCommentLine();
			ShowReport();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}
		
		private void InitializeComponent()
		{    
			this.Load += new EventHandler(this.Page_Load);

		}
		#endregion

		private void DisplayHTML(System.Web.HttpResponse response,WeeklySalesPerYearBLL reportData)
		{
			int yearsCount = reportData.Years.Count * 2;

			decimal[] GrandTotalArray = new decimal[yearsCount];
			int GrandTotalArrayIndex = 0;

			// Populate the GrandTotalArray with 0's
			for (int j = 0; j < GrandTotalArray.Length; j++)
			{
				GrandTotalArray[j] = 0;
			}

			//Set the response content type
			response.ContentType =  @"application/vnd.ms-excel";
            response.AddHeader("content-disposition", "attachment; filename=\"" + "ReportYearWeeklySummary.csv" + "\"");
            
			//Output the title & comment rows
            response.Output.Write("Weekly Sales Per Year Summary Report,");
            response.Output.Write("\n");
            response.Output.Write("\"" + commentLineString + "\",");
            response.Output.Write("\n");
            
			//Display the Header row
            response.Output.Write("Week,");
            response.Output.Write("StartDate,");
            response.Output.Write("EndDate,");

			//foreach(int year in reportData.Years)
			for(int i=reportData.Years.Count -1;i>=0;i--)
			{
				int year= (int)reportData.Years[i];
                response.Output.Write("\"" + "Qnt" + year.ToString() + "-" + (year + 1).ToString() + "\",");
                response.Output.Write("\"" + "Amount" + year.ToString() + "-" + (year + 1).ToString() + "\",");
			}
			//Finish the header row
            response.Output.Write("\n");

			//Loop through the data outputing a row for each record
			foreach(WeeklySalesPerYearCBO.WeeklyItem weeklyItem in reportData.WeeklySummary)
			{
				//Output the Week and desc
                response.Output.Write("\"" + weeklyItem.Week.ToString() + "\",");
                response.Output.Write("\"" + weeklyItem.StartDate.ToString("MM-dd") + "\",");
                response.Output.Write("\"" + weeklyItem.EndDate.ToString("MM-dd") + "\",");

				//foreach(int year in reportData.Years)
				for(int i = reportData.Years.Count -1; i >= 0; i--)
				{
					int year = (int)reportData.Years[i];
                    response.Output.Write("\"" + Misc.DisplayFormat2(Convert.ToDouble(weeklyItem.GetQnt(year).ToString().Replace(",", ""))) + "\",");
					GrandTotalArray[GrandTotalArrayIndex] = Convert.ToDecimal(GrandTotalArray[GrandTotalArrayIndex]) + Convert.ToDecimal(weeklyItem.GetQnt(year));
					GrandTotalArrayIndex++;
                    response.Output.Write("\"" + Misc.DisplayCurency(Convert.ToDouble(Convert.ToString(weeklyItem.GetAmount(year)).Replace(",", ""))) + "\",");
					GrandTotalArray[GrandTotalArrayIndex] = Convert.ToDecimal(GrandTotalArray[GrandTotalArrayIndex]) + Convert.ToDecimal(weeklyItem.GetAmount(year));
					GrandTotalArrayIndex++;
				}

				//Finsh row
                response.Output.Write("\n");
				GrandTotalArrayIndex = 0;
			}

			//Print the Grand Total
            response.Output.Write("Grand Total,,,");
			
			for (int i = 0; i < GrandTotalArray.Length; i++)
			{
				// If Quantity, print without the currency sign
				if(i % 2 == 0)
                    response.Output.Write("\"" + Misc.DisplayFormat2(Convert.ToDouble(Convert.ToString(GrandTotalArray[i]).Replace(",",""))) + "\",");
				// Otherwise, print with the currency sign
				else
                    response.Output.Write("\"" + Misc.DisplayCurency(Convert.ToDouble(GrandTotalArray[i].ToString().Replace(",", ""))) + "\",");
			}

			//End the response			
			response.End();
		}

		private void ShowReport()
		{
			DataSet dsRet = new DataSet();
			weeklySalesPerYearBLL = new WeeklySalesPerYearBLL(this);
			bool grossPrice = false;
			bool actualQuantity = false;

			if(Price == "Gross Sales")
				grossPrice=true;
			if(Quantity == "Actual")
				actualQuantity = true;

			//Run the report
			weeklySalesPerYearBLL.GetWeekSalesData(grossPrice,actualQuantity,Customer,ShipToIdx,ShipToLocation,CommodityIdx,VarietyIdx,SalesPerson,WarehouseIdx,CommoditySizeIdx,YearStartDate);

			//Display the Report
			DisplayHTML(Response, weeklySalesPerYearBLL);
		}

		private void SetCommentLine()
		{
			StringBuilder comment = new StringBuilder();

			if(YearStartDate.Date.ToString() != "")
			{
				comment.Append("Start Date: ");
				comment.Append(YearStartDate.ToShortDateString());
			}

			if(Customer != "")
			{
				comment.Append("; Customer: ");
				comment.Append(CustomerName);
			}

			if(ShipTo != "")
			{
				comment.Append("; Ship to: ");
				comment.Append(ShipTo);
			}

			if(CommodityIdx.Count != 0)
			{ 
				comment.Append("; Commodity: ");

				foreach(CriteriaBoxRecord record in CommodityIdx)
				{
					comment.Append(record.description);
					comment.Append(", ");
				}
				comment.Remove(comment.Length-2,2);
			}

			if(CommoditySizeIdx.Count != 0)
			{ 
				comment.Append("; Size: ");

				foreach(CriteriaBoxRecord record in CommoditySizeIdx)
				{
					comment.Append(record.description);
					comment.Append(", ");
				}
				comment.Remove(comment.Length-2,2);
			}

			if(Variety != "")
			{
				comment.Append("; Variety: ");
				comment.Append(Variety);
			}

			if(SalesPerson != "")
			{
				comment.Append("; Salesperson: ");
				comment.Append(SalesPerson);
			}

			if(WarehouseIdx.Count != 0)
			{ 
				comment.Append("; Warehouse ID: ");
				foreach(CriteriaBoxRecord record in WarehouseIdx)
				{
					comment.Append(record.text);
					comment.Append(", ");
				}
				comment.Remove(comment.Length-2,2);
			}

			if(Price != "")
			{
				comment.Append("; Price: ");
				comment.Append(Price);
			}

			if(Quantity != "")
			{
				comment.Append("; Quantity: ");
				comment.Append(Quantity);
			}

			comment.Append("     ");
			comment.Append(System.DateTime.Now.ToString());

			commentLineString = comment.ToString();
		}
	}
}
