<%@ Page language="c#" Inherits="WeeklySales.Reports.WeeklySales.CriteriaWeeklySales" Codebehind="CriteriaWeeklySales.aspx.cs" %>
<%@ Register Assembly="KeyCentralABZLib" Namespace="KeyCentralLib" TagPrefix="cc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>CustomerStatusInput</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
        </style>-->
        <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
    <body oncontextmenu="return false;">
         <form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
             <div class="scroll" id="BodyDiv">
				<script type="text/vbscript" language="VBScript">
			Function doConfirm(msg)
				if MsgBox(msg,vbOKOnly) = vbYes then
				doConfirm = true
				else
				doConfirm = false
				end if
			End function
				</script>
				<script type="text/javascript" language="javascript">
				function AskIfFieldsBlank(dateFieldName)
				{
					if(document.getElementById(dateFieldName +"_minDate").value =="" || document.getElementById(dateFieldName +"_maxDate").value =="")
					{
						alert('You must enter a date range for this report.');
						return false;
					}
					else
						return true;
				}
				</script>
				<table align="center">
					<tr>
						<td align="center"><div id="TitleDiv" runat="server"></div>
						</td>
					</tr>
					<tr>
						<td align="center" colspan="2">
                            &nbsp;<cc1:datecriteria id="ShipDate" runat="server"></cc1:datecriteria></td>
					</tr>
				</table>
				<table align="center">
					<tr>
				        <td>&nbsp;</td>
				        <td>
				            <table align="center">
				                <tr>
						            <td width="232" align="left" valign="bottom"><font face="Tahoma" size="2">Customer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ID:&nbsp;</font><asp:textbox id="CustomerIDTextBox" runat="server" EnterAsTab="" Width="100px" AutoPostBack="true" Visible="True" TabIndex="1" OnTextChanged="CustomerIDTextBox_OnTextChanged" onfocus="this.select();"></asp:textbox></td>
						            <td width="192" align="left"><asp:textbox id="CustomerKey" runat="server" Width="100px" Height="19" Visible="False"></asp:textbox></td>
					            </tr>
					            <tr>
						            <td width="232" align="left"><asp:listbox id="CustomerList" runat="server" Width="232px" Height="136px" Font-Names="Tahoma" AutoPostBack="True" Enabled="True" TabIndex="2" onselectedindexchanged="CustomerList_SelectedIndexChanged"></asp:listbox></td>
						            <td width="192" valign="top" align="left"><asp:label id="customerName" runat="server" Width="300px" Font-Names="Tahoma" Font-Size="X-Small"></asp:label></td>
					            </tr>
					            <tr>
						            <td width="232" align="left"><font face="Tahoma" size="2">Ship To</font></td>
						            <td width="192" align="left"><asp:textbox id="ShipToKey" runat="server" Width="100px" Height="19" Visible="False"></asp:textbox></td>
					            </tr>
					            <tr>
						            <td width="232" align="left"><asp:listbox id="ShipToList" runat="server" Width="232px" Height="136px" Font-Names="Tahoma" TabIndex="3" AutoPostBack="True" Enabled="True" onselectedindexchanged="ShipToList_SelectedIndexChanged"></asp:listbox></td>
						            <td width="192" valign="top" align="left"><asp:label id="ShipToName" runat="server" Width="300px" Font-Names="Tahoma" Font-Size="X-Small"></asp:label></td>
					            </tr>
					        </table>
				        </td>
				    </tr>
					<tr>
						<td align="right"></td>
						<td>
                            <cc1:commoditylookup id="CommodityLookup1" runat="server"></cc1:commoditylookup>
                        </td>
					</tr>
					<tr>
					    <td>
							<input type="hidden" id="commodity_Key" runat="server" name="commodity_Key"/>
							<input type="hidden" id="txtCommodity" runat="server" name="txtCommodity"/>
						</td>
					</tr>
					<tr>
						<td align="right"><asp:label id="lbVariety" runat="server">Variety:</asp:label></td>
						<td><asp:textbox id="txtVariety" runat="server" LeftClickLookup="" SingleLookup="variety_Key" LookupSearch="VarietySingle"
								EnterAsTab="" LookupSearchParam1="txtCommodity"></asp:textbox>
							<input type="hidden" id="variety_Key" runat="server" name="variety_Key"/></td>
					</tr>
					<tr>
						<td align="right"><asp:label id="lbSalePerson" runat="server">Salesperson:</asp:label></td>
						<td><asp:textbox id="txtSalesPerson" runat="server" SingleLookup="salesperson_Key" LookupSearch="SalespersonSingle"
								EnterAsTab=""></asp:textbox>
							<input type="hidden" id="salesperson_Key" runat="server" name="salesperson_Key"/></td>
					</tr>
				</table>
				<table align="center">
					<tr>
						<td align="center">
                            <cc1:warehouseid id="WarehouseId1" runat="server"></cc1:warehouseid>
                        </td>
					</tr>
				</table>
				<table align="center">
					<tr>
						<td>
							<asp:RadioButtonList id="Quantity" runat="server" RepeatDirection="Horizontal">
								<asp:ListItem Value="Actual">Actual Quantity</asp:ListItem>
								<asp:ListItem Value="Equivalent" Selected="True">Equivalent Quantity</asp:ListItem>
							</asp:RadioButtonList></td>
					</tr>
					<tr>
						<td>
							<asp:RadioButtonList id="Price" runat="server" RepeatDirection="Horizontal">
								<asp:ListItem Value="Gross Sales">Sales Price&#160;&#160;&#160;&#160;&#160;&#160;&#160;</asp:ListItem>
								<asp:ListItem Value="F.O.B. Sales" Selected="True">F.O.B. Price</asp:ListItem>
							</asp:RadioButtonList></td>
					</tr>
				</table>
				<table align="center">
					<tr>
						<td></td>
						<td><asp:button id="cmdRun" runat="server" Text="Run Report" TabFinish="" EnterAsTab="" onclick="cmdRun_Click"></asp:button></td>
						<td><asp:button id="cmdReport" runat="server" Text="Return to Reports Menu" CausesValidation="False" onclick="cmdReport_Click"></asp:button></td>
					</tr>
				</table>
				<asp:Label id="Message" runat="server" ForeColor="Red">Message</asp:Label>
			</div> <!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
		</form>
	</body>
</html>
