using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Text;
using KeyCentral.Functions;
using WeeklySales.App_Code;
using WeeklySales.Reports.WeeklySales;
using KeyCentral.Lookups;

namespace WeeklySales.Reports.WeeklySales
{
	public partial class ReportWeekly :  KeyCentral.Functions.BasePage
	{
		#region Declares
		protected string QuantityStyle ="";
		protected string priceHeader = "";
		protected System.Web.UI.WebControls.Repeater Repeater2;
		protected ReportTotalsHelper totalHelper;
		protected WeeklySalesData WeeklySalesData;
		protected DateTime minDate;
		protected DateTime maxDate;

		WeeklySalesBLL weeklySalesBLL;
		#endregion
		
		#region Param Properties
		private string MinShipDate{get{return Session["MinShipDate"].ToString();}}
		private string MaxShipDate{get{return Session["MaxShipDate"].ToString();}}
		private string Price{get{return Session["Price"].ToString();}}
		private string Quantity{get{return Session["Quantity"].ToString();}}
		private string Customer { get { return Session["Customer"].ToString(); } }
		private string CustomerName { get { return Session["CustomerName"].ToString(); } }
		private string ShipTo{get{return Session["ShipTo"].ToString();}}
		private string ShipToIdx{get{return Session["ShipToIdx"].ToString().Trim();}}
		private string ShipToLocation{get{return Session["ShipToLocation"].ToString().Trim();}}
		private ArrayList CommodityIdx{get{return (ArrayList)Session["CommodityIdx"];}}
		private string Variety{get{return Session["Variety"].ToString();}}
		private string VarietyIdx{get{return Session["VarietyIdx"].ToString();}}
		private string SalesPerson{get{return Session["SalesPerson"].ToString();}}
		private ArrayList WarehouseIdx{get{return (ArrayList)Session["WarehouseIdx"];}}
		#endregion

		#region Totals

		private bool IsPartWeek(DateTime thisRecordDate)
		{
			//Check for this being first week in report and not starting on Monday
			if(KeyCentral.Functions.Misc.DateToWeek(MinShipDate) == KeyCentral.Functions.Misc.DateToWeek(thisRecordDate) && (int)DateTime.Parse(MinShipDate).DayOfWeek !=1)
				return true;
			//Check for this being last week in report and not ending on Sunday
			if(KeyCentral.Functions.Misc.DateToWeek(MaxShipDate) == KeyCentral.Functions.Misc.DateToWeek(thisRecordDate) && (int)DateTime.Parse(MaxShipDate).DayOfWeek !=0)
				return true;

			return false;
		}


		private string GetField(object oItem,string strField)
		{
			return DataBinder.Eval(oItem,"DataItem." + strField).ToString();
		}
		private string GetDateField(object oItem,string strField)
		{
			string str = DataBinder.Eval(oItem,"DataItem." + strField).ToString();
			if(str.IndexOf(" ",0)>0)
				return str.Substring(0,str.IndexOf(" ",0));
			else
				return str;
		}
		#endregion

		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Weekly Sales Report") == false)
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Sales Reports");
			
			KeyCentral.Functions.JavaStatusMessage status = new KeyCentral.Functions.JavaStatusMessage(this);
			status.ShowMessage("Computing");

			minDate = DateTime.Parse(MinShipDate);
			maxDate = DateTime.Parse(MaxShipDate);

			SetCommentLine();
			ShowReport(true);
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}
		
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Display code
		private int lastWeek=-1;
		private DateTime lastDate;

		public string DisplayReportItem(WeeklySalesBLL.ReportItem data)
		{
			string display="";

			if(lastWeek==-1)
				display += RenderMissingWeeks(DateTime.Parse(this.MinShipDate).AddDays(-7),data.EndDate);
			else
				display += RenderMissingWeeks(lastDate,data.StartDate);	

			//Only display the headers when the week changes
			if(lastWeek != data.Week)
				display += DisplayWeekHeader(data);

			//Display the details
			int loopLimit = data.Details.Count;
			for(int x=0;x<loopLimit;x++)
				display += DisplayDetail((WeeklySalesBLL.ItemDetail)data.Details[x]);

			//Only display the headers when the week changes
			if(lastWeek != data.Week)
				display += DisplayWeekFooter(data);

			//save 'last'
			lastWeek = data.Week;
			lastDate = data.StartDate;

			return display;
		}
		public string DisplayFooter()
		{
			string display = String.Empty;

			//Display any missing weeks at the end of our search
			display += RenderMissingWeeks(lastDate,DateTime.Parse(this.MaxShipDate).AddDays(7));

			//Sub total header
			display += @"
					<table cellSpacing='0' cellPadding='0' border='0' width='720'><tr>
						<td width='95' class='OrderHeader4' align=center>" + QuantityStyle + @"</td>
						<td width='509' class='OrderHeader5' align=center colspan='3'>Totals</td>
						<td width='105' class='OrderHeader6' align=left>&nbsp;" + priceHeader + @"</td>
					</tr></table><table cellSpacing='0' cellPadding='3' border='0' width='720'>";
				
			//Output Sub Totals
			int loopLimit = weeklySalesBLL.TotalDetails.Count;
			for(int x=0;x<loopLimit;x++)
				display += @"
					<tr>
						<td width='90' class='DetailLine1' align=right>" + FormatQnt(((WeeklySalesBLL.ItemDetail)weeklySalesBLL.TotalDetails[x]).Qnt) + @"</td>
						<td width='511' class='DetailLine2' align=left colspan='3'>" + KeyCentral.Functions.StringFormater.FormatString(((WeeklySalesBLL.ItemDetail)weeklySalesBLL.TotalDetails[x]).Desc,66)+ @"</td>
						<td width='100' class='DetailLine3' align=right>" + (((WeeklySalesBLL.ItemDetail)weeklySalesBLL.TotalDetails[x]).Amount).ToString("$###,##0.00") + @"</td>
					</tr>";

			//Output Grand Totals
			display += @"
				</table><table cellSpacing='0' cellPadding='3' border='0' width='720'><tr>
					<td width='90' class='OrderHeader4' align=right>" + FormatQnt(weeklySalesBLL.TotalQnt) + @"</td>
					<td width='76' class='OrderHeader5'>" + QuantityStyle + @"</td>
					<td width='180' class='OrderHeader5' align=right>Grand Total</td>
					<td width='250' class='OrderHeader5' align=right>" + priceHeader + @"</td>
					<td width='100' class='OrderHeader6' align=right>" + weeklySalesBLL.TotalAmount.ToString("C") + @"</td>
				</tr></table><table cellSpacing='0' cellPadding='0' border='0' width='720'>";
				

			return display;
		}


		private string DisplayWeekHeader(WeeklySalesBLL.ReportItem data)
		{
			string display="";

			
			string partWeek = "&nbsp;";

			if(IsPartWeek(data.StartDate))
				partWeek = "Partial Week";

			display += @"
					<table cellSpacing='0' cellPadding='0' border='0' width='720'><tr>
						<td width='94' class='OrderHeader1' align=center>" + QuantityStyle + @"</td>
						<td width='56' class='OrderHeader2'>&nbsp;Week&nbsp;" + data.Week + @"&nbsp;</td>
						<td width='230' class='OrderHeader2' align=left>" + GetDateRange(data.StartDate,data.EndDate) + @"</td>
						<td width='225' class='OrderHeader2a' align=left>&nbsp;" + partWeek + @"</td>
						<td width='104' class='OrderHeader3' align=left>&nbsp;" + priceHeader + @"</td>
					</tr></table><table cellSpacing='0' cellPadding='3' border='0' width='720'>";
				
			return display;
		}

		private string DisplayDetail(WeeklySalesBLL.ItemDetail data)
		{
			string display="";

			display +=@"
					<tr>
						<td width='90' class='DetailLine1' align=right>" + FormatQnt(data.Qnt)+ @"</td>
						<td width='511' class='DetailLine2' align=left colspan='3'>" + KeyCentral.Functions.StringFormater.FormatString(data.Desc,66) + @"</td>
						<td width='100' class='DetailLine3' align=right>" + ((double)data.Amount).ToString("C") + @"</td>
					</tr>";

			return display;
		}
		private string DisplayWeekFooter(WeeklySalesBLL.ReportItem data)
		{
			string display="";

			display += @"</table><table cellSpacing='0' cellPadding='0' border='0' width='720'>
						<tr>
							<td width='90' class='OrderHeader7' align=right>" + FormatQnt(data.Qnt)+ @"</td>
							<td width='511' class='OrderHeader7' align=Center colspan='3'>" + "Sub Total for Week " + data.Week.ToString() + @"</td>
							<td width='100' class='OrderHeader7' align=right>" + (data.Amount).ToString("C") + @"</td>
						</tr></table>";

			return display;
		}


		private string GetDateRange(DateTime date1,DateTime date2)
		{
			string startDate;
			string endDate; 

			if(date1 < minDate)
				startDate = minDate.ToShortDateString();
			else
				startDate = date1.ToShortDateString();

			if(date2 > maxDate)
				endDate = maxDate.ToShortDateString();
			else
				endDate = date2.ToShortDateString();

			return startDate + " > " + endDate;
		}
		
		private string FormatQnt(float value)
		{
			if (QuantityStyle == "Actual")
				return value.ToString("###,##0");
			else
				return value.ToString("###,##0.00");
		}
		private string FormatQnt(double value)
		{
			if (QuantityStyle == "Actual")
				return value.ToString("###,##0");
			else
				return value.ToString("###,##0.00");
		}
		private string RenderMissingWeeks(DateTime startDate,DateTime endDate)
		{
			string ret="";
			string partWeek = "&nbsp;";
			//StartWeek and startYear are the next week after the last record
			DateTime temp =startDate.AddDays(7);

			string startWeek = KeyCentral.Functions.Misc.DateToWeek(temp);
			int startYear = temp.Year;

			string endWeek = KeyCentral.Functions.Misc.DateToWeek(endDate);


			while(startWeek !=endWeek)
			{
				
				if(IsPartWeek(temp))
					partWeek = "Partial Week";
				//********************************************** NEED TO FIX GetDateRange part of this

				ret += @"
					</table><table cellSpacing='0' cellPadding='0' border='0' width='720'><tr>
						<td class='OrderHeader1' width='95'>&nbsp;</td>
						<td class='OrderHeader2' width='2'>&nbsp;</td>
						<td width='56' class='OrderHeader2'>&nbsp;Week&nbsp;" + startWeek + @"&nbsp;</td>
						<td width='180' class='OrderHeader2' align=left>" + GetDateRange(temp,DateTime.Parse(Misc.GetSundayOfThisWeek(temp))) + @"</td>
						<td width='50' class='OrderHeader2' align=left><b>" + "None" + @"</b></td>
						<td width='335' class='OrderHeader2a' align=left>" + partWeek + @"</td>
					</tr></table><table cellSpacing='0' cellPadding='0' border='0' width='720'>";

				//Move the startWeek and startYear a week foward
				temp = KeyCentral.Functions.Misc.WeekToStartDate(Int32.Parse(startWeek),startYear);
				temp = temp.AddDays(7);
				startWeek = KeyCentral.Functions.Misc.DateToWeek(temp);
				startYear = temp.Year;
				partWeek = "&nbsp;";
			}
			return ret;
		}
		#endregion

		#region Private Helpers
		private void SetCommentLine()
		{
			priceHeader = Price;
			QuantityStyle = Quantity;
			StringBuilder comment = new StringBuilder();

			if(MinShipDate != "")
			{ 
				comment.Append(MinShipDate);
				comment.Append(" TO ");
				comment.Append(MaxShipDate);
				comment.Append(" ");
			}
			if(Customer != "")
			{ 
				comment.Append(" Customer: ");
				comment.Append(CustomerName);
				comment.Append(" ");
			}
			if(ShipTo != "")
			{ 
				comment.Append(" ShipTo: ");
				comment.Append(ShipTo);
				comment.Append(" ");
			}
			if(Variety != "")
			{ 
				comment.Append(" Variety: ");
				comment.Append(Variety);
				comment.Append(" ");
			}
			if(SalesPerson != "")
			{ 
				comment.Append(" Salesperson: ");
				comment.Append(SalesPerson);
				comment.Append(" ");
			}
			if (WarehouseIdx.Count!= 0)
			{ 
				comment.Append("Warehouse: ");
				foreach(CriteriaBoxRecord record in WarehouseIdx)
				{
					comment.Append(record.text);
					comment.Append(", ");
				}
				comment.Remove(comment.Length-2,2);
				comment.Append(" ");
			}
			
			if (CommodityIdx.Count!= 0)
			{ 
				comment.Append("Commodity: ");
				foreach(CriteriaBoxRecord record in CommodityIdx)
				{
					comment.Append(record.description);
					comment.Append(", ");
				}
				comment.Remove(comment.Length-2,2);
				comment.Append(" ");
			}

			CommentLine.InnerText = comment.ToString();
		}

		private void ShowReport(bool bDataBind)
		{
			
			weeklySalesBLL = new WeeklySalesBLL(this);

			bool grossPrice = false;
			bool actualQuantity = false;

			if(Price == "Gross Sales")
				grossPrice=true;
			if(Quantity == "Actual")
				actualQuantity = true;

			weeklySalesBLL.GetWeekSalesData(grossPrice,actualQuantity,Customer,ShipToIdx,ShipToLocation,CommodityIdx,VarietyIdx,SalesPerson,WarehouseIdx,new ArrayList(),MinShipDate,MaxShipDate);

			if (weeklySalesBLL.ReportItems.Count >0)
			{
				//this.Repeater1.DataSource = dsRet.Tables[0].DefaultView;
				this.Repeater1.DataSource = weeklySalesBLL.ReportItems;
				if(bDataBind)
					Repeater1.DataBind();
			}
			else
			{
				JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"]) + "&&Records=None");
			}
		}
		
		#endregion
	}
}
