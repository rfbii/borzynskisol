using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Text;
using KeyCentral.Functions;
using WeeklySales.App_Code;
using WeeklySales.Reports.RawSales;
using KeyCentral.Lookups;

namespace WeeklySales.Reports.RawSales
{
	public partial class ReportRawSales :  KeyCentral.Functions.BasePage
	{
		#region Declares
		protected string QuantityStyle ="";
		protected string priceHeader = "";
		protected System.Web.UI.WebControls.Repeater Repeater2;
		protected System.Web.UI.WebControls.Repeater Repeater1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl CommentLine;
		protected ReportTotalsHelper totalHelper;
		//protected DateTime minDate;
		//protected DateTime maxDate;

		RawSalesBLL rawSalesBLL;
		#endregion
		
		#region Param Properties
		private string MinShipDate{get{return Session["MinShipDate"].ToString();}}
		private string MaxShipDate{get{return Session["MaxShipDate"].ToString();}}
		private string Customer { get { return Session["Customer"].ToString(); } }
		private string CustomerName { get { return Session["CustomerName"].ToString(); } }
		private string ShipTo{get{return Session["ShipTo"].ToString();}}
		private string ShipToIdx{get{return Session["ShipToIdx"].ToString().Trim();}}
		private string ShipToLocation{get{return Session["ShipToLocation"].ToString().Trim();}}
		private ArrayList CommodityIdx{get{return (ArrayList)Session["CommodityIdx"];}}
		private string Variety{get{return Session["Variety"].ToString();}}
		private string VarietyIdx{get{return Session["VarietyIdx"].ToString();}}
		private string SalesPerson{get{return Session["SalesPerson"].ToString();}}
		private ArrayList WarehouseIdx{get{return (ArrayList)Session["WarehouseIdx"];}}
		private DateTime YearStartDate{get{return DateTime.Parse(Session["YearStartDate"].ToString());}}

		private string commentLineString = "";
		#endregion

		#region Totals

		private bool IsPartWeek(DateTime thisRecordDate)
		{
			//Check for this being first week in report and not starting on Monday
			if(KeyCentral.Functions.Misc.DateToWeek(MinShipDate) == KeyCentral.Functions.Misc.DateToWeek(thisRecordDate) && (int)DateTime.Parse(MinShipDate).DayOfWeek !=1)
				return true;
			//Check for this being last week in report and not ending on Sunday
			if(KeyCentral.Functions.Misc.DateToWeek(MaxShipDate) == KeyCentral.Functions.Misc.DateToWeek(thisRecordDate) && (int)DateTime.Parse(MaxShipDate).DayOfWeek !=0)
				return true;

			return false;
		}


		private string GetField(object oItem,string strField)
		{
			return DataBinder.Eval(oItem,"DataItem." + strField).ToString();
		}
		private string GetDateField(object oItem,string strField)
		{
			string str = DataBinder.Eval(oItem,"DataItem." + strField).ToString();
			if(str.IndexOf(" ",0)>0)
				return str.Substring(0,str.IndexOf(" ",0));
			else
				return str;
		}
		#endregion

		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Weekly Sales Report") == false)
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=");
			
			//KeyCentral.Functions.JavaStatusMessage status = new KeyCentral.Functions.JavaStatusMessage(this);
			//status.ShowMessage("Computing");

			//minDate = DateTime.Parse(MinShipDate);
			//maxDate = DateTime.Parse(MaxShipDate);

			SetCommentLine();
			ShowReport();
		}
		#endregion

		private void DisplayHTML(System.Web.HttpResponse response,RawSalesBLL reportData)
		{
            response.Clear();
			int Week = -1;
			//Set the response contenct type
			Response.ContentType = "Application/x-msexcel";
			Response.AddHeader("content-disposition", "attachment; filename=\"" + "RawSalesData.csv" + "\"");
			//response.ContentType =  @"text/vnd.msexcel";

			//Output the table header
			response.Output.WriteLine("Raw Sales Data Report");
			response.Output.WriteLine("" + commentLineString + "");

			//Display the Header row
			response.Output.Write("SoNo,");
			response.Output.Write("ShipDateTime,");
			response.Output.Write("Year,");
			response.Output.Write("Month,");
			response.Output.Write("Day,");
			response.Output.Write("Week,");
			response.Output.Write("AWeek,");
			response.Output.Write("Year-Week,");
			response.Output.Write("Year-Month,");
			response.Output.Write("CustName-ShipToDescr,");
			response.Output.Write("CustName,");
			response.Output.Write("CustCity,");
			response.Output.Write("CustState,");
			response.Output.Write("ShipToName,");
			response.Output.Write("ShipToDescr,");
			response.Output.Write("ShipToCity,");
			response.Output.Write("ShipToState,");
			response.Output.Write("ShipToZip,");
			response.Output.Write("Type,");
			response.Output.Write("SalesPerson,");
			response.Output.Write("Warehouse,");
			response.Output.Write("InvDescr,");
			response.Output.Write("Commodity,");
			response.Output.Write("Variety,");
			response.Output.Write("Style,");
			response.Output.Write("Size,");
			response.Output.Write("Grade,");
			response.Output.Write("Label,");
			response.Output.Write("Qnt,");
			response.Output.Write("EquivQnt,");
			response.Output.Write("FOBAmt,");
			//response.Output.Write("Weight,");
			//response.Output.Write("FreightAmt,");
			response.Output.Write("SalesAmt,");
			response.Output.Write("LotID,");
			response.Output.Write("GrowerName,");
			response.Output.Write("FillType,");

			//Finish the header row
			response.Output.Write("\n");

			//Loop through the data outputing a row for each record
			foreach(RawSalesBLL.ItemDetail itemDetail in reportData.RawDetails)
			{
				//Output the Week and desc
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.SoNo) + "\",");
				response.Output.Write("\"" + itemDetail.ShipDateTime.ToShortDateString() + "\",");
				response.Output.Write("\"" + itemDetail.ShipDateTime.Year + "\",");
				response.Output.Write("\"" + itemDetail.ShipDateTime.Month + "\",");
				response.Output.Write("\"" + itemDetail.ShipDateTime.Day + "\",");

				Misc.WeekBaseData weekData = Misc.GetWeekBase(YearStartDate);
				Week = Misc.GetDateBasedWeek(itemDetail.ShipDateTime,weekData,false);

				response.Output.Write("\"" + Week + "\",");
				response.Output.Write("\"" + Misc.DateToWeek(itemDetail.ShipDateTime) + "\",");
				response.Output.Write("\"" + itemDetail.ShipDateTime.Year + "-");
				if(Week < 10)
					response.Output.Write("0");
				response.Output.Write(Week + "\",");

				response.Output.Write("\"" + itemDetail.ShipDateTime.Year + "-");
				if((Convert.ToInt32(itemDetail.ShipDateTime.Month)) < 10)
					response.Output.Write("0");
				response.Output.Write(itemDetail.ShipDateTime.Month + "\",");

				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.CustName) + "-" + StringFormater.FormatForCSV(itemDetail.ShipToDescr) + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.CustName) + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.CustCity) + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.CustState) + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.ShipToName) + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.ShipToDescr) + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.ShipToCity) + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.ShipToState) + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.ShipToZip) + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.Type) + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.SalesPerson) + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.Warehouse) + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.Invdescr) + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.Commodity) + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.Variety) + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.Style) + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.Size) + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.Grade) + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.Label) + "\",");
				response.Output.Write("\"" + itemDetail.Qnt + "\",");
				response.Output.Write("\"" + itemDetail.EquivQnt + "\",");
				response.Output.Write("\"" + itemDetail.FOBQnt + "\",");
				//response.Output.Write("\"" + itemDetail.Weight + "\",");
				//response.Output.Write("\"" + itemDetail.FreightAmt + "\",");
				response.Output.Write("\"" + itemDetail.SalesQnt + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.LotId) + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.GrowerName) + "\",");
				response.Output.Write("\"" + StringFormater.FormatForCSV(itemDetail.FillType) + "\",");

				//Finsh row
				response.Output.Write("\n");
			}

			//End the table and the response
			response.End();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}
		
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Private Helpers
		private void SetCommentLine()
		{
			StringBuilder comment = new StringBuilder();

			if(MinShipDate != "")
			{ 
				comment.Append(MinShipDate);
				comment.Append(" TO ");
				comment.Append(MaxShipDate);
				comment.Append(" ");
			}
			if(Customer != "")
			{ 
				comment.Append(" Customer: ");
				comment.Append(CustomerName.Replace(",", ""));
				comment.Append(" ");
			}
			if(ShipTo != "")
			{ 
				comment.Append(" ShipTo: ");
				comment.Append(ShipTo);
				comment.Append(" ");
			}
			if(Variety != "")
			{ 
				comment.Append(" Variety: ");
				comment.Append(Variety);
				comment.Append(" ");
			}
			if(SalesPerson != "")
			{ 
				comment.Append(" Salesperson: ");
				comment.Append(SalesPerson);
				comment.Append(" ");
			}
			if (WarehouseIdx.Count!= 0)
			{ 
				comment.Append("Warehouse: ");
				foreach(CriteriaBoxRecord record in WarehouseIdx)
				{
					comment.Append(record.text);
					comment.Append(", ");
				}
				comment.Remove(comment.Length-2,2);
				comment.Append(" ");
			}
			
			if (CommodityIdx.Count!= 0)
			{ 
				comment.Append("Commodity: ");
				foreach(CriteriaBoxRecord record in CommodityIdx)
				{
					comment.Append(record.description);
					comment.Append(", ");
				}
				comment.Remove(comment.Length-2,2);
				comment.Append(" ");
			}

			commentLineString = comment.ToString();
		}

		private void ShowReport()
		{
			rawSalesBLL = new RawSalesBLL(this);

			rawSalesBLL.GetRawSalesData(Customer,ShipToIdx,ShipToLocation,CommodityIdx,VarietyIdx,SalesPerson,WarehouseIdx,new ArrayList(),MinShipDate,MaxShipDate);

			if(rawSalesBLL.RawDetails.Count > 0)
			{
				//Display the Report
				DisplayHTML(Response,rawSalesBLL);
			}
			else
			{
				Response.Write("<script>alert('No Records Found.');</script>");
				Response.Write("<script>window.close();</script>");
				//Response.Redirect(Misc.DecodeQueryString(Request.QueryString["LastPage"]) + "&&Records=None");
				//JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"]) + "&&Records=None");
			}
		}
		
		#endregion
	}
}
