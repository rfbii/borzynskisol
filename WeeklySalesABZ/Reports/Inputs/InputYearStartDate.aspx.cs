using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb; 
using Oracle.DataAccess.Client;
using KeyCentral.Functions;

namespace WeeklySales.Reports.Inputs
{

	/// <summary>
	/// Summary description for Expense Type.
	/// </summary>
	public partial class InputYearStartDate : BasePage
	{
		#region Declares

		#endregion

		#region events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Mayan Data Central Setup") == false)
			{
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Weekly Sales Reports");
			}

			if (!IsPostBack)
			{
				Message.Text = "";
				FillDays();
			}
		}
		protected void save_Click(object sender, System.EventArgs e)
		{
			Message.Text = "";
			SqlConnection Conn = this.GetSQLConn("date_TextChanged");
			SqlCommand updateCMD = new SqlCommand("use WeeklySales; update [Year_Start_Date] Set Year_Start_Date=@Date", Conn);
			updateCMD.Parameters.Add("@Date", startDate.Text);

			updateCMD.ExecuteNonQuery();
			Message.Text = "Date Updated.";

			Conn.Close();
			Conn.Dispose();
		}
		protected void Return_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Reports/Sales Reports");
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

		#region Private helpers
		private void FillDays()
		{
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("use WeeklySales; select Year_Start_Date from [Year_Start_Date]", Conn);

			SqlDataReader myReader = selectCMD.ExecuteReader();

			if (myReader.Read() == true)
			{
				startDate.Text = myReader.GetDateTime(0).ToShortDateString();
			}

			myReader.Close();
			Conn.Close();
			Conn.Dispose();
		}
		#endregion
	}
}
