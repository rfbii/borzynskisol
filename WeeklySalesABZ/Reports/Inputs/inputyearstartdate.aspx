<%@ Register TagPrefix="cc1" Namespace="Cet" Assembly="Cet.ConfirmButton" %>
<%@ Page language="c#" Inherits="WeeklySales.Reports.Inputs.InputYearStartDate" Codebehind="InputYearStartDate.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
		<title>Year Start Date</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
		</style>-->
        <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body bgcolor="white" onload="document.Form1.startDate.focus()">
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
				<div style="DISPLAY: inline; Z-INDEX: 101; LEFT: 264px; WIDTH: 160px; POSITION: absolute; TOP: 80px; HEIGHT: 24px"
					align="center"><font face="Tahoma">Year Start Date</font></div>
				<asp:label id="Message" style="Z-INDEX: 104; LEFT: 8px; POSITION: absolute; TOP: 176px" runat="server"
					ForeColor="Red"></asp:label><asp:textbox id="startDate" style="Z-INDEX: 102; LEFT: 296px; POSITION: absolute; TOP: 128px"
					tabIndex="1" runat="server" EnterAsTab="" CalendarPopUp="" Width="104px"></asp:textbox><asp:button id="Return" style="Z-INDEX: 103; LEFT: 338px; POSITION: absolute; TOP: 216px" tabIndex="3"
					runat="server" EnterAsTab="" Width="160px" Height="24px" Text="Return to Reports Menu" onclick="Return_Click"></asp:button><asp:button id="save" style="Z-INDEX: 103; LEFT: 228px; POSITION: absolute; TOP: 216px" tabIndex="3"
					runat="server" EnterAsTab="" Width="100px" Height="24px" Text="Save" onclick="save_Click"></asp:button>
                        <asp:comparevalidator id="CompareValidator2" style="Z-INDEX: 104; LEFT: 8px; POSITION: absolute; TOP: 176px"
					runat="server" ControlToValidate="startDate" ErrorMessage="Invalid Year Start Date." Display="Dynamic" Type="Date" Operator="DataTypeCheck"></asp:comparevalidator></div>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"--></form>
	</body>
</html>
