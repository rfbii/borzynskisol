using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using KeyCentral.Functions;
using KeyCentral.Lookups;
using WeeklySales.App_Code;

namespace WeeklySales.Reports.WeeklySalesPerYear
{
	public partial class ReportYearWeekly :  BasePage
	{
		protected string QuantityStyle ="";
		protected string priceHeader = "";
		protected Repeater Repeater2;
		protected Repeater Repeater1;
		protected HtmlGenericControl CommentLine;
		protected ReportTotalsHelper totalHelper;
		protected WeeklySalesPerYearBLL weeklySalesPerYearBLL;
		protected DateTime minDate;
		protected DateTime maxDate;
		
		#region Param Properties
		private string Price{get{return Session["Price"].ToString();}}
		private string Quantity{get{return Session["Quantity"].ToString();}}
		private string Customer { get { return Session["Customer"].ToString(); } }
		private string CustomerName { get { return Session["CustomerName"].ToString(); } }
		private string ShipTo{get{return Session["ShipTo"].ToString();}}
		private string ShipToIdx{get{return Session["ShipToIdx"].ToString().Trim();}}
		private string ShipToLocation{get{return Session["ShipToLocation"].ToString().Trim();}}
		private string Variety{get{return Session["Variety"].ToString();}}
		private string VarietyIdx{get{return Session["VarietyIdx"].ToString();}}
		private string SalesPerson{get{return Session["SalesPerson"].ToString();}}
		private DateTime YearStartDate{get{return DateTime.Parse(Session["YearStartDate"].ToString());}}//
		private ArrayList WarehouseIdx{get{return (ArrayList)Session["WarehouseIdx"];}}
		private ArrayList CommodityIdx{get{return (ArrayList)Session["CommodityIdx"];}}
		private ArrayList CommoditySizeIdx{get{return (ArrayList)Session["CommoditySizeIdx"];}}
		#endregion

		#region Vars
		private ArrayList Qnts = new ArrayList();
		private ArrayList Product = new ArrayList();
		private ArrayList Amount = new ArrayList();
		private ArrayList shipTo = new ArrayList();
		private ArrayList SubQnts = new ArrayList();
		private ArrayList SubProduct = new ArrayList();
		private ArrayList SubAmount = new ArrayList();
		#endregion
		
		protected void Page_Load(object sender, EventArgs e)
		{
            if (CheckSecurity("Weekly Sales Report") == false)
            {
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Sales Reports");
            }

			ShowReport();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}
		
		private void InitializeComponent()
		{    
			this.Load += new EventHandler(this.Page_Load);

		}
		#endregion

        private void ShowReport()
        {
            weeklySalesPerYearBLL = new WeeklySalesPerYearBLL(this);
            bool grossPrice = false;
            bool actualQuantity = false;

            if (Price == "Gross Sales")
                grossPrice = true;
            if (Quantity == "Actual")
                actualQuantity = true;

            //Run the report
            weeklySalesPerYearBLL.GetWeekSalesData(grossPrice, actualQuantity, Customer, ShipToIdx, ShipToLocation, CommodityIdx, VarietyIdx, SalesPerson, WarehouseIdx, CommoditySizeIdx, YearStartDate);

            //Display the Report
            DisplayHTML(Response, weeklySalesPerYearBLL);
        } 

		private void DisplayHTML(System.Web.HttpResponse response, WeeklySalesPerYearBLL reportData)
		{
			//Set the response content type
			response.ContentType =  @"application/vnd.ms-excel";
            Response.AddHeader("content-disposition", "attachment; filename=\"" + "ReportYearWeekly.csv" + "\"");

			//Display the Header row
            response.Output.Write("Week,");
            response.Output.Write("INVCDESCR,");
            response.Output.Write("StartDate,");
            response.Output.Write("EndDate,");

			//foreach(int year in reportData.Years)
			for(int i=reportData.Years.Count -1;i>=0;i--)
			{
				int year= (int)reportData.Years[i];
                response.Output.Write("\"" + "Qnt" + year.ToString() + "-" + (year + 1).ToString() + "\",");
                response.Output.Write("\"" + "Amount" + year.ToString() + "-" + (year+1).ToString() + "\",");
			}
			//Finish the header row
            response.Output.Write("\n");

            //Loop through the data outputing a row for each record
			foreach(WeeklySalesPerYearCBO.ReportItem reportItem in reportData.ReportItems)
			{
                //Output the Week and desc
                response.Output.Write("\"" + reportItem.Week.ToString() + "\",");
                response.Output.Write("\"" + reportItem.Desc + "\",");
                response.Output.Write("\"" + reportItem.StartDate.ToString("MM-dd") + "\",");
                response.Output.Write("\"" + reportItem.EndDate.ToString("MM-dd") + "\",");
                
				//foreach(int year in reportData.Years)
				for(int i=reportData.Years.Count -1;i>=0;i--)
				{
					int year= (int)reportData.Years[i];
                    response.Output.Write("\"" + reportItem.GetQnt(year) + "\",");
                    response.Output.Write("\"" + reportItem.GetAmount(year) + "\",");
				}

				//Finsh row
                response.Output.Write("\n");
			}

			//End the table and the response
			response.End();
		}
		
		
	}
}
