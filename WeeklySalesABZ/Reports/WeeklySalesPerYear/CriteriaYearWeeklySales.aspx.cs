using System;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Oracle.DataAccess.Client;
using KeyCentral.Functions;
using KeyCentral.Lookups;

namespace WeeklySales.Reports.WeeklySalesPerYear
{

	/// <summary>
	/// Summary description for CriteriaWeeklySales.
	/// </summary>
	public partial class CriteriaYearWeeklySales :  BasePage
	{
		#region Declares
		ReportSessionManager reportSessionManager;
		#endregion
	
		#region Events
		protected void Page_Load(object sender, EventArgs e)
		{
			//Check Secuirty
			if (CheckSecurity("Weekly Sales Report") == false)
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Sales Reports");

			if (!IsPostBack)
			{
				FillCustomerList();
			}

			#region SessionManager
			reportSessionManager = new ReportSessionManager(this,Request["ReportName"].ToString());
			
			//Load Vars from session if this report is in SessionManager
			if(IsPostBack == false && reportSessionManager.CheckReportVars())
			{
				WarehouseId1.Keys = (ArrayList)Session["WarehouseIdx"];
				CommodityLookup1.Keys = (ArrayList)Session["CommodityIdx"];
				CommoditySizeLookup1.Keys = (ArrayList)Session["CommoditySizeIdx"];
				this.txtSalesPerson.Text = Session["SalesPerson"].ToString();
				this.yearStartDate.Text = Session["YearStartDate"].ToString();

				if(Session["Price"].ToString() != "")
					this.Price.SelectedValue = Session["Price"].ToString();

				if(Session["Quantity"].ToString() != "")
					this.Quantity.SelectedValue = Session["Quantity"].ToString();

				if(Session["Variety"].ToString() != "")
				{
					this.txtVariety.Text = Session["Variety"].ToString();
					this.variety_Key.Value = Session["VarietyIdx"].ToString() + "~";
				}

				ArrayList Customers = (ArrayList)Session["Customers"];

				CustomerKey.Text = Session["CustomerKey"].ToString();
				customerName.Text = Session["CustomerName"].ToString();
				CustomerList.SelectedIndex = Convert.ToInt32(Session["CustomerIndex"].ToString());

				if (CustomerList.SelectedIndex != -1)
				{
					CustomerList_SelectedIndexChanged(null, null);
				}

				ShipToKey.Text = Session["ShipToKey"].ToString();
				ShipToName.Text = Session["ShipToName"].ToString();
				CustomerIDTextBox.Text = Session["CustomerID"].ToString();

				ShipToList.SelectedIndex = Convert.ToInt32(Session["ShipToIndex"].ToString());
			}
			else if(IsPostBack ==false)//Load defaults if not a postback and not loading from this report
			{
				this.yearStartDate.Text=  ((DateTime)BaseDAL.ExecuteScalar("use WeeklySales; select Year_Start_Date from Year_Start_Date")).ToShortDateString();
			}
#endregion
			
			if(Request["ReportName"].ToString() == "WeeklySalesPerYearReport")
			     this.TitleDiv.InnerText = " Weekly Sales Per Year Report";
			else
				this.TitleDiv.InnerText = " Weekly Sales Per Year Summary Report";			

			//KeyCentral.Functions.JavaStatusMessage status = new KeyCentral.Functions.JavaStatusMessage(this);
			//status.ShowMessage("Computing");
            if (IsPostBack && Session["VarietySingle"] != null)
            {
                if (Session["VarietySingle"].ToString() != "")
                {
                    this.txtVariety.Text = Misc.ParseTildaString(Session["VarietySingle"].ToString().Replace("!", "~"), 2);
                    this.variety_Key.Value = Misc.ParseTildaString(Session["VarietySingle"].ToString().Replace("!", "~"), 0) + "~" + Misc.ParseTildaString(Session["VarietySingle"].ToString().Replace("!", "~"), 1);
                }
            }

            if (IsPostBack && Session["SalespersonSingle"] != null)
            {
                if (Session["SalespersonSingle"].ToString() != "")
                {
                    this.txtSalesPerson.Text = Misc.ParseTildaString(Session["SalespersonSingle"].ToString().Replace("!", "~"), 2);
                    this.salesperson_Key.Value = Misc.ParseTildaString(Session["SalespersonSingle"].ToString().Replace("!", "~"), 0) + "~" + Misc.ParseTildaString(Session["SalespersonSingle"].ToString().Replace("!", "~"), 1);
                }
            }

			if(IsPostBack ==false)
			{
				Message.Text ="";

				if (Request.QueryString["Records"]== "None")
					Message.Text = "No Records Found.";

								
			}
		}
		protected void cmdRun_Click(object sender, EventArgs e)
		{
			//Check for vaild fields
			
			string lastPage = Misc.EncodeQueryString("CriteriaYearWeeklySales.aspx?ReportName=" + Request["ReportName"]);

			//Add Report Vars
			reportSessionManager.AddSession("WarehouseIdx",WarehouseId1.Keys); 
			reportSessionManager.AddSession("CommodityIdx",CommodityLookup1.Keys);
			reportSessionManager.AddSession("CommoditySizeIdx",CommoditySizeLookup1.Keys);
			reportSessionManager.AddSession("VarietyIdx",ParseTildaString(Request["variety_Key"],0)); //.ToString().Substring(0,(Request["variety_Key"].ToString().IndexOf("~")
			reportSessionManager.AddSession("Variety",txtVariety.Text);
			reportSessionManager.AddSession("SalesPerson",txtSalesPerson.Text);
			reportSessionManager.AddSession("Price",Price.SelectedValue);
			reportSessionManager.AddSession("Quantity",Quantity.SelectedValue);
			reportSessionManager.AddSession("YearStartDate",yearStartDate.Text);

			ArrayList Customers = new ArrayList();

			if (CustomerKey.Text != "" && CustomerList.SelectedIndex != -1)
			{
				Customers.Add(new CriteriaBoxRecord(CustomerKey.Text, customerName.Text, ""));
			}

			reportSessionManager.AddSession("Customers", Customers);

			reportSessionManager.AddSession("Customer", CustomerIDTextBox.Text);

			reportSessionManager.AddSession("ShipTo", ShipToName.Text);
			reportSessionManager.AddSession("ShipToIdx", CustomerKey.Text);
			reportSessionManager.AddSession("ShipToLocation", ShipToKey.Text);

			reportSessionManager.AddSession("CustomerID", CustomerIDTextBox.Text);
			reportSessionManager.AddSession("CustomerKey", CustomerKey.Text);
			reportSessionManager.AddSession("CustomerName", customerName.Text);
			reportSessionManager.AddSession("ShipToKey", ShipToKey.Text);
			reportSessionManager.AddSession("ShipToName", ShipToName.Text);

			reportSessionManager.AddSession("CustomerIndex", CustomerList.SelectedIndex);
			reportSessionManager.AddSession("ShipToIndex", ShipToList.SelectedIndex);

			//Let the reportSessionManager know that we are done
			reportSessionManager.SaveSessions();

            if (Request["ReportName"].ToString() == "WeeklySalesPerYearReport")
            {
                this.RegisterStartupScript("PopUpReport", "<script>window.open('ReportYearWeekly.aspx?LastPage=CriteriaYearWeeklySales.aspx~ReportName=WeeklySalesPerYearReport','YearWeeklySalesDataReportWindow');</script>");
            }
            else
            {
                this.RegisterStartupScript("PopUpReport", "<script>window.open('../YearWeeklySummary/ReportYearWeeklySummary.aspx?LastPage=CriteriaYearWeeklySales.aspx~ReportName=WeeklySalesPerYearSummaryReport','YearWeeklySalesSummaryDataReportWindow');</script>"); 
            }   
			
			//Response.Redirect("ReportYearWeekly.aspx?LastPage=" + lastPage);
		}
		protected void cmdReport_Click(object sender, EventArgs e)
		{

            JavaScriptRedirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Reports/Sales Reports");
		}
		protected void CustomerIDTextBox_OnTextChanged(object sender, System.EventArgs e)
		{
			string value = "-1";

			CustomerList.ClearSelection();
			CustomerList.SelectedIndex = -1;
			CustomerKey.Text = "";
			customerName.Text = "";

			for (int i = 0; i < CustomerList.Items.Count; i++)
			{
				if (CustomerList.Items[i].Text.Substring(0, CustomerList.Items[i].Text.IndexOf(" - ")) == CustomerIDTextBox.Text)
				{
					value = CustomerList.Items[i].Value;
					i = CustomerList.Items.Count;
				}
			}

			if (value != "-1")
			{
				CustomerList.Items.FindByValue(value).Selected = true;
				CustomerList_SelectedIndexChanged(null, null);

				Message.Text = "";
			}
			else
			{
				Message.Text = "Customer ID not found.";

				ShipToList.ClearSelection();
				ShipToList.Items.Clear();
				ShipToList.SelectedIndex = -1;
				ShipToKey.Text = "";
				ShipToName.Text = "";

				CustomerIDTextBox.Focus();
			}
		}
		protected void ShipToList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ShipToKey.Text = "";
			ShipToName.Text = "";
			ShipToKey.Text = ShipToList.SelectedItem.Value;
			ShipToName.Text = ShipToList.SelectedItem.Text;
			Message.Text = "";
		}
		protected void CustomerList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			CustomerKey.Text = "";
			customerName.Text = "";
			CustomerKey.Text = CustomerList.SelectedItem.Value;
			customerName.Text = CustomerList.SelectedItem.Text;
			Message.Text = "";
			CustomerIDTextBox.Text = CustomerList.SelectedItem.Text.Substring(0, CustomerList.SelectedItem.Text.IndexOf(" - "));
			FillShipToList(CustomerList.SelectedItem.Value);
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		protected override void OnLoadComplete(EventArgs e)
		{
			base.OnLoadComplete(e);

			bool differentCommodity = true;

			if (CommodityLookup1.Keys.Count == 1)
			{
				CriteriaBoxRecord criteriaBoxRecord = (CriteriaBoxRecord)CommodityLookup1.Keys[0];

				if (commodity_Key.Value == criteriaBoxRecord.key || !IsPostBack)
				{
					differentCommodity = false;
				}

				txtCommodity.Value = criteriaBoxRecord.text;
				commodity_Key.Value = criteriaBoxRecord.key;
			}
			else
			{
				txtCommodity.Value = "";
				commodity_Key.Value = "";
			}

            //if (differentCommodity)
            //{
            //    txtVariety.Text = "";
            //    variety_Key.Value = "";
            //}
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Private Helpers
		private void FillShipToList(string customerIndex)
		{
			ShipToList.ClearSelection();
			ShipToList.Items.Clear();
			ShipToList.SelectedIndex = -1;
			ShipToKey.Text = "";
			ShipToName.Text = "";

			OracleConnection OracleConn = GetOracleConn();
			string oracleSQL = FormatOracle(@"
Select
	Fc_Name.NameIdx,
	Fc_Name_Location.NameLocationSeq,
	Fc_Name.LastCoName,
	Fc_Name_Location.Descr
From
	#COMPANY_NUMBER#.Ar_Cust,
	#COMPANY_NUMBER#.Fc_Name,
	#COMPANY_NUMBER#.Fc_Name_Location
Where
	Ar_Cust.CustNameIdx		= Fc_Name.NameIdx and
	Fc_Name.NameIdx			= Fc_Name_Location.NameIdx and
	Fc_Name.NameIdx = '" + customerIndex + @"'

Order by Fc_Name.LastCoName, Fc_Name_Location.Descr");
			OracleCommand OracleCmd = new OracleCommand(oracleSQL, OracleConn);
			OracleCmd.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader2 = OracleCmd.ExecuteReader();

			while (myReader2.Read())
			{
				ShipToList.Items.Add(new ListItem(/*myReader2.GetValue(2).ToString() + " - " + */myReader2.GetValue(3).ToString(), myReader2.GetValue(1).ToString()));
			}

			myReader2.Close();
			myReader2.Dispose();
			OracleCmd.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();
		}
		private void FillCustomerList()
		{
			CustomerList.ClearSelection();
			CustomerList.Items.Clear();
			CustomerList.SelectedIndex = -1;
			CustomerKey.Text = "";
			customerName.Text = "";

			OracleConnection OracleConn = GetOracleConn();
			string oracleSQL = FormatOracle(@"
Select
	Fc_Name.Id ||' - '|| Fc_Name.LastCoName as ID,
	Fc_Name.NameIdx

From
	Ar_Cust,
	Fc_Name

Where
	Ar_Cust.CustNameIdx = Fc_Name.NameIdx

Order By Fc_Name.LastCoName");
			OracleCommand OracleCmd = new OracleCommand(oracleSQL, OracleConn);
			OracleCmd.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader2 = OracleCmd.ExecuteReader();

			while (myReader2.Read())
			{
				CustomerList.Items.Add(new ListItem(myReader2.GetValue(0).ToString(), myReader2.GetValue(1).ToString()));
			}

			myReader2.Close();
			myReader2.Dispose();
			OracleCmd.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();
		}
		public static string ParseTildaString(string stringToParse,int field)
		{
			string[] stringSplit = stringToParse.Split('~');
			
			if(stringSplit.Length <= field)
				return "";
			else
				return stringSplit[field];
		}
		#endregion
	}
}
