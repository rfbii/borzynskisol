using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Text;
using KeyCentral.Functions;
using WeeklySales.App_Code;
using KeyCentral.Lookups;

namespace WeeklySales.Reports.TotalWeekly
{
	public partial class ReportTotalWeekly :  KeyCentral.Functions.BasePage
	{
		#region Declares
		protected string QuantityStyle ="";
		protected string priceHeader = "";
		protected System.Web.UI.WebControls.Repeater Repeater2;
		protected ReportTotalsHelper totalHelper;
		protected DateTime minDate;
		protected DateTime maxDate;
		WeeklySalesPerShipToBLL weeklySalesPerShipToBLL;
		#endregion
		
		#region Param Properties
		private string MinShipDate{get{return Session["MinShipDate"].ToString();}}
		private string MaxShipDate{get{return Session["MaxShipDate"].ToString();}}
		private string Price{get{return Session["Price"].ToString();}}
		private string Quantity{get{return Session["Quantity"].ToString();}}
		private string Customer { get { return Session["Customer"].ToString(); } }
		private string CustomerName { get { return Session["CustomerName"].ToString(); } }
		private string ShipTo{get{return Session["ShipTo"].ToString();}}
		private string ShipToIdx{get{return Session["ShipToIdx"].ToString().Trim();}}
		private string ShipToLocation{get{return Session["ShipToLocation"].ToString().Trim();}}
		private ArrayList CommodityIdx{get{return (ArrayList)Session["CommodityIdx"];}}
		private string Variety{get{return Session["Variety"].ToString();}}
		private string VarietyIdx{get{return Session["VarietyIdx"].ToString();}}
		private string SalesPerson{get{return Session["SalesPerson"].ToString();}}
		private ArrayList WarehouseIdx{get{return (ArrayList)Session["WarehouseIdx"];}}
		#endregion
 
		#region Totals
/*  		#region Vars
		private bool WeekChanged = false;
		private string LastWeek="";
		private double fTotalQnt=0.0F;
		private double fTotalSales=0.0F;
		private DateTime lastWeekDate;
		private DateTime FinalDate;
		private ArrayList Qnts = new ArrayList();
		private ArrayList Product = new ArrayList();
		private ArrayList Amount = new ArrayList();
		private ArrayList shipTo = new ArrayList();
		private ArrayList SubQnts = new ArrayList();
		private ArrayList SubProduct = new ArrayList();
		private ArrayList SubAmount = new ArrayList();
		private string SubProd="";
		private double SubQnt=0.0F;
		private double SubAmt=0.0F;
		
		#endregion

		public string CheckForChanges(object oItem)
		{
			string strRet = "";
			if(LastWeek != GetField(oItem,"Week"))
			{
				if (LastWeek != "")
				{
					WeekChanged = true;
				}
				else
				{
					//this only happend for the first record
					LastWeek = GetField(oItem,"Week");
					lastWeekDate = DateTime.Parse(GetField(oItem,"ShipDateTime"));	;
					
					//Render Weeks with non before the first record
					strRet += RenderMissingWeeks(DateTime.Parse(MinShipDate).AddDays(-7),lastWeekDate);
				}
			}

			return strRet;
		}  
		public string NewRecord(object oItem)
		{
			//If the week changed start a new week now.
			if (WeekChanged)
				SetNewWeek(oItem);
			
			//Add the data on this record to the curent running week.
			AddToWeek(oItem);

			return "";
		}


		public string Detail(object oItem)
		{
			string partWeek = "";
			string strRet = "";
			string lastShip = "";
			double subQuantity = 0;
			double subAmounts = 0;
			double shipQuantity = 0;
			double shipAmounts = 0;

			if(IsPartWeek(lastWeekDate))
				partWeek = "Partial Week";


			if (WeekChanged)
			{

				//Week header
				strRet += @"
					</table><table cellSpacing='0' cellPadding='0' border='0' width='720'><tr>
						<td width='95' class='OrderHeader1' align=center>" + QuantityStyle + @"</td>
						<td width='56' class='OrderHeader2'>&nbsp;Week&nbsp;" + LastWeek + @"</td>
						<td width='230' class='OrderHeader2' align=left>&nbsp;&nbsp;" + GetDateRange(lastWeekDate) + @"</td>
						<td width='225' class='OrderHeader2a' align=left>&nbsp;" + partWeek + @"</td>
						<td width='105' class='OrderHeader3' align=left>&nbsp;" + priceHeader + @"</td>
					</tr></table><table cellSpacing='0' cellPadding='3' border='0' width='720'>";
				
				//Week Detail
				for(int x=0;x<Product.Count;x++)
				{ 
					if (x==0)
						lastShip = (shipTo[x]).ToString().Trim();
					if (lastShip.Trim() != shipTo[x].ToString().Trim())
					{
						strRet = strRet + @"
						<tr>
							<td width='90' class='OrderHeader8' align=right>" + FormatQnt((double)shipQuantity )+ @"</td>
							<td width='511' class='OrderHeader8' align=left colspan='3'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + KeyCentral.Functions.StringFormater.FormatString("Sub Total for " + lastShip,69) + @"</td>
							<td width='100' class='OrderHeader8' align=right>" + ((double)shipAmounts).ToString("C") + @"</td>
						</tr>";
						strRet = strRet + @"
						<tr>
							<td width='90' class='DetailLine1' align=right>" + FormatQnt((double)Qnts[x])+ @"</td>
							<td width='511' class='DetailLine2' align=left colspan='3'>" + KeyCentral.Functions.StringFormater.FormatString(Product[x],66) + @"</td>
							<td width='100' class='DetailLine3' align=right>" + ((double)Amount[x]).ToString("C") + @"</td>
						</tr></table><table cellSpacing='0' cellPadding='3' border='0' width='720'>";
						shipQuantity = (double)Qnts[x];
						shipAmounts = (double)Amount[x];
						lastShip = shipTo[x].ToString().Trim();
					
					}
					else
					{
						strRet = strRet + @"
						<tr>
							<td width='90' class='DetailLine1' align=right>" + FormatQnt((double)Qnts[x])+ @"</td>
							<td width='511' class='DetailLine2' align=left colspan='3'>" + KeyCentral.Functions.StringFormater.FormatString(Product[x],66) + @"</td>
							<td width='100' class='DetailLine3' align=right>" + ((double)Amount[x]).ToString("C") + @"</td>
						</tr></table><table cellSpacing='0' cellPadding='3' border='0' width='720'>";
						shipQuantity = ((double)shipQuantity + (double)Qnts[x]);
						shipAmounts = ((double)shipAmounts + (double)Amount[x]);
					}
					subQuantity = ((double)subQuantity + (double)Qnts[x]);
					subAmounts = ((double)subAmounts + (double)Amount[x]);
				}
				//Sub Total for last Shipto
				strRet = strRet + @"
						<tr>
							<td width='90' class='OrderHeader8' align=right>" + FormatQnt((double)shipQuantity )+ @"</td>
							<td width='511' class='OrderHeader8' align=left colspan='3'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + KeyCentral.Functions.StringFormater.FormatString("Sub Total for " + lastShip,69) + @"</td>
							<td width='100' class='OrderHeader8' align=right>" + ((double)shipAmounts).ToString("C") + @"</td>
						</tr>";
				// SubTotal
				strRet = strRet + @"
						<tr>
							<td width='90' class='OrderHeader7' align=right>" + FormatQnt((double)subQuantity )+ @"</td>
							<td width='511' class='OrderHeader7' align=center colspan='3'>" + "Sub Total for Week " + LastWeek + @"</td>
							<td width='100' class='OrderHeader7' align=right>" + ((double)subAmounts).ToString("C") + @"</td>
						</tr>";
				//None Weeks

				strRet += RenderMissingWeeks(lastWeekDate,DateTime.Parse(GetField(oItem,"ShipDateTime")));
			}

			return strRet;
		}
		private string FormatQnt(float value)
		{
			if (QuantityStyle == "Actual")
				return value.ToString("###,##0");
			else
				return value.ToString("###,##0.00");
		}
		private string FormatQnt(double value)
		{
			if (QuantityStyle == "Actual")
				return value.ToString("###,##0");
			else
				return value.ToString("###,##0.00");
		}
		private string RenderMissingWeeks(DateTime startDate,DateTime endDate)
		{
			string ret="";
			string partWeek = "&nbsp;";
			//StartWeek and startYear are the next week after the last record
			DateTime temp =startDate.AddDays(7);

			string startWeek = KeyCentral.Functions.Misc.DateToWeek(temp);
			int startYear = temp.Year;

			string endWeek = KeyCentral.Functions.Misc.DateToWeek(endDate);

			while(startWeek !=endWeek)
			{
				
				if(IsPartWeek(temp))
					partWeek = "Partial Week";

				ret += @"
					</table><table cellSpacing='0' cellPadding='0' border='0' width='720'><tr>
						<td width='56' class='OrderHeader2'>&nbsp;Week&nbsp;" + startWeek + @"</td>
						<td width='180' class='OrderHeader2'align=left>&nbsp;&nbsp;" + GetDateRange(temp) + @"</td>
						<td width='50' class='OrderHeader2'align=left><b>" + "None" + @"</b></td>
						<td width='335' class='OrderHeader2a'align=left>" + partWeek + @"</td>
					</tr></table><table cellSpacing='0' cellPadding='0' border='0' width='720'>";

				//Move the startWeek and startYear a week foward
				temp = KeyCentral.Functions.Misc.WeekToStartDate(Int32.Parse(startWeek),startYear);
				temp = temp.AddDays(7);
				startWeek = KeyCentral.Functions.Misc.DateToWeek(temp);
				startYear = temp.Year;
				partWeek = "&nbsp;";
			}
			return ret;
		}
		private bool IsPartWeek(DateTime thisRecordDate)
		{
			//Check for this being first week in report and not starting on Monday
			if(KeyCentral.Functions.Misc.DateToWeek(MinShipDate) == KeyCentral.Functions.Misc.DateToWeek(thisRecordDate) && (int)DateTime.Parse(MinShipDate).DayOfWeek !=1)
				return true;
			//Check for this being last week in report and not ending on Sunday
			if(KeyCentral.Functions.Misc.DateToWeek(MaxShipDate) == KeyCentral.Functions.Misc.DateToWeek(thisRecordDate) && (int)DateTime.Parse(MaxShipDate).DayOfWeek !=0)
				return true;

			return false;
		}

		private string GetDateRange(DateTime thisRecordDate)
		{
			string startDate;
			string endDate; 
			string thisWeek = Misc.DateToWeek(thisRecordDate);

			
			if(DateTime.Parse(Misc.GetMondayOfThisWeek(thisRecordDate)) < minDate)
				startDate = minDate.ToShortDateString();
			else
				startDate = Misc.GetMondayOfThisWeek(thisRecordDate);

			if(DateTime.Parse(Misc.GetSundayOfThisWeek(thisRecordDate)) > maxDate)
				endDate = maxDate.ToShortDateString();
			else
				endDate = Misc.GetSundayOfThisWeek(thisRecordDate);

			return startDate + " > " + endDate;
		}
										   
		public string OrderFooter()
		{
			string partWeek = "";
			string strRet = "";
			string lastShip = "";
			double subQuantity = 0;
			double subAmounts = 0;
			double shipQuantity = 0;
			double shipAmounts = 0;

			if(IsPartWeek(lastWeekDate))
				partWeek = "Partial Week";

		{
			//Output the lastweek's header
			strRet = @"
					</table><table cellSpacing='0' cellPadding='0' border='0' width='720'><tr>
						<td width='95' class='OrderHeader1' align=center>" + QuantityStyle + @"</td>
						<td width='56' class='OrderHeader2'>&nbsp;Week&nbsp;" + LastWeek + @"</td>
						<td width='230' class='OrderHeader2' align=left>&nbsp;&nbsp;" + GetDateRange(lastWeekDate) + @"</td>
						<td width='225' class='OrderHeader2a' align=left>&nbsp;" + partWeek + @"</td>
						<td width='105' class='OrderHeader3' align=left>&nbsp;" + priceHeader + @"</td>
					</tr></table><table cellSpacing='0' cellPadding='3' border='0' width='720'>";
				
			//Last Week's detail
			for(int x=0;x<Product.Count;x++)
			{ 
				if (x==0)
					lastShip = (shipTo[x]).ToString().Trim();
				if (lastShip.Trim() != shipTo[x].ToString().Trim())
				{
					strRet = strRet + @"
						<tr>
							<td width='90' class='OrderHeader8' align=right>" + FormatQnt((double)shipQuantity )+ @"</td>
							<td width='511' class='OrderHeader8' align=left colspan='3'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + KeyCentral.Functions.StringFormater.FormatString("Sub Total for " + lastShip,69) + @"</td>
							<td width='100' class='OrderHeader8' align=right>" + ((double)shipAmounts).ToString("C") + @"</td>
						</tr>";
					strRet = strRet + @"
						<tr>
							<td width='90' class='DetailLine1' align=right>" + FormatQnt((double)Qnts[x])+ @"</td>
							<td width='511' class='DetailLine2' align=left colspan='3'>" + KeyCentral.Functions.StringFormater.FormatString(Product[x],66) + @"</td>
							<td width='100' class='DetailLine3' align=right>" + ((double)Amount[x]).ToString("C") + @"</td>
						</tr></table><table cellSpacing='0' cellPadding='3' border='0' width='720'>";
					shipQuantity = (double)Qnts[x];
					shipAmounts = (double)Amount[x];
					lastShip = shipTo[x].ToString().Trim();
					
				}
				else
				{
					strRet = strRet + @"
						<tr>
							<td width='90' class='DetailLine1' align=right>" + FormatQnt((double)Qnts[x])+ @"</td>
							<td width='511' class='DetailLine2' align=left colspan='3'>" + KeyCentral.Functions.StringFormater.FormatString(Product[x],66) + @"</td>
							<td width='100' class='DetailLine3' align=right>" + ((double)Amount[x]).ToString("C") + @"</td>
						</tr></table><table cellSpacing='0' cellPadding='3' border='0' width='720'>";
					shipQuantity = ((double)shipQuantity + (double)Qnts[x]);
					shipAmounts = ((double)shipAmounts + (double)Amount[x]);
				}
				subQuantity = ((double)subQuantity + (double)Qnts[x]);
				subAmounts = ((double)subAmounts + (double)Amount[x]);
			}
				
			// SubTotal
			strRet = strRet + @"
						<tr>
							<td width='90' class='OrderHeader7' align=right>" + FormatQnt((double)subQuantity )+ @"</td>
							<td width='511' class='OrderHeader7' align=center colspan='3'>" + "Sub Total for Week " + LastWeek + @"</td>
							<td width='100' class='OrderHeader7' align=right>" + ((double)subAmounts).ToString("C") + @"</td>
						</tr>";
			//Empty weeks
			strRet += RenderMissingWeeks(lastWeekDate,DateTime.Parse(MaxShipDate).AddDays(7));

			//Sub total header
			strRet = strRet + @"
					</table><table cellSpacing='0' cellPadding='0' border='0' width='720'><tr>
						<td width='95' class='OrderHeader4'align=center>" + QuantityStyle + @"</td>
						<td width='515' class='OrderHeader5'align=center colspan='3'>Totals</td>
						<td width='106' class='OrderHeader6'align=left>&nbsp;" + priceHeader + @"</td>
					</tr></table><table cellSpacing='0' cellPadding='3' border='0' width='720'>";
				
			//Sort Total Lines
			for(int x=0;x<SubProduct.Count;x++)
				for(int y=0;y<SubProduct.Count;y++)
					if ((((string)SubProduct[x]).CompareTo((string)(SubProduct[y]))) < 0)
					{
						SubProd = SubProduct[x].ToString();
						SubQnt = ((double)(SubQnts[x]));
						SubAmt = ((double)(SubAmount[x]));
						SubProduct[x] = SubProduct[y];
						SubQnts[x] =SubQnts[y];
						SubAmount[x] =SubAmount[y];
						SubProduct[y] = SubProd;
						SubQnts[y] = SubQnt;
						SubAmount[y] = SubAmt;	
					}

			//Output Sub Totals
			for(int x=0;x<SubProduct.Count;x++)
				strRet = strRet + @"
					<tr>
						<td width='90' class='DetailLine1' align=right>" + FormatQnt((double)SubQnts[x]) + @"</td>
						<td width='511' class='DetailLine2' align=left colspan='3'>" + KeyCentral.Functions.StringFormater.FormatString(SubProduct[x],66)+ @"</td>
						<td width='100' class='DetailLine3' align=right>" + ((double)SubAmount[x]).ToString("$###,##0.00") + @"</td>
					</tr>";

			//Output Grand Totals
			strRet = strRet + @"
				</table><table cellSpacing='0' cellPadding='3' border='0' width='720'><tr>
					<td width='90' class='OrderHeader4' align=right>" + FormatQnt(fTotalQnt) + @"</td>
					<td width='76' class='OrderHeader5'>" + QuantityStyle + @"</td>
					<td width='180' class='OrderHeader5' align=right>Grand Total</td>
					<td width='250' class='OrderHeader5' align=right>" + priceHeader + @"</td>
					<td width='100' class='OrderHeader6' align=right>" + fTotalSales.ToString("C") + @"</td>
				</tr></table><table cellSpacing='0' cellPadding='0' border='0' width='720'>";
				
		}
			
			return strRet;
		}
		public void AddToWeek(object oItem)
		{
			double OrderPrice;
			double OrderLinePrice;
			double OrderSalesPrice;
			double OrderQuantity;
			double OrderLineAmount;
			bool found = false;
			bool subFound = false;

			//Get Order Price and Sales Price from FOB or Invoice based on if it's a invoice
			
			OrderPrice = double.Parse(GetField(oItem,"FOBPrice").ToString());
			OrderSalesPrice = double.Parse(GetField(oItem,"SalesPrice").ToString());
			
			if (priceHeader == "Gross Sales")
				OrderLinePrice = OrderSalesPrice;
			else
				OrderLinePrice = OrderPrice;

			if (QuantityStyle == "Actual")
				OrderQuantity = double.Parse(GetField(oItem,"Qnt"));
			else
				OrderQuantity = double.Parse(GetField(oItem,"EquivQnt"));

			OrderLineAmount = OrderLinePrice *  double.Parse(GetField(oItem,"Qnt"));
				
			//Update values in Product if found
			for(int x=0;x<Product.Count;x++)
				if (GetField(oItem,"INVCDESCR").ToString().Trim()== Product[x].ToString().Trim() && (GetField(oItem,"ShipToName").ToString().Trim() +" "+ GetField(oItem,"ShipToCity").ToString().Trim()+" "+ GetField(oItem,"ShipToState").ToString().Trim()+" "+ GetField(oItem,"ShipToZip").ToString().Trim())== shipTo[x].ToString().Trim())
				{
					Qnts[x]= ((double)Qnts[x] + OrderQuantity);	
					Amount[x] = ((double)Amount[x] + (OrderLineAmount));	
					found = true;
				}
			//If not found add it
			if (found != true)
			{
				Qnts.Add(OrderQuantity);	
				Amount.Add(OrderLineAmount) ;	
				Product.Add(GetField(oItem,"INVCDESCR").ToString()) ;
				shipTo.Add((GetField(oItem,"ShipToName").ToString().Trim() +" "+ GetField(oItem,"ShipToCity").ToString().Trim()+" "+ GetField(oItem,"ShipToState").ToString().Trim()+" "+ GetField(oItem,"ShipToZip").ToString().Trim())) ;
			}

			//Update values in SubProduct if found
			for(int x=0;x<SubProduct.Count;x++)
				if (GetField(oItem,"INVCDESCR").ToString().Trim()== SubProduct[x].ToString().Trim())
				{
					SubQnts[x] = ((double)SubQnts[x] + OrderQuantity);	
					SubAmount[x] = ((double)SubAmount[x] + (OrderLineAmount));	
					subFound = true;
				}
			//If not found add it
			if (subFound != true)
			{
				SubQnts.Add(OrderQuantity);	
				SubAmount.Add(OrderLineAmount);	
				SubProduct.Add(GetField(oItem,"INVCDESCR").ToString());	
			}
			
			//Sum report Totals
			fTotalQnt += OrderQuantity;
			fTotalSales += OrderLineAmount;
			DateTime date = DateTime.Parse(GetField(oItem,"ShipDateTime"));	
			FinalDate = date;
			
		}
		public void SetNewWeek(object oItem)
		{ 
			WeekChanged = false;
			Qnts.Clear();
			Amount.Clear();
			Product.Clear();
			shipTo.Clear();

			LastWeek = GetField(oItem,"Week");
			lastWeekDate = DateTime.Parse(GetField(oItem,"ShipDateTime"));;

		}  


		private string GetField(object oItem,string strField)
		{
			return DataBinder.Eval(oItem,"DataItem." + strField).ToString();
		}
		private string GetDateField(object oItem,string strField)
		{
			string str = DataBinder.Eval(oItem,"DataItem." + strField).ToString();
			if(str.IndexOf(" ",0)>0)
				return str.Substring(0,str.IndexOf(" ",0));
			else
				return str;
		}*/
		#endregion

		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Weekly Sales Report") == false)
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Sales Reports");
			
			KeyCentral.Functions.JavaStatusMessage status = new KeyCentral.Functions.JavaStatusMessage(this);
			status.ShowMessage("Computing");

			minDate = DateTime.Parse(MinShipDate);
			maxDate = DateTime.Parse(MaxShipDate);

			SetCommentLine();
			ShowReport(true);
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}
		
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Display code
		private int lastWeek=-1;
		private DateTime lastDate;

		public string DisplayReportItem(WeeklySalesPerShipToBLL.ReportItem data)
		{
			string display="";

			if(lastWeek==-1)
				display += RenderMissingWeeks(DateTime.Parse(this.MinShipDate).AddDays(-7),data.EndDate);
			else
				display += RenderMissingWeeks(lastDate,data.StartDate);	

			//Only display the headers when the week changes
			if(lastWeek != data.Week)
				display += DisplayWeekHeader(data);

			//Display the details
			int loopLimit = data.Details.Count;
			for(int x=0;x<loopLimit;x++)
				display += DisplayShipTo((WeeklySalesPerShipToBLL.ItemShipTo)data.Details[x]);

			//Only display the headers when the week changes
			if(lastWeek != data.Week)
				display += DisplayWeekFooter(data);

			//save 'last'
			lastWeek = data.Week;
			lastDate = data.StartDate;

			return display;
		}
		public string DisplayFooter()
		{
			string display = String.Empty;

			//Display any missing weeks at the end of our search
			display += RenderMissingWeeks(lastDate,DateTime.Parse(this.MaxShipDate).AddDays(7));

			//Sub total header
			display += @"
					<table cellSpacing='0' cellPadding='0' border='0' width='720'><tr>
						<td width='95' class='OrderHeader4' align=center>" + QuantityStyle + @"</td>
						<td width='509' class='OrderHeader5' align=center colspan='3'>Totals</td>
						<td width='105' class='OrderHeader6' align=left>&nbsp;" + priceHeader + @"</td>
					</tr></table><table cellSpacing='0' cellPadding='3' border='0' width='720'>";
				
			//Output Sub Totals
			int loopLimit = weeklySalesPerShipToBLL.TotalDetails.Count;
			for(int x=0;x<loopLimit;x++)
				display += @"
					<tr>
						<td width='90' class='DetailLine1' align=right>" + FormatQnt(((WeeklySalesPerShipToBLL.ItemDetail)weeklySalesPerShipToBLL.TotalDetails[x]).Qnt) + @"</td>
						<td width='511' class='DetailLine2' align=left colspan='3'>" + KeyCentral.Functions.StringFormater.FormatString(((WeeklySalesPerShipToBLL.ItemDetail)weeklySalesPerShipToBLL.TotalDetails[x]).Desc,66)+ @"</td>
						<td width='100' class='DetailLine3' align=right>" + (((WeeklySalesPerShipToBLL.ItemDetail)weeklySalesPerShipToBLL.TotalDetails[x]).Amount).ToString("$###,##0.00") + @"</td>
					</tr>";

			//Output Grand Totals
			display += @"
				</table><table cellSpacing='0' cellPadding='3' border='0' width='720'><tr>
					<td width='90' class='OrderHeader4' align=right>" + FormatQnt(weeklySalesPerShipToBLL.TotalQnt) + @"</td>
					<td width='76' class='OrderHeader5'>" + QuantityStyle + @"</td>
					<td width='180' class='OrderHeader5' align=right>Grand Total</td>
					<td width='250' class='OrderHeader5' align=right>" + priceHeader + @"</td>
					<td width='100' class='OrderHeader6' align=right>" + weeklySalesPerShipToBLL.TotalAmount.ToString("C") + @"</td>
				</tr></table><table cellSpacing='0' cellPadding='0' border='0' width='720'>";
				

			return display;
		}


		private string DisplayWeekHeader(WeeklySalesPerShipToBLL.ReportItem data)
		{
			string display="";

			
			string partWeek = "&nbsp;";

			if(IsPartWeek(data.StartDate))
				partWeek = "Partial Week";

			display += @"
					<table cellSpacing='0' cellPadding='0' border='0' width='720'><tr>
						<td width='94' class='OrderHeader1' align=center>" + QuantityStyle + @"</td>
						<td width='56' class='OrderHeader2'>&nbsp;Week&nbsp;" + data.Week + @"&nbsp;</td>
						<td width='230' class='OrderHeader2' align=left>" + GetDateRange(data.StartDate,data.EndDate) + @"</td>
						<td width='225' class='OrderHeader2a' align=left>&nbsp;" + partWeek + @"</td>
						<td width='104' class='OrderHeader3' align=left>&nbsp;" + priceHeader + @"</td>
					</tr></table><table cellSpacing='0' cellPadding='1' border='0' width='720'>";
				//TODO: The CellPadding above this line is causing the records to be taller than normal.

			return display;
		}

		private string DisplayShipTo(WeeklySalesPerShipToBLL.ItemShipTo data)
		{
			string display = String.Empty;

			int loopLimit = data.Details.Count;
			for(int x=0;x<loopLimit;x++)
				display += DisplayDetail((WeeklySalesPerShipToBLL.ItemDetail)data.Details[x]);

			display += @"
						<tr>
							<td width='90' class='OrderHeader8' align=right>" + FormatQnt(data.Qnt )+ @"</td>
							<td width='511' class='OrderHeader8' align=left colspan='3'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + KeyCentral.Functions.StringFormater.FormatString("Sub Total for " + data.ShipToDesc,60) + @"</td>
							<td width='100' class='OrderHeader8' align=right>" + (data.Amount).ToString("C") + @"</td>
						</tr>";

			return display;
		}
		private string DisplayDetail(WeeklySalesPerShipToBLL.ItemDetail data)
		{
			string display="";

			display +=@"
					<tr>
						<td width='90' class='DetailLine1' align=right>" + FormatQnt(data.Qnt)+ @"</td>
						<td width='511' class='DetailLine2' align=left colspan='3'>" + KeyCentral.Functions.StringFormater.FormatString(data.Desc,66) + @"</td>
						<td width='100' class='DetailLine3' align=right>" + ((double)data.Amount).ToString("C") + @"</td>
					</tr>";

			return display;
		}
		private string DisplayWeekFooter(WeeklySalesPerShipToBLL.ReportItem data)
		{
			string display="";

			display += @"</table><table cellSpacing='0' cellPadding='0' border='0' width='720'>
						<tr>
							<td width='90' class='OrderHeader7' align=right>" + FormatQnt(data.Qnt)+ @"</td>
							<td width='511' class='OrderHeader7' align=Center colspan='3'>" + "Sub Total for Week " + data.Week.ToString() + @"</td>
							<td width='100' class='OrderHeader7' align=right>" + (data.Amount).ToString("C") + @"</td>
						</tr></table>";

			return display;
		}


		private string GetDateRange(DateTime date1,DateTime date2)
		{
			string startDate;
			string endDate; 

			if(date1 < minDate)
				startDate = minDate.ToShortDateString();
			else
				startDate = date1.ToShortDateString();

			if(date2 > maxDate)
				endDate = maxDate.ToShortDateString();
			else
				endDate = date2.ToShortDateString();

			return startDate + " > " + endDate;
		}
		
		private string FormatQnt(float value)
		{
			if (QuantityStyle == "Actual")
				return value.ToString("###,##0");
			else
				return value.ToString("###,##0.00");
		}
		private string FormatQnt(double value)
		{
			if (QuantityStyle == "Actual")
				return value.ToString("###,##0");
			else
				return value.ToString("###,##0.00");
		}
		
		private bool IsPartWeek(DateTime thisRecordDate)
		{
			//Check for this being first week in report and not starting on Monday
			if(KeyCentral.Functions.Misc.DateToWeek(MinShipDate) == KeyCentral.Functions.Misc.DateToWeek(thisRecordDate) && (int)DateTime.Parse(MinShipDate).DayOfWeek !=1)
				return true;
			//Check for this being last week in report and not ending on Sunday
			if(KeyCentral.Functions.Misc.DateToWeek(MaxShipDate) == KeyCentral.Functions.Misc.DateToWeek(thisRecordDate) && (int)DateTime.Parse(MaxShipDate).DayOfWeek !=0)
				return true;

			return false;
		}
		private string RenderMissingWeeks(DateTime startDate,DateTime endDate)
		{
			string ret="";
			string partWeek = "&nbsp;";
			//StartWeek and startYear are the next week after the last record
			DateTime temp =startDate.AddDays(7);

			string startWeek = KeyCentral.Functions.Misc.DateToWeek(temp);
			int startYear = temp.Year;

			string endWeek = KeyCentral.Functions.Misc.DateToWeek(endDate);

			while(startWeek !=endWeek)
			{
				
				if(IsPartWeek(temp))
					partWeek = "Partial Week";
				//********************************************** NEED TO FIX GetDateRange part of this

				ret += @"
					</table><table cellSpacing='0' cellPadding='0' border='0' width='720'><tr>
						<td class='OrderHeader1' width='95'>&nbsp;</td>
						<td class='OrderHeader2' width='2'>&nbsp;</td>
						<td width='56' class='OrderHeader2'>&nbsp;Week&nbsp;" + startWeek + @"</td>
						<td width='180' class='OrderHeader2' align=left>" + GetDateRange(temp,DateTime.Parse(Misc.GetSundayOfThisWeek(temp))) + @"</td>
						<td width='50' class='OrderHeader2' align=left><b>" + "None" + @"</b></td>
						<td width='335' class='OrderHeader2a' align=left>" + partWeek + @"</td>
					</tr></table><table cellSpacing='0' cellPadding='0' border='0' width='720'>";

				//Move the startWeek and startYear a week foward
				temp = KeyCentral.Functions.Misc.WeekToStartDate(Int32.Parse(startWeek),startYear);
				temp = temp.AddDays(7);
				startWeek = KeyCentral.Functions.Misc.DateToWeek(temp);
				startYear = temp.Year;
				partWeek = "&nbsp;";
			}
			return ret;
		}
		#endregion

		#region Private Helpers
		private void SetCommentLine()
		{
			priceHeader = Price.Trim();
			QuantityStyle = Quantity.Trim();
			StringBuilder comment = new StringBuilder();

			if(MinShipDate!= "")
			{ 
				comment.Append(MinShipDate);
				comment.Append(" TO ");
				comment.Append(MaxShipDate);
				comment.Append(" ");
			}
			if(Customer != "")
			{ 
				comment.Append(" Customer: ");
				comment.Append(CustomerName);
				comment.Append(" ");
			}
			if(ShipTo != "")
			{ 
				comment.Append(" ShipTo: ");
				comment.Append(ShipTo);
				comment.Append(" ");
			}
			if(Variety != "")
			{ 
				comment.Append(" Variety: ");
				comment.Append(Variety);
				comment.Append(" ");
			}
			if(SalesPerson != "")
			{ 
				comment.Append(" Salesperson: ");
				comment.Append(SalesPerson);
				comment.Append(" ");
			}
			if (WarehouseIdx.Count != 0)
			{ 
				comment.Append("Warehouse: ");
				foreach(CriteriaBoxRecord record in WarehouseIdx)
				{
					comment.Append(record.text);
					comment.Append(", ");
				}
				comment.Remove(comment.Length-2,2);
				comment.Append(" ");
			}
			if (CommodityIdx.Count!= 0)
			{ 
				comment.Append("Commodity: ");
				foreach(CriteriaBoxRecord record in CommodityIdx)
				{
					comment.Append(record.description);
					comment.Append(", ");
				}
				comment.Remove(comment.Length-2,2);
				comment.Append(" ");
			}
			
			CommentLine.InnerText = comment.ToString();
		}

		private void ShowReport(bool bDataBind)
		{
			weeklySalesPerShipToBLL = new WeeklySalesPerShipToBLL(this);

			bool grossPrice = false;
			bool actualQuantity = false;

			if(Price == "Gross Sales")
				grossPrice=true;
			if(Quantity == "Actual")
				actualQuantity = true;

			weeklySalesPerShipToBLL.GetWeekSalesData(grossPrice,actualQuantity,Customer,ShipToIdx,ShipToLocation,CommodityIdx,VarietyIdx,SalesPerson,WarehouseIdx,new ArrayList(),MinShipDate,MaxShipDate);

			
			if (weeklySalesPerShipToBLL.ReportItems.Count >0)
			{
				this.Repeater1.DataSource = weeklySalesPerShipToBLL.ReportItems;
				if(bDataBind)
					Repeater1.DataBind();
			}
			else
			{
				JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"]) + "&&Records=None");
			}
/*
			DataSet dsRet = new DataSet();
			WeeklySalesData = new WeeklySalesData(this);
			WeeklySalesData.SortShipTo = true;
			dsRet = WeeklySalesData.GetWeekSalesData(MinShipDate,MaxShipDate,Customer,ShipToIdx,ShipToLocation,Commodity,VarietyIdx,SalesPerson,WarehouseIdx);
			//dsRet.WriteXml(@"C:\temp\ReportTotalWeekly.xls");
			
			
			if (dsRet.Tables[0].DefaultView.Count != 0)
			{
				this.Repeater1.DataSource = dsRet.Tables[0].DefaultView;
				if(bDataBind)
					Repeater1.DataBind();
			}
			else
			{
				JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"]) + "&&Records=None");
			}	
			*/
		}
		#endregion
	}
}
