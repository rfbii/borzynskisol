set nocount on
use WeeklySales

declare @Expected_Version int
declare @New_Version int
DECLARE @TodaysDate DATETIME

set @Expected_Version = 3
set @New_Version = 4
SET @TodaysDate = GETDATE()

if((select Version from WeeklySales..version)!=@Expected_Version)
begin
	Print 'This function is only for WeeklySales version ' + cast(@Expected_Version as varchar(3))
	return
end
Print 'Backing up DB.'
BACKUP DATABASE WeeklySales to disk ='C:\WeeklySales.bak'

-- **************************************************************
-- Start Version Changes
-- **************************************************************

Print 'Dropping table'
drop table Valid_Warehouse

-- **************************************************************
-- End Version Changes
-- **************************************************************


Print 'Updating Weekly Sales DataBase to Version: ' + cast(@New_Version as varchar(3))


--Set that the version has been updated
update WeeklySales..Version set Version = @New_Version, Updated = @TodaysDate

Print 'Updated WeeklySales DataBase to Version: ' + cast(@New_Version as varchar(3))