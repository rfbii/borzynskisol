set nocount on
use WeeklySales

declare @Expected_Version int
declare @New_Version int

set @Expected_Version = 2
set @New_Version = 3

if((select Version from WeeklySales..version)!=@Expected_Version)
begin
	Print 'This function is only for WeeklySales version ' + cast(@Expected_Version as varchar(3))
	return
end
Print 'Backing up DB.'
BACKUP DATABASE SRP to disk ='C:\WeeklySales.bak'

-- **************************************************************
-- Start Version Changes
-- **************************************************************

Print 'Add columns'
ALTER TABLE Valid_Warehouse ADD Commodity_Key int NULL

alter table Valid_Warehouse ALTER COLUMN Commodity_Key int Not NULL
alter table Valid_Warehouse ALTER COLUMN Warehouse_Key int Not NULL
go
--after a go we must make sure 
if((select Version from WeeklySales..version)!=2)
begin
	Print 'This function is only for WeeklySales version 2'
	return
end

Print 'Add Primary Key'
alter table Valid_Warehouse add CONSTRAINT pk_Valid_Warehouse PRIMARY KEY (Commodity_Key, Warehouse_Key)

-- **************************************************************
-- End Version Changes
-- **************************************************************


Print 'Updating Weekly Sales DataBase to Version: 3' 


--Set that the version has been updated
update WeeklySales..Version set Version = 3, Updated = GETDATE()

Print 'Updated WeeklySales DataBase to Version: 3' 