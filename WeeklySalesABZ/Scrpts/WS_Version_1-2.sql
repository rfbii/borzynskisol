set nocount on
use WeeklySales

declare @Expected_Version int
declare @New_Version int
DECLARE @TodaysDate DATETIME

set @Expected_Version = 1
set @New_Version = 2
SET @TodaysDate = GETDATE()

if((select Version from WeeklySales..version)!=@Expected_Version)
begin
	Print 'This function is only for WeeklySales version ' + cast(@Expected_Version as varchar(3))
	return
end
Print 'Backing up DB.'
BACKUP DATABASE SRP to disk ='C:\WeeklySales.bak'

-- **************************************************************
-- Start Version Changes
-- **************************************************************

--Create new tables
Print 'Creating the new tables.'
create table Valid_Warehouse
(
	Warehouse_Key int
)

-- **************************************************************
-- End Version Changes
-- **************************************************************


Print 'Updating Weekly Sales DataBase to Version: ' + cast(@New_Version as varchar(3))


--Set that the version has been updated
update WeeklySales..Version set Version = @New_Version, Updated = @TodaysDate

Print 'Updated WeeklySales DataBase to Version: ' + cast(@New_Version as varchar(3))