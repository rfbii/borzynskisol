using System;
using Oracle.DataAccess.Client;
using System.Web.SessionState;
using KeyCentral.Functions;


namespace CustomerStatus.Reports.CustomerStatus
{
	public class CustomerStatusReport
	{
		public CustomerStatusReport(){}

		public static string GetCustomerStatusReport(HttpSessionState Session)
		{			
			return Misc.FormatOracle(Session,strCustomerStatus);
		}

		#region strCustomerStatus
		private static string strCustomerStatus = @"
SELECT /*+ CHOOSE */
	Ar_Trx_Header.ArTrxHdrIdx,
	Ar_Trx_Header.SONO,
	Ar_Trx_Line.ArTrxHdrIdx as ArLineIdx,
	Ar_Trx_Header.SoDateTime,
	Ar_Trx_Header.ShipDateTime,
	Ar_Trx_Header.InvoiceDate,
	Sales_Name.LastCoName as SalesMan,
	Cust_Name.LastCoName As SoldToName,
	Cust_Loc.City As SoldToCity,
	Cust_Loc.State As SoldToState,
	Cust_Loc.Zip As SoldToZip,
	ShipTo_Name.LastCoName As ShipToName,
	ShipTo_Loc.City As ShipToCity,
	ShipTo_Loc.State As ShipToState,
	ShipTo_Loc.Zip As ShipToZip,
	Fc_Sale_Terms.DESCR,
	Ar_Pay_Terms.DESCR as PayTerms,
	Ar_Trx_Header.CUSTPOREF,
	Ar_Trx_Header.CARRIER,
	Ar_Trx_Header.TRAILERLICENSE,
	Ar_So_Line.UnitsPerPallet,
	Ar_So_Line.INVCDESCR,   
	Fc_Unit_Of_Measure.Uom,   
	Ar_So_Line.OrderQnt as IcQnt,
	Ar_So_Line.PRICE as FOBPrice,   
	Ar_So_Line.SALEPRICE as SalesPrice, 
	Ar_So_Line.ConsignFlag as ConsignFlag,
	Warehouse_Loc.Descr as Warehouse,  
	Ar_Trx_Header.SOAMT as SoldAmt,   
	Ar_Trx_Header.ARAMT as PriceAMT,   
	Ar_Trx_Header.SOSTATUS,   
	Ar_Trx_Header.RECEIPTAMT, 
	Ar_Trx_Header.HOLDFLAG, 
	Ar_Trx_Header.TOETADATETIME,   
	 Ar_Trx_Line.Qnt as Qnt,
	trunc(Decode(Ar_Trx_Line.UnitsPerPallet,0,0,(Ar_Trx_Line.Qnt/Ar_Trx_Line.UnitsPerPallet)),16) as InvoicePallets,
	trunc(Decode(Ar_So_Line.UnitsPerPallet,0,0,(Ar_So_Line.OrderQnt/Ar_So_Line.UnitsPerPallet)),16) as OrderPallets,
	Ar_Trx_Line.PRICE as InvoicePrice,   
	Ar_Trx_Line.SALEPRICE as InvoiceSalePrice,
	Ic_Product_Produce.CmtyIdx
	 				
   FROM  
	Ar_Trx_Header, 
	Ar_So_Line,
	Fc_Unit_Of_Measure,
	Ic_Warehouse, 
	FC_NAME_LOCATION Warehouse_Loc,
	FC_NAME Cust_Name,
	FC_NAME_LOCATION Cust_Loc,
	FC_NAME ShipTo_Name,
	FC_NAME_LOCATION ShipTo_Loc,
	Fc_Sale_Terms,
	Ar_Pay_Terms,
	Fc_Name Sales_Name,
	Ar_Trx_Line,
	Ic_Product_Produce
				
   WHERE  
	AR_TRX_HEADER.SOTYPE IN ('2','5') and
	Ar_So_Line.ArTrxHdrIdx            	= Ar_Trx_Header.ArTrxHdrIdx And  
	Ar_So_Line.ArSoLineTrxType		= '1' and  
	Ar_Trx_Header.SoStatus		in ('2','3','4','5') and 
	Ar_Trx_Line.ArTrxHdrIdx		(+)= Ar_So_Line.ArTrxHdrIdx and
	Ar_Trx_Line.ArTrxLineTrxType	(+)= '1' and
	Ar_Trx_Line.ProductIdx		(+)= Ar_So_Line.ProductIdx and
	Ar_Trx_Line.artrxlineseq		(+)= Ar_So_Line.arsolineseq and
	Ar_Trx_Line.deleteflag		(+)= 'N' and
	Fc_Unit_Of_Measure.UomIdx		= Ar_So_Line.UomIdx And 
	Ic_Warehouse.WarehouseIdx 	= Ar_So_Line.WarehouseIdx And 
	Warehouse_Loc.NameIdx		= Ic_Warehouse.NameIdx AND  
	Warehouse_Loc.NameLocationSeq     	= Ic_Warehouse.NameLocationSeq and 
	Cust_Name.NameIdx 		= Ar_Trx_Header.CUSTNAMEIDX and 
	Cust_Loc.nameidx  			= Ar_Trx_Header.tocustnameidx and 
	Cust_Loc.OrderBy          	= '1' and
	ShipTo_Name.NameIdx 		= Ar_Trx_Header.ToCUSTNAMEIDX and 
	ShipTo_Loc.nameidx               	= Ar_Trx_Header.tocustnameidx and  
	ShipTo_Loc.namelocationseq        	= Ar_Trx_Header.tocustlocseq and
	Fc_Sale_Terms.SaleTermsIdx        	= Ar_Trx_Header.SaleTermsIdx and
	Ar_Pay_Terms.ArPayTermsIdx        	= Ar_Trx_Header.ArPayTermsIdx and
	Sales_Name.NameIdx 		= Ar_Trx_Header.SalesPersonNameIdx and
	Ic_Product_Produce.ProductIdx (+)= Ar_So_Line.ProductIdx and
		
  	/* Conditions */
	Ar_Trx_Header.ShipDateTime between 
	Decode(:MinShipDate,'',To_Date('01/01/1900','MM/DD/YYYY'),To_Date(:MinShipDate, 'MM/DD/YY')) and 
	Decode(:MaxShipDate,'',To_Date('01/01/2099','MM/DD/YYYY'),To_Date(:MaxShipDate, 'MM/DD/YY')) and
	lower(Ar_Trx_Header.SoNo)		= DECODE(:OrderNumber, '', lower(Ar_Trx_Header.SoNo), lower(:OrderNumber)) AND
	(lower(Ar_Trx_Header.CustPoRef)	= DECODE(:CustomerPO, '', lower(Ar_Trx_Header.CustPoRef), lower(:CustomerPO)) OR
	Ar_Trx_Header.CustPoRef 		is null ) and
	lower(Cust_Name.Id)		= DECODE(:Customer, '', lower(Cust_Name.Id), lower(:Customer)) 
    
union all
	
  SELECT /*+ CHOOSE */

	Ar_Trx_Header.ArTrxHdrIdx,
	Ar_Trx_Header.SONO,
	Ar_Trx_Line.ArTrxHdrIdx as ArLineIdx,
	Ar_Trx_Header.SoDateTime,
	Ar_Trx_Header.ShipDateTime,
	Ar_Trx_Header.InvoiceDate,
	Sales_Name.LastCoName as SalesMan,
	Cust_Name.LastCoName As SoldToName,
	Cust_Loc.City As SoldToCity,
	Cust_Loc.State As SoldToState,
	Cust_Loc.Zip As SoldToZip,
	ShipTo_Name.LastCoName As ShipToName,
	ShipTo_Loc.City As ShipToCity,
	ShipTo_Loc.State As ShipToState,
	ShipTo_Loc.Zip As ShipToZip,
	Fc_Sale_Terms.DESCR,
	Ar_Pay_Terms.DESCR as PayTerms,
	Ar_Trx_Header.CUSTPOREF,
	Ar_Trx_Header.CARRIER,
	Ar_Trx_Header.TRAILERLICENSE,
	Ar_Trx_Line.UnitsPerPallet,
	Ar_Trx_Line.INVCDESCR,  
	Fc_Unit_Of_Measure.Uom,   
	Ar_So_Line.icQnt as IcQnt,/* testing */
	Ar_Trx_Line.PRICE as FOBPrice,   
	Ar_Trx_Line.SALEPRICE as SalesPrice, 
	Ar_Trx_Line.ConsignFlag as ConsignFlag,
	Warehouse_Loc.Descr as Warehouse,  
	Ar_Trx_Header.SOAMT as SoldAmt,   
	Ar_Trx_Header.ARAMT as PriceAMT,   
	Ar_Trx_Header.SOSTATUS,   
	Ar_Trx_Header.RECEIPTAMT, 
	Ar_Trx_Header.HOLDFLAG, 
	Ar_Trx_Header.TOETADATETIME,   
      	Ar_Trx_Line.Qnt as Qnt,
	trunc(Decode(Ar_Trx_Line.UnitsPerPallet,0,0,(Ar_Trx_Line.Qnt/Ar_Trx_Line.UnitsPerPallet)),16) as InvoicePallets,
	0,
	Ar_Trx_Line.PRICE as InvoicePrice,   
	Ar_Trx_Line.SALEPRICE as InvoiceSalePrice,
	Ic_Product_Produce.CmtyIdx
			
   FROM  
	Ar_Trx_Header, 
	Ar_So_Line,
	Fc_Unit_Of_Measure,
	FC_NAME_LOCATION Warehouse_Loc,
	Ic_Warehouse,
	FC_NAME Cust_Name,
	FC_NAME_LOCATION Cust_Loc,
	FC_NAME ShipTo_Name,
	FC_NAME_LOCATION ShipTo_Loc,
	Fc_Sale_Terms,
	Ar_Pay_Terms,
	Fc_Name Sales_Name,
	Ar_Trx_Line,
	Ic_Product_Produce
				
   WHERE  
	AR_TRX_HEADER.SOTYPE 		IN ('2','5') and
	Ar_Trx_Header.SoStatus		in ('6','7','C','P') and 
	Ar_Trx_Line.ArTrxHdrIdx		= AR_TRX_HEADER.ArTrxHdrIdx and
	Ar_Trx_Line.ArTrxLineTrxType	= '1' and 
	Ar_So_Line.ArTrxHdrIdx		(+)= Ar_Trx_Line.ArTrxHdrIdx and
	Ar_So_Line.ProductIdx		(+)= Ar_Trx_Line.ProductIdx and
	Ar_So_Line.arsolineseq		(+)= Ar_Trx_Line.artrxlineseq and
	Ar_So_Line.ArSoLineTrxType		(+)= '1' and
	

	Ic_Warehouse.WarehouseIdx 	(+)= Ar_So_Line.WarehouseIdx And 
	Warehouse_Loc.NameIdx		(+)= Ic_Warehouse.NameIdx AND  
	Warehouse_Loc.NameLocationSeq     	(+)= Ic_Warehouse.NameLocationSeq and 

	Fc_Unit_Of_Measure.UomIdx		= Ar_Trx_Line.UomIdx And 
	Cust_Name.NameIdx 		= Ar_Trx_Header.CUSTNAMEIDX and 	
	Cust_Loc.nameidx  			= Ar_Trx_Header.tocustnameidx and 
	Cust_Loc.OrderBy         	= '1' and
	ShipTo_Name.NameIdx 		= Ar_Trx_Header.ToCUSTNAMEIDX and 
	ShipTo_Loc.nameidx               	= Ar_Trx_Header.tocustnameidx and  
	ShipTo_Loc.namelocationseq       	= Ar_Trx_Header.tocustlocseq and
	Fc_Sale_Terms.SaleTermsIdx        	= Ar_Trx_Header.SaleTermsIdx and
	Ar_Pay_Terms.ArPayTermsIdx        	= Ar_Trx_Header.ArPayTermsIdx and
	Sales_Name.NameIdx 		= Ar_Trx_Header.SalesPersonNameIdx and
	Ic_Product_Produce.ProductIdx (+)= Ar_Trx_Line.ProductIdx and

   	/* Conditions */
	Ar_Trx_Header.ShipDateTime     between 
	Decode(:MinShipDate,'',To_Date('01/01/1900','MM/DD/YYYY'),To_Date(:MinShipDate, 'MM/DD/YY')) and 
	Decode(:MaxShipDate,'',To_Date('01/01/2099','MM/DD/YYYY'),To_Date(:MaxShipDate, 'MM/DD/YY')) and
	lower(Ar_Trx_Header.SoNo)		= DECODE(:OrderNumber, '', lower(Ar_Trx_Header.SoNo), lower(:OrderNumber)) AND
	(lower(Ar_Trx_Header.CustPoRef)	= DECODE(:CustomerPO, '', lower(Ar_Trx_Header.CustPoRef), lower(:CustomerPO)) or
	Ar_Trx_Header.CustPoRef is null) and
	lower(Cust_Name.Id)				= DECODE(:Customer, '', lower(Cust_Name.Id), lower(:Customer)) 
     

";
		#endregion

	}
}
