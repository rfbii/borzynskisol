using System;
using System.Collections.Generic;
using System.Text;
using KeyCentral.Functions;

namespace CustomerStatusLib
{
    [MenuMap(Security = "Customer Status Report", Description = "Customer Status Report", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "/Reports/Customer Status", SortOrder = 10, Url = "../../../CustomerStatusABZ/Reports/CustomerStatus/CriteriaCustomerStatus.aspx?ReportName=CustomerStatusReport")]
    [MenuMap(Security = "", Description = "Customer Status", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "/Reports", DisplayIfEmpty = false, SortOrder = 1, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Reports/Customer Status")]
    [MenuMap(Security = "Customer Status Report", Description = "Customer Status Four Week Report", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "/Reports/Customer Status", SortOrder = 11, Url = "../../../CustomerStatusABZ/Reports/CustomerStatus/CriteriaCustomerStatusFourWeek.aspx?ReportName=CustomerStatusFourWeekReport")]

    class Menumap
    {
    }
}
