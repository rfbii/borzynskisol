using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Text;
using KeyCentral.Lookups;
using KeyCentral.Functions;
using System.Data.SqlClient;

namespace ConsolidatedFinancial.Reports.MonthlyEntries
{
	public partial class ReportMonthlyEntries : BasePage
	{
		#region Declares
		#endregion
	
		#region Param Properties
		private DataSet MonthlyEntriesData{get{return (DataSet)Session["Data"];}}
		private DataSet NewMonthlyEntriesData{get{return (DataSet)Session["NewData"];}}
		private string GLMinDate{get{return Session["GLMinDate"].ToString();}}
		private string GLMinYear{get{return Session["GLMinYear"].ToString();}}
		private string GLMinWeek{get{return Session["GLMinWeek"].ToString();}}
		private string GLMaxDate{get{return Session["GLMaxDate"].ToString();}}
		private string GLMaxYear{get{return Session["GLMaxYear"].ToString();}}
		private string GLMaxWeek{get{return Session["GLMaxWeek"].ToString();}}
		#endregion
		#region Report Print
		#region Vars
		bool mainHeader = true;
		bool mainFooter = true;
		protected System.Web.UI.WebControls.Button cmdTransfer;
		private int NumberOfCompanies = 0;
		private decimal[] SubtotalArray;
		int SubtotalArrayIndex = 0;
        private ArrayList CompanyArl = new ArrayList();
        #endregion

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckSecurity("Monthly Entries") == false)
            {
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/MenuMain.aspx");
            }

            string commentStr = SetCommentLine();
            DisplayOutput(Response, commentStr);
        }

        private void DisplayOutput(System.Web.HttpResponse response, string commentStr)
        {
            //Set the response content type
            response.ContentType = @"application/x-msexcel";
            Response.AddHeader("content-disposition", "attachment; filename=\"" + "ReportMonthlyEntries.csv" + "\"");
            
            #region  Header
            DataSet MonthlyEntriesData = new DataSet();
            MonthlyEntriesData = GetMonthlyEntriesData(); //Fills CompanyArl
            MonthlyEntriesData.Tables[0].DefaultView.Sort = "GLAccountID";

            response.Output.Write("Monthly Entries Report,");
            response.Output.Write("\n");
            response.Output.Write("\"" + commentStr + "\",");
            response.Output.Write("\n");
            response.Output.Write("\n");
            response.Output.Write("GL Account ID,");

            for (int j = 0; j < CompanyArl.Count; j++)
            {
                Company tempCompany = (Company)CompanyArl[j];
                response.Output.Write("\"" + tempCompany.companyNameStr.ToString() + "\",");
            }

            response.Output.Write("GL Account Total,");
            response.Output.Write("\n");
            #endregion

            #region Detail
            
            for (int i = 0; i < MonthlyEntriesData.Tables[0].Rows.Count; i++)
            {
                DataRow tempRow = MonthlyEntriesData.Tables[0].Rows[i];
                response.Output.Write("\"" + tempRow["GLAccountID"] + "\",");
                double tempLineTotalDbl = 0;
                for (int j = 0; j < CompanyArl.Count; j++)
                {
                    Company tempCompany = (Company)CompanyArl[j];
                    response.Output.Write("\"" + Misc.DisplayCurency(Convert.ToDouble(tempRow[tempCompany.companyNameStr.Replace(".", "")])) + "\",");
                    tempLineTotalDbl += (Convert.ToDouble(tempRow[tempCompany.companyNameStr.Replace(".", "")]));
                }
                response.Output.Write("\"" +  Misc.DisplayCurency(tempLineTotalDbl) + "\",");
                response.Output.Write("\n");
            }            
            #endregion

            #region Footer
            response.Output.Write("\n");
            double totalZeroDbl = 0;

            response.Output.Write("SubTotal,"); //Zero out bottom line.
            for (int j = 0; j < CompanyArl.Count; j++)
            {
                Company tempCompany = (Company)CompanyArl[j];
                response.Output.Write("\"" + Misc.DisplayCurency(totalZeroDbl) + "\",");
            }
            response.Output.Write("\"" + Misc.DisplayCurency(totalZeroDbl) + "\",");//add 1 more zero for Total Column
            response.End();
            #endregion           
        }

//        public string Header()
//        {
//            string CompanyName = "";
//            string CompanyKey = "";

//            string header="";

//            SqlConnection Conn = GetSQLConn();
//            SqlCommand selectCMD = new SqlCommand("use KeyCentral;select [Company].Company_Name,[Company].Report_Number as Company_Key from [Company] Order by Company_Name", Conn);

//            SqlDataReader myReader = selectCMD.ExecuteReader();

//            if(mainHeader)
//            {
//                header = @"<table border='0' cellpadding='1' cellspacing='0' width='960'>
//								<tr>
//									<td class='Title2' height=21 align=center>GL Account ID</td>";

//                if(myReader.HasRows == true)
//                {
//                    while (myReader.Read())
//                    {
//                        CompanyName = myReader.GetString(0);
//                        CompanyKey = myReader.GetString(1);

//                        header += @"<td class='Title2' height=21 align=center>" + CompanyName + "</td>";
//                    }
//                }

//                header += @"<td class='Title2' height=21 align=center>GL Account Total</td>
//							</tr>
//							</table>";
//                mainHeader = false;

//                // Create the subtotals variables
//                SubtotalArray = new decimal[NumberOfCompanies + 1];

//                // Populate the SubtotalArray with 0's
//                for (int j = 0; j < SubtotalArray.Length; j++)
//                {
//                    SubtotalArray[j] = 0;
//                }
//            }
			
//            Conn.Close();
//            Conn.Dispose();

//            return string.Empty;// header;
//        }

//        public string Detail(object oItem)
//        {
//            decimal subTotal = 0;

//            string CompanyName = "";
//            string CompanyKey = "";

//            string detail="";

//            SqlConnection Conn = GetSQLConn();
//            SqlCommand selectCMD = new SqlCommand("use KeyCentral;select [Company].Company_Name,[Company].Report_Number as Company_Key from [Company] Order by Company_Name", Conn);

//            SqlDataReader myReader = selectCMD.ExecuteReader();

//            detail = @"
//				<table border='0' cellpadding='1' cellspacing='0' width='960'>
//					<tr>
//						<td class='Detail2' height=21 align=left>" + GetField(oItem,"GLAccountID") + @"</td>";

//            if(myReader.HasRows == true)
//            {
//                while (myReader.Read())
//                {
//                    CompanyName = myReader.GetString(0);
//                    CompanyKey = myReader.GetString(1);

//                    CompanyName = CompanyName.Replace(".", "");

//                    detail += @"<td class='Title3' height=21 align=right>" + Misc.DisplayHTMLForCurency(decimal.Parse(GetField(oItem,CompanyName))) + @"</td>";

//                    subTotal = Convert.ToDecimal(SubtotalArray[SubtotalArrayIndex]) + Convert.ToDecimal(GetField(oItem,CompanyName));

//                    SubtotalArray[SubtotalArrayIndex] = subTotal;

//                    SubtotalArrayIndex++;
//                }
//            }

//            detail += @"<td class='Detail1' height=21 align=right>" + Misc.DisplayHTMLForCurency(decimal.Parse(GetField(oItem,"GLAccountTotal"))) + @"</td>
//						</tr>
//						</table>";

//            subTotal = Convert.ToDecimal(SubtotalArray[SubtotalArrayIndex]) + Convert.ToDecimal(GetField(oItem,"GLAccountTotal"));

//            SubtotalArray[SubtotalArrayIndex] = subTotal;

//            if (SubtotalArrayIndex >= NumberOfCompanies)
//                SubtotalArrayIndex = 0;

//            Conn.Close();
//            Conn.Dispose();

//            //ultimateOutput += detail;
//            ultimateOutput.Append(detail);

//            return string.Empty;// detail;
//        }

//        public string Footer()
//        {
//            string footer="";

//            if(mainFooter)
//            {
//                footer += @"
//				<table border='0' cellpadding='1' cellspacing='0' width='960'>
//					<tr>
//						<td class='Detail2' height=21 align=left>&nbsp;</td>";
//                for (int i = 0; i < SubtotalArray.Length - 1; i++)
//                {
//                    footer += @"<td class='Title3' height=21 align=right>&nbsp;</td>";
//                }
//                footer += @"<td class='Detail1' height=21 align=right>&nbsp;</td>
//						</tr>
//					</table>";

//                footer += @"
//				<table border='0' cellpadding='1' cellspacing='0' width='960'>
//					<tr>
//						<td class='Detail2' height=21 align=left style='FONT-WEIGHT: bold; COLOR: red'>Subtotal</td>";
//                for (int i = 0; i < SubtotalArray.Length - 1; i++)
//                {
//                    footer += @"<td class='Title3' height=21 align=right>" + Misc.DisplayHTMLForCurency(SubtotalArray[i]) + @"</td>";
//                }
//                footer += @"<td class='Detail1' height=21 align=right>" + Misc.DisplayHTMLForCurency(SubtotalArray[SubtotalArray.Length - 1]) + @"</td>
//						</tr>
//					</table>";

//                mainFooter = false;

//                //ultimateOutput += footer;
//                ultimateOutput.Append(footer);
//            }

//            return string.Empty;// footer;
//        }

		private string GetField(object oItem,string strField)
		{
			string temp = DataBinder.Eval(oItem,"DataItem." + strField).ToString();
			return DataBinder.Eval(oItem,"DataItem." + strField).ToString();
		}

		private string GetDateField(object oItem,string strField)
		{
			string str = DataBinder.Eval(oItem,"DataItem." + strField).ToString();
			if(str.IndexOf(" ",0)>0)
				return str.Substring(0,str.IndexOf(" ",0));
			else
				return str;
		}
		#endregion

		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}
		
		private void InitializeComponent()
		{    
		}
		#endregion
		
		/// <summary>
		/// Get Bank Balance for all Companies.
		/// </summary>
		/// <returns></returns>
		private DataSet GetMonthlyEntriesData()
		{
			DataSet reportData = new DataSet();
			string CompanyName = "";
			string CompanyKey = "";

			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("use KeyCentral;select [Company].Company_Name,[Company].Report_Number as Company_Key from [Company] Order by Company_Name", Conn);

			SqlDataReader myReader = selectCMD.ExecuteReader();

			if(myReader.HasRows == true)
			{
				while (myReader.Read())
				{
					CompanyName = myReader.GetString(0);
					CompanyKey = myReader.GetString(1);

                    Company tempCompany = new Company();
                    tempCompany = FindOrAddCompany(CompanyArl, CompanyName);

					reportData.Merge(GetMonthlyEntriesData(CompanyKey,CompanyName));
				}
			}
			Conn.Close();
			Conn.Dispose();

			reportData = MakeNewDataSet(reportData.Tables[0].DefaultView);

			return reportData;
		}

		private DataSet MakeNewDataSet(DataView pDataView)
		{
			pDataView.Sort = "GLAccountID, CompanyName ASC";

			DataSet NewMonthlyEntriesData = new DataSet();
			NewMonthlyEntriesData.Tables.Add("Table");

			string CompanyName = "";
			string CompanyKey = "";

			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("use KeyCentral;select [Company].Company_Name,[Company].Report_Number as Company_Key from [Company] Order by Company_Name", Conn);

			SqlDataReader myReader = selectCMD.ExecuteReader();

			NewMonthlyEntriesData.Tables[0].Columns.Add("GLAccountID");

			if(myReader.HasRows == true)
			{
				while (myReader.Read())
				{
					CompanyName = myReader.GetString(0);
					CompanyKey = myReader.GetString(1);

					CompanyName = CompanyName.Replace("''", "'");
					CompanyName = CompanyName.Replace(".", "");

					NewMonthlyEntriesData.Tables[0].Columns.Add(CompanyName, typeof(System.String));
					NumberOfCompanies ++;
				}
			}
			Conn.Close();
			Conn.Dispose();

			NewMonthlyEntriesData.Tables[0].Columns.Add("GLAccountTotal", typeof(System.String));

			int i = 0;
			CompanyName = "";

			// tempGLAccountID holds the "previous" GL Account ID for comparison
			string tempGLAccountID = "";

			DataRow newRow = NewMonthlyEntriesData.Tables[0].NewRow();
			while (i <= pDataView.Count)
			{
				// for the last row only
				if (i == pDataView.Count)
				{
					decimal GLAccountTotal = 0;

					for (int y = 1; y <= NumberOfCompanies; y++)
					{
						// if there's no record for this company, put in a zero
						if (newRow[y].ToString() == "")
						{
							newRow[y] = "0";
						}

						GLAccountTotal += Convert.ToDecimal(newRow[y].ToString());
					}

					// Calculate the GL Account Total
					newRow["GLAccountTotal"] = GLAccountTotal;

					// Add the row to the DataSet
					NewMonthlyEntriesData.Tables[0].Rows.Add(newRow);
					newRow = NewMonthlyEntriesData.Tables[0].NewRow();
					break;
				}

				// for all the other rows
				if (i > 0 && tempGLAccountID != pDataView[i]["GLAccountID"].ToString())
				{
					decimal GLAccountTotal = 0;
					for (int y = 1; y <= NumberOfCompanies; y++)
					{
						// if there's no record for this company, put in a zero
						if (newRow[y].ToString() == "")
						{
							newRow[y] = "0";
						}

						GLAccountTotal += Convert.ToDecimal(newRow[y].ToString());
					}

					// Calculate the GL Account Total
					newRow["GLAccountTotal"] = GLAccountTotal;

					// Add the row to the DataSet
					NewMonthlyEntriesData.Tables[0].Rows.Add(newRow);
					newRow = NewMonthlyEntriesData.Tables[0].NewRow();
				}

				newRow["GLAccountID"] = pDataView[i]["GLAccountID"].ToString();
				tempGLAccountID = newRow["GLAccountID"].ToString();

				CompanyName = pDataView[i]["CompanyName"].ToString();

                CompanyName = CompanyName.Replace("''", "'");
                CompanyName = CompanyName.Replace(".", "");

				newRow[CompanyName] = pDataView[i]["Amount"].ToString();

				i++;
			}

			return NewMonthlyEntriesData;
		}

		/// <summary>
		/// Get Monthly Entries Data from Oracle
		/// </summary>
		/// <param name="oracleReportNumber"></param>
		/// <param name="name"></param>
		/// <returns></returns>
		private DataSet GetMonthlyEntriesData(string oracleReportNumber,string name)
		{
			DataSet MonthlyEntriesData = new DataSet();
			name = name.Replace("'", "''");

			OracleConnection OracleConn = Misc.GetOracleConn(oracleReportNumber);
			#region Oracle Code
			string reportCommand = Misc.FormatOracle(oracleReportNumber + "_rpt",@"SELECT 
			SUM (GL_TRANSACTION_VIEW.AMOUNT) AS AMOUNT,
			GL_TRANSACTION_VIEW.GLACCOUNTID,
			GL_TRANSACTION_VIEW.GLACCOUNTNAME,
			GL_TRANSACTION_VIEW.DEPTNAME,
			GL_TRANSACTION_VIEW.DIVISIONNAME,
			GL_TRANSACTION_VIEW.DEPTID,
			GL_TRANSACTION_VIEW.DIVIDX

			FROM    GL_TRANSACTION_VIEW
			
			WHERE
				(GL_TRANSACTION_VIEW.ACRUCASHTYPE = 'A') AND
				(GL_TRANSACTION_VIEW.GLPOSTINGDATE BETWEEN
				DECODE(:GLMinDate,'',To_Date('01/01/1900 00:00:00','MM/DD/YYYY HH24:MI:SS'),To_Date(:GLMinDate, 'MM/DD/YY')) AND 
				DECODE(:GLMaxDate,'',To_Date('01/01/2099 23:59:59','MM/DD/YYYY HH24:MI:SS'),To_Date(:GLMaxDate, 'MM/DD/YY')))
			GROUP BY
				GL_TRANSACTION_VIEW.GLACCOUNTID,
				GL_TRANSACTION_VIEW.GLACCOUNTNAME,
				GL_TRANSACTION_VIEW.DEPTNAME,
				GL_TRANSACTION_VIEW.DIVISIONNAME,
				GL_TRANSACTION_VIEW.DEPTID,
				GL_TRANSACTION_VIEW.DIVIDX");
			#endregion
			OracleDataAdapter OracleData = new OracleDataAdapter(reportCommand, OracleConn);
			OracleData.SelectCommand.BindByName = true;
			OracleData.SelectCommand.Parameters.Add(":GLMinDate",GLMinDate);
			OracleData.SelectCommand.Parameters.Add(":GLMaxDate",GLMaxDate);
			OracleData.Fill(MonthlyEntriesData);
			OracleData.Dispose();
			Misc.CleanUpOracle(OracleConn);

			//MonthlyEntriesData.WriteXml(@"C:\temp\GetRawMonthlyEntriesData" + name.Replace("''", "'") + ".xls");

			//Add columns
			//MonthlyEntriesData.Tables[0].Columns.Add("CompanyKey",typeof(System.String),"'" + oracleReportNumber + "'");
			
            //MonthlyEntriesData.Tables[0].Columns.Add("CompanyName",typeof(System.String),"'" + name + "'");
            MonthlyEntriesData.Tables[0].Columns.Add(CreateColumnWithDefaultValue("CompanyName", System.Type.GetType("System.String"), name));

			return MonthlyEntriesData;
		}

		private void ShowReport()
		{	
            //DataSet MonthlyEntriesData = new DataSet();
            //MonthlyEntriesData = GetMonthlyEntriesData();
            //MonthlyEntriesData.Tables[0].DefaultView.Sort = "GLAccountID";
            //MonthlyEntriesData.WriteXml(@"C:\temp\GetRawMonthlyEntriesData.xls");

            //if (MonthlyEntriesData.Tables[0].DefaultView.Count != 0)
            //{
            //    this.Repeater1.DataSource = MonthlyEntriesData.Tables[0].DefaultView;
            //    Repeater1.DataBind();
            //}
            //else
            //    JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"])+ "&&Records=None");
		}

        private DataColumn CreateColumnWithDefaultValue(string columnsName, Type columnType, object value)
        {
            System.Data.DataColumn ret = new System.Data.DataColumn(columnsName, columnType);
            ret.DefaultValue = value;
            return ret;
        }

		private string SetCommentLine()
		{
			StringBuilder comment = new StringBuilder();

			if(GLMinYear != "")
			{
				comment.Append(GLMinYear);
				comment.Append("-");
				comment.Append(GLMinWeek);
				comment.Append(" TO ");

				comment.Append(GLMaxYear);
				comment.Append("-");
				comment.Append(GLMaxWeek);
				comment.Append(" ");

			}
			else if(GLMinDate != "")
			{ 
				comment.Append(GLMinDate);
				comment.Append(" TO ");
				comment.Append(GLMaxDate);
				comment.Append(" ");
			}
            return comment.ToString();
		}

        public Company FindOrAddCompany(ArrayList list, string companyNameStr)
        {
            Company tempCompany = new Company(companyNameStr);
            int tempCompanyIdx = list.BinarySearch(tempCompany);

            if (tempCompanyIdx < 0)
                list.Insert(Math.Abs(tempCompanyIdx) - 1, tempCompany);
            else
                tempCompany = (Company)list[tempCompanyIdx];

            return tempCompany;
        }

        #region CBO
        public class Company : IComparable
        {
            #region Vars
            public string companyNameStr;
            public double calcDbl;
            #endregion

            #region Constructor
            public Company()
            {
                companyNameStr = string.Empty;
                calcDbl = 0;
            }
            public Company(string CompanyNameStr)
            {
                companyNameStr = CompanyNameStr;
                calcDbl = 0;
            }
            #endregion

            #region IComparable Members
            public int CompareTo(object obj)
            {
                int tempCompare;
                Company Y = (Company)obj;

                //Compare companyNameStr
                tempCompare = this.companyNameStr.CompareTo(Y.companyNameStr);
                if (tempCompare != 0)
                    return tempCompare;

                return 0;
            }
            #endregion
        }
        #endregion
	}
}
