<%@ Page EnableViewState="False" language="c#" Inherits="ConsolidatedFinancial.Reports.ConsolidatedFinancial.ReportConsolidatedFinancial" Codebehind="ReportConsolidatedFinancial.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Consolidated Financial Report</title> 
		<!-- Email Start -->
		<style type="text/css">TD { FONT-SIZE: 8pt }
	.Title { FONT-WEIGHT: bold; FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: Arial }
	.Header { BORDER-TOP: black 1px solid; FONT-WEIGHT: bold; FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: Arial; BACKGROUND-COLOR: silver }
	.Title1 { FONT-SIZE: 14pt; FONT-FAMILY: Arial }
	.Title2 { BORDER-RIGHT: black 1px solid; FONT-SIZE: 11pt; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Arial }
	.Title3 { BORDER-RIGHT: black 1px solid; FONT-SIZE: 11pt; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Arial }
	.Detail1 { BORDER-RIGHT: black double; FONT-SIZE: 11pt; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Arial }
	.Detail2 { BORDER-RIGHT: black 1px solid; FONT-SIZE: 11pt; BORDER-LEFT: black double; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Arial }
	.Title4 { BORDER-RIGHT: black 1px solid; FONT-WEIGHT: bold; FONT-SIZE: 11pt; FONT-FAMILY: Arial }
	.Title4A { FONT-WEIGHT: bold; FONT-SIZE: 11pt; BORDER-LEFT: black double; FONT-FAMILY: Arial }
	.Title4B { BORDER-RIGHT: black double; FONT-WEIGHT: bold; FONT-SIZE: 11pt; FONT-FAMILY: Arial }
	.Title5 { BORDER-RIGHT: black double; BORDER-TOP: black double; FONT-WEIGHT: bold; FONT-SIZE: 11pt; BORDER-LEFT: black double; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Arial }
	.Title6 { BORDER-RIGHT: black 1px solid; BORDER-TOP: black double; FONT-WEIGHT: bold; FONT-SIZE: 11pt; BORDER-BOTTOM: black double; FONT-FAMILY: Arial }
	.Title6A { BORDER-TOP: black double; FONT-WEIGHT: bold; FONT-SIZE: 11pt; BORDER-LEFT: black double; BORDER-BOTTOM: black double; FONT-FAMILY: Arial }
	.Title6B { BORDER-RIGHT: black double; BORDER-TOP: black double; FONT-WEIGHT: bold; FONT-SIZE: 11pt; BORDER-BOTTOM: black double; FONT-FAMILY: Arial }
	.Title7 { BORDER-RIGHT: black 1px solid; FONT-WEIGHT: bold; FONT-SIZE: 11pt; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Arial }
	.Title7A { FONT-WEIGHT: bold; FONT-SIZE: 11pt; BORDER-LEFT: black double; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Arial }
	.Title7B { BORDER-RIGHT: black double; FONT-WEIGHT: bold; FONT-SIZE: 11pt; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Arial }
		</style>
		<!-- Email End -->
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
        <!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
		</style>-->
        <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
		<link href="/KeystoneCustomerStatus/Inc/_styles.css" type="text/css" rel="stylesheet"/>
	</head>
	<body bgcolor="#ffffff" onload="SetupPrinting();">

		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div id="BodyDiv" class="scroll">
				<!-- Email Start -->
					<%--<asp:repeater id="Repeater1" runat="server">
						<ItemTemplate>
							<%# Header(Container)%>
							<%# Detail(Container)%>
						</ItemTemplate>
						<FooterTemplate>
							<%# Footer()%>
						</FooterTemplate>
					</asp:repeater>--%>
				<!-- Email End -->
			</div>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
		</form>
	</body>
</html>