using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KeyCentral.Functions;
using KeyCentral.Lookups;
using System.Data.SqlClient;


namespace ConsolidatedFinancial.Reports.ConsolidatedFinancial
{

	/// <summary>
	/// Summary description for Consolidated Financial.
	/// </summary>
	/// 
	public partial class CriteriaConsolidatedFinancial : BasePage
	{
		protected ReportSessionManager reportSessionManager;
		protected System.Web.UI.WebControls.Label Label2;
		protected DataSet reportData = new DataSet();
		
		
		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Consolidated Financial Report") == false)
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/MenuMain.aspx");

			GLDate.Description = "GL";

			#region SessionManager
			reportSessionManager = new ReportSessionManager(this,Request["ReportName"].ToString());
			
			//Load Vars from session if this report is in SessionManager
			if(IsPostBack ==false && reportSessionManager.CheckReportVars())
			{
				GLDate.MinDate = (string) Session["GLMinDate"];
				GLDate.MaxDate = (string) Session["GLMaxDate"];
				reportData = (DataSet) Session["Data"];
			}
		
			#endregion
			if(IsPostBack ==false)
			{
				Message.Text ="";

				if (Request.QueryString["Records"]== "None")
					Message.Text = "No Records Found.";

				//GLDate.MinDate = (string) Session["GLMinDate"];
				//GLDate.MaxDate = (string) Session["GLMaxDate"];

				cmdRun.Attributes.Add("onClick","return AskIfFieldsBlank('GLDate');");
				//GLDate.IsRequired=true;
			}
			
		}
		protected void cmdRun_Click(object sender, System.EventArgs e)
		{
			if(GLDate.IsValid)
			{
				//Add Report Vars
				reportSessionManager.AddSession("Data",new DataSet());

				//Let the reportSessionManager know that we are done
				SaveVarsToReportMananger();

				//Build next page string and redirect to it
                this.RegisterStartupScript("PopUpReport", "<script>window.open('ReportConsolidatedFinancial.aspx?LastPage=CriteriaConsolidatedFinancial.aspx~ReportName=CriteriaConsolidatedFinancial','ConsolidatedFinancialDataReportWindow');</script>");
			}
		}

		protected void cmdMenu_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Financial");
		}
		#endregion
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion
	
		#region Private helpers
		private void LoadVarsFromReportManager()
		{
			//From Session (Session will override any QueryString Values
			GLDate.MinDate = Session["GLMinDate"].ToString();
			GLDate.MinYear = Session["GLMinYear"].ToString();
			GLDate.MinWeek = Session["GLMinWeek"].ToString();
			GLDate.MaxDate = Session["GLMaxDate"].ToString();
			GLDate.MaxYear = Session["GLMaxYear"].ToString();
			GLDate.MaxWeek = Session["GLMaxWeek"].ToString();

			//Let the reportSessionManager know that we are done
			reportSessionManager.SaveSessions();
		}
		private void SaveVarsToReportMananger()
		{
			reportSessionManager.AddSession("GLMinDate",GLDate.MinDate);
			reportSessionManager.AddSession("GLMinYear",GLDate.MinYear);
			reportSessionManager.AddSession("GLMinWeek",GLDate.MinWeek);
			reportSessionManager.AddSession("GLMaxDate",GLDate.MaxDate);
			reportSessionManager.AddSession("GLMaxYear",GLDate.MaxYear);
			reportSessionManager.AddSession("GLMaxWeek",GLDate.MaxWeek);

			reportSessionManager.AddSession("Report",Request.QueryString["ReportName"]);

			//Let the reportSessionManager know that we are done
			reportSessionManager.SaveSessions();
		}
		#endregion	
	}
}
