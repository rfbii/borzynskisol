<%@ Register TagPrefix="cc1" Namespace="Cet" Assembly="Cet.ConfirmButton" %>
<%@ Page language="c#" Inherits="ConsolidatedFinancial.Reports.ConsolidatedFinancial.CriteriaConsolidatedFinancial" Codebehind="CriteriaConsolidatedFinancial.aspx.cs" %>
<%@ Register Assembly="KeyCentralABZLib" Namespace="KeyCentralLib" TagPrefix="cc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Consolidated Financial</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
		</style>-->
        <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body>
		<script type="text/javascript" language="javascript">
			function AskIfFieldsBlank(dateFieldName)
			{
				if(document.getElementById(dateFieldName +"_minDate").value =="" || document.getElementById(dateFieldName +"_maxDate").value =="" )
				{
					alert('From and Thru dates are required.');
					return false;
				}
				else
					return true;
			}
		</script>
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
				<table cellspacing="0" cellpadding="0" width="785" align="center" border="0">
					<tr height="20">
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center">Consolidated Financial</td>
					</tr>
					<tr height="10">
						<td>&nbsp;</td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" align="center" border="0">
					<tr>
						<td align="center">
                            <cc2:datecriteria id="GLDate" runat="server"></cc2:datecriteria>
                        </td>
					</tr>
				</table>
				<table width="785" align="center">
					<tr>
						<td align="center"><asp:button id="cmdRun" runat="server" Text="Run Report" TabFinish="" EnterAsTab="" onclick="cmdRun_Click"></asp:button>&nbsp;<asp:button id="cmdMenu" runat="server" Text="Return to Financial Menu" CausesValidation="False" onclick="cmdMenu_Click"></asp:button></td>
					</tr>
				</table>
				<asp:label id="Message" style="Z-INDEX: 101; LEFT: 8px" runat="server" ForeColor="Red"></asp:label>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"--></div>
		</form>
	</body>
</html>