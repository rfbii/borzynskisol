using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Text;
using KeyCentral.Lookups;
using KeyCentral.Functions;
using System.Data.SqlClient;
using ConsolidatedFinancialLib;

namespace ConsolidatedFinancial.Reports.ConsolidatedFinancial
{
    public partial class ReportConsolidatedFinancial : BasePage
    {
        #region Declares
        protected ReportSessionManager reportSessionManager;
        #endregion

        #region Param Properties
        private DataSet ConsolidatedFinancialData { get { return (DataSet)Session["Data"]; } }
        private DataSet NewConsolidatedFinancialData { get { return (DataSet)Session["NewData"]; } }
        private string GLMinDate { get { return Session["GLMinDate"].ToString(); } }
        private string GLMinYear { get { return Session["GLMinYear"].ToString(); } }
        private string GLMinWeek { get { return Session["GLMinWeek"].ToString(); } }
        private string GLMaxDate { get { return Session["GLMaxDate"].ToString(); } }
        private string GLMaxYear { get { return Session["GLMaxYear"].ToString(); } }
        private string GLMaxWeek { get { return Session["GLMaxWeek"].ToString(); } }
        #endregion

        #region Report Print
        #region Vars
        bool mainHeader = true;
        bool mainFooter = true;
        protected System.Web.UI.WebControls.Button cmdTransfer;
        private int NumberOfCompanies = 0;
        private decimal[] SubtotalArray;
        int SubtotalArrayIndex = 0;
        StringBuilder ultimateOutput;
        private string[] CalcLineArray;
        private ArrayList CompanyArl = new ArrayList();
        private bool IBln;
        private bool CBln;
        private bool LBln;
        #endregion

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckSecurity("Consolidated Financial Report") == false)
            {
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/MenuMain.aspx");
            }

            ultimateOutput = new StringBuilder();
            string commentStr = SetCommentLine();
            DisplayOutput(Response, commentStr);
        }

        private void DisplayOutput(System.Web.HttpResponse response, string commentStr)
        {
            DataSet ConsolidatedFinancialData = new DataSet();
            ConsolidatedFinancialData = GetConsolidatedFinancialData();
            ConsolidatedFinancialData.Tables[0].DefaultView.Sort = "GLAccountID";

            //Set the response content type
            response.ContentType = @"application/x-msexcel";
            Response.AddHeader("content-disposition", "attachment; filename=\"" + "ReportConsolidatedFinancial.csv" + "\"");

            IBln = false;
            CBln = false;
            LBln = false;
            for (int i = 0; i < ConsolidatedFinancialData.Tables[0].Rows.Count; i++)
            {
                DataRow tempRow = ConsolidatedFinancialData.Tables[0].Rows[i];
                if (tempRow["Type"].ToString().Trim() == "C")
                {
                    CBln = true;
                }
                if (tempRow["Type"].ToString().Trim() == "L")
                {
                    LBln = true;
                }
                if (tempRow["Type"].ToString().Trim() == "I")
                {
                    IBln = true;
                }
                //Add values into CompanyArl for CALC Line
                for (int j = 0; j < CompanyArl.Count; j++)
                {
                    Company tempCompany = (Company)CompanyArl[j];
                    tempCompany.calcDbl += Convert.ToDouble(tempRow[tempCompany.companyNameStr.Replace(".", "")]);
                }
            }
            response.Output.Write("Consolidated Financial Report,");
            response.Output.Write("\n");
            response.Output.Write("\"" + commentStr + "\",");
            response.Output.Write("\n");
            response.Output.Write("\n");
            response.Output.Write("GL Account ID,");
            response.Output.Write("Type,");

            for (int j = 0; j < CompanyArl.Count; j++)
            {
                Company tempCompany = (Company)CompanyArl[j];
                response.Output.Write("\"" + tempCompany.companyNameStr.ToString() + "\",");
            }
            response.Output.Write("GL Account Total,");
            response.Output.Write("\n");
            bool CalCFoundBln = false;
            string CALCTypeStr = "C";
            if (!CBln)
            {
                if (LBln)
                {
                    CALCTypeStr = "L";
                }
                else
                {
                    CALCTypeStr = "A";
                }
            }
            for (int i = 0; i < ConsolidatedFinancialData.Tables[0].Rows.Count; i++)
            {
                DataRow tempRow = ConsolidatedFinancialData.Tables[0].Rows[i];
                if (CALCTypeStr == tempRow["Type"].ToString())
                {
                    CalCFoundBln = true;
                }
                if (CalCFoundBln && tempRow["Type"].ToString().Trim() != CALCTypeStr)
                {
                    //Insert CALC line
                    double tempCalcLineTotalDbl = 0;
                    response.Output.Write("CALC,");
                    response.Output.Write("C,");
                    for (int j = 0; j < CompanyArl.Count; j++)
                    {
                        Company tempCompany = (Company)CompanyArl[j];
                        response.Output.Write("\"" + Misc.DisplayCurency(tempCompany.calcDbl) + "\",");
                        tempCalcLineTotalDbl += (tempCompany.calcDbl);
                    }
                    response.Output.Write("\"" + Misc.DisplayCurency(tempCalcLineTotalDbl) + "\",");
                    response.Output.Write("\n");
                    CalCFoundBln = false;
                }
                response.Output.Write("\"" + tempRow["GLAccountID"] + "\",");
                response.Output.Write("\"" + tempRow["Type"] + "\",");
                double tempLineTotalDbl = 0;
                if (tempRow["Type"].ToString().Trim() == "L" || tempRow["Type"].ToString().Trim() == "C" || tempRow["Type"].ToString().Trim() == "I")
                {
                    //Need for loop for CompanyArl add Column for each 
                    for (int j = 0; j < CompanyArl.Count; j++)
                    {
                        Company tempCompany = (Company)CompanyArl[j];
                        response.Output.Write("\"" + (Misc.DisplayCurency(Convert.ToDouble(tempRow[tempCompany.companyNameStr.Replace(".", "")]) * -1) + "\","));
                        tempLineTotalDbl += (Convert.ToDouble(tempRow[tempCompany.companyNameStr.Replace(".", "")]) * -1);
                    }
                }
                else
                {
                    //Need for loop for CompanyArl add Column for each 
                    for (int j = 0; j < CompanyArl.Count; j++)
                    {
                        Company tempCompany = (Company)CompanyArl[j];
                        response.Output.Write("\"" + (Misc.DisplayCurency(Convert.ToDouble(tempRow[tempCompany.companyNameStr.Replace(".", "")])) + "\","));
                        tempLineTotalDbl += (Convert.ToDouble(tempRow[tempCompany.companyNameStr.Replace(".", "")]));
                    }
                }
                response.Output.Write("\"" + Misc.DisplayCurency(tempLineTotalDbl) + "\",");
                response.Output.Write("\n");
            }

            #region Footer
            response.Output.Write("\n");
            response.Output.Write("SubTotal,,"); //Zero out bottom line.
            double totalZeroDbl = 0;
            for (int j = 0; j < CompanyArl.Count; j++)
            {
                Company tempCompany = (Company)CompanyArl[j];
                response.Output.Write("\"" + Misc.DisplayCurency(totalZeroDbl) + "\",");
            }
            response.Output.Write("\"" + Misc.DisplayCurency(totalZeroDbl) + "\",");//add 1 more zero for Total Column
            response.End();
            #endregion
        }
        #endregion
        
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        private void InitializeComponent()
        {
        }
        #endregion

        /// <summary>
        /// Get Bank Balance for all Companies.
        /// </summary>
        /// <returns></returns>
        private DataSet GetConsolidatedFinancialData()
        {
            DataSet reportData = new DataSet();
            string CompanyName = "";
            string CompanyKey = "";
            CompanyArl = new ArrayList();
            SqlConnection Conn = GetSQLConn();
            SqlCommand selectCMD = new SqlCommand("use KeyCentral;select [Company].Company_Name,[Company].Report_Number as Company_Key from [Company] Order by Company_Name", Conn);

            SqlDataReader myReader = selectCMD.ExecuteReader();

            if (myReader.HasRows == true)
            {
                while (myReader.Read())
                {
                    CompanyName = myReader.GetString(0);
                    CompanyKey = myReader.GetString(1);
                    
                    Company tempCompany = new Company();
                    tempCompany = FindOrAddCompany(CompanyArl, CompanyName);

                    reportData.Merge(GetOracleConsolidatedFinancialData(CompanyKey, CompanyName));
                }
            }
            Conn.Close();
            Conn.Dispose();

            reportData = MakeNewDataSet(reportData.Tables[0].DefaultView);

            return reportData;
        }

        private DataSet MakeNewDataSet(DataView pDataView)
        {
            pDataView.Sort = "GLAccountID, CompanyName ASC";

            DataSet NewConsolidatedFinancialData = new DataSet();
            NewConsolidatedFinancialData.Tables.Add("Table");

            string CompanyName = "";
            string CompanyKey = "";

            SqlConnection Conn = GetSQLConn();
            SqlCommand selectCMD = new SqlCommand("use KeyCentral;select [Company].Company_Name,[Company].Report_Number as Company_Key from [Company] Order by Company_Name", Conn);

            SqlDataReader myReader = selectCMD.ExecuteReader();

            NewConsolidatedFinancialData.Tables[0].Columns.Add("GLAccountID");
            NewConsolidatedFinancialData.Tables[0].Columns.Add("Type");

            if (myReader.HasRows == true)
            {
                while (myReader.Read())
                {
                    CompanyName = myReader.GetString(0);
                    CompanyKey = myReader.GetString(1);

                    CompanyName = CompanyName.Replace("''", "'");
                    CompanyName = CompanyName.Replace(".", "");

                    NewConsolidatedFinancialData.Tables[0].Columns.Add(CompanyName, typeof(System.String));
                    NumberOfCompanies++;
                }
            }
            myReader.Close();
            Conn.Close();
            Conn.Dispose();

            NewConsolidatedFinancialData.Tables[0].Columns.Add("GLAccountTotal", typeof(System.String));

            int i = 0;
            CompanyName = "";

            // tempGLAccountID holds the "previous" GL Account ID for comparison
            string tempGLAccountID = "";

            DataRow newRow = NewConsolidatedFinancialData.Tables[0].NewRow();
            while (i <= pDataView.Count)
            {
                // for the last row only
                if (i == pDataView.Count)
                {
                    decimal GLAccountTotal = 0;

                    for (int y = 2; y <= NumberOfCompanies + 1; y++)
                    {
                        // if there's no record for this company, put in a zero
                        if (newRow[y].ToString() == "")
                        {
                            newRow[y] = "0";
                        }

                        GLAccountTotal += Convert.ToDecimal(newRow[y].ToString());
                    }

                    // Calculate the GL Account Total
                    newRow["GLAccountTotal"] = GLAccountTotal;

                    // Add the row to the DataSet
                    NewConsolidatedFinancialData.Tables[0].Rows.Add(newRow);
                    newRow = NewConsolidatedFinancialData.Tables[0].NewRow();
                    break;
                }

                // for all the other rows
                if (i > 0 && tempGLAccountID != pDataView[i]["GLAccountID"].ToString())
                {
                    decimal GLAccountTotal = 0;
                    for (int y = 2; y <= NumberOfCompanies + 1; y++)
                    {
                        // if there's no record for this company, put in a zero
                        if (newRow[y].ToString() == "")
                        {
                            newRow[y] = "0";
                        }

                        GLAccountTotal += Convert.ToDecimal(newRow[y].ToString());
                    }

                    // Calculate the GL Account Total
                    newRow["GLAccountTotal"] = GLAccountTotal;

                    // Add the row to the DataSet
                    NewConsolidatedFinancialData.Tables[0].Rows.Add(newRow);
                    newRow = NewConsolidatedFinancialData.Tables[0].NewRow();
                }

                newRow["GLAccountID"] = pDataView[i]["GLAccountID"].ToString();
                newRow["Type"] = pDataView[i]["Type"].ToString();

                tempGLAccountID = newRow["GLAccountID"].ToString();

                CompanyName = pDataView[i]["CompanyName"].ToString();

                CompanyName = CompanyName.Replace("''", "'");
                CompanyName = CompanyName.Replace(".", "");
                
                newRow[CompanyName] = pDataView[i]["Amount"].ToString(); ;
                i++;
            }

            return NewConsolidatedFinancialData;
        }

        /// <summary>
        /// Get Consolidated Financial Data from Oracle
        /// </summary>
        /// <param name="oracleReportNumber"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        private DataSet GetOracleConsolidatedFinancialData(string oracleReportNumber, string name)
        {
            DataSet ConsolidatedFinancialData = new DataSet();
            name = name.Replace("'", "''");

            OracleConnection OracleConn = Misc.GetOracleConn(oracleReportNumber);
            #region Oracle Code
            string reportCommand = Misc.FormatOracle(oracleReportNumber + "_rpt", @"
			SELECT 
				SUM (GL_TRANSACTION_VIEW.AMOUNT) AS AMOUNT,
				GL_TRANSACTION_VIEW.GLACCOUNTID,
				GL_TRANSACTION_VIEW.GLACCOUNTNAME,
				GL_TRANSACTION_VIEW.DEPTNAME,
				GL_TRANSACTION_VIEW.DIVISIONNAME,
				GL_TRANSACTION_VIEW.DEPTID,
				GL_TRANSACTION_VIEW.DIVIDX,
				DECODE (GL_ACCOUNT.TYPE, 1, 'A', 2, 'L', 3, 'C', ' ') As Type

			FROM
				GL_TRANSACTION_VIEW,
				GL_ACCOUNT

			WHERE
				(GL_TRANSACTION_VIEW.ACRUCASHTYPE = 'A') AND
				(GL_TRANSACTION_VIEW.GLPOSTINGDATE BETWEEN
				DECODE(:GLMinDate,'',To_Date('01/01/1900 00:00:00','MM/DD/YYYY HH24:MI:SS'),To_Date('01/01/00', 'MM/DD/YY')) AND 
				DECODE(:GLMaxDate,'',To_Date('01/01/2099 23:59:59','MM/DD/YYYY HH24:MI:SS'),To_Date(:GLMaxDate, 'MM/DD/YY'))) AND
				GL_TRANSACTION_VIEW.GLACCOUNTID = GL_ACCOUNT.ID AND
				(GL_ACCOUNT.TYPE = '1' OR GL_ACCOUNT.TYPE = '2' OR GL_ACCOUNT.TYPE = '3')

			GROUP BY
				GL_TRANSACTION_VIEW.GLACCOUNTID,
				GL_TRANSACTION_VIEW.GLACCOUNTNAME,
				GL_TRANSACTION_VIEW.DEPTNAME,
				GL_TRANSACTION_VIEW.DIVISIONNAME,
				GL_TRANSACTION_VIEW.DEPTID,
				GL_TRANSACTION_VIEW.DIVIDX,
				GL_ACCOUNT.TYPE

			union all

			SELECT
				SUM (GL_TRANSACTION_VIEW.AMOUNT) AS AMOUNT,
				GL_TRANSACTION_VIEW.GLACCOUNTID,
				GL_TRANSACTION_VIEW.GLACCOUNTNAME,
				GL_TRANSACTION_VIEW.DEPTNAME,
				GL_TRANSACTION_VIEW.DIVISIONNAME,
				GL_TRANSACTION_VIEW.DEPTID,
				GL_TRANSACTION_VIEW.DIVIDX,
				DECODE (GL_ACCOUNT.TYPE, 4, 'I', 5, 'E', 6, 'E', ' ') As Type

			FROM
				GL_TRANSACTION_VIEW,
				GL_ACCOUNT

			WHERE
				(GL_TRANSACTION_VIEW.ACRUCASHTYPE = 'A') AND
				(GL_TRANSACTION_VIEW.GLPOSTINGDATE BETWEEN
				DECODE(:GLMinDate,'',To_Date('01/01/1900 00:00:00','MM/DD/YYYY HH24:MI:SS'),To_Date(:GLMinDate, 'MM/DD/YY')) AND 
				DECODE(:GLMaxDate,'',To_Date('01/01/2099 23:59:59','MM/DD/YYYY HH24:MI:SS'),To_Date(:GLMaxDate, 'MM/DD/YY'))) AND
				GL_TRANSACTION_VIEW.GLACCOUNTID = GL_ACCOUNT.ID AND
				(GL_ACCOUNT.TYPE = '4' OR GL_ACCOUNT.TYPE = '5' OR GL_ACCOUNT.TYPE = '6')

			GROUP BY
				GL_TRANSACTION_VIEW.GLACCOUNTID,
				GL_TRANSACTION_VIEW.GLACCOUNTNAME,
				GL_TRANSACTION_VIEW.DEPTNAME,
				GL_TRANSACTION_VIEW.DIVISIONNAME,
				GL_TRANSACTION_VIEW.DEPTID,
				GL_TRANSACTION_VIEW.DIVIDX,
				GL_ACCOUNT.TYPE");
            #endregion
            OracleDataAdapter OracleData = new OracleDataAdapter(reportCommand, OracleConn);
            OracleData.SelectCommand.BindByName = true;
            OracleData.SelectCommand.Parameters.Add(":GLMinDate", GLMinDate);
            OracleData.SelectCommand.Parameters.Add(":GLMaxDate", GLMaxDate);
            OracleData.Fill(ConsolidatedFinancialData);
            OracleData.Dispose();
            Misc.CleanUpOracle(OracleConn);

            //ConsolidatedFinancialData.WriteXml(@"C:\temp\GetRawConsolidatedFinancialData" + name.Replace("''", "'") + ".xls");

            //Add columns
            //ConsolidatedFinancialData.Tables[0].Columns.Add("CompanyKey",typeof(System.String),"'" + oracleReportNumber + "'");

            //ConsolidatedFinancialData.Tables[0].Columns.Add("CompanyName",typeof(System.String),"'" + name + "'");
            ConsolidatedFinancialData.Tables[0].Columns.Add(CreateColumnWithDefaultValue("CompanyName", System.Type.GetType("System.String"), name));

            return ConsolidatedFinancialData;
        }

        private DataColumn CreateColumnWithDefaultValue(string columnsName, Type columnType, object value)
        {
            System.Data.DataColumn ret = new System.Data.DataColumn(columnsName, columnType);
            ret.DefaultValue = value;
            return ret;
        }

        private string SetCommentLine()
        {
            StringBuilder comment = new StringBuilder();

            if (GLMinYear != "")
            {
                comment.Append(GLMinYear);
                comment.Append("-");
                comment.Append(GLMinWeek);
                comment.Append(" TO ");

                comment.Append(GLMaxYear);
                comment.Append("-");
                comment.Append(GLMaxWeek);
                comment.Append(" ");
            }
            else if (GLMinDate != "")
            {
                comment.Append(GLMinDate);
                comment.Append(" TO ");
                comment.Append(GLMaxDate);
                comment.Append(" ");
            }
            return comment.ToString();
        }

        public Company FindOrAddCompany(ArrayList list, string companyNameStr)
        {
            Company tempCompany = new Company(companyNameStr);
            int tempCompanyIdx = list.BinarySearch(tempCompany);

            if (tempCompanyIdx < 0)
                list.Insert(Math.Abs(tempCompanyIdx) - 1, tempCompany);
            else
                tempCompany = (Company)list[tempCompanyIdx];

            return tempCompany;
        }
        
        #region CBO
        public class Company : IComparable
        {
            #region Vars
            public string companyNameStr;
            public double calcDbl;
            #endregion

            #region Constructor
            public Company()
            {
                companyNameStr = string.Empty;
                calcDbl = 0;
            }
            public Company(string CompanyNameStr)
            {
                companyNameStr = CompanyNameStr;
                calcDbl = 0;
            }
            #endregion

            #region IComparable Members
            public int CompareTo(object obj)
            {
                int tempCompare;
                Company Y = (Company)obj;

                //Compare companyNameStr
                tempCompare = this.companyNameStr.CompareTo(Y.companyNameStr);
                if (tempCompare != 0)
                    return tempCompare;

                return 0;
            }
            #endregion
        }
        #endregion
    }
}
