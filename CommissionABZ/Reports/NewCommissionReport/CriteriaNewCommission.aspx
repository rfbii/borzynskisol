<%@ Page language="c#" Inherits="CommissionBZ.Reports.NewCommissionReport.CriteriaNewCommission" Codebehind="CriteriaNewCommission.aspx.cs" %>

<%@ Register Assembly="KeyCentralABZLib" Namespace="KeyCentralLib" TagPrefix="cc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>New Commission Report</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
				<table cellspacing="0" cellpadding="0" align="center">
					<tr height="20">
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center">Criteria New Commission Report</td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td align="center">
                            <cc2:DateCriteria ID="ShipDate" runat="server" />
                        </td>
                    </tr>
                </table>
				<table cellspacing="0" cellpadding="0" width="600" align="center">
					<tr>
						<td align="left" width="45%"><font face="Tahoma" size="2">Sales Person</font></td>
						<td align="left" width="10%">&nbsp;</td>
						<td align="left" width="45%"><font face="Tahoma" size="2">Sales Person to be included in the House sales</font></td>
					</tr>
					<tr>
						<td align="left" width="45%"><asp:listbox id="SalesPersonList" runat="server" SelectionMode="Single" Font-Names="Tahoma" Width="240px" Height="136px" TabIndex="4"></asp:listbox></td>
						<td valign="middle" align="center" width="10%">
							<table cellspacing="10">
								<tr>
									<td><asp:button id="addSalesPerson" runat="server" Width="75" Height="24" Text=">" TabIndex="5" onclick="addSalesPerson_Click"></asp:button></td>
								</tr>
								<tr>
									<td><asp:button id="deleteSalesPerson" runat="server" Width="75" Height="24" Text="<" TabIndex="6" onclick="deleteSalesPerson_Click"></asp:button></td>
								</tr>
							</table>
						</td>
						<td align="left" width="45%"><asp:listbox id="HouseSalesPersonList" runat="server" SelectionMode="Single" Font-Names="Tahoma" Width="240px" Height="136px" TabIndex="7"></asp:listbox></td>
					</tr>
					<tr>
						<td align="left" width="45%"><font face="Tahoma" size="2">Commodity</font></td>
						<td align="left" width="10%">&nbsp;</td>
						<td align="left" width="45%"><font face="Tahoma" size="2">Selected Commodity</font></td>
					</tr>
					<tr>
						<td align="left" width="45%"><asp:listbox id="CommodityList" runat="server" SelectionMode="Single" Font-Names="Tahoma" Width="240px" Height="136px" TabIndex="4"></asp:listbox></td>
						<td valign="middle" align="center" width="10%">
							<table cellspacing="10">
								<tr>
									<td><asp:button id="addCommodity" runat="server" Width="75" Height="24" Text=">" TabIndex="5" onclick="addCommodity_Click"></asp:button></td>
								</tr>
								<tr>
									<td><asp:button id="deleteCommodity" runat="server" Width="75" Height="24" Text="<" TabIndex="6" onclick="deleteCommodity_Click"></asp:button></td>
								</tr>
							</table>
						</td>
						<td align="left" width="45%"><asp:listbox id="SelectedCommodityList" runat="server" SelectionMode="Single" Font-Names="Tahoma" Width="240px" Height="136px" TabIndex="7"></asp:listbox></td>
					</tr>
					<tr>
						<td align="left" width="45%"><font face="Tahoma" size="2">Variety</font></td>
						<td align="left" width="10%">&nbsp;</td>
						<td align="left" width="45%"><font face="Tahoma" size="2">Selected Varieties</font></td>
					</tr>
					<tr>
						<td align="left" width="45%"><asp:listbox id="VarietyList" runat="server" SelectionMode="Multiple" Font-Names="Tahoma" Width="240px" Height="136px" TabIndex="12"></asp:listbox></td>
						<td valign="middle" align="center" width="10%">
							<table cellspacing="10">
								<tr>
									<td><asp:button id="addVariety" runat="server" Width="75" Height="24" Text=">" TabIndex="13" onclick="addVariety_Click"></asp:button></td>
								</tr>
								<tr>
									<td><asp:button id="deleteVariety" runat="server" Width="75" Height="24" Text="<" TabIndex="14" onclick="deleteVariety_Click"></asp:button></td>
								</tr>
							</table>
						</td>
						<td align="left" width="45%"><asp:listbox id="SelectedVarietyList" runat="server" SelectionMode="Multiple" Font-Names="Tahoma" Width="240px" Height="136px" TabIndex="15"></asp:listbox></td>
					</tr>
				</table>
				<table>
    				<tr height="10">
						<td>&nbsp;</td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" align="center">
				   <tr>
                     	<td align="left">
                            <asp:RadioButtonList ID="PaidRadioList" runat="server" onselectedindexchanged="PaidRadioList_SelectedIndexChanged" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="N">Not Paid</asp:ListItem>
                                <asp:ListItem Value="P">Paid</asp:ListItem>
                                <asp:ListItem Value="B">Both</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
					</tr>
                    <tr>
				        <td align="left">Outside Sale Commission Rate:&nbsp;<asp:textbox id="OutsideSaleCommissionRate" tabIndex="1" runat="server" Width="50px" Enabled="True" EnterAsTab="" STYLE="text-align:right;"></asp:textbox>&nbsp;<asp:comparevalidator id="CompareValidator1" runat="server" Font-Size="10" Font-Name="Tahoma" Operator="DataTypeCheck" Type="Double" Display="Dynamic" ErrorMessage="Invalid Outside Sale Commission Rate." ControlToValidate="OutsideSaleCommissionRate"></asp:comparevalidator></td>
				    </tr>
				    <tr>
				        <td align="left">Inside Sale Commission Rate:&nbsp;<asp:textbox id="InsideSaleCommissionRate" tabIndex="1" runat="server" Width="50px" Enabled="True" EnterAsTab="" STYLE="text-align:right;"></asp:textbox>&nbsp;<asp:comparevalidator id="CompareValidator2" runat="server" Font-Size="10" Font-Name="Tahoma" Operator="DataTypeCheck" Type="Double" Display="Dynamic" ErrorMessage="Invalid Inside Sale Commission Rate." ControlToValidate="InsideSaleCommissionRate"></asp:comparevalidator></td>
				    </tr>
				</table>
				<table cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td width="200"></td>
						<td align="center" width="180"><asp:button id="cmdReport" runat="server" Text="Run Report" EnterAsTab="" TabFinish="" onclick="cmdReport_Click"></asp:button></td>
						<td align="center" width="180"><asp:button id="cmdReturn" runat="server" Text="Return to Menu" EnterAsTab="" TabFinish="" onclick="cmdReturn_Click"></asp:button></td>
						<td width="200"></td>
					</tr>
				</table>
				<asp:label id="Message" runat="server" ForeColor="Red">Message</asp:label>
			</div>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
		</form>
	</body>
</html>