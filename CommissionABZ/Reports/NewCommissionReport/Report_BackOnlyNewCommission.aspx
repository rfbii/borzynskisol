<%@ Page EnableViewState="True" language="c#" Inherits="CommissionBZ.Reports.NewCommissionReport.Report_BackOnlyNewCommission" Codebehind="Report_BackOnlyNewCommission.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
        <!-- Email Start -->
		<title>New Commission Report</title>
		<style type="text/css">
		.Title { FONT-WEIGHT: bold; FONT-SIZE: 12pt; COLOR: black; FONT-FAMILY: Arial }
		.Title1 { FONT-WEIGHT: bold; FONT-SIZE: 9pt; FONT-FAMILY: Arial }
				.Title2 { FONT-WEIGHT: bold; FONT-SIZE: 9pt; FONT-FAMILY: Arial; BORDER-LEFT: #000000 1px solid; BORDER-BOTTOM: #000000 1px solid; BORDER-Right: #000000 1px solid}
		.HeaderStart { FONT-SIZE: 9pt; BORDER-LEFT: #000000 1px solid; BORDER-BOTTOM: #000000 1px solid; BORDER-TOP: #000000 1px solid; FONT-FAMILY: Arial }
		.Header { FONT-SIZE: 9pt;  BORDER-LEFT: #000000 1px solid; BORDER-BOTTOM: #000000 1px solid; BORDER-TOP: #000000 1px solid; FONT-FAMILY: Arial }
		.HeaderEnd { FONT-SIZE: 9pt;  BORDER-LEFT: #000000 1px solid; BORDER-BOTTOM: #000000 1px solid; BORDER-TOP: #000000 1px solid; BORDER-Right: #000000 1px solid; FONT-FAMILY: Arial }
		.DetailStart { FONT-SIZE: 9pt; BORDER-LEFT: #000000 1px solid; BORDER-BOTTOM: #000000 1px solid; FONT-FAMILY: Arial }
		.Detail { FONT-SIZE: 9pt;  BORDER-LEFT: #000000 1px solid; BORDER-BOTTOM: #000000 1px solid; FONT-FAMILY: Arial }
		.DetailEnd { FONT-SIZE: 9pt;  BORDER-LEFT: #000000 1px solid; BORDER-BOTTOM: #000000 1px solid; BORDER-Right: #000000 1px solid;FONT-FAMILY: Arial }
		.FooterStart {BORDER-TOP: #000000 thick double;FONT-WEIGHT: bold; FONT-SIZE: 9pt; BORDER-LEFT: #000000 1px solid;  BORDER-BOTTOM: #000000 1px solid; FONT-FAMILY: Arial }
		.Footer { BORDER-TOP: #000000 thick double;FONT-WEIGHT: bold;FONT-SIZE: 9pt;  BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: #000000 1px solid; FONT-FAMILY: Arial }
		.Footer1 { BORDER-TOP: #000000 thick double;FONT-WEIGHT: bold;FONT-SIZE: 9pt;  BORDER-BOTTOM: #000000 1px solid; FONT-FAMILY: Arial }
		.FooterEnd { BORDER-TOP: #000000 thick double;FONT-WEIGHT: bold;FONT-SIZE: 9pt;  BORDER-LEFT: lightgrey 1px solid; BORDER-BOTTOM: #000000 1px solid; BORDER-Right: #000000 1px solid;FONT-FAMILY: Arial }
		.Detail1 { FONT-SIZE: 9pt; FONT-FAMILY: Arial }
		</style>
        <!-- Email End -->
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="C#" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
        <!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body bgcolor="#ffffff" onload="SetupPrinting();">
		<object id="factory" style="DISPLAY: none" codebase="http://www.meadroid.com/scriptx/ScriptX.cab#Version=6,1,431,2"
			classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814" viewastext="">
		</object>
		<script type="text/javascript" language="javascript">
		function SetupPrinting()
		{
		    factory.printing.footer = "Printed: &d &t&bPage &p of &P&bNew Commission Report";
			factory.printing.header = "";
			factory.printing.leftMargin = .5;
			factory.printing.rightMargin = .5;
			factory.printing.topMargin = .5;
			factory.printing.bottomMargin = .5;
			factory.printing.portrait = false;
		}  
		</script>
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div id="BodyDiv"  class="scroll">
            <!-- Email Start -->
			    <table cellspacing="0" cellpadding="0" border="0" width="870">
				    <tr>
					    <td class="Title" align="center" width="870">New Commission Report</td>
				    </tr>
				    <tr>
					    <td class="Title1" align="center">
						    <div id="CommentLine" runat="server"></div>
					    </td>
				    </tr>
			    </table>
				<table width="870">
					<tr>
						<td align="center">
						    <asp:button id="MakeNewReport" EnterAsTab="" tabstop="" runat="server" Text="Make New Report" Width="150" OnClick="MakeNewReport_Click"></asp:button>
						    <%= SalesPersonsButtons() %>
						</td>
					</tr>
				</table>
				<%= Detail() %>
                <!-- Email End -->
			</div>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
		</form>
	</body>
</html>