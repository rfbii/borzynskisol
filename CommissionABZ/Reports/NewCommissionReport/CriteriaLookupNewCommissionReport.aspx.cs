using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using Oracle.DataAccess.Client;
using System.IO;
using KeyCentral.Functions;
using KeyCentral.Lookups;

namespace CommissionABZ.Reports.NewCommissionReport
{

	/// <summary>
	/// Summary description for CriteriaLookupNewCommissionReport.
	/// </summary>
	public partial class CriteriaLookupNewCommissionReport : BasePage
	{
		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Commission Reports") == false)
			{
				Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=");
			}

			if (!IsPostBack)
			{
				FillNewCommissionReportList();
			}
		}
		protected void NewCommissionReportList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			NewCommissionReportKey.Text = NewCommissionReportList.SelectedItem.Value;
			NewCommissionReportListText.Text = NewCommissionReportList.SelectedItem.Text;

			string ReportNumberStr = NewCommissionReportKey.Text;

			Message.Text = "";

			string saveFolderLocationStr = ConfigurationManager.AppSettings["UploadFolder"] + "CommissionABZ";

			FilesDiv.InnerHtml = String.Empty;
			LblPrint.Visible = true;

			//Display Pdf's on screen for printing
			DisplayFilesUploaded(saveFolderLocationStr);
		}
		protected void Return_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Commission/Reports");			
		}
		protected void back_Click(object sender, System.EventArgs e)
		{
			JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"]) + "?ReportName=LookupNewCommissionReport");
		}
		protected void cancel_Click(object sender, System.EventArgs e)
		{
			NewCommissionReportKey.Text = "";
			NewCommissionReportListText.Text = "";
			NewCommissionReportList.ClearSelection();
			NewCommissionReportList.Items.Clear();
			NewCommissionReportList.SelectedIndex = -1;

			FillNewCommissionReportList();

			Message.Text = "";

			FilesDiv.InnerHtml = String.Empty;
			LblPrint.Visible = false;
		}
        #endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Private helpers
		private void FillNewCommissionReportList()
		{
			ArrayList ret = new ArrayList();
			NewCommissionReportList.ClearSelection();
			NewCommissionReportList.Items.Clear();
			NewCommissionReportList.SelectedIndex = -1;

			SqlConnection Conn = Misc.GetSQLConn();

			#region SQL Code
			string sqlCommand = @"use Commission; 
			Select
				Report_Number,
				Report_Date
			From
				New_Commission_Report_Numbers";
			#endregion

			SqlCommand selectCMD = new SqlCommand(sqlCommand, Conn);
			SqlDataReader myReader = selectCMD.ExecuteReader();

			while (myReader.Read())
			{
				NewCommissionReportList.Items.Add(new ListItem(myReader.GetValue(0).ToString() + " - " + myReader.GetDateTime(1).ToShortDateString(), myReader.GetValue(0).ToString()));
			}

			SortListBox(NewCommissionReportList);

			myReader.Dispose();
			Misc.CleanUpSQL(Conn);
		}
		public static int TextIsInListBox(ListBox list, string textToFind, string valueToFind)
		{
			for (int i = 0; i < list.Items.Count; i++)
				if (list.Items[i].Text.Trim() == textToFind.Trim() && list.Items[i].Value.Trim() == valueToFind.Trim())
					return i;

			return -1;
		}
		public static string ParseTildaString(string stringToParse, int field)
		{
			string[] stringSplit = stringToParse.Split('~');

			if (stringSplit.Length <= field)
				return "";
			else
				return stringSplit[field];
		}
		private ListBox SortListBox(ListBox list)
		{
			// Sort list using bubblesort
			if (list.Items.Count > 0)
			{
				for (int i = list.Items.Count; i >= 0; i--)
				{
					for (int j = 0; j < (i - 1); j++)
					{
						if (String.Compare(list.Items[j].Text, list.Items[j + 1].Text, false) > 0)
						{
							ListItem tempListItem = new ListItem(list.Items[j].Text, list.Items[j].Value);
							list.Items[j].Text = list.Items[j + 1].Text;
							list.Items[j].Value = list.Items[j + 1].Value;
							list.Items[j + 1].Text = tempListItem.Text;
							list.Items[j + 1].Value = tempListItem.Value;
						}
					}
				}
			}

			return list;
		}
		private static ArrayList BuildArrayListFromListBox(ListBox list)
		{
			ArrayList listBoxArrayList = new ArrayList();

			if (list.Items.Count == 0)
				return listBoxArrayList;

			for (int i = 0; i < list.Items.Count; i++)
			{
				string key = list.Items[i].Value;
				string text = list.Items[i].Text;

				if (key != "" && text != "")
					listBoxArrayList.Add(new CriteriaBoxRecord(key, text, ""));
			}

			return listBoxArrayList;
		}
		private void DisplayFilesUploaded(string saveFolderLocationStr)
		{
			#region Get a list of all the PDF's that match the selected lots
			ArrayList fileNames = new ArrayList();

			if (Directory.Exists(saveFolderLocationStr))
			{
				string[] files = Directory.GetFiles(saveFolderLocationStr);

				foreach (string fullPath in files)
				{
					string[] fileParts = fullPath.Split('\\');
					string fileName = fileParts[fileParts.Length - 1];

					if (fileName.StartsWith(NewCommissionReportKey.Text + "_"))
					{
						fileNames.Add(fileName);
					}
				}
			}
			#endregion

			#region Display File Uploaded
			foreach (string fileName in fileNames)
				FilesDiv.InnerHtml += @"<a href='" + GetUrl(fileName) + "' target='_blank' style='white-space: nowrap'>" + fileName + @"</a>,</br>";

			//Remove the last comma
			if (FilesDiv.InnerHtml.Length > 10)
				FilesDiv.InnerHtml = FilesDiv.InnerHtml.Remove(FilesDiv.InnerHtml.Length - 13, 1);
			#endregion
		}
		public static string GetUrl(string fileName)
		{
			return @"../../../UploadedFiles/CommissionABZ/" + HttpUtility.UrlEncode(fileName).Replace("+", "%20");
		}
		#endregion
	}
}
