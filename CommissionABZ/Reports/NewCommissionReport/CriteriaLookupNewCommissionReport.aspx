<%@ Page language="c#" Inherits="CommissionABZ.Reports.NewCommissionReport.CriteriaLookupNewCommissionReport" Codebehind="CriteriaLookupNewCommissionReport.aspx.cs" %>
<%@ Register TagPrefix="cc1" Namespace="Cet" Assembly="Cet.ConfirmButton" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
  <head id="Head1" runat="server">
		<title>Lookup New Commission Report</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
  </head>
	<body bgcolor="white">
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
				<table width="830">
					<tbody>
					</tbody>
				</table>
				<table width="830">
					<tr>
						<td width="830" align="center">Lookup New Commission Report</td>
					</tr>
				</table>
				<table width="800">
					<tr>
					    <td align="left"><font face="Tahoma" size="2">New Commission Reports</font> 
					    </td>
					</tr>
					<tr>
					   <td align="left" width="800"><asp:listbox id="NewCommissionReportList" runat="server" SelectionMode="Single" Font-Names="Tahoma" Width="300px" Height="136px" TabIndex="1" AutoPostBack="True" onselectedindexchanged="NewCommissionReportList_SelectedIndexChanged"></asp:listbox></td>
					</tr>
					<tr>
					    <td align="left" width="800"><asp:textbox id="NewCommissionReportKey" runat="server" Width="100px" Height="19" Visible="False"></asp:textbox><asp:textbox id="NewCommissionReportListText" runat="server" Width="100px" Height="19" Visible="False"></asp:textbox></td>
					</tr>
				</table>
				<table width="830">
					<tr>
					    <td"><asp:Label Visible="False" Text="To print this open link:" runat="server" ID="LblPrint"></asp:Label></td>
				    </tr>
				    <tr>
					    <td><div runat="server" id="FilesDiv"></div></td>
				    </tr>
				</table>
				<table width="830">
					<tr>
						<td align="left" height="25"><asp:label id="Message" runat="server" ForeColor="Red"></asp:label></td>
					</tr>
				</table>
				<table width="830">
					<tr>
						<td align="right"><asp:button id="cancel" runat="server" Width="100px" Enabled="True" Text="Cancel" EnterAsTab="" tabIndex="2" onclick="cancel_Click" CausesValidation="false"></asp:button></td>
						<td align="left"><asp:button id="Return" tabIndex="3" runat="server" Width="160px" Text="Return to Menu" Height="24px" EnterAsTab="" tabstop="" onclick="Return_Click" CausesValidation="false"></asp:button></td>
					</tr>
				</table>
				<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
			</div>
		</form>
	</body>
</html>
