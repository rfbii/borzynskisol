using System;
using System.Collections;
using System.ComponentModel;
using System.Globalization;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Text;
using System.IO;
using KeyCentral.Functions;
using KeyCentral.Lookups;
using CommissionBZLib;

namespace CommissionBZ.Reports.CommissionReport
{
	public partial class ReportCommission : BasePage
	{
		#region Vars
		private double lineCount = 0;
		private bool newPage = true;
		private bool firstPage = true;
		CommissionReportBLL commissionReportBLL;
		private double GrandTotalQntDbl = 0;
		private double GrandTotalFOBQntDbl = 0;
		private double GrandTotalTotalFOBPerUnitDbl = 0;
		private double GrandTotalCommissionDbl = 0;
		private double GrandTotalCommissionPerUnitDbl = 0;
		private double GrandTotalFamousCommissionAmtDbl = 0;
		private double GrandTotalDifferenceDbl = 0;
		private double GrandTotalPaidDbl = 0;
		private double GrandTotalDueDbl = 0;
		#endregion

		#region Param Properties
		private string MinShipDate { get { return Session["MinShipDate"].ToString(); } }
		private string MaxShipDate { get { return Session["MaxShipDate"].ToString(); } }
		private string SalesPerson { get { return Session["SalesPerson"].ToString(); } }
        private ArrayList reportDataArl { get { return (ArrayList)Session["ReportDataArl"]; } }
		private ArrayList Commodities { get { return (ArrayList)Session["Commodities"]; } }
		private ArrayList Varieties { get { return (ArrayList)Session["Varieties"]; } }
		private ArrayList ExcludedCommodities { get { return (ArrayList)Session["ExcludedCommodities"]; } }
		private ArrayList ExcludedCustomers { get { return (ArrayList)Session["ExcludedCustomers"]; } }
		#endregion

		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Commission Reports") == false)
				Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx");

			JavaStatusMessage status = new JavaStatusMessage(this);
			status.ShowMessage("Computing");

			CommentLine.InnerHtml = SetCommentLine();

			commissionReportBLL = new CommissionReportBLL();
            ArrayList ReportDataArl = commissionReportBLL.GetData(Commodities, ExcludedCommodities, ExcludedCustomers, Varieties, SalesPerson, MinShipDate, MaxShipDate, this);
            Session.Add("ReportDataArl", ReportDataArl);
            this.Repeater1.DataSource = ReportDataArl;
            if (((ArrayList)this.Repeater1.DataSource).Count != 0)
			{
				Repeater1.DataBind();
			}
			else
				JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"]) + "&&Records=None");
		}
        protected void ExcelBtn_Click(object sender, EventArgs e)
        {
            GrandTotalQntDbl = 0;
		    GrandTotalFOBQntDbl = 0;
		    GrandTotalTotalFOBPerUnitDbl = 0;
		    GrandTotalCommissionDbl = 0;
		    GrandTotalCommissionPerUnitDbl = 0;
		    GrandTotalFamousCommissionAmtDbl = 0;
		    GrandTotalDifferenceDbl = 0;
		    GrandTotalPaidDbl = 0;
		    GrandTotalDueDbl = 0;
            #region Create Excel
            string TimeStampStr = DateTime.Now.ToString().Replace("/", "-").Replace("\\", "-").Replace(":", ".").Replace(" ", "_");
            string lineToWriteToFile = "";
            StreamWriter fileToWriteTo = null;
            fileToWriteTo = new StreamWriter(ConfigurationManager.AppSettings["UploadFolder"] + "CommissionABZ\\CommissionReport_" + TimeStampStr + @".csv");

            lineToWriteToFile = "Commission Report";
            fileToWriteTo.WriteLine(lineToWriteToFile);
            
            lineToWriteToFile = "Date, Load #, Customer, Item, Unit, Total FOB, FOB, Cmsn., /Unit, Fms., Diff, Paid, Due, CHK #, OSS";
            fileToWriteTo.WriteLine(lineToWriteToFile);

            for (int i = 0; i < reportDataArl.Count; i++)
            {
                CommissionItem commissionItem = (CommissionItem)reportDataArl[i];
                for (int j = 0; j < commissionItem.CommissionItemDetails.Count; j++)
                {
                    CommissionItem_Detail commissionItemDetail = (CommissionItem_Detail)commissionItem.CommissionItemDetails[j];
                    lineToWriteToFile = commissionItemDetail.ShipDateTime + "," + commissionItemDetail.SoNo + "," + commissionItemDetail.CustName.Replace(",","") + "," + commissionItemDetail.Commodity + "," + commissionItemDetail.Qnt + "," + commissionItemDetail.FOBQnt + "," + commissionItemDetail.TotalFOBPerUnit + "," + commissionItemDetail.Commission + "," + commissionItemDetail.CommissionPerUnit + ",,,,,," + commissionItemDetail.OutSideSale.ToUpper();
                    fileToWriteTo.WriteLine(lineToWriteToFile);
                }

                lineToWriteToFile = "Total," + commissionItem.SoNo + "," + commissionItem.CustomerName.Replace(",", "") + ",," + commissionItem.SumOfUnits + "," + commissionItem.SumOfFOBs + ",," + commissionItem.SumOfCommissions + ",," + commissionItem.FamousCommissionAmt + "," + commissionItem.Difference + "," + commissionItem.Paid + "," + commissionItem.Due + ",,";
                fileToWriteTo.WriteLine(lineToWriteToFile);

                GrandTotalQntDbl += commissionItem.SumOfUnits;
                GrandTotalFOBQntDbl += commissionItem.SumOfFOBs;

                if (GrandTotalQntDbl != 0)
                {
                    GrandTotalTotalFOBPerUnitDbl = GrandTotalFOBQntDbl / GrandTotalQntDbl;
                }

                GrandTotalCommissionDbl += commissionItem.SumOfCommissions;

                if (GrandTotalQntDbl != 0)
                {
                    GrandTotalCommissionPerUnitDbl = GrandTotalCommissionDbl / GrandTotalQntDbl;
                }

                GrandTotalFamousCommissionAmtDbl += commissionItem.FamousCommissionAmt;
                GrandTotalDifferenceDbl = GrandTotalCommissionDbl - GrandTotalFamousCommissionAmtDbl;
                GrandTotalPaidDbl += commissionItem.Paid;
                GrandTotalDueDbl = GrandTotalCommissionDbl - GrandTotalPaidDbl;
            }
            lineToWriteToFile = ",,,Grand Total," + GrandTotalQntDbl + "," + GrandTotalFOBQntDbl + "," + GrandTotalTotalFOBPerUnitDbl + "," + GrandTotalCommissionDbl + "," + GrandTotalCommissionPerUnitDbl + "," + GrandTotalFamousCommissionAmtDbl + "," + GrandTotalDifferenceDbl + "," + GrandTotalPaidDbl + "," + GrandTotalDueDbl + ",,";
            fileToWriteTo.WriteLine(lineToWriteToFile);
            if (fileToWriteTo != null)
            {
                fileToWriteTo.Close();
            }
            this.RegisterStartupScript("PopUpReport1", "<script>window.open('../../../UploadedFiles/" + "CommissionABZ/CommissionReport_" + TimeStampStr + @".csv','CSVReportWindow', 'directories=no,location=no,menubar=no,status=no,toolbar=no,scrollbars=no,resizable=yes');</script>");
            #endregion
        }
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

		#region Display Code
		public string CheckLineCount()
		{
			if (firstPage == true)
			{
				if (lineCount == 45)
				{
					newPage = true;
					firstPage = false;
					lineCount = 0;
				}
			}
			else if (firstPage == false)
			{
				if (lineCount == 46)
				{
					newPage = true;
					lineCount = 0;
				}
			}

			return "";
		}
		public string ColumnHeader(object oItem)
		{
			string columnHeader = "";
			if (newPage)
			{
				if (firstPage == false)
				{
					columnHeader += @"<!-- email end --></table>";

					if (disclaimer.DisclaimerList.Count != 0)
					{
						columnHeader += @"
                        <table border='0' cellPadding='0' cellspacing='0' width='960'>
	                        <tr>
		                        <td align=center width=100% nowrap>
                                    <font face='Tahoma' size='1.5'>" + disclaimer.DisclaimerList[0].ToString().Trim() + @"</font>
                                </td>
                            </tr>
                        </table>";
					}
					else
					{
						columnHeader += @"
					    <table border='0' cellPadding='0' cellspacing='0' width='960'>
						    <tr>
							    <td height='20'><font size='2' face='Arial'>&nbsp;</font></td>
						    </tr>
					    </table>";
					}

					columnHeader += @"
					<div style='page-break-before:always'><span style='visibility: hidden;'><font style='font-size:0pt;'>&nbsp;</font></span></div>
					<table border='0' cellPadding='0' cellspacing='0' width='960'>";
				}
				else
				{
					columnHeader += @"<table border='0' cellPadding='0' cellspacing='0' width='960'>";
				}

				columnHeader += @"
				<tr>
					<td class='HeaderStart' width='65' align='center'>Date</td>
					<td class='Header' width='65' align='center'>Load #</td>
					<td class='Header' width='100' align='center'>Customer</td>
					<td class='Header' width='100' align='center'>Item</td>
					<td class='Header' width='50' align='center'>Unit</td>
					<td class='Header' width='80' align='center'>Total FOB</td>
					<td class='Header' width='50' align='center'>FOB</td>
					<td class='Header' width='80' align='center'>Cmsn.</td>
					<td class='Header' width='50' align='center'>/Unit</td>
					<td class='Header' width='80' align='center'>Fms.</td>
					<td class='Header' width='50' align='center'>Diff</td>
					<td class='Header' width='50' align='center'>Paid</td>
					<td class='Header' width='50' align='center'>Due</td>
					<td class='Header' width='50' align='center'>CHK #</td>
					<td class='HeaderEnd' width='40' align='center'>OSS</td>
				</tr>";

				if (firstPage == false)
				{
					columnHeader = columnHeader + @"<!-- email start -->";
				}

				newPage = false;
			}

			return columnHeader;
		}
		public string Detail(object oItem)
		{
			string detail = "";

			CommissionItem commissionItem = (CommissionItem)((RepeaterItem)oItem).DataItem;

			#region Print Details
			for (int i = 0; i < commissionItem.CommissionItemDetails.Count; i++)
			{
				CommissionItem_Detail commissionItemDetail = (CommissionItem_Detail)commissionItem.CommissionItemDetails[i];

				string ShipDateDetail = commissionItemDetail.ShipDateTime.ToShortDateString();

				string LoadNumberDetail = "&nbsp;";

				if (commissionItemDetail.SoNo != "")
					LoadNumberDetail = StringFormater.FormatString(commissionItemDetail.SoNo, 10);

				string CustomerNameDetail = "&nbsp;";

				if (commissionItemDetail.CustName != "")
					CustomerNameDetail = StringFormater.FormatString(commissionItemDetail.CustName, 10);

				string CommodityDetail = "&nbsp;";

				if (commissionItemDetail.Commodity != "")
					CommodityDetail = StringFormater.FormatString(commissionItemDetail.Commodity, 10);

				string UnitDetail = Misc.DisplayFormat(commissionItemDetail.Qnt);
				string TotalFOBDetail = Misc.DisplayHTMLForCurency(commissionItemDetail.FOBQnt);
				string FOBDetail = Misc.DisplayHTMLForCurency(commissionItemDetail.TotalFOBPerUnit);
				string CommissionDetail = Misc.DisplayHTMLForCurency(commissionItemDetail.Commission);
				string CommissionPerUnitDetail = Misc.DisplayHTMLForCurency(commissionItemDetail.CommissionPerUnit);
				string OutSideSaleDetail = commissionItemDetail.OutSideSale.ToUpper();

				detail += @"
				<tr>
					<td class='DetailStart' width='65' align=left style='padding-left:1px;'>" + ShipDateDetail + @"</td>
					<td class='Detail' width='65' align=left style='padding-left:1px;'>" + LoadNumberDetail + @"</td>
					<td class='Detail' width='100' align=left style='padding-left:1px;'>" + CustomerNameDetail + @"</td>
					<td class='Detail' width='100' align=left style='padding-left:1px;'>" + CommodityDetail + @"</td>
					<td class='Detail' width='50' align=right style='padding-right:1px;'>" + UnitDetail + @"</td>
					<td class='Detail' width='80' align=right style='padding-right:1px;'>" + TotalFOBDetail + @"</td>
					<td class='Detail' width='50' align=right style='padding-right:1px;'>" + FOBDetail + @"</td>
					<td class='Detail' width='80' align=right style='padding-right:1px;'>" + CommissionDetail + @"</td>
					<td class='Detail' width='50' align=right style='padding-right:1px;'>" + CommissionPerUnitDetail + @"</td>
					<td class='Detail' width='80' align=right style='padding-right:1px;'>&nbsp;</td>
					<td class='Detail' width='50' align=right style='padding-right:1px;'>&nbsp;</td>
					<td class='Detail' width='50' align=right style='padding-right:1px;'>&nbsp;</td>
					<td class='Detail' width='50' align=right style='padding-right:1px;'>&nbsp;</td>
					<td class='Detail' width='50' align=left style='padding-left:1px;'>&nbsp;</td>
					<td class='DetailEnd' width='40' align=left style='padding-left:1px;'>" + OutSideSaleDetail + @"</td>
				</tr>";

				lineCount += 1;

				CheckLineCount();

				detail += ColumnHeader(commissionItem);
			}
			#endregion

			#region Print Total
			string LoadNumberItem = "&nbsp;";
			if (commissionItem.SoNo != "")
				LoadNumberItem = StringFormater.FormatString(commissionItem.SoNo, 10);

			string CustomerNameItem = "&nbsp;";
            if (commissionItem.CustomerName != "")
				CustomerNameItem = StringFormater.FormatString(commissionItem.CustomerName, 10);

			string UnitItem = Misc.DisplayFormat(commissionItem.SumOfUnits);
			string TotalFOBItem = Misc.DisplayHTMLForCurency(commissionItem.SumOfFOBs);
			string CommissionItem = Misc.DisplayHTMLForCurency(commissionItem.SumOfCommissions);
			string FamousCommissionItem = Misc.DisplayHTMLForCurency(commissionItem.FamousCommissionAmt);
			string DifferenceItem = Misc.DisplayHTMLForCurency(commissionItem.Difference);
			string PaidItem = Misc.DisplayHTMLForCurency(commissionItem.Paid);
			string DueItem = Misc.DisplayHTMLForCurency(commissionItem.Due);
			string CheckNumberItem = "&nbsp;";
			if (commissionItem.CheckNumber != "")
				CheckNumberItem = StringFormater.FormatString(commissionItem.CheckNumber, 10);

			detail += @"
			<tr>
				<td class='DetailStart' width='65' align=center><b>Total</b></td>
				<td class='Detail' width='65' align=left style='padding-left:1px;'>" + LoadNumberItem + @"</td>
				<td class='Detail' width='100' align=left style='padding-left:1px;'>" + CustomerNameItem + @"</td>
				<td class='Detail' width='100' align=center><b>Total</b></td>
				<td class='Detail' width='50' align=right style='padding-right:1px;'>" + UnitItem + @"</td>
				<td class='Detail' width='80' align=right style='padding-right:1px;'>" + TotalFOBItem + @"</td>
				<td class='Detail' width='50' align=right style='padding-right:1px;'>&nbsp;</td>
				<td class='Detail' width='80' align=right style='padding-right:1px;'>" + CommissionItem + @"</td>
				<td class='Detail' width='50' align=right style='padding-right:1px;'>&nbsp;</td>
				<td class='Detail' width='80' align=right style='padding-right:1px;'>" + FamousCommissionItem + @"</td>
				<td class='Detail' width='50' align=right style='padding-right:1px;'>" + DifferenceItem + @"</td>
				<td class='Detail' width='50' align=right style='padding-right:1px;'>" + PaidItem + @"</td>
				<td class='Detail' width='50' align=right style='padding-right:1px;'>" + DueItem + @"</td>
				<td class='Detail' width='50' align=left style='padding-left:1px;'>" + CheckNumberItem + @"</td>
				<td class='DetailEnd' width='40' align=left style='padding-left:1px;'>&nbsp;</td>
			</tr>";

			lineCount += 1;

			CheckLineCount();

			detail += ColumnHeader(commissionItem);

			GrandTotalQntDbl += commissionItem.SumOfUnits;
			GrandTotalFOBQntDbl += commissionItem.SumOfFOBs;

			if (GrandTotalQntDbl != 0)
			{
				GrandTotalTotalFOBPerUnitDbl = GrandTotalFOBQntDbl / GrandTotalQntDbl;
			}

			GrandTotalCommissionDbl += commissionItem.SumOfCommissions;

			if (GrandTotalQntDbl != 0)
			{
				GrandTotalCommissionPerUnitDbl = GrandTotalCommissionDbl / GrandTotalQntDbl;
			}

			GrandTotalFamousCommissionAmtDbl += commissionItem.FamousCommissionAmt;
			GrandTotalDifferenceDbl = GrandTotalCommissionDbl - GrandTotalFamousCommissionAmtDbl;
			GrandTotalPaidDbl += commissionItem.Paid;
			GrandTotalDueDbl = GrandTotalCommissionDbl - GrandTotalPaidDbl;
			#endregion

			return detail;
		}
		public string Total()
		{
			string totalStr = "";

			string GrandTotalQntStr = Misc.DisplayFormat(GrandTotalQntDbl).Replace("$", "");
			string GrandTotalFOBQntStr = Misc.DisplayHTMLForCurency(GrandTotalFOBQntDbl).Replace("$", "");
			string GrandTotalTotalFOBPerUnitStr = Misc.DisplayHTMLForCurency(GrandTotalTotalFOBPerUnitDbl);
			string GrandTotalCommissionStr = Misc.DisplayHTMLForCurency(GrandTotalCommissionDbl).Replace("$", "");
			string GrandTotalCommissionPerUnitStr = Misc.DisplayHTMLForCurency(GrandTotalCommissionPerUnitDbl);
			string GrandTotalFamousCommissionAmtStr = Misc.DisplayHTMLForCurency(GrandTotalFamousCommissionAmtDbl).Replace("$", "");
			string GrandTotalDifferenceStr = Misc.DisplayHTMLForCurency(GrandTotalDifferenceDbl).Replace("$", "");
			string GrandTotalPaidStr = Misc.DisplayHTMLForCurency(GrandTotalPaidDbl).Replace("$", "");
			string GrandTotalDueStr = Misc.DisplayHTMLForCurency(GrandTotalDueDbl).Replace("$", "");

			totalStr += @"
				<tr>
					<td class='DetailStart' width='330' colspan='4' align=center style='FONT-SIZE: 7pt;'><b>Grand Total</b></td>
					<td class='Detail' width='50' align=right style='padding-right:1px; FONT-SIZE: 7pt;'>" + GrandTotalQntStr + @"</td>
					<td class='Detail' width='80' align=right style='padding-right:1px; FONT-SIZE: 7pt;'>" + GrandTotalFOBQntStr + @"</td>
					<td class='Detail' width='50' align=right style='padding-right:1px; FONT-SIZE: 7pt;'>" + GrandTotalTotalFOBPerUnitStr + @"</td>
					<td class='Detail' width='80' align=right style='padding-right:1px; FONT-SIZE: 7pt;'>" + GrandTotalCommissionStr + @"</td>
					<td class='Detail' width='50' align=right style='padding-right:1px; FONT-SIZE: 7pt;'>" + GrandTotalCommissionPerUnitStr + @"</td>
					<td class='Detail' width='80' align=right style='padding-right:1px; FONT-SIZE: 7pt;'>" + GrandTotalFamousCommissionAmtStr + @"</td>
					<td class='Detail' width='50' align=right style='padding-right:1px; FONT-SIZE: 7pt;'>" + GrandTotalDifferenceStr + @"</td>
					<td class='Detail' width='50' align=right style='padding-right:1px; FONT-SIZE: 7pt;'>" + GrandTotalPaidStr + @"</td>
					<td class='Detail' width='50' align=right style='padding-right:1px; FONT-SIZE: 7pt;'>" + GrandTotalDueStr + @"</td>
					<td class='Detail' width='50' align=left style='padding-left:1px; FONT-SIZE: 7pt;'>&nbsp;</td>
					<td class='DetailEnd' width='40' align=left style='padding-left:1px; FONT-SIZE: 7pt;'>&nbsp;</td>
				</tr>
			</table>";

			return totalStr;
		}
		public string ReportFooter()
		{
			string display = "";

			if (disclaimer.DisclaimerListLandscape.Count != 0 && disclaimer.LineCountLandscape != 0)
			{
				display += @"<table cellspacing='0' cellpadding='0' width='960' class='ReportTable'>";

				for (int i = 1; i <= disclaimer.LineCountLandscape; i++)
				{
					display += @"
					<tr>
						<td align=left width=100% nowrap>
							<font face='Tahoma' size='2'>" + disclaimer.DisclaimerListLandscape[i].ToString().Trim() + @"</font>
						</td>
					</tr>";
				}

				display += @"</table>";
			}

			return display;
		}
		public string ReplaceWithZero(string str)
		{
			if (str == "")
				return "0";

			return str;
		}
		private string SetCommentLine()
		{
			StringBuilder comment = new StringBuilder();

			if (MinShipDate != "")
			{
				comment.Append(MinShipDate);
				comment.Append(" TO ");
				comment.Append(MaxShipDate);
				comment.Append(" ");
			}
			if (SalesPerson != "")
			{
				comment.Append(" SalesPerson: ");
				comment.Append(SalesPerson);
				comment.Append(" ");
			}

			if (comment.Length > 126)
			{
				lineCount += Math.Ceiling(Convert.ToDouble(comment.Length) / 126);
				lineCount--;
			}

			return comment.ToString();
		}
		#endregion

        
	}
}
