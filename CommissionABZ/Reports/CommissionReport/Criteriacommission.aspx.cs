using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KeyCentral.Functions;
using KeyCentral.Lookups;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;
using CommissionBZLib;

namespace CommissionBZ.Reports.CommissionReport
{
	/// <summary>
	/// Summary description for CriteriaCommission.
	/// </summary>
	/// 
	public partial class CriteriaCommission : BasePage
	{
		#region Vars
		protected ReportSessionManager reportSessionManager;
		private ArrayList ExcludedCommoditiesArrayList;
		private ArrayList ExcludedCustomersArrayList;
		#endregion

		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Commission Reports") == false)
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/MenuMain.aspx");
         	ExcludedCommoditiesArrayList = new ArrayList();
			ExcludedCustomersArrayList = new ArrayList();

			FillExcludedCommoditiesArrayList();
			FillExcludedCustomersArrayList();

            if (IsPostBack && Session["SalespersonSingle"] != null)
            {
                if (Session["SalespersonSingle"].ToString() != "")
                {
                    this.txtSalesPerson.Text = Misc.ParseTildaString(Session["SalespersonSingle"].ToString().Replace("!", "~"), 2);
                    this.salesperson_Key.Value = Misc.ParseTildaString(Session["SalespersonSingle"].ToString().Replace("!", "~"), 0) + "~" + Misc.ParseTildaString(Session["SalespersonSingle"].ToString().Replace("!", "~"), 1);
                }
            }

			if(!IsPostBack)
			{
				FillCommodityList();
				FillCommissionRates();

				Message.Text = "";

				//Check for returned error messages
				if (Request.QueryString["Records"] == "None")
					Message.Text = "No Records Found.";
			}

			ShipDate.Description = "Ship";

			#region SessionManager
			reportSessionManager = new ReportSessionManager(this,Request["ReportName"].ToString());
			
			//Load Vars from session if this report is in SessionManager
			if(IsPostBack ==false && reportSessionManager.CheckReportVars())
				LoadVarsFromReportManager();
			#endregion
		}
		protected void addCommodity_Click(object sender, System.EventArgs e)
		{
			if (this.SelectedCommodityList.Items.Count >= 1)
				Response.Write("<script>alert('You Can Select Only 1 Commodity.');</script>");
			else if (CommodityList.SelectedIndex != -1)
			{
				foreach (ListItem item in CommodityList.Items)
				{
					if (item.Selected)
					{
						SelectedCommodityList.Items.Add(new ListItem(item.Text, item.Value));
					}
				}

				SelectedCommodityList = SortListBox(SelectedCommodityList);

				FillCommodityList();

				VarietyList.Items.Clear();

				foreach (ListItem item in SelectedCommodityList.Items)
				{
					FillVarietyList(item.Value);
				}
			}
		}
		protected void deleteCommodity_Click(object sender, System.EventArgs e)
		{
			if (SelectedCommodityList.SelectedIndex != -1)
			{
				for (int i = 0; i < SelectedCommodityList.Items.Count; i++)
				{
					if (SelectedCommodityList.Items[i].Selected)
					{
						SelectedCommodityList.Items.Remove(SelectedCommodityList.SelectedItem);
						i--;
					}
				}

				SelectedCommodityList = SortListBox(SelectedCommodityList);

				FillCommodityList();

				VarietyList.Items.Clear();
				SelectedVarietyList.Items.Clear();

				foreach (ListItem item in SelectedCommodityList.Items)
				{
					FillVarietyList(item.Value);
				}
			}
		}
		protected void addVariety_Click(object sender, System.EventArgs e)
		{
			if (VarietyList.SelectedIndex != -1)
			{
				foreach (ListItem item in VarietyList.Items)
				{
					if (item.Selected)
					{
						SelectedVarietyList.Items.Add(new ListItem(item.Text, item.Value));
					}
				}

				SelectedVarietyList = SortListBox(SelectedVarietyList);

				VarietyList.Items.Clear();

				foreach (ListItem item in SelectedCommodityList.Items)
				{
					FillVarietyList(item.Value);
				}
			}
		}
		protected void deleteVariety_Click(object sender, System.EventArgs e)
		{
			if (SelectedVarietyList.SelectedIndex != -1)
			{
				for (int i = 0; i < SelectedVarietyList.Items.Count; i++)
				{
					if (SelectedVarietyList.Items[i].Selected)
					{
						SelectedVarietyList.Items.Remove(SelectedVarietyList.SelectedItem);
						i--;
					}
				}

				SelectedVarietyList = SortListBox(SelectedVarietyList);

				VarietyList.Items.Clear();

				foreach (ListItem item in SelectedCommodityList.Items)
				{
					FillVarietyList(item.Value);
				}
			}
		}
		protected void cmdReport_Click(object sender, System.EventArgs e)
		{
			#region Update Commission Rates
			if (OutsideSaleCommissionRate.Text != "" && InsideSaleCommissionRate.Text != "")
			{
				SqlConnection Conn = GetSQLConn();

				SqlCommand selectCMD = new SqlCommand("use Commission; select * from Commission_Rates", Conn);

				SqlDataReader myReader = selectCMD.ExecuteReader();

				if (myReader.Read())
				{
					myReader.Close();

					SqlCommand updateCMD = new SqlCommand("use Commission; update Commission_Rates set Outside_Commission_Rate=@Outside_Commission_Rate, Inside_Commission_Rate=@Inside_Commission_Rate", Conn);
					updateCMD.Parameters.Add("@Outside_Commission_Rate", OutsideSaleCommissionRate.Text);
					updateCMD.Parameters.Add("@Inside_Commission_Rate", InsideSaleCommissionRate.Text);

					updateCMD.ExecuteNonQuery();
					updateCMD.Dispose();
				}
				else
				{
					myReader.Close();

					SqlCommand insertCMD = new SqlCommand("use Commission; insert into Commission_Rates (Outside_Commission_Rate, Inside_Commission_Rate) values(@Outside_Commission_Rate, @Inside_Commission_Rate) Select @@Identity", Conn);
					insertCMD.Parameters.Add("@Outside_Commission_Rate", OutsideSaleCommissionRate.Text);
					insertCMD.Parameters.Add("@Inside_Commission_Rate", InsideSaleCommissionRate.Text);

					insertCMD.ExecuteScalar().ToString();
				}

				Misc.CleanUpSQL(Conn);
			}
			else
			{
				Message.Text = "The Commission Rates are not filled in.";

				return;
			}
			#endregion

			SaveVarsToReportMananger();

			//Build next page string and redirect to it
			Response.Redirect("ReportCommission.aspx" + "?LastPage=" + Misc.EncodeQueryString(Request.Path + "?ReportName=" + Request.QueryString["ReportName"]) + "&&ReportName=" + Request.QueryString["ReportName"]);
		}
		protected void cmdReturn_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Commission/Reports");
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
        #endregion

		#region Private helpers
		private void FillExcludedCommoditiesArrayList()
		{
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("use Commission; select Commodity_Key from [Excluded_Commodities]", Conn);

			SqlDataReader myReader = selectCMD.ExecuteReader();

			while (myReader.Read())
			{
				int commodityKey = Convert.ToInt32(myReader.GetValue(0).ToString());

				AddExcludedCommodity(ExcludedCommoditiesArrayList, commodityKey);
			}

			myReader.Close();
			Misc.CleanUpSQL(Conn);
		}
		private void FillExcludedCustomersArrayList()
		{
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("use Commission; select Customer_Key from [Excluded_Customers]", Conn);

			SqlDataReader myReader = selectCMD.ExecuteReader();

			while (myReader.Read())
			{
				int customerKey = Convert.ToInt32(myReader.GetValue(0).ToString());

				AddExcludedCustomer(ExcludedCustomersArrayList, customerKey);
			}

			myReader.Close();
			Misc.CleanUpSQL(Conn);
		}
		private void FillCommodityList()
		{
			CommodityList.ClearSelection();
			CommodityList.Items.Clear();
			CommodityList.SelectedIndex = -1;

			OracleConnection OracleConn = GetOracleConn();
			string oracleSQL = FormatOracle("Select Name, Cmtyidx From #COMPANY_NUMBER#.Ic_Ps_Commodity Order by Name");
			OracleCommand OracleCmd = new OracleCommand(oracleSQL, OracleConn);
			OracleCmd.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader = OracleCmd.ExecuteReader();

			while (myReader.Read())
			{
				//if commodity is not excluded, add to the listbox
				if (!FindExcludedCommodity(ExcludedCommoditiesArrayList, Convert.ToInt32(myReader.GetValue(1).ToString())))
				{
					if (SelectedCommodityList.Items.Count > 0)
					{
						int x = 0;
						x = TextIsInListBox(SelectedCommodityList, myReader.GetValue(0).ToString(), myReader.GetValue(1).ToString());

						if (x == -1)
							CommodityList.Items.Add(new ListItem(myReader.GetValue(0).ToString(), myReader.GetValue(1).ToString()));
					}
					else
						CommodityList.Items.Add(new ListItem(myReader.GetValue(0).ToString(), myReader.GetValue(1).ToString()));
				}
			}

			myReader.Close();
			myReader.Dispose();
			OracleCmd.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();
		}
		private void FillVarietyList(string commodityIndex)
		{
			VarietyList.ClearSelection();
			VarietyList.SelectedIndex = -1;

			OracleConnection OracleConn = GetOracleConn();
			string oracleCommand = "Select Ic_Ps_Commodity.Name as Commodity, Ic_Ps_Variety.Name as Variety, Ic_Ps_Variety.CmtyIdx as CommodityKey, Ic_Ps_Variety.VarietyIdx as VarietyKey, Ic_Ps_Variety.CmtyIdx || '~' || Ic_Ps_Variety.VarietyIdx || '!' || Ic_Ps_Commodity.Name || ' ' || Ic_Ps_Variety.Name as Keys from #COMPANY_NUMBER#.Ic_Ps_Commodity ,#COMPANY_NUMBER#.Ic_Ps_Variety where Ic_Ps_Commodity.CmtyIdx = Ic_Ps_Variety.CmtyIdx and Ic_Ps_Variety.CmtyIdx = '" + commodityIndex + "' order by 1,2";
			OracleCommand OracleData = new OracleCommand(FormatOracle(oracleCommand), OracleConn);

			OracleData.BindByName = true;
			Oracle.DataAccess.Client.OracleDataReader myReader = OracleData.ExecuteReader();


			while (myReader.Read())
			{
				int x = 0;
				x = TextIsInListBox(VarietyList, myReader.GetString(1), myReader.GetValue(3).ToString());

				if (x == -1)
				{
					x = 0;
					x = TextIsInListBox(SelectedVarietyList, myReader.GetString(1), myReader.GetValue(3).ToString());
					if (x == -1)
					{
						VarietyList.Items.Add(new ListItem(myReader.GetString(1), myReader.GetValue(3).ToString()));
					}
				}
			}

			myReader.Close();
			myReader.Dispose();
			OracleData.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();
		}
		private void AddExcludedCommodity(ArrayList list, int CommodityKey)
		{
			ExcludedCommodity tempExcludedCommodity = new ExcludedCommodity(CommodityKey);
			int tempExcludedCommodityIdx = list.BinarySearch(tempExcludedCommodity);

			if (tempExcludedCommodityIdx < 0)
				list.Insert(Math.Abs(tempExcludedCommodityIdx) - 1, tempExcludedCommodity);
		}
		private bool FindExcludedCommodity(ArrayList list, int CommodityKey)
		{
			bool found = false;

			ExcludedCommodity tempExcludedCommodity = new ExcludedCommodity(CommodityKey);
			int tempExcludedCommodityIdx = list.BinarySearch(tempExcludedCommodity);

			if (tempExcludedCommodityIdx >= 0)
				found = true;

			return found;
		}
		private void AddExcludedCustomer(ArrayList list, int CustomerKey)
		{
			ExcludedCustomer tempExcludedCustomer = new ExcludedCustomer(CustomerKey);
			int tempExcludedCustomerIdx = list.BinarySearch(tempExcludedCustomer);

			if (tempExcludedCustomerIdx < 0)
				list.Insert(Math.Abs(tempExcludedCustomerIdx) - 1, tempExcludedCustomer);
		}
		private bool FindExcludedCustomer(ArrayList list, int CustomerKey)
		{
			bool found = false;

			ExcludedCustomer tempExcludedCustomer = new ExcludedCustomer(CustomerKey);
			int tempExcludedCustomerIdx = list.BinarySearch(tempExcludedCustomer);

			if (tempExcludedCustomerIdx >= 0)
				found = true;

			return found;
		}
		private void FillCommissionRates()
		{
			OutsideSaleCommissionRate.Text = "";
			InsideSaleCommissionRate.Text = "";

			SqlConnection Conn = GetSQLConn();

			SqlCommand selectCMD = new SqlCommand("use Commission; select Outside_Commission_Rate, Inside_Commission_Rate from Commission_Rates", Conn);

			SqlDataReader myReader = selectCMD.ExecuteReader();

			if (myReader.Read())
			{
				OutsideSaleCommissionRate.Text = myReader.GetValue(0).ToString();
				InsideSaleCommissionRate.Text = myReader.GetValue(1).ToString();
			}

			myReader.Close();

			Misc.CleanUpSQL(Conn);
		}
		public static int TextIsInListBox(ListBox list, string textToFind, string valueToFind)
		{
			for (int i = 0; i < list.Items.Count; i++)
				if (list.Items[i].Text.Trim() == textToFind.Trim() && list.Items[i].Value.Trim() == valueToFind.Trim())
					return i;

			return -1;
		}
		public static string ParseTildaString(string stringToParse, int field)
		{
			string[] stringSplit = stringToParse.Split('~');

			if (stringSplit.Length <= field)
				return "";
			else
				return stringSplit[field];
		}
		private ListBox SortListBox(ListBox list)
		{
			// Sort list using bubblesort
			if (list.Items.Count > 0)
			{
				for (int i = list.Items.Count; i >= 0; i--)
				{
					for (int j = 0; j < (i - 1); j++)
					{
						if (String.Compare(list.Items[j].Text, list.Items[j + 1].Text, false) > 0)
						{
							ListItem tempListItem = new ListItem(list.Items[j].Text, list.Items[j].Value);
							list.Items[j].Text = list.Items[j + 1].Text;
							list.Items[j].Value = list.Items[j + 1].Value;
							list.Items[j + 1].Text = tempListItem.Text;
							list.Items[j + 1].Value = tempListItem.Value;
						}
					}
				}
			}

			return list;
		}
		private static ArrayList BuildArrayListFromListBox(ListBox list)
		{
			ArrayList listBoxArrayList = new ArrayList();

			if (list.Items.Count == 0)
				return listBoxArrayList;

			for (int i = 0; i < list.Items.Count; i++)
			{
				string key = list.Items[i].Value;

				//If this a a combined key, take the second
				if (key.IndexOf("~") != -1)
					key = Misc.ParseTildaString(key, 1);

				if (list.Items[i].Text != "" && list.Items[i].Value != "")
					listBoxArrayList.Add(new CriteriaBoxRecord(key, list.Items[i].Text, ""));
			}

			return listBoxArrayList;
		}
		#endregion

		#region Private Session Manager Functions
		private void LoadVarsFromReportManager()
		{
			ShipDate.MinDate = Session["MinShipDate"].ToString();
			ShipDate.MaxDate = Session["MaxShipDate"].ToString();
			txtSalesPerson.Text = Session["SalesPerson"].ToString();

			//Fill the Commodity and Selected Commodity listboxes
			CommodityList.Items.Clear();
			SelectedCommodityList.Items.Clear();
			ArrayList SelectedCommodities = (ArrayList)Session["Commodities"];
			for (int i = 0; i < SelectedCommodities.Count; i++)
			{
				//if commodity is not excluded, add to the listbox
				if (!FindExcludedCommodity(ExcludedCommoditiesArrayList, Convert.ToInt32(((CriteriaBoxRecord)SelectedCommodities[i]).key)))
				{
					SelectedCommodityList.Items.Add(new ListItem(((CriteriaBoxRecord)SelectedCommodities[i]).text, ((CriteriaBoxRecord)SelectedCommodities[i]).key));
				}
			}

			FillCommodityList();

			VarietyList.Items.Clear();

			foreach (ListItem item in SelectedCommodityList.Items)
			{
				FillVarietyList(item.Value);
			}

			//Fill the Variety and Selected Variety listboxes
			if (VarietyList.Items.Count > 0)
			{
				ArrayList SelectedVarieties = (ArrayList)Session["Varieties"];
				for (int i = 0; i < SelectedVarieties.Count; i++)
					SelectedVarietyList.Items.Add(new ListItem(((CriteriaBoxRecord)SelectedVarieties[i]).text, ((CriteriaBoxRecord)SelectedVarieties[i]).key));

				if (SelectedVarietyList.Items.Count > 0)
				{
					SelectedVarietyList = SortListBox(SelectedVarietyList);

					VarietyList.Items.Clear();

					foreach (ListItem item in SelectedCommodityList.Items)
					{
						FillVarietyList(item.Value);
					}
				}
			}
		}
		private void SaveVarsToReportMananger()
		{
			//Add Report Vars
			reportSessionManager.AddSession("MinShipDate", ShipDate.MinDate);
			reportSessionManager.AddSession("MaxShipDate", ShipDate.MaxDate);
			reportSessionManager.AddSession("SalesPerson", txtSalesPerson.Text);
			reportSessionManager.AddSession("ExcludedCommodities", ExcludedCommoditiesArrayList);
			reportSessionManager.AddSession("ExcludedCustomers", ExcludedCustomersArrayList);

			if (SelectedCommodityList.Items.Count == 0)
				reportSessionManager.AddSession("Commodities", new ArrayList());
			else
				reportSessionManager.AddSession("Commodities", BuildArrayListFromListBox(SelectedCommodityList));

			if (SelectedVarietyList.Items.Count == 0)
				reportSessionManager.AddSession("Varieties", new ArrayList());
			else
				reportSessionManager.AddSession("Varieties", BuildArrayListFromListBox(SelectedVarietyList));

			//Let the reportSessionManager know that we are done
			reportSessionManager.SaveSessions();
		}
		#endregion
	}
}
