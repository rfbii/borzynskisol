using System;
using System.Collections;
using System.ComponentModel;
using System.Globalization;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using KeyCentral.Functions;
using KeyCentral.Lookups;
using CommissionBZLib;

namespace CommissionBZ.Reports.PendingCommissionReport
{
	public partial class Report_BackOnlyPendingCommission : BasePage
	{
		#region Vars
		private double lineCount = 0;
		private bool newPage = true;
		private bool firstPage = true;
		private CommissionReportBLL commissionReportBLL;
		private ReportSessionManager reportSessionManager;
		#endregion

		#region Param Properties
		private string MinShipDate { get { return Session["MinShipDate"].ToString(); } }
		private string MaxShipDate { get { return Session["MaxShipDate"].ToString(); } }
		private string PaidStr { get { return Session["PaidRadioListValue"].ToString(); } }
		private ArrayList SalesPersonArl { get { return (ArrayList)Session["SalesPerson"]; } }
		private ArrayList Commodities { get { return (ArrayList)Session["Commodities"]; } }
		private ArrayList Varieties { get { return (ArrayList)Session["Varieties"]; } }
		private ArrayList ExcludedCommodities { get { return (ArrayList)Session["ExcludedCommodities"]; } }
		private ArrayList ExcludedCustomers { get { return (ArrayList)Session["ExcludedCustomers"]; } }
		private ArrayList InsideSalesArrayList { get { return (ArrayList)Session["InsideSalesArrayList"]; } }
		private ArrayList OutsideSalesArrayList { get { return (ArrayList)Session["OutsideSalesArrayList"]; } }
		private CommissionReportBLL CommissionReportDataBLL { get { return (CommissionReportBLL)Session["CommissionReportDataBLL"]; } }
		#endregion

		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Commission Reports") == false)
				Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx");

			reportSessionManager = new ReportSessionManager(this, Request.QueryString["ReportName"]);

			if (!IsPostBack)
			{
				JavaStatusMessage status = new JavaStatusMessage(this);
				status.ShowMessage("Computing");

				CommentLine.InnerHtml = SetCommentLine();

				commissionReportBLL = new CommissionReportBLL();

				commissionReportBLL.GetNewPendingData(Commodities, ExcludedCommodities, ExcludedCustomers, Varieties, SalesPersonArl, MinShipDate, MaxShipDate, this, true, PaidStr);

				if (commissionReportBLL.CommissionOrderArl.Count != 0)
				{
					reportSessionManager.AddSession("CommisionOrderArrayList", commissionReportBLL.CommissionOrderArl);
					reportSessionManager.AddSession("CommissionReportDataBLL", commissionReportBLL);
					reportSessionManager.SaveSessions();
				}
				else
					JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"]) + "&&Records=None");
			}
		}
		protected void MakePendingReport_Click(object sender, EventArgs e)
		{
			JavaStatusMessage status = new JavaStatusMessage(this);
			status.ShowMessage("Saving");

			#region Save PDF
			string saveFolderLocationStr = "";

			saveFolderLocationStr = ConfigurationManager.AppSettings["UploadFolder"] + "CommissionABZ";

			if (saveFolderLocationStr != "")
			{
				#region Entire report
				string userKeyStr = Session["User_Key"].ToString();
				string timeStampStr = DateTime.Now.ToString().Replace("/", "-").Replace("\\", "-").Replace(":", ".");
				//string fileNameStr = reportNumberStr + "_Main_PendingCommissionReport_" + userKeyStr + "_" + timeStampStr + ".pdf";
				string fileNameStr = "Main_PendingCommissionReport_" + userKeyStr + "_" + timeStampStr + ".pdf";

				CommissionReportPDF commissionReportPDF = new CommissionReportPDF();
				commissionReportPDF.Main(saveFolderLocationStr, fileNameStr, CommissionReportDataBLL, "", "", SetCommentLine(), true);

				this.RegisterStartupScript("PopUpReport", "<script>window.open('../../../UploadedFiles/CommissionABZ/" + fileNameStr + "','PDFReportWindow', 'directories=no,location=no,menubar=no,status=no,toolbar=no,scrollbars=no,resizable=yes');</script>");
				#endregion
			}
			#endregion
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

        #region Display Code
		
        public string CheckLineCount()
		{
			if (firstPage == true)
			{
				if (lineCount == 45)
				{
					newPage = true;
					firstPage = false;
					lineCount = 0;
				}
			}
			else if (firstPage == false)
			{
				if (lineCount == 46)
				{
					newPage = true;
					lineCount = 0;
				}
			}

			return "";
		}
		
        public string ColumnHeader()
		{
			string columnHeader = "";

			if (newPage)
			{
				if (firstPage == false)
				{
					columnHeader += @"<!-- email end --></table>";

					if (disclaimer.DisclaimerList.Count != 0)
					{
						columnHeader += @"
                        <table border='0' cellPadding='0' cellspacing='0' width='870'>
	                        <tr>
		                        <td align=center width=100% nowrap>
                                    <font face='Tahoma' size='1.5'>" + disclaimer.DisclaimerList[0].ToString().Trim() + @"</font>
                                </td>
                            </tr>
                        </table>";
					}
					else
					{
						columnHeader += @"
					    <table border='0' cellPadding='0' cellspacing='0' width='870'>
						    <tr>
							    <td height='20'><font size='2' face='Arial'>&nbsp;</font></td>
						    </tr>
					    </table>";
					}

					columnHeader += @"
					<div style='page-break-before:always'><span style='visibility: hidden;'><font style='font-size:0pt;'>&nbsp;</font></span></div>
					<table border='0' cellPadding='0' cellspacing='0' width='870'>";
				}
				else
				{
					columnHeader += @"<table border='0' cellPadding='0' cellspacing='0' width='870'>";
				}

				columnHeader += @"
				<tr>
					<td class='HeaderStart' width='65' align='center'>Date</td>
					<td class='Header' width='65' align='center'>Load #</td>
					<td class='Header' width='100' align='center'>Customer</td>
					<td class='Header' width='100' align='center'>Item</td>
					<td class='Header' width='50' align='center'>Unit</td>
					<td class='Header' width='90' align='center'>Total FOB</td>
					<td class='Header' width='50' align='center'>FOB</td>
					<td class='Header' width='90' align='center'>Cmsn.</td>
					<td class='Header' width='50' align='center'>/Unit</td>
					<td class='Header' width='60' align='center'>Paid</td>
					<td class='Header' width='60' align='center'>Due</td>
					<td class='Header' width='50' align='center'>RPT #</td>
					<td class='HeaderEnd' width='40' align='center'>OSS</td>
				</tr>";

				if (firstPage == false)
				{
					columnHeader = columnHeader + @"<!-- email start -->";
				}

				newPage = false;
			}

			return columnHeader;
		}
		
        public string Detail()
		{ // beginning of overall Detail           
            #region Overall Vars
            string detail = "";
            string SubTotalInQntStr = "";
            string SubTotalInFOBStr = "";
            string SubTotalInCommissionAmtStr = "";
            string SubTotalInNetStr = "";
            string SubTotalInPaidStr = "";
            double SubTotalInQntDbl = 0;
            double SubTotalInFOBDbl = 0;
            double SubTotalInAllFOBPerQntDbl = 0;
            double SubTotalInCommissionAmtDbl = 0;
            double SubTotalInCommissionAmtPerQntDbl = 0;
            double SubTotalInPaidDbl = 0;
            //double SubTotalInCurrentDbl = 0;
            double SubTotalInNetDbl = 0;
            string SubTotalOutQntStr = "";
            string SubTotalOutFOBStr = "";
            //string SubTotalOutAllFOBPerQntStr = "";
            string SubTotalOutCommissionAmtStr = "";
            //string SubTotalOutCommissionAmtPerQntStr = "";
            string SubTotalOutPaidStr = "";
            //string SubTotalOutCurrentStr = "";
            string SubTotalOutNetStr = "";
            double SubTotalOutQntDbl = 0;
            double SubTotalOutFOBDbl = 0;
            double SubTotalOutAllFOBPerQntDbl = 0;
            double SubTotalOutCommissionAmtDbl = 0;
            double SubTotalOutCommissionAmtPerQntDbl = 0;
            double SubTotalOutPaidDbl = 0;
            //double SubTotalOutCurrentDbl = 0;
            double SubTotalOutNetDbl = 0; 
        
           #endregion

                if (CommissionReportDataBLL.CommissionOrderArl.Count != 0)
                {
                    SubTotalInQntDbl = 0;
                    SubTotalInFOBDbl = 0;
                    SubTotalInAllFOBPerQntDbl = 0;
                    SubTotalInCommissionAmtDbl = 0;
                    SubTotalInCommissionAmtPerQntDbl = 0;
                    SubTotalInPaidDbl = 0;
                    SubTotalInNetDbl = 0;
                    
                    //Loop for each Saleman for inside sales.
                    bool firstTimeBln = true;
                    for (int m = 0; m < CommissionReportDataBLL.SalesPersonArl.Count; m++)
                    {
                        SubTotalOutQntDbl = 0;
                        SubTotalOutFOBDbl = 0;
                        SubTotalOutAllFOBPerQntDbl = 0;
                        SubTotalOutCommissionAmtDbl = 0;
                        SubTotalOutCommissionAmtPerQntDbl = 0;
                        SubTotalOutPaidDbl = 0;
                        SubTotalOutNetDbl = 0;
                        SalesPerson tempSalesman = (SalesPerson)CommissionReportDataBLL.SalesPersonArl[m];

                        if (firstTimeBln)
                        {
                        #region Inside Sales

                            firstTimeBln = false;
                            //For loop for Commission Orders
                            for (int n = 0; n < CommissionReportDataBLL.CommissionOrderArl.Count; n++)
                            {
                                Commission_Order tempCommissionOrder = (Commission_Order)CommissionReportDataBLL.CommissionOrderArl[n];
                                if (tempSalesman.SalesPersonKeyStr == tempCommissionOrder.SalesmanInt.ToString() && (tempCommissionOrder.InCurrentDbl != 0 || tempCommissionOrder.InNetDbl != 0 || tempCommissionOrder.InPaidDbl != 0))
                                {
                                    detail += ColumnHeader();
                                    double totalInOrderQnt = 0;
                                    double totalFOBAmt = 0;

                                    string LoadNumberDetail = "&nbsp;";
                                    string CustomerNameDetail = "&nbsp;";

                                    #region Print Details
                                    for (int s = 0; s < tempCommissionOrder.CommissionOrderDetailArl.Count; s++)
                                    {
                                        //CommissionTable tempCommissionTable = new CommissionTable();
                                        Commission_Order_Detail tempCommissionOrderDetail = (Commission_Order_Detail)tempCommissionOrder.CommissionOrderDetailArl[s];
                                        if (tempCommissionOrderDetail.InBln)
                                        {

                                            string ShipDateDetail = Misc.FormatDateTime(tempCommissionOrderDetail.DateStr);

                                            LoadNumberDetail = "&nbsp;";

                                            if (tempCommissionOrderDetail.LoadStr != "")
                                                LoadNumberDetail = StringFormater.FormatString(tempCommissionOrderDetail.LoadStr, 10);

                                            CustomerNameDetail = "&nbsp;";

                                            if (tempCommissionOrderDetail.CustomerStr != "")
                                                CustomerNameDetail = StringFormater.FormatString(tempCommissionOrderDetail.CustomerStr, 10);

                                            string ItemDetail = "&nbsp;";

                                            if (tempCommissionOrderDetail.ItemStr != "")
                                                ItemDetail = StringFormater.FormatString(tempCommissionOrderDetail.ItemStr, 10);

                                            string UnitDetail = Misc.DisplayFormat(tempCommissionOrderDetail.QntDbl);
                                            totalInOrderQnt += tempCommissionOrderDetail.QntDbl;
                                            string TotalFOBDetail = Misc.DisplayHTMLForCurency(tempCommissionOrderDetail.FOBDbl);
                                            string FOBDetail = Misc.DisplayHTMLForCurency(tempCommissionOrderDetail.FOBDbl / tempCommissionOrderDetail.QntDbl);
                                            totalFOBAmt += tempCommissionOrderDetail.FOBDbl;
                                            string CommissionDetail = Misc.DisplayHTMLForCurency(tempCommissionOrderDetail.FOBDbl * CommissionReportDataBLL.InsideSaleCommissionRateDbl);
                                            string CommissionPerUnitDetail = Misc.DisplayHTMLForCurency((tempCommissionOrderDetail.FOBDbl * CommissionReportDataBLL.InsideSaleCommissionRateDbl) / tempCommissionOrderDetail.QntDbl);
                                            string OutSideSaleDetail = "N";

                                            detail += @"
							<tr>
								<td class='DetailStart' width='65' align=left style='padding-left:1px;'>" + ShipDateDetail + @"</td>
								<td class='Detail' width='65' align=left style='padding-left:1px;'>" + LoadNumberDetail + @"</td>
								<td class='Detail' width='100' align=left style='padding-left:1px;'>" + CustomerNameDetail + @"</td>
								<td class='Detail' width='100' align=left style='padding-left:1px;'>" + ItemDetail + @"</td>
								<td class='Detail' width='50' align=right style='padding-right:1px;'>" + UnitDetail + @"</td>
								<td class='Detail' width='90' align=right style='padding-right:1px;'>" + TotalFOBDetail + @"</td>
								<td class='Detail' width='50' align=right style='padding-right:1px;'>" + FOBDetail + @"</td>
								<td class='Detail' width='90' align=right style='padding-right:1px;'>" + CommissionDetail + @"</td>
								<td class='Detail' width='50' align=right style='padding-right:1px;'>" + CommissionPerUnitDetail + @"</td>
								<td class='Detail' width='60' align=right style='padding-right:1px;'>&nbsp;</td>
								<td class='Detail' width='60' align=right style='padding-right:1px;'>&nbsp;</td>
								<td class='Detail' width='50' align=left style='padding-left:1px;'>&nbsp;</td>
								<td class='DetailEnd' width='40' align=left style='padding-left:1px;'>" + OutSideSaleDetail + @"</td>
							</tr>";

                                            //lineCount += 1;

                                            //CheckLineCount();

                                            //detail += ColumnHeader();
                                        }//end on inside Sales If
                                    }//end of Order Detail loop
                                    #endregion

                                    #region Print Total

                                    string LoadNumberStr = LoadNumberDetail;

                                    string CustomerNameStr = CustomerNameDetail;

                                    SubTotalInQntStr = Misc.DisplayFormat(totalInOrderQnt);
                                    SubTotalInFOBStr = Misc.DisplayHTMLForCurency(totalFOBAmt);
                                    SubTotalInCommissionAmtStr = Misc.DisplayHTMLForCurency(tempCommissionOrder.InCurrentDbl);
                                    SubTotalInPaidStr = Misc.DisplayHTMLForCurency(tempCommissionOrder.InPaidDbl);
                                    SubTotalInNetStr = Misc.DisplayHTMLForCurency(tempCommissionOrder.InNetDbl);
                                    //Todo get report number
                                    string ReportNumberItem = "&nbsp;";

                                    if (tempCommissionOrder.CommissionPaidDetailArl.Count != 0)
                                    {
                                        CommissionPaidDetails tempPaidDetails = (CommissionPaidDetails)tempCommissionOrder.CommissionPaidDetailArl[0];
                                            if (tempPaidDetails.InPaid != 0)
                                            {
                                                ReportNumberItem = Convert.ToString(tempPaidDetails.ReportNo);
                                            }
                                        
                                        #region print Paid detail not used now
                                        //                                    else
                                        //                                        for (int t = 0; t < tempCommissionOrder.CommissionPaidDetailArl.Count; t++)
                                        //                                        {
                                        //                                            CommissionPaidDetails tempPaidDetails = (CommissionPaidDetails)tempCommissionOrder.CommissionPaidDetailArl[t];

                                        //                                            if (tempPaidDetails.InPaid != 0)
                                        //                                            {
                                        //                                                detail += @"
                                        //							                <tr>
                                        //								                <td class='DetailStart' width='65' align=center><b>Paid</b></td>
                                        //								                <td class='Detail' width='65' align=left style='padding-left:1px;'>&nbsp;</td>
                                        //								                <td class='Detail' width='100' align=left style='padding-left:1px;'>&nbsp;</td>
                                        //								                <td class='Detail' width='100' align=center><b>Paid</b></td>
                                        //								                <td class='Detail' width='50' align=right style='padding-right:1px;'>&nbsp;</td>
                                        //								                <td class='Detail' width='90' align=right style='padding-right:1px;'>&nbsp;</td>
                                        //								                <td class='Detail' width='50' align=right style='padding-right:1px;'>&nbsp;</td>
                                        //								                <td class='Detail' width='90' align=right style='padding-right:1px;'>&nbsp;</td>
                                        //								                <td class='Detail' width='50' align=right style='padding-right:1px;'>&nbsp;</td>
                                        //								                <td class='Detail' width='60' align=right style='padding-right:1px;'>" + Misc.DisplayHTMLForCurency(tempPaidDetails.InPaid) + @"</td>
                                        //								                <td class='Detail' width='60' align=right style='padding-right:1px;'>&nbsp;</td>
                                        //								                <td class='Detail' width='50' align=left style='padding-left:1px;'>" + Convert.ToString(tempPaidDetails.ReportNo) + @"</td>
                                        //								                <td class='DetailEnd' width='40' align=left style='padding-left:1px;'>&nbsp;</td>
                                        //							                </tr>";
                                        //                                            }

                                        //                                        }
                                        #endregion

                                    }

                                    detail += @"
							<tr>
								<td class='DetailStart' width='65' align=center><b>Total</b></td>
								<td class='Detail' width='65' align=left style='padding-left:1px;'>" + LoadNumberStr + @"</td>
								<td class='Detail' width='100' align=left style='padding-left:1px;'>" + CustomerNameStr + @"</td>
								<td class='Detail' width='100' align=center><b>Total</b></td>
								<td class='Detail' width='50' align=right style='padding-right:1px;'>" + SubTotalInQntStr + @"</td>
								<td class='Detail' width='90' align=right style='padding-right:1px;'>" + SubTotalInFOBStr + @"</td>
								<td class='Detail' width='50' align=right style='padding-right:1px;'>&nbsp;</td>
								<td class='Detail' width='90' align=right style='padding-right:1px;'>" + SubTotalInCommissionAmtStr + @"</td>
								<td class='Detail' width='50' align=right style='padding-right:1px;'>&nbsp;</td>
								<td class='Detail' width='60' align=right style='padding-right:1px;'>" + SubTotalInPaidStr + @"</td>
								<td class='Detail' width='60' align=right style='padding-right:1px;'>" + SubTotalInNetStr + @"</td>
								<td class='Detail' width='50' align=left style='padding-left:1px;'>" + ReportNumberItem + @"</td>
								<td class='DetailEnd' width='40' align=left style='padding-left:1px;'>&nbsp;</td>
							</tr>";

                                    //lineCount += 1;

                                    //CheckLineCount();

                                    //detail += ColumnHeader();

                                    SubTotalInQntDbl += totalInOrderQnt;

                                    SubTotalInFOBDbl += totalFOBAmt;

                                    if (SubTotalInFOBDbl != 0)
                                    {
                                        SubTotalInAllFOBPerQntDbl = SubTotalInFOBDbl / SubTotalInQntDbl;
                                    }

                                    SubTotalInCommissionAmtDbl += tempCommissionOrder.InCurrentDbl;

                                    if (SubTotalInQntDbl != 0)
                                    {
                                        SubTotalInCommissionAmtPerQntDbl = SubTotalInCommissionAmtDbl / SubTotalInQntDbl;
                                    }

                                    SubTotalInPaidDbl += tempCommissionOrder.InPaidDbl;
                                    SubTotalInNetDbl += tempCommissionOrder.InNetDbl;

                                    #endregion
                                }//End of Saleman If (Match)
                            }//end of n loop for Commission Orders

                            #region Inside Sub Totals
                            SubTotalInQntStr = Misc.DisplayFormat(SubTotalInQntDbl);
                            SubTotalInFOBStr = Misc.DisplayHTMLForCurency(SubTotalInFOBDbl);
                            SubTotalInCommissionAmtStr = Misc.DisplayHTMLForCurency(SubTotalInCommissionAmtDbl);
                            SubTotalInNetStr = Misc.DisplayHTMLForCurency(SubTotalInNetDbl);

                            //<td class='DetailStart' width='230' colspan='3' align=center><b>Grand Total</b></td>                
                            detail += @"
					<tr>
						<td class='DetailStart' width='230' colspan='3' align=center><b>Total - Inside Sales</b></td>
                        <td class='Detail' width='150' colspan='2' align=right style='padding-right:1px;'>" + SubTotalInQntStr + @"</td>
						<td class='Detail' width='140' colspan='2' align=right style='padding-right:1px;'>" + SubTotalInFOBStr + @"</td>
						<td class='Detail' width='140' colspan='2' align=right style='padding-right:1px;'>" + SubTotalInCommissionAmtStr + @"</td>
						<td class='Detail' width='60' align=right style='padding-right:1px;'>&nbsp;</td>
						<td class='Detail' width='110' colspan='2' align=right style='padding-right:1px;'><b>" + SubTotalInNetStr + @"</b></td>
						<td class='DetailEnd' width='40' align=left style='padding-left:1px;'>&nbsp;</td>
					</tr>";
                            SubTotalInQntStr = Misc.DisplayFormat(SubTotalInQntDbl / CommissionReportDataBLL.SalesPersonArl.Count);
                            SubTotalInFOBStr = Misc.DisplayHTMLForCurency(SubTotalInFOBDbl / CommissionReportDataBLL.SalesPersonArl.Count);
                            SubTotalInCommissionAmtStr = Misc.DisplayHTMLForCurency(SubTotalInCommissionAmtDbl / CommissionReportDataBLL.SalesPersonArl.Count);
                            SubTotalInNetStr = Misc.DisplayHTMLForCurency(SubTotalInNetDbl / CommissionReportDataBLL.SalesPersonArl.Count);
                            detail += @"
					<tr>
						<td class='DetailStart' width='230' colspan='3' align=center><b>" + tempSalesman.SalesPersonNameStr + @" Share Inside</b></td>
                        <td class='Detail' width='150' colspan='2' align=right style='padding-right:1px;'>" + SubTotalInQntStr + @"</td>
						<td class='Detail' width='140' colspan='2' align=right style='padding-right:1px;'>" + SubTotalInFOBStr + @"</td>
						<td class='Detail' width='140' colspan='2' align=right style='padding-right:1px;'>" + SubTotalInCommissionAmtStr + @"</td>
						<td class='Detail' width='60' align=right style='padding-right:1px;'>&nbsp;</td>
						<td class='Detail' width='110' colspan='2' align=right style='padding-right:1px;'><b>" + SubTotalInNetStr + @"</b></td>
						<td class='DetailEnd' width='40' align=left style='padding-left:1px;'>&nbsp;</td>
					</tr>";
                            #region loop for other Salesman subtotals.
                            for (int p = m + 1; p < CommissionReportDataBLL.SalesPersonArl.Count; p++)
                            {
                                SalesPerson tempSalesman1 = (SalesPerson)CommissionReportDataBLL.SalesPersonArl[p];
                                detail += @"
					        <tr>
						        <td class='DetailStart' width='230' colspan='3' align=center><b>" + tempSalesman1.SalesPersonNameStr + @" Share Inside</b></td>
                                <td class='Detail' width='150' colspan='2' align=right style='padding-right:1px;'>" + SubTotalInQntStr + @"</td>
						        <td class='Detail' width='140' colspan='2' align=right style='padding-right:1px;'>" + SubTotalInFOBStr + @"</td>
						        <td class='Detail' width='140' colspan='2' align=right style='padding-right:1px;'>" + SubTotalInCommissionAmtStr + @"</td>
						        <td class='Detail' width='60' align=right style='padding-right:1px;'>&nbsp;</td>
						        <td class='Detail' width='110' colspan='2' align=right style='padding-right:1px;'><b>" + SubTotalInNetStr + @"</b></td>
						        <td class='DetailEnd' width='40' align=left style='padding-left:1px;'>&nbsp;</td>
					        </tr>";
                            }
                            #endregion
                            #endregion


                            #endregion
                        }
                        #region Outside Sales
                        detail += ColumnHeader();


                        detail += @"<table border='0' cellPadding='0' cellspacing='0' width='870'>
                                                      <tr>
							                            <td class='Title2' align='center' width='870' colspan='13'>" + tempSalesman.SalesPersonNameStr + @"</td>
						                              </tr>";
                        detail += @"
				                    <tr>
					                    <td class='HeaderStart' width='65' align='center'>Date</td>
					                    <td class='Header' width='65' align='center'>Load #</td>
					                    <td class='Header' width='100' align='center'>Customer</td>
					                    <td class='Header' width='100' align='center'>Item</td>
					                    <td class='Header' width='50' align='center'>Unit</td>
					                    <td class='Header' width='90' align='center'>Total FOB</td>
					                    <td class='Header' width='50' align='center'>FOB</td>
					                    <td class='Header' width='90' align='center'>Cmsn.</td>
					                    <td class='Header' width='50' align='center'>/Unit</td>
					                    <td class='Header' width='60' align='center'>Paid</td>
					                    <td class='Header' width='60' align='center'>Due</td>
					                    <td class='Header' width='50' align='center'>RPT #</td>
					                    <td class='HeaderEnd' width='40' align='center'>OSS</td>
				                    </tr>";


                        for (int p = 0; p < CommissionReportDataBLL.CommissionOrderArl.Count; p++)
                        {
                            Commission_Order tempCommissionOrder3 = (Commission_Order)CommissionReportDataBLL.CommissionOrderArl[p];

                            if (tempSalesman.SalesPersonKeyStr == tempCommissionOrder3.SalesmanInt.ToString() && (tempCommissionOrder3.OutCurrentDbl != 0 || tempCommissionOrder3.OutNetDbl != 0 || tempCommissionOrder3.OutPaidDbl != 0))
                            {
                                detail += ColumnHeader();
                                double totalOutOrderQnt = 0;
                                double totalFOBAmt = 0;
                                string LoadNumberDetail = "&nbsp;";
                                string CustomerNameDetail = "&nbsp;";

                                #region Print Details
                                for (int q = 0; q < tempCommissionOrder3.CommissionOrderDetailArl.Count; q++)
                                {
                                    Commission_Order_Detail tempCommissionOrderDetail = (Commission_Order_Detail)tempCommissionOrder3.CommissionOrderDetailArl[q];

                                    if (!tempCommissionOrderDetail.InBln)
                                    {
                                        string ShipDateDetail = Misc.FormatDateTime(tempCommissionOrderDetail.DateStr);

                                        LoadNumberDetail = "&nbsp;";
                                        if (tempCommissionOrderDetail.DateStr != "")
                                        {
                                            LoadNumberDetail = StringFormater.FormatString(tempCommissionOrderDetail.LoadStr, 10);
                                        }

                                        CustomerNameDetail = "&nbsp;";
                                        if (tempCommissionOrderDetail.CustomerStr != "")
                                        {
                                            CustomerNameDetail = StringFormater.FormatString(tempCommissionOrderDetail.CustomerStr, 10);
                                        }

                                        string ItemDetail = "&nbsp;";

                                        if (tempCommissionOrderDetail.ItemStr != "")
                                        {
                                            ItemDetail = StringFormater.FormatString(tempCommissionOrderDetail.ItemStr, 10);
                                        }

                                        string UnitDetail = Misc.DisplayFormat(tempCommissionOrderDetail.QntDbl);
                                        totalOutOrderQnt += tempCommissionOrderDetail.QntDbl;
                                        string TotalFOBDetail = Misc.DisplayHTMLForCurency(tempCommissionOrderDetail.FOBDbl);
                                        string FOBDetail = Misc.DisplayHTMLForCurency(tempCommissionOrderDetail.FOBDbl / tempCommissionOrderDetail.QntDbl);
                                        totalFOBAmt += tempCommissionOrderDetail.FOBDbl;
                                        string CommissionDetail = Misc.DisplayHTMLForCurency((tempCommissionOrderDetail.FOBDbl - tempCommissionOrderDetail.CostDbl) * CommissionReportDataBLL.OutsideSaleCommissionRateDbl);
                                        string CommissionPerUnitDetail = Misc.DisplayHTMLForCurency(((tempCommissionOrderDetail.FOBDbl - tempCommissionOrderDetail.CostDbl) * CommissionReportDataBLL.OutsideSaleCommissionRateDbl) / tempCommissionOrderDetail.QntDbl);
                                        string OutSideSaleDetail = "Y";

                                        detail += @"
                                                      <tr>
                    								    <td class='DetailStart' width='65' align=left style='padding-left:1px;'>" + ShipDateDetail + @"</td>
                    								    <td class='Detail' width='65' align=left style='padding-left:1px;'>" + LoadNumberDetail + @"</td>
                    								    <td class='Detail' width='100' align=left style='padding-left:1px;'>" + CustomerNameDetail + @"</td>
                    								    <td class='Detail' width='100' align=left style='padding-left:1px;'>" + ItemDetail + @"</td>
                    								    <td class='Detail' width='50' align=right style='padding-right:1px;'>" + UnitDetail + @"</td>
                    								    <td class='Detail' width='90' align=right style='padding-right:1px;'>" + TotalFOBDetail + @"</td>
                    								    <td class='Detail' width='50' align=right style='padding-right:1px;'>" + FOBDetail + @"</td>
                    								    <td class='Detail' width='90' align=right style='padding-right:1px;'>" + CommissionDetail + @"</td>
                    								    <td class='Detail' width='50' align=right style='padding-right:1px;'>" + CommissionPerUnitDetail + @"</td>
                    								    <td class='Detail' width='60' align=right style='padding-right:1px;'>&nbsp;</td>
                    								    <td class='Detail' width='60' align=right style='padding-right:1px;'>&nbsp;</td>
                    								    <td class='Detail' width='50' align=left style='padding-left:1px;'>&nbsp;</td>
                    								    <td class='DetailEnd' width='40' align=left style='padding-left:1px;'>" + OutSideSaleDetail + @"</td>
                    							    </tr>";

                                        //lineCount += 1;

                                        //CheckLineCount();

                                        //detail += ColumnHeader();

                                    } // end of Outside sales If

                                } //end of q loop for Commission Orders

                                #endregion

                                #region Print Total
                                string LoadNumberStr = LoadNumberDetail;
                                string CustomerNameStr = CustomerNameDetail;

                                SubTotalOutQntStr = Misc.DisplayFormat(totalOutOrderQnt);
                                SubTotalOutFOBStr = Misc.DisplayHTMLForCurency(totalFOBAmt);
                                SubTotalOutCommissionAmtStr = Misc.DisplayHTMLForCurency(tempCommissionOrder3.OutCurrentDbl);
                                SubTotalOutPaidStr = Misc.DisplayHTMLForCurency(tempCommissionOrder3.OutPaidDbl);
                                SubTotalOutNetStr = Misc.DisplayHTMLForCurency(tempCommissionOrder3.OutNetDbl);
                               
                                string ReportNumberItem = "&nbsp;";
                                if (tempCommissionOrder3.CommissionPaidDetailArl.Count != 0)
                                {
                                    CommissionPaidDetails tempPaidDetails = (CommissionPaidDetails)tempCommissionOrder3.CommissionPaidDetailArl[0];

                                    if (tempPaidDetails.OutPaid != 0)
                                       {
                                         ReportNumberItem = Convert.ToString(tempPaidDetails.ReportNo);
                                       }
                                       #region Not used now print paid details
//                                        for (int r = 0; r < tempCommissionOrder3.CommissionPaidDetailArl.Count; r++)
//                                        {
//                                            CommissionPaidDetails tempPaidDetails = (CommissionPaidDetails)tempCommissionOrder3.CommissionPaidDetailArl[r];
//                                            if (tempPaidDetails.OutPaid != 0)
//                                            {
//                                                detail += @"
//                    						                    <tr>
//                    							                    <td class='DetailStart' width='65' align=center><b>Paid</b></td>
//                    							                    <td class='Detail' width='65' align=left style='padding-left:1px;'>&nbsp;</td>
//                    							                    <td class='Detail' width='100' align=left style='padding-left:1px;'>&nbsp;</td>
//                    							                    <td class='Detail' width='100' align=center><b>Paid</b></td>
//                    							                    <td class='Detail' width='50' align=right style='padding-right:1px;'>&nbsp;</td>
//                    							                    <td class='Detail' width='90' align=right style='padding-right:1px;'>&nbsp;</td>
//                    							                    <td class='Detail' width='50' align=right style='padding-right:1px;'>&nbsp;</td>
//                    							                    <td class='Detail' width='90' align=right style='padding-right:1px;'>&nbsp;</td>
//                    							                    <td class='Detail' width='50' align=right style='padding-right:1px;'>&nbsp;</td>
//                    							                    <td class='Detail' width='60' align=right style='padding-right:1px;'>" + Misc.DisplayHTMLForCurency(tempPaidDetails.OutPaid) + @"</td>
//                    							                    <td class='Detail' width='60' align=right style='padding-right:1px;'>&nbsp;</td>
//                    							                    <td class='Detail' width='50' align=left style='padding-left:1px;'>" + Convert.ToString(tempPaidDetails.ReportNo) + @"</td>
//                    							                    <td class='DetailEnd' width='40' align=left style='padding-left:1px;'>&nbsp;</td>
//                    						                    </tr>";
//                                            }
//                                        }
                                    #endregion
                                }

                                detail += @"
                    						<tr>
                    							<td class='DetailStart' width='65' align=center><b>Total</b></td>
                    							<td class='Detail' width='65' align=left style='padding-left:1px;'>" + LoadNumberStr + @"</td>
                    							<td class='Detail' width='100' align=left style='padding-left:1px;'>" + CustomerNameStr + @"</td>
                    							<td class='Detail' width='100' align=center><b>Total</b></td>
                    							<td class='Detail' width='50' align=right style='padding-right:1px;'>" + SubTotalOutQntStr + @"</td>
                    							<td class='Detail' width='90' align=right style='padding-right:1px;'>" + SubTotalOutFOBStr + @"</td>
                    							<td class='Detail' width='50' align=right style='padding-right:1px;'>&nbsp;</td>
                    							<td class='Detail' width='90' align=right style='padding-right:1px;'>" + SubTotalOutCommissionAmtStr + @"</td>
                    							<td class='Detail' width='50' align=right style='padding-right:1px;'>&nbsp;</td>
                    							<td class='Detail' width='60' align=right style='padding-right:1px;'>" + SubTotalOutPaidStr + @"</td>
                    							<td class='Detail' width='60' align=right style='padding-right:1px;'>" + SubTotalOutNetStr + @"</td>
                    							<td class='Detail' width='50' align=left style='padding-left:1px;'>" + ReportNumberItem + @"</td>
                    							<td class='DetailEnd' width='40' align=left style='padding-left:1px;'>&nbsp;</td>
                    						</tr>";

                                //lineCount += 1;

                                //CheckLineCount();

                                //detail += ColumnHeader();

                                SubTotalOutQntDbl += totalOutOrderQnt;
                                SubTotalOutFOBDbl += totalFOBAmt;

                                if (SubTotalOutFOBDbl != 0)
                                {
                                    SubTotalOutAllFOBPerQntDbl = (SubTotalOutFOBDbl / SubTotalOutQntDbl);
                                }

                                SubTotalOutCommissionAmtDbl += tempCommissionOrder3.OutCurrentDbl;

                                if (SubTotalOutQntDbl != 0)
                                {
                                    SubTotalOutCommissionAmtPerQntDbl = (SubTotalOutCommissionAmtDbl / SubTotalOutQntDbl);
                                }

                                SubTotalOutPaidDbl += tempCommissionOrder3.OutPaidDbl;
                                SubTotalOutNetDbl += tempCommissionOrder3.OutCurrentDbl;


                            }                //End of Saleman If (Match) 

                        }               //end of p loop

                                    #region Outside Sub Totals 683-700
                                    SubTotalOutQntStr = Misc.DisplayFormat(SubTotalOutQntDbl);
                                    SubTotalOutFOBStr = Misc.DisplayHTMLForCurency(SubTotalOutFOBDbl);
                                    SubTotalOutCommissionAmtStr = Misc.DisplayHTMLForCurency(SubTotalOutCommissionAmtDbl);
                                    SubTotalOutNetStr = Misc.DisplayHTMLForCurency(SubTotalOutNetDbl);

                                    //<td class='DetailStart' width='230' colspan='3' align=center><b>Grand Total</b></td>                
                                    detail += @"
					            <tr>
						            <td class='DetailStart' width='230' colspan='3' align=center><b>Sub Total - Outside Sales</b></td>
                                    <td class='Detail' width='150' colspan='2' align=right style='padding-right:1px;'>" + SubTotalOutQntStr + @"</td>
						            <td class='Detail' width='140' colspan='2' align=right style='padding-right:1px;'>" + SubTotalOutFOBStr + @"</td>
						            <td class='Detail' width='140' colspan='2' align=right style='padding-right:1px;'>" + SubTotalOutCommissionAmtStr + @"</td>
						            <td class='Detail' width='60' align=right style='padding-right:1px;'>&nbsp;</td>
						            <td class='Detail' width='110' colspan='2' align=right style='padding-right:1px;'><b>" + SubTotalOutNetStr + @"</b></td>
						            <td class='DetailEnd' width='40' align=left style='padding-left:1px;'>&nbsp;</td>
					            </tr>";
                                    #endregion
                        #endregion

                        #region Print Salesman Inside Share
                        SubTotalInQntStr = Misc.DisplayFormat(SubTotalInQntDbl / CommissionReportDataBLL.SalesPersonArl.Count);
                        SubTotalInFOBStr = Misc.DisplayHTMLForCurency(SubTotalInFOBDbl / CommissionReportDataBLL.SalesPersonArl.Count);
                        SubTotalInCommissionAmtStr = Misc.DisplayHTMLForCurency(SubTotalInCommissionAmtDbl / CommissionReportDataBLL.SalesPersonArl.Count);
                        SubTotalInNetStr = Misc.DisplayHTMLForCurency(SubTotalInNetDbl / CommissionReportDataBLL.SalesPersonArl.Count);
                        detail += @"
					<tr>
						<td class='DetailStart' width='230' colspan='3' align=center><b>" + tempSalesman.SalesPersonNameStr + @" Share Inside</b></td>
                        <td class='Detail' width='150' colspan='2' align=right style='padding-right:1px;'>" + SubTotalInQntStr + @"</td>
						<td class='Detail' width='140' colspan='2' align=right style='padding-right:1px;'>" + SubTotalInFOBStr + @"</td>
						<td class='Detail' width='140' colspan='2' align=right style='padding-right:1px;'>" + SubTotalInCommissionAmtStr + @"</td>
						<td class='Detail' width='60' align=right style='padding-right:1px;'>&nbsp;</td>
						<td class='Detail' width='110' colspan='2' align=right style='padding-right:1px;'><b>" + SubTotalInNetStr + @"</b></td>
						<td class='DetailEnd' width='40' align=left style='padding-left:1px;'>&nbsp;</td>
					</tr>";
                        #endregion
                        #region Grand Totals
                        string GrandTotalQntStr = Misc.DisplayFormat((SubTotalInQntDbl / CommissionReportDataBLL.SalesPersonArl.Count) + SubTotalOutQntDbl);
                        string GrandTotalFOBStr = Misc.DisplayHTMLForCurency((SubTotalInFOBDbl / CommissionReportDataBLL.SalesPersonArl.Count)  + SubTotalOutFOBDbl);
                        string GrandTotalCommissionAmtStr = Misc.DisplayHTMLForCurency((SubTotalInCommissionAmtDbl / CommissionReportDataBLL.SalesPersonArl.Count)  + SubTotalOutCommissionAmtDbl);
                        string GrandTotalDueStr = Misc.DisplayHTMLForCurency((SubTotalInNetDbl / CommissionReportDataBLL.SalesPersonArl.Count)  + SubTotalOutNetDbl);
                        string salesmanStr = tempSalesman.SalesPersonNameStr;

                        detail += @"
								<tr>
									<td class='DetailStart' width='230' colspan='3' align=center><b>" + salesmanStr + @" Grand Total</b></td>
									<td class='Detail' width='150' colspan='2' align=right style='padding-right:1px;'>" + GrandTotalQntStr + @"</td>
									<td class='Detail' width='140' colspan='2' align=right style='padding-right:1px;'>" + GrandTotalFOBStr + @"</td>
									<td class='Detail' width='140' colspan='2' align=right style='padding-right:1px;'>" + GrandTotalCommissionAmtStr + @"</td>
									<td class='Detail' width='60' align=right style='padding-right:1px;'>&nbsp;</td>
									<td class='Detail' width='110' colspan='2' align=right style='padding-right:1px;'><b>" + GrandTotalDueStr + @"</b></td>
									<td class='DetailEnd' width='40' align=left style='padding-left:1px;'>&nbsp;</td>
								</tr>";

                        #endregion
                        #endregion
                    }//end of m Loop for Salesman

                }
                detail += @"</table>";
                return detail;
            
     }// end of overall Detail()
        
        public string ReportFooter()
		{
			string display = "";

			if (disclaimer.DisclaimerListLandscape.Count != 0 && disclaimer.LineCountLandscape != 0)
			{
				display += @"<table cellspacing='0' cellpadding='0' width='870' class='ReportTable'>";

				for (int i = 1; i <= disclaimer.LineCountLandscape; i++)
				{
					display += @"
					<tr>
						<td align=left width=100% nowrap>
							<font face='Tahoma' size='2'>" + disclaimer.DisclaimerListLandscape[i].ToString().Trim() + @"</font>
						</td>
					</tr>";
				}

				display += @"</table>";
			}

			return display;
		}
		
        public string ReplaceWithZero(string str)
		{
			if (str == "")
				return "0";

			return str;
		}
		
        private string SetCommentLine()
		{
			StringBuilder comment = new StringBuilder();

			if (MinShipDate != "")
			{
				comment.Append(MinShipDate);
				comment.Append(" TO ");
				comment.Append(MaxShipDate);
				comment.Append(" ");
			}

			//adds radio button qualifiers to top of report
			if (PaidStr == "P")
			{
				comment.Append("- Paid Only");
			}
			if (PaidStr == "N")
			{
				comment.Append("- Not Paid Only");
			}
			if (PaidStr == "B")
			{
				comment.Append("- Includes Both");
			}

			if (comment.Length > 126)
			{
				lineCount += Math.Ceiling(Convert.ToDouble(comment.Length) / 126);
				lineCount--;
			}		
			return comment.ToString();
        }
        
        #endregion
    }
}
