using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb; 
using Oracle.DataAccess.Client;
using KeyCentral.Functions;
using KeyCentral.Lookups;

namespace CommissionBZ.UI.Inputs
{
	/// <summary>
	/// Summary description for InputExcludedCommodities.
	/// </summary>
	public partial class InputExcludedCommodities : BasePage
	{
		#region Declares
		private SortedList CommoditiesSortedList;
		#endregion

		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Commission Inputs") == false)
			{
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx");
			}

			CommoditiesSortedList = FillCommoditiesSortedList();

			if (!IsPostBack)
			{
				FillExcludedList();
				FillAvailableList();
			}
		}
		protected void Return_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Commission/Inputs");
		}
		protected void addCommodity_Click(object sender, System.EventArgs e)
		{
			JavaStatusMessage status = new JavaStatusMessage(this);
			status.ShowMessage("Excluding");

			if (AvailableCommoditiesList.SelectedIndex != -1)
			{
				foreach (ListItem item in AvailableCommoditiesList.Items)
				{
					if (item.Selected)
					{
						string CommodityKey = item.Value;

						SqlConnection Conn = GetSQLConn();
						SqlCommand insertCMD = new SqlCommand("use Commission; insert into [Excluded_Commodities] (Commodity_Key) values(@Commodity_Key)", Conn);
						insertCMD.Parameters.Add("@Commodity_Key", CommodityKey);

						insertCMD.ExecuteNonQuery();
						Message.Text = "Commodities Excluded.";

						Conn.Close();
						Conn.Dispose();
					}
				}

				FillExcludedList();
				FillAvailableList();
			}
			else
			{
				Message.Text = "Please select a Commodity to exclude.";
			}
		}
		protected void addAllCommodity_Click(object sender, System.EventArgs e)
		{
			JavaStatusMessage status = new JavaStatusMessage(this);
			status.ShowMessage("Excluding");

			ExcludedCommoditiesList.Items.Clear();

			SqlConnection Conn = GetSQLConn();
			SqlCommand removeCMD = new SqlCommand("use Commission; delete from [Excluded_Commodities]", Conn);
			removeCMD.ExecuteNonQuery();

			for (int i = 0; i < CommoditiesSortedList.Count; i++)
			{
				string commodityKey = CommoditiesSortedList.GetKey(i).ToString();
				string commodityName = CommoditiesSortedList.GetByIndex(i).ToString();

				SqlCommand insertCMD = new SqlCommand("use Commission; insert into [Excluded_Commodities] (Commodity_Key) values(@Commodity_Key)", Conn);
				insertCMD.Parameters.Add("@Commodity_Key", commodityKey);

				insertCMD.ExecuteNonQuery();
			}

			FillExcludedList();
			FillAvailableList();

			Message.Text = "All Commodities Excluded.";

			Conn.Close();
			Conn.Dispose();
		}
		protected void deleteCommodity_Click(object sender, System.EventArgs e)
		{
			JavaStatusMessage status = new JavaStatusMessage(this);
			status.ShowMessage("Including");

			if (ExcludedCommoditiesList.SelectedIndex != -1)
			{
				foreach (ListItem item in ExcludedCommoditiesList.Items)
				{
					if (item.Selected)
					{
						string CommodityKey = item.Value;

						SqlConnection Conn = GetSQLConn();

						SqlCommand removeCMD = new SqlCommand("use Commission; delete from [Excluded_Commodities] where Commodity_Key=@Commodity_Key", Conn);
						removeCMD.Parameters.Add("@Commodity_Key", CommodityKey);

						removeCMD.ExecuteNonQuery();

						Message.Text = "Commodities Included.";

						Conn.Close();
						Conn.Dispose();
					}
				}

				FillExcludedList();
				FillAvailableList();
			}
			else
			{
				Message.Text = "Please select a Commodity to include.";
			}
		}
		protected void deleteAllCommodity_Click(object sender, System.EventArgs e)
		{
			JavaStatusMessage status = new JavaStatusMessage(this);
			status.ShowMessage("Including");
			SqlConnection Conn = GetSQLConn();

			SqlCommand removeCMD = new SqlCommand("use Commission; delete from [Excluded_Commodities]", Conn);
			removeCMD.ExecuteNonQuery();

			FillExcludedList();
			FillAvailableList();

			Message.Text = "All Commodities Included.";

			Conn.Close();
			Conn.Dispose();
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
		
		#region Private helpers
		private SortedList FillCommoditiesSortedList()
		{
			SortedList ret = new SortedList();

			OracleConnection OracleConn = GetOracleConn();
			OracleCommand OracleData = new OracleCommand(FormatOracle(@"Select Name, Cmtyidx From Ic_Ps_Commodity Order by Name"), OracleConn);

			OracleData.BindByName = true;
			OracleDataReader myReader = OracleData.ExecuteReader();

			while (myReader.Read())
			{
				ret.Add(myReader.GetValue(1).ToString(), myReader.GetValue(0).ToString());
			}

			return ret;
		}
		private void FillAvailableList()
		{
			AvailableCommoditiesList.Items.Clear();

			for (int i = 0; i < CommoditiesSortedList.Count; i++)
			{
				string commodityKey = CommoditiesSortedList.GetKey(i).ToString();
				string commodityName = CommoditiesSortedList.GetByIndex(i).ToString();

				int x = 0;

				x = TextIsInListBox(ExcludedCommoditiesList, commodityName, commodityKey);

				if (x == -1)
					AvailableCommoditiesList.Items.Add(new ListItem(commodityName, commodityKey));
			}

			AvailableCommoditiesList = SortListBox(AvailableCommoditiesList);
		}
		private void FillExcludedList()
		{
			ExcludedCommoditiesList.Items.Clear();

			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("use Commission; select Commodity_Key from [Excluded_Commodities]", Conn);

			SqlDataReader myReader = selectCMD.ExecuteReader();

			while (myReader.Read())
			{
				string commodityKey = myReader.GetValue(0).ToString();
				string commodityName = CommoditiesSortedList[commodityKey].ToString();

				ExcludedCommoditiesList.Items.Add(new ListItem(commodityName, commodityKey));
			}

			myReader.Close();
			Misc.CleanUpSQL(Conn);

			ExcludedCommoditiesList = SortListBox(ExcludedCommoditiesList);
		}
		public static int TextIsInListBox(ListBox list, string textToFind, string valueToFind)
		{
			for (int i = 0; i < list.Items.Count; i++)
				if (list.Items[i].Text.Trim() == textToFind.Trim() && list.Items[i].Value.Trim() == valueToFind.Trim())
					return i;

			return -1;
		}
		public static string ParseTildaString(string stringToParse, int field)
		{
			string[] stringSplit = stringToParse.Split('~');

			if (stringSplit.Length <= field)
				return "";
			else
				return stringSplit[field];
		}
		private ListBox SortListBox(ListBox list)
		{
			// Sort list using bubblesort
			if (list.Items.Count > 0)
			{
				for (int i = list.Items.Count; i >= 0; i--)
				{
					for (int j = 0; j < (i - 1); j++)
					{
						if (String.Compare(list.Items[j].Text, list.Items[j + 1].Text, false) > 0)
						{
							ListItem tempListItem = new ListItem(list.Items[j].Text, list.Items[j].Value);
							list.Items[j].Text = list.Items[j + 1].Text;
							list.Items[j].Value = list.Items[j + 1].Value;
							list.Items[j + 1].Text = tempListItem.Text;
							list.Items[j + 1].Value = tempListItem.Value;
						}
					}
				}
			}

			return list;
		}
		private static ArrayList BuildArrayListFromListBox(ListBox list)
		{
			ArrayList listBoxArrayList = new ArrayList();

			if (list.Items.Count == 0)
				return listBoxArrayList;

			for (int i = 0; i < list.Items.Count; i++)
			{
				string key = list.Items[i].Value;

				//If this a a combined key, take the second
				if (key.IndexOf("~") != -1)
					key = Misc.ParseTildaString(key, 1);

				if (list.Items[i].Text != "" && list.Items[i].Value != "")
					listBoxArrayList.Add(new CriteriaBoxRecord(key, list.Items[i].Text, ""));
			}

			return listBoxArrayList;
		}
		#endregion		
	}
}

