using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb; 
using Oracle.DataAccess.Client;
using KeyCentral.Functions;
using KeyCentral.Lookups;

namespace CommissionBZ.UI.Inputs
{
	/// <summary>
	/// Summary description for InputExcludedCustomers.
	/// </summary>
	public partial class InputExcludedCustomers : BasePage
	{
		#region Declares
		private SortedList CustomersSortedList;
		#endregion

		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Commission Inputs") == false)
			{
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx");
			}

			CustomersSortedList = FillCustomersSortedList();

			if (!IsPostBack)
			{
				FillExcludedList();
				FillAvailableList();
			}
		}
		protected void Return_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Commission/Inputs");
		}
		protected void addCustomer_Click(object sender, System.EventArgs e)
		{
			JavaStatusMessage status = new JavaStatusMessage(this);
			status.ShowMessage("Excluding");

			if (AvailableCustomersList.SelectedIndex != -1)
			{
				foreach (ListItem item in AvailableCustomersList.Items)
				{
					if (item.Selected)
					{
						string CustomerKey = item.Value;

						SqlConnection Conn = GetSQLConn();
						SqlCommand insertCMD = new SqlCommand("use Commission; insert into [Excluded_Customers] (Customer_Key) values(@Customer_Key)", Conn);
						insertCMD.Parameters.Add("@Customer_Key", CustomerKey);

						insertCMD.ExecuteNonQuery();
						Message.Text = "Customers Excluded.";

						Conn.Close();
						Conn.Dispose();
					}
				}

				FillExcludedList();
				FillAvailableList();
			}
			else
			{
				Message.Text = "Please select a Customer to exclude.";
			}
		}
		protected void addAllCustomers_Click(object sender, System.EventArgs e)
		{
			JavaStatusMessage status = new JavaStatusMessage(this);
			status.ShowMessage("Excluding");

			ExcludedCustomersList.Items.Clear();

			SqlConnection Conn = GetSQLConn();
			SqlCommand removeCMD = new SqlCommand("use Commission; delete from [Excluded_Customers]", Conn);
			removeCMD.ExecuteNonQuery();

			for (int i = 0; i < CustomersSortedList.Count; i++)
			{
				string CustomerKey = CustomersSortedList.GetKey(i).ToString();
				string CustomerName = CustomersSortedList.GetByIndex(i).ToString();

				SqlCommand insertCMD = new SqlCommand("use Commission; insert into [Excluded_Customers] (Customer_Key) values(@Customer_Key)", Conn);
				insertCMD.Parameters.Add("@Customer_Key", CustomerKey);

				insertCMD.ExecuteNonQuery();
			}

			FillExcludedList();
			FillAvailableList();

			Message.Text = "All Customers Excluded.";

			Conn.Close();
			Conn.Dispose();
		}
		protected void deleteCustomer_Click(object sender, System.EventArgs e)
		{
			JavaStatusMessage status = new JavaStatusMessage(this);
			status.ShowMessage("Including");

			if (ExcludedCustomersList.SelectedIndex != -1)
			{
				foreach (ListItem item in ExcludedCustomersList.Items)
				{
					if (item.Selected)
					{
						string CustomerKey = item.Value;

						SqlConnection Conn = GetSQLConn();

						SqlCommand removeCMD = new SqlCommand("use Commission; delete from [Excluded_Customers] where Customer_Key=@Customer_Key", Conn);
						removeCMD.Parameters.Add("@Customer_Key", CustomerKey);

						removeCMD.ExecuteNonQuery();

						Message.Text = "Customers Included.";

						Conn.Close();
						Conn.Dispose();
					}
				}

				FillExcludedList();
				FillAvailableList();
			}
			else
			{
				Message.Text = "Please select a Customer to include.";
			}
		}
		protected void deleteAllCustomers_Click(object sender, System.EventArgs e)
		{
			JavaStatusMessage status = new JavaStatusMessage(this);
			status.ShowMessage("Including");
			SqlConnection Conn = GetSQLConn();

			SqlCommand removeCMD = new SqlCommand("use Commission; delete from [Excluded_Customers]", Conn);
			removeCMD.ExecuteNonQuery();

			FillExcludedList();
			FillAvailableList();

			Message.Text = "All Customers Included.";

			Conn.Close();
			Conn.Dispose();
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
		
		#region Private helpers
		private SortedList FillCustomersSortedList()
		{
			SortedList ret = new SortedList();

			OracleConnection OracleConn = GetOracleConn();
			OracleCommand OracleData = new OracleCommand(FormatOracle(@"
			Select
				Fc_Name.Id ||' - '|| Fc_Name.LastCoName as CustomerName,
				Fc_Name.NameIdx as CustomerKey

			From
				Ar_Cust,
				Fc_Name

			Where
				Ar_Cust.CustNameIdx = Fc_Name.NameIdx

			Order By Fc_Name.LastCoName"), OracleConn);

			OracleData.BindByName = true;
			OracleDataReader myReader = OracleData.ExecuteReader();

			while (myReader.Read())
			{
				ret.Add(myReader.GetValue(1).ToString(), myReader.GetValue(0).ToString());
			}

			return ret;
		}
		private void FillAvailableList()
		{
			AvailableCustomersList.Items.Clear();

			for (int i = 0; i < CustomersSortedList.Count; i++)
			{
				string CustomerKey = CustomersSortedList.GetKey(i).ToString();
				string CustomerName = CustomersSortedList.GetByIndex(i).ToString();

				int x = 0;

				x = TextIsInListBox(ExcludedCustomersList, CustomerName, CustomerKey);

				if (x == -1)
					AvailableCustomersList.Items.Add(new ListItem(CustomerName, CustomerKey));
			}

			AvailableCustomersList = SortListBox(AvailableCustomersList);
		}
		private void FillExcludedList()
		{
			ExcludedCustomersList.Items.Clear();

			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("use Commission; select Customer_Key from [Excluded_Customers]", Conn);

			SqlDataReader myReader = selectCMD.ExecuteReader();

			while (myReader.Read())
			{
				string CustomerKey = myReader.GetValue(0).ToString();
				string CustomerName = CustomersSortedList[CustomerKey].ToString();

				ExcludedCustomersList.Items.Add(new ListItem(CustomerName, CustomerKey));
			}

			myReader.Close();
			Misc.CleanUpSQL(Conn);

			ExcludedCustomersList = SortListBox(ExcludedCustomersList);
		}
		public static int TextIsInListBox(ListBox list, string textToFind, string valueToFind)
		{
			for (int i = 0; i < list.Items.Count; i++)
				if (list.Items[i].Text.Trim() == textToFind.Trim() && list.Items[i].Value.Trim() == valueToFind.Trim())
					return i;

			return -1;
		}
		public static string ParseTildaString(string stringToParse, int field)
		{
			string[] stringSplit = stringToParse.Split('~');

			if (stringSplit.Length <= field)
				return "";
			else
				return stringSplit[field];
		}
		private ListBox SortListBox(ListBox list)
		{
			// Sort list using bubblesort
			if (list.Items.Count > 0)
			{
				for (int i = list.Items.Count; i >= 0; i--)
				{
					for (int j = 0; j < (i - 1); j++)
					{
						if (String.Compare(list.Items[j].Text, list.Items[j + 1].Text, false) > 0)
						{
							ListItem tempListItem = new ListItem(list.Items[j].Text, list.Items[j].Value);
							list.Items[j].Text = list.Items[j + 1].Text;
							list.Items[j].Value = list.Items[j + 1].Value;
							list.Items[j + 1].Text = tempListItem.Text;
							list.Items[j + 1].Value = tempListItem.Value;
						}
					}
				}
			}

			return list;
		}
		private static ArrayList BuildArrayListFromListBox(ListBox list)
		{
			ArrayList listBoxArrayList = new ArrayList();

			if (list.Items.Count == 0)
				return listBoxArrayList;

			for (int i = 0; i < list.Items.Count; i++)
			{
				string key = list.Items[i].Value;

				//If this a a combined key, take the second
				if (key.IndexOf("~") != -1)
					key = Misc.ParseTildaString(key, 1);

				if (list.Items[i].Text != "" && list.Items[i].Value != "")
					listBoxArrayList.Add(new CriteriaBoxRecord(key, list.Items[i].Text, ""));
			}

			return listBoxArrayList;
		}
		#endregion		
	}
}

