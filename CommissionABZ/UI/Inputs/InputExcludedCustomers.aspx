<%@ Page language="c#" Inherits="CommissionBZ.UI.Inputs.InputExcludedCustomers" Codebehind="InputExcludedCustomers.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Input Excluded Customers</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body bgcolor="white">
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
				<table width="650">
					<tbody>
					</tbody>
				</table>
				<table width="650">
					<tr>
						<td align="center">Input Excluded Customers</td>
					</tr>
				</table>
				<table width="650">
					<tr>
						<td align="left" width="40%"><font face="Tahoma" size="2">Available Customers</font></td>
						<td align="left" width="20%">&nbsp;</td>
						<td align="left" width="40%"><font face="Tahoma" size="2">Excluded Customers</font></td>
					</tr>
					<tr>
						<td align="left" width="40%"><asp:listbox id="AvailableCustomersList" runat="server" SelectionMode="Multiple" Font-Names="Tahoma" Width="290px" Height="136px" TabIndex="4"></asp:listbox></td>
						<td valign="middle" align="center" width="20%">
							<table cellspacing="10">
								<tr>
									<td><asp:button id="addCustomer" runat="server" Width="75" Height="24" Text=">" TabIndex="5" onclick="addCustomer_Click"></asp:button></td>
								</tr>
								<tr>
									<td><asp:button id="deleteCustomer" runat="server" Width="75" Height="24" Text="<" TabIndex="6" onclick="deleteCustomer_Click"></asp:button></td>
								</tr>
								<tr>
									<td><asp:button id="addAllCustomers" runat="server" Width="75" Height="24" Text=">>" TabIndex="5" onclick="addAllCustomers_Click"></asp:button></td>
								</tr>
								<tr>
									<td><asp:button id="deleteAllCustomers" runat="server" Width="75" Height="24" Text="<<" TabIndex="6" onclick="deleteAllCustomers_Click"></asp:button></td>
								</tr>
							</table>
						</td>
						<td align="left" width="40%"><asp:listbox id="ExcludedCustomersList" runat="server" SelectionMode="Multiple" Font-Names="Tahoma" Width="290px" Height="136px" TabIndex="7"></asp:listbox></td>
					</tr>
				</table>
				<table width="650">
					<tr>
						<td align="left" width="200"><asp:label id="Message" runat="server" ForeColor="Red"></asp:label></td>
						<td><asp:button id="Return" tabIndex="4" runat="server" Width="160px" Height="24px" Text="Return to Menu" onclick="Return_Click"></asp:button></td>
					</tr>
				</table>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
		    </div>
		</form>
	</body>
</html>
