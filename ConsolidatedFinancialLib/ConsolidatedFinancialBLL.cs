using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Diagnostics;
using System.Reflection;
using System.Web.UI;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;
using KeyCentral.Functions;
using KeyCentral.Lookups;

namespace ConsolidatedFinancialLib
{
	/// <summary>
	/// Summary description for ConsolidatedFinancialBLL.
	/// </summary>
	public class ConsolidatedFinancialBLL
	{
		#region Vars
		private Page page;
		private System.Web.SessionState.HttpSessionState Session {get{return System.Web.HttpContext.Current.Session;}}
		#endregion

		#region Constructor/Dispose
        public ConsolidatedFinancialBLL()
		{
		}
		#endregion

		#region Public Get Functions
		#endregion

		#region Private Helper Functions
        public Company FindOrAddCompany(ArrayList list, string companyNameStr)
        {
            Company tempCompany = new Company(companyNameStr);
            int tempCompanyIdx = list.BinarySearch(tempCompany);

            if (tempCompanyIdx < 0)
                list.Insert(Math.Abs(tempCompanyIdx) - 1, tempCompany);
            else
                tempCompany = (Company)list[tempCompanyIdx];

            return tempCompany;
        }
		#endregion
	}

	#region CBO
	public class Company : IComparable
	{
		#region Vars
		public string companyNameStr;
		public double calcDbl;
        #endregion

		#region Constructor
		public Company()
		{
			companyNameStr = string.Empty;
			calcDbl = 0;
		}
		public Company(string CompanyNameStr)
		{
			companyNameStr = CompanyNameStr;
			calcDbl = 0;
		}
		#endregion

		#region IComparable Members
		public int CompareTo(object obj)
		{
			int tempCompare;
			Company Y = (Company)obj;

			//Compare companyNameStr
			tempCompare = this.companyNameStr.CompareTo(Y.companyNameStr);
			if (tempCompare != 0)
				return tempCompare;

			return 0;
		}
		#endregion
	}
	#endregion
}
