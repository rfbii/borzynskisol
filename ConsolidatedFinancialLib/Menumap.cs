using System;
using System.Collections.Generic;
using System.Text;
using KeyCentral.Functions;

namespace ConsolidatedFinancialLib
{
	[MenuMap(Security = "", Description = "Financial", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "", SortOrder = 5, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Financial")]
	[MenuMap(Security = "Consolidated Financial Report", Description = "Consolidated Financial", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Financial", SortOrder = 1, Url = "../../../ConsolidatedFinancialABZ/Reports/ConsolidatedFinancial/CriteriaConsolidatedFinancial.aspx?ReportName=CriteriaConsolidatedFinancial")]
	[MenuMap(Security = "Monthly Entries", Description = "Monthly Entries", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Financial", SortOrder = 2, Url = "../../../ConsolidatedFinancialABZ/Reports/MonthlyEntries/CriteriaMonthlyEntries.aspx?ReportName=CriteriaMonthlyEntries")]

    class Menumap
    {
    }
}
