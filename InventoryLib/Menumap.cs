using System;
using System.Collections.Generic;
using System.Text;
using KeyCentral.Functions;

namespace InventoryLib
{
	[MenuMap(Security = "Daily Inventory Report", Description = "Inventory", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "", DisplayIfEmpty = false, SortOrder = 6, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Inventory")]
	[MenuMap(Security = "Daily Inventory Report", Description = "Reports", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "/Inventory", DisplayIfEmpty = false, SortOrder = 2, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Inventory/Reports")]
	[MenuMap(Security = "Daily Inventory Report", Description = "Daily Inventory Detail", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Inventory/Reports", SortOrder = 2, Url = "../../../InventoryABZ/Reports/DailyInventoryDetail/CriteriaDailyInventoryDetail.aspx?ReportName=InputDailyInventory")]
	[MenuMap(Security = "Daily Inventory Report", Description = "Inventory", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "", DisplayIfEmpty = false, SortOrder = 6, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Inventory")]
	[MenuMap(Security = "Daily Inventory Report", Description = "Reports", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "/Inventory", DisplayIfEmpty = false, SortOrder = 2, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Inventory/Reports")]
	[MenuMap(Security = "Daily Inventory Report", Description = "Daily Inventory Sheet", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Inventory/Reports", SortOrder = 1, Url = "../../../InventoryABZ/Reports/DailyInventorySheet/CriteriaDailyInventorySheet.aspx?ReportName=InputDailyInventorySheet")]
	[MenuMap(Security = "Daily Inventory Report", Description = "Daily Inventory Summary", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Inventory/Reports", SortOrder = 3, Url = "../../../InventoryABZ/Reports/DailyInventorySummary/CriteriaDailyInventorySummary.aspx?ReportName=InputDailyInventorySummary")]
	[MenuMap(Security = "Inventory Input", Description = "Daily Inventory", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Inventory/Inputs", SortOrder = 1, Url = "../../../InventoryABZ/UI/Inputs/CriteriaInputDailyInventory.aspx?ReportName=CriteriaInputDailyInventory")]
	[MenuMap(Security = "Inventory Input", Description = "Inputs", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input,MayanDoc,Accounting", Menu = "/Inventory", DisplayIfEmpty = false, SortOrder = 1, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Inventory/Inputs")]
	[MenuMap(Security = "Inventory Input", Description = "Warehouse", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Inventory/Inputs", SortOrder = 2, Url = "../../../InventoryABZ/UI/Inputs/InputInventoryWarehouse.aspx?ReportName=InputInventoryWarehouse")]
	[MenuMap(Security = "Inventory Input", Description = "Warehouse-Products", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Inventory/Inputs", SortOrder = 3, Url = "../../../InventoryABZ/UI/Inputs/InputWarehouseProducts.aspx?ReportName=InputWarehouseProducts")]

    class Menumap
    {
    }
}
