using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Text;
using KeyCentral.Functions;

namespace FreightConfirmation.UI.Reports
{
	public partial class ReportFreightConfirmation : BasePage
	{
		#region Declares
		protected System.Web.UI.WebControls.Repeater Repeater2;
		protected System.Web.UI.HtmlControls.HtmlGenericControl CommentLine;
		protected ReportTotalsHelper totalHelper;
		#endregion

		#region Param Properties
		private string orderNumber { get { return Session["OrderNumber"].ToString(); } }
		private string totalPallets { get { return Session["TotalPallet"].ToString(); } }
		private string palletPrice { get { return Session["PalletPrice"].ToString(); } }
		#endregion
		#region Report Print
		#region Vars
		bool mainHeader = true;
		bool ChargeHeader = true;
		bool DetailHeader = true;
		bool dropChange = false;
		string lastOrder = "";
		double totalQnt = 0;
		double totalFreight = 0;
		#endregion
		public string CheckForChanges(object oItem)
		{
			if (lastOrder != GetField(oItem, "SoNo"))
			{
				dropChange = true;
				lastOrder = GetField(oItem, "SoNo");
				ChargeHeader = true;
				DetailHeader = true;
			}
			else
			{
				dropChange = false;
			}
			return "";
		}
		public string Header1(object oItem)
		{
			string header = "";
			if (mainHeader)
			{
				header = @"
					<table border='0' cellPadding='0' cellspacing='0'>
						<tr>
							<td class='Title1' width=120 height=21 align=right valign=top>Pickup Location:</td>
							<td class='Title' width=350 height=21 align=left>" + GetField(oItem, "FromPrintAddress").ToString().Replace("\r\n", "<br>") + @"</td>
                        </tr>
					</table>";
				mainHeader = false;
			}

			return header;
		}
		public string Header(object oItem)
		{
			string header = "";
			if (dropChange)
			{
                string dropNumberStr = "1";
                if (GetField(oItem, "SoNo").ToString().Length > 6)
                {
                    dropNumberStr = GetField(oItem, "SoNo").ToString().Substring(7, 1);
                }
                header = @"
					<table border='0' cellPadding='0' cellspacing='0'>
						<tr>
							<td width=720 align=center height=10 ></td>
						</tr>
						<tr>
							<td class='Header' width=720 align=center>" + "Drop " + dropNumberStr + @"</td>
						</tr>
					</table>
					<table border='0' cellPadding='0' cellspacing='0'>
						<tr>
							<td class='Title1' width=70 height=21 align=right valign=top>Customer:</td>
							<td class='Title' width=220 height=21 align=left>" + GetField(oItem, "tocust").ToString().Replace("\r\n", "<br>") + @"</td>
							<td class='Title1' width=80 height=21 align=right valign=top>Order:<br>Cust PO:<br>Carrier:</td>
							<td class='Title' width=150 height=21 align=left valign=top>" + GetField(oItem, "SoNo") + "<br>" + GetField(oItem, "custPoRef") + "<br>" + GetField(oItem, "Carrier").PadRight(15).Substring(0, 15) + @"</td>
							<td class='Title1' width=100 height=21 align=right valign=top>Delivery Date:<br>Ship Date:</td>
							<td class='Title' width=100 height=21 align=left valign=top>" + Misc.FormatDateTime(GetField(oItem, "ToEtaDateTime").ToString()) + "<br>" + Misc.FormatDateTime(GetField(oItem, "ShipDateTime").ToString()) + @"</td>
							
                        </tr>
					</table>
					<table border='0' cellPadding='0' cellspacing='0'>
						<tr>
							<td class='Title1' width=180 height=21 align=right>Customer Phone Number:</td>
							<td class='Title' width=100 height=21 align=left>" + GetField(oItem, "custCode") + "-" + GetField(oItem, "custPhone") + @"</td>
							<td class='Title1' width=120 height=21 align=right>Carrier Phone:</td>
							<td class='Title' width=100 height=21 align=left>" + GetField(oItem, "carrCode") + "-" + GetField(oItem, "carrPhone") + @"</td>
							<td class='Title1' width=90 height=21 align=right>Carrier Fax:</td>
							<td class='Title' width=130 height=21 align=left>" + GetField(oItem, "carrFax") + @"</td>
						</tr>
					</table>
					<table border='0' cellPadding='0' cellspacing='0'>
						<tr>
							<td class='Title1' width=80 height=21 align=right>Comment:</td>
							<td class='Title' width=640 height=21 align=left>" + GetField(oItem, "TruckComment") + @"</td>
						</tr>
					</table>
					<table border='0' cellPadding='0' cellspacing='0'>
						<tr>
							<td class='Title1' width=250 height=21 align=right>Freight:</td>
							<td class='Title' width=200 height=21 align=left>" + Misc.DisplayHTMLForCurency(totalHelper.GetTotal(0, lastOrder)) + @"</td>
						</tr>
					</table>";
			}

			return header;
		}
		public string Detail(object oItem)
		{
			string detail = "";
			// lines for extra charges will not have OrderQnt
			if (GetField(oItem, "OrderQnt") != "")
			{
				// need to print header if first detail
				if (DetailHeader)
				{
					detail = @"
							<table border='0' cellPadding='0' cellspacing='0'>
								<tr>
									<td class='Title3' width=100 height=21 align=center>Quantity</td>
									<td class='Title3' width=100 height=21 align=center>Rate</td>
									<td class='Title3' width=370 height=21 align=center>Description</td>
									<td class='Title3' width=150 height=21 align=center>Warehouse Location</td>
								</tr>
							</table>
							<table border='0' cellPadding='0' cellspacing='0'>
								<tr>
									<td class='Title4' width=80 height=21 align=right>" + GetField(oItem, "OrderQnt") + @"</td>
									<td class='Title4' width=90 height=21 align=right>" + Misc.DisplayFormat4dec(double.Parse(GetField(oItem, "Rate"))) + @"</td>
									<td class='Title4' width=400 height=21 align=left>&nbsp&nbsp" + GetField(oItem, "Name") + @"</td>
									<td class='Title4' width=150 height=21 align=left>" + GetField(oItem, "descr") + @"</td>
								</tr>
							</table>";
					totalQnt = totalQnt + double.Parse(GetField(oItem, "OrderQnt"));
					DetailHeader = false;
				}
				else
				{
					detail = @"
							<table border='0' cellPadding='0' cellspacing='0'>
								<tr>
									<td class='Title4' width=80 height=21 align=right>" + GetField(oItem, "OrderQnt") + @"</td>
									<td class='Title4' width=90 height=21 align=right>" + Misc.DisplayFormat4dec(double.Parse(GetField(oItem, "Rate"))) + @"</td>
									<td class='Title4' width=400 height=21 align=left>&nbsp&nbsp" + GetField(oItem, "Name") + @"</td>
									<td class='Title4' width=150 height=21 align=left>" + GetField(oItem, "descr") + @"</td>
								</tr>
							</table>";
					totalQnt = totalQnt + double.Parse(GetField(oItem, "OrderQnt"));
				}
			}
			else
			{
				// need to print header if first charge detail
				if (ChargeHeader)
				{
					detail = @"
					<table border='0' cellPadding='0' cellspacing='0'>
						<tr>
							<td class='Title3' width=80 height=21 align=center>Quantity</td>
							<td class='Title3' width=90 height=21 align=center>Rate</td>
							<td class='Title3' width=90 height=21 align=center>Amount</td>
							<td class='Title3' width=430 height=21 align=center>Description</td>
						</tr>
					</table>
					<table border='0' cellPadding='0' cellspacing='0'>
						<tr>
							<td class='Title4' width=80 height=21 align=right>" + GetField(oItem, "Qnt") + @"</td>
							<td class='Title4' width=90 height=21 align=right>" + Misc.DisplayFormat4dec(double.Parse(GetField(oItem, "Rate"))) + @"</td>
							<td class='Title4' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(decimal.Parse(GetField(oItem, "Amount"))) + @"</td>
							<td class='Title4' width=450 height=21 align=left>&nbsp&nbsp" + GetField(oItem, "InvcDescr") + @"</td>
						</tr>
					</table>";
					totalQnt = totalQnt + double.Parse(GetField(oItem, "Qnt"));
					ChargeHeader = false;
				}
				else
				{
					detail = @"
					<table border='0' cellPadding='0' cellspacing='0'>
						<tr>
							<td class='Title4' width=80 height=21 align=right>" + GetField(oItem, "Qnt") + @"</td>
							<td class='Title4' width=90 height=21 align=right>" + Misc.DisplayFormat4dec(double.Parse(GetField(oItem, "Rate"))) + @"</td>
							<td class='Title4' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(decimal.Parse(GetField(oItem, "Amount"))) + @"</td>
							<td class='Title4' width=450 height=21 align=left>&nbsp&nbsp" + GetField(oItem, "InvcDescr") + @"</td>
						</tr>
					</table>";
					totalQnt = totalQnt + double.Parse(GetField(oItem, "Qnt"));
				}
			}
			totalFreight = totalFreight + double.Parse(GetField(oItem, "Amount"));
			return detail;
		}
		public string Total()
		{
			string total = "";
			string total1 = "";
			string total2 = "";
			total = @"
					<table border='0' cellPadding='0' cellspacing='0'>
						<tr>
							<td class='Title5' width=80 height=21 align=right>" + totalQnt + @"</td>
							<td class='Title5' width=90 height=21 align=right>" + Misc.DisplayHTMLForCurency(totalFreight) + @"</td>
							<td class='Title6' width=100 height=21 align=left>&nbsp&nbsp" + "Total" + @"</td>";
			if (totalPallets != "" && palletPrice != "")
			{
				total1 = @"
							<td class='Title6' width=150 height=21 align=left>&nbsp&nbsp" + "The Truck needs " + @"</td>
							<td class='Title5' width=30 height=21 align=left>" + totalPallets + @"</td>
							<td class='Title6' width=100 height=21 align=left>" + " Pallets or " + @"</td>
							<td class='Title5' width=50 height=21 align=left>" + Misc.DisplayHTMLForCurency(double.Parse(palletPrice)) + @"</td>
							<td class='Title6' width=120 height=21 align=left>&nbsp&nbsp" + " Per Pallet." + @"</td>
						</tr>
					</table>";
			}
			else
			{
				if (totalPallets != "" && palletPrice == "")
				{
					total1 = @"
							<td class='Title6' width=150 height=21 align=left>&nbsp&nbsp" + "The Truck needs " + @"</td>
							<td class='Title5' width=30 height=21 align=left>" + totalPallets + @"</td>
							<td class='Title6' width=270 height=21 align=left>" + " Pallets " + @"</td>
						</tr>
					</table>";
				}
				else
				{
					total1 = @"
						</tr>
					</table>";
				}
			}

			total2 = @" 
				 <table border='0' cellPadding='0' cellspacing='0'>
						<tr>
							<td class='Title1' width=500 height=21 align=left>" + "PLEASE REFERENCE OUR LOAD NUMBER WHEN INVOICING" + @"</td>
						    <td class='Title' width=220 height=21 align=left>" + orderNumber + @"</td>
						</tr>
						<tr>
							<td class='Title1' width=720 height=21 align=left colspan=2>Unloading and gate fees are included in freight.</td>
						</tr>
						<tr>
							<td class='Title1' width=720 height=21 align=left colspan=2>Must call in by 9:00 AM daily at 800-248-0420 ext 122</td>
						</tr>
						<tr>
							<td class='Title1' width=720 height=21 align=left colspan=2>Any discrepancies must be reported immediately</td>
						</tr>
						<tr>
							<td class='Title1' width=720 height=21 align=left colspan=2>Please sign and fax to 262-886-2111&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Signature:&nbsp;&nbsp;_____________________________________________</td>
						</tr>
					</table>";
			return total + total1 + total2;
		}
		private string GetField(object oItem, string strField)
		{
			return DataBinder.Eval(oItem, "DataItem." + strField).ToString();
		}
		private string GetDateField(object oItem, string strField)
		{
			string str = DataBinder.Eval(oItem, "DataItem." + strField).ToString();
			if (str.IndexOf(" ", 0) > 0)
				return str.Substring(0, str.IndexOf(" ", 0));
			else
				return str;
		}
		#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Freight Confirmation Report") == false)
				Response.Redirect("../../../KeyCentralABZ/UI/Menu/MenuMain.aspx");

			JavaStatusMessage status = new JavaStatusMessage(this);
			status.ShowMessage("Computing");
			ShowReport();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}

		private void InitializeComponent()
		{

		}
		#endregion

		private DataSet GetOrderData(string orderNumber)
		{
			OracleConnection OracleConn = GetOracleConn();
			DataSet orderData = new DataSet();
			#region Oracle Code
			string reportCommand = Misc.FormatOracle(Session, @"
	SELECT /*+ CHOOSE */
			Ar_Trx_Header.SoNo, 
			Ar_Trx_Header.ShipDateTime,
			Ar_Trx_Header.CustPoRef,
			CarrName.LastCoName as Carrier,
			Ar_So_Line.IcQnt,
			Ar_So_Line.OrderQnt,
			Ic_Product_Id.Name,
			Fc_Name_Location.Descr,
			Ar_So_Line.UnitsPerPallet,
			CustName.PrintAddress as tocust,
			LComment.TruckComment,
			Ar_Trx_Header.TruckBroker,
			Ar_So_Charge.Amt as Amount,
			Ar_So_Charge.rate,
			Ar_Trx_Header.ToEtaDateTime,
			Ar_Trx_Header_Ship.FromPrintAddress as FromPrintAddress,
			Fc_Name_Comm.AreaCode custCode,
			Fc_Name_Comm.CommString as custPhone,
			CarrPhone.AreaCode as CarrCode,
			CarrPhone.CommString as CarrPhone,
			CarrFax.CommString as CarrFax
			
	
    FROM    AR_TRX_HEADER,
			Fc_Name_Location CustName,
			Fc_Name CarrName,
			Ar_So_Line,
			Ar_So_Detail,
			Ar_Trx_Header_Ship,
			Ic_Product_Id,
			Ic_Warehouse,
			Fc_Name_Location,
			Ar_So_Charge,
			Fc_Name_Comm,
			Fc_Name_Comm CarrPhone,
			Fc_Name_Comm CarrFax,
			Fc_Charge_Code,

		(
		Select 
					Ar_So_Comment.Cmnt as TruckComment,
					Ar_Trx_Header.ArTrxHdrIdx
			
		From
					AR_TRX_HEADER,
					Ar_So_Comment,
					Fc_Comment_Type
			
		Where
			    	lower(SubStr(AR_TRX_HEADER.SoNo,0,6)) = lower(:OrderNumber) and
					Ar_So_Comment.ArTrxHdrIdx             = Ar_Trx_Header.ArTrxHdrIdx and
					Fc_Comment_Type.CommentTypeIdx        = Ar_So_Comment.CommentTypeIdx and
					Fc_Comment_Type.ID                    = 'Truck'
		) LComment
	
		Where
			 lower(SubStr(AR_TRX_HEADER.SoNo,0,6)) = lower(:OrderNumber) and
			CustName.NameIdx                      = Ar_Trx_Header.ToCustNameIdx and
			CustName.NameLocationSeq              = Ar_Trx_Header.ToCustLocSeq and
			CarrName.NameIdx                      (+)= Ar_Trx_Header.CarrierNameIdx and
			Ar_So_Line.ArTrxHdrIdx                = Ar_Trx_Header.ArTrxHdrIdx and
			Ar_So_Line.OrderQnt                   <> '0' and
			Ic_Product_Id.ProductIdx              = Ar_So_Line.ProductIdx and
			Ic_Warehouse.WarehouseIdx             = Ar_So_Line.WarehouseIdx and
			Fc_Name_Location.NameIdx              = Ic_Warehouse.NameIdx and
			Fc_Name_Location.NameLocationSeq      = Ic_Warehouse.NameLocationSeq and
			Fc_Name_Comm.NameIdx                  (+)= CustName.NameIdx and
			Fc_Name_Comm.NameLocationSeq          (+)= CustName.NameLocationSeq and
			Fc_Name_Comm.CommType                 (+)= '2' and
			Fc_Name_Comm.NameCommSeq              (+)= '1' and
			CarrPhone.NameIdx                     (+)= Ar_Trx_Header.CarrierNameIdx and
			CarrPhone.NameLocationSeq             (+)= Ar_Trx_Header.CarrierLocationSeq and
			CarrPhone.CommType                    (+)= '2' and
			CarrPhone.NameCommSeq                 (+)= '1' and
			CarrFax.NameIdx                       (+)= Ar_Trx_Header.CarrierNameIdx and
			CarrFax.NameLocationSeq               (+)= Ar_Trx_Header.CarrierLocationSeq and
			CarrFax.CommType                      (+)= '3' and
			Ar_So_Detail.ArTrxHdrIdx              = Ar_So_Line.ArTrxHdrIdx and
			Ar_So_Detail.ArSoLineTrxType          = Ar_So_Line.ArSoLineTrxType and
			Ar_So_Detail.ArSoLineSeq              = Ar_So_Line.ArSoLineSeq and
			Ar_So_Charge.ArTrxHdrIdx              (+)= Ar_So_Detail.ArTrxHdrIdx and
			Ar_So_Charge.ArSoDtlSeq               (+)= Ar_So_Detail.ArSoDtlSeq and
			Ar_So_Charge.NameIdx      			  is not null and
			Ar_So_Charge.Qnt					  <> '0' and
            Fc_Charge_Code.FcChargeIdx            = Ar_So_Charge.FcChargeIdx and
			(Fc_Charge_Code.ID                     <> 'AP-REB' and
            Fc_Charge_Code.ID                     <> 'APBRKG') and
			Ar_Trx_Header_Ship.ArTrxHdrIdx        = Ar_So_Detail.ArTrxHdrIdx and
			Ar_Trx_Header_Ship.FromLocationSeq    = Ic_Warehouse.NameLocationSeq and
			Ar_Trx_Header_Ship.FromNameIdx        = Ic_Warehouse.NameIdx and
            LComment.ArTrxHdrIdx               (+)= Ar_Trx_Header.ArTrxHdrIdx and
			Fc_Name_Comm.OrderBy				  (+)= '1' and
			CarrPhone.OrderBy					  (+)= '1' and
			CarrFax.OrderBy						  (+)= '1'

    Order By
			Ar_Trx_Header.SoNo ");
			#endregion"
			OracleDataAdapter OracleData = new OracleDataAdapter(reportCommand, OracleConn);

			OracleData.SelectCommand.BindByName = true;
			OracleData.SelectCommand.Parameters.Add(":OrderNumber", orderNumber);

			OracleData.Fill(orderData);
			OracleData.Dispose();
			Misc.CleanUpOracle(OracleConn);
			return orderData;
		}
		private DataSet GetInvoiceData(string orderNumber)
		{
			OracleConnection OracleConn = GetOracleConn();
			DataSet invoiceData = new DataSet();
			#region Oracle Code
			string reportCommand = Misc.FormatOracle(Session, @"
	SELECT /*+ CHOOSE */
			Ar_Trx_Header.SoNo, 
			Ar_Trx_Header.ShipDateTime,
			Ar_Trx_Header.CustPoRef,
			CarrName.LastCoName as Carrier,
			Ar_Trx_Line.Qnt as OrderQnt,
			Ic_Product_Id.Name,
			Fc_Name_Location.Descr,
			Ar_Trx_Line.UnitsPerPallet,
			CustName.PrintAddress as tocust,
			LComment.TruckComment,
			Ar_Trx_Header.TruckBroker,
			Ar_Trx_Detail.ForeignAmt as Amount,
			Ar_Trx_Charge.rate,
			Ar_Trx_Header.ToEtaDateTime,
			Ar_Trx_Header_Ship.FromPrintAddress as FromPrintAddress,
			Fc_Name_Comm.AreaCode custCode,
			Fc_Name_Comm.CommString as custPhone,
			CarrPhone.AreaCode as CarrCode,
			CarrPhone.CommString as CarrPhone,
			CarrFax.CommString as CarrFax
			
	
    FROM    AR_TRX_HEADER,
			Fc_Name_Location CustName,
			Fc_Name CarrName,
			Ar_Trx_Line,
			Ar_Trx_Detail,
			Ar_Trx_Header_Ship,
			Ic_Product_Id,
			Ic_Warehouse,
			Fc_Name_Location,
			Ar_Trx_Charge,
			Fc_Name_Comm,
			Fc_Name_Comm CarrPhone,
			Fc_Name_Comm CarrFax,
            Fc_Charge_Code,

		(
		Select 
					Ar_Trx_Comment.Cmnt as TruckComment,
					Ar_Trx_Header.ArTrxHdrIdx
			
		From
					AR_TRX_HEADER,
					Ar_Trx_Comment,
					Fc_Comment_Type
			
		Where
			    	lower(SubStr(AR_TRX_HEADER.SoNo,0,6)) = lower(:OrderNumber) and
					Ar_Trx_Comment.ArTrxHdrIdx             = Ar_Trx_Header.ArTrxHdrIdx and
					Fc_Comment_Type.CommentTypeIdx        = Ar_Trx_Comment.CommentTypeIdx and
					Fc_Comment_Type.ID                    = 'Truck'
		) LComment
	
		Where
			lower(SubStr(AR_TRX_HEADER.SoNo,0,6)) = lower(:OrderNumber) and
			CustName.NameIdx                      = Ar_Trx_Header.ToCustNameIdx and
			CustName.NameLocationSeq              = Ar_Trx_Header.ToCustLocSeq and
			CarrName.NameIdx                     (+)= Ar_Trx_Header.CarrierNameIdx and
			Ar_Trx_Line.ArTrxHdrIdx               = Ar_Trx_Header.ArTrxHdrIdx and
			Ar_Trx_Line.Qnt                       <> '0' and
			Ic_Product_Id.ProductIdx              = Ar_Trx_Line.ProductIdx and
			Fc_Name_Comm.NameIdx                  (+)= CustName.NameIdx and
			Fc_Name_Comm.NameLocationSeq          (+)= CustName.NameLocationSeq and
			Fc_Name_Comm.CommType                 (+)= '2' and
			Fc_Name_Comm.NameCommSeq              (+)= '1' and
			CarrPhone.NameIdx                     (+)= Ar_Trx_Header.CarrierNameIdx and
			CarrPhone.NameLocationSeq             (+)= Ar_Trx_Header.CarrierLocationSeq and
			CarrPhone.CommType                    (+)= '2' and
			CarrPhone.NameCommSeq                 (+)= '1' and
			CarrFax.NameIdx                       (+)= Ar_Trx_Header.CarrierNameIdx and
			CarrFax.NameLocationSeq               (+)= Ar_Trx_Header.CarrierLocationSeq and
			CarrFax.CommType                      (+)= '3' and
			Ar_Trx_Detail.ArTrxHdrIdx              = Ar_Trx_Line.ArTrxHdrIdx and
			Ar_Trx_Detail.ArTrxLineTrxType         = Ar_Trx_Line.ArTrxLineTrxType and
			Ar_Trx_Detail.ArTrxLineSeq             = Ar_Trx_Line.ArTrxLineSeq and
            Ar_Trx_Detail.GlDeleteCode            = 'N' and
			Ar_Trx_Charge.ArTrxHdrIdx             (+)= Ar_Trx_Detail.ArTrxHdrIdx and
			Ar_Trx_Charge.ArTrxDtlSeq             (+)= Ar_Trx_Detail.ArTrxDtlSeq and
			Ar_Trx_Charge.NameIdx      			  is not null and
			Ar_Trx_Charge.Qnt					  <> '0' and
			Fc_Charge_Code.FcChargeIdx            = Ar_Trx_Charge.FcChargeIdx and
			(Fc_Charge_Code.ID                     <> 'AP-REB' and
            Fc_Charge_Code.ID                     <> 'APBRKG') and
			Ic_Warehouse.WarehouseIdx              = Ar_Trx_Charge.WarehouseIdx and
			Fc_Name_Location.NameIdx               = Ic_Warehouse.NameIdx and
			Fc_Name_Location.NameLocationSeq       = Ic_Warehouse.NameLocationSeq and
			Ar_Trx_Header_Ship.FromNameIdx         = Ic_Warehouse.NameIdx and
			Ar_Trx_Header_Ship.FromLocationSeq     = Ic_Warehouse.NameLocationSeq and
			Ar_Trx_Header_Ship.ArTrxHdrIdx         = Ar_Trx_Detail.ArTrxHdrIdx and
			LComment.ArTrxHdrIdx                  (+)= Ar_Trx_Header.ArTrxHdrIdx and
			Fc_Name_Comm.OrderBy				  = '1' and
			CarrPhone.OrderBy					  = '1' and
			CarrFax.OrderBy						  = '1'

    Order By
			Ar_Trx_Header.SoNo  
			");
			#endregion"
			OracleDataAdapter OracleData = new OracleDataAdapter(reportCommand, OracleConn);
			OracleData.SelectCommand.BindByName = true;
			OracleData.SelectCommand.Parameters.Add(":OrderNumber", orderNumber);
			OracleData.Fill(invoiceData);
			OracleData.Dispose();
			Misc.CleanUpOracle(OracleConn);
			return invoiceData;
		}
		private DataSet GetOrderChargeData(string orderNumber)
		{
			OracleConnection OracleConn = GetOracleConn();
			DataSet chargeData = new DataSet();
			#region Oracle Code
			string reportCommand = Misc.FormatOracle(Session, @"SELECT 
			Ar_Trx_Header.SoNo, 
			Ar_So_Charge.Amt as Amount,
			Ar_So_Charge.rate,
			Ar_So_Charge.Qnt,
			Ar_So_Charge.InvcDescr,
			Ar_Trx_Header.ShipDateTime,
			Ar_Trx_Header.CustPoRef,
			LComment.TruckComment,
			Ar_Trx_Header.TruckBroker,
			Ar_Trx_Header.ToEtaDateTime,
			CustName.PrintAddress as tocust,
			CarrName.LastCoName as Carrier,
			Fc_Name_Comm.AreaCode custCode,
			Fc_Name_Comm.CommString as custPhone,
			CarrPhone.AreaCode as CarrCode,
			CarrPhone.CommString as CarrPhone,
			CarrFax.CommString as CarrFax,
			Ar_Trx_Header_Ship.FromPrintAddress as FromPrintAddress
					
							
	FROM    AR_TRX_HEADER,
			Ar_So_Line,
			Ar_So_Detail,
			Ar_Trx_Header_Ship,
			Ar_So_Charge,
            Fc_Name_Location CustName,
			Fc_Name CarrName,
			Fc_Name_Comm,
			Fc_Name_Comm CarrPhone,
			Fc_Name_Comm CarrFax,
			Fc_Charge_Code,
			
		(
		Select 
					Ar_So_Comment.Cmnt as TruckComment,
					Ar_Trx_Header.ArTrxHdrIdx
			
		From
					AR_TRX_HEADER,
					Ar_So_Comment,
					Fc_Comment_Type
			
		Where
			    	lower(SubStr(AR_TRX_HEADER.SoNo,0,6)) = lower(:OrderNumber) and
					Ar_So_Comment.ArTrxHdrIdx             = Ar_Trx_Header.ArTrxHdrIdx and
					Fc_Comment_Type.CommentTypeIdx        = Ar_So_Comment.CommentTypeIdx and
					Fc_Comment_Type.ID                    = 'Truck'
		) LComment
	
		Where
			 lower(SubStr(AR_TRX_HEADER.SoNo,0,6)) = lower(:OrderNumber) and
			CustName.NameIdx                      = Ar_Trx_Header.ToCustNameIdx and
			CustName.NameLocationSeq              = Ar_Trx_Header.ToCustLocSeq and
			CarrName.NameIdx                      (+)= Ar_Trx_Header.CarrierNameIdx and
			Ar_So_Line.ArTrxHdrIdx                = Ar_Trx_Header.ArTrxHdrIdx and
			Ar_So_Detail.ArTrxHdrIdx              = Ar_So_Line.ArTrxHdrIdx and
			Ar_So_Detail.ArSoLineTrxType          = Ar_So_Line.ArSoLineTrxType and
			Ar_So_Detail.ArSoLineSeq              = Ar_So_Line.ArSoLineSeq and
			Ar_So_Line.ProductIdx                 is null and
			Fc_Name_Comm.NameIdx                  (+)= CustName.NameIdx and
			Fc_Name_Comm.NameLocationSeq          (+)= CustName.NameLocationSeq and
			Fc_Name_Comm.CommType                 (+)= '2' and
			Fc_Name_Comm.NameCommSeq              (+)= '1' and
			CarrPhone.NameIdx                     (+)= Ar_Trx_Header.CarrierNameIdx and
			CarrPhone.NameLocationSeq             (+)= Ar_Trx_Header.CarrierLocationSeq and
			CarrPhone.CommType                    (+)= '2' and
			CarrPhone.NameCommSeq                 (+)= '1' and
			CarrFax.NameIdx                       (+)= Ar_Trx_Header.CarrierNameIdx and
			CarrFax.NameLocationSeq               (+)= Ar_Trx_Header.CarrierLocationSeq and
			CarrFax.CommType                      (+)= '3' and
			
			Ar_So_Charge.ArTrxHdrIdx             (+)= Ar_So_Detail.ArTrxHdrIdx and
			Ar_So_Charge.ArSoDtlSeq              (+)= Ar_So_Detail.ArSoDtlSeq and
            Ar_So_Charge.NameIdx      			  is not null and
			Ar_So_Charge.NameIdx                 = Ar_Trx_Header.CarrierNameIdx and
			Ar_So_Charge.NameLocationSeq         = Ar_Trx_Header.CarrierLocationSeq And
            Fc_Charge_Code.FcChargeIdx            = Ar_So_Charge.FcChargeIdx and
			(Fc_Charge_Code.ID                     <> 'AP-REB' and
            Fc_Charge_Code.ID                     <> 'APBRKG') and
            Ar_Trx_Header_Ship.ArTrxHdrIdx       (+)= Ar_So_Detail.ArTrxHdrIdx and
			LComment.ArTrxHdrIdx                  (+)= Ar_Trx_Header.ArTrxHdrIdx and
			Fc_Name_Comm.OrderBy				  = '1' and
			CarrPhone.OrderBy					  = '1' and
			CarrFax.OrderBy						  = '1'");
			#endregion"
			OracleDataAdapter OracleData = new OracleDataAdapter(reportCommand, OracleConn);
			OracleData.SelectCommand.BindByName = true;
			OracleData.SelectCommand.Parameters.Add(":OrderNumber", orderNumber);
			OracleData.Fill(chargeData);
			OracleData.Dispose();
			Misc.CleanUpOracle(OracleConn);
			return chargeData;

		}
		private DataSet GetInvoiceChargeData(string orderNumber)
		{

			OracleConnection OracleConn = GetOracleConn();
			DataSet chargeData = new DataSet();
			#region Oracle Code
			string reportCommand = Misc.FormatOracle(Session, @"SELECT 
			Ar_Trx_Header.SoNo, 
			Ar_Trx_Detail.ForeignAmt as Amount,
			Ar_Trx_Charge.rate,
			Ar_Trx_Charge.Qnt,
			Ar_Trx_Charge.InvcDescr,
			Ar_Trx_Header.ShipDateTime,
			Ar_Trx_Header.CustPoRef,
			CarrName.LastCoName as Carrier,
			CustName.PrintAddress as tocust,
			LComment.TruckComment,
			Ar_Trx_Header.TruckBroker,
			Ar_Trx_Header.ToEtaDateTime,
			Ar_Trx_Header_Ship.FromPrintAddress as FromPrintAddress,
			Fc_Name_Comm.AreaCode custCode,
			Fc_Name_Comm.CommString as custPhone,
			CarrPhone.AreaCode as CarrCode,
			CarrPhone.CommString as CarrPhone,
			CarrFax.CommString as CarrFax

	FROM    AR_TRX_HEADER,
			Ar_Trx_Line,
			Ar_Trx_Detail,
			Ar_Trx_Charge,
			Fc_Name_Comm,
			Fc_Name_Comm CarrPhone,
			Fc_Name_Comm CarrFax,
			Fc_Name_Location CustName,
			Fc_Name CarrName,
			Ar_Trx_Header_Ship,
            Fc_Charge_Code,
	
	(
		Select 
					Ar_Trx_Comment.Cmnt as TruckComment,
					Ar_Trx_Header.ArTrxHdrIdx
			
		From
					AR_TRX_HEADER,
					Ar_Trx_Comment,
					Fc_Comment_Type
			
		Where
			    	lower(SubStr(AR_TRX_HEADER.SoNo,0,6)) = lower(:OrderNumber) and
					Ar_Trx_Comment.ArTrxHdrIdx             = Ar_Trx_Header.ArTrxHdrIdx and
					Fc_Comment_Type.CommentTypeIdx        = Ar_Trx_Comment.CommentTypeIdx and
					Fc_Comment_Type.ID                    = 'Truck'
		) LComment
		
		Where
			lower(SubStr(AR_TRX_HEADER.SoNo,0,6)) = lower(:OrderNumber) and
			CustName.NameIdx                      = Ar_Trx_Header.ToCustNameIdx and
			CustName.NameLocationSeq              = Ar_Trx_Header.ToCustLocSeq and
			CarrName.NameIdx                     (+)= Ar_Trx_Header.CarrierNameIdx and
			Ar_Trx_Line.ArTrxHdrIdx                = Ar_Trx_Header.ArTrxHdrIdx and
			Ar_Trx_Detail.ArTrxHdrIdx              = Ar_Trx_Line.ArTrxHdrIdx and
			Ar_Trx_Detail.ArTrxLineTrxType          = Ar_Trx_Line.ArTrxLineTrxType and
			Ar_Trx_Detail.ArTrxLineSeq              = Ar_Trx_Line.ArTrxLineSeq and
            Ar_Trx_Detail.GlDeleteCode            = 'N' and
			Ar_Trx_Line.ProductIdx                 is null and
			Fc_Name_Comm.NameIdx                  (+)= CustName.NameIdx and
			Fc_Name_Comm.NameLocationSeq          (+)= CustName.NameLocationSeq and
			Fc_Name_Comm.CommType                 (+)= '2' and
			Fc_Name_Comm.NameCommSeq              (+)= '1' and
			CarrPhone.NameIdx                     (+)= Ar_Trx_Header.CarrierNameIdx and
			CarrPhone.NameLocationSeq             (+)= Ar_Trx_Header.CarrierLocationSeq and
			CarrPhone.CommType                    (+)= '2' and
			CarrPhone.NameCommSeq                 (+)= '1' and
			CarrFax.NameIdx                       (+)= Ar_Trx_Header.CarrierNameIdx and
			CarrFax.NameLocationSeq               (+)= Ar_Trx_Header.CarrierLocationSeq and
			CarrFax.CommType                      (+)= '3' and
			Ar_Trx_Charge.ArTrxHdrIdx             (+)= Ar_Trx_Detail.ArTrxHdrIdx and
			Ar_Trx_Charge.ArTrxDtlSeq              (+)= Ar_Trx_Detail.ArTrxDtlSeq and
			Ar_Trx_Charge.ChargeMethodType        = '1' and
			Ar_Trx_Charge.NameIdx                 = Ar_Trx_Header.CarrierNameIdx and
			Ar_Trx_Charge.NameLocationSeq         = Ar_Trx_Header.CarrierLocationSeq And
			Fc_Charge_Code.FcChargeIdx            = Ar_Trx_Charge.FcChargeIdx and
			(Fc_Charge_Code.ID                     <> 'AP-REB' and
            Fc_Charge_Code.ID                     <> 'APBRKG') and
			Ar_Trx_Header_Ship.ArTrxHdrIdx         (+)= Ar_Trx_Detail.ArTrxHdrIdx and
			LComment.ArTrxHdrIdx                  (+)= Ar_Trx_Header.ArTrxHdrIdx and
			Fc_Name_Comm.OrderBy				  = '1' and
			CarrPhone.OrderBy					  = '1' and
			CarrFax.OrderBy						  = '1'");
			#endregion"
			OracleDataAdapter OracleData = new OracleDataAdapter(reportCommand, OracleConn);
			OracleData.SelectCommand.BindByName = true;
			OracleData.SelectCommand.Parameters.Add(":OrderNumber", orderNumber);
			OracleData.Fill(chargeData);
			OracleData.Dispose();
			Misc.CleanUpOracle(OracleConn);
			return chargeData;

		}
		private DataSet GetReportData(string orderNumber)
		{
			//Call GetInvoiceData
			DataSet invoiceData = GetInvoiceData(orderNumber);

			//Get Orders that are not in the invoice data
			DataSet orderData = GetOrderData(orderNumber);

			//Delete Order data that is Invoiced
			DeleteOrderThatAreInvoiced(orderData, invoiceData);

			//Get OrderChargeData
			DataSet orderChargeData = GetOrderChargeData(orderNumber);

			//Get InvoiceChargeData
			DataSet invoiceChargeData = GetInvoiceChargeData(orderNumber);

			//Delete Charge Order data that is Invoiced
			DeleteOrderThatAreInvoiced(orderChargeData, invoiceChargeData);

			//Merge Order data with Order Charge Data
			orderData.Merge(orderChargeData);

			//Merge Invoice Data with Invoice Charge Data
			invoiceData.Merge(invoiceChargeData);

			//Combine Order Data and Invoice Data
			orderData.Merge(invoiceData);

			return orderData;
		}
		private void DeleteOrderThatAreInvoiced(DataSet orders, DataSet invoices)
		{
			ArrayList invoiceLotIds = Misc.BuildStringArrayListFromDataSet(invoices, 0);
			invoiceLotIds.Sort();

			for (int i = orders.Tables[0].Rows.Count - 1; i >= 0; i--)
				if (invoiceLotIds.BinarySearch((string)orders.Tables[0].Rows[i]["SoNo"]) >= 0)
					orders.Tables[0].Rows.RemoveAt(i);
		}
		private void ShowReport()
		{
			DataSet dsRet = new DataSet();
			dsRet = GetReportData(orderNumber.Trim());
			totalHelper = new ReportTotalsHelper();
			totalHelper.AddSubTotal1And1("SoNo", "Amount");//0
			totalHelper.CalcTotals(dsRet);
			dsRet.Tables[0].DefaultView.Sort = "SoNo";
			if (dsRet.Tables[0].DefaultView.Count != 0)
			{
				this.Repeater1.DataSource = dsRet.Tables[0].DefaultView;
				Repeater1.DataBind();
			}
			else
				JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"]) + "&&Records=None");
		}
	}
}
