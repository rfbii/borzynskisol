using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Text;
using KeyCentral.Lookups;
using KeyCentral.Functions;
using System.Data.SqlClient;
using BankBalanceLib;
using System.IO;

namespace BankBalance.UI.Reports
{
	public partial class Report_BackOnlyBankBalance : BasePage
	{
		#region Declares
		protected System.Web.UI.WebControls.Repeater Repeater2;
		protected System.Web.UI.HtmlControls.HtmlGenericControl CommentLine;
		protected ReportTotalsHelper totalHelper;
		protected ReportSessionManager reportSessionManager;
		protected System.Web.UI.WebControls.Button cmdTransfer;
		private double lineCount = 0;
		private bool newPage = true;
		private bool firstPage = true;
		#endregion

		#region Param Properties
		private string AccountType { get { return Session["AccountType"].ToString(); } }
		private ArrayList Company { get { return (ArrayList)Session["CompanyList"]; } }
		private string AccountActive { get { return Session["AccountActive"].ToString(); } }
		private DataSet BankBalanceData { get { return (DataSet)Session["Data"]; } }
		#endregion
		#region Report Print
		#region Vars
		bool mainHeader = true;
		string lastCompany = "";
		string lastBank = "";
		bool companyChanged = true;
		bool BankChanged = false;
		double subLimit = 0;
		double subLoanBal = 0;
		double subLoanAvailable = 0;
		double subGLBalance = 0;
		double subLoan = 0;
		double subBalance = 0;
		double companyTotalLimit = 0;
		double companyTotalLoanBal = 0;
		double companyTotalLoanAvailable = 0;
		double companyTotalGLBalance = 0;
		double companyTotalLoan = 0;
		double companyTotalBalance = 0;
		double totalLimit = 0;
		double totalLoanBal = 0;
		double totalLoanAvailable = 0;
		double totalGLBalance = 0;
		double totalLoan = 0;
		double totalBalance = 0;
		#endregion
		public string CheckLineCount(object oItem)
		{
			if (firstPage == true)
			{
				if (lineCount >= 28)
				{
					newPage = true;
					firstPage = false;
					lineCount = 0;
				}
			}
			else if (firstPage == false)
			{
				if (lineCount >= 28)
				{
					newPage = true;
					lineCount = 0;
				}
			}
			return "";
		}
		public string CheckForChanges(object oItem)
		{
			if (lastCompany != GetField(oItem, "Deptname"))
			{
				companyChanged = true;
			}
			if (lastBank != (GetField(oItem, "BankName") + GetField(oItem, "Deptname")))
			{
				if (lastBank == "")
				{
					lastBank = (GetField(oItem, "BankName") + GetField(oItem, "Deptname"));
				}
				else
				{
					lastBank = (GetField(oItem, "BankName") + GetField(oItem, "Deptname"));
					BankChanged = true;
				}
			}
			return "";
		}
		public string Header(object oItem)
		{
			string header = "";
			if (newPage)
			{
				if (firstPage == false)
				{
					header = @"
						<!-- email end -->
						</table><div style='page-break-before:always'><span style='visibility: hidden;'><font style='font-size:0pt;'>&nbsp;</font></span></div>
						<table border='0' cellpadding='1' cellspacing='0' class='ReportTable'>
							<tr>
								<td class='Title2' width=266 height=21 align=center>Company</td>
								<td class='Title2' width=20 height=21 align=left>TN</td>
								<td class='Title2' width=108 height=21 align=center>Loan Limit</td>
								<td class='Title2' width=108 height=21 align=center>Loan Balance</td>
								<td class='Title2' width=108 height=21 align=center>Available Balance</td>
								<td class='Title2' width=108 height=21 align=center>G/L Balance</td>
								<td class='Title2' width=108 height=21 align=center>Total Loan</td>
								<td class='Title2' width=108 height=21 align=center>Balance Available</td>
							</tr>
						</table>					
					<!-- email start -->";
				}
				else
				{
					header = @"
						<table border='0' cellpadding='1' cellspacing='0' class='ReportTable'>
							<tr>
								<td class='Title2' width=266 height=21 align=center>Company</td>
								<td class='Title2' width=20 height=21 align=left>TN</td>
								<td class='Title2' width=108 height=21 align=center>Loan Limit</td>
								<td class='Title2' width=108 height=21 align=center>Loan Balance</td>
								<td class='Title2' width=108 height=21 align=center>Available Balance</td>
								<td class='Title2' width=108 height=21 align=center>G/L Balance</td>
								<td class='Title2' width=108 height=21 align=center>Total Loan</td>
								<td class='Title2' width=108 height=21 align=center>Balance Available</td>
							</tr>
						</table>";
				}

				newPage = false;
			}

			return header;
		}
		public string CompanyHeader(object oItem)
		{
			string header = "";
			if (companyChanged)
			{
				header = @"
							<table border='0' cellpadding='0' cellspacing='0' class='ReportTable'>
								<tr>
									<td class='Title5' height=21 width=952 >" + GetField(oItem, "DeptName") + @"</td>
								</tr>
							</table>";
				companyChanged = false;
				lastCompany = GetField(oItem, "Deptname");
				lineCount += 1;
			}

			return header;
		}
		public string SubTotal(object oItem)
		{
			string total = "";
			if (BankChanged && lastBank != "" || oItem == null)
			{
				total = @"
							<table border='0' cellpadding='0' cellspacing='0' class='ReportTable' class='ReportTable'>
								<tr>
									<td class='Title7A' width=266 height=21 align=left>Subtotal </td>
									<td class='Title7' width=20 height=21 align=left>&nbsp;</td>
									<td class='Title7' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(subLimit) + @"</td>
									<td class='Title7' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(subLoanBal) + @"</td>
									<td class='Title7' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(subLoanAvailable) + @"</td>
									<td class='Title7' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(subGLBalance) + @"</td>
									<td class='Title7' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(subLoan) + @"</td>
									<td class='Title7B' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(subBalance) + @"</td>
								</tr>
						</table>";
				BankChanged = false;
				companyTotalLimit += subLimit;
				companyTotalLoanBal += subLoanBal;
				companyTotalLoanAvailable += subLoanAvailable;
				companyTotalGLBalance += subGLBalance;
				companyTotalLoan += subLoan;
				companyTotalBalance += subBalance;
				subLimit = 0;
				subLoanBal = 0;
				subLoanAvailable = 0;
				subGLBalance = 0;
				subLoan = 0;
				subBalance = 0;
				lineCount += 1;
			}
			return total;
		}
		public string CompanyTotal(object oItem)
		{
			string total = "";
			if (companyChanged && lastCompany != "" || oItem == null)
			{
				total = @"
							<table border='0' cellpadding='0' cellspacing='0' class='ReportTable'>
								<tr>
									<td class='Title4A' width=266 height=21 align=left>Total </td>
									<td class='Title4' width=20 height=21 align=left>&nbsp;</td>
									<td class='Title4' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(companyTotalLimit) + @"</td>
									<td class='Title4' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(companyTotalLoanBal) + @"</td>
									<td class='Title4' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(companyTotalLoanAvailable) + @"</td>
									<td class='Title4' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(companyTotalGLBalance) + @"</td>
									<td class='Title4' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(companyTotalLoan) + @"</td>
									<td class='Title4B' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(companyTotalBalance) + @"</td>
								</tr>
						</table>";
				totalLimit += companyTotalLimit;
				totalLoanBal += companyTotalLoanBal;
				totalLoanAvailable += companyTotalLoanAvailable;
				totalGLBalance += companyTotalGLBalance;
				totalLoan += companyTotalLoan;
				totalBalance += companyTotalBalance;
				companyTotalLimit = 0;
				companyTotalLoanBal = 0;
				companyTotalLoanAvailable = 0;
				companyTotalGLBalance = 0;
				companyTotalLoan = 0;
				companyTotalBalance = 0;
				lineCount += 1;
			}
			return total;
		}
		public string Total()
		{
			string total = "";
			total = @"
							<table border='0' cellpadding='0' cellspacing='0' class='ReportTable'>
								<tr>
									<td class='Title6A' width=266 height=21 align=left>Grand Total </td>
									<td class='Title6' width=20 height=21 align=left>&nbsp;</td>
									<td class='Title6' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(totalLimit) + @"</td>
									<td class='Title6' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(totalLoanBal) + @"</td>
									<td class='Title6' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(totalLoanAvailable) + @"</td>
									<td class='Title6' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(totalGLBalance) + @"</td>
									<td class='Title6' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(totalLoan) + @"</td>
									<td class='Title6B' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(totalBalance) + @"</td>
								</tr>
							</table>";
			lineCount += 1;
			return total;
		}
		public string Detail(object oItem)
		{
			double availableBalance = 0;
			double totalLoan = 0;
			double balance = 0;
			string detail = "";
			if (GetField(oItem, "LoanLimit") != "0" || GetField(oItem, "LoanBalance") != "0")
			{
				availableBalance = (double.Parse(GetField(oItem, "LoanLimit")) - double.Parse(GetField(oItem, "LoanBalance")));
				totalLoan = (double.Parse(GetField(oItem, "LoanBalance")) - double.Parse(GetField(oItem, "GLBalance")));
				balance = (double.Parse(GetField(oItem, "LoanLimit")) - totalLoan);
			}
			else
			{
				balance = double.Parse(GetField(oItem, "GLBalance"));
			}
			if (GetField(oItem, "TN").ToString() == "0")
			{
				detail = @"
						<table border='0' cellpadding='0' cellspacing='0' class='ReportTable'>
							<tr>
								<td class='Detail2' width=265 height=21 align=left>" + GetField(oItem, "BankName").PadRight(28).Substring(0, 28) + @"</td>
								<td class='Title3' width=20 height=21 align=center>&nbsp;</td>
								<td class='Title3' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(decimal.Parse(GetField(oItem, "LoanLimit"))) + @"</td>
								<td class='Title3' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(decimal.Parse(GetField(oItem, "LoanBalance"))) + @"</td>
								<td class='Title3' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(availableBalance) + @"</td>
								<td class='Title3' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(decimal.Parse(GetField(oItem, "GLBalance"))) + @"</td>
								<td class='Title3' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(totalLoan) + @"</td>
								<td class='Detail1' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(balance) + @"</td>
							</tr>
						</table>";
			}
			else
			{
				detail = @"
						<table border='0' cellpadding='0' cellspacing='0' class='ReportTable'>
							<tr>
								<td class='Detail2' width=265 height=21 align=left>" + GetField(oItem, "BankName").PadRight(28).Substring(0, 28) + @"</td>
								<td class='Title3' width=20 height=21 align=center>" + GetField(oItem, "TN") + @"</td>
								<td class='Title3' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(decimal.Parse(GetField(oItem, "LoanLimit"))) + @"</td>
								<td class='Title3' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(decimal.Parse(GetField(oItem, "LoanBalance"))) + @"</td>
								<td class='Title3' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(availableBalance) + @"</td>
								<td class='Title3' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(decimal.Parse(GetField(oItem, "GLBalance"))) + @"</td>
								<td class='Title3' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(totalLoan) + @"</td>
								<td class='Detail1' width=110 height=21 align=right>" + Misc.DisplayHTMLForCurency(balance) + @"</td>
							</tr>
						</table>";
			}
			subLimit += double.Parse(GetField(oItem, "LoanLimit"));
			subLoanBal += double.Parse(GetField(oItem, "LoanBalance"));
			subLoanAvailable += availableBalance;
			subGLBalance += double.Parse(GetField(oItem, "GLBalance"));
			subLoan += totalLoan;
			subBalance += balance;
			lineCount += 1;
			return detail;
		}
		private string GetField(object oItem, string strField)
		{
			return DataBinder.Eval(oItem, "DataItem." + strField).ToString();
		}
		private string GetDateField(object oItem, string strField)
		{
			string str = DataBinder.Eval(oItem, "DataItem." + strField).ToString();
			if (str.IndexOf(" ", 0) > 0)
				return str.Substring(0, str.IndexOf(" ", 0));
			else
				return str;
		}
		#endregion

		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Bank Balance Report") == false)
				Response.Redirect("../../../KeyCentralABZ/UI/Menu/MenuMain.aspx");

			if (!IsPostBack)
			{
				JavaStatusMessage status = new JavaStatusMessage(this);
				status.ShowMessage("Computing");
			}

			#region SessionManager
			reportSessionManager = new ReportSessionManager(this, "BankBalanceReport");
			#endregion

			if (BankBalanceData.Tables.Count != 0)
			{
				BankBalanceData.Tables[0].DefaultView.Sort = "DeptName,BankName,TN";
				this.Repeater1.DataSource = BankBalanceData.Tables[0].DefaultView;
				Repeater1.DataBind();
			}
			else
				ShowReport();
		}
		protected void addTransfer_Click(object sender, System.EventArgs e)
		{
			JavaScriptRedirect("../../UI/Inputs/InputTransferBankBalance.aspx");
		}
		protected void SaveReport_Click(object sender, EventArgs e)
		{
			JavaStatusMessage status = new JavaStatusMessage(this);
			status.ShowMessage("Saving");

			#region Save PDF
			string saveFolderLocationStr = "";

			saveFolderLocationStr = ConfigurationManager.AppSettings["UploadFolder"] + "BankBalanceABZ";

			if (saveFolderLocationStr != "")
			{
				//delete PDF's older than today
				DirectoryInfo directoryInfo = new DirectoryInfo(saveFolderLocationStr);

				FileInfo[] fileInfo = directoryInfo.GetFiles("*.pdf");

				for (int i = fileInfo.Length - 1; i >= 0; i--)
				{
					FileInfo file = fileInfo[i];

					if (file.CreationTime < DateTime.Today)
					{
						file.Delete();
					}
				}

				string userKeyStr = Session["User_Key"].ToString();
				string timeStampStr = DateTime.Now.ToString().Replace("/", "-").Replace("\\", "-").Replace(":", ".");
				string fileNameStr = "BankBalanceReport_" + userKeyStr + "_" + timeStampStr + ".pdf";

				BankBalanceReportPDF bankBalanceReportPDF = new BankBalanceReportPDF();
				bankBalanceReportPDF.Main(saveFolderLocationStr, fileNameStr, BankBalanceData);

				this.RegisterStartupScript("PopUpReport", "<script>window.open('../../../UploadedFiles/BankBalanceABZ/" + fileNameStr + "','PDFReportWindow', 'directories=no,location=no,menubar=no,status=no,toolbar=no,scrollbars=no,resizable=yes');</script>");
			}
			#endregion
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}
		private void InitializeComponent()
		{

		}
		#endregion

		/// <summary>
		/// Get Bank Balance for all Companies.
		/// </summary>
		/// <returns></returns>
		private DataSet GetBankBalanceData()
		{
			DataSet reportData = new DataSet();
			foreach (CriteriaBoxRecord record in Company)
			{
				reportData.Merge(GetBankData(Misc.ParseTildaString(record.key, 1), record.text));
			}
			return reportData;
		}
		/// <summary>
		/// Get Bank Balance Data from Oracle
		/// </summary>
		/// <param name="oracleReportNumber"></param>
		/// <param name="name"></param>
		/// <returns></returns>
		private DataSet GetBankData(string oracleReportNumber, string name)
		{
			OracleConnection OracleConn = Misc.GetOracleConn(oracleReportNumber);
			DataSet bankData = new DataSet();
			#region Oracle Code
			string reportCommand = Misc.FormatOracle(oracleReportNumber + "_rpt", @"SELECT 
			Bank_Balance_View.*
							
			FROM    #COMPANY_NUMBER#.Bank_Balance_View
			
			Where
				Bank_Balance_View.Acrucashtype (+) = :Acrucash");
			#endregion"
			OracleDataAdapter OracleData = new OracleDataAdapter(reportCommand, OracleConn);
			OracleData.SelectCommand.BindByName = true;
			OracleData.SelectCommand.Parameters.Add(":Acrucash", AccountType);
			OracleData.Fill(bankData);
			OracleData.Dispose();
			Misc.CleanUpOracle(OracleConn);

			//Add column
			//bankData.Tables[0].Columns.Add("CompanyKey",typeof(System.String),"'" + oracleReportNumber + "'");
			bankData.Tables[0].Columns.Add(CreateColumnWithDefaultValue("CompanyKey", System.Type.GetType("System.String"), oracleReportNumber));
			bankData.Tables[0].Columns.Add(CreateColumnWithDefaultValue("LoanLimit", System.Type.GetType("System.Double"), 0f));
			bankData.Tables[0].Columns.Add(CreateColumnWithDefaultValue("LoanBalance", System.Type.GetType("System.Double"), 0f));
			bankData.Tables[0].Columns.Add(CreateColumnWithDefaultValue("TN", System.Type.GetType("System.Double"), 0f));
			return bankData;

		}
		/// <summary>
		/// Get Bank Account Data
		/// </summary>
		/// <returns></returns>
		private DataSet GetBankAccountData()
		{
			SqlConnection Conn = Misc.GetSQLConn();
			DataSet retData = new DataSet();
			SqlDataAdapter selectCMD = new SqlDataAdapter("use BankBalance; select A.Report_Number, Account_Type.Description, Account.Company_Key, Account.Account_Type_Key, Account.Account_Number, Account.Account_Link, Account.Account_Loan_Limit, Account.Active from Account Join [Account_Type] on [Account_Type].Account_Type_Key = [Account].Account_Type_Key Join KeyCentral.dbo.Company As A on A.Company_Key = [Account].Company_Key", Conn);
			selectCMD.Fill(retData);
			Conn.Close();
			Conn.Dispose();
			return retData;
		}

		private void ShowReport()
		{
			DataSet bankBalance = new DataSet();
			DataSet bankAccount = new DataSet();
			bankBalance = GetBankBalanceData();
			bankBalance.Tables[0].DefaultView.Sort = "DeptName,BankName";
			bankAccount = GetBankAccountData();
			if (AccountActive == "Active")
			{
				DeleteNonActiveRecords(bankBalance.Tables[0].DefaultView, bankAccount);
			}
			MergeBankRecords(bankBalance.Tables[0].DefaultView, bankAccount);
			if (bankBalance.Tables[0].DefaultView.Count != 0)
			{
				//Add DataSet to Session.
				reportSessionManager.AddSession("Data", bankBalance);
				//Let the reportSessionManager know that we are done
				reportSessionManager.SaveSessions();
				this.Repeater1.DataSource = bankBalance.Tables[0].DefaultView;
				Repeater1.DataBind();
			}
			else
				JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"]) + "&&Records=None");
		}
		#region Private Helpers
		/// <summary>
		/// Set Loan and balance if Line of credit.
		/// </summary>
		/// <param name="balance"></param>
		/// <param name="bank"></param>
		private void MergeBankRecords(DataView balance, DataSet bank)
		{
			for (int x = 0; x < balance.Count; x++)
			{
				for (int y = 0; y < bank.Tables[0].Rows.Count; y++)
				{
					// Find Bank Account in bank DataSet
					if (balance[x]["BankAccountNo"].ToString() == bank.Tables[0].Rows[y]["Account_Number"].ToString() && balance[x]["CompanyKey"].ToString() == bank.Tables[0].Rows[y]["Report_Number"].ToString())
					{
						// IF Line of Credit need to put G/L Balance on Loan Balance and add Loan Limit.
						if (bank.Tables[0].Rows[y]["Description"].ToString().ToLower().Substring(0, 4) == "line")
						{
							//Add to Linked account line.
							if (bank.Tables[0].Rows[y]["Account_Link"] != DBNull.Value)
							{
								for (int w = 0; w < balance.Count; w++)
								{
									//Find Linked Account line.
									if (bank.Tables[0].Rows[y]["Account_Link"].ToString() == balance[w]["BankAccountNo"].ToString())
									{
										balance[w]["LoanLimit"] = bank.Tables[0].Rows[y]["Account_Loan_Limit"];
										balance[w]["LoanBalance"] = (double.Parse(balance[x]["GLBalance"].ToString()) * -1);
										balance[x].Delete();
										x--;
										w = balance.Count;
										y = bank.Tables[0].Rows.Count;
									}
								}
							}
							else
							//If no Account to Link to.
							{
								balance[x]["LoanLimit"] = bank.Tables[0].Rows[y]["Account_Loan_Limit"];
								balance[x]["LoanBalance"] = (double.Parse(balance[x]["GLBalance"].ToString()) * -1);
								balance[x]["GLBalance"] = 0;
								y = bank.Tables[0].Rows.Count;
							}
						}
						else
							y = bank.Tables[0].Rows.Count;
					}
				}
			}
			balance.Table.AcceptChanges();
		}
		/// <summary>
		/// Delete Records if not Active.
		/// </summary>
		/// <param name="balance"></param>
		/// <param name="bank"></param>
		private void DeleteNonActiveRecords(DataView balance, DataSet bank)
		{
			string found = "";
			for (int x = 0; x < balance.Count; x++)
			{
				found = "no";
				for (int y = 0; y < bank.Tables[0].Rows.Count; y++)
				{
					// Find Bank Account in bank DataSet
					if (balance[x]["BankAccountNo"].ToString() == bank.Tables[0].Rows[y]["Account_Number"].ToString() && balance[x]["CompanyKey"].ToString() == bank.Tables[0].Rows[y]["Report_Number"].ToString())
					{
						// IF Line of Credit need to put G/L Balance on Loan Balance and add Loan Limit.
						if (bank.Tables[0].Rows[y]["Active"].ToString() != "True")
						{
							balance[x].Delete();
							x--;
							y = bank.Tables[0].Rows.Count;
							found = "yes";
						}
						else
						{
							y = bank.Tables[0].Rows.Count;
							found = "yes";
						}
					}
				}
				if (found == "no")
				{
					balance[x].Delete();
					x--;
				}

			}
			balance.Table.AcceptChanges();
		}

		private DataColumn CreateColumnWithDefaultValue(string columnsName, Type columnType, object value)
		{
			System.Data.DataColumn ret = new System.Data.DataColumn(columnsName, columnType);
			ret.DefaultValue = value;
			return ret;
		}
		#endregion
	}
}

