using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KeyCentral.Functions;
using KeyCentral.Lookups;
using System.Data.SqlClient;


namespace BankBalance.UI.Reports
{
    /// <summary>
    /// Summary description for Bank Balance.
    /// </summary>
    /// 

    public partial class CriteriaBankBalance : BasePage
    {
        protected ReportSessionManager reportSessionManager;
        //protected System.Web.UI.WebControls.Button addAll;
        //protected System.Web.UI.WebControls.Button removeAll;
        protected DataSet reportData = new DataSet();

        #region Events
        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckSecurity("Bank Balance Report") == false)
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/MenuMain.aspx");

            #region SessionManager
            reportSessionManager = new ReportSessionManager(this, Request["ReportName"].ToString());

            //Load Vars from session if this report is in SessionManager
            if (IsPostBack == false && reportSessionManager.CheckReportVars())
            {
                RadioTypeList.SelectedIndex = (int)Session["Type"];
                RadioActive.SelectedIndex = (int)Session["Active"];
                reportData = (DataSet)Session["Data"];
            }

            #endregion
            if (IsPostBack == false)
            {
                Message.Text = "";
                if (reportData.Tables.Count != 0)
                {
                    runOldReport.Message = "Do you want to Run Old Report?";
                    runOldReport.Visible = true;
                    Message.Text = "There is an old Report to see it click Run Old Report";
                }
                //Check for returned error messages
                if (Request.QueryString["Records"] == "None")
                    Message.Text = "No Records Found.";
                FillcompanyList();
                FillselectedList();
            }

        }
        protected void cmdReport_Click(object sender, System.EventArgs e)
        {
            if (selectedList.Items.Count > 0)
            {
                //Add Report Vars
                reportSessionManager.AddSession("Type", RadioTypeList.SelectedIndex);
                reportSessionManager.AddSession("AccountType", RadioTypeList.SelectedValue);
                reportSessionManager.AddSession("Active", RadioActive.SelectedIndex);
                reportSessionManager.AddSession("AccountActive", RadioActive.SelectedValue);
                reportSessionManager.AddSession("CompanyList", MakeArrayList());
                reportSessionManager.AddSession("Data", new DataSet());

                //Let the reportSessionManager know that we are done
                reportSessionManager.SaveSessions();

                //Build next page string and redirect to it
                Response.Redirect("Report_BackOnlyBankBalance.aspx" + "?LastPage=" + Misc.EncodeQueryString(Request.Path + "?ReportName=" + Request.QueryString["ReportName"]));
            }
            else
                Message.Text = "You must have at least one selected company.";
        }
        protected void Add_Click(object sender, System.EventArgs e)
        {
            if (companyList.SelectedIndex != -1)
            {
                SqlConnection Conn = GetSQLConn();
                SqlCommand insertCMD = new SqlCommand("use BankBalance;insert into [Selected_Company] (Company_Key) values(@Company)", Conn);
                insertCMD.Parameters.Add("@Company", Misc.ParseTildaString(companyList.SelectedItem.Value, 0));

                insertCMD.ExecuteNonQuery();
                /*add*/
                Message.Text = "Company Added.";
                companyList.Items.Clear();
                selectedList.Items.Clear();
                FillcompanyList();
                FillselectedList();
                Conn.Close();
                Conn.Dispose();
            }
            else
            {
                /*add*/
                Message.Text = "Please select a Company to add.";
            }
        }
        protected void Remove_Click(object sender, System.EventArgs e)
        {
            if (selectedList.SelectedIndex != -1)
            {
                //SqlConnection Conn = GetSQLConn();

                ////Remove entries from BB_Company where this is a detail.
                //SqlCommand removeCMDGroupDetail = new SqlCommand("use BankBalance; delete from [Account] where Company_Key = @Company_Key", Conn);
                //removeCMDGroupDetail.Parameters.Add("@Company_Key", Misc.ParseTildaString(selectedList.SelectedItem.Value, 0));
                //removeCMDGroupDetail.ExecuteNonQuery();
                //removeCMDGroupDetail = new SqlCommand("use BankBalance; delete from [Selected_Company] where Company_Key = @Company_Key", Conn);
                //removeCMDGroupDetail.Parameters.Add("@Company_Key", Misc.ParseTildaString(selectedList.SelectedItem.Value, 0));
                //removeCMDGroupDetail.ExecuteNonQuery();
                ///*add*/
                //Message.Text = "Company Removed.";
                //FillcompanyList();
                //FillselectedList();
                //Conn.Close();
                //Conn.Dispose();

                for (int i = selectedList.Items.Count - 1; i >= 0; i--)
                {
                    if (selectedList.Items[i].Selected)
                    {
                        ListItem item = selectedList.Items[i];
                        selectedList.Items.Remove(selectedList.Items[i]);
                        companyList.Items.Add(new ListItem(item.Text, item.Value));

                    }
                }
                selectedList.SelectedIndex = -1;
            }
            else
            {
                /*add*/
                Message.Text = "Please select a Company to remove.";
            }
        }
        /*private void addAll_Click(object sender, System.EventArgs e)
        {	
            FillselectedList();

            SqlConnection Conn = GetSQLConn();

            companyList.Items.Clear();
            SqlCommand insertCMD = new SqlCommand("use BankBalance; INSERT INTO Selected_Company (Company_Key) SELECT A.Company_Key FROM KeyCentral.dbo.Company As A", Conn);
            RemoveAllCompany(Conn);
            insertCMD.ExecuteNonQuery();
            addMessage.Text = "All Companies Added.";
            Conn.Close();
            Conn.Dispose();
            FillcompanyList();
            FillselectedList();
        }
        private void removeAll_Click(object sender, System.EventArgs e)
        {	
            SqlConnection Conn = GetSQLConn();
            RemoveAllCompany(Conn);
            addMessage.Text = "All Companies Removed.";
            Conn.Close();
            Conn.Dispose();
            selectedList.Items.Clear();
            FillcompanyList();
            FillselectedList();
        }*/
        protected void runOldReport_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("Report_BackOnlyBankBalance.aspx" + "?LastPage=" + Misc.EncodeQueryString(Request.Path + "?ReportName=" + Request.QueryString["ReportName"]));
        }
        private void RemoveAllCompany(SqlConnection Conn)
        {
            SqlCommand removeCMD = new SqlCommand("use BankBalance; delete from [Selected_Company]", Conn);
            removeCMD.ExecuteNonQuery();
        }
        protected void cmdMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Bank");
        }
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        #region Private helpers
        private void FillcompanyList()
        {
            SqlConnection Conn = GetSQLConn();
            SqlCommand selectCMD = new SqlCommand("use KeyCentral; select A.Company_Name, cast(A.Company_Key as varchar(4))+ '~' + A.Report_Number as Company_Key from [Company] As A where A.Company_Key not in (select B.Company_Key from BankBalance.dbo.Selected_Company As B) Order By Company_Name", Conn);

            SqlDataReader myReader = selectCMD.ExecuteReader();

            if (myReader.HasRows == true)
            {
                companyList.DataSource = myReader;
                companyList.DataTextField = "Company_Name";
                companyList.DataValueField = "Company_Key";
                companyList.DataBind();
            }
            Conn.Close();
            Conn.Dispose();
        }
        private void FillselectedList()
        {
            SqlConnection Conn = GetSQLConn();
            SqlCommand selectCMD = new SqlCommand("use KeyCentral; select A.Company_Name, cast(A.Company_Key as varchar(4))+ '~' + A.Report_Number as Company_Key from [Company] As A join BankBalance.dbo.Selected_Company As B on B.Company_Key = A.Company_Key Order by Company_Name", Conn);

            SqlDataReader myReader = selectCMD.ExecuteReader();

            //if(myReader.HasRows ==true)
            //{				 
            selectedList.DataSource = myReader;
            selectedList.DataTextField = "Company_Name";
            selectedList.DataValueField = "Company_Key";
            selectedList.DataBind();
            //}
            Conn.Close();
            Conn.Dispose();

        }
        private ArrayList MakeArrayList()
        {
            ArrayList newCriteria = new ArrayList();
            foreach (ListItem item in selectedList.Items)
                newCriteria.Add(new CriteriaBoxRecord(item.Value, item.Text, ""));

            /*
                        for(int x=0;x<selectedList.Items.Count;x++)
                        {
                        selectedList.Items[x].Value;
                        selectedList.SelectedIndex = x;
                        newCriteria.Add(new DynamicCriteriaBox.CriteriaBoxRecord(selectedList.SelectedValue,selectedList.SelectedItem.Text,""));
                        }*/
            return newCriteria;
        }
        #endregion
    }
}
