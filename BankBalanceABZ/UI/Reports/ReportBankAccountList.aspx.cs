using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Text;
//using KeyCentral.Lookups;
using KeyCentral.Functions;
using System.Data.SqlClient;


namespace BankBalance.UI.Reports
{
	public partial class ReportBankAccountList :  BasePage
	{
		#region Declares
		protected System.Web.UI.WebControls.Repeater Repeater2;
		protected System.Web.UI.HtmlControls.HtmlGenericControl CommentLine;
		protected ReportTotalsHelper totalHelper;
		#endregion
	
		#region Param Properties
		private string AccountType{get{return Session["AccountType"].ToString();}}
		private ArrayList Company{get{return (ArrayList)Session["CompanyList"];}}
		#endregion
		#region Report Print
		#region Vars
		bool mainHeader = true;
		#endregion
		public string Header(object oItem)
		{
			string header="";
			if(mainHeader)
			{
				header = @"
							<table border='0' cellpadding='0' cellspacing='0' class='ReportTable'>
								<tr>
									<td class='Title3' width=250 height=21 align=left>Company</td>
									<td class='Title3' width=120 height=21 align=left>Account Type</td>
									<td class='Title3' width=100 height=21 align=left>Account #</td>
									<td class='Title3' width=100 height=21 align=left>Account Link</td>
									<td class='Title3' width=100 height=21 align=right>Loan Limit</td>
									<td class='Title3' width=50 height=21 align=right>Active</td>
								</tr>
							</table>";
				mainHeader = false;
			}
			
			return header;
		}
		public string Detail(object oItem)
		{
			string detail="";
			string active="";
				if(GetField(oItem,"Active") == "True")
					active = "X";
				else
					active="";

				detail = @"
							<table border='0' cellpadding='0' cellspacing='0' class='ReportTable'>
								<tr>
									<td class='Title4' width=250 height=21 align=left>" + GetField(oItem,"Company_Name") + @"</td>
									<td class='Title4' width=120 height=21 align=left>"+ GetField(oItem,"Description") + @"</td>
									<td class='Title4' width=100 height=21 align=left>"+ GetField(oItem,"Account_Number") + @"</td>
									<td class='Title4' width=100 height=21 align=left>&nbsp"+ GetField(oItem,"Account_Link") + @"</td>
									<td class='Title4' width=100 height=21 align=right>"+ Misc.DisplayHTMLForCurency(GetField(oItem,"Account_Loan_Limit",0)) + @"</td>
									<td class='Title4' width=50 height=21 align=right>&nbsp"+ active + @"</td>
								</tr>
							</table>";
				return detail;
		}
		private string GetField(object oItem,string strField)
		{
			return DataBinder.Eval(oItem,"DataItem." + strField).ToString();
		}
		private string GetDateField(object oItem,string strField)
		{
			string str = DataBinder.Eval(oItem,"DataItem." + strField).ToString();
			if(str.IndexOf(" ",0)>0)
				return str.Substring(0,str.IndexOf(" ",0));
			else
				return str;
		}
		private double GetField(object oItem,string strField,float defaultValue)
		{
			object value = DataBinder.Eval(oItem,"DataItem." + strField);
			if(value != null && value != DBNull.Value)
				return double.Parse(value.ToString());
			else
				return double.Parse(defaultValue.ToString());
		}
		#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Admin") == false)
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/MenuMain.aspx");
			
			JavaStatusMessage status = new JavaStatusMessage(this);
			status.ShowMessage("Computing");
			ShowReport();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}
		
		private void InitializeComponent()
		{    

		}
		#endregion
		
		
		
		
		private void ShowReport()
		{	
			SqlConnection Conn = Misc.GetSQLConn();
			DataSet retData = new DataSet();
			SqlDataAdapter selectCMD = new SqlDataAdapter("use BankBalance; select A.Company_Name, Account_Type.Description, Account.Company_Key, Account.Account_Type_Key, Account.Account_Number, Account.Account_Link, Account.Account_Loan_Limit, Account.Active from [Account] Join KeyCentral.dbo.Company As A on A.Company_Key = [Account].Company_key Join [Account_Type] on [Account_Type].Account_Type_Key = [Account].Account_Type_Key", Conn);
			
			selectCMD.Fill(retData);
			retData.Tables[0].DefaultView.Sort = "Company_Name";
			if (retData.Tables[0].DefaultView.Count != 0)
			{
				this.Repeater1.DataSource = retData.Tables[0].DefaultView;
				Repeater1.DataBind();
			}
			else
				JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"])+ "&&Records=None");
		}
		
	}
}
