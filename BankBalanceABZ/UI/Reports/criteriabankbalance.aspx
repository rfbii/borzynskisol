<%@ Page language="c#" Inherits="BankBalance.UI.Reports.CriteriaBankBalance" Codebehind="CriteriaBankBalance.aspx.cs" %>
<%@ Register TagPrefix="cc1" Namespace="Cet" Assembly="Cet.ConfirmButton" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Bank Balance</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr height="20">
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center">Bank Balance
						</td>
					</tr>
					<tr height="10">
						<td>&nbsp;</td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td width="300"><asp:label id="Label1" runat="server">Company Available</asp:label></td>
						<td width="180"></td>
						<td width="300"><asp:label id="Label2" runat="server">Company Selected</asp:label></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td valign="top" width="300"><asp:listbox id="companyList" style="Z-INDEX: 103; LEFT: 8px" runat="server" Height="204px" Width="300px"></asp:listbox></td>
						<td align="center"><asp:button id="add" runat="server" Width="100" Text=">" onclick="Add_Click"></asp:button><br/>
							<br/>
							<asp:button id="remove" runat="server" Width="100" Text="<" onclick="Remove_Click"></asp:button></td>
						<td valign="top" width="300"><asp:listbox id="selectedList" style="Z-INDEX: 103; LEFT: 8px" runat="server" Height="204px"
								Width="300px" SelectionMode="Multiple"></asp:listbox></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td width="150"></td>
						<td align="right" width="210"><asp:radiobuttonlist id="RadioTypeList" runat="server">
								<asp:ListItem Value="A" Selected="True">Accrual</asp:ListItem>
								<asp:ListItem Value="C">Cash</asp:ListItem>
							</asp:radiobuttonlist></td>
						<td width="210"><asp:radiobuttonlist id="RadioActive" runat="server">
								<asp:ListItem Value="Active" Selected="True">Active Accounts</asp:ListItem>
								<asp:ListItem Value="All">All Accounts</asp:ListItem>
							</asp:radiobuttonlist></td>
						<td width="150"></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td width="300" align="right" style="height: 24px"><cc1:confirmbutton id="runOldReport" style="Z-INDEX: 113" runat="server" Text="Run Old Report" Width="160px"
								Visible="false" onclick="runOldReport_Click"></cc1:confirmbutton></td>
						<td align="center" width="180" style="height: 24px"><asp:button id="cmdReport" runat="server" Text="Run Report" EnterAsTab="" TabFinish="" onclick="cmdReport_Click"></asp:button></td>
						<td width="300" style="height: 24px"><asp:button id="cmdMenu" runat="server" Text="Return to Bank Menu" OnClick="cmdMenu_Click"></asp:button></td>
					</tr>
				</table>
				<asp:label id="addMessage" style="Z-INDEX: 101; LEFT: 8px" runat="server" ForeColor="Red"></asp:label><asp:label id="Message" runat="server" ForeColor="Red">Message</asp:label>
			</div>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
		</form>
	</body>
</html>