<%@ Page EnableViewState="True" language="c#" Inherits="BankBalance.UI.Reports.Report_BackOnlyBankBalance" Codebehind="Report_BackOnlyBankBalance.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Bank Balance</title> 
		<!-- Email Start -->
		<style type="text/css">.ReportTable.TD { FONT-SIZE: 8pt }
	.Title { FONT-WEIGHT: bold; FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: Arial }
	.Header { BORDER-TOP: black 1px solid; FONT-WEIGHT: bold; FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: Arial; BACKGROUND-COLOR: silver }
	.Title1 { FONT-SIZE: 14pt; FONT-FAMILY: Arial }
	.Title2 { BORDER-RIGHT: black 1px solid; FONT-SIZE: 11pt; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Arial }
	.Title3 { BORDER-RIGHT: black 1px solid; FONT-SIZE: 11pt; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Arial }
	.Detail1 { BORDER-RIGHT: black double; FONT-SIZE: 11pt; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Arial }
	.Detail2 { BORDER-RIGHT: black 1px solid; FONT-SIZE: 11pt; BORDER-LEFT: black double; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Arial }
	.Title4 { BORDER-RIGHT: black 1px solid; FONT-WEIGHT: bold; FONT-SIZE: 11pt; FONT-FAMILY: Arial }
	.Title4A { FONT-WEIGHT: bold; FONT-SIZE: 11pt; BORDER-LEFT: black double; FONT-FAMILY: Arial }
	.Title4B { BORDER-RIGHT: black double; FONT-WEIGHT: bold; FONT-SIZE: 11pt; FONT-FAMILY: Arial }
	.Title5 { BORDER-RIGHT: black double; BORDER-TOP: black double; FONT-WEIGHT: bold; FONT-SIZE: 11pt; BORDER-LEFT: black double; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Arial }
	.Title6 { BORDER-RIGHT: black 1px solid; BORDER-TOP: black double; FONT-WEIGHT: bold; FONT-SIZE: 11pt; BORDER-BOTTOM: black double; FONT-FAMILY: Arial }
	.Title6A { BORDER-TOP: black double; FONT-WEIGHT: bold; FONT-SIZE: 11pt; BORDER-LEFT: black double; BORDER-BOTTOM: black double; FONT-FAMILY: Arial }
	.Title6B { BORDER-RIGHT: black double; BORDER-TOP: black double; FONT-WEIGHT: bold; FONT-SIZE: 11pt; BORDER-BOTTOM: black double; FONT-FAMILY: Arial }
	.Title7 { BORDER-RIGHT: black 1px solid; FONT-WEIGHT: bold; FONT-SIZE: 11pt; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Arial }
	.Title7A { FONT-WEIGHT: bold; FONT-SIZE: 11pt; BORDER-LEFT: black double; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Arial }
	.Title7B { BORDER-RIGHT: black double; FONT-WEIGHT: bold; FONT-SIZE: 11pt; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Arial }
		</style>
		<!-- Email End -->
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<link href="/KeystoneCustomerStatus/Inc/_styles.css" type="text/css" rel="stylesheet" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body bgcolor="#ffffff" onload="SetupPrinting();">
		<object id="factory" style="DISPLAY: none" codebase="http://www.meadroid.com/scriptx/ScriptX.cab#Version=6,1,431,2"
			classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814" viewastext="">
		</object>
		<script type="text/javascript" language="javascript">
		function SetupPrinting()
		{
			factory.printing.footer = "Printed: &d &t&bPage &p of &P&bBank Balance Report";
			factory.printing.header = "";
			factory.printing.leftMargin = .5;
			factory.printing.rightMargin = .5;
			factory.printing.topMargin = .5;
			factory.printing.bottomMargin = .5
			factory.printing.portrait =false;
		}  
		</script>
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div id="BodyDiv" class="scroll">
				
				<table cellspacing="0" cellpadding="0" border="0" width="960" class="ReportTable">
					<tr>
						<td align="center" width="180"><asp:button id="addTransfer" runat="server" Text="Add Transfer" onclick="addTransfer_Click"></asp:button></td>
						<td align="center" width="180"><asp:button id="SaveReport" EnterAsTab="" tabstop="" runat="server" Text="Save PDF" Width="150" OnClick="SaveReport_Click"></asp:button></td>
					</tr>
				</table>
				<!-- Email Start -->
				<table cellspacing="0" cellpadding="0" border="0" width="960" class="ReportTable">
					<tr>
						<td align="center" width="960" class='Title'>Bank Balance Report</td>
					</tr>
					<tr>
					</tr>
					<tr>
						<td height="20"><font size="2">&nbsp;</font></td>
					</tr>
				</table>
				<table style="COLOR: black" bgcolor="white" class="ReportTable">
					<tr>
						<td></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" border="0" width="960" class="ReportTable">
					<asp:repeater id="Repeater1" runat="server">
						<ItemTemplate>
							<%# CheckForChanges(Container)%>
							<%# SubTotal(Container)%>
							<%# CompanyTotal(Container)%>
							<%# CheckLineCount(Container)%>
							<%# Header(Container)%>
							<%# CompanyHeader(Container)%>
							<%# Detail(Container)%>
						</ItemTemplate>
						<FooterTemplate>
							<%# SubTotal(null)%>
							<%# CompanyTotal(null)%>
							<%# Total()%>
						</FooterTemplate>
					</asp:repeater>
				</table>
				<table class="ReportTable">
					<tr>
						<td height="40"><font size="2">&nbsp;</font></td>
					</tr>
				</table>
				<!-- Email End -->
			</div>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
		</form>
	</body>
</html>