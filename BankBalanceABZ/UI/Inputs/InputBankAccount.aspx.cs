using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using KeyCentral.Functions;

namespace BankBalance.UI.Inputs
{
	/// <summary>
	/// Summary description for Security Type.
	/// </summary>
	/// 
	
	public partial class InputBankAccount : BasePage
	{
		#region Declares

		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator1;
		#endregion

		#region events
		protected void Page_Load(object sender, System.EventArgs e)
		{
		}
		protected void AvailableList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			FillScreen();
			Message.Text = "";
			deleteBankAccount.Message= "Do you want to delete " + AvailableList.SelectedItem.Text + "  Bank Account?";
			deleteBankAccount.Enabled=true;
			Save.Enabled=true;
			canel.Enabled=true;
		}
		protected void Save_Click(object sender, System.EventArgs e)
		{
			Page.Validate();
			if (Page.IsValid)
			{
				SqlConnection Conn = GetSQLConn();
				SqlCommand selectCMD = new SqlCommand("use BankBalance;select Company_Key,Account_Type_Key,Account_Number,Account_Link,Account_Loan_Limit,Active from [Account] where Company_Key=@CompanyKey and Account_Type_Key = @AccountKey and Account_Number = @AccountNo", Conn);
				SqlCommand updateCMD = new SqlCommand("use BankBalance;update [Account] Set Account_Link=@AccountLink,Account_Loan_Limit=@LoanLimit,Active=@Active where Company_Key=@CompanyKey and Account_Type_Key = @AccountKey and Account_Number = @AccountNo", Conn);
				selectCMD.Parameters.Add("@CompanyKey",CompanyKey.Text);
				selectCMD.Parameters.Add("@AccountKey",AccountTypeKey.Text);
				selectCMD.Parameters.Add("@AccountNo",AccountNo.Text);
				updateCMD.Parameters.Add("@CompanyKey",CompanyKey.Text);
				updateCMD.Parameters.Add("@AccountKey",AccountTypeKey.Text);
				updateCMD.Parameters.Add("@AccountNo",AccountNo.Text);
				if(AccountLink.Text.Trim() == "")
					updateCMD.Parameters.Add("@AccountLink",DBNull.Value);
				else
					updateCMD.Parameters.Add("@AccountLink",AccountLink.Text);
				if(LoanLimit.Text.Trim() == "")
					updateCMD.Parameters.Add("@LoanLimit",DBNull.Value);
				else
					updateCMD.Parameters.Add("@LoanLimit",LoanLimit.Text);
				updateCMD.Parameters.Add("@Active",BoolToSQlBit(Active.Checked));
			
			
				SqlDataReader myReader = selectCMD.ExecuteReader();
				if(myReader.Read()==true) //Existing Bank Account
				{
					myReader.Close();
					updateCMD.ExecuteNonQuery();
					Message.Text = "Bank Account Updated.";
				}
				Conn.Close();
				Conn.Dispose();	
				FillBankAccountList();
				this.SetFocus("AvailableList");
				deleteBankAccount.Enabled=false;
				Save.Enabled=false;
				canel.Enabled=false;
			}
		}
		protected void deleteBankAccount_Click(object sender, System.EventArgs e)
		{
			SqlConnection Conn = GetSQLConn();
			//Remove Bank Account
			SqlCommand removeCMD = new SqlCommand("use BankBalance;delete from [Account] where Company_Key=@CompanyKey and Account_Type_Key = @Account_Type_Key and Account_Number = @AccountNo", Conn);
			removeCMD.Parameters.Add("@Account_Type_Key",AccountTypeKey.Text);
			removeCMD.Parameters.Add("@CompanyKey",CompanyKey.Text);
			removeCMD.Parameters.Add("@AccountNo",AccountNo.Text);
			removeCMD.ExecuteNonQuery();
			FillBankAccountList();
			Message.Text = " Bank Account Deleted.";	
			deleteBankAccount.Enabled=false;
			Save.Enabled=false;
			canel.Enabled=false;
			CompanyKey.Text = "";
			AccountTypeKey.Text = "";
			AccountNo.Text = "";
			AccountLink.Text = "";
			LoanLimit.Text = "";
			Company.Text = "";
			AccountType.Text = "";
			Active.Checked = false;
			this.SetFocus("AvailableList");
		}
		protected void canel_Click(object sender, System.EventArgs e)
		{
			Message.Text = "";	
			CompanyKey.Text = "";
			AccountTypeKey.Text = "";
			AccountNo.Text = "";
			AccountLink.Text = "";
			LoanLimit.Text = "";
			Company.Text = "";
			AccountType.Text = "";
			Active.Checked = false;
			this.SetFocus("AvailableList");
			deleteBankAccount.Enabled=false;
			Save.Enabled=false;
			canel.Enabled=false;
			AvailableList.SelectedIndex = -1;
		}
		protected void Return_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Bank");			
		}
		protected void Add_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("InputAddBankAccount.aspx");
		}
		protected void Print_Click(object sender, System.EventArgs e)
		{
			//Response.Redirect("ReportBankAccountList.aspx");
            Response.Redirect("../../../BankBalanceABZ/UI/Reports/ReportBankAccountList.aspx" + "?LastPage=" + Misc.EncodeQueryString(Request.Path + "?ReportName=" + Request.QueryString["ReportName"]));
		}
		#endregion
			
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			FillBankAccountList();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Private helpers
		private void FillBankAccountList()
		{
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("use BankBalance; select A.Company_Name + ' ' + C.Description + ' ' + B.Account_Number as Description, cast(B.Company_Key as varchar(4))+ '~' + cast(B.Account_Type_Key as varchar(4)) + '~' + B.Account_Number + '~' + cast(B.Active as varchar(4)) as Account_Type_Key from [Account] As B Join KeyCentral.dbo.Company As A on A.Company_Key = B.Company_key Join Account_Type As C on C.Account_Type_Key = B.Account_Type_Key", Conn);
		
			SqlDataReader myReader = selectCMD.ExecuteReader();
			
			//if(myReader.HasRows ==true)
			//{				 
				AvailableList.DataSource = myReader;
				AvailableList.DataTextField = "Description";
				AvailableList.DataValueField = "Account_Type_Key";
				AvailableList.DataBind();	
			//}
			foreach( ListItem item in AvailableList.Items)
			{
				string test="";
				if(Misc.ParseTildaString(item.Value,3) == "1")
					item.Text = "Active " + item.Text;
				else
				{
					test = "__________" + item.Text;
					item.Text = test.Replace("_",Server.HtmlDecode("&nbsp;"));
				}
			} 
            Conn.Close();
			Conn.Dispose();	  		
					
		}	
		private void FillScreen()
		{
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("use BankBalance;select A.Company_Name,Account_Type.Description,Account.Company_Key,Account.Account_Type_Key,Account_Number,Account_Link,Account_Loan_Limit,Active from [Account] Join  KeyCentral.dbo.Company As A on A.Company_Key = [Account].Company_key Join [Account_Type] on [Account_Type].Account_Type_Key = [Account].Account_Type_Key where [Account].Company_Key=@CompanyKey and [Account].Account_Type_Key = @AccountKey and [Account].Account_Number = @AccountNo", Conn);
			selectCMD.Parameters.Add("@CompanyKey",Misc.ParseTildaString(AvailableList.SelectedValue,0));
			selectCMD.Parameters.Add("@AccountKey",Misc.ParseTildaString(AvailableList.SelectedValue,1));
			selectCMD.Parameters.Add("@AccountNo",Misc.ParseTildaString(AvailableList.SelectedValue,2));
			SqlDataReader myReader = selectCMD.ExecuteReader();
			if(myReader.Read()==true) //Existing Bank Account
			{
				CompanyKey.Text = myReader.GetValue(2).ToString();
				Company.Text = myReader.GetValue(0).ToString();
				AccountType.Text = myReader.GetValue(1).ToString();
				AccountTypeKey.Text = myReader.GetValue(3).ToString();
				AccountNo.Text = myReader.GetValue(4).ToString();
				AccountLink.Text = myReader.GetValue(5).ToString();
				LoanLimit.Text = myReader.GetValue(6).ToString();
				if (myReader.GetValue(7).ToString() == "True")
					Active.Checked = true;
				else
				    Active.Checked = false;
			}
			else
			{
				Message.Text = "Please select a Bank Account";
			}
		Conn.Close();
		Conn.Dispose();		
		}
		private string BoolToSQlBit(bool value)
		{
			if(value)
				return "1";
			else
				return "0";
		}		
		#endregion	

		

		

		

		
		
		
	}
}
