using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;
using System.Configuration;
using KeyCentral.Functions;

namespace BankBalance.UI.Inputs
{
	/// <summary>
    /// Summary Input Check Writing User.
	/// </summary>
	/// 

    public partial class InputCheckWritingUser : BasePage
	{
		#region Declares
        protected ReportSessionManager reportSessionManager;
		
		#endregion

		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
            if (CheckSecurity("Check Writing Number") == false)
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/MenuMain.aspx");
            
            reportSessionManager = new ReportSessionManager(this, Request["ReportName"].ToString());
			
            if (!IsPostBack)
            {
                FillCompanyDdl();
            }
		}
        protected void CompanyDdl_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (CompanyDdl.SelectedValue != "")
            {
                BankNameDdl.Enabled = true;
               
                FillBankNameDdl(Misc.ParseTildaString(CompanyDdl.SelectedValue, 1));

            }
            else if (CompanyDdl.SelectedValue == "")
            {
                BankNameDdl.SelectedIndex = -1;
                BankNameDdl.ClearSelection();
                BankNameDdl.Items.Clear();

                BankNameDdl.Enabled = false;
            }
        }
        protected void BankNameDdl_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            string userName = Session["User_Name"].ToString();
               
            SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("use BankBalance;select Company_Key, Bank_Name_Key, User_Name as UserName from [Check_Writing_User] where Company_Key=@Company_Key and Bank_Name_Key=@Bank_Name_Key", Conn);
			selectCMD.Parameters.Add("@Company_Key",Misc.ParseTildaString(CompanyDdl.SelectedValue, 0));
			selectCMD.Parameters.Add("@Bank_Name_Key",BankNameDdl.SelectedValue);
			
			SqlDataReader myReader = selectCMD.ExecuteReader();
			if(myReader.Read()==true) //Account in use
			{
                if (userName.ToUpper() == myReader.GetValue(2).ToString().ToUpper())
                {
                    myReader.Close();
                    WriteChecksBtn.Enabled = true;
                    Message.Text = "";
                }
                else
                { 
                    string Company = GetCompany(myReader.GetValue(0).ToString());
                    string BankAccount = GetBankAccount(Misc.ParseTildaString(CompanyDdl.SelectedValue, 1), myReader.GetValue(1).ToString());
                    string UserName = myReader.GetValue(2).ToString();
                    WriteChecksBtn.Enabled = false;
                    Message.Text = Company + ", " + BankAccount + "  is in use by " + UserName;
                }
			}
			else  //Account Available
			{
				myReader.Close();
                WriteChecksBtn.Enabled = true;
                Message.Text = "";
               
			}
			
            Conn.Close();
			Conn.Dispose();		
            
        }

        protected void WriteChecks_Click(object sender, System.EventArgs e)
		{
            string username = Session["User_Name"].ToString();
            SqlConnection Conn = GetSQLConn();
            SqlCommand selectCMD = new SqlCommand("use BankBalance;select Company_Key, Bank_Name_Key, User_Name as UserName from [Check_Writing_User] where Company_Key=@Company_Key and Bank_Name_Key=@Bank_Name_Key", Conn);
			selectCMD.Parameters.Add("@Company_Key",Misc.ParseTildaString(CompanyDdl.SelectedValue, 0));
			selectCMD.Parameters.Add("@Bank_Name_Key",BankNameDdl.SelectedValue);
			
			SqlDataReader myReader = selectCMD.ExecuteReader();
            if (myReader.Read() != true)
            {
                myReader.Close();
                SqlCommand insertCMD = new SqlCommand("use BankBalance;insert into [Check_Writing_User] (Company_Key,Bank_Name_Key,User_Name) values(@Company_Key,@Bank_Name_Key,@User_Name)", Conn);
                insertCMD.Parameters.AddWithValue("@Company_Key", Misc.ParseTildaString(CompanyDdl.SelectedValue, 0));
                insertCMD.Parameters.AddWithValue("@Bank_Name_Key", BankNameDdl.SelectedValue);
                insertCMD.Parameters.AddWithValue("@User_Name", username);

                insertCMD.ExecuteNonQuery();

             }
            Conn.Close();
            Conn.Dispose();

            //Add Report Vars
            reportSessionManager.AddSession("CompanyName", CompanyDdl.SelectedItem.Text);
            reportSessionManager.AddSession("CompanyKey", Misc.ParseTildaString(CompanyDdl.SelectedValue, 0));
            reportSessionManager.AddSession("CompanyReportNumber", Misc.ParseTildaString(CompanyDdl.SelectedValue, 1));
            reportSessionManager.AddSession("BankAccountName", BankNameDdl.SelectedItem);
            reportSessionManager.AddSession("BankAccountKey", BankNameDdl.SelectedValue);

            //Let the reportSessionManager know that we are done
            reportSessionManager.SaveSessions();

            Response.Redirect("../../../BankBalanceABZ/UI/Inputs/InputCheckWritingScreen.aspx");
           
		 }
		protected void Return_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Bank");			
		}
		#endregion
			
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Private helpers
        private void FillCompanyDdl()
        {
            CompanyDdl.Items.Clear();
            CompanyDdl.Items.Add(new ListItem());
            
            SqlConnection Conn = GetSQLConn();
            SqlCommand selectCMD = new SqlCommand("use KeyCentral;select [Company].Company_Name,[Company].Company_Key,[Company].Report_Number from [Company] Order By Company_Name", Conn);

            SqlDataReader myReader = selectCMD.ExecuteReader();

            while (myReader.Read())
            {
                string CommodityName = myReader.GetValue(0).ToString();
                string CommodityKey = myReader.GetValue(1).ToString() + "~" + myReader.GetValue(2).ToString();

                CompanyDdl.Items.Add(new ListItem(CommodityName, CommodityKey));
            }
            Conn.Close();
            Conn.Dispose();

        }

        private void FillBankNameDdl(string CompanyKey)
        {
            BankNameDdl.SelectedIndex = -1;
            BankNameDdl.ClearSelection();
            BankNameDdl.Items.Clear();
            BankNameDdl.Items.Add(new ListItem());

            OracleConnection OracleConn = Misc.GetOracleConn(CompanyKey);
			DataSet bankData = new DataSet();
			
            string selectCMD = Misc.FormatOracle(CompanyKey + "_rpt", @"SELECT 
			Fc_Name.LastCoName,
            Bank.BankNameidx
            from Bank,
                 Fc_Name
            Where Bank.BankNameidx = Fc_Name.Nameidx 
            ");

            OracleCommand OracleCmd = new OracleCommand(selectCMD, OracleConn);
           
            OracleDataReader myReader = OracleCmd.ExecuteReader();

            while (myReader.Read())
            {
                string BankAccountName = myReader.GetValue(0).ToString();
                string BankAccountKey = myReader.GetValue(1).ToString();

                BankNameDdl.Items.Add(new ListItem(BankAccountName, BankAccountKey));
            }

            myReader.Close();
            Misc.CleanUpOracle(OracleConn);
        }
       
        private string GetCompany(string companyKey)
        {
            string returnCompany = "";
           
            SqlConnection Conn = GetSQLConn();
            SqlCommand selectCMD = new SqlCommand("use KeyCentral;select [Company].Company_Name from [Company] where Company_Key=@Company_Key", Conn);
            selectCMD.Parameters.Add("@Company_Key", companyKey);

            SqlDataReader myReader = selectCMD.ExecuteReader();

            while (myReader.Read())
            {
                returnCompany = myReader.GetValue(0).ToString();
            }

            myReader.Close();
            myReader.Dispose();

            return returnCompany;

        }
        private string GetBankAccount(string companyKey, string bankAccountKey)
        {
            string returnBankAccount = "";
                
            OracleConnection OracleConn = Misc.GetOracleConn(companyKey);
            DataSet bankData = new DataSet();

            string selectCMD = Misc.FormatOracle(companyKey + "_rpt", @"SELECT 
			Fc_Name.LastCoName,
            Bank.BankNameidx
            from Bank,
                 Fc_Name
            Where Bank.BankNameidx = Fc_Name.Nameidx and
                  Bank.BankNameidx = :BankAccountKey
            ");


            OracleCommand OracleCmd = new OracleCommand(selectCMD, OracleConn);
            OracleCmd.Parameters.Add(":BankAccountKey", bankAccountKey);

			OracleCmd.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader2 = OracleCmd.ExecuteReader();

			while (myReader2.Read())
			{
				returnBankAccount = myReader2.GetValue(0).ToString();
			}

			myReader2.Close();
			myReader2.Dispose();
			OracleCmd.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();

			return returnBankAccount;
            
        }
		#endregion	
		
	}
}
