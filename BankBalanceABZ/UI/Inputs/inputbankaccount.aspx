<%@ Page language="c#" Inherits="BankBalance.UI.Inputs.InputBankAccount" Codebehind="InputBankAccount.aspx.cs" %>
<%@ Register TagPrefix="cc1" Namespace="Cet" Assembly="Cet.ConfirmButton" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Edit Bank Account</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
		</style>-->
        <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body bgcolor="white">
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr height="20">
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center">Edit Bank Accounts</td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td><asp:label id="Label1" style="Z-INDEX: 100; LEFT: 8px" runat="server">Bank Accounts</asp:label></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="2" width="785" border="0">
					<tr>
						<td width="280"><asp:listbox id="AvailableList" style="Z-INDEX: 105" runat="server" Height="120px" AutoPostBack="True"
								Width="500px" onselectedindexchanged="AvailableList_SelectedIndexChanged"></asp:listbox><asp:textbox id="CompanyKey" style="Z-INDEX: 101" runat="server" Width="84px" Visible="False"></asp:textbox><asp:textbox id="AccountTypeKey" style="Z-INDEX: 101" runat="server" Width="84px" Visible="False"></asp:textbox></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td width="280"></td>
						<td align="right" width="180"><asp:label id="Label4" style="Z-INDEX: 100" runat="server">Company:</asp:label></td>
						<td><asp:textbox id="Company" style="Z-INDEX: 109" tabIndex="1" runat="server" Width="250px" EnterAsTab=""
								ReadOnly="True"></asp:textbox></td>
					</tr>
					<tr>
						<td width="280"></td>
						<td align="right" width="180"><asp:label id="Label2" style="Z-INDEX: 100" runat="server">Bank Account Type:</asp:label></td>
						<td><asp:textbox id="AccountType" style="Z-INDEX: 109" tabIndex="2" runat="server" Width="250px"
								EnterAsTab="" ReadOnly="True"></asp:textbox></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td width="280"></td>
						<td align="right" width="180"><asp:label id="Label5" style="Z-INDEX: 100" runat="server">Bank Account Number:</asp:label></td>
						<td><asp:textbox id="AccountNo" style="Z-INDEX: 109" runat="server" tabIndex="3" Width="250px" EnterAsTab="" ReadOnly="True"
								MaxLength="32"></asp:textbox></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td width="150"></td>
						<td align="right" width="310"><asp:label id="Label6" style="Z-INDEX: 100" runat="server">Line of Credit to Checking Link Number:</asp:label></td>
						<td><asp:textbox id="AccountLink" style="Z-INDEX: 109" runat="server" Width="250px" tabIndex="4" EnterAsTab="" MaxLength="32"></asp:textbox></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td width="280"></td>
						<td align="right" width="180"><asp:label id="Label7" style="Z-INDEX: 100" runat="server">Account Loan Limit:</asp:label></td>
						<td><asp:textbox id="LoanLimit" style="Z-INDEX: 109" runat="server" Width="250px" tabIndex="5" EnterAsTab="" MaxLength="32"></asp:textbox></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td width="280"></td>
						<td align="right" width="180"><asp:label id="Label8" style="Z-INDEX: 100" runat="server">Active:</asp:label></td>
						<td><asp:checkbox id="Active" runat="server" EnterAsTab="" HEIGHT="24px" tabIndex="6"></asp:checkbox></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr height="40" valign="middle">
						<td width="180"></td>
						<td align="center" width="197"><asp:button id="Save" style="Z-INDEX: 111; LEFT: 264px" tabIndex="6" runat="server" Height="24px"
								Width="160px" Text="Save" Enabled="False" EnterAsTab="" Tabstop="" onclick="Save_Click"></asp:button></td>
						<td align="left" width="343"><asp:button id="canel" style="Z-INDEX: 111; LEFT: 264px" tabIndex="7" runat="server" Height="24px"
								Width="160px" Text="Cancel" Enabled="False" onclick="canel_Click"></asp:button></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td align="center"><asp:button id="Add" style="Z-INDEX: 111; LEFT: 264px" tabIndex="8" runat="server" Height="24px"
								Width="160px" Text="Add Bank Account" onclick="Add_Click"></asp:button></td>
						<td align="center"><cc1:confirmbutton id="deleteBankAccount" style="Z-INDEX: 113" runat="server" Text="Delete Bank Account"
								Enabled="False" Width="160px" onclick="deleteBankAccount_Click"></cc1:confirmbutton></td>
						<td align="center"><asp:button id="Print" style="Z-INDEX: 111; LEFT: 264px" tabIndex="9" runat="server" Height="24px"
								Width="160px" Text="Account List" onclick="Print_Click"></asp:button></td>
						<td align="center"><asp:button id="Return" style="Z-INDEX: 111; LEFT: 264px" tabIndex="10" runat="server" Height="24px"
								Width="160px" Text="Return to Menu" onclick="Return_Click"></asp:button></td>
					</tr>
					<tr>
						<td><asp:label id="Message" style="Z-INDEX: 100; LEFT: 8px" runat="server" ForeColor="Red"></asp:label></td>
					</tr>
				</table>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
			</div>
        </form>
	</body>
</html>