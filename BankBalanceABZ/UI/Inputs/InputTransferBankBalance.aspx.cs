using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using KeyCentral.Functions;

namespace BankBalance.UI.Inputs
{
	/// <summary>
	/// Summary description for Security Type.
	/// </summary>
	/// 
	
	public partial class InputTransferBankBalance : BasePage
	{
		#region Declares

		private DataSet transferData;

		private DataSet TransferData
		{
			get
			{
				if(transferData==null)
				{
					transferData = (DataSet)ViewState["TransferData"];
					if(transferData == null)
					{
						transferData = new DataSet();
						transferData.Tables.Add();
						transferData.Tables[0].Columns.Add("FromComp",typeof(System.String));
						transferData.Tables[0].Columns.Add("FromBank",typeof(System.String));
					}
				}
				return transferData;
			}
			set
			{
				ViewState["TransferData"] = value;
			}
		}
			#endregion 

		#region Param Properties
		private DataSet BankBalanceData{get{return (DataSet)Session["Data"];}}
	    #endregion

		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(IsPostBack ==false)
			{
				FillBankAccounts();
				BankBalanceData.Tables[0].DefaultView.Sort = "TN,GLBalance";
				FillTransfers(BankBalanceData.Tables[0].DefaultView);
			}

		}
		protected void Return_Click(object sender, System.EventArgs e)
		{
			SaveTransfers();
			Response.Redirect("../Reports/Report_BackOnlyBankBalance.aspx" + "?LastPage=" + Misc.EncodeQueryString(Request.Path + "?ReportName=" + Request.QueryString["ReportName"]));			
		}
		protected void bankList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			fromAccount.Enabled = true;
			toAccount.Enabled = true;
			save.Enabled = true;
			cancel.Enabled = true;
			Message.Text = "";
		}
		protected void fromAccount_Click(object sender, System.EventArgs e)
		{
			fromCompanyTextbox.Text = Misc.ParseTildaString(bankList.SelectedItem.Text,0);
			fromAccountTextBox.Text = Misc.ParseTildaString(bankList.SelectedItem.Text,1);
			fromAccount.Enabled = false;
			toAccount.Enabled = false;
			bankList.SelectedIndex = -1;
		}
		protected void toAccount_Click(object sender, System.EventArgs e)
		{
			toCompanyTextBox.Text = Misc.ParseTildaString(bankList.SelectedItem.Text,0);
			toAccountTextBox.Text = Misc.ParseTildaString(bankList.SelectedItem.Text,1);
			fromAccount.Enabled = false;
			toAccount.Enabled = false;
			bankList.SelectedIndex = -1;
		}
		protected void save_Click(object sender, System.EventArgs e)
		{
			Page.Validate();
			if (Page.IsValid)
			{
				DataRow newRow = TransferData.Tables[0].NewRow();
				newRow["FromComp"] = fromCompanyTextbox.Text + "~" + toCompanyTextBox.Text;
				newRow["FromBank"] = fromAccountTextBox.Text + "~" + toAccountTextBox.Text + "~" + amount.Text;
				newRow.Table.Rows.Add(newRow);
				TransferData  = TransferData;
				ClearScreen();
				transfersList.Items.Clear();
				FillTransferList();
			}
		}
		protected void cancel_Click(object sender, System.EventArgs e)
		{
			ClearScreen();
		}
		protected void deleteTransfer_Click(object sender, System.EventArgs e)
		{
			TransferData.Tables[0].Rows[transfersList.SelectedIndex].Delete();
			TransferData  = TransferData;
            transfersList.SelectedIndex = -1;
			deleteTransfer.Enabled = false;
			transfersList.Items.Clear();
			FillTransferList();
			ClearScreen();
		}
		protected void transfers_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			deleteTransfer.Message = "Do you want to Delete This Transfer?";
			deleteTransfer.Enabled = true;
		}
		#endregion
			
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Private helpers
		/// <summary>
		/// Fill Bank Accounts from DataSet.
		/// /// </summary>
		private void FillBankAccounts()
		{
			string test;
			double balance;
			for(int x=0;x<BankBalanceData.Tables[0].Rows.Count;x++)
			{
				//if not Transfer line.
				if(BankBalanceData.Tables[0].Rows[x]["TN"].ToString() == "0")
				{
					if(BankBalanceData.Tables[0].Rows[x]["LoanLimit"].ToString()!= "0" || BankBalanceData.Tables[0].Rows[x]["LoanBalance"].ToString()!= "0")
						{
							balance = Math.Round((double.Parse(BankBalanceData.Tables[0].Rows[x]["LoanLimit"].ToString()) - (double.Parse(BankBalanceData.Tables[0].Rows[x]["LoanBalance"].ToString()) - double.Parse(BankBalanceData.Tables[0].Rows[x]["GLBalance"].ToString()))),2);
						}
						else
						{
							balance = double.Parse(BankBalanceData.Tables[0].Rows[x]["GLBalance"].ToString());
						}
					test = (BankBalanceData.Tables[0].Rows[x]["DeptName"].ToString() +"~" +BankBalanceData.Tables[0].Rows[x]["BankName"].ToString() +"~ $" + balance);
					bankList.Items.Add(test);
				}
			}
		}
		private void ClearScreen()
		{
			amount.Text = "";
			fromAccountTextBox.Text = "";
			toAccountTextBox.Text = "";
			fromCompanyTextbox.Text = "";
			toCompanyTextBox.Text = "";
			fromAccount.Enabled = false;
			toAccount.Enabled = false;
			save.Enabled = false;
			cancel.Enabled = false;
			bankList.SelectedIndex = -1;
			transfersList.SelectedIndex = -1;
			Message.Text = "";
		}
		private void SaveTransfers()
		{
			DataSet data = BankBalanceData;
			
			for(int x=0;x<TransferData.Tables[0].Rows.Count;x++)
			{  
				//add From Bank Transfer
				DataRow newRow = data.Tables[0].NewRow();
				newRow["DeptName"] = Misc.ParseTildaString(TransferData.Tables[0].Rows[x]["FromComp"].ToString(),0);
				newRow["BankName"] = Misc.ParseTildaString(TransferData.Tables[0].Rows[x]["FromBank"].ToString(),0);
				newRow["GLBalance"] = (double.Parse(Misc.ParseTildaString(TransferData.Tables[0].Rows[x]["FromBank"].ToString(),2))*-1);
				newRow["LoanBalance"] = 0;
				newRow["LoanLimit"] = 0;
				newRow["TN"] = (x + 1);
				data.Tables[0].Rows.Add(newRow);
				//add To Bank Transfer
				DataRow newRow1 = data.Tables[0].NewRow();
				newRow1["DeptName"] = Misc.ParseTildaString(TransferData.Tables[0].Rows[x]["FromComp"].ToString(),1);
				newRow1["BankName"] = Misc.ParseTildaString(TransferData.Tables[0].Rows[x]["FromBank"].ToString(),1);
				newRow1["GLBalance"] = Misc.ParseTildaString(TransferData.Tables[0].Rows[x]["FromBank"].ToString(),2);
				newRow1["LoanBalance"] = 0;
				newRow1["LoanLimit"] = 0;
				newRow1["TN"] = (x + 1);
				data.Tables[0].Rows.Add(newRow1);
				
			}
		}
		private void FillTransfers(DataView data)
		{
			for(int x=0;x<data.Count;x++)
				{
					//Line that is a transfer.
					if (data[x]["TN"].ToString() != "0")
					{
						for(int y=x;y<data.Count;y++)
						{	
							//Find the right transfer line.
							if (data[x]["TN"].ToString() == data[y]["TN"].ToString() && data[x]["GLBalance"].ToString() != data[y]["GLBalance"].ToString())
							{
								DataRow newRow = TransferData.Tables[0].NewRow();
								//To See which line is first line of transfer.
								if (double.Parse(data[x]["GLBalance"].ToString()) < 0)
								{
									newRow["FromComp"] = data[x]["DeptName"].ToString() + "~" + data[y]["DeptName"].ToString();
									newRow["FromBank"] = data[x]["BankName"].ToString() + "~" + data[y]["BankName"].ToString() + "~" + data[y]["GLBalance"].ToString();
								}
								else
								{
									newRow["FromComp"] = data[y]["DeptName"].ToString() + "~" + data[x]["DeptName"].ToString();
									newRow["FromBank"] = data[y]["BankName"].ToString() + "~" + data[x]["BankName"].ToString() + "~" + data[x]["GLBalance"].ToString();
								}
								TransferData.Tables[0].Rows.Add(newRow);
								TransferData = TransferData;
								data[x].Delete();
								data[y - 1].Delete();
								x--;
								break;
							}
						}
					}
				}
				FillTransferList();
		}

		private void FillTransferList()
		{
			transfersList.DataSource = TransferData;
			transfersList.DataTextField = "FromBank";
			transfersList.DataValueField = "FromComp";
			transfersList.DataBind();
		}
        #endregion	

		
		
		

		

		

		

		

		

		

		

		

		
		
		
	}
}
