using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;
using System.Configuration;
using KeyCentral.Functions;
//using KeyCentral.Lookups;

namespace BankBalance.UI.Inputs
{
	/// <summary>
	/// Summary description for Security Type.
	/// </summary>
	/// 

    public partial class InputCheckWritingScreen : BasePage
	{
		#region Declares
        protected ReportSessionManager reportSessionManager;
		#endregion
        
        #region Param Properties
        private string CompanyName { get { return Session["CompanyName"].ToString(); } }
        private string CompanyKey { get { return Session["CompanyKey"].ToString(); } }
        private string CompanyReportNumber { get { return Session["CompanyReportNumber"].ToString(); } }
        private string BankAccountName { get { return Session["BankAccountName"].ToString(); } }
        private string BankAccountKey { get { return Session["BankAccountKey"].ToString(); } }
       #endregion
	
		#region events
		protected void Page_Load(object sender, System.EventArgs e)
        {
            if (CheckSecurity("Check Writing Number") == false)
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/MenuMain.aspx");

            if (!IsPostBack)
            {
                Response.ClearHeaders();
                Response.ClearContent();
                Response.Clear();

                Response.AddHeader("Refresh", "60;URL=InputCheckWritingScreen.aspx");

                CompanyAccountLbl.Text = CompanyName + ", " + BankAccountName;
                FillCheckList();
            }
		}
        protected void ExitAndReleaseBtn_Click(object sender, System.EventArgs e)
        {
            SqlConnection Conn = GetSQLConn();
            //Remove Bank Account
            SqlCommand removeCMD = new SqlCommand("use BankBalance;delete from [Check_Writing_User] where Company_Key=@Company_Key and Bank_Name_Key = @Bank_Name_Key and User_Name = @User_Name", Conn);
            removeCMD.Parameters.AddWithValue("@Company_Key", CompanyKey);
            removeCMD.Parameters.AddWithValue("@Bank_Name_Key", BankAccountKey);
            removeCMD.Parameters.AddWithValue("@User_Name", Session["User_Name"].ToString());
            removeCMD.ExecuteNonQuery();
           
            Conn.Close();
            Conn.Dispose();

            Response.Redirect("../../../BankBalanceABZ/UI/Inputs/InputCheckWritingUser.aspx?ReportName=CheckWritingUser");
        }
		
	    #endregion
			
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Private helpers
        private void FillCheckList()
        {
            CheckList.SelectedIndex = -1;
            CheckList.ClearSelection();
            CheckList.Items.Clear();

            OracleConnection OracleConn = Misc.GetOracleConn(CompanyReportNumber);
            DataSet bankData = new DataSet();

            string selectCMD = Misc.FormatOracle(CompanyReportNumber + "_rpt", @" 
		    Select Bank_Activity.ActivityDate,
                   Bank_Activity.Descr,
                   Bank_Payment.CheckId,
                   Bank_Activity.ForeignAmt,
                   Decode(Bank_Activity.ActivityStatus,'1','Entered',Decode(Bank_Activity.ActivityStatus,'2','Deleted',Decode(Bank_Activity.ActivityStatus,'3','Void',Decode(Bank_Activity.ActivityStatus,'4','Marked','Reconcile')))) as Status,
                   Bank.InitBalance + Bank.ActivityBalance as Balance

            from Bank_Activity,
                 Bank_Payment,
                 Bank

            Where Bank_Payment.BankActivityIdx  = Bank_Activity.BankActivityIdx and
                  Bank_Payment.PaymentType   In ('1','2') and
                  Bank.BankNameIdx             = Bank_Activity.BankNameIdx and
                  Bank.BankNameidx = :BankAccountKey
            Order By Bank_Payment.BankActivityIdx desc
            ");

            OracleCommand OracleCmd = new OracleCommand(selectCMD, OracleConn);
            OracleCmd.Parameters.Add(":BankAccountKey", BankAccountKey);

            OracleDataReader myReader = OracleCmd.ExecuteReader();

            while (myReader.Read())
            {
                if (CheckNumberLbl.Text == "" && myReader.GetValue(2).ToString().Trim() != "")
                {
                    CheckNumberLbl.Text = Convert.ToString(Convert.ToInt32(myReader.GetValue(2).ToString().Trim()) + 1);
                }

                if (BalanceLbl.Text == "")
                {
                    BalanceLbl.Text = Misc.DisplayHTMLForCurency(Convert.ToDouble(myReader.GetValue(5)));
                }

                string CheckNumber = myReader.GetValue(2).ToString();
                string CheckDate = Misc.FormatDateTime(myReader.GetValue(0).ToString());
                string Amount = Misc.DisplayHTMLForCurency(Convert.ToDouble(myReader.GetValue(3)));
                string Description = myReader.GetValue(1).ToString();
                string Status = myReader.GetValue(4).ToString();

                if (CheckList.Items.Count < 200)
                {
                    CheckList.Items.Add(new ListItem(CheckNumber + " ~~ " + CheckDate + " ~ " + Amount + " ~ " + Description + " ~ " + Status));
                }
            }

            myReader.Close();
            Misc.CleanUpOracle(OracleConn);
           
        }
       
		#endregion	

		

		

		

		
		
		
	}
}
