using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using KeyCentral.Functions;

namespace BankBalance.UI.Inputs
{
	/// <summary>
	/// Summary description for Security Type.
	/// </summary>
	/// 
	
	public partial class InputAddBankAccount : BasePage
	{
		#region Declares

		
		#endregion

		#region events
		protected void Page_Load(object sender, System.EventArgs e)
		{
					
		}
		protected void AccountTypeList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			AccountTypeKey.Text = AccountTypeList.SelectedItem.Value;
			AccountType.Text = AccountTypeList.SelectedItem.Text.ToString();
		}
		protected void CompanyList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			CompanyKey.Text = companyList.SelectedItem.Value;
			Company.Text = companyList.SelectedItem.Text.ToString();
		}
		protected void save_Click(object sender, System.EventArgs e)
		{
			Page.Validate();
			if (Page.IsValid)
			{
				SqlConnection Conn = GetSQLConn();
				SqlCommand selectCMD = new SqlCommand("use BankBalance;select Company_Key,Account_Type_Key,Account_Number,Account_Link,Account_Loan_Limit,Active from [Account] where Company_Key=@CompanyKey and Account_Type_Key = @AccountKey and Account_Number = @AccountNo", Conn);
				SqlCommand insertCMD = new SqlCommand("use BankBalance;insert into [Account] (Company_Key,Account_Type_Key,Account_Number,Account_Link,Account_Loan_Limit,Active) values(@CompanyKey,@AccountKey,@AccountNo,@AccountLink,@LoanLimit,@Active)", Conn);
				selectCMD.Parameters.Add("@CompanyKey",CompanyKey.Text);
				selectCMD.Parameters.Add("@AccountKey",AccountTypeKey.Text);
				selectCMD.Parameters.Add("@AccountNo",AccountNo.Text);
				insertCMD.Parameters.Add("@CompanyKey",CompanyKey.Text);
				insertCMD.Parameters.Add("@AccountKey",AccountTypeKey.Text);
				insertCMD.Parameters.Add("@AccountNo",AccountNo.Text);
				if(AccountLink.Text.Trim() == "")
					insertCMD.Parameters.Add("@AccountLink",DBNull.Value);
				else
					insertCMD.Parameters.Add("@AccountLink",AccountLink.Text);
				if(LoanLimit.Text.Trim() == "")
					insertCMD.Parameters.Add("@LoanLimit",DBNull.Value);
				else
					insertCMD.Parameters.Add("@LoanLimit",LoanLimit.Text);
				insertCMD.Parameters.Add("@Active",BoolToSQlBit(Active.Checked));
						
				SqlDataReader myReader = selectCMD.ExecuteReader();
				if(myReader.Read()==true) //Existing Bank Account
				{
					Message.Text = "Need to Edit This Bank Account on the edit screen.";
				}
				else  //New Bank Account 
				{
					myReader.Close();
					insertCMD.ExecuteNonQuery();
					Message.Text = "Bank Account Added.";
					CompanyKey.Text = "";
					AccountTypeKey.Text = "";
					AccountNo.Text = "";
					AccountLink.Text = "";
					LoanLimit.Text = "";
					Company.Text = "";
					AccountType.Text = "";
					Active.Checked = false;
					companyList.SelectedIndex = -1;
					AccountTypeList.SelectedIndex = -1;
				}
				Conn.Close();
				Conn.Dispose();	
			}
		}
		protected void canel_Click(object sender, System.EventArgs e)
		{
			Message.Text = "";
			CompanyKey.Text = "";
			AccountTypeKey.Text = "";
			AccountNo.Text = "";
			AccountLink.Text = "";
			LoanLimit.Text = "";
			Company.Text = "";
			AccountType.Text = "";
			Active.Checked = false;
		}
		protected void Return_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Bank");			
		}
		protected void ReturnEdit_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("../../UI/Inputs/InputBankAccount.aspx");
		}
		#endregion
			
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			FillBankAccountTypeList();
			FillcompanyList();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Private helpers
		private void FillBankAccountTypeList()
		{
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("use BankBalance;select * from [Account_Type]", Conn);
		
			SqlDataReader myReader = selectCMD.ExecuteReader();
			
			if(myReader.HasRows ==true)
			{				 
				AccountTypeList.DataSource = myReader;
				AccountTypeList.DataTextField = "Description";
				AccountTypeList.DataValueField = "Account_Type_Key";
				AccountTypeList.DataBind();	
			}
			Conn.Close();
			Conn.Dispose();	  		
					
		}	
		private void FillcompanyList()
		{
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("use KeyCentral;select [Company].Company_Name,[Company].Company_Key from [Company] Order By Company_Name", Conn);
		
			SqlDataReader myReader = selectCMD.ExecuteReader();
			
			if(myReader.HasRows ==true)
			{				 
				companyList.DataSource = myReader;
				companyList.DataTextField = "Company_Name";
				companyList.DataValueField = "Company_Key";
				companyList.DataBind();	
			}
			Conn.Close();
			Conn.Dispose();	  		
		}
		public static string BoolToSQlBit(bool value)
		{
			if(value)
				return "1";
			else
				return "0";
		}		
		#endregion	

		

		

		
		
	}
}
