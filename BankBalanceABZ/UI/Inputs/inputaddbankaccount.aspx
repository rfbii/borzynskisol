<%@ Page language="c#" Inherits="BankBalance.UI.Inputs.InputAddBankAccount" Codebehind="InputAddBankAccount.aspx.cs" %>
<%@ Register TagPrefix="cc1" Namespace="Cet" Assembly="Cet.ConfirmButton" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Add Bank Account</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
		</style>-->
        <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body bgcolor="white">
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
				<asp:ValidationSummary id="ValidationSummary1" runat="server" ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
				<asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ControlToValidate="Company" ErrorMessage="Company must be provided."
					Display="None" Enabled="True"></asp:requiredfieldvalidator>
				<asp:requiredfieldvalidator id="Requiredfieldvalidator2" runat="server" ControlToValidate="AccountType" ErrorMessage="Bank Account Type must be provided."
					Display="None" Enabled="True"></asp:requiredfieldvalidator>
				<asp:requiredfieldvalidator id="Requiredfieldvalidator3" runat="server" ControlToValidate="AccountNo" ErrorMessage="Bank Account Number must be provided."
					Display="None" Enabled="True"></asp:requiredfieldvalidator>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr height="20">
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center">Add Bank Accounts</td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td><asp:label id="Label1" style="Z-INDEX: 100; LEFT: 8px" runat="server">Company</asp:label></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="2" width="785" border="0">
					<tr>
						<td width="280"><asp:listbox id="companyList" style="Z-INDEX: 105" runat="server" Height="120px" AutoPostBack="True"
								Width="251px" onselectedindexchanged="CompanyList_SelectedIndexChanged"></asp:listbox></td>
						<td align="right" width="180"><asp:label id="Label3" style="Z-INDEX: 100" runat="server">Company:</asp:label></td>
						<td><asp:textbox id="Company" style="Z-INDEX: 109" tabIndex="1" runat="server" Width="250px" EnterAsTab=""
								ReadOnly="True"></asp:textbox><br/>
							<asp:textbox id="CompanyKey" style="Z-INDEX: 101" runat="server" Width="84px" Visible="False"></asp:textbox></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td><asp:label id="Label4" style="Z-INDEX: 100; LEFT: 8px" runat="server">Bank Account Types</asp:label></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="2" width="785" border="0">
					<tr>
						<td width="280"><asp:listbox id="AccountTypeList" style="Z-INDEX: 105" runat="server" Height="120px" AutoPostBack="True"
								Width="251px" onselectedindexchanged="AccountTypeList_SelectedIndexChanged"></asp:listbox></td>
						<td align="right" width="180"><asp:label id="Label2" style="Z-INDEX: 100" runat="server">Bank Account Type:</asp:label></td>
						<td><asp:textbox id="AccountType" style="Z-INDEX: 109" tabIndex="1" runat="server" Width="250px"
								EnterAsTab="" ReadOnly="True"></asp:textbox><br/>
							<asp:textbox id="AccountTypeKey" style="Z-INDEX: 101" runat="server" Width="84px" Visible="False"></asp:textbox></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td width="280"></td>
						<td align="right" width="180"><asp:label id="Label5" style="Z-INDEX: 100" runat="server">Bank Account Number:</asp:label></td>
						<td><asp:textbox id="AccountNo" style="Z-INDEX: 109" runat="server" Width="250px" EnterAsTab="" MaxLength="32"></asp:textbox></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td width="150"></td>
						<td align="right" width="310"><asp:label id="Label6" style="Z-INDEX: 100" runat="server">Line of Credit to Checking Link Number:</asp:label></td>
						<td><asp:textbox id="AccountLink" style="Z-INDEX: 109" runat="server" Width="250px" EnterAsTab="" MaxLength="32"></asp:textbox></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td width="280"></td>
						<td align="right" width="180"><asp:label id="Label7" style="Z-INDEX: 100" runat="server">Account Loan Limit:</asp:label></td>
						<td><asp:textbox id="LoanLimit" style="Z-INDEX: 109" runat="server" Width="250px" EnterAsTab="" MaxLength="32"></asp:textbox></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td width="280"></td>
						<td align="right" width="180"><asp:label id="Label8" style="Z-INDEX: 100" runat="server">Active:</asp:label></td>
						<td><asp:checkbox id="Active" runat="server" EnterAsTab="" HEIGHT="24px" Tabstop=""></asp:checkbox></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td align="center"><asp:button id="save" style="Z-INDEX: 111; LEFT: 264px" tabIndex="4" runat="server" Height="24px"
								Width="160px" Text="Save" onclick="save_Click"></asp:button></td>
						<td align="center"><asp:button id="canel" style="Z-INDEX: 111; LEFT: 264px" tabIndex="4" runat="server" Height="24px"
								Width="160px" Text="Cancel" CausesValidation="False" onclick="canel_Click"></asp:button></td>
						<td align="center"><asp:button id="ReturnEdit" style="Z-INDEX: 111; LEFT: 264px" tabIndex="4" runat="server" Height="24px"
								Width="160px" Text="Return to Edit" CausesValidation="False" onclick="ReturnEdit_Click"></asp:button></td>
						<td align="center"><asp:button id="Return" style="Z-INDEX: 111; LEFT: 264px" tabIndex="4" runat="server" Height="24px"
								Width="160px" Text="Return to Menu" CausesValidation="False" onclick="Return_Click"></asp:button></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr height="20">
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center"><asp:label id="Message" style="Z-INDEX: 100; LEFT: 8px" runat="server" ForeColor="Red"></asp:label></td>
					</tr>
				</table>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
			</div>
		</form>
	</body>
</html>