<%@ Page language="c#" Inherits="BankBalance.UI.Inputs.InputAccountType" Codebehind="InputAccountType.aspx.cs" %>
<%@ Register TagPrefix="cc1" Namespace="Cet" Assembly="Cet.ConfirmButton" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Bank Account Type</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
		</style>-->
        <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body bgcolor="white">
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
				<table cellspacing="0" cellpadding="10" width="785" border="0">
					<tr height="20">
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center">Input Bank Account Types</td>
					</tr>
					<tr>
						<td><asp:label id="Label1" style="Z-INDEX: 100; LEFT: 8px" runat="server">Bank Account Types</asp:label>
						</td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="10" width="785" border="0">
					<tr>
						<td><asp:listbox id="AccountTypeList" style="Z-INDEX: 105; LEFT: 8px" runat="server" Width="251px"
								AutoPostBack="True" Height="120px" onselectedindexchanged="AccountTypeList_SelectedIndexChanged"></asp:listbox>
						</td>
						<td><asp:label id="Label2" style="Z-INDEX: 100" runat="server">Add New Bank Account Types</asp:label>
							<asp:textbox id="NewAccountType" onblur="__doPostBack('NewAccountType','')" style="Z-INDEX: 109" tabIndex="1" runat="server" Width="328px" EnterAsTab="" MaxLength="32" OnTextChanged="NewAccountType_TextChanged" ></asp:textbox><br/>
							<br/>
							<cc1:confirmbutton id="deleteBankAccountType" style="Z-INDEX: 113" runat="server" Text="Delete Bank Account Type"
								Enabled="False" Width="160px" onclick="deleteBankAccountType_Click"></cc1:confirmbutton>
							<asp:textbox id="AccountTypeKey" style="Z-INDEX: 101" runat="server" Width="84px" Visible="False"></asp:textbox>
						</td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td align="center"><asp:button id="Return" style="Z-INDEX: 111; LEFT: 264px" tabIndex="4" runat="server" Text="Return to Menu"
								Width="160px" Height="24px" onclick="Return_Click"></asp:button>
						</td>
					</tr>
					<tr>
						<td><asp:label id="Message" style="Z-INDEX: 100; LEFT: 8px" runat="server" ForeColor="Red"></asp:label>
						</td>
					</tr>
				</table>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
		    </div>
		</form>
	</body>
</html>