using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using KeyCentral.Functions;

namespace BankBalance.UI.Inputs
{
	/// <summary>
	/// Summary description for Security Type.
	/// </summary>
	/// 
	
	public partial class InputAccountType : BasePage
	{
		#region Declares

		#endregion

		#region events
		protected void Page_Load(object sender, System.EventArgs e)
		{
		}
		protected void AccountTypeList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			AccountTypeKey.Text = AccountTypeList.SelectedItem.Value;
			NewAccountType.Text = AccountTypeList.SelectedItem.Text.ToString();
			deleteBankAccountType.Message= "Do you want to delete " + AccountTypeList.SelectedItem.Text + "  Bank Account Type?";
			deleteBankAccountType.Enabled=true;
		}
		
		protected void NewAccountType_TextChanged(object sender, System.EventArgs e)
		{
			//Make sure a  Security Type has been entered
			if(NewAccountType.Text.Trim() == "")
			{				
				Message.Text = "Please enter a  Bank Account Type.";
				return;
			}
			Message.Text = "";
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("use BankBalance;select Account_Type_Key from [Account_Type] where Description=@Description", Conn);
			SqlCommand insertCMD = new SqlCommand("use BankBalance;insert into [Account_Type] (Description) values(@Description) Select @@Identity", Conn);
			selectCMD.Parameters.Add("@Description",NewAccountType.Text.ToLower());
			insertCMD.Parameters.Add("@Description",NewAccountType.Text);
			
			SqlDataReader myReader = selectCMD.ExecuteReader();
			if(myReader.Read()==true) //Existing Header
			{
				AccountTypeKey.Text = myReader.GetValue(0).ToString();
				myReader.Close();
				//Fill ListBoxs
				Message.Text = "This  Bank Account Type already exists.";					
			}
			else  //New  Security Type
			{
				myReader.Close();
				AccountTypeKey.Text = insertCMD.ExecuteScalar().ToString();
				Message.Text = " Bank Account Type Added.";
				FillBankAccountTypeList();				
			}
			AccountTypeKey.Text = "";
			NewAccountType.Text = "";
			Conn.Close();
			Conn.Dispose();		
		}
		protected void Return_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Bank");			
		}
		protected void deleteBankAccountType_Click(object sender, System.EventArgs e)
		{
			SqlConnection Conn = GetSQLConn();
			//Remove Bank Account_Type 
			SqlCommand removeCMD = new SqlCommand("use BankBalance;delete from [Account_Type] where Account_Type_key = @Account_Type_Key ", Conn);
			removeCMD.Parameters.Add("@Account_Type_Key",AccountTypeKey.Text);
			removeCMD.ExecuteNonQuery();
			NewAccountType.Text = "";
			FillBankAccountTypeList();
			Message.Text = " Bank Account Type Deleted.";	
			deleteBankAccountType.Enabled= false;
		}
		

		#endregion
			
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			FillBankAccountTypeList();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Private helpers
		private void FillBankAccountTypeList()
		{
			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("use BankBalance;select * from [Account_Type]", Conn);
		
			SqlDataReader myReader = selectCMD.ExecuteReader();
			
			if(myReader.HasRows ==true)
			{				 
				AccountTypeList.DataSource = myReader;
				AccountTypeList.DataTextField = "Description";
				AccountTypeList.DataValueField = "Account_Type_Key";
				AccountTypeList.DataBind();	
			}
			Conn.Close();
			Conn.Dispose();	  		
					
		}	
		
		
		#endregion	
		
	}
}
