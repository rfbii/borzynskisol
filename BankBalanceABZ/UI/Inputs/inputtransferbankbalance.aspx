<%@ Register TagPrefix="cc1" Namespace="Cet" Assembly="Cet.ConfirmButton" %>
<%@ Page language="c#" Inherits="BankBalance.UI.Inputs.InputTransferBankBalance" Codebehind="InputTransferBankBalance.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Edit Bank Account</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body bgcolor="white">
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
				<asp:ValidationSummary id="ValidationSummary1" runat="server" ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
				<asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ControlToValidate="fromAccountTextBox"
					ErrorMessage="From Bank Account must be provided." Display="None" Enabled="True"></asp:requiredfieldvalidator>
				<asp:requiredfieldvalidator id="Requiredfieldvalidator2" runat="server" ControlToValidate="toAccountTextBox"
					ErrorMessage="To Bank Account must be provided." Display="None" Enabled="True"></asp:requiredfieldvalidator>
				<asp:requiredfieldvalidator id="Requiredfieldvalidator3" runat="server" ControlToValidate="amount" ErrorMessage="Amount to transfer must be provided."
					Display="None" Enabled="True"></asp:requiredfieldvalidator>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr height="20">
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center">Input Transfer Bank Balance</td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td align="center"><asp:label id="Label1" runat="server">Bank Accounts</asp:label></td>
					</tr>
					<tr>
						<td align="center"><asp:listbox id="bankList" style="Z-INDEX: 103; LEFT: 8px" runat="server" Width="600px" Height="204px"
								Font-Name="Arial" AutoPostBack="True" onselectedindexchanged="bankList_SelectedIndexChanged"></asp:listbox></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="10" width="785" border="0">
					<tr>
						<td align="center"><asp:button id="fromAccount" style="Z-INDEX: 111; LEFT: 264px" tabIndex="10" runat="server"
								Width="160px" Height="24px" Text="From Account" Enabled="false" CausesValidation="false" onclick="fromAccount_Click"></asp:button>
						</td>
						<td align="center"><asp:button id="toAccount" style="Z-INDEX: 111; LEFT: 264px" tabIndex="10" runat="server" Width="160px"
								Height="24px" Text="To Account" Enabled="false" CausesValidation="false" onclick="toAccount_Click"></asp:button>
						</td>
						<td align="center"><asp:button id="save" style="Z-INDEX: 111; LEFT: 264px" tabIndex="10" runat="server" Width="160px"
								Height="24px" Text="Save" Enabled="false" CausesValidation="true" onclick="save_Click"></asp:button>
						</td>
						<td align="center"><asp:button id="cancel" style="Z-INDEX: 111; LEFT: 264px" tabIndex="10" runat="server" Width="160px"
								Height="24px" Text="Cancel" Enabled="false" CausesValidation="false" onclick="cancel_Click"></asp:button>
						</td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td align="right" width="200"><asp:label id="Labsl2" runat="server">From Account:</asp:label>
						</td>
						<td>
							<asp:TextBox id="fromAccountTextBox" runat="server" ReadOnly="true" Width="250"></asp:TextBox>
							<asp:TextBox id="fromCompanyTextbox" runat="server" ReadOnly="true" Width="250"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td align="right" width="200"><asp:label id="Label3" runat="server">To Account:</asp:label>
						</td>
						<td>
							<asp:TextBox id="toAccountTextBox" runat="server" ReadOnly="true" Width="250"></asp:TextBox>
							<asp:TextBox id="toCompanyTextBox" runat="server" ReadOnly="true" Width="250"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td align="right" width="200"><asp:label id="Label2" runat="server">Amount of Transfer:</asp:label>
						</td>
						<td>
							<asp:TextBox id="amount" runat="server" Width="300"></asp:TextBox>
						</td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td align="center"><asp:label id="Label4" runat="server">Transfers</asp:label></td>
						<td><cc1:confirmbutton id="deleteTransfer" style="Z-INDEX: 113" runat="server" Text="Delete Transfer" Enabled="False"
								Width="160px" onclick="deleteTransfer_Click"></cc1:confirmbutton>
						</td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td align="center"><asp:listbox id="transfersList" style="Z-INDEX: 103; LEFT: 8px" runat="server" Width="600px"
								Height="204px" Font-Name="Arial" AutoPostBack="True" onselectedindexchanged="transfers_SelectedIndexChanged"></asp:listbox></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td align="center"><asp:button id="Return" style="Z-INDEX: 111; LEFT: 264px" tabIndex="10" runat="server" Width="300px"
								Height="24px" Text="Return to Bank Balance Report" CausesValidation="false" onclick="Return_Click"></asp:button></td>
					</tr>
					<tr>
						<td><asp:label id="Message" style="Z-INDEX: 100; LEFT: 8px" runat="server" ForeColor="Red"></asp:label></td>
					</tr>
				</table>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
			</div>
		</form>
	</body>
</html>