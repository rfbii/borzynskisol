<%@ Page language="c#" Inherits="BankBalance.UI.Inputs.InputCheckWritingScreen" Codebehind="InputCheckWritingScreen.aspx.cs" %>
<%@ Register TagPrefix="cc1" Namespace="Cet" Assembly="Cet.ConfirmButton" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Check Writing Screen</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }-->
        <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
		<style type="text/css">
            .Title { FONT-WEIGHT: bold; FONT-SIZE: 13pt; COLOR: black; FONT-FAMILY: Arial }
	    </style>
	</head>
	<body bgcolor="white">
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr height="20">
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center" width="700">Check Writing Screen</td>
					</tr>
				    <tr height="20">
						<td>&nbsp;</td>
					</tr>
				    <tr>
						<td align="center" width="700"><asp:label id="CompanyAccountLbl" runat="server"></asp:label></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td align="right" width="350" class='Title'><asp:label id="Label1" runat="server">Available Balance: </asp:label></td>
						<td align="left" width="350" class='Title'><asp:label id="BalanceLbl" runat="server"></asp:label></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td align="center" width="250"><asp:label id="Label3" runat="server" ForeColor="Red">The Next Check Number: </asp:label></td>
					</tr>
					<tr>
						<td align="center" width="250"><asp:label id="CheckNumberLbl" runat="server" ForeColor="Red"></asp:label></td>
						<td align="left"><asp:button id="Button1" style="Z-INDEX: 111; LEFT: 264px" tabIndex="8" runat="server" Height="24px" Width="175px" Text="Exit and Release Account" onclick="ExitAndReleaseBtn_Click"></asp:button>
						</td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="2" width="785" border="0">
						<tr>
						<td><asp:label id="Label2" style="Z-INDEX: 100; LEFT: 8px" runat="server">Check Number ~~ Date ~ Amount ~ Description/Customer ~ Activity Status</asp:label></td>
					</tr>
				    <tr>
						<td width="700"><asp:listbox id="CheckList" runat="server" Height="400px" AutoPostBack="False"	Width="700px" ></asp:listbox></td>
					</tr>
				</table>
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					<tr>
						<td align="center" width="250">&nbsp;</td>
						<td align="left"><asp:button id="ExitAndReleaseBtn" style="Z-INDEX: 111; LEFT: 264px" tabIndex="8" runat="server" Height="24px"
								Width="175px" Text="Exit and Release Account" onclick="ExitAndReleaseBtn_Click"></asp:button></td>
					</tr>
					<tr>
						<td><asp:label id="Message" style="Z-INDEX: 100; LEFT: 8px" runat="server" ForeColor="Red"></asp:label></td>
					</tr>
				</table>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
			</div>
        </form>
	</body>
</html>