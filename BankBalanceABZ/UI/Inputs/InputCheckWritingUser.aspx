<%@ Page language="c#" Inherits="BankBalance.UI.Inputs.InputCheckWritingUser" Codebehind="InputCheckWritingUser.aspx.cs" %>
<%@ Register TagPrefix="cc1" Namespace="Cet" Assembly="Cet.ConfirmButton" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Check Writing Numbers</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }-->
        <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
		<style type="text/css">
            .Title { FONT-WEIGHT: bold; FONT-SIZE: 13pt; COLOR: black; FONT-FAMILY: Arial }
	    </style>
	</head>
	<body bgcolor="white">
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
				<table cellspacing="0" cellpadding="30" width="785" border="0">
					<tr>
						<td align="center" class='Title'>Check Writing Numbers</td>
					</tr>
				</table>
				<table>
		    	<tr align="center">
                        <td width="200" align="right"><font face="Tahoma" size="2">Company:</font></td>
                        <td width="400" align="left"><asp:DropDownList width="400" runat="server" 
                                EnterAsTab="" TabIndex="2" ID="CompanyDdl" AutoPostBack="true" 
                                OnSelectedIndexChanged="CompanyDdl_SelectedIndexChanged"></asp:DropDownList></td>
		            </tr>
		            <tr>
		                <td>&nbsp;</td>
		            </tr>
					 <tr align="center">
                        <td width="200" align="right"><font face="Tahoma" size="2">Bank Account:</font></td>
                        <td width="400" align="left"><asp:DropDownList width="400" runat="server" EnterAsTab="" TabIndex="2" ID="BankNameDdl" AutoPostBack="true" OnSelectedIndexChanged="BankNameDdl_SelectedIndexChanged" Enabled="False"></asp:DropDownList></td>
		            </tr>
		            <tr>
		                <td colspan="2"><asp:label id="Message" style="Z-INDEX: 100; LEFT: 8px" runat="server" ForeColor="Red"></asp:label></td>
					  </tr>
										
	         </table>				
				<table cellspacing="0" cellpadding="0" width="785" border="0">
					  <tr>
		                <td>&nbsp;</td>
		            </tr>
					<tr align="center">
						<td colspan="2"><asp:button id="WriteChecksBtn" style="Z-INDEX: 111; LEFT: 264px" 
                                tabIndex="4" runat="server" Text="To write checks click here"
								Width="200px" Height="24px" onclick="WriteChecks_Click" Enabled="False"></asp:button>
						</td>
					</tr>
					<tr>
		                <td>&nbsp;</td>
		            </tr>
		            <tr>
						<td align="center"><asp:button id="Return" style="Z-INDEX: 111; LEFT: 264px" tabIndex="4" runat="server" Text="Exit to Menu"
								Width="160px" Height="24px" onclick="Return_Click"></asp:button>
						</td>
					</tr>
					
				</table>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
		    </div>
		</form>
	</body>
</html>