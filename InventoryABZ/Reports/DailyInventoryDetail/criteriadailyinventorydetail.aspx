<%@ Register TagPrefix="cc1" Namespace="Cet" Assembly="Cet.ConfirmButton" %>
<%@ Page language="c#" Inherits="Inventory.Reports.DailyInventoryDetail.CriteriaDailyInventoryDetail" Codebehind="CriteriaDailyInventoryDetail.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
  <head runat="server">
		<title>Input Daily Inventory</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
		</style>-->
        <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
  </head>
	<body bgcolor="white" oncontextmenu="return false;">
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
				<table width="600">
					<tr>
						<td width="100%" height="30" align="center" valign="top" colspan="4">Daily Inventory Detail</td>
					</tr>
					<tr>
						<td width="100%" align="left" colspan="4"><font face="Tahoma" size="2">Select Commodity</font></td>
					</tr>
					<tr>
						<td width="232" align="left"><asp:listbox id="CommodityList" runat="server" Width="232px" Height="136px" Font-Names="Tahoma" AutoPostBack="True" SelectionMode="Single" onselectedindexchanged="CommodityList_SelectedIndexChanged"></asp:listbox></td>
						<td width="192" valign="top" align="left" colspan="2"><asp:label id="commodityName" runat="server" Width="192px" Font-Names="Tahoma" Font-Size="Small"></asp:label></td>
						<td width="100" align="left" valign="top"><asp:textbox id="commodityKey" runat="server" Width="100px" Height="19" Visible="False"></asp:textbox></td>
					</tr>
					<tr>
						<td width="232" align="left" rowspan="2"><asp:listbox id="WarehouseList" runat="server" Width="232px" Height="136px" Font-Names="Tahoma" AutoPostBack="True" SelectionMode="Single" onselectedindexchanged="WarehouseList_SelectedIndexChanged"></asp:listbox></td>
						<td width="192" valign="top" align="left" colspan="2"><asp:label id="warehouseName" runat="server" Width="192px" Font-Names="Tahoma" Font-Size="Small"></asp:label></td>
						<td width="100" align="left" valign="top"><asp:textbox id="warehouseKey" runat="server" Width="100px" Height="19" Visible="False"></asp:textbox></td>
					</tr>
					<tr>
						<td width="25%" align="right" valign="bottom">Ship Date:&nbsp;</td>
						<td width="100" align="left" valign="bottom"><asp:textbox id="startDate" tabIndex="1" runat="server" EnterAsTab="" Width="100px" Enabled="True" MaxLength="10" CalendarPopUp=""></asp:textbox></td>
						<td width="100" align="left" valign="bottom"><asp:comparevalidator id="CompareValidator2" runat="server" ControlToValidate="startDate" ErrorMessage="Invalid Date."	Display="Dynamic" Type="Date" Operator="DataTypeCheck"></asp:comparevalidator></td>
					</tr>
					<tr>
						<td width="25%" align="right" valign="bottom"><asp:button id="run" runat="server" Height="24" Width="160" Text="Run Report" EnterAsTab="" Enabled="True" tabIndex="2" onclick="run_Click"></asp:button></td>
						<td width="25%" align="center" valign="bottom" colspan="2"><asp:button id="cancel" runat="server" Height="24" Width="160" Enabled="true" Text="Clear" EnterAsTab="" CausesValidation="False" tabstop="" tabIndex="3" onclick="cancel_Click"></asp:button></td>
						<td width="25%" align="left" valign="bottom"><asp:button id="Return" runat="server" Height="24" Width="160" Text="Return to Menu" Enabled="True" tabIndex="4" onclick="Return_Click"></asp:button></td>
					</tr>
					<tr>
						<td width="100%" align="left" colspan="4"><asp:label id="Message" runat="server" Height="19" Width="552px" ForeColor="Red"></asp:label></td>
					</tr>
				</table>
				<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
			</div>
		</form>
	</body>
</html>