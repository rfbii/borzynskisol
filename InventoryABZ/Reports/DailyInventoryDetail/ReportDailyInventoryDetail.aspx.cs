using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using KeyCentral.Functions;
using Oracle.DataAccess.Client;
using System.Text;
using KeyCentral.Lookups;

namespace Inventory.Reports.DailyInventoryDetail
{
	public partial class ReportDailyInventoryDetail : BasePage
	{
		#region Delcares
		private double lineCount = 0;
		private bool newPage = true;
		private bool firstPage = true;

		private string strCommodity = "";
		private bool commodityChanged = false;
		#endregion
		
		#region Param Properties
		private string CommodityName{get{return (string)Session["CommodityName"];}}
		private string CommodityKey{get{return (string)Session["CommodityKey"];}}
		private ArrayList Commodities{get{return (ArrayList)Session["Commodities"];}}
		private string WarehouseName{get{return (string)Session["WarehouseName"];}}
		private string WarehouseKey{get{return (string)Session["WarehouseKey"];}}
		private ArrayList Warehouses{get{return (ArrayList)Session["Warehouses"];}}
		private string StartDate{get{return (string)Session["StartDate"];}}
		private string WarehouseCode{get{return (string)Session["WarehouseCode"];}}
		public string refreshRate;
		#endregion

		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Daily Inventory Report") == false)
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/MenuMain.aspx");

			//KeyCentral.Functions.JavaStatusMessage status = new KeyCentral.Functions.JavaStatusMessage(this);
			//status.ShowMessage("Computing");

            //Set page's refresh rate
            refreshRate = BaseDAL.ExecuteScalar("use Inventory; select Rate from [Refresh_Rate]").ToString();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.Clear();
            Response.AddHeader("Refresh", refreshRate + ";URL=../../Reports/DailyInventoryDetail/ReportDailyInventoryDetail.aspx?Lastpage=" + Misc.EncodeQueryString("../../Reports/DailyInventoryDetail/CriteriaDailyInventoryDetail.aspx?ReportName=InputDailyInventory"));
            this.Repeater1.DataSource = GetData(CommodityKey, StartDate);

			//remove invalid entries
			for(int i = ((ArrayList)this.Repeater1.DataSource).Count - 1; i >= 0; i--)
			{
				InventoryCBO inv = (InventoryCBO)((ArrayList)Repeater1.DataSource)[i];

				//if all the values are 0's, delete item
				if (inv.Estimated == "0" &&
					inv.OnHand == "0" &&
					inv.OrderedQnt == "0" &&
					inv.ShippedQnt == "0" &&
					inv.OneDayAhead == "0" &&
					inv.TwoDaysAhead == "0" &&
					inv.ThreePlusDaysAhead == "0")
					((ArrayList)Repeater1.DataSource).RemoveAt(i);
			}
			if(((ArrayList)this.Repeater1.DataSource).Count != 0)
				Repeater1.DataBind();
			else
				JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"])+ "&&Records=None");
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Display Code
		public string CheckLineCount(object oItem)
		{
			if (firstPage == true)
			{
				if(lineCount >= 36)
				{
					newPage = true;
					firstPage = false;
					lineCount = 0;
				}
			}
			else if (firstPage == false)
			{
				if(lineCount >= 36)
				{
					newPage = true;
					lineCount = 0;
				}
			}
			return "";
		}

		public string ColumnHeader(object oItem)
		{
			string columnHeader="";
			if(newPage)
			{
				if(firstPage == false)
				{
					if (commodityChanged)
					{
						columnHeader = @"
							<!-- email end -->
							</table><div style='page-break-before:always'><span style='visibility: hidden;'><font style='font-size:0pt;'>&nbsp;</font></span></div>
							<table border=0 cellspacing=0 cellpadding=1 width=936 class='ReportTable'>
								<tr>
									<td height='20'><font size='2'>&nbsp;</font></td>
								</tr><!-- email start -->";
					}
					else
					{
							columnHeader = @"
							<!-- email end -->
							</table><div style='page-break-before:always'><span style='visibility: hidden;'><font style='font-size:0pt;'>&nbsp;</font></span></div>
							<table border=0 cellspacing=0 cellpadding=1 width=936 class='ReportTable'>
								<tr>
									<td height='20'><font size='2'>&nbsp;</font></td>
								</tr>
							<td class='BottomLeftRightBlack' nowrap align=left width=216>" + GetProduct(oItem).PadRight(26).Substring(0,26) + @"</td>
							<td class='BottomRightBlack' align=center width=68>On Hand</td>
							<td class='BottomRightBlack' align=center width=90>Production</td>
							<td class='BottomRightBlack' align=center width=68>Shipped</td>
							<td class='BottomRightBlack' align=center width=72><b>Subtotal</b></td>
							<td class='BottomRightBlack' align=center width=70>Ordered</td>
							<td class='BottomRightBlack' align=center width=72>Ord&Shp</td>
							<td class='BottomRightBlack' align=center width=72><b>Left</b></td>
							<td class='BottomRightBlack' align=center width=68>+1 day</td>
							<td class='BottomRightBlack' align=center width=68>+2 days</td>
							<td class='BottomRightBlack' align=center width=72>+3 > +10</td>					
						</tr><!-- email start -->";
					}
				}
				else
				{
					columnHeader += @"
						<table border=0 cellspacing=0 cellpadding=1 width=936 class='ReportTable'>
							<tr>";
				}
				newPage = false;
			}
			return columnHeader;
		}

		public string CheckForChanges(object oItem)
		{

			if(strCommodity != GetCommodityName(oItem))
			{
				commodityChanged = true;
				strCommodity = GetCommodityName(oItem);
			}
			else
				commodityChanged= false;

					
			return "";
		}

		public string Detail(object oItem)
		{
			string strRet="";
			if (commodityChanged)
			{
				strRet = @"<tr>
						<td class='BottomLeftRightBlack' align=left width=216><b>" + GetCommodityName(oItem) + @"</b></td>
						<td class='BottomRightBlack' align=center width=68>On Hand</td>
						<td class='BottomRightBlack' align=center width=90>Production</td>
						<td class='BottomRightBlack' align=center width=68>Shipped</td>
						<td class='BottomRightBlack' align=center width=72><b>Subtotal</b></td>
						<td class='BottomRightBlack' align=center width=70>Ordered</td>
						<td class='BottomRightBlack' align=center width=72>Ord&Shp</td>
						<td class='BottomRightBlack' align=center width=72><b>Left</b></td>
						<td class='BottomRightBlack' align=center width=68>+1 day</td>
						<td class='BottomRightBlack' align=center width=68>+2 days</td>
						<td class='BottomRightBlack' align=center width=72>+3 > +10</td>					
					</tr>
					<tr>
						<td class='BottomLeftRightBlack' align=left width=216>" + GetProduct(oItem) + @"</td>
						<td class='BottomRightBlack' align=right width=68>" + GetOnHand(oItem) + @"</td>
						<td class='BottomRightBlack' align=right width=90>" + GetEstimated(oItem) + @"</td>
						<td class='BottomRightBlack' align=right width=68>" + GetShipped(oItem) + @"</td>
						<td class='BottomRightBlack' align=right width=72><b>" + GetSubtotal(oItem) + @"</b></td>
						<td class='BottomRightBlack' align=right width=70>" + GetOrdered(oItem) + @"</td>
						<td class='BottomRightBlack' align=right width=72>" + GetTotalOrdered(oItem) + @"</td>
						<td class='BottomRightBlack' align=right width=72><b>" + GetTotal(oItem) + @"</b></td>
						<td class='BottomRightBlack' align=right width=68>" + GetOneDayAhead(oItem) + @"</td>
						<td class='BottomRightBlack' align=right width=68>" + GetTwoDaysAhead(oItem) + @"</td>
						<td class='BottomRightBlack' align=right width=72>" + GetThreePlusDaysAhead(oItem) + @"</td>
					</tr>";

				if (GetProduct(oItem).Length > 27)
					lineCount += 3;
				else
					lineCount += 2;
			}
			else
			{
				if(GetCustomer(oItem) == "")
				{
					strRet = @"<tr>
						<td class='BottomLeftRightBlack' align=left width=216>" + GetProduct(oItem) + @"</td>
						<td class='BottomRightBlack' align=right width=68>" + GetOnHand(oItem) + @"</td>
						<td class='BottomRightBlack' align=right width=90>" + GetEstimated(oItem) + @"</td>
						<td class='BottomRightBlack' align=right width=68>" + GetShipped(oItem) + @"</td>
						<td class='BottomRightBlack' align=right width=72><b>" + GetSubtotal(oItem) + @"</b></td>
						<td class='BottomRightBlack' align=right width=70>" + GetOrdered(oItem) + @"</td>
						<td class='BottomRightBlack' align=right width=72>" + GetTotalOrdered(oItem) + @"</td>
						<td class='BottomRightBlack' align=right width=72><b>" + GetTotal(oItem) + @"</b></td>
						<td class='BottomRightBlack' align=right width=68>" + GetOneDayAhead(oItem) + @"</td>
						<td class='BottomRightBlack' align=right width=68>" + GetTwoDaysAhead(oItem) + @"</td>
						<td class='BottomRightBlack' align=right width=72>" + GetThreePlusDaysAhead(oItem) + @"</td>
					</tr>";
					if (GetProduct(oItem).Length > 27)
						lineCount += 2;
					else
						lineCount += 1;
				}
				else
				{
					strRet = @"<tr>
						<td class='BottomGray' colspan='3' align=left nowrap width=358>" + GetCustomer(oItem) + @"</td>
						<td class='BottomGray' align=right width=68>" + GetShipped(oItem) + @"</td>
						<td class='BottomGray' align=right width=72>&nbsp;</td>
						<td class='BottomGray' align=right width=68>" + GetOrdered(oItem) + @"</td>
						<td class='BottomGray' align=right width=72>&nbsp;</td>
						<td class='BottomGray' align=right width=72>&nbsp;</td>
						<td class='BottomGray' align=right width=68>" + GetOneDayAhead(oItem) + @"</td>
						<td class='BottomGray' align=right width=68>" + GetTwoDaysAhead(oItem) + @"</td>
						<td class='BottomGray' align=right width=72>" + GetThreePlusDaysAhead(oItem) + @"</td>
					</tr>";
					lineCount += 1;
				}
			}

			return strRet;
		}

		private string GetCommodityName(object oItem)
		{
			InventoryCBO inv = (InventoryCBO)((RepeaterItem)oItem).DataItem;
			return inv.Commodity;
		}

		public string GetCommodity()
		{
			if(CommodityName == "")
				return "";
            
			return "Commodity: " + CommodityName + "; ";
		}

		public string GetWarehouse()
		{
			if(WarehouseName == "")
				return "";
            
			return "Warehouse: " + WarehouseName + "; ";
		}
		public string GetTime()
		{
			return " Time: " + System.DateTime.Now;
		}
		public string GetStartDate()
		{
			return "Ship Date: " + StartDate;
		}
		
		public string GetCustomer(object oItem)
		{
			InventoryCBO inv = (InventoryCBO)((RepeaterItem)oItem).DataItem;
			return inv.Customer;
		}

		public string GetProduct(object oItem)
		{
			InventoryCBO inv = (InventoryCBO)((RepeaterItem)oItem).DataItem;
			return inv.Product;
		}

		public string GetOnHand(object oItem)
		{
			InventoryCBO inv = (InventoryCBO)((RepeaterItem)oItem).DataItem;
			if (inv.OnHand == "0")
				return "&nbsp";
			else
				return Misc.DisplayFormat0dec(Convert.ToDouble(inv.OnHand));
		}

		public string GetEstimated(object oItem)
		{
			InventoryCBO inv = (InventoryCBO)((RepeaterItem)oItem).DataItem;
			if (inv.Estimated == "0")
				return "&nbsp";
			else
				return Misc.DisplayFormat0dec(Convert.ToDouble(inv.Estimated));
		}

		public string GetShipped(object oItem)
		{
			InventoryCBO inv = (InventoryCBO)((RepeaterItem)oItem).DataItem;
			if (inv.ShippedQnt == "0")
				return "&nbsp";
			else
				return Misc.DisplayFormat0dec(Convert.ToDouble(inv.ShippedQnt));
		}

		public string GetOrdered(object oItem)
		{
			InventoryCBO inv = (InventoryCBO)((RepeaterItem)oItem).DataItem;
			if (inv.OrderedQnt == "0")
				return "&nbsp";
			else
				return Misc.DisplayFormat0dec(Convert.ToDouble(inv.OrderedQnt));
		}
		public string GetTotalOrdered(object oItem)
		{	
			InventoryCBO inv = (InventoryCBO)((RepeaterItem)oItem).DataItem;
			if (Convert.ToDouble(inv.OrderedQnt) + Convert.ToDouble(inv.ShippedQnt) == 0)
				return "&nbsp";
			else
				return Misc.DisplayFormat0dec(Convert.ToDouble(inv.OrderedQnt) + Convert.ToDouble(inv.ShippedQnt));
		}
		public string GetSubtotal(object oItem)
		{
			InventoryCBO inv = (InventoryCBO)((RepeaterItem)oItem).DataItem;
			if (Convert.ToDouble(inv.OnHand) + Convert.ToDouble(inv.Estimated) - Convert.ToDouble(inv.ShippedQnt) == 0)
				return "&nbsp";
			else
				return Misc.DisplayFormat0dec(Convert.ToDouble(inv.OnHand) + Convert.ToDouble(inv.Estimated) - Convert.ToDouble(inv.ShippedQnt));
		}

		public string GetTotal(object oItem)
		{
			InventoryCBO inv = (InventoryCBO)((RepeaterItem)oItem).DataItem;
			if ((Convert.ToDouble(inv.OnHand) + Convert.ToDouble(inv.Estimated) - Convert.ToDouble(inv.ShippedQnt)) - Convert.ToDouble(inv.OrderedQnt) == 0)
				return "&nbsp";
			else
				return Misc.DisplayFormat0dec((Convert.ToDouble(inv.OnHand) + Convert.ToDouble(inv.Estimated) - Convert.ToDouble(inv.ShippedQnt)) - Convert.ToDouble(inv.OrderedQnt));
		}

		public string GetOneDayAhead(object oItem)
		{
			InventoryCBO inv = (InventoryCBO)((RepeaterItem)oItem).DataItem;
			if (inv.OneDayAhead == "0")
				return "&nbsp";
			else
				return Misc.DisplayFormat0dec(Convert.ToDouble(inv.OneDayAhead));
		}

		public string GetTwoDaysAhead(object oItem)
		{
			InventoryCBO inv = (InventoryCBO)((RepeaterItem)oItem).DataItem;
			if (inv.TwoDaysAhead == "0")
				return "&nbsp";
			else
				return Misc.DisplayFormat0dec(Convert.ToDouble(inv.TwoDaysAhead));
		}

		public string GetThreePlusDaysAhead(object oItem)
		{
			InventoryCBO inv = (InventoryCBO)((RepeaterItem)oItem).DataItem;
			if (inv.ThreePlusDaysAhead == "0")
				return "&nbsp";
			else
				return Misc.DisplayFormat0dec(Convert.ToDouble(inv.ThreePlusDaysAhead));
		}
		#endregion

		#region Private HelperFunctions
		public ArrayList GetData(string CommodityKey, string StartDate)
		{
			ArrayList ret = new ArrayList();

			#region sql code for On Hand and Production
			ArrayList IcQntArrayList = new ArrayList();

			SqlConnection Conn = GetSQLConn();
			string sqlCommand = @"
			use inventory;
			Select
				Product_Key,
				On_Hand,
				Production
			From
				[Warehouse_Product_Inventory]
			Where
				1=1

				#WhereClause:Warehouse_Key#
				#WhereClause:Commodity_Key#";

			WhereClauseBuilder whereClause2 = new WhereClauseBuilder();
			whereClause2.AddCriteriaBox(WhereClauseUnit.UnitType.Int32,"Warehouse_Key",Warehouses);
			whereClause2.AddCriteriaBox(WhereClauseUnit.UnitType.Int32,"Commodity_Key",Commodities);

			SqlDataAdapter selectCMD = new SqlDataAdapter(whereClause2.BuildWhereClause(sqlCommand), Conn);

			SqlDataReader myReader2 = selectCMD.SelectCommand.ExecuteReader();

			while(myReader2.Read())
			{
				IcQntArrayList.Add(new InventoryCBO(GetProductName(myReader2.GetValue(0).ToString()),"0","0",myReader2.GetValue(1).ToString(),myReader2.GetValue(2).ToString(),myReader2.GetValue(0).ToString(),"0","0","0",GetCommodity(myReader2.GetValue(0).ToString()),"0",false));
			}

			myReader2.Close();

			Conn.Close();
			Conn.Dispose();
            #endregion

            #region Get Ordered and Shipped for Main Company
            WhereClauseBuilder whereClause = new WhereClauseBuilder();
            OracleConnection OracleConn = GetOracleConn();
			#region oracle code for Ordered and Shipped
			string oracleSQL = @"
				Select	Ic_Product_Id.Name,
						Sum(Ar_So_Line.IcQnt) AS OrderedQnt,
						0 AS ShippedQnt,
						Ar_So_Line.ProductIdx,
						0 AS OnHandIcQnt,
						0 AS TodayIcQnt,
						Ic_Ps_Commodity.Name AS CommodityName,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer
						
						

				From	Ar_So_Line,
						Ar_Trx_Header,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location

				Where	Ar_Trx_Header.SoType In ('2','5','9') AND
						Ar_Trx_Header.SoStatus In ('2','3') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + StartDate + @"','MM/DD/YYYY') AND
						Ar_Trx_Header.ArTrxHdrIdx = Ar_So_Line.ArTrxHdrIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Warehouse.WarehouseIdx = Ar_So_Line.WarehouseIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Ic_Product_Id.Name,
						Ar_So_Line.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) 
				UNION ALL

				Select	Ic_Product_Id.Name,
						0 AS OrderedQnt,
						Sum(Ar_So_Line.IcQnt) AS ShippedQnt,
						Ar_So_Line.ProductIdx,
						0 AS OnHandIcQnt,
						0 AS TodayIcQnt,
						Ic_Ps_Commodity.Name AS CommodityName,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer
						
						

				From	Ar_So_Line,
						Ar_Trx_Header,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location

				Where	Ar_Trx_Header.SoType In ('2','5','9') AND
						Ar_Trx_Header.SoStatus In ('4','5') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + StartDate + @"','MM/DD/YYYY') AND
						Ar_Trx_Header.ArTrxHdrIdx = Ar_So_Line.ArTrxHdrIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Warehouse.WarehouseIdx = Ar_So_Line.WarehouseIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Ic_Product_Id.Name,
						Ar_So_Line.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) 
	
UNION ALL	
				Select  Ic_Product_Id.Name,
						0 AS OrderedQnt,
						Ar_Trx_Product.IcQnt AS ShippedQnt,
						Ar_Trx_Product.ProductIdx,
						0 AS OnHandIcQnt,
						0 AS TodayIcQnt,
						Ic_Ps_Commodity.Name AS CommodityName,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer

				From	Ar_Trx_Header,
						Ar_Trx_Line,
						Ar_Trx_Detail,
						Ar_Trx_Product,
						Ar_Trx_Header_Ship,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location

				Where	Ar_Trx_Header.SoType In ('2','5','9') AND
						Ar_Trx_Header.SoStatus In ('6','7','C') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + StartDate + @"','MM/DD/YYYY') AND
						Ar_Trx_Line.ArTrxHdrIdx    = Ar_Trx_Header.ArTrxHdrIdx AND
						Ar_Trx_Detail.ArTrxHdrIdx    = Ar_Trx_Line.ArTrxHdrIdx AND
						Ar_Trx_Detail.ArTrxLineTrxType   = Ar_Trx_Line.ArTrxLineTrxType AND
						Ar_Trx_Detail.ArTrxLineSeq    = Ar_Trx_Line.ArTrxLineSeq AND
						Ar_Trx_Detail.TrxType         = '1' AND
						Ar_Trx_Line.DeleteFlag      = 'N' AND
						Ar_Trx_Product.ArTrxHdrIdx    = Ar_Trx_Detail.ArTrxHdrIdx AND
						Ar_Trx_Product.ArTrxDtlSeq    = Ar_Trx_Detail.ArTrxDtlSeq AND
						Ar_Trx_Product.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_Trx_Product.ProductIdx = Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq AND
						Ar_Trx_Header_Ship.ShipHdrIdx    (+)= Ar_Trx_Detail.ShipHdrIdx AND
						Ic_Warehouse.NameIdx             (+)= Ar_Trx_Header_Ship.FromNameIdx AND 
						Ic_Warehouse.NameLocationSeq     (+)= Ar_Trx_Header_Ship.FromLocationSeq AND
						Ar_Trx_Detail.GlDeleteCode = 'N'

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Ic_Product_Id.Name,
						Ar_Trx_Product.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)),
						Ar_Trx_Product.IcQnt
			
				Order By 7,8,1,4";
#endregion
            //build the functions
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32, "Ic_Product_Produce.CmtyIdx", Commodities);
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32, "Ic_Warehouse.WarehouseIdx", Warehouses);
            OracleCommand OracleCmd = new OracleCommand(FormatOracle(whereClause.BuildWhereClause(oracleSQL)), OracleConn);
			OracleCmd.BindByName = true;
            Oracle.DataAccess.Client.OracleDataReader myReader = OracleCmd.ExecuteReader();
            while (myReader.Read()) 
			{
				//InventoryCBO(string product,string orderedQnt,string shippedQnt,string status,string onHand,string estimated,string productIdx,string oneDayAhead,string twoDaysAhead,string threePlusDaysAhead)
				ret.Add(new InventoryCBO(myReader.GetString(7).ToString(),myReader.GetValue(1).ToString(),myReader.GetValue(2).ToString(),myReader.GetValue(4).ToString(),myReader.GetValue(5).ToString(),myReader.GetValue(3).ToString(),"0","0","0",myReader.GetString(6).ToString(),myReader.GetString(8).ToString(),false));			
			}
            myReader.Close();
			myReader.Dispose();
			OracleCmd.Dispose();
            OracleConn.Close();
            OracleConn.Dispose();
            #endregion

            #region Get Ordered and Shipped for Michael Borzynski Company
            WhereClauseBuilder whereClause1 = new WhereClauseBuilder();
            string server = System.Configuration.ConfigurationSettings.AppSettings["FamousServer"];
            string report = "Company_31";
            OracleConnection OracleConn1 = new OracleConnection("Data Source=" + server + ";User ID=" + report + "_RPT;Password=FAMOUS");
            OracleConn1.Open();
            #region oracle code for Ordered and Shipped
            string oracleSQL1 = @"
				Select	Ic_Product_Id.Name,
						Sum(Ar_So_Line.IcQnt) AS OrderedQnt,
						0 AS ShippedQnt,
						Ar_So_Line.ProductIdx,
						0 AS OnHandIcQnt,
						0 AS TodayIcQnt,
						Ic_Ps_Commodity.Name AS CommodityName,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer
						
						

				From	Ar_So_Line,
						Ar_Trx_Header,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location

				Where	Ar_Trx_Header.SoType In ('2','5','9') AND
						Ar_Trx_Header.SoStatus In ('2','3') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + StartDate + @"','MM/DD/YYYY') AND
						Ar_Trx_Header.ArTrxHdrIdx = Ar_So_Line.ArTrxHdrIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Warehouse.WarehouseIdx = Ar_So_Line.WarehouseIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Ic_Product_Id.Name,
						Ar_So_Line.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) 
				UNION ALL

				Select	Ic_Product_Id.Name,
						0 AS OrderedQnt,
						Sum(Ar_So_Line.IcQnt) AS ShippedQnt,
						Ar_So_Line.ProductIdx,
						0 AS OnHandIcQnt,
						0 AS TodayIcQnt,
						Ic_Ps_Commodity.Name AS CommodityName,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer
						
						

				From	Ar_So_Line,
						Ar_Trx_Header,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location

				Where	Ar_Trx_Header.SoType In ('2','5','9') AND
						Ar_Trx_Header.SoStatus In ('4','5') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + StartDate + @"','MM/DD/YYYY') AND
						Ar_Trx_Header.ArTrxHdrIdx = Ar_So_Line.ArTrxHdrIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Warehouse.WarehouseIdx = Ar_So_Line.WarehouseIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Ic_Product_Id.Name,
						Ar_So_Line.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) 
	
UNION ALL	
				Select  Ic_Product_Id.Name,
						0 AS OrderedQnt,
						Ar_Trx_Product.IcQnt AS ShippedQnt,
						Ar_Trx_Product.ProductIdx,
						0 AS OnHandIcQnt,
						0 AS TodayIcQnt,
						Ic_Ps_Commodity.Name AS CommodityName,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer

				From	Ar_Trx_Header,
						Ar_Trx_Line,
						Ar_Trx_Detail,
						Ar_Trx_Product,
						Ar_Trx_Header_Ship,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location

				Where	Ar_Trx_Header.SoType In ('2','5','9') AND
						Ar_Trx_Header.SoStatus In ('6','7','C') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + StartDate + @"','MM/DD/YYYY') AND
						Ar_Trx_Line.ArTrxHdrIdx    = Ar_Trx_Header.ArTrxHdrIdx AND
						Ar_Trx_Detail.ArTrxHdrIdx    = Ar_Trx_Line.ArTrxHdrIdx AND
						Ar_Trx_Detail.ArTrxLineTrxType   = Ar_Trx_Line.ArTrxLineTrxType AND
						Ar_Trx_Detail.ArTrxLineSeq    = Ar_Trx_Line.ArTrxLineSeq AND
						Ar_Trx_Detail.TrxType         = '1' AND
						Ar_Trx_Line.DeleteFlag      = 'N' AND
						Ar_Trx_Product.ArTrxHdrIdx    = Ar_Trx_Detail.ArTrxHdrIdx AND
						Ar_Trx_Product.ArTrxDtlSeq    = Ar_Trx_Detail.ArTrxDtlSeq AND
						Ar_Trx_Product.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_Trx_Product.ProductIdx = Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq AND
						Ar_Trx_Header_Ship.ShipHdrIdx    (+)= Ar_Trx_Detail.ShipHdrIdx AND
						Ic_Warehouse.NameIdx             (+)= Ar_Trx_Header_Ship.FromNameIdx AND 
						Ic_Warehouse.NameLocationSeq     (+)= Ar_Trx_Header_Ship.FromLocationSeq AND
						Ar_Trx_Detail.GlDeleteCode = 'N'

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Ic_Product_Id.Name,
						Ar_Trx_Product.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)),
						Ar_Trx_Product.IcQnt
			
				Order By 7,8,1,4";
            #endregion
            //build the functions
            whereClause1.AddCriteriaBox(WhereClauseUnit.UnitType.Int32, "Ic_Product_Produce.CmtyIdx", Commodities);
            whereClause1.AddCriteriaBox(WhereClauseUnit.UnitType.Int32, "Ic_Warehouse.WarehouseIdx", Warehouses);
            OracleCommand OracleCmd1 = new OracleCommand(FormatOracle(whereClause1.BuildWhereClause(oracleSQL)), OracleConn1);
            OracleCmd1.BindByName = true;
            Oracle.DataAccess.Client.OracleDataReader myReader1 = OracleCmd1.ExecuteReader();
            while (myReader1.Read())
            {
                ret.Add(new InventoryCBO(myReader1.GetString(7).ToString(), myReader1.GetValue(1).ToString(), myReader1.GetValue(2).ToString(), myReader1.GetValue(4).ToString(), myReader1.GetValue(5).ToString(), myReader1.GetValue(3).ToString(), "0", "0", "0", myReader1.GetString(6).ToString(), myReader1.GetString(8).ToString(), false));
            }
            myReader1.Close();
            myReader1.Dispose();
            OracleCmd1.Dispose();
            OracleConn1.Close();
            OracleConn1.Dispose();
            #endregion

            #region oracle code for On Hand and Production
            /*string oracleSQL2 = @"
				Select	distinct
						0 AS OrderedQnt,
						0 AS ShippedQnt,
						Ap_Po_Product.ProductIdx,
						Ap_Po_Product.IcQnt AS OnHandIcQnt,
						0 AS TodayIcQnt,
						Ic_Ps_Commodity.Name AS CommodityName,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name))))))))))


				From	Ap_Po_Header,
						Ap_Po_Detail,
						Ap_Po_Product,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label
              where
						Ap_Po_Detail.GlDeleteCode = 'N' AND
						Ap_Po_Header.ApPoHdrIdx = Ap_Po_Detail.ApPoHdrIdx AND
						Ap_Po_Detail.ApPoHdrIdx = Ap_Po_Product.ApPoHdrIdx AND
						Ap_Po_Detail.ApPoDtlSeq = Ap_Po_Product.ApPoDtlSeq AND
						Ap_Po_Product.ProductIdx (+)= Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx 
						#WhereClause:Ap_Po_Header.ApPoNo#
						#WhereClause:Ic_Product_Produce.CmtyIdx#
				Union All

				Select	distinct
						0 AS OrderedQnt,
						0 AS ShippedQnt,
						Ap_Po_Product.ProductIdx,
						0 AS OnHandIcQnt,
						Ap_Po_Product.IcQnt AS TodayIcQnt,
						Ic_Ps_Commodity.Name AS CommodityName,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name))))))))))


				From	Ap_Po_Header Header,
						Ap_Po_Detail,
						Ap_Po_Product,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label

               Where
						Ap_Po_Detail.GlDeleteCode = 'N' AND
						Header.ApPoHdrIdx = Ap_Po_Detail.ApPoHdrIdx AND
						Ap_Po_Detail.ApPoHdrIdx = Ap_Po_Product.ApPoHdrIdx AND
						Ap_Po_Detail.ApPoDtlSeq = Ap_Po_Product.ApPoDtlSeq AND
						Ap_Po_Product.ProductIdx (+)= Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx 
						#WhereClause:Header.ApPoNo#
						#WhereClause:Ic_Product_Produce.CmtyIdx#
				Order By 5,6";

			ArrayList IcQntArrayList = new ArrayList();
			//Setup string to check PO Number.
			   string test = "%ONHAND";
			   string test2 = "%TODAY";
			if (WarehouseCode != "")
			{
				test = WarehouseCode + "ONHAND";
				test2 = WarehouseCode + "TODAY";
			}
			whereClause.Add(WhereClauseUnit.UnitType.LikeString,"Ap_Po_Header.ApPoNo",test);
			whereClause.Add(WhereClauseUnit.UnitType.LikeString,"Header.ApPoNo",test2);
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32, "Ic_Product_Produce.CmtyIdx", Commodities);
			OracleCommand OracleCmd2 = new OracleCommand(FormatOracle(whereClause.BuildWhereClause(oracleSQL2)), OracleConn);
			
			
			OracleCmd2.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader2 = OracleCmd2.ExecuteReader();

			while (myReader2.Read()) 
			{
				
				IcQntArrayList.Add(new InventoryCBO(myReader2.GetString(6).ToString(),myReader2.GetValue(0).ToString(),myReader2.GetValue(1).ToString(),myReader2.GetValue(3).ToString(),myReader2.GetValue(4).ToString(),myReader2.GetValue(2).ToString(),"0","0","0",myReader2.GetString(5).ToString(),"0",false));
			}

		
			myReader2.Close();
			myReader2.Dispose();
			OracleCmd2.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();*/
            #endregion

            //add the +1 day, +2 days and +3>10 days ahead values
            MergeData3(ret,CommodityKey,StartDate);
			ret.Sort();
			
			//Add Total line for each product
			AddTotalLines(ret);
			//Add ONHAND and PRODUCTION LINES
			ret = MergeData2(ret,IcQntArrayList);

			if (ret.Count == 0 && IcQntArrayList.Count == 0)
				JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"])+ "&&Records=None");

			return ret;
		}

		public ArrayList MergeData2(ArrayList ret, ArrayList IcQntArrayList)
		{
			bool edited;

			for(int j = 0; j < IcQntArrayList.Count; j++)
			{
				edited = false;
				for(int i = 0; i < ret.Count; i++)
				{
					if(((InventoryCBO)ret[i]).Product == ((InventoryCBO)IcQntArrayList[j]).Product && ((InventoryCBO)ret[i]).Customer == "")
					{
						((InventoryCBO)ret[i]).OnHand = Convert.ToString(Convert.ToDouble(((InventoryCBO)ret[i]).OnHand) + Convert.ToDouble(((InventoryCBO)IcQntArrayList[j]).OnHand));
						((InventoryCBO)ret[i]).Estimated = Convert.ToString(Convert.ToDouble(((InventoryCBO)ret[i]).Estimated) + Convert.ToDouble(((InventoryCBO)IcQntArrayList[j]).Estimated));
						edited = true;
						i = ret.Count;
					}

				}
				if(edited == false)
				{
					ret.Add(new InventoryCBO(((InventoryCBO)IcQntArrayList[j]).Product,((InventoryCBO)IcQntArrayList[j]).OrderedQnt,((InventoryCBO)IcQntArrayList[j]).ShippedQnt,((InventoryCBO)IcQntArrayList[j]).OnHand,((InventoryCBO)IcQntArrayList[j]).Estimated,((InventoryCBO)IcQntArrayList[j]).ProductIdx,"0","0","0",((InventoryCBO)IcQntArrayList[j]).Commodity,"",true));
				}
			}

			ret.Sort();
		
			return ret;
		}

		public void MergeData3(ArrayList ret,string CommodityKey,string StartDate)
		{
			string OneDayAhead = Convert.ToDateTime(StartDate).AddDays(1).ToShortDateString();
			string TwoDaysAhead = Convert.ToDateTime(StartDate).AddDays(2).ToShortDateString();
			string ThreePlusDaysAheadMin = Convert.ToDateTime(StartDate).AddDays(3).ToShortDateString();
			string ThreePlusDaysAheadMax = Convert.ToDateTime(StartDate).AddDays(10).ToShortDateString();
			OracleConnection OracleConn = GetOracleConn();
			WhereClauseBuilder whereClause = new WhereClauseBuilder();
			
			#region oracle code for OneDayAhead and TwoDaysAhead

			string oracleSQL = @"
				Select	Ar_So_Line.ProductIdx,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Sum(Ar_So_Line.IcQnt) AS OrderedQnt,
						'1 Day Ahead' AS ShipDateTime,
						Ic_Ps_Commodity.Name AS CommodityName,
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer

				From	Ar_So_Line,
						Ar_Trx_Header,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location

				Where	Ar_Trx_Header.SoType In ('2','5','9') AND
						Ar_Trx_Header.SoStatus In ('2','3') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + OneDayAhead + @"','MM/DD/YYYY') AND
						Ar_Trx_Header.ArTrxHdrIdx = Ar_So_Line.ArTrxHdrIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Produce.ProductIdx and
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Warehouse.WarehouseIdx (+)= Ar_So_Line.WarehouseIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq


						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Ar_So_Line.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) 

				UNION ALL

				Select	Ar_So_Line.ProductIdx,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Sum(Ar_So_Line.IcQnt) AS OrderedQnt,
						'2 Days Ahead' AS ShipDateTime,
						Ic_Ps_Commodity.Name AS CommodityName,
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer

				From	Ar_So_Line,
						Ar_Trx_Header,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location

				Where	Ar_Trx_Header.SoType In ('2','5','9') AND
						Ar_Trx_Header.SoStatus In ('2','3') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + TwoDaysAhead + @"','MM/DD/YYYY') AND
						Ar_Trx_Header.ArTrxHdrIdx = Ar_So_Line.ArTrxHdrIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Produce.ProductIdx and
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Warehouse.WarehouseIdx (+)= Ar_So_Line.WarehouseIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Ar_So_Line.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) 

				Order By 2,1";
			#endregion

			//build the functions
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32, "Ic_Product_Produce.CmtyIdx", Commodities);
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32, "Ic_Warehouse.WarehouseIdx", Warehouses);

			OracleCommand OracleCmd = new OracleCommand(FormatOracle(whereClause.BuildWhereClause(oracleSQL)), OracleConn);
			OracleCmd.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader = OracleCmd.ExecuteReader();
			
			while (myReader.Read()) 
			{
				if(myReader.GetString(3) == "1 Day Ahead")
						ret.Add(new InventoryCBO(myReader.GetString(1).ToString(),"0","0","0","0","0",myReader.GetValue(2).ToString(),"0","0",myReader.GetString(4).ToString(),myReader.GetString(5).ToString(),false));
					else
						ret.Add(new InventoryCBO(myReader.GetString(1).ToString(),"0","0","0","0","0","0",myReader.GetValue(2).ToString(),"0",myReader.GetString(4).ToString(),myReader.GetString(5).ToString(),false));
					
			}
			

			myReader.Close();
			myReader.Dispose();
			OracleCmd.Dispose();

			#region oracle code for 3 Plus Days Ahead

			string oracleSQL2 = @"
				Select	Ar_So_Line.ProductIdx,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Sum(Ar_So_Line.IcQnt) AS OrderedQnt,
						'3 Plus Days Ahead' AS ShipDateTime,
						Ic_Ps_Commodity.Name AS CommodityName,
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer

				From	Ar_So_Line,
						Ar_Trx_Header,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location

				Where	Ar_Trx_Header.SoType In ('2','5','9') AND
						Ar_Trx_Header.SoStatus In ('2','3') AND
						Ar_Trx_Header.ShipDateTime between
						Decode('" + ThreePlusDaysAheadMin + @"','',To_Date('01/01/1900','MM/DD/YYYY'),To_Date('" + ThreePlusDaysAheadMin + @"', 'MM/DD/YY')) AND
						Decode('" + ThreePlusDaysAheadMax + @"','',To_Date('01/01/2099','MM/DD/YYYY'),To_Date('" + ThreePlusDaysAheadMax + @"', 'MM/DD/YY')) AND
						Ar_Trx_Header.ArTrxHdrIdx = Ar_So_Line.ArTrxHdrIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Produce.ProductIdx and
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Warehouse.WarehouseIdx (+)= Ar_So_Line.WarehouseIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Ar_So_Line.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr))

				Order By 2,1";
			#endregion

			//build the functions
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32, "Ic_Product_Produce.CmtyIdx", Commodities);
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32, "Ic_Warehouse.WarehouseIdx", Warehouses);

			OracleCommand OracleCmd2 = new OracleCommand(FormatOracle(whereClause.BuildWhereClause(oracleSQL2)), OracleConn);
			OracleCmd2.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader2 = OracleCmd2.ExecuteReader();

			while (myReader2.Read()) 
			{
				ret.Add(new InventoryCBO(myReader2.GetString(1).ToString(),"0","0","0","0","0","0","0",myReader2.GetValue(2).ToString(),myReader2.GetString(4).ToString(),myReader2.GetString(5).ToString(),false));
			}

			myReader2.Close();
			myReader2.Dispose();
			OracleCmd2.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();
		}
		public void AddTotalLines(ArrayList ret)
		{
			int line = 0;
			string product = "";
			string lineCommodity = "";
			double shipped = 0;
			double ordered = 0;
			double day1 = 0;
			double day2 = 0;
			double day3 = 0;
			for(int j = ret.Count -1; j >= 0; j--)
			{
				if(((InventoryCBO)ret[j]).Product == product)
				{
					shipped += (Convert.ToDouble(((InventoryCBO)ret[j]).ShippedQnt));
					ordered += (Convert.ToDouble(((InventoryCBO)ret[j]).OrderedQnt));
					day1 += (Convert.ToDouble(((InventoryCBO)ret[j]).OneDayAhead));
					day2 += (Convert.ToDouble(((InventoryCBO)ret[j]).TwoDaysAhead));
					day3 += (Convert.ToDouble(((InventoryCBO)ret[j]).ThreePlusDaysAhead));
				}
				else
				{
					//if first line
					if (product == "")
					{
						shipped += (Convert.ToDouble(((InventoryCBO)ret[j]).ShippedQnt));
						ordered += (Convert.ToDouble(((InventoryCBO)ret[j]).OrderedQnt));
						day1 += (Convert.ToDouble(((InventoryCBO)ret[j]).OneDayAhead));
						day2 += (Convert.ToDouble(((InventoryCBO)ret[j]).TwoDaysAhead));
						day3 += (Convert.ToDouble(((InventoryCBO)ret[j]).ThreePlusDaysAhead));
						lineCommodity = (Convert.ToString(((InventoryCBO)ret[j]).Commodity));
						product = (Convert.ToString(((InventoryCBO)ret[j]).Product));
					}
						//insert total line in Arraylist
					else
					{
						line = j+1;
						ret.Insert(line,new InventoryCBO(product,Convert.ToString(ordered),Convert.ToString(shipped),"0","0","0",Convert.ToString(day1),Convert.ToString(day2),Convert.ToString(day3),lineCommodity,"",true));
						shipped = (Convert.ToDouble(((InventoryCBO)ret[j]).ShippedQnt));
						ordered = (Convert.ToDouble(((InventoryCBO)ret[j]).OrderedQnt));
						day1 = (Convert.ToDouble(((InventoryCBO)ret[j]).OneDayAhead));
						day2 = (Convert.ToDouble(((InventoryCBO)ret[j]).TwoDaysAhead));
						day3 = (Convert.ToDouble(((InventoryCBO)ret[j]).ThreePlusDaysAhead));
						lineCommodity = (Convert.ToString(((InventoryCBO)ret[j]).Commodity));
						product = (Convert.ToString(((InventoryCBO)ret[j]).Product));
					}
				}
			}
			//Add last Total Line
			ret.Insert(line,new InventoryCBO(product,Convert.ToString(ordered),Convert.ToString(shipped),"0","0","0",Convert.ToString(day1),Convert.ToString(day2),Convert.ToString(day3),lineCommodity,"",true));
		}
		private string GetProductName(string productIndex)
		{
			string returnProductName = "";

			OracleConnection OracleConn = GetOracleConn();
			string oracleSQL = FormatOracle(@"
				Select	Ic_Product_Produce.ProductIdx,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))) As FullName

				From	Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Product_Produce

				Where	Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Product_Produce.ProductIdx = :ProductIndex

				Group By
						Ic_Product_Produce.ProductIdx,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name))))))))))

				Order By Fullname");

			OracleCommand OracleCmd = new OracleCommand(oracleSQL, OracleConn);
			OracleCmd.Parameters.Add(":ProductIndex", productIndex);

			OracleCmd.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader2 = OracleCmd.ExecuteReader();

			while (myReader2.Read())
			{
				returnProductName = myReader2.GetValue(1).ToString();
			}

			myReader2.Close();
			myReader2.Dispose();
			OracleCmd.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();

			return returnProductName;
		}

		private string GetCommodity(string productIndex)
		{
			string returnCommodity = "";

			OracleConnection OracleConn = GetOracleConn();
			string oracleSQL = FormatOracle(@"
				Select	Ic_Ps_Commodity.Name AS CommodityName

				From	Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Product_Produce

				Where	Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Product_Produce.ProductIdx = :ProductIndex

				Group By
						Ic_Ps_Commodity.Name

				Order By CommodityName");

			OracleCommand OracleCmd = new OracleCommand(oracleSQL, OracleConn);
			OracleCmd.Parameters.Add(":ProductIndex", productIndex);

			OracleCmd.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader2 = OracleCmd.ExecuteReader();

			while (myReader2.Read())
			{
				returnCommodity = myReader2.GetValue(0).ToString();
			}

			myReader2.Close();
			myReader2.Dispose();
			OracleCmd.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();

			return returnCommodity;
		}
		#endregion

		#region public code
		//
		#endregion

		#region CBO
		
		public class InventoryCBO: IComparable
		{
			#region Declares
			/*
			public string Product;
			public string Shipped;
			public string Ordered;
			*/
			public string Product;
			public string OrderedQnt;
			public string ShippedQnt;
			//public string Status;
			public string OnHand;
			public string Estimated;
			public string ProductIdx;
			public string OneDayAhead;
			public string TwoDaysAhead;
			public string ThreePlusDaysAhead;
			public string Commodity;
			public string Customer;
			public bool IsTotal;
			#endregion

			#region Constructor
			//public InventoryCBO(string product,string shipped,string ordered)
            public InventoryCBO(string product, string orderedQnt, string shippedQnt,/*string status,*/string onHand, string estimated, string productIdx, string oneDayAhead, string twoDaysAhead, string threePlusDaysAhead, string commodity, string customer, bool isTotal)
			{
				/*
				Product = product;
				Shipped = shipped;
				Ordered = ordered;
				*/
				Product = product;
				OrderedQnt = orderedQnt;
				ShippedQnt = shippedQnt;
				//Status = status;
				OnHand = onHand;
				Estimated = estimated;
				ProductIdx = productIdx;
				OneDayAhead = oneDayAhead;
				TwoDaysAhead = twoDaysAhead;
				ThreePlusDaysAhead = threePlusDaysAhead;
				Commodity = commodity;
				Customer = customer;
				IsTotal = isTotal;
			}
		
			#endregion

			#region IComparable Members

			public int CompareTo(object obj)
			{
				int tempCompare;
                InventoryCBO Y = (InventoryCBO)obj;

				//Compare Desc
				tempCompare = this.Product.CompareTo(Y.Product);
				if(tempCompare != 0)
					return tempCompare;
				
				//Compare IsTotal
				tempCompare = this.IsTotal.CompareTo(Y.IsTotal);
				if(tempCompare != 0)
					return tempCompare*-1;//*-1 to change asc to dec

				//Compare ShippedQnt
				tempCompare = this.ShippedQnt.CompareTo(Y.ShippedQnt);
				if(tempCompare != 0)
					return tempCompare*-1;

				//Compare OrderedQnt
				tempCompare = this.OrderedQnt.CompareTo(Y.OrderedQnt);
				if(tempCompare != 0)
					return tempCompare*-1;

				//Compare OneDayAhead
				tempCompare = this.OneDayAhead.CompareTo(Y.OneDayAhead);
				if(tempCompare != 0)
					return tempCompare*-1;

				//Compare TwoDaysAhead
				tempCompare = this.TwoDaysAhead.CompareTo(Y.TwoDaysAhead);
				if(tempCompare != 0)
					return tempCompare*-1;

				//Compare ThreePlusDaysAhead
				tempCompare = this.ThreePlusDaysAhead.CompareTo(Y.ThreePlusDaysAhead);
				if(tempCompare != 0)
					return tempCompare*-1;

				return 0;
			}

			#endregion
		}
		#endregion
	}
}
