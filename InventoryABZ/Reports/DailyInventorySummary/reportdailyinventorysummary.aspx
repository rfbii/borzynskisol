<%@ Page language="c#" Inherits="Inventory.Reports.DailyInventorySummary.ReportDailyInventorySummary" Codebehind="ReportDailyInventorySummary.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Daily Inventory Report</title>
		<!-- Email Start -->
		<style type="text/css">.ReportTable.TD { FONT-SIZE: 8pt }
		.BottomBlack {FONT-SIZE: 10pt; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Courier New }
		.BottomRightBlack {FONT-SIZE: 10pt; BORDER-BOTTOM: black 1px solid; BORDER-RIGHT: black 1px solid; FONT-FAMILY: Courier New }
		.BottomLeftBlack {FONT-SIZE: 10pt; BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; FONT-FAMILY: Courier New }
		.BottomLeftRightBlack {FONT-SIZE: 10pt; BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; BORDER-RIGHT: black 1px solid; FONT-FAMILY: Courier New }
		.BottomGray {BORDER-BOTTOM: lightgrey 1px solid; FONT-SIZE: 10pt;FONT-FAMILY: Courier New }
		.Header {FONT-SIZE: 14pt;FONT-FAMILY: Courier New }
		</style>
		<!-- Email End -->
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C#" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<!--<style type="text/css">
			DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
		</style>-->
        <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body onbeforeprint="SetupPrinting();">
		<object id="factory" style="DISPLAY: none" codebase="http://www.meadroid.com/scriptx/ScriptX.cab#Version=6,1,431,2"
			classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814" viewastext="">
		</object>
		<script type="text/javascript" language="javascript">
		function SetupPrinting()
		{
			factory.printing.footer = "Printed: &d &t&bPage &p of &P&bDaily Inventory Summary Report";
			factory.printing.header = "";
			factory.printing.leftMargin = .5;
			factory.printing.rightMargin = .5;
			factory.printing.topMargin = .5;
			factory.printing.bottomMargin = .5;
			factory.printing.portrait = false;
		}
		</script>
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
				<table cellspacing="0" cellpadding="0" border="0" width="936">
					<tr>
						<td align="center" width="180"><asp:button id="refresh" runat="server" Text="Refresh"></asp:button></td>
					</tr>
				</table>
				<!-- Email Start -->
				<table cellspacing="0" cellpadding="0" border="0" width="936" class="ReportTable">
					<tr>
						<td class='Header'align="center" colspan="2">Daily Inventory Summary Report</td>
					</tr>
					<tr>
						<td class='BottomBlack' width="700"><%= GetCommodity() %><%= GetWarehouse() %><%= GetStartDate() %></td>
						<td class='BottomBlack' nowrap="nowrap" width="236"><%= GetTime() %></td>
					</tr>
				</table>
				<asp:repeater id="Repeater1" runat="server">
					<ItemTemplate>
						<%# CheckLineCount(Container)%>
						<%# CheckForChanges(Container)%>
						<%# ColumnHeader(Container)%>
						<%# Detail(Container)%>
					</ItemTemplate>
					<FooterTemplate>
						</table>
					</FooterTemplate>
				</asp:repeater><!-- Email End -->
				<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"--></div>
            </div>
		</form>
	</body>
</html>