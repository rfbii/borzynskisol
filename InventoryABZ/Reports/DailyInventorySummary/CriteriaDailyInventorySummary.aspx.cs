using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb; 
using Oracle.DataAccess.Client;
using KeyCentral.Functions;
using KeyCentral.Lookups;

namespace Inventory.Reports.DailyInventorySummary
{

	/// <summary>
	/// Summary description for Destination.
	/// </summary>
	public partial class CriteriaDailyInventorySummary : BasePage
	{
		#region Declares

		protected ReportSessionManager reportSessionManager;
		#endregion

		#region events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Daily Inventory Report") == false)
			{
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx");
			}

			if(startDate.Text == "")
				startDate.Text = System.DateTime.Today.ToShortDateString();

			#region SessionManager
			reportSessionManager = new ReportSessionManager(this,Request["ReportName"].ToString());

			if (Request.QueryString["Records"]== "None")
				Message.Text = "No Records Found.";
			
			//Load Vars from session if this report is in SessionManager
			if(IsPostBack == false && reportSessionManager.CheckReportVars())
                LoadVarsFromReportManager();

			#endregion
		}
		protected void CommodityList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Message.Text = "";

			commodityKey.Text = "";
			commodityName.Text = "";

			commodityName.Text = CommodityList.SelectedItem.Text;
			commodityKey.Text = CommodityList.SelectedItem.Value;

			//run.Enabled = true;
			//startDate.Enabled = true;
		}

		protected void WarehouseList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Message.Text = "";

			warehouseKey.Text = "";
			warehouseName.Text = "";

			//if warehouse has code, warehouseName is only the warehouse, excluding the code
			if(WarehouseList.SelectedItem.Text.Substring(2,1) == "-")
			{
				warehouseName.Text = WarehouseList.SelectedItem.Text.Substring(3);
			}
			else
			{
				warehouseName.Text = WarehouseList.SelectedItem.Text;
			}

			warehouseKey.Text = WarehouseList.SelectedItem.Value;

			//run.Enabled = true;
			//startDate.Enabled = true;
		}
		
		protected void Return_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Inventory/Reports");
		}

		protected void cancel_Click(object sender, System.EventArgs e)
		{
			Message.Text = "";
			commodityKey.Text = "";
			commodityName.Text = "";
			warehouseKey.Text = "";
			warehouseName.Text = "";

			startDate.Text = System.DateTime.Today.ToShortDateString();

			//run.Enabled = false;

			CommodityList.ClearSelection();
			CommodityList.SelectedIndex = -1;

			WarehouseList.ClearSelection();
			WarehouseList.SelectedIndex = -1;
		}

		protected void run_Click(object sender, System.EventArgs e)
		{
			//if(CommodityList.SelectedIndex != -1 && startDate.Text != "")
			if(startDate.Text != "")
			{
				SaveVarsToReportMananger();
				Response.Redirect("../../Reports/DailyInventorySummary/ReportDailyInventorySummary.aspx?LastPage=" + Misc.EncodeQueryString("../../Reports/DailyInventorySummary/CriteriaDailyInventorySummary.aspx?ReportName=" + Request["ReportName"]));
			}
		}
		
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			FillCommodityList();
			FillWarehouseList();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Private helpers
		private void FillCommodityList()
		{
			OracleConnection OracleConn = GetOracleConn();
			string oracleSQL = FormatOracle("Select Name, Cmtyidx From Ic_Ps_Commodity Order by Name");
			OracleCommand OracleCmd = new OracleCommand(oracleSQL, OracleConn);
			OracleCmd.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader = OracleCmd.ExecuteReader();

			while (myReader.Read()) 
			{
				CommodityList.Items.Add(new ListItem(myReader.GetValue(0).ToString(),myReader.GetValue(1).ToString()));
			}

			myReader.Close();
			myReader.Dispose();
			OracleCmd.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();

			if(CommodityList.Items.Count > 0)
				CommodityList.Enabled = true;
			else
				CommodityList.Enabled = false;
		}
		
		private void FillWarehouseList()
		{
			OracleConnection OracleConn = GetOracleConn();
			string oracleSQL = FormatOracle(@"
				Select
					Fc_Name_Location.Descr,
					Ic_Warehouse.WarehouseIdx
				From
					#COMPANY_NUMBER#.Ic_Warehouse,
					#COMPANY_NUMBER#.Fc_Name_Location
				Where
					Ic_Warehouse.NameIdx = Fc_Name_Location.NameIdx and
					Ic_Warehouse.NameLocationSeq = Fc_Name_Location.NameLocationSeq
				Order by Descr");
			OracleCommand OracleCmd = new OracleCommand(oracleSQL, OracleConn);
			OracleCmd.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader = OracleCmd.ExecuteReader();

			while (myReader.Read()) 
			{
				warehouseKey.Text = myReader.GetValue(1).ToString();
				warehouseName.Text = myReader.GetValue(0).ToString();

				SqlConnection Conn = GetSQLConn();
				SqlCommand selectCMD = new SqlCommand("use Inventory; select Warehouse_Code, Warehouse_Key from [Warehouse] where Warehouse_Key = @warehouseKey", Conn);
				selectCMD.Parameters.Add("@warehouseKey",warehouseKey.Text);

				SqlDataReader myReader2 = selectCMD.ExecuteReader();
			
				if(myReader2.HasRows ==true)
				{
					myReader2.Read();

					WarehouseList.Items.Add(new ListItem(myReader2.GetValue(0).ToString() + "-" + myReader.GetValue(0).ToString(),myReader.GetValue(1).ToString()));
				}
				else
				{
					WarehouseList.Items.Add(new ListItem(myReader.GetValue(0).ToString(),myReader.GetValue(1).ToString()));
				}
				myReader2.Close();
			}

			myReader.Close();
			myReader.Dispose();
			OracleCmd.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();
			warehouseName.Text = "";

			if(WarehouseList.Items.Count > 0)
				WarehouseList.Enabled = true;
			else
				WarehouseList.Enabled = false;
		}
		#endregion
		
		#region Provate Session Manager Functions
		private void LoadVarsFromReportManager()
		{
			startDate.Text = Session["StartDate"].ToString();
			CommodityList.SelectedIndex = Convert.ToInt32(Session["CommodityIndex"].ToString());
			commodityKey.Text = Session["CommodityKey"].ToString();
			commodityName.Text = Session["CommodityName"].ToString();
			WarehouseList.SelectedIndex = Convert.ToInt32(Session["WarehouseIndex"].ToString());
			warehouseKey.Text = Session["WarehouseKey"].ToString();
			warehouseName.Text = Session["WarehouseName"].ToString();
		}
		private void SaveVarsToReportMananger()
		{
			reportSessionManager.AddSession("CommodityKey",commodityKey.Text);
			reportSessionManager.AddSession("CommodityName",commodityName.Text);
			reportSessionManager.AddSession("CommodityIndex",CommodityList.SelectedIndex);

			if(CommodityList.SelectedIndex != -1)
			{
				ArrayList Commodities = new ArrayList();
				Commodities.Add(new CriteriaBoxRecord(commodityKey.Text,commodityName.Text,""));
				reportSessionManager.AddSession("Commodities",Commodities);
			}
			else
				reportSessionManager.AddSession("Commodities",new ArrayList());

			reportSessionManager.AddSession("WarehouseKey",warehouseKey.Text);
			reportSessionManager.AddSession("WarehouseName",warehouseName.Text);
			reportSessionManager.AddSession("WarehouseIndex",WarehouseList.SelectedIndex);

			string warehouseCode = "";
			if(WarehouseList.SelectedIndex != -1)
			{
				ArrayList Warehouses = new ArrayList();
				Warehouses.Add(new CriteriaBoxRecord(warehouseKey.Text,warehouseName.Text,""));
				reportSessionManager.AddSession("Warehouses",Warehouses);

				//if warehouse has code, warehouseCode is only the warehouse code, excluding the warehouse name
				if(WarehouseList.SelectedItem.Text.Substring(2,1) == "-")
					warehouseCode = WarehouseList.SelectedItem.Text.Substring(0,2);
			}
			else
				reportSessionManager.AddSession("Warehouses",new ArrayList());

			reportSessionManager.AddSession("WarehouseCode",warehouseCode);

			reportSessionManager.AddSession("StartDate",startDate.Text);

			//Let the reportSessionManager know that we are done
			reportSessionManager.SaveSessions();
		}
		#endregion
	}
}
