<%@ Page language="c#" Inherits="Inventory.Reports.DailyInventorySheet.ReportDailyInventorySheet" Codebehind="ReportDailyInventorySheet.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Daily Inventory Sheet Report</title>
		<!-- Email Start -->
		<style type="text/css">.ReportTable.TD { FONT-SIZE: 8pt; FONT-FAMILY: Arial }
		.BottomBlack {FONT-SIZE: 10pt; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Arial; }
		.BottomRightBlack {FONT-SIZE: 8pt; BORDER-BOTTOM: black 1px solid; BORDER-RIGHT: black 1px solid; FONT-FAMILY: Arial; }
		.BottomLeftBlack {FONT-SIZE: 8pt; BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; FONT-FAMILY: Arial; }
		.BottomLeftRightBlack {FONT-SIZE: 8pt; BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; BORDER-RIGHT: black 1px solid; FONT-FAMILY: Arial; }
		.BottomGray {BORDER-BOTTOM: lightgrey 1px solid; FONT-SIZE: 8pt; FONT-FAMILY: Arial;}
		.Header {FONT-SIZE: 14pt; FONT-FAMILY: Arial;}
		</style>
		<!-- Email End -->
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C#" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<!--<style type="text/css">
			DIV.scroll { OVERFLOW: auto; WIDTH: expression(document.body.clientWidth-11); HEIGHT: expression(document.body.clientHeight-213) }
		</style>-->
        <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
		<style type="text/css">
			DIV.scrollFooter
			{ 
				OVERFLOW: auto;
				WIDTH: expression(document.body.clientWidth);
				HEIGHT: expression(document.body.clientHeight-200);
			}
			DIV.scrollLeft
			{ 
				OVERFLOW: auto;
				WIDTH: 517px;
				HEIGHT: expression(document.body.clientHeight-225);
			}
			DIV.scrollRight
			{
				OVERFLOW: auto;
				WIDTH: expression(document.body.clientWidth-525);
				HEIGHT: expression(document.body.clientHeight-225);
			}
			DIV.scrollHeader
			{
				OVERFLOW: hidden;
				WIDTH: expression(document.body.clientWidth-540);
				/*HEIGHT: 144px;*/
			}
		</style>
	</head>
	<body onbeforeprint="SetupPrinting();">
		<object id="factory" style="DISPLAY: none" codebase="http://www.meadroid.com/scriptx/ScriptX.cab#Version=6,1,431,2" classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814" viewastext=""></object>
		<script type="text/javascript" language="javascript">
		function SetupPrinting()
		{
			factory.printing.footer = "Printed: &d &t&bPage &p of &P&bDaily Inventory Sheet Report";
			factory.printing.header = "";
			factory.printing.leftMargin = .5;
			factory.printing.rightMargin = .5;
			factory.printing.topMargin = .5;
			factory.printing.bottomMargin = .5;
			factory.printing.portrait = false;
		}
		</script>
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			 <div class="scroll" id="BodyDiv"> 
				<!--<div class="scrollHeader" id="HeaderDiv" ScrollWithBody="true">-->
				<div style='PADDING-LEFT: 15px;'>
				<table cellspacing="0" cellpadding="0" border="0" class="ReportTable">
					<tr>
						<td align="center" width="180"><asp:button id="refresh" runat="server" Text="Refresh"></asp:button></td>
					</tr>
				</table>
				<!-- Email Start -->
				<table cellspacing="0" cellpadding="0" border="0" class="ReportTable">
					<tr>
						<td class="Header" align="center">Daily Inventory Sheet Report</td>
					</tr>
					<tr>
						<td class="BottomBlack" nowrap="nowrap"><%= GetCommodity() %><%= GetWarehouse() %><%= GetStartDate() %><%= GetTime() %></td>
					</tr>
					<tr>
						<td width="100"><font size="2" face="Arial">&nbsp;</font></td>
					</tr>
				</table>
				</div>
				<table class="ReportTable">
					<tr>
					<asp:repeater id="Repeater1" runat="server">
						<ItemTemplate>
							<%# Columns()%>
						</ItemTemplate>
						<FooterTemplate>
							</table>
						</FooterTemplate>
					</asp:repeater>
					</tr>
				</table>
				<!-- Email End -->
				<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
				</div>
		</form>
	</body>
</html>