using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using KeyCentral.Functions;
using Oracle.DataAccess.Client;
using System.Text;
using KeyCentral.Lookups;

namespace Inventory.Reports.DailyInventorySheet
{
	public partial class ReportDailyInventorySheet : BasePage
	{
		#region Delcares
		private double lineCount = 0;
		private bool newPage = true;
		private bool firstPage = true;
		//private string strCommodity = "";
		//private bool commodityChanged = false;
		private ArrayList CustomersArrayList;
		private ArrayList CommoditiesArrayList;
		private ArrayList CustomersCommoditiesArrayList;
		private double GrandTotalToShip = 0;
		private double GrandTotalShipped = 0;
		private double GrandTotalOnHand = 0;
		private double GrandTotalProduction = 0;
		public string refreshRate;
		#endregion
		
		#region Param Properties
		private string CommodityName{get{return (string)Session["CommodityName"];}}
		private string CommodityKey{get{return (string)Session["CommodityKey"];}}
		private ArrayList Commodities{get{return (ArrayList)Session["Commodities"];}}
		private string WarehouseName{get{return (string)Session["WarehouseName"];}}
		private string WarehouseKey{get{return (string)Session["WarehouseKey"];}}
		private ArrayList Warehouses{get{return (ArrayList)Session["Warehouses"];}}
		private string StartDate{get{return (string)Session["StartDate"];}}
		//private string WarehouseCode{get{return (string)Session["WarehouseCode"];}}
		#endregion

		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Daily Inventory Report") == false)
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/MenuMain.aspx");

			//KeyCentral.Functions.JavaStatusMessage status = new KeyCentral.Functions.JavaStatusMessage(this);
			//status.ShowMessage("Computing");

			//Set page's refresh rate
            refreshRate = BaseDAL.ExecuteScalar("use Inventory; select Rate from [Refresh_Rate]").ToString();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.Clear();
            Response.AddHeader("Refresh", refreshRate + ";URL=../../Reports/DailyInventorySheet/ReportDailyInventorySheet.aspx?Lastpage=" + Misc.EncodeQueryString("../../Reports/DailyInventorySheet/CriteriaDailyInventorySheet.aspx?ReportName=InputDailyInventorySheet"));
           
			this.Repeater1.DataSource = GetData(CommodityKey, StartDate);

			//remove invalid entries
			for(int i = ((ArrayList)this.Repeater1.DataSource).Count - 1; i >= 0; i--)
			{
				Commodity commodity = (Commodity)((ArrayList)Repeater1.DataSource)[i];

				//if all the values are 0's, delete item
				if (commodity.Estimated == 0 &&
					commodity.OnHand == 0 &&
					commodity.OrderedQnt == 0 &&
					commodity.ShippedQnt == 0)
					((ArrayList)Repeater1.DataSource).RemoveAt(i);
			}
			if(((ArrayList)this.Repeater1.DataSource).Count != 0)
				Repeater1.DataBind();
			else
				JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"])+ "&&Records=None");
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion

		#region Display Code
		public string CheckLineCount(object oItem)
		{
			if (firstPage == true)
			{
				if(lineCount >= 36)
				{
					newPage = true;
					firstPage = false;
					lineCount = 0;
				}
			}
			else if (firstPage == false)
			{
				if(lineCount >= 36)
				{
					newPage = true;
					lineCount = 0;
				}
			}
			return "";
		}

		public string Columns() /*ColumnHeader()*/
		{
			/* div's layout
			 *
			 * | div			|	scrollHeader |
			 * | scrollLeft		|	scrollRight  |
			 * 
			*/

			//int tableWidth = 600 + (CustomersArrayList.Count * 130);
			int tableWidthLeft = 500;
			int tableWidthRight = (CustomersArrayList.Count * 130);

			string columnHeader="";
			string strWarehouseLocation = "";
			if(newPage)
			{
				if(firstPage == false)
				{
					columnHeader = @"
						<!-- email end -->
						</table>
						<table class='ReportTable'>
							<tr>
								<td height='20'><font size='2' face='Arial'>&nbsp;</font></td>
							</tr>
						</table><div style='page-break-before:always'><span style='visibility: hidden;'><font style='font-size:0pt;'>&nbsp;</font></span></div>
						<table border='0' cellpadding='0' cellspacing='0' width=" + tableWidthLeft + @" class='ReportTable'>";
				}
				else
				{
					columnHeader += @"
					<td>
						<div style='PADDING-LEFT: 15px;'>
						<table border='0' cellpadding='0' cellspacing='0' width=" + tableWidthLeft + @" class='ReportTable'>";
				}
				columnHeader = columnHeader + @"
					<tr>
						<td class='BottomLeftRightBlack' align=center width=55 height=35><b>ON HAND</b></td>
						<td class='BottomRightBlack' align=center width=55 height=35><b>PROD.</b></td>
						<td class='BottomRightBlack' align=center width=55 height=35><b>SHIP</b></td>
						<td class='BottomRightBlack' align=center width=55 height=35><b>SUB<br>TOTAL</b></td>
						<td class='BottomRightBlack' align=center width=55 height=35><b>TO SHIP</b></td>
						<td class='BottomRightBlack' align=center width=55 height=35><b>PROJ.</b></td>
						<td class='BottomRightBlack' align=center width=170 height=35><b>" + Convert.ToDateTime(StartDate).DayOfWeek.ToString() + " " + Convert.ToDateTime(StartDate).Month.ToString() + "/" + Convert.ToDateTime(StartDate).Day.ToString() + @"</b></td>
					</tr>
					<tr>
						<td class='BottomLeftRightBlack' align=right width=55 style='PADDING-RIGHT: 1px'><font style='FONT-SIZE: 9pt;'>" + Misc.DisplayFormat0dec(GrandTotalOnHand) + @"</font></td>
						<td class='BottomRightBlack' align=right width=55 style='PADDING-RIGHT: 1px'><font style='FONT-SIZE: 9pt;'>" + Misc.DisplayFormat0dec(GrandTotalProduction) + @"</font></td>
						<td class='BottomRightBlack' align=right width=55 style='PADDING-RIGHT: 1px'><font color='#0000FF' style='FONT-SIZE: 9pt;'>" + Misc.DisplayFormat0dec(GrandTotalShipped) + @"</font></td>
						<td class='BottomRightBlack' align=right width=55 style='PADDING-RIGHT: 1px'><font style='FONT-SIZE: 9pt;'>" + Misc.DisplayFormat0dec((GrandTotalOnHand + GrandTotalProduction) - GrandTotalShipped) + @"</font></td>
						<td class='BottomRightBlack' align=right width=55 style='PADDING-RIGHT: 1px'><font style='FONT-SIZE: 9pt;'>" + Misc.DisplayFormat0dec(GrandTotalToShip) + @"</font></td>
						<td class='BottomRightBlack' align=right width=55 style='PADDING-RIGHT: 1px'><font style='FONT-SIZE: 9pt;'>" + Misc.DisplayFormat0dec(((GrandTotalOnHand + GrandTotalProduction) - GrandTotalShipped) - GrandTotalToShip) + @"</font></td>
						<td class='BottomRightBlack' align=center width=170><b>STATUS</b></td>
					</tr>
					<tr>
						<td class='BottomLeftRightBlack' align=center width=55>&nbsp;</td>
						<td class='BottomRightBlack' align=center width=55>&nbsp;</td>
						<td class='BottomRightBlack' align=center width=55>&nbsp;</td>
						<td class='BottomRightBlack' align=center width=55>&nbsp;</td>
						<td class='BottomRightBlack' align=center width=55>&nbsp;</td>
						<td class='BottomRightBlack' align=center width=55>&nbsp;</td>
						<td class='BottomRightBlack' align=center width=170><b>TRUCK NAME</b></td>
					</tr>
				</table>
				</div>
			</td>
			<td>
				<div class='scrollHeader' id='HeaderDiv' scrollwithbody='true'>
				<table border='0' cellpadding='0' cellspacing='0' width=" + tableWidthRight + @" class='ReportTable'>
					<tr>";

				for(int i = 0; i < CustomersArrayList.Count; i++)
				{
					if(i == 0)
					{
						if(((Customer)CustomersArrayList[i]).CustomerName != "")
						{
							if(((Customer)CustomersArrayList[i]).CustomerName.Substring(((Customer)CustomersArrayList[i]).CustomerName.IndexOf(" ") + 1).Length > 15)
							{
								columnHeader += @"
									<td class='BottomLeftRightBlack' align=center width=130 height=35><b>" + ((Customer)CustomersArrayList[i]).CustomerName.Substring(0,((Customer)CustomersArrayList[i]).CustomerName.IndexOf(" ")) + "<br>" + ((Customer)CustomersArrayList[i]).CustomerName.Substring(((Customer)CustomersArrayList[i]).CustomerName.IndexOf(" ") + 1,15) + @"</b></td>";
							}
							else
							{
								columnHeader += @"
									<td class='BottomLeftRightBlack' align=center width=130 height=35><b>" + ((Customer)CustomersArrayList[i]).CustomerName.Substring(0,((Customer)CustomersArrayList[i]).CustomerName.IndexOf(" ")) + "<br>" + ((Customer)CustomersArrayList[i]).CustomerName.Substring(((Customer)CustomersArrayList[i]).CustomerName.IndexOf(" ") + 1) + @"</b></td>";
							}
						}
						else
						{
							columnHeader += @"
								<td class='BottomLeftRightBlack' align=center width=130 height=35>&nbsp;</td>";
						}
					}
					else
					{
						if(((Customer)CustomersArrayList[i]).CustomerName != "")
						{
							if(((Customer)CustomersArrayList[i]).CustomerName.Substring(((Customer)CustomersArrayList[i]).CustomerName.IndexOf(" ") + 1).Length > 15)
							{
								columnHeader += @"
									<td class='BottomRightBlack' align=center width=130 height=35><b>" + ((Customer)CustomersArrayList[i]).CustomerName.Substring(0,((Customer)CustomersArrayList[i]).CustomerName.IndexOf(" ")) + "<br>" + ((Customer)CustomersArrayList[i]).CustomerName.Substring(((Customer)CustomersArrayList[i]).CustomerName.IndexOf(" ") + 1,15) + @"</b></td>";
							}
							else
							{
								columnHeader += @"
									<td class='BottomRightBlack' align=center width=130 height=35><b>" + ((Customer)CustomersArrayList[i]).CustomerName.Substring(0,((Customer)CustomersArrayList[i]).CustomerName.IndexOf(" ")) + "<br>" + ((Customer)CustomersArrayList[i]).CustomerName.Substring(((Customer)CustomersArrayList[i]).CustomerName.IndexOf(" ") + 1) + @"</b></td>";
							}
						}
						else
						{
							columnHeader += @"
								<td class='BottomRightBlack' align=center width=130 height=35>&nbsp;</td>";
						}
					}
				}

				columnHeader += @"
					</tr>
					<tr>";

				for(int i = 0; i < CustomersArrayList.Count; i++)
				{
					if(i == 0)
					{
						if(((Customer)CustomersArrayList[i]).Status != "")
						{
							columnHeader += @"
							<td class='BottomLeftRightBlack' align=center width=130><b>" + ((Customer)CustomersArrayList[i]).Status + @"</b></td>";
						}
						else
						{
							columnHeader += @"
							<td class='BottomLeftRightBlack' align=center width=130>&nbsp;</td>";
						}
					}
					else
					{
						if(((Customer)CustomersArrayList[i]).Status != "")
						{
							columnHeader += @"
								<td class='BottomRightBlack' align=center width=130><b>" + ((Customer)CustomersArrayList[i]).Status + @"</b></td>";
						}
						else
						{
							columnHeader += @"
								<td class='BottomRightBlack' align=center width=130>&nbsp;</td>";
						}
					}
				}

				columnHeader += @"
					</tr>
					<tr>";

				for(int i = 0; i < CustomersArrayList.Count; i++)
				{
					if (i == 0)
					{
						if(((Customer)CustomersArrayList[i]).TruckName != "")
						{
							if(((Customer)CustomersArrayList[i]).TruckName.Length > 15)
							{
								columnHeader += @"
									<td class='BottomLeftRightBlack' align=center width=130><b>" + ((Customer)CustomersArrayList[i]).TruckName.Substring(0,15) + @"</b></td>";
							}
							else
							{
								columnHeader += @"
									<td class='BottomLeftRightBlack' align=center width=130><b>" + ((Customer)CustomersArrayList[i]).TruckName + @"</b></td>";
							}
						}
						else
						{
							columnHeader += @"
								<td class='BottomLeftRightBlack' align=center width=130>&nbsp;</td>";
						}
					}
					else
					{
						if(((Customer)CustomersArrayList[i]).TruckName != "")
						{
							if(((Customer)CustomersArrayList[i]).TruckName.Length > 15)
							{
								columnHeader += @"
									<td class='BottomRightBlack' align=center width=130><b>" + ((Customer)CustomersArrayList[i]).TruckName.Substring(0,15) + @"</b></td>";
							}
							else
							{
								columnHeader += @"
									<td class='BottomRightBlack' align=center width=130><b>" + ((Customer)CustomersArrayList[i]).TruckName + @"</b></td>";
							}
						}
						else
						{
							columnHeader += @"
								<td class='BottomRightBlack' align=center width=130>&nbsp;</td>";
						}
					}
				}
				columnHeader += @"
					</tr>
				</table>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class='scrollLeft' id='LeftDiv' scrollwithbody='true' dir='rtl' style='PADDING-LEFT: 15px;'>
				<div dir='ltr'>
				<table border='0' cellpadding='0' cellspacing='0' width=" + tableWidthLeft + " class='ReportTable'>";

				for(int i = 0; i < CommoditiesArrayList.Count; i++)
				{
					if(((Commodity)CommoditiesArrayList[i]).WarehouseLocation != strWarehouseLocation)
					{
						columnHeader += @"
						<tr>
							<td class='BottomLeftRightBlack' align=left width=500 height=30 colspan=7><b>" + ((Commodity)CommoditiesArrayList[i]).WarehouseLocation + @"</b></td>
						</tr>";

						strWarehouseLocation = ((Commodity)CommoditiesArrayList[i]).WarehouseLocation;
					}

					columnHeader += @"
					<tr>
						<td class='BottomLeftRightBlack' align=right width=55 height=30 style='PADDING-RIGHT: 1px'><font style='FONT-SIZE: 9pt;'>" + GetOnHand((Commodity)CommoditiesArrayList[i]) + @"</font></td>
						<td class='BottomRightBlack' align=right width=55 height=30 style='PADDING-RIGHT: 1px'><font style='FONT-SIZE: 9pt;'>" + GetProduction((Commodity)CommoditiesArrayList[i]) + @"</font></td>
						<td class='BottomRightBlack' align=right width=55 height=30 style='PADDING-RIGHT: 1px'><font color='#0000FF' style='FONT-SIZE: 9pt;'>" + GetTotalShipped((Commodity)CommoditiesArrayList[i]) + @"</font></td>
						<td class='BottomRightBlack' align=right width=55 height=30 style='PADDING-RIGHT: 1px'><font style='FONT-SIZE: 9pt;'>" + GetSubtotal((Commodity)CommoditiesArrayList[i]) + @"</font></td>
						<td class='BottomRightBlack' align=right width=55 height=30 style='PADDING-RIGHT: 1px'><font style='FONT-SIZE: 9pt;'>" + GetTotalToShip((Commodity)CommoditiesArrayList[i]) + @"</font></td>
						<td class='BottomRightBlack' align=right width=55 height=30 style='PADDING-RIGHT: 1px'><font style='FONT-SIZE: 9pt;'>" + GetProjected((Commodity)CommoditiesArrayList[i]) + @"</font></td>
						<td class='BottomRightBlack' align=left width=170 height=30><b>" + GetCommodityFullName((Commodity)CommoditiesArrayList[i]) + @"</b></td>
					</tr>";
				}

				columnHeader += @"
				</table>
				</div>
				</div>
			</td>
			<td>
				<div class='scrollRight' id='BodyDiv'>
				<table border='0' cellpadding='0' cellspacing='0' width=" + tableWidthRight + @" class='ReportTable'>
					<tr>";

				strWarehouseLocation = "";

				for(int i = 0; i < CommoditiesArrayList.Count; i++)
				{
					if(((Commodity)CommoditiesArrayList[i]).WarehouseLocation != strWarehouseLocation)
					{
						columnHeader += @"
						<tr>
							<td class='BottomLeftRightBlack' align=left width=" + tableWidthRight + @" height=30 colspan=" + CustomersArrayList.Count + @"><b>" + ((Commodity)CommoditiesArrayList[i]).WarehouseLocation + @"</b></td>
						</tr>";

						strWarehouseLocation = ((Commodity)CommoditiesArrayList[i]).WarehouseLocation;
					}

					columnHeader += @"
						<tr>";

					for(int j = 0; j < CustomersArrayList.Count; j++)
					{
						if(j == 0)
						{
							if(((Customer)CustomersArrayList[j]).Status == "Shipped")
							{
								columnHeader += @"
									<td class='BottomLeftRightBlack' align=right width=130 height=30 style='PADDING-RIGHT: 1px'><font color='#0000FF' style='FONT-SIZE: 11pt;'>" + GetCustomerCommodityQnt((Commodity)CommoditiesArrayList[i],((Customer)CustomersArrayList[j]).CustomerName) + @"</font></td>";
							}
							else
							{
								columnHeader += @"
									<td class='BottomLeftRightBlack' align=right width=130 height=30 style='PADDING-RIGHT: 1px'><font style='FONT-SIZE: 11pt;'>" + GetCustomerCommodityQnt((Commodity)CommoditiesArrayList[i],((Customer)CustomersArrayList[j]).CustomerName) + @"</font></td>";
							}
						}
						else
						{
							if(((Customer)CustomersArrayList[j]).Status == "Shipped")
							{
								columnHeader += @"
									<td class='BottomRightBlack' align=right width=130 height=30 style='PADDING-RIGHT: 1px'><font color='#0000FF' style='FONT-SIZE: 11pt;'>" + GetCustomerCommodityQnt((Commodity)CommoditiesArrayList[i],((Customer)CustomersArrayList[j]).CustomerName) + @"</font></td>";
							}
							else
							{
								columnHeader += @"
									<td class='BottomRightBlack' align=right width=130 height=30 style='PADDING-RIGHT: 1px'><font style='FONT-SIZE: 11pt;'>" + GetCustomerCommodityQnt((Commodity)CommoditiesArrayList[i],((Customer)CustomersArrayList[j]).CustomerName) + @"</font></td>";
							}						
						}
					}

					columnHeader += @"
						</tr>";
				}

				columnHeader += @"
				</table>
				</div>
			</td>";

				if (firstPage == false)
				{
					columnHeader = columnHeader + @"<!-- email start -->";
				}

				newPage = false;
			}

			return columnHeader;
		}

		public string Detail(object oItem)
		{
			string strRet="";
			/*strRet = @"
				<tr>
					<td class='BottomLeftRightBlack' align=right width=100>" + GetNeedToPack(oItem) + @"</td>
					<td class='BottomRightBlack' align=right width=100>" + GetOnHand(oItem) + @"</td>
					<td class='BottomRightBlack' align=right width=100>" + GetProduction(oItem) + @"</td>
					<td class='BottomRightBlack' align=right width=100>" + GetTotalToShip(oItem) + @"</td>
					<td class='BottomRightBlack' align=left width=200><b>" + GetCommodityFullName(oItem) + @"</b></td>";

			for(int i = 0; i < CustomersArrayList.Count; i++)
			{
				strRet += @"
					<td class='BottomRightBlack' align=right width=130>" + GetCustomerCommodityQnt(oItem,((Customer)CustomersArrayList[i]).CustomerName) + @"</td>";
			}

			strRet += @"
					</tr>";*/

			return strRet;
		}

		public string GetCommodity()
		{
			if(CommodityName == "")
				return "";

			return "Commodity: " + CommodityName + "; ";
		}

		public string GetWarehouse()
		{
			if(WarehouseName == "")
				return "";

			return "Warehouse: " + WarehouseName + "; ";
		}
		public string GetTime()
		{
			return "Time: " + System.DateTime.Now;
		}
		public string GetStartDate()
		{
			return "Ship Date: " + StartDate + "; ";
		}

		public string GetCommodityFullName(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			return commodity.CommodityFullName.Trim();
		}

		public string GetProjected(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			if (((commodity.OnHand + commodity.Estimated) - (commodity.TotalToShip + commodity.TotalShipped)) == 0)
				return "&nbsp;";
			else
				return Misc.DisplayFormat0dec((commodity.OnHand + commodity.Estimated) - (commodity.TotalToShip + commodity.TotalShipped));
		}

		public string GetOnHand(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			if (commodity.OnHand == 0)
				return "&nbsp;";
			else
				return Misc.DisplayFormat0dec(commodity.OnHand);
		}

		public string GetProduction(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			if (commodity.Estimated == 0)
				return "&nbsp;";
			else
				return Misc.DisplayFormat0dec(commodity.Estimated);
		}

		public string GetTotalToShip(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			if (commodity.TotalToShip == 0)
				return "&nbsp;";
			else
				return Misc.DisplayFormat0dec(commodity.TotalToShip);
		}

		public string GetTotalShipped(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			if (commodity.TotalShipped == 0)
				return "&nbsp;";
			else
				return Misc.DisplayFormat0dec(commodity.TotalShipped);
		}

		public string GetSubtotal(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			if (((commodity.OnHand + commodity.Estimated) - commodity.TotalShipped) == 0)
				return "&nbsp;";
			else
				return Misc.DisplayFormat0dec((commodity.OnHand + commodity.Estimated) - commodity.TotalShipped);
		}

		public string GetCustomerCommodityQnt(/*object oItem*/ Commodity commodity,string CustomerName)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			/*CustomerCommodity tempCustomerCommodity = new CustomerCommodity(CustomerName,commodity.CommodityFullName.Trim(),commodity.WarehouseLocation);
			int tempCustomerCommodityIdx = CustomersCommoditiesArrayList.BinarySearch(tempCustomerCommodity);

			if(tempCustomerCommodityIdx <0)
				return "&nbsp;";
			else
				tempCustomerCommodity = (CustomerCommodity)CustomersCommoditiesArrayList[tempCustomerCommodityIdx];

			return Misc.DisplayFormat0dec(tempCustomerCommodity.Qnt);*/

			for(int i = 0; i < CustomersCommoditiesArrayList.Count; i++)
			{
				if(((CustomerCommodity)CustomersCommoditiesArrayList[i]).CommodityFullName == commodity.CommodityFullName.Trim() && ((CustomerCommodity)CustomersCommoditiesArrayList[i]).CustomerName == CustomerName && ((CustomerCommodity)CustomersCommoditiesArrayList[i]).WarehouseLocation == commodity.WarehouseLocation)
				{
					return Misc.DisplayFormat0dec(((CustomerCommodity)CustomersCommoditiesArrayList[i]).Qnt);
				}
			}

			return "&nbsp;";
	}
		#endregion

		#region Private HelperFunctions
		public ArrayList GetData(string CommodityKey, string StartDate)
		{
			CustomersArrayList = new ArrayList();
			CommoditiesArrayList = new ArrayList();
			CustomersCommoditiesArrayList = new ArrayList();

			#region sql code for On Hand and Production
			SqlConnection Conn = GetSQLConn();
			string sqlCommand = @"
			use inventory;
			Select
				Product_Key,
				On_Hand,
				Production,
				Warehouse_Key,
				Commodity_Key
			From
				[Warehouse_Product_Inventory]
			Where
				1=1

				#WhereClause:Warehouse_Key#
				#WhereClause:Commodity_Key#";

			WhereClauseBuilder whereClause2 = new WhereClauseBuilder();
			whereClause2.AddCriteriaBox(WhereClauseUnit.UnitType.Int32,"Warehouse_Key",Warehouses);
			whereClause2.AddCriteriaBox(WhereClauseUnit.UnitType.Int32,"Commodity_Key",Commodities);

			SqlDataAdapter selectCMD = new SqlDataAdapter(whereClause2.BuildWhereClause(sqlCommand), Conn);

			SqlDataReader myReader2 = selectCMD.SelectCommand.ExecuteReader();

			while(myReader2.Read())
			{
				Commodity commodity = FindOrAddCommodity(CommoditiesArrayList,GetProductName(myReader2.GetValue(0).ToString()),GetWarehouseLocation(myReader2.GetValue(3).ToString()), myReader2.GetValue(3).ToString());
				commodity.OnHand += Convert.ToDouble(myReader2.GetValue(1).ToString());
				commodity.Estimated += Convert.ToDouble(myReader2.GetValue(2).ToString());
				commodity.ProductIdx = myReader2.GetValue(0).ToString();
				commodity.WarehouseIdx = myReader2.GetValue(3).ToString();

				if(!myReader2.IsDBNull(4))
					commodity.CommodityName = GetCommodityName(myReader2.GetValue(4).ToString());
				else
					commodity.CommodityName = "";
			}

			myReader2.Close();

			Conn.Close();
			Conn.Dispose();
            #endregion
            #region Get Ordered and Shipped Orders for main Company.
            WhereClauseBuilder whereClause = new WhereClauseBuilder();

			OracleConnection OracleConn = GetOracleConn();

			#region oracle code for Ordered and Shipped
			string oracleSQL = @"
				Select	Ic_Product_Id.Name,
						Sum(Ar_So_Line.IcQnt) AS OrderedQnt,
						0 AS ShippedQnt,
						Ar_So_Line.ProductIdx,
						NVL(Ic_Ps_Commodity.Name,'') AS CommodityName,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer,
						Ar_Trx_Header.Carrier,
						NVL(Ic_Warehouse.WarehouseIdx,'-1') as WarehouseIdx,
						NVL(Warehouse_Location.Descr,'') as Warehouse,
						NVL(LineDetail.FillType,''),
						NVL(Ar_Trx_Header.CoLocationSeq,'-1'),
						NVL(Header_Warehouse_Location.Descr,'') as HeaderWarehouse

				From	Ar_So_Line,
						Ar_Trx_Header,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location,
						Fc_Name_Location Warehouse_Location,
						Ic_Warehouse Header_Warehouse,
						Fc_Name_Location Header_Warehouse_Location,
                        (Select	Ar_Trx_Header.ArTrxHdrIdx,
						Ar_So_Product.FillType,
                        Ar_So_Line.ArSoLineSeq

				From	Ar_So_Line,
						Ar_Trx_Header,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location,
						Fc_Name_Location Warehouse_Location,
						Ar_So_Product,
						Ar_So_Detail,
						Ic_Warehouse Header_Warehouse,
						Fc_Name_Location Header_Warehouse_Location

				Where	Ar_Trx_Header.SoType In ('2','5','9') AND
						Ar_Trx_Header.SoStatus In ('2','3') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + StartDate + @"','MM/DD/YYYY') AND
						Ar_Trx_Header.ArTrxHdrIdx = Ar_So_Line.ArTrxHdrIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Warehouse.WarehouseIdx = Ar_So_Line.WarehouseIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq AND
						Warehouse_Location.NameIdx (+)= Ic_Warehouse.NameIdx and
						Warehouse_Location.NameLocationSeq (+)= Ic_Warehouse.NameLocationSeq AND
						Ar_So_Line.ArTrxHdrIdx = Ar_So_Detail.ArTrxHdrIdx AND
						Ar_So_Line.ArSoLineTrxType = Ar_So_Detail.ArSoLineTrxType AND
						Ar_So_Line.ArSoLineSeq = Ar_So_Detail.ArSoLineSeq AND
						Ar_So_Detail.ArTrxHdrIdx = Ar_So_Product.ArTrxHdrIdx  AND
						Ar_So_Detail.ArSoDtlSeq = Ar_So_Product.ArSoDtlSeq  AND
						Header_Warehouse.WarehouseIdx (+)= Ar_Trx_Header.CoLocationSeq AND
						Header_Warehouse_Location.NameIdx (+)= Header_Warehouse.NameIdx and
						Header_Warehouse_Location.NameLocationSeq (+)= Header_Warehouse.NameLocationSeq

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#) LineDetail

				Where	Ar_Trx_Header.SoType In ('2','5','9') AND
						Ar_Trx_Header.SoStatus In ('2','3') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + StartDate + @"','MM/DD/YYYY') AND
						Ar_Trx_Header.ArTrxHdrIdx = Ar_So_Line.ArTrxHdrIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Warehouse.WarehouseIdx = Ar_So_Line.WarehouseIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq AND
						Warehouse_Location.NameIdx (+)= Ic_Warehouse.NameIdx and
						Warehouse_Location.NameLocationSeq (+)= Ic_Warehouse.NameLocationSeq AND
						Header_Warehouse.WarehouseIdx (+)= Ar_Trx_Header.CoLocationSeq AND
						Header_Warehouse_Location.NameIdx (+)= Header_Warehouse.NameIdx and
						Header_Warehouse_Location.NameLocationSeq (+)= Header_Warehouse.NameLocationSeq and
                        LineDetail.ArTrxHdrIdx (+)= Ar_Trx_Header.ArTrxHdrIdx and
                        LineDetail.ArSoLineSeq (+)= Ar_So_Line.ArSoLineSeq 

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Ic_Product_Id.Name,
						Ar_So_Line.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)),
						Ar_Trx_Header.Carrier,
						Ic_Warehouse.WarehouseIdx,
						Warehouse_Location.Descr,												
						LineDetail.FillType,
						Ar_Trx_Header.CoLocationSeq,
						Header_Warehouse_Location.Descr

				UNION ALL

				Select	Ic_Product_Id.Name,
						0 AS OrderedQnt,
						Sum(NVL(Ar_So_Product.IcQnt, Ar_So_Line.OrderQnt)) AS ShippedQnt,
						Ar_So_Line.ProductIdx,
						NVL(Ic_Ps_Commodity.Name,'') AS CommodityName,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer,
						Ar_Trx_Header.Carrier,
						NVL(Ic_Warehouse.WarehouseIdx,'-1'),
						NVL(Warehouse_Location.Descr,''),
						Ar_So_Product.FillType,
						NVL(Ar_Trx_Header.CoLocationSeq,'-1'),
						NVL(Header_Warehouse_Location.Descr,'')

				From	Ar_So_Line,
						Ar_Trx_Header,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location,
						Fc_Name_Location Warehouse_Location,
						Ar_So_Product,
						Ar_So_Detail,
						Ic_Warehouse Header_Warehouse,
						Fc_Name_Location Header_Warehouse_Location

				Where	Ar_Trx_Header.SoType In ('2','5','9') AND
						Ar_Trx_Header.SoStatus In ('4','5') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + StartDate + @"','MM/DD/YYYY') AND
						Ar_Trx_Header.ArTrxHdrIdx = Ar_So_Line.ArTrxHdrIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Warehouse.WarehouseIdx = Ar_So_Line.WarehouseIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq AND
						Warehouse_Location.NameIdx (+)= Ic_Warehouse.NameIdx and
						Warehouse_Location.NameLocationSeq (+)= Ic_Warehouse.NameLocationSeq AND
						Ar_So_Line.ArTrxHdrIdx = Ar_So_Detail.ArTrxHdrIdx AND
						Ar_So_Line.ArSoLineTrxType = Ar_So_Detail.ArSoLineTrxType AND
						Ar_So_Line.ArSoLineSeq = Ar_So_Detail.ArSoLineSeq AND
						Ar_So_Detail.ArTrxHdrIdx = Ar_So_Product.ArTrxHdrIdx  AND
						Ar_So_Detail.ArSoDtlSeq = Ar_So_Product.ArSoDtlSeq  AND
						Header_Warehouse.WarehouseIdx (+)= Ar_Trx_Header.CoLocationSeq AND
						Header_Warehouse_Location.NameIdx (+)= Header_Warehouse.NameIdx and
						Header_Warehouse_Location.NameLocationSeq (+)= Header_Warehouse.NameLocationSeq

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Ic_Product_Id.Name,
						Ar_So_Line.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)),
						Ar_Trx_Header.Carrier,
						Ic_Warehouse.WarehouseIdx,
						Warehouse_Location.Descr,
						Ar_So_Product.FillType,
						Ar_Trx_Header.CoLocationSeq,
						Header_Warehouse_Location.Descr

				UNION ALL

				Select  Ic_Product_Id.Name,
						0 AS OrderedQnt,
						Sum(Ar_Trx_Product.IcQnt) AS ShippedQnt,
						Ar_Trx_Product.ProductIdx,
						NVL(Ic_Ps_Commodity.Name,'') AS CommodityName,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer,
						Ar_Trx_Header.Carrier,
						NVL(Ic_Warehouse.WarehouseIdx,'-1'),
						NVL(Warehouse_Location.Descr,''),
						Ar_Trx_Product.FillType,
						NVL(Ar_Trx_Header.CoLocationSeq,'-1'),
						NVL(Header_Warehouse_Location.Descr,'')

				From	Ar_Trx_Header,
						Ar_Trx_Line,
						Ar_Trx_Detail,
						Ar_Trx_Product,
						Ar_Trx_Header_Ship,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location,
						Fc_Name_Location Warehouse_Location,
						Ic_Warehouse Header_Warehouse,
						Fc_Name_Location Header_Warehouse_Location

				Where	Ar_Trx_Header.SoType In ('2','5','9') AND
						Ar_Trx_Header.SoStatus In ('6','7','C') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + StartDate + @"','MM/DD/YYYY') AND
						Ar_Trx_Line.ArTrxHdrIdx    = Ar_Trx_Header.ArTrxHdrIdx AND
						Ar_Trx_Detail.ArTrxHdrIdx    = Ar_Trx_Line.ArTrxHdrIdx AND
						Ar_Trx_Detail.ArTrxLineTrxType   = Ar_Trx_Line.ArTrxLineTrxType AND
						Ar_Trx_Detail.ArTrxLineSeq    = Ar_Trx_Line.ArTrxLineSeq AND
						Ar_Trx_Detail.TrxType         = '1' AND
						Ar_Trx_Line.DeleteFlag      = 'N' AND
						Ar_Trx_Product.ArTrxHdrIdx    = Ar_Trx_Detail.ArTrxHdrIdx AND
						Ar_Trx_Product.ArTrxDtlSeq    = Ar_Trx_Detail.ArTrxDtlSeq AND
						Ar_Trx_Product.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_Trx_Product.ProductIdx = Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq AND
						Ar_Trx_Header_Ship.ShipHdrIdx    (+)= Ar_Trx_Detail.ShipHdrIdx AND
						Ic_Warehouse.NameIdx             (+)= Ar_Trx_Header_Ship.FromNameIdx AND 
						Ic_Warehouse.NameLocationSeq     (+)= Ar_Trx_Header_Ship.FromLocationSeq AND
						Ar_Trx_Detail.GlDeleteCode = 'N' AND
						Warehouse_Location.NameIdx (+)= Ic_Warehouse.NameIdx and
						Warehouse_Location.NameLocationSeq (+)= Ic_Warehouse.NameLocationSeq AND
						Header_Warehouse.WarehouseIdx = Ar_Trx_Header.CoLocationSeq AND
						Header_Warehouse_Location.NameIdx (+)= Header_Warehouse.NameIdx and
						Header_Warehouse_Location.NameLocationSeq (+)= Header_Warehouse.NameLocationSeq

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Ic_Product_Id.Name,
						Ar_Trx_Product.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)),
						Ar_Trx_Product.IcQnt,
						Ar_Trx_Header.Carrier,
						Ic_Warehouse.WarehouseIdx,
						Warehouse_Location.Descr,
						Ar_Trx_Product.FillType,
						Ar_Trx_Header.CoLocationSeq,
						Header_Warehouse_Location.Descr

				Order By 7,8,1,4";

			#region old oracle code
			/*string oracleSQL = @"
				Select	Ic_Product_Id.Name,
						Sum(Ar_So_Line.IcQnt) AS OrderedQnt,
						0 AS ShippedQnt,
						Ar_So_Line.ProductIdx,
						Ic_Ps_Commodity.Name AS CommodityName,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer,
						Ar_Trx_Header.Carrier,
						Ic_Warehouse.WarehouseIdx,
						Warehouse_Location.Descr

				From	Ar_So_Line,
						Ar_Trx_Header,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location,
						Fc_Name_Location Warehouse_Location

				Where	Ar_Trx_Header.SoType In ('2','5') AND
						Ar_Trx_Header.SoStatus In ('2','3') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + StartDate + @"','MM/DD/YYYY') AND
						Ar_Trx_Header.ArTrxHdrIdx = Ar_So_Line.ArTrxHdrIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Warehouse.WarehouseIdx = Ar_So_Line.WarehouseIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq AND
						Warehouse_Location.NameIdx (+)= Ic_Warehouse.NameIdx and
						Warehouse_Location.NameLocationSeq (+)= Ic_Warehouse.NameLocationSeq

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Ic_Product_Id.Name,
						Ar_So_Line.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)),
						Ar_Trx_Header.Carrier,
						Ic_Warehouse.WarehouseIdx,
						Warehouse_Location.Descr

				UNION ALL

				Select	Ic_Product_Id.Name,
						0 AS OrderedQnt,
						Sum(Ar_So_Line.IcQnt) AS ShippedQnt,
						Ar_So_Line.ProductIdx,
						Ic_Ps_Commodity.Name AS CommodityName,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer,
						Ar_Trx_Header.Carrier,
						Ic_Warehouse.WarehouseIdx,
						Warehouse_Location.Descr

				From	Ar_So_Line,
						Ar_Trx_Header,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location,
						Fc_Name_Location Warehouse_Location

				Where	Ar_Trx_Header.SoType In ('2','5') AND
						Ar_Trx_Header.SoStatus In ('4','5') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + StartDate + @"','MM/DD/YYYY') AND
						Ar_Trx_Header.ArTrxHdrIdx = Ar_So_Line.ArTrxHdrIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Warehouse.WarehouseIdx = Ar_So_Line.WarehouseIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq AND
						Warehouse_Location.NameIdx (+)= Ic_Warehouse.NameIdx and
						Warehouse_Location.NameLocationSeq (+)= Ic_Warehouse.NameLocationSeq

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Ic_Product_Id.Name,
						Ar_So_Line.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)),
						Ar_Trx_Header.Carrier,
						Ic_Warehouse.WarehouseIdx,
						Warehouse_Location.Descr

				UNION ALL

				Select  Ic_Product_Id.Name,
						0 AS OrderedQnt,
						Ar_Trx_Product.IcQnt AS ShippedQnt,
						Ar_Trx_Product.ProductIdx,
						Ic_Ps_Commodity.Name AS CommodityName,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer,
						Ar_Trx_Header.Carrier,
						Ic_Warehouse.WarehouseIdx,
						Warehouse_Location.Descr

				From	Ar_Trx_Header,
						Ar_Trx_Line,
						Ar_Trx_Detail,
						Ar_Trx_Product,
						Ar_Trx_Header_Ship,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location,
						Fc_Name_Location Warehouse_Location

				Where	Ar_Trx_Header.SoType In ('2','5') AND
						Ar_Trx_Header.SoStatus In ('6','7','C') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + StartDate + @"','MM/DD/YYYY') AND
						Ar_Trx_Line.ArTrxHdrIdx    = Ar_Trx_Header.ArTrxHdrIdx AND
						Ar_Trx_Detail.ArTrxHdrIdx    = Ar_Trx_Line.ArTrxHdrIdx AND
						Ar_Trx_Detail.ArTrxLineTrxType   = Ar_Trx_Line.ArTrxLineTrxType AND
						Ar_Trx_Detail.ArTrxLineSeq    = Ar_Trx_Line.ArTrxLineSeq AND
						Ar_Trx_Detail.TrxType         = '1' AND
						Ar_Trx_Line.DeleteFlag      = 'N' AND
						Ar_Trx_Product.ArTrxHdrIdx    = Ar_Trx_Detail.ArTrxHdrIdx AND
						Ar_Trx_Product.ArTrxDtlSeq    = Ar_Trx_Detail.ArTrxDtlSeq AND
						Ar_Trx_Product.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_Trx_Product.ProductIdx = Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq AND
						Ar_Trx_Header_Ship.ShipHdrIdx    (+)= Ar_Trx_Detail.ShipHdrIdx AND
						Ic_Warehouse.NameIdx             (+)= Ar_Trx_Header_Ship.FromNameIdx AND 
						Ic_Warehouse.NameLocationSeq     (+)= Ar_Trx_Header_Ship.FromLocationSeq AND
						Ar_Trx_Detail.GlDeleteCode = 'N' AND
						Warehouse_Location.NameIdx (+)= Ic_Warehouse.NameIdx and
						Warehouse_Location.NameLocationSeq (+)= Ic_Warehouse.NameLocationSeq

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Ic_Product_Id.Name,
						Ar_Trx_Product.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)),
						Ar_Trx_Product.IcQnt,
						Ar_Trx_Header.Carrier,
						Ic_Warehouse.WarehouseIdx,
						Warehouse_Location.Descr

				Order By 7,8,1,4";*/
			#endregion
			#endregion

			//build the functions
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32, "Ic_Product_Produce.CmtyIdx", Commodities);
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32, "Ic_Warehouse.WarehouseIdx", Warehouses);

			OracleCommand OracleCmd = new OracleCommand(FormatOracle(whereClause.BuildWhereClause(oracleSQL)), OracleConn);
			OracleCmd.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader = OracleCmd.ExecuteReader();

            while (myReader.Read())
            {
                #region set customer
                Customer customer = FindOrAddCustomer(CustomersArrayList, myReader.GetString(6));

                if (!myReader.IsDBNull(7))
                {
                    customer.TruckName = myReader.GetString(7);
                }
                else
                {
                    customer.TruckName = "";
                }

                if (Convert.ToDouble(myReader.GetValue(1)) == 0 && Convert.ToDouble(myReader.GetValue(2)) != 0)
                {
                    customer.Status = "Shipped";
                }
                else if (Convert.ToDouble(myReader.GetValue(1)) != 0 && Convert.ToDouble(myReader.GetValue(2)) == 0)
                {
                    customer.Status = "Ordered";
                }
                else
                {
                    customer.Status = "";
                }
                #endregion

                #region set commodity
                string warehouseLocation = "";
                string warehouseIdx = "-1";
                //if Warehouse is null, get HeaderWarehouse instead
                if (!myReader.IsDBNull(9))
                {
                    warehouseLocation = myReader.GetString(9);
                    warehouseIdx = myReader.GetValue(8).ToString();
                }
                else
                {
                    warehouseLocation = myReader.GetString(12);
                    warehouseIdx = myReader.GetValue(11).ToString();
                }
                if (!myReader.IsDBNull(10))
                {
                    if (myReader.GetValue(10).ToString() == "3")
                        warehouseLocation = "OUTSIDE PURCHASE";
                }
                   
                    
                
                Commodity commodity = FindOrAddCommodity(CommoditiesArrayList, myReader.GetString(5), warehouseLocation, warehouseIdx);

                commodity.OrderedQnt += Convert.ToDouble(myReader.GetValue(1));
                commodity.ShippedQnt += Convert.ToDouble(myReader.GetValue(2));
                commodity.ProductIdx = myReader.GetValue(3).ToString();
                if (!myReader.IsDBNull(4))
                {
                    commodity.CommodityName = myReader.GetValue(4).ToString();
                }
               
             
                #endregion

                #region set customer & commodity
                CustomerCommodity customerCommodity = FindOrAddCustomerCommodity(CustomersCommoditiesArrayList, myReader.GetValue(6).ToString(), myReader.GetValue(5).ToString(), myReader.GetValue(12).ToString());

                if (Convert.ToDouble(myReader.GetValue(1)) == 0 && Convert.ToDouble(myReader.GetValue(2)) != 0)
                {
                    customerCommodity.Qnt += Convert.ToDouble(myReader.GetValue(2));
                }
                else if (Convert.ToDouble(myReader.GetValue(1)) != 0 && Convert.ToDouble(myReader.GetValue(2)) == 0)
                {
                    customerCommodity.Qnt += Convert.ToDouble(myReader.GetValue(1));
                }
                else
                {
                    customerCommodity.Qnt += 0;
                }

                //if Warehouse is null, get HeaderWarehouse instead
                if (!myReader.IsDBNull(9))
                {
                    customerCommodity.WarehouseLocation = myReader.GetString(9);
                }
                else
                {
                    customerCommodity.WarehouseLocation = myReader.GetString(12);
                }
				if(!myReader.IsDBNull(10))
				{
					if(myReader.GetValue(10).ToString() == "3")
						customerCommodity.WarehouseLocation = "OUTSIDE PURCHASE";
				}
				#endregion
			}

			myReader.Close();
			myReader.Dispose();
			OracleCmd.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();
            #endregion

            #region Get Ordered and Shipped Orders for Michael Borzynski Company.
            string server = System.Configuration.ConfigurationSettings.AppSettings["FamousServer"];
            string report = "Company_31";
            OracleConnection OracleConn1 = new OracleConnection("Data Source=" + server + ";User ID=" + report + "_RPT;Password=FAMOUS");
            OracleConn1.Open();
            WhereClauseBuilder whereClause1 = new WhereClauseBuilder();

            #region oracle code for Ordered and Shipped
            string oracleSQL1 = @"
				Select	Ic_Product_Id.Name,
						Sum(Ar_So_Line.IcQnt) AS OrderedQnt,
						0 AS ShippedQnt,
						Ar_So_Line.ProductIdx,
						NVL(Ic_Ps_Commodity.Name,'') AS CommodityName,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer,
						Ar_Trx_Header.Carrier,
						NVL(Ic_Warehouse.WarehouseIdx,'-1') as WarehouseIdx,
						NVL(Warehouse_Location.Descr,'') as Warehouse,
						NVL(LineDetail.FillType,''),
						NVL(Ar_Trx_Header.CoLocationSeq,'-1'),
						NVL(Header_Warehouse_Location.Descr,'') as HeaderWarehouse

				From	Ar_So_Line,
						Ar_Trx_Header,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location,
						Fc_Name_Location Warehouse_Location,
						Ic_Warehouse Header_Warehouse,
						Fc_Name_Location Header_Warehouse_Location,
                        (Select	Ar_Trx_Header.ArTrxHdrIdx,
						Ar_So_Product.FillType,
                        Ar_So_Line.ArSoLineSeq

				From	Ar_So_Line,
						Ar_Trx_Header,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location,
						Fc_Name_Location Warehouse_Location,
						Ar_So_Product,
						Ar_So_Detail,
						Ic_Warehouse Header_Warehouse,
						Fc_Name_Location Header_Warehouse_Location

				Where	Ar_Trx_Header.SoType In ('2','5','9') AND
						Ar_Trx_Header.SoStatus In ('2','3') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + StartDate + @"','MM/DD/YYYY') AND
						Ar_Trx_Header.ArTrxHdrIdx = Ar_So_Line.ArTrxHdrIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Warehouse.WarehouseIdx = Ar_So_Line.WarehouseIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq AND
						Warehouse_Location.NameIdx (+)= Ic_Warehouse.NameIdx and
						Warehouse_Location.NameLocationSeq (+)= Ic_Warehouse.NameLocationSeq AND
						Ar_So_Line.ArTrxHdrIdx = Ar_So_Detail.ArTrxHdrIdx AND
						Ar_So_Line.ArSoLineTrxType = Ar_So_Detail.ArSoLineTrxType AND
						Ar_So_Line.ArSoLineSeq = Ar_So_Detail.ArSoLineSeq AND
						Ar_So_Detail.ArTrxHdrIdx = Ar_So_Product.ArTrxHdrIdx  AND
						Ar_So_Detail.ArSoDtlSeq = Ar_So_Product.ArSoDtlSeq  AND
						Header_Warehouse.WarehouseIdx (+)= Ar_Trx_Header.CoLocationSeq AND
						Header_Warehouse_Location.NameIdx (+)= Header_Warehouse.NameIdx and
						Header_Warehouse_Location.NameLocationSeq (+)= Header_Warehouse.NameLocationSeq

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#) LineDetail

				Where	Ar_Trx_Header.SoType In ('2','5','9') AND
						Ar_Trx_Header.SoStatus In ('2','3') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + StartDate + @"','MM/DD/YYYY') AND
						Ar_Trx_Header.ArTrxHdrIdx = Ar_So_Line.ArTrxHdrIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Warehouse.WarehouseIdx = Ar_So_Line.WarehouseIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq AND
						Warehouse_Location.NameIdx (+)= Ic_Warehouse.NameIdx and
						Warehouse_Location.NameLocationSeq (+)= Ic_Warehouse.NameLocationSeq AND
						Header_Warehouse.WarehouseIdx (+)= Ar_Trx_Header.CoLocationSeq AND
						Header_Warehouse_Location.NameIdx (+)= Header_Warehouse.NameIdx and
						Header_Warehouse_Location.NameLocationSeq (+)= Header_Warehouse.NameLocationSeq and
                        LineDetail.ArTrxHdrIdx (+)= Ar_Trx_Header.ArTrxHdrIdx and
                        LineDetail.ArSoLineSeq (+)= Ar_So_Line.ArSoLineSeq 

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Ic_Product_Id.Name,
						Ar_So_Line.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)),
						Ar_Trx_Header.Carrier,
						Ic_Warehouse.WarehouseIdx,
						Warehouse_Location.Descr,						
						LineDetail.FillType,
						Ar_Trx_Header.CoLocationSeq,
						Header_Warehouse_Location.Descr

				UNION ALL

				Select	Ic_Product_Id.Name,
						0 AS OrderedQnt,
						Sum(Ar_So_Line.IcQnt) AS ShippedQnt,
						Ar_So_Line.ProductIdx,
						NVL(Ic_Ps_Commodity.Name,'') AS CommodityName,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer,
						Ar_Trx_Header.Carrier,
						NVL(Ic_Warehouse.WarehouseIdx,'-1'),
						NVL(Warehouse_Location.Descr,''),
						Ar_So_Product.FillType,
						NVL(Ar_Trx_Header.CoLocationSeq,'-1'),
						NVL(Header_Warehouse_Location.Descr,'')

				From	Ar_So_Line,
						Ar_So_Product,
						Ar_Trx_Header,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location,
						Fc_Name_Location Warehouse_Location,
						Ar_So_Detail,
						Ic_Warehouse Header_Warehouse,
						Fc_Name_Location Header_Warehouse_Location

				Where	Ar_Trx_Header.SoType In ('2','5','9') AND
						Ar_Trx_Header.SoStatus In ('4','5') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + StartDate + @"','MM/DD/YYYY') AND
						Ar_Trx_Header.ArTrxHdrIdx = Ar_So_Line.ArTrxHdrIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Warehouse.WarehouseIdx = Ar_So_Line.WarehouseIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq AND
						Warehouse_Location.NameIdx (+)= Ic_Warehouse.NameIdx and
						Warehouse_Location.NameLocationSeq (+)= Ic_Warehouse.NameLocationSeq AND
						Ar_So_Line.ArTrxHdrIdx = Ar_So_Detail.ArTrxHdrIdx AND
						Ar_So_Line.ArSoLineTrxType = Ar_So_Detail.ArSoLineTrxType AND
						Ar_So_Line.ArSoLineSeq = Ar_So_Detail.ArSoLineSeq AND
						Ar_So_Detail.ArTrxHdrIdx = Ar_So_Product.ArTrxHdrIdx AND
						Ar_So_Detail.ArSoDtlSeq = Ar_So_Product.ArSoDtlSeq AND
						Header_Warehouse.WarehouseIdx (+)= Ar_Trx_Header.CoLocationSeq AND
						Header_Warehouse_Location.NameIdx (+)= Header_Warehouse.NameIdx and
						Header_Warehouse_Location.NameLocationSeq (+)= Header_Warehouse.NameLocationSeq

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Ic_Product_Id.Name,
						Ar_So_Line.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)),
						Ar_Trx_Header.Carrier,
						Ic_Warehouse.WarehouseIdx,
						Warehouse_Location.Descr,
						Ar_So_Product.FillType,
						Ar_Trx_Header.CoLocationSeq,
						Header_Warehouse_Location.Descr

				UNION ALL

				Select  Ic_Product_Id.Name,
						0 AS OrderedQnt,
						Sum(Ar_Trx_Product.IcQnt) AS ShippedQnt,
						Ar_Trx_Product.ProductIdx,
						NVL(Ic_Ps_Commodity.Name,'') AS CommodityName,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer,
						Ar_Trx_Header.Carrier,
						NVL(Ic_Warehouse.WarehouseIdx,'-1'),
						NVL(Warehouse_Location.Descr,''),
						Ar_Trx_Product.FillType,
						NVL(Ar_Trx_Header.CoLocationSeq,'-1'),
						NVL(Header_Warehouse_Location.Descr,'')

				From	Ar_Trx_Header,
						Ar_Trx_Line,
						Ar_Trx_Detail,
						Ar_Trx_Product,
						Ar_Trx_Header_Ship,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location,
						Fc_Name_Location Warehouse_Location,
						Ic_Warehouse Header_Warehouse,
						Fc_Name_Location Header_Warehouse_Location

				Where	Ar_Trx_Header.SoType In ('2','5','9') AND
						Ar_Trx_Header.SoStatus In ('6','7','C') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + StartDate + @"','MM/DD/YYYY') AND
						Ar_Trx_Line.ArTrxHdrIdx    = Ar_Trx_Header.ArTrxHdrIdx AND
						Ar_Trx_Detail.ArTrxHdrIdx    = Ar_Trx_Line.ArTrxHdrIdx AND
						Ar_Trx_Detail.ArTrxLineTrxType   = Ar_Trx_Line.ArTrxLineTrxType AND
						Ar_Trx_Detail.ArTrxLineSeq    = Ar_Trx_Line.ArTrxLineSeq AND
						Ar_Trx_Detail.TrxType         = '1' AND
						Ar_Trx_Line.DeleteFlag      = 'N' AND
						Ar_Trx_Product.ArTrxHdrIdx    = Ar_Trx_Detail.ArTrxHdrIdx AND
						Ar_Trx_Product.ArTrxDtlSeq    = Ar_Trx_Detail.ArTrxDtlSeq AND
						Ar_Trx_Product.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_Trx_Product.ProductIdx = Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq AND
						Ar_Trx_Header_Ship.ShipHdrIdx    (+)= Ar_Trx_Detail.ShipHdrIdx AND
						Ic_Warehouse.NameIdx             (+)= Ar_Trx_Header_Ship.FromNameIdx AND 
						Ic_Warehouse.NameLocationSeq     (+)= Ar_Trx_Header_Ship.FromLocationSeq AND
						Ar_Trx_Detail.GlDeleteCode = 'N' AND
						Warehouse_Location.NameIdx (+)= Ic_Warehouse.NameIdx and
						Warehouse_Location.NameLocationSeq (+)= Ic_Warehouse.NameLocationSeq AND
						Header_Warehouse.WarehouseIdx = Ar_Trx_Header.CoLocationSeq AND
						Header_Warehouse_Location.NameIdx (+)= Header_Warehouse.NameIdx and
						Header_Warehouse_Location.NameLocationSeq (+)= Header_Warehouse.NameLocationSeq

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Ic_Product_Id.Name,
						Ar_Trx_Product.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)),
						Ar_Trx_Product.IcQnt,
						Ar_Trx_Header.Carrier,
						Ic_Warehouse.WarehouseIdx,
						Warehouse_Location.Descr,
						Ar_Trx_Product.FillType,
						Ar_Trx_Header.CoLocationSeq,
						Header_Warehouse_Location.Descr

				Order By 7,8,1,4";

          
            #endregion

            //build the functions
            whereClause1.AddCriteriaBox(WhereClauseUnit.UnitType.Int32, "Ic_Product_Produce.CmtyIdx", Commodities);
            whereClause1.AddCriteriaBox(WhereClauseUnit.UnitType.Int32, "Ic_Warehouse.WarehouseIdx", Warehouses);

            OracleCommand OracleCmd1 = new OracleCommand(FormatOracle(whereClause1.BuildWhereClause(oracleSQL1)), OracleConn1);
            OracleCmd1.BindByName = true;

            Oracle.DataAccess.Client.OracleDataReader myReader1 = OracleCmd1.ExecuteReader();

            while (myReader1.Read())
            {
                #region set customer
                Customer customer = FindOrAddCustomer(CustomersArrayList, myReader1.GetString(6));

                if (!myReader1.IsDBNull(7))
                {
                    customer.TruckName = myReader1.GetString(7);
                }
                else
                {
                    customer.TruckName = "";
                }

                if (Convert.ToDouble(myReader1.GetValue(1)) == 0 && Convert.ToDouble(myReader1.GetValue(2)) != 0)
                {
                    customer.Status = "Shipped";
                }
                else if (Convert.ToDouble(myReader1.GetValue(1)) != 0 && Convert.ToDouble(myReader1.GetValue(2)) == 0)
                {
                    customer.Status = "Ordered";
                }
                else
                {
                    customer.Status = "";
                }
                #endregion

                #region set commodity
                string warehouseLocation = "";
                string warehouseIdx = "-1";
                //if Warehouse is null, get HeaderWarehouse instead
                if (!myReader1.IsDBNull(9))
                {
                    warehouseLocation = myReader1.GetString(9);
                    warehouseIdx = myReader1.GetValue(8).ToString();
                }
                else
                {
                    warehouseLocation = myReader1.GetString(12);
                    warehouseIdx = myReader1.GetValue(11).ToString();
                }
                if (!myReader1.IsDBNull(10))
                {
                    if (myReader1.GetValue(10).ToString() == "3")
                        warehouseLocation = "OUTSIDE PURCHASE";
                }



                Commodity commodity = FindOrAddCommodity(CommoditiesArrayList, myReader1.GetString(5), warehouseLocation, warehouseIdx);

                commodity.OrderedQnt += Convert.ToDouble(myReader1.GetValue(1));
                commodity.ShippedQnt += Convert.ToDouble(myReader1.GetValue(2));
                commodity.ProductIdx = myReader1.GetValue(3).ToString();
                if (!myReader1.IsDBNull(4))
                {
                    commodity.CommodityName = myReader1.GetValue(4).ToString();
                }


                #endregion

                #region set customer & commodity
                CustomerCommodity customerCommodity = FindOrAddCustomerCommodity(CustomersCommoditiesArrayList, myReader1.GetValue(6).ToString(), myReader1.GetValue(5).ToString(), myReader1.GetValue(12).ToString());

                if (Convert.ToDouble(myReader1.GetValue(1)) == 0 && Convert.ToDouble(myReader1.GetValue(2)) != 0)
                {
                    customerCommodity.Qnt += Convert.ToDouble(myReader1.GetValue(2));
                }
                else if (Convert.ToDouble(myReader1.GetValue(1)) != 0 && Convert.ToDouble(myReader1.GetValue(2)) == 0)
                {
                    customerCommodity.Qnt += Convert.ToDouble(myReader1.GetValue(1));
                }
                else
                {
                    customerCommodity.Qnt += 0;
                }

                //if Warehouse is null, get HeaderWarehouse instead
                if (!myReader1.IsDBNull(9))
                {
                    customerCommodity.WarehouseLocation = myReader1.GetString(9);
                }
                else
                {
                    customerCommodity.WarehouseLocation = myReader1.GetString(12);
                }
                if (!myReader1.IsDBNull(10))
                {
                    if (myReader1.GetValue(10).ToString() == "3")
                        customerCommodity.WarehouseLocation = "OUTSIDE PURCHASE";
                }
                #endregion
            }

            myReader1.Close();
            myReader1.Dispose();
            OracleCmd1.Dispose();
            OracleConn1.Close();
            OracleConn1.Dispose();
            #endregion

            //sql pull

            #region old oracle code for On Hand and Production
            /*
                        #region oracle code for On Hand and Production
                        string oracleSQL2 = @"
                            Select	distinct
                                    0 AS OrderedQnt,
                                    0 AS ShippedQnt,
                                    Ap_Po_Product.ProductIdx,
                                    Ap_Po_Product.IcQnt AS OnHandIcQnt,
                                    0 AS TodayIcQnt,
                                    Ic_Ps_Commodity.Name AS CommodityName,
                                    Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name))))))))))

                            From	Ap_Po_Header,
                                    Ap_Po_Detail,
                                    Ap_Po_Product,
                                    Ic_Product_Produce,
                                    Ic_Ps_Commodity,
                                    Ic_Ps_Style,
                                    Ic_Ps_Size,
                                    Ic_Ps_Variety,
                                    Ic_Ps_Grade,
                                    Ic_Ps_Label
                            Where
                                    Ap_Po_Detail.GlDeleteCode = 'N' AND
                                    Ap_Po_Header.ApPoHdrIdx = Ap_Po_Detail.ApPoHdrIdx AND
                                    Ap_Po_Detail.ApPoHdrIdx = Ap_Po_Product.ApPoHdrIdx AND
                                    Ap_Po_Detail.ApPoDtlSeq = Ap_Po_Product.ApPoDtlSeq AND
                                    Ap_Po_Product.ProductIdx (+)= Ic_Product_Produce.ProductIdx AND
                                    Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
                                    Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
                                    Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
                                    Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
                                    Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
                                    Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx 
                                    #WhereClause:Ap_Po_Header.ApPoNo#
                                    #WhereClause:Ic_Product_Produce.CmtyIdx#

                            Union All

                            Select	distinct
                                    0 AS OrderedQnt,
                                    0 AS ShippedQnt,
                                    Ap_Po_Product.ProductIdx,
                                    0 AS OnHandIcQnt,
                                    Ap_Po_Product.IcQnt AS TodayIcQnt,
                                    Ic_Ps_Commodity.Name AS CommodityName,
                                    Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name))))))))))

                            From	Ap_Po_Header Header,
                                    Ap_Po_Detail,
                                    Ap_Po_Product,
                                    Ic_Product_Produce,
                                    Ic_Ps_Commodity,
                                    Ic_Ps_Style,
                                    Ic_Ps_Size,
                                    Ic_Ps_Variety,
                                    Ic_Ps_Grade,
                                    Ic_Ps_Label

                            Where
                                    Ap_Po_Detail.GlDeleteCode = 'N' AND
                                    Header.ApPoHdrIdx = Ap_Po_Detail.ApPoHdrIdx AND
                                    Ap_Po_Detail.ApPoHdrIdx = Ap_Po_Product.ApPoHdrIdx AND
                                    Ap_Po_Detail.ApPoDtlSeq = Ap_Po_Product.ApPoDtlSeq AND
                                    Ap_Po_Product.ProductIdx (+)= Ic_Product_Produce.ProductIdx AND
                                    Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
                                    Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
                                    Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
                                    Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
                                    Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
                                    Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx 
                                    #WhereClause:Header.ApPoNo#
                                    #WhereClause:Ic_Product_Produce.CmtyIdx#
                            Order By 5,6";
                        #endregion

                        //Setup string to check PO Number.
                        string test = "%ONHAND";
                        string test2 = "%TODAY";
                        if (WarehouseCode != "")
                        {
                            test = WarehouseCode + "ONHAND";
                            test2 = WarehouseCode + "TODAY";
                        }
                        whereClause.Add(WhereClauseUnit.UnitType.LikeString,"Ap_Po_Header.ApPoNo",test);
                        whereClause.Add(WhereClauseUnit.UnitType.LikeString,"Header.ApPoNo",test2);
                        whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32, "Ic_Product_Produce.CmtyIdx", Commodities);
                        OracleCommand OracleCmd2 = new OracleCommand(FormatOracle(whereClause.BuildWhereClause(oracleSQL2)), OracleConn);

                        OracleCmd2.BindByName = true;

                        Oracle.DataAccess.Client.OracleDataReader myReader2 = OracleCmd2.ExecuteReader();

                        while (myReader2.Read()) 
                        {
                            #region set on hand and production
                            Commodity commodity = FindOrAddCommodity(CommoditiesArrayList,myReader2.GetString(6));

                            commodity.OnHand += Convert.ToDouble(myReader2.GetValue(3));
                            commodity.Estimated += Convert.ToDouble(myReader2.GetValue(4));
                            #endregion
                        }

                        myReader2.Close();
                        myReader2.Dispose();

                        OracleCmd2.Dispose();*/
            #endregion

            //Compute totals for commodities
            ComputeTotals();

			SortCommoditiesArrayList();
			SortCustomersCommoditiesArrayList();

			if (CustomersArrayList.Count == 0 && CommoditiesArrayList.Count == 0 && CustomersCommoditiesArrayList.Count == 0)
				JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"])+ "&&Records=None");

			return CommoditiesArrayList;
		}

		private void ComputeTotals()
		{
			for(int i = 0; i < CommoditiesArrayList.Count; i++)
			{
				Commodity commodity = (Commodity)CommoditiesArrayList[i];
				commodity.TotalToShip = commodity.OrderedQnt;
				commodity.TotalShipped = commodity.ShippedQnt;

				GrandTotalToShip += commodity.TotalToShip;
				GrandTotalShipped += commodity.TotalShipped;
				GrandTotalOnHand += commodity.OnHand;
				GrandTotalProduction += commodity.Estimated;
			}
		}

		private void SortCommoditiesArrayList()
		{
			ArrayList tempArrayList = new ArrayList();
			ArrayList outsidePurchaseArrayList = new ArrayList();
            for(int i = 0; i < CommoditiesArrayList.Count; i++)
			{
				if(((Commodity)CommoditiesArrayList[i]).WarehouseLocation != "OUTSIDE PURCHASE")
				{
					Commodity commodity = FindOrAddCommodity(tempArrayList,((Commodity)CommoditiesArrayList[i]).CommodityFullName.Trim(),((Commodity)CommoditiesArrayList[i]).WarehouseLocation, ((Commodity)CommoditiesArrayList[i]).WarehouseIdx);
					commodity.CommodityName = ((Commodity)CommoditiesArrayList[i]).CommodityName;
					commodity.Estimated += ((Commodity)CommoditiesArrayList[i]).Estimated;
					commodity.OnHand += ((Commodity)CommoditiesArrayList[i]).OnHand;
					commodity.OrderedQnt += ((Commodity)CommoditiesArrayList[i]).OrderedQnt;
					commodity.ProductIdx = ((Commodity)CommoditiesArrayList[i]).ProductIdx;
					commodity.ShippedQnt += ((Commodity)CommoditiesArrayList[i]).ShippedQnt;
					commodity.TotalShipped += ((Commodity)CommoditiesArrayList[i]).TotalShipped;
					commodity.TotalToShip += ((Commodity)CommoditiesArrayList[i]).TotalToShip;
					commodity.WarehouseIdx = ((Commodity)CommoditiesArrayList[i]).WarehouseIdx;
					//commodity.CustomerName = ((Commodity)CommoditiesArrayList[i]).CustomerName;
				}
				else
				{
					Commodity commodity = FindOrAddCommodity(outsidePurchaseArrayList,((Commodity)CommoditiesArrayList[i]).CommodityFullName.Trim(),((Commodity)CommoditiesArrayList[i]).WarehouseLocation, ((Commodity)CommoditiesArrayList[i]).WarehouseIdx);
					commodity.CommodityName = ((Commodity)CommoditiesArrayList[i]).CommodityName;
					commodity.Estimated += ((Commodity)CommoditiesArrayList[i]).Estimated;
					commodity.OnHand += ((Commodity)CommoditiesArrayList[i]).OnHand;
					commodity.OrderedQnt += ((Commodity)CommoditiesArrayList[i]).OrderedQnt;
					commodity.ProductIdx = ((Commodity)CommoditiesArrayList[i]).ProductIdx;
					commodity.ShippedQnt += ((Commodity)CommoditiesArrayList[i]).ShippedQnt;
					commodity.TotalShipped += ((Commodity)CommoditiesArrayList[i]).TotalShipped;
					commodity.TotalToShip += ((Commodity)CommoditiesArrayList[i]).TotalToShip;
					commodity.WarehouseIdx = ((Commodity)CommoditiesArrayList[i]).WarehouseIdx;
					//commodity.CustomerName = ((Commodity)CommoditiesArrayList[i]).CustomerName;
				}
			}

			CommoditiesArrayList = tempArrayList;
			
			for(int i = 0; i < outsidePurchaseArrayList.Count; i++)
			{
				CommoditiesArrayList.Add((Commodity)outsidePurchaseArrayList[i]);
			}
		}

		private void SortCustomersCommoditiesArrayList()
		{
			ArrayList tempArrayList = new ArrayList();
			ArrayList outsidePurchaseArrayList = new ArrayList();

			for(int i = 0; i < CustomersCommoditiesArrayList.Count; i++)
			{
				if(((CustomerCommodity)CustomersCommoditiesArrayList[i]).WarehouseLocation != "OUTSIDE PURCHASE")
				{
					CustomerCommodity customerCommodity = FindOrAddCustomerCommodity(tempArrayList,((CustomerCommodity)CustomersCommoditiesArrayList[i]).CustomerName,((CustomerCommodity)CustomersCommoditiesArrayList[i]).CommodityFullName.Trim(),((CustomerCommodity)CustomersCommoditiesArrayList[i]).WarehouseLocation);
					customerCommodity.Qnt += ((CustomerCommodity)CustomersCommoditiesArrayList[i]).Qnt;
				}
				else
				{
					CustomerCommodity customerCommodity = FindOrAddCustomerCommodity(outsidePurchaseArrayList,((CustomerCommodity)CustomersCommoditiesArrayList[i]).CustomerName,((CustomerCommodity)CustomersCommoditiesArrayList[i]).CommodityFullName.Trim(),((CustomerCommodity)CustomersCommoditiesArrayList[i]).WarehouseLocation);
					customerCommodity.Qnt += ((CustomerCommodity)CustomersCommoditiesArrayList[i]).Qnt;
				}
			}

			CustomersCommoditiesArrayList = tempArrayList;
			
			for(int i = 0; i < outsidePurchaseArrayList.Count; i++)
			{
				CustomersCommoditiesArrayList.Add((CustomerCommodity)outsidePurchaseArrayList[i]);
			}
		}

		private Commodity FindOrAddCommodity(ArrayList list,string FullCommodityName, string WarehouseLocation, string WarehouseIdx)
		{
			Commodity tempCommodity = new Commodity(FullCommodityName,WarehouseLocation,WarehouseIdx);
			int tempCommodityIdx = list.BinarySearch(tempCommodity);

			if(tempCommodityIdx <0)
				list.Insert(Math.Abs(tempCommodityIdx)-1,tempCommodity);
			else
				tempCommodity = (Commodity)list[tempCommodityIdx];

			return tempCommodity;
		}

		private Customer FindOrAddCustomer(ArrayList list,string CustomerName)
		{
			Customer tempCustomer = new Customer(CustomerName);
			int tempCustomerIdx = list.BinarySearch(tempCustomer);

			if(tempCustomerIdx <0)
				list.Insert(Math.Abs(tempCustomerIdx)-1,tempCustomer);
			else
				tempCustomer = (Customer)list[tempCustomerIdx];

			return tempCustomer;
		}

		private CustomerCommodity FindOrAddCustomerCommodity(ArrayList list,string CustomerName,string CommodityFullName,string WarehouseLocation)
		{
			CustomerCommodity tempCustomerCommodity = new CustomerCommodity(CustomerName,CommodityFullName,WarehouseLocation);
			int tempCustomerCommodityIdx = list.BinarySearch(tempCustomerCommodity);

			if(tempCustomerCommodityIdx <0)
				list.Insert(Math.Abs(tempCustomerCommodityIdx)-1,tempCustomerCommodity);
			else
				tempCustomerCommodity = (CustomerCommodity)list[tempCustomerCommodityIdx];

			return tempCustomerCommodity;
		}

		private string GetProductName(string productIndex)
		{
			string returnProductName = "";

			OracleConnection OracleConn = GetOracleConn();
			string oracleSQL = FormatOracle(@"
				Select	Ic_Product_Produce.ProductIdx,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))) As FullName

				From	Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Product_Produce

				Where	Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Product_Produce.ProductIdx = :ProductIndex

				Group By
						Ic_Product_Produce.ProductIdx,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name))))))))))

				Order By Fullname");

			OracleCommand OracleCmd = new OracleCommand(oracleSQL, OracleConn);
			OracleCmd.Parameters.Add(":ProductIndex", productIndex);

			OracleCmd.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader2 = OracleCmd.ExecuteReader();

			while (myReader2.Read())
			{
				returnProductName = myReader2.GetValue(1).ToString();
			}

			myReader2.Close();
			myReader2.Dispose();
			OracleCmd.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();

			return returnProductName;
		}

		private string GetWarehouseLocation(string warehouseIndex)
		{
			string returnWarehouseLocation = "";

			OracleConnection OracleConn = GetOracleConn();
			string oracleSQL = FormatOracle(@"
				Select
					Ic_Warehouse.WarehouseIdx,
					Fc_Name_Location.Descr
				From
					Ic_Warehouse,
					Fc_Name_Location
				Where
					Ic_Warehouse.NameIdx = Fc_Name_Location.NameIdx and
					Ic_Warehouse.NameLocationSeq = Fc_Name_Location.NameLocationSeq and
					Ic_Warehouse.WarehouseIdx = :WarehouseIndex
				Order by Descr");

			OracleCommand OracleCmd = new OracleCommand(oracleSQL, OracleConn);
			OracleCmd.Parameters.Add(":WarehouseIndex", warehouseIndex);

			OracleCmd.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader2 = OracleCmd.ExecuteReader();

			while (myReader2.Read())
			{
				returnWarehouseLocation = myReader2.GetValue(1).ToString();
			}

			myReader2.Close();
			myReader2.Dispose();
			OracleCmd.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();

			return returnWarehouseLocation;
		}

		private string GetCommodityName(string commodityIndex)
		{
			string returnCommodityName = "";

			OracleConnection OracleConn = GetOracleConn();
			string oracleSQL = FormatOracle(@"
				Select
					Name,
					Cmtyidx
				From
					Ic_Ps_Commodity
				Where
					Ic_Ps_Commodity.Cmtyidx = :CommodityIndex
				Order by Name");

			OracleCommand OracleCmd = new OracleCommand(oracleSQL, OracleConn);
			OracleCmd.Parameters.Add(":CommodityIndex", commodityIndex);

			OracleCmd.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader2 = OracleCmd.ExecuteReader();

			while (myReader2.Read())
			{
				returnCommodityName = myReader2.GetValue(1).ToString();
			}

			myReader2.Close();
			myReader2.Dispose();
			OracleCmd.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();

			return returnCommodityName;
		}
		#endregion

		#region CBO
		
		public class Commodity: IComparable
		{
			#region Declares
			public double OrderedQnt;
			public double ShippedQnt;
			public double OnHand;
			public double Estimated;
			public string ProductIdx;
			public string CommodityName;
			public string CommodityFullName;
			public double TotalShipped;
			public double TotalToShip;
			public string WarehouseIdx;
			public string WarehouseLocation;
			//public string CustomerName;
			//public double TotalOnHand;
			//public double TotalProduction;
			#endregion

			#region Constructor
			//public Inventory(string product,string shipped,string ordered)
			public Commodity(string commodityFullName, string warehouseLocation, string warehouseIdx)
			{
				#region old
				/*
				Product = product;
				Shipped = shipped;
				Ordered = ordered;
				*/
				/*Product = product;
				OrderedQnt = orderedQnt;
				ShippedQnt = shippedQnt;
				//Status = status;
				OnHand = onHand;
				Estimated = estimated;
				ProductIdx = productIdx;
				OneDayAhead = oneDayAhead;
				TwoDaysAhead = twoDaysAhead;
				ThreePlusDaysAhead = threePlusDaysAhead;
				Commodity = commodity;
				Customer = customer;
				IsTotal = isTotal;*/
				#endregion
				CommodityFullName = commodityFullName;
				WarehouseLocation = warehouseLocation;
                OrderedQnt = 0;
                ShippedQnt = 0;
                OnHand = 0;
                Estimated = 0;
                ProductIdx = "-1";
                CommodityName = "";
                TotalShipped = 0;
                TotalToShip = 0;
                WarehouseIdx = warehouseIdx;

        }

			#endregion

			#region IComparable Members

			public int CompareTo(object obj)
			{
				int tempCompare;
				Commodity Y = (Commodity) obj;

				//Compare WarehouseLocation
				tempCompare = this.WarehouseLocation.CompareTo(Y.WarehouseLocation);
				if(tempCompare != 0)
					return tempCompare;

				//Compare CommodityFullName
				tempCompare = this.CommodityFullName.CompareTo(Y.CommodityFullName);
				if(tempCompare != 0)
					return tempCompare;
//
//				//Compare Customer
//				tempCompare = this.CustomerName.CompareTo(Y.CustomerName);
//				if(tempCompare != 0)
//					return tempCompare;
				#region old
/*
				//Compare Desc
				tempCompare = this.Product.CompareTo(Y.Product);
				if(tempCompare != 0)
					return tempCompare;
				
				//Compare IsTotal
				tempCompare = this.IsTotal.CompareTo(Y.IsTotal);
				if(tempCompare != 0)
					return tempCompare*-1;//*-1 to change asc to dec

				//Compare ShippedQnt
				tempCompare = this.ShippedQnt.CompareTo(Y.ShippedQnt);
				if(tempCompare != 0)
					return tempCompare*-1;

				//Compare OrderedQnt
				tempCompare = this.OrderedQnt.CompareTo(Y.OrderedQnt);
				if(tempCompare != 0)
					return tempCompare*-1;

				//Compare OneDayAhead
				tempCompare = this.OneDayAhead.CompareTo(Y.OneDayAhead);
				if(tempCompare != 0)
					return tempCompare*-1;

				//Compare TwoDaysAhead
				tempCompare = this.TwoDaysAhead.CompareTo(Y.TwoDaysAhead);
				if(tempCompare != 0)
					return tempCompare*-1;

				//Compare ThreePlusDaysAhead
				tempCompare = this.ThreePlusDaysAhead.CompareTo(Y.ThreePlusDaysAhead);
				if(tempCompare != 0)
					return tempCompare*-1;
*/
				#endregion
				return 0;
			}

			#endregion
		}

		public class Customer: IComparable
		{
			#region Declares
			public string CustomerName;
			public string Status;
			public string TruckName;
			#endregion

			#region Constructor
			public Customer(string customerName)
			{
				CustomerName = customerName;
			}
		
			#endregion

			#region IComparable Members

			public int CompareTo(object obj)
			{
				int tempCompare;
				Customer Y = (Customer) obj;

				//Compare Customer
				tempCompare = this.CustomerName.CompareTo(Y.CustomerName);
				if(tempCompare != 0)
					return tempCompare;

				return 0;
			}

			#endregion
		}

		public class CustomerCommodity: IComparable
		{
			#region Declares
			public string CustomerName;
			public string CommodityFullName;
			public string WarehouseLocation;
			public double Qnt;
			#endregion

			#region Constructor
			public CustomerCommodity(string customerName, string commodityFullName, string warehouseLocation)
			{
				CustomerName = customerName;
				CommodityFullName = commodityFullName;
				WarehouseLocation = warehouseLocation;
			}
		
			#endregion

			#region IComparable Members

			public int CompareTo(object obj)
			{
				int tempCompare;
				CustomerCommodity Y = (CustomerCommodity) obj;

				//Compare WarehouseLocation
				tempCompare = this.WarehouseLocation.CompareTo(Y.WarehouseLocation);
				if(tempCompare != 0)
					return tempCompare;

				//Compare CommodityFullName
				tempCompare = this.CommodityFullName.CompareTo(Y.CommodityFullName);
				if(tempCompare != 0)
					return tempCompare;

				//Compare CustomerName
				tempCompare = this.CustomerName.CompareTo(Y.CustomerName);
				if(tempCompare != 0)
					return tempCompare;

				return 0;
			}

			#endregion
		}
		#endregion
	}
}
