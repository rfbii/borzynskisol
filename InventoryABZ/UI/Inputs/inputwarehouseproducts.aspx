<%@ Page language="c#" Inherits="Inventory.UI.Inputs.InputWarehouseProducts" Codebehind="InputWarehouseProducts.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Input Warehouse Commodities</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C#" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
	<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body bgcolor="white">
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
				<table width="600">
					<tr>
						<td height="70" align="center" valign="top" colspan="3">Input Warehouse-Products</td>
					</tr>
					<tr>
						<td align="left" colspan="3"><font face="Tahoma" size="2">Warehouse</font></td>
					</tr>
					<tr>
						<td width="232" align="left"><asp:listbox id="WarehouseList" runat="server" Width="300px" Height="136px" Font-Names="Tahoma" AutoPostBack="True" Enabled="False" onselectedindexchanged="WarehouseList_SelectedIndexChanged"></asp:listbox></td>
						<td width="192" valign="top" align="left"><asp:label id="warehouseName" runat="server" Width="192px" Font-Names="Tahoma" Font-Size="Small"></asp:label></td>
						<td width="100" align="left" valign="top"><asp:textbox id="warehouseKey" runat="server" Width="100px" Height="19" Visible="False"></asp:textbox></td>
					</tr>
					<tr>
						<td align="center"><font face="Tahoma" size="2">Products Available</font></td>
						<td>&nbsp;</td>
						<td align="center"><font face="Tahoma" size="2">Products Selected</font></td>
					</tr>
					<tr>
						<td><asp:listbox id="availableList" runat="server" Font-Names="Courier New" Height="120px" Width="300px"></asp:listbox></td>
						<td align="center">
							<asp:button id="add" runat="server" Font-Names="Tahoma" Height="24" Width="75px" Text=">" onclick="Add_Click"></asp:button><br/>
							<asp:button id="remove" runat="server" Height="24" Width="75" Text="<" onclick="Remove_Click"></asp:button><br/>
							<asp:button id="addAll" runat="server" Height="24" Width="75px" Text=">>" CausesValidation="False" onclick="addAll_Click"></asp:button><br/>
							<asp:button id="removeAll" runat="server" Height="24" Width="75" Text="<<" CausesValidation="False" onclick="removeAll_Click"></asp:button>
						</td>
						<td><asp:listbox id="selectedList" tabIndex="2" runat="server" Font-Names="Courier New" Height="120px" Width="300px"></asp:listbox></td>
					</tr>
					<tr>
						<td colspan="3" align="center"><asp:button id="Return" tabIndex="4" runat="server" Height="24px" Width="160px" Text="Return to Menu" onclick="Return_Click"></asp:button></td>
					</tr>
					<tr>
						<td colspan="3"><asp:label id="Message" runat="server" ForeColor="Red"></asp:label></td>
					</tr>
				</table>
			</div>
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
		</form>
	</body>
</html>