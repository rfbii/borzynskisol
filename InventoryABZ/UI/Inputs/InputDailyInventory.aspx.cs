using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using KeyCentral.Functions;
using Oracle.DataAccess.Client;
using System.Text;
using KeyCentral.Lookups;

namespace Inventory.UI.Inputs
{
	public partial class InputDailyInventory : BasePage
	{
		#region Delcares
		private double lineCount = 0;
		private bool newPage = true;
		private bool firstPage = true;
		private ArrayList CustomersArrayList;
		private ArrayList CommoditiesArrayList;
		private ArrayList CustomersCommoditiesArrayList;
		private double GrandTotalToShip = 0;
		private double GrandTotalShipped = 0;
		private double GrandTotalOnHand = 0;
		private double GrandTotalProduction = 0;
		ReportSessionManager reportSessionManager;
		#endregion
		
		#region Param Properties
		private string CommodityName{get{return (string)Session["CommodityName"];}}
		private string CommodityKey{get{return (string)Session["CommodityKey"];}}
		private ArrayList Commodities{get{return (ArrayList)Session["Commodities"];}}
		private string WarehouseName{get{return (string)Session["WarehouseName"];}}
		private string WarehouseKey{get{return (string)Session["WarehouseKey"];}}
		private ArrayList Warehouses{get{return (ArrayList)Session["Warehouses"];}}
		private string StartDate { get { return (string)Session["StartDate"]; } }
		private ArrayList ReportCommoditiesArrayList { get { return (ArrayList)Session["ReportCommoditiesArrayList"]; } }
		//private string WarehouseCode{get{return (string)Session["WarehouseCode"];}}
		#endregion

		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Inventory Input") == false)
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/MenuMain.aspx");

			reportSessionManager = new ReportSessionManager(this, Request.QueryString["ReportName"]);

			if(!IsPostBack)
			{
				KeyCentral.Functions.JavaStatusMessage status = new KeyCentral.Functions.JavaStatusMessage(this);
				status.ShowMessage("Computing");

				this.Repeater1.DataSource = GetData(CommodityKey, StartDate);

				if(((ArrayList)this.Repeater1.DataSource).Count != 0)
					Repeater1.DataBind();
				else
					JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"])+ "&&Records=None");
			}
		}

		private void save_Click(object sender, EventArgs e)
		{
			KeyCentral.Functions.JavaStatusMessage status = new KeyCentral.Functions.JavaStatusMessage(this);
			status.ShowMessage("Saving");

			SaveAmounts();

			Response.Write("<script>alert('Save complete.');</script>");

			//Status Message
			status.ShowMessage("Computing");

			this.Repeater1.DataSource = GetData(CommodityKey, StartDate);

			if(((ArrayList)this.Repeater1.DataSource).Count != 0)
				Repeater1.DataBind();
			else
				JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"])+ "&&Records=None");
		}

		protected void exit_Click(object sender, System.EventArgs e)
		{
            JavaScriptRedirect("../../UI/Inputs/CriteriaInputDailyInventory.aspx?ReportName=CriteriaInputDailyInventory");
		}

		private void saveExit_Click(object sender, EventArgs e)
		{
			KeyCentral.Functions.JavaStatusMessage status = new KeyCentral.Functions.JavaStatusMessage(this);
			status.ShowMessage("Saving");

			SaveAmounts();

			Response.Write("<script>alert('Save complete.');</script>");

            JavaScriptRedirect("../../UI/Inputs/CriteriaInputDailyInventory.aspx?ReportName=CriteriaInputDailyInventory");
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.save.Click += new EventHandler(this.save_Click);
			this.saveExit.Click += new EventHandler(saveExit_Click);
		}
		#endregion

		#region Display Code
		public string Columns() /*ColumnHeader()*/
		{
			/* div's layout
			 *
			 * | div			|	scrollHeader |
			 * | scrollLeft		|	scrollRight  |
			 * 
			*/

			//int tableWidth = 600 + (CustomersArrayList.Count * 130);
			int tableWidthLeft = 510;
			int tableWidthRight = (CustomersArrayList.Count * 130);

			string columnHeader="";
			if(newPage)
			{
				if(firstPage == false)
				{
					columnHeader = @"
						<!-- email end -->
						</table>
						<table class='ReportTable'>
							<tr>
								<td height='20'><font size='2' face='Arial'>&nbsp;</font></td>
							</tr>
						</table><div style='page-break-before:always'><span style='visibility: hidden;'><font style='font-size:0pt;'>&nbsp;</font></span></div>
						<table border='0' cellpadding='0' cellspacing='0' width=" + tableWidthLeft + @" class='ReportTable'>";
				}
				else
				{
					columnHeader += @"
					<td>
						<div style='PADDING-LEFT: 15px;'>
						<table border='0' cellpadding='0' cellspacing='0' width=" + tableWidthLeft + @" class='ReportTable'>";
				}
				columnHeader = columnHeader + @"
					<tr>
						<td class='BottomLeftRightBlack' align=center width=55 height=35><b>ON HAND</b></td>
						<td class='BottomRightBlack' align=center width=55 height=35><b>PROD.</b></td>
						<td class='BottomRightBlack' align=center width=55 height=35><b>SHIP</b></td>
						<td class='BottomRightBlack' align=center width=55 height=35><b>SUB<br>TOTAL</b></td>
						<td class='BottomRightBlack' align=center width=55 height=35><b>TO SHIP</b></td>
						<td class='BottomRightBlack' align=center width=55 height=35><b>PROJ.</b></td>
						<td class='BottomRightBlack' align=center width=180 height=35><b>" + Convert.ToDateTime(StartDate).DayOfWeek.ToString() + " " + Convert.ToDateTime(StartDate).Month.ToString() + "/" + Convert.ToDateTime(StartDate).Day.ToString() + @"</b></td>
					</tr>
					<tr>
						<td class='BottomLeftRightBlack' align=right width=55 height=21>";

				columnHeader += "<INPUT id='GrandTotalOnHandTextBox' name='GrandTotalOnHandTextBox' style='TEXT-ALIGN: right; FONT-SIZE: 10pt; FONT-FAMILY: Arial; WIDTH: 55px; HEIGHT: 20px;' type='text' EnterAsTab readOnly value='" + Misc.DisplayFormat(GrandTotalOnHand) + "'>";
				columnHeader += @"</td>

						<td class='BottomRightBlack' align=right width=55 height=21>";
				
				columnHeader += "<INPUT id='GrandTotalProductionTextBox' name='GrandTotalProductionTextBox' style='TEXT-ALIGN: right; FONT-SIZE: 10pt; FONT-FAMILY: Arial; WIDTH: 55px; HEIGHT: 20px;' type='text' EnterAsTab readOnly value='" + Misc.DisplayFormat(GrandTotalProduction) + "'>";
				columnHeader += @"</td>

						<td class='BottomRightBlack' align=right width=55 height=21>";

				columnHeader += "<INPUT id='GrandTotalShippedTextBox' name='GrandTotalShippedTextBox' style='TEXT-ALIGN: right; COLOR: #0000FF; FONT-SIZE: 10pt; FONT-FAMILY: Arial; WIDTH: 55px; HEIGHT: 20px;' type='text' EnterAsTab readOnly value='" + Misc.DisplayFormat(GrandTotalShipped) + "'>";
				columnHeader += @"</td>

						<td class='BottomRightBlack' align=right width=55 height=21>";

				columnHeader += "<INPUT id='GrandTotalSubtotalTextBox' name='GrandTotalSubtotalTextBox' style='TEXT-ALIGN: right; FONT-SIZE: 10pt; FONT-FAMILY: Arial; WIDTH: 55px; HEIGHT: 20px;' type='text' EnterAsTab readOnly value='" + Misc.DisplayFormat((GrandTotalOnHand + GrandTotalProduction) - GrandTotalShipped) + "'>";
				columnHeader += @"</td>

						<td class='BottomRightBlack' align=right width=55 height=21>";

				columnHeader += "<INPUT id='GrandTotalToShipTextBox' name='GrandTotalToShipTextBox' style='TEXT-ALIGN: right; FONT-SIZE: 10pt; FONT-FAMILY: Arial; WIDTH: 55px; HEIGHT: 20px;' type='text' EnterAsTab readOnly value='" + Misc.DisplayFormat(GrandTotalToShip) + "'>";
				columnHeader += @"</td>

						<td class='BottomRightBlack' align=right width=55 height=21>";

				columnHeader += "<INPUT id='GrandTotalProjectedTextBox' name='GrandTotalProjectedTextBox' style='TEXT-ALIGN: right; FONT-SIZE: 10pt; FONT-FAMILY: Arial; WIDTH: 55px; HEIGHT: 20px;' type='text' EnterAsTab readOnly value='" + Misc.DisplayFormat(((GrandTotalOnHand + GrandTotalProduction) - GrandTotalShipped) - GrandTotalToShip) + "'>";
				columnHeader += @"</td>

						<td class='BottomRightBlack' align=center width=180 height=21><b>STATUS</b></td>
					</tr>
					<tr>
						<td class='BottomLeftRightBlack' align=center width=55>&nbsp;</td>
						<td class='BottomRightBlack' align=center width=55>&nbsp;</td>
						<td class='BottomRightBlack' align=center width=55>&nbsp;</td>
						<td class='BottomRightBlack' align=center width=55>&nbsp;</td>
						<td class='BottomRightBlack' align=center width=55>&nbsp;</td>
						<td class='BottomRightBlack' align=center width=55>&nbsp;</td>
						<td class='BottomRightBlack' align=center width=180><b>TRUCK NAME</b></td>
					</tr>
				</table>
				</div>
			</td>
			<td>
				<div class='scrollHeader' id='HeaderDiv' scrollwithbody='true'>
				<table border='0' cellpadding='0' cellspacing='0' width=" + tableWidthRight + @" class='ReportTable'>
					<tr>";

				for(int i = 0; i < CustomersArrayList.Count; i++)
				{
					if(i == 0)
					{
						if(((Customer)CustomersArrayList[i]).CustomerName != "")
						{
							if(((Customer)CustomersArrayList[i]).CustomerName.Substring(((Customer)CustomersArrayList[i]).CustomerName.IndexOf(" ") + 1).Length > 15)
							{
								columnHeader += @"
									<td class='BottomLeftRightBlack' align=center width=130 height=35><b>" + ((Customer)CustomersArrayList[i]).CustomerName.Substring(0,((Customer)CustomersArrayList[i]).CustomerName.IndexOf(" ")) + "<br>" + ((Customer)CustomersArrayList[i]).CustomerName.Substring(((Customer)CustomersArrayList[i]).CustomerName.IndexOf(" ") + 1,15) + @"</b></td>";
							}
							else
							{
								columnHeader += @"
									<td class='BottomLeftRightBlack' align=center width=130 height=35><b>" + ((Customer)CustomersArrayList[i]).CustomerName.Substring(0,((Customer)CustomersArrayList[i]).CustomerName.IndexOf(" ")) + "<br>" + ((Customer)CustomersArrayList[i]).CustomerName.Substring(((Customer)CustomersArrayList[i]).CustomerName.IndexOf(" ") + 1) + @"</b></td>";
							}
						}
						else
						{
							columnHeader += @"
								<td class='BottomLeftRightBlack' align=center width=130 height=35>&nbsp;</td>";
						}
					}
					else
					{
						if(((Customer)CustomersArrayList[i]).CustomerName != "")
						{
							if(((Customer)CustomersArrayList[i]).CustomerName.Substring(((Customer)CustomersArrayList[i]).CustomerName.IndexOf(" ") + 1).Length > 15)
							{
								columnHeader += @"
									<td class='BottomRightBlack' align=center width=130 height=35><b>" + ((Customer)CustomersArrayList[i]).CustomerName.Substring(0,((Customer)CustomersArrayList[i]).CustomerName.IndexOf(" ")) + "<br>" + ((Customer)CustomersArrayList[i]).CustomerName.Substring(((Customer)CustomersArrayList[i]).CustomerName.IndexOf(" ") + 1,15) + @"</b></td>";
							}
							else
							{
								columnHeader += @"
									<td class='BottomRightBlack' align=center width=130 height=35><b>" + ((Customer)CustomersArrayList[i]).CustomerName.Substring(0,((Customer)CustomersArrayList[i]).CustomerName.IndexOf(" ")) + "<br>" + ((Customer)CustomersArrayList[i]).CustomerName.Substring(((Customer)CustomersArrayList[i]).CustomerName.IndexOf(" ") + 1) + @"</b></td>";
							}
						}
						else
						{
							columnHeader += @"
								<td class='BottomRightBlack' align=center width=130 height=35>&nbsp;</td>";
						}
					}
				}

				columnHeader += @"
					</tr>
					<tr>";

				for(int i = 0; i < CustomersArrayList.Count; i++)
				{
					if(i == 0)
					{
						if(((Customer)CustomersArrayList[i]).Status != "")
						{
							columnHeader += @"
							<td class='BottomLeftRightBlack' align=center width=130 height=21><b>" + ((Customer)CustomersArrayList[i]).Status + @"</b></td>";
						}
						else
						{
							columnHeader += @"
							<td class='BottomLeftRightBlack' align=center width=130 height=21>&nbsp;</td>";
						}
					}
					else
					{
						if(((Customer)CustomersArrayList[i]).Status != "")
						{
							columnHeader += @"
								<td class='BottomRightBlack' align=center width=130 height=21><b>" + ((Customer)CustomersArrayList[i]).Status + @"</b></td>";
						}
						else
						{
							columnHeader += @"
								<td class='BottomRightBlack' align=center width=130 height=21>&nbsp;</td>";
						}
					}
				}

				columnHeader += @"
					</tr>
					<tr>";

				for(int i = 0; i < CustomersArrayList.Count; i++)
				{
					if (i == 0)
					{
						if(((Customer)CustomersArrayList[i]).TruckName != "")
						{
							if(((Customer)CustomersArrayList[i]).TruckName.Length > 15)
							{
								columnHeader += @"
									<td class='BottomLeftRightBlack' align=center width=130><b>" + ((Customer)CustomersArrayList[i]).TruckName.Substring(0,15) + @"</b></td>";
							}
							else
							{
								columnHeader += @"
									<td class='BottomLeftRightBlack' align=center width=130><b>" + ((Customer)CustomersArrayList[i]).TruckName + @"</b></td>";
							}
						}
						else
						{
							columnHeader += @"
								<td class='BottomLeftRightBlack' align=center width=130>&nbsp;</td>";
						}
					}
					else
					{
						if(((Customer)CustomersArrayList[i]).TruckName != "")
						{
							if(((Customer)CustomersArrayList[i]).TruckName.Length > 15)
							{
								columnHeader += @"
									<td class='BottomRightBlack' align=center width=130><b>" + ((Customer)CustomersArrayList[i]).TruckName.Substring(0,15) + @"</b></td>";
							}
							else
							{
								columnHeader += @"
									<td class='BottomRightBlack' align=center width=130><b>" + ((Customer)CustomersArrayList[i]).TruckName + @"</b></td>";
							}
						}
						else
						{
							columnHeader += @"
								<td class='BottomRightBlack' align=center width=130>&nbsp;</td>";
						}
					}
				}
				columnHeader += @"
					</tr>
				</table>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class='scrollLeft' id='LeftDiv' scrollwithbody='true' dir='rtl' style='PADDING-LEFT: 15px;'>
				<div dir='ltr'>
				<table border='0' cellpadding='0' cellspacing='0' width=" + tableWidthLeft + " class='ReportTable'>";

				for (int i = 0; i < ReportCommoditiesArrayList.Count; i++)
				{
					columnHeader += @"
					<tr>
						<td class='BottomLeftRightBlack' align=right width=55 height=30>";

					/*columnHeader += "<INPUT id='ProductIdxTextBox' name ='ProductIdxTextBox" + Convert.ToString(i) + "' style='DISPLAY: none; VISIBILITY: hidden; TEXT-ALIGN: center; FONT-SIZE: 10pt; FONT-FAMILY: Arial; WIDTH: 70px; HEIGHT: 20px;' EnterAsTab="" type='text' value='";
					columnHeader += GetProductIdx((Commodity)CommoditiesArrayList[i]) + "'>";

					columnHeader += "<INPUT id='CommodityKeyTextBox' name ='CommodityKeyTextBox" + Convert.ToString(i) + "' style='DISPLAY: none; VISIBILITY: hidden; TEXT-ALIGN: center; FONT-SIZE: 10pt; FONT-FAMILY: Arial; WIDTH: 70px; HEIGHT: 20px;' EnterAsTab="" type='text' value='";
					columnHeader += GetCommodityKey((Commodity)CommoditiesArrayList[i]) + "'>";

					columnHeader += "<INPUT id='StyleKeyTextBox' name ='StyleKeyTextBox" + Convert.ToString(i) + "' style='DISPLAY: none; VISIBILITY: hidden; TEXT-ALIGN: center; FONT-SIZE: 10pt; FONT-FAMILY: Arial; WIDTH: 70px; HEIGHT: 20px;' EnterAsTab="" type='text' value='";
					columnHeader += GetStyleKey((Commodity)CommoditiesArrayList[i]) + "'>";

					columnHeader += "<INPUT id='SizeKeyTextBox' name ='SizeKeyTextBox" + Convert.ToString(i) + "' style='DISPLAY: none; VISIBILITY: hidden; TEXT-ALIGN: center; FONT-SIZE: 10pt; FONT-FAMILY: Arial; WIDTH: 70px; HEIGHT: 20px;' EnterAsTab="" type='text' value='";
					columnHeader += GetSizeKey((Commodity)CommoditiesArrayList[i]) + "'>";

					columnHeader += "<INPUT id='VarietyKeyTextBox' name ='VarietyKeyTextBox" + Convert.ToString(i) + "' style='DISPLAY: none; VISIBILITY: hidden; TEXT-ALIGN: center; FONT-SIZE: 10pt; FONT-FAMILY: Arial; WIDTH: 70px; HEIGHT: 20px;' EnterAsTab="" type='text' value='";
					columnHeader += GetVarietyKey((Commodity)CommoditiesArrayList[i]) + "'>";

					columnHeader += "<INPUT id='GradeKeyTextBox' name ='GradeKeyTextBox" + Convert.ToString(i) + "' style='DISPLAY: none; VISIBILITY: hidden; TEXT-ALIGN: center; FONT-SIZE: 10pt; FONT-FAMILY: Arial; WIDTH: 70px; HEIGHT: 20px;' EnterAsTab="" type='text' value='";
					columnHeader += GetGradeKey((Commodity)CommoditiesArrayList[i]) + "'>";

					columnHeader += "<INPUT id='LabelKeyTextBox' name ='LabelKeyTextBox" + Convert.ToString(i) + "' style='DISPLAY: none; VISIBILITY: hidden; TEXT-ALIGN: center; FONT-SIZE: 10pt; FONT-FAMILY: Arial; WIDTH: 70px; HEIGHT: 20px;' EnterAsTab="" type='text' value='";
					columnHeader += GetLabelKey((Commodity)CommoditiesArrayList[i]) + "'>";

					columnHeader += "<INPUT id='WarehouseKeyTextBox' name ='WarehouseKeyTextBox" + Convert.ToString(i) + "' style='DISPLAY: none; VISIBILITY: hidden; TEXT-ALIGN: center; FONT-SIZE: 10pt; FONT-FAMILY: Arial; WIDTH: 70px; HEIGHT: 20px;' EnterAsTab="" type='text' value='";
					columnHeader += GetWarehouseKey((Commodity)CommoditiesArrayList[i]) + "'>";*/

					columnHeader += "<INPUT id='OnHandTextBox' name='OnHandTextBox" + Convert.ToString(i) + "' onblur='RenewSession();TotalOnHand();UpdateSubtotal();UpdateProjected();' onfocus='this.select();' style='TEXT-ALIGN: right; FONT-SIZE: 10pt; FONT-FAMILY: Arial; WIDTH: 55px; HEIGHT: 20px;' onkeydown='return checkArrowsOnHand(this.name, event);' type='text' value='";
					columnHeader += GetOnHand((Commodity)ReportCommoditiesArrayList[i]).Replace("&nbsp;", "0") + @"'></td>

						<td class='BottomRightBlack' align=right width=55 height=30>";

					columnHeader += "<INPUT id='ProductionTextBox' name='ProductionTextBox" + Convert.ToString(i) + "' onblur='RenewSession();TotalProduction();UpdateSubtotal();UpdateProjected();' onfocus='this.select();' style='TEXT-ALIGN: right; FONT-SIZE: 10pt; FONT-FAMILY: Arial; WIDTH: 55px; HEIGHT: 20px;' onkeydown='return checkArrowsProduction(this.name, event);' type='text' value='";
					columnHeader += GetProduction((Commodity)ReportCommoditiesArrayList[i]).Replace("&nbsp;", "0") + @"'></td>

						<td class='BottomRightBlack' align=right width=55 height=30>";

					columnHeader += "<INPUT id='ShippedTextBox' onfocus='this.select();' style='TEXT-ALIGN: right; COLOR: #0000FF; FONT-SIZE: 10pt; FONT-FAMILY: Arial; WIDTH: 55px; HEIGHT: 20px;' type='text' EnterAsTab readOnly value='";
					columnHeader += GetTotalShipped((Commodity)ReportCommoditiesArrayList[i]).Replace("&nbsp;", "0") + @"'></td>

						<td class='BottomRightBlack' align=right width=55 height=30>";

					columnHeader += "<INPUT id='SubtotalTextBox' onfocus='this.select();' style='TEXT-ALIGN: right; FONT-SIZE: 10pt; FONT-FAMILY: Arial; WIDTH: 55px; HEIGHT: 20px;' type='text' EnterAsTab readOnly value='";
					columnHeader += GetSubtotal((Commodity)ReportCommoditiesArrayList[i]).Replace("&nbsp;", "0") + @"'></td>

						<td class='BottomRightBlack' align=right width=55 height=30>";

					columnHeader += "<INPUT id='ToShipTextBox' onfocus='this.select();' style='TEXT-ALIGN: right; FONT-SIZE: 10pt; FONT-FAMILY: Arial; WIDTH: 55px; HEIGHT: 20px;' type='text' EnterAsTab readOnly value='";
					columnHeader += GetTotalToShip((Commodity)ReportCommoditiesArrayList[i]).Replace("&nbsp;", "0") + @"'></td>

						<td class='BottomRightBlack' align=right width=55 height=30>";

					columnHeader += "<INPUT id='ProjectedTextBox' onfocus='this.select();' style='TEXT-ALIGN: right; FONT-SIZE: 10pt; FONT-FAMILY: Arial; WIDTH: 55px; HEIGHT: 20px;' type='text' EnterAsTab readOnly value='";
					columnHeader += GetProjected((Commodity)ReportCommoditiesArrayList[i]).Replace("&nbsp;", "0") + @"'></td>

						<td class='BottomRightBlack' align=left width=180 height=30><b>" + GetCommodityFullName((Commodity)ReportCommoditiesArrayList[i]) + @"</b></td>
					</tr>";
				}

				columnHeader += @"
				</table>
				</div>
				</div>
			</td>
			<td>
				<div class='scrollRight' id='BodyDiv'>
				<table border='0' cellpadding='0' cellspacing='0' width=" + tableWidthRight + @" class='ReportTable'>
					<tr>";

				for (int i = 0; i < ReportCommoditiesArrayList.Count; i++)
				{
					columnHeader += @"
						<tr>";

					for(int j = 0; j < CustomersArrayList.Count; j++)
					{
						if(j == 0)
						{
							if(((Customer)CustomersArrayList[j]).Status == "Shipped")
							{
								columnHeader += @"
									<td class='BottomLeftRightBlack' align=right width=130 height=30 style='PADDING-RIGHT: 1px'><font color='#0000FF' style='FONT-SIZE: 11pt;'>" + GetCustomerCommodityQnt((Commodity)ReportCommoditiesArrayList[i], ((Customer)CustomersArrayList[j]).CustomerName) + @"</font></td>";
							}
							else
							{
								columnHeader += @"
									<td class='BottomLeftRightBlack' align=right width=130 height=30 style='PADDING-RIGHT: 1px'><font style='FONT-SIZE: 11pt;'>" + GetCustomerCommodityQnt((Commodity)ReportCommoditiesArrayList[i], ((Customer)CustomersArrayList[j]).CustomerName) + @"</font></td>";
							}
						}
						else
						{
							if(((Customer)CustomersArrayList[j]).Status == "Shipped")
							{
								columnHeader += @"
									<td class='BottomRightBlack' align=right width=130 height=30 style='PADDING-RIGHT: 1px'><font color='#0000FF' style='FONT-SIZE: 11pt;'>" + GetCustomerCommodityQnt((Commodity)ReportCommoditiesArrayList[i], ((Customer)CustomersArrayList[j]).CustomerName) + @"</font></td>";
							}
							else
							{
								columnHeader += @"
									<td class='BottomRightBlack' align=right width=130 height=30 style='PADDING-RIGHT: 1px'><font style='FONT-SIZE: 11pt;'>" + GetCustomerCommodityQnt((Commodity)ReportCommoditiesArrayList[i], ((Customer)CustomersArrayList[j]).CustomerName) + @"</font></td>";
							}						
						}
					}

					columnHeader += @"
						</tr>";
				}

				columnHeader += @"
				</table>
				</div>
			</td>";

				if (firstPage == false)
				{
					columnHeader = columnHeader + @"<!-- email start -->";
				}

				newPage = false;
			}

			return columnHeader;
		}

		public string GetCommodity()
		{
			if(CommodityName == "")
				return "";

			return "Commodity: " + CommodityName + "; ";
		}

		public string GetWarehouse()
		{
			if(WarehouseName == "")
				return "";

			return "Warehouse: " + WarehouseName + "; ";
		}
		public string GetTime()
		{
			return "Time: " + System.DateTime.Now;
		}
		public string GetStartDate()
		{
			return "Ship Date: " + StartDate + "; ";
		}

		public string GetCommodityFullName(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			return commodity.CommodityFullName.Trim();
		}

		public string GetProductIdx(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			return commodity.ProductIdx;
		}

		public string GetCommodityKey(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			return commodity.CommodityKey;
		}

		public string GetStyleKey(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			return commodity.StyleKey;
		}

		public string GetSizeKey(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			return commodity.SizeKey;
		}

		public string GetVarietyKey(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			return commodity.VarietyKey;
		}

		public string GetGradeKey(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			return commodity.GradeKey;
		}

		public string GetLabelKey(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			return commodity.LabelKey;
		}

		public string GetWarehouseKey(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			return commodity.WarehouseKey;
		}

		public string GetProjected(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			if (((commodity.OnHand + commodity.Estimated) - (commodity.TotalToShip + commodity.TotalShipped)) == 0)
				return "&nbsp;";
			else
				return Misc.DisplayFormat((commodity.OnHand + commodity.Estimated) - (commodity.TotalToShip + commodity.TotalShipped));
		}

		public string GetOnHand(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			if (commodity.OnHand == 0)
				return "&nbsp;";
			else
				return Misc.DisplayFormat(commodity.OnHand);
		}

		public string GetProduction(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			if (commodity.Estimated == 0)
				return "&nbsp;";
			else
				return Misc.DisplayFormat(commodity.Estimated);
		}

		public string GetTotalToShip(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			if (commodity.TotalToShip == 0)
				return "&nbsp;";
			else
				return Misc.DisplayFormat(commodity.TotalToShip);
		}

		public string GetTotalShipped(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			if (commodity.TotalShipped == 0)
				return "&nbsp;";
			else
				return Misc.DisplayFormat(commodity.TotalShipped);
		}

		public string GetSubtotal(/*object oItem*/ Commodity commodity)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			if (((commodity.OnHand + commodity.Estimated) - commodity.TotalShipped) == 0)
				return "&nbsp;";
			else
				return Misc.DisplayFormat((commodity.OnHand + commodity.Estimated) - commodity.TotalShipped);
		}

		public string GetCustomerCommodityQnt(/*object oItem*/ Commodity commodity,string CustomerName)
		{
			//Commodity commodity = (Commodity)((RepeaterItem)oItem).DataItem;

			CustomerCommodity tempCustomerCommodity = new CustomerCommodity(CustomerName,commodity.CommodityFullName);
			int tempCustomerCommodityIdx = CustomersCommoditiesArrayList.BinarySearch(tempCustomerCommodity);

			if(tempCustomerCommodityIdx <0)
				return "&nbsp;";
			else
				tempCustomerCommodity = (CustomerCommodity)CustomersCommoditiesArrayList[tempCustomerCommodityIdx];

			return Misc.DisplayFormat0dec(tempCustomerCommodity.Qnt);
		}
		#endregion

		#region Private HelperFunctions
		public ArrayList GetData(string CommodityKey, string StartDate)
		{
			CustomersArrayList = new ArrayList();
			CommoditiesArrayList = new ArrayList();
			CustomersCommoditiesArrayList = new ArrayList();

			#region start arraylist
			SqlConnection Conn1 = GetSQLConn();
			string sqlCommand1 = @"
			use inventory;
			Select
				Product_Key,
				Warehouse_Key,
				Commodity_Key,
				Style_Key,
				Size_Key,
				Variety_Key,
				Grade_Key,
				Label_Key
			From
				[Warehouse_Products]
			Where
				1=1

				#WhereClause:Warehouse_Key#
				#WhereClause:Commodity_Key#";

			WhereClauseBuilder whereClause1 = new WhereClauseBuilder();
			whereClause1.AddCriteriaBox(WhereClauseUnit.UnitType.Int32,"Warehouse_Key",Warehouses);
			whereClause1.AddCriteriaBox(WhereClauseUnit.UnitType.Int32,"Commodity_Key",Commodities);

			SqlDataAdapter selectCMD1 = new SqlDataAdapter(whereClause1.BuildWhereClause(sqlCommand1), Conn1);

			SqlDataReader myReader3 = selectCMD1.SelectCommand.ExecuteReader();

			while(myReader3.Read())
			{
				Commodity commodity = FindOrAddCommodity(CommoditiesArrayList,GetProductName(myReader3.GetValue(0).ToString()));
				
				commodity.ProductIdx = myReader3.GetValue(0).ToString();

				if(!myReader3.IsDBNull(1))
					commodity.WarehouseKey = myReader3.GetValue(1).ToString();
				else
					commodity.WarehouseKey = "-1";

				if(!myReader3.IsDBNull(2))
					commodity.CommodityKey = myReader3.GetValue(2).ToString();
				else
					commodity.CommodityKey = "-1";

				if(!myReader3.IsDBNull(3))
					commodity.StyleKey = myReader3.GetValue(3).ToString();
				else
					commodity.StyleKey = "-1";

				if(!myReader3.IsDBNull(4))
					commodity.SizeKey = myReader3.GetValue(4).ToString();
				else
					commodity.SizeKey = "-1";

				if(!myReader3.IsDBNull(5))
					commodity.VarietyKey = myReader3.GetValue(5).ToString();
				else
					commodity.VarietyKey = "-1";

				if(!myReader3.IsDBNull(6))
					commodity.GradeKey = myReader3.GetValue(6).ToString();
				else
					commodity.GradeKey = "-1";

				if(!myReader3.IsDBNull(7))
					commodity.LabelKey = myReader3.GetValue(7).ToString();
				else
					commodity.LabelKey = "-1";
			}

			myReader3.Close();

			Conn1.Close();
			Conn1.Dispose();
			#endregion

			#region sql code for On Hand and Production
			SqlConnection Conn = GetSQLConn();
			string sqlCommand = @"
			use inventory;
			Select
				Product_Key,
				On_Hand,
				Production,
				Warehouse_Key,
				Commodity_Key,
				Style_Key,
				Size_Key,
				Variety_Key,
				Grade_Key,
				Label_Key
			From
				[Warehouse_Product_Inventory]
			Where
				1=1

				#WhereClause:Warehouse_Key#
				#WhereClause:Commodity_Key#";

			WhereClauseBuilder whereClause2 = new WhereClauseBuilder();
			whereClause2.AddCriteriaBox(WhereClauseUnit.UnitType.Int32,"Warehouse_Key",Warehouses);
			whereClause2.AddCriteriaBox(WhereClauseUnit.UnitType.Int32,"Commodity_Key",Commodities);

			SqlDataAdapter selectCMD = new SqlDataAdapter(whereClause2.BuildWhereClause(sqlCommand), Conn);

			SqlDataReader myReader2 = selectCMD.SelectCommand.ExecuteReader();

			while(myReader2.Read())
			{
				Commodity commodity = FindOrAddCommodity(CommoditiesArrayList,GetProductName(myReader2.GetValue(0).ToString()));
				commodity.OnHand += Convert.ToDouble(myReader2.GetValue(1).ToString());
				commodity.Estimated += Convert.ToDouble(myReader2.GetValue(2).ToString());

				commodity.ProductIdx = myReader2.GetValue(0).ToString();

				if(!myReader2.IsDBNull(3))
					commodity.WarehouseKey = myReader2.GetValue(3).ToString();
				else
					commodity.WarehouseKey = "-1";

				if(!myReader2.IsDBNull(4))
					commodity.CommodityKey = myReader2.GetValue(4).ToString();
				else
					commodity.CommodityKey = "-1";

				if(!myReader2.IsDBNull(5))
					commodity.StyleKey = myReader2.GetValue(5).ToString();
				else
					commodity.StyleKey = "-1";

				if(!myReader2.IsDBNull(6))
					commodity.SizeKey = myReader2.GetValue(6).ToString();
				else
					commodity.SizeKey = "-1";

				if(!myReader2.IsDBNull(7))
					commodity.VarietyKey = myReader2.GetValue(7).ToString();
				else
					commodity.VarietyKey = "-1";

				if(!myReader2.IsDBNull(8))
					commodity.GradeKey = myReader2.GetValue(8).ToString();
				else
					commodity.GradeKey = "-1";

				if(!myReader2.IsDBNull(9))
					commodity.LabelKey = myReader2.GetValue(9).ToString();
				else
					commodity.LabelKey = "-1";
			}

			myReader2.Close();

			Conn.Close();
			Conn.Dispose();
			#endregion

			WhereClauseBuilder whereClause = new WhereClauseBuilder();

			OracleConnection OracleConn = GetOracleConn();

			#region oracle code for Ordered and Shipped
			string oracleSQL = @"
				Select	Ic_Product_Id.Name,
						Sum(Ar_So_Line.IcQnt) AS OrderedQnt,
						0 AS ShippedQnt,
						Ar_So_Line.ProductIdx,
						Ic_Ps_Commodity.Name AS CommodityName,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer,
						Ar_Trx_Header.Carrier,
						Ic_Ps_Commodity.CmtyIdx,
						Ic_Ps_Style.StyleIdx,
						Ic_Ps_Size.SizeIdx,
						Ic_Ps_Variety.VarietyIdx,
						Ic_Ps_Grade.GradeIdx,
						Ic_Ps_Label.LabelIdx,
						Ic_Warehouse.WarehouseIdx

				From	Ar_So_Line,
						Ar_Trx_Header,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location

				Where	Ar_Trx_Header.SoType In ('2','3','5') AND
						Ar_Trx_Header.SoStatus In ('2','3') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + StartDate + @"','MM/DD/YYYY') AND
						Ar_Trx_Header.ArTrxHdrIdx = Ar_So_Line.ArTrxHdrIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Warehouse.WarehouseIdx = Ar_So_Line.WarehouseIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Ic_Product_Id.Name,
						Ar_So_Line.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)),
						Ar_Trx_Header.Carrier,
						Ic_Ps_Commodity.CmtyIdx,
						Ic_Ps_Style.StyleIdx,
						Ic_Ps_Size.SizeIdx,
						Ic_Ps_Variety.VarietyIdx,
						Ic_Ps_Grade.GradeIdx,
						Ic_Ps_Label.LabelIdx,
						Ic_Warehouse.WarehouseIdx

				UNION ALL

				Select	Ic_Product_Id.Name,
						0 AS OrderedQnt,
						Sum(Ar_So_Line.IcQnt) AS ShippedQnt,
						Ar_So_Line.ProductIdx,
						Ic_Ps_Commodity.Name AS CommodityName,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer,
						Ar_Trx_Header.Carrier,
						Ic_Ps_Commodity.CmtyIdx,
						Ic_Ps_Style.StyleIdx,
						Ic_Ps_Size.SizeIdx,
						Ic_Ps_Variety.VarietyIdx,
						Ic_Ps_Grade.GradeIdx,
						Ic_Ps_Label.LabelIdx,
						Ic_Warehouse.WarehouseIdx

				From	Ar_So_Line,
						Ar_Trx_Header,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location

				Where	Ar_Trx_Header.SoType In ('2','3','5') AND
						Ar_Trx_Header.SoStatus In ('4','5') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + StartDate + @"','MM/DD/YYYY') AND
						Ar_Trx_Header.ArTrxHdrIdx = Ar_So_Line.ArTrxHdrIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_So_Line.ProductIdx = Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Warehouse.WarehouseIdx = Ar_So_Line.WarehouseIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Ic_Product_Id.Name,
						Ar_So_Line.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)),
						Ar_Trx_Header.Carrier,
						Ic_Ps_Commodity.CmtyIdx,
						Ic_Ps_Style.StyleIdx,
						Ic_Ps_Size.SizeIdx,
						Ic_Ps_Variety.VarietyIdx,
						Ic_Ps_Grade.GradeIdx,
						Ic_Ps_Label.LabelIdx,
						Ic_Warehouse.WarehouseIdx

				UNION ALL

				Select  Ic_Product_Id.Name,
						0 AS OrderedQnt,
						Ar_Trx_Product.IcQnt AS ShippedQnt,
						Ar_Trx_Product.ProductIdx,
						Ic_Ps_Commodity.Name AS CommodityName,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)) as Customer,
						Ar_Trx_Header.Carrier,
						Ic_Ps_Commodity.CmtyIdx,
						Ic_Ps_Style.StyleIdx,
						Ic_Ps_Size.SizeIdx,
						Ic_Ps_Variety.VarietyIdx,
						Ic_Ps_Grade.GradeIdx,
						Ic_Ps_Label.LabelIdx,
						Ic_Warehouse.WarehouseIdx

				From	Ar_Trx_Header,
						Ar_Trx_Line,
						Ar_Trx_Detail,
						Ar_Trx_Product,
						Ar_Trx_Header_Ship,
						Ic_Product_Id,
						Ic_Product_Produce,
						Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Warehouse,
						Fc_Name_Location

				Where	Ar_Trx_Header.SoType In ('2','3','5') AND
						Ar_Trx_Header.SoStatus In ('6','7','C') AND
						Ar_Trx_Header.ShipDateTime = To_Date('" + StartDate + @"','MM/DD/YYYY') AND
						Ar_Trx_Line.ArTrxHdrIdx    = Ar_Trx_Header.ArTrxHdrIdx AND
						Ar_Trx_Detail.ArTrxHdrIdx    = Ar_Trx_Line.ArTrxHdrIdx AND
						Ar_Trx_Detail.ArTrxLineTrxType   = Ar_Trx_Line.ArTrxLineTrxType AND
						Ar_Trx_Detail.ArTrxLineSeq    = Ar_Trx_Line.ArTrxLineSeq AND
						Ar_Trx_Detail.TrxType         = '1' AND
						Ar_Trx_Line.DeleteFlag      = 'N' AND
						Ar_Trx_Product.ArTrxHdrIdx    = Ar_Trx_Detail.ArTrxHdrIdx AND
						Ar_Trx_Product.ArTrxDtlSeq    = Ar_Trx_Detail.ArTrxDtlSeq AND
						Ar_Trx_Product.ProductIdx = Ic_Product_Id.ProductIdx AND
						Ar_Trx_Product.ProductIdx = Ic_Product_Produce.ProductIdx AND
						Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Fc_Name_Location.NameIdx = Ar_Trx_Header.ToCustNameIdx AND
						Fc_Name_Location.NameLocationSeq = Ar_Trx_Header.ToCustLocSeq AND
						Ar_Trx_Header_Ship.ShipHdrIdx    (+)= Ar_Trx_Detail.ShipHdrIdx AND
						Ic_Warehouse.NameIdx             (+)= Ar_Trx_Header_Ship.FromNameIdx AND 
						Ic_Warehouse.NameLocationSeq     (+)= Ar_Trx_Header_Ship.FromLocationSeq AND
						Ar_Trx_Detail.GlDeleteCode = 'N'

						#WhereClause:Ic_Product_Produce.CmtyIdx#
						#WhereClause:Ic_Warehouse.WarehouseIdx#

				Group By
						Ic_Product_Id.Name,
						Ar_Trx_Product.ProductIdx,
						Ic_Ps_Commodity.Name,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))),
						Concat(Ar_Trx_Header.Sono,Concat(' ',Fc_Name_Location.Descr)),
						Ar_Trx_Product.IcQnt,
						Ar_Trx_Header.Carrier,
						Ic_Ps_Commodity.CmtyIdx,
						Ic_Ps_Style.StyleIdx,
						Ic_Ps_Size.SizeIdx,
						Ic_Ps_Variety.VarietyIdx,
						Ic_Ps_Grade.GradeIdx,
						Ic_Ps_Label.LabelIdx,
						Ic_Warehouse.WarehouseIdx

				Order By 7,8,1,4";
			#endregion

			//build the functions
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32, "Ic_Product_Produce.CmtyIdx", Commodities);
			whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32, "Ic_Warehouse.WarehouseIdx", Warehouses);

			OracleCommand OracleCmd = new OracleCommand(FormatOracle(whereClause.BuildWhereClause(oracleSQL)), OracleConn);
			OracleCmd.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader = OracleCmd.ExecuteReader();

			while (myReader.Read()) 
			{
				#region set customer
				Customer customer = FindOrAddCustomer(CustomersArrayList,myReader.GetString(6));

				if(!myReader.IsDBNull(7))
					customer.TruckName = myReader.GetString(7);
				else
					customer.TruckName = "";

				if(Convert.ToDouble(myReader.GetValue(1)) == 0 && Convert.ToDouble(myReader.GetValue(2)) != 0)
					customer.Status = "Shipped";
				else if(Convert.ToDouble(myReader.GetValue(1)) != 0 && Convert.ToDouble(myReader.GetValue(2)) == 0)
					customer.Status = "Ordered";
				else
					customer.Status = "";
				#endregion

				#region set commodity
				Commodity commodity = FindOrAddCommodity(CommoditiesArrayList,myReader.GetString(5));

				commodity.OrderedQnt += Convert.ToDouble(myReader.GetValue(1));
				commodity.ShippedQnt += Convert.ToDouble(myReader.GetValue(2));
				commodity.ProductIdx = myReader.GetValue(3).ToString();
				commodity.CommodityName = myReader.GetString(4);

				if(!myReader.IsDBNull(8))
					commodity.CommodityKey = myReader.GetValue(8).ToString();
				else
					commodity.CommodityKey = "-1";

				if(!myReader.IsDBNull(9))
					commodity.StyleKey = myReader.GetValue(9).ToString();
				else
					commodity.StyleKey = "-1";

				if(!myReader.IsDBNull(10))
					commodity.SizeKey = myReader.GetValue(10).ToString();
				else
					commodity.SizeKey = "-1";

				if(!myReader.IsDBNull(11))
					commodity.VarietyKey = myReader.GetValue(11).ToString();
				else
					commodity.VarietyKey = "-1";

				if(!myReader.IsDBNull(12))
					commodity.GradeKey = myReader.GetValue(12).ToString();
				else
					commodity.GradeKey = "-1";

				if(!myReader.IsDBNull(13))
					commodity.LabelKey = myReader.GetValue(13).ToString();
				else
					commodity.LabelKey = "-1";

				if(!myReader.IsDBNull(14))
					commodity.WarehouseKey = myReader.GetValue(14).ToString();
				else
					commodity.WarehouseKey = "-1";
				#endregion

				#region set customer & commodity
				CustomerCommodity customerCommodity = FindOrAddCustomerCommodity(CustomersCommoditiesArrayList,myReader.GetString(6),myReader.GetString(5));

				if(Convert.ToDouble(myReader.GetValue(1)) == 0 && Convert.ToDouble(myReader.GetValue(2)) != 0)
					customerCommodity.Qnt += Convert.ToDouble(myReader.GetValue(2));
				else if(Convert.ToDouble(myReader.GetValue(1)) != 0 && Convert.ToDouble(myReader.GetValue(2)) == 0)
					customerCommodity.Qnt += Convert.ToDouble(myReader.GetValue(1));
				else
					customerCommodity.Qnt += 0;
				#endregion
			}

			myReader.Close();
			myReader.Dispose();
			OracleCmd.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();

			//sql pull

			#region oracle code for On Hand and Production
			/*
						#region oracle code for On Hand and Production
						string oracleSQL2 = @"
							Select	distinct
									0 AS OrderedQnt,
									0 AS ShippedQnt,
									Ap_Po_Product.ProductIdx,
									Ap_Po_Product.IcQnt AS OnHandIcQnt,
									0 AS TodayIcQnt,
									Ic_Ps_Commodity.Name AS CommodityName,
									Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name))))))))))

							From	Ap_Po_Header,
									Ap_Po_Detail,
									Ap_Po_Product,
									Ic_Product_Produce,
									Ic_Ps_Commodity,
									Ic_Ps_Style,
									Ic_Ps_Size,
									Ic_Ps_Variety,
									Ic_Ps_Grade,
									Ic_Ps_Label
							Where
									Ap_Po_Detail.GlDeleteCode = 'N' AND
									Ap_Po_Header.ApPoHdrIdx = Ap_Po_Detail.ApPoHdrIdx AND
									Ap_Po_Detail.ApPoHdrIdx = Ap_Po_Product.ApPoHdrIdx AND
									Ap_Po_Detail.ApPoDtlSeq = Ap_Po_Product.ApPoDtlSeq AND
									Ap_Po_Product.ProductIdx (+)= Ic_Product_Produce.ProductIdx AND
									Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
									Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
									Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
									Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
									Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
									Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx 
									#WhereClause:Ap_Po_Header.ApPoNo#
									#WhereClause:Ic_Product_Produce.CmtyIdx#

							Union All

							Select	distinct
									0 AS OrderedQnt,
									0 AS ShippedQnt,
									Ap_Po_Product.ProductIdx,
									0 AS OnHandIcQnt,
									Ap_Po_Product.IcQnt AS TodayIcQnt,
									Ic_Ps_Commodity.Name AS CommodityName,
									Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name))))))))))

							From	Ap_Po_Header Header,
									Ap_Po_Detail,
									Ap_Po_Product,
									Ic_Product_Produce,
									Ic_Ps_Commodity,
									Ic_Ps_Style,
									Ic_Ps_Size,
									Ic_Ps_Variety,
									Ic_Ps_Grade,
									Ic_Ps_Label

							Where
									Ap_Po_Detail.GlDeleteCode = 'N' AND
									Header.ApPoHdrIdx = Ap_Po_Detail.ApPoHdrIdx AND
									Ap_Po_Detail.ApPoHdrIdx = Ap_Po_Product.ApPoHdrIdx AND
									Ap_Po_Detail.ApPoDtlSeq = Ap_Po_Product.ApPoDtlSeq AND
									Ap_Po_Product.ProductIdx (+)= Ic_Product_Produce.ProductIdx AND
									Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
									Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
									Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
									Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
									Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
									Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx 
									#WhereClause:Header.ApPoNo#
									#WhereClause:Ic_Product_Produce.CmtyIdx#
							Order By 5,6";
						#endregion

						//Setup string to check PO Number.
						string test = "%ONHAND";
						string test2 = "%TODAY";
						if (WarehouseCode != "")
						{
							test = WarehouseCode + "ONHAND";
							test2 = WarehouseCode + "TODAY";
						}
						whereClause.Add(WhereClauseUnit.UnitType.LikeString,"Ap_Po_Header.ApPoNo",test);
						whereClause.Add(WhereClauseUnit.UnitType.LikeString,"Header.ApPoNo",test2);
						whereClause.AddCriteriaBox(WhereClauseUnit.UnitType.Int32, "Ic_Product_Produce.CmtyIdx", Commodities);
						OracleCommand OracleCmd2 = new OracleCommand(FormatOracle(whereClause.BuildWhereClause(oracleSQL2)), OracleConn);

						OracleCmd2.BindByName = true;

						Oracle.DataAccess.Client.OracleDataReader myReader2 = OracleCmd2.ExecuteReader();

						while (myReader2.Read()) 
						{
							region set on hand and production
							Commodity commodity = FindOrAddCommodity(CommoditiesArrayList,myReader2.GetString(6));
				
							commodity.OnHand += Convert.ToDouble(myReader2.GetValue(3));
							commodity.Estimated += Convert.ToDouble(myReader2.GetValue(4));
							endregion
						}

						myReader2.Close();
						myReader2.Dispose();

						OracleCmd2.Dispose();*/
			#endregion

			//Compute totals for commodities
			ComputeTotals();

			if (CustomersArrayList.Count == 0 && CommoditiesArrayList.Count == 0 && CustomersCommoditiesArrayList.Count == 0)
				JavaScriptRedirect(Misc.DecodeQueryString(Request.QueryString["LastPage"])+ "&&Records=None");

			reportSessionManager.AddSession("ReportCommoditiesArrayList", CommoditiesArrayList);
			reportSessionManager.SaveSessions();

			return CommoditiesArrayList;
		}

		public void ComputeTotals()
		{
			for(int i = 0; i < CommoditiesArrayList.Count; i++)
			{
				Commodity commodity = (Commodity)CommoditiesArrayList[i];
				commodity.TotalToShip = commodity.OrderedQnt;
				commodity.TotalShipped = commodity.ShippedQnt;

				GrandTotalToShip += commodity.TotalToShip;
				GrandTotalShipped += commodity.TotalShipped;
				GrandTotalOnHand += commodity.OnHand;
				GrandTotalProduction += commodity.Estimated;
			}
		}

		private Commodity FindOrAddCommodity(ArrayList list,string FullCommodityName)
		{
			Commodity tempCommodity = new Commodity(FullCommodityName);
			int tempCommodityIdx = list.BinarySearch(tempCommodity);

			if(tempCommodityIdx <0)
				list.Insert(Math.Abs(tempCommodityIdx)-1,tempCommodity);
			else
				tempCommodity = (Commodity)list[tempCommodityIdx];

			return tempCommodity;
		}

		private Customer FindOrAddCustomer(ArrayList list,string CustomerName)
		{
			Customer tempCustomer = new Customer(CustomerName);
			int tempCustomerIdx = list.BinarySearch(tempCustomer);

			if(tempCustomerIdx <0)
				list.Insert(Math.Abs(tempCustomerIdx)-1,tempCustomer);
			else
				tempCustomer = (Customer)list[tempCustomerIdx];

			return tempCustomer;
		}

		private CustomerCommodity FindOrAddCustomerCommodity(ArrayList list,string CustomerName,string CommodityFullName)
		{
			CustomerCommodity tempCustomerCommodity = new CustomerCommodity(CustomerName,CommodityFullName);
			int tempCustomerCommodityIdx = list.BinarySearch(tempCustomerCommodity);

			if(tempCustomerCommodityIdx <0)
				list.Insert(Math.Abs(tempCustomerCommodityIdx)-1,tempCustomerCommodity);
			else
				tempCustomerCommodity = (CustomerCommodity)list[tempCustomerCommodityIdx];

			return tempCustomerCommodity;
		}

		private string GetProductName(string productIndex)
		{
			string returnProductName = "";

			OracleConnection OracleConn = GetOracleConn();
			string oracleSQL = FormatOracle(@"
				Select	Ic_Product_Produce.ProductIdx,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))) As FullName

				From	Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Product_Produce

				Where	Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Product_Produce.ProductIdx = :ProductIndex

				Group By
						Ic_Product_Produce.ProductIdx,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name))))))))))

				Order By Fullname");

			OracleCommand OracleCmd = new OracleCommand(oracleSQL, OracleConn);
			OracleCmd.Parameters.Add(":ProductIndex", productIndex);

			OracleCmd.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader2 = OracleCmd.ExecuteReader();

			while (myReader2.Read())
			{
				returnProductName = myReader2.GetValue(1).ToString();
			}

			myReader2.Close();
			myReader2.Dispose();
			OracleCmd.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();

			return returnProductName;
		}

		public void SaveAmounts()
		{
			SqlConnection Conn = GetSQLConn();
			string deleteSql = "use Inventory; delete from [Warehouse_Product_Inventory] where Warehouse_Key=@Warehouse_Key";
			string insertSql = "use Inventory; insert into Warehouse_Product_Inventory (Warehouse_Key, Product_Key, Commodity_Key, Style_Key, Size_Key, Variety_Key, Grade_Key, Label_Key, On_Hand, Production) values (@Warehouse_Key, @Product_Key, @Commodity_Key, @Style_Key, @Size_Key, @Variety_Key, @Grade_Key, @Label_Key, @On_Hand, @Production)";

			//first delete all previous lines based on the selected warehouse
			for (int i = 0; i < ReportCommoditiesArrayList.Count; i++)
			{
				Commodity commodity = (Commodity)ReportCommoditiesArrayList[i];

				SqlCommand selectCMD = new SqlCommand("use Inventory; select * from [Warehouse_Product_Inventory] where Warehouse_Key = @Warehouse_Key", Conn);
				selectCMD.Parameters.Add("@Warehouse_Key",commodity.WarehouseKey);

				SqlDataReader myReader2 = selectCMD.ExecuteReader();
			
				if(myReader2.HasRows ==true)
				{
					myReader2.Close();

					SqlCommand cmd = new SqlCommand();
					cmd.Connection = Conn;

					cmd.CommandText = deleteSql;
					cmd.Parameters.Add("@Warehouse_Key",commodity.WarehouseKey);

					cmd.ExecuteNonQuery();
				}
				else
					myReader2.Close();
			}

			//second insert the new values for On Hand and Production
			for (int i = 0; i < ReportCommoditiesArrayList.Count; i++)
			{
				if((Request["OnHandTextBox" + i].Replace(",","") != "0" && Request["OnHandTextBox" + i].Replace(",","") != "") || (Request["ProductionTextBox" + i].Replace(",","") != "0" && Request["ProductionTextBox" + i].Replace(",","") != ""))
				{
					Commodity commodity = (Commodity)ReportCommoditiesArrayList[i];

					SqlCommand cmd = new SqlCommand();
					cmd.Connection = Conn;

					cmd.CommandText = insertSql;
					cmd.Parameters.Add("@Warehouse_Key",commodity.WarehouseKey);
					cmd.Parameters.Add("@Product_Key",commodity.ProductIdx);

					if(commodity.CommodityKey != "-1")
						cmd.Parameters.Add("@Commodity_Key",commodity.CommodityKey);
					else
						cmd.Parameters.Add("@Commodity_Key",DBNull.Value);

					if(commodity.StyleKey != "-1")
						cmd.Parameters.Add("@Style_Key",commodity.StyleKey);
					else
						cmd.Parameters.Add("@Style_Key",DBNull.Value);

					if(commodity.SizeKey != "-1")
						cmd.Parameters.Add("@Size_Key",commodity.SizeKey);
					else
						cmd.Parameters.Add("@Size_Key",DBNull.Value);

					if(commodity.VarietyKey != "-1")
						cmd.Parameters.Add("@Variety_Key",commodity.VarietyKey);
					else
						cmd.Parameters.Add("@Variety_Key",DBNull.Value);

					if(commodity.GradeKey != "-1")
						cmd.Parameters.Add("@Grade_Key",commodity.GradeKey);
					else
						cmd.Parameters.Add("@Grade_Key",DBNull.Value);

					if(commodity.LabelKey != "-1")
						cmd.Parameters.Add("@Label_Key",commodity.LabelKey);
					else
						cmd.Parameters.Add("@Label_Key",DBNull.Value);

					cmd.Parameters.Add("@On_Hand",Request["OnHandTextBox" + i].Replace(",",""));
					cmd.Parameters.Add("@Production",Request["ProductionTextBox" + i].Replace(",",""));

					cmd.ExecuteNonQuery();
				}
			}

			Misc.CleanUpSQL(Conn);
		}
		#endregion

		#region CBO
		
		public class Commodity: IComparable
		{
			#region Declares
			public double OrderedQnt;
			public double ShippedQnt;
			public double OnHand;
			public double Estimated;
			public string ProductIdx;
			public string CommodityName;
			public string CommodityFullName;
			public double TotalShipped;
			public double TotalToShip;
			public string WarehouseKey;
			public string CommodityKey;
			public string StyleKey;
			public string SizeKey;
			public string VarietyKey;
			public string GradeKey;
			public string LabelKey;
			//public double TotalOnHand;
			//public double TotalProduction;
			#endregion

			#region Constructor
			//public Inventory(string product,string shipped,string ordered)
			public Commodity(string commodityFullName)
			{
				#region old
				/*
				Product = product;
				Shipped = shipped;
				Ordered = ordered;
				*/
				/*Product = product;
				OrderedQnt = orderedQnt;
				ShippedQnt = shippedQnt;
				//Status = status;
				OnHand = onHand;
				Estimated = estimated;
				ProductIdx = productIdx;
				OneDayAhead = oneDayAhead;
				TwoDaysAhead = twoDaysAhead;
				ThreePlusDaysAhead = threePlusDaysAhead;
				Commodity = commodity;
				Customer = customer;
				IsTotal = isTotal;*/
				#endregion
				CommodityFullName = commodityFullName;
			}
		
			#endregion

			#region IComparable Members

			public int CompareTo(object obj)
			{
				int tempCompare;
				Commodity Y = (Commodity) obj;

				//Compare Desc
				tempCompare = this.CommodityFullName.CompareTo(Y.CommodityFullName);
				if(tempCompare != 0)
					return tempCompare;
				#region old
				/*
								//Compare Desc
								tempCompare = this.Product.CompareTo(Y.Product);
								if(tempCompare != 0)
									return tempCompare;
				
								//Compare IsTotal
								tempCompare = this.IsTotal.CompareTo(Y.IsTotal);
								if(tempCompare != 0)
									return tempCompare*-1;//*-1 to change asc to dec

								//Compare ShippedQnt
								tempCompare = this.ShippedQnt.CompareTo(Y.ShippedQnt);
								if(tempCompare != 0)
									return tempCompare*-1;

								//Compare OrderedQnt
								tempCompare = this.OrderedQnt.CompareTo(Y.OrderedQnt);
								if(tempCompare != 0)
									return tempCompare*-1;

								//Compare OneDayAhead
								tempCompare = this.OneDayAhead.CompareTo(Y.OneDayAhead);
								if(tempCompare != 0)
									return tempCompare*-1;

								//Compare TwoDaysAhead
								tempCompare = this.TwoDaysAhead.CompareTo(Y.TwoDaysAhead);
								if(tempCompare != 0)
									return tempCompare*-1;

								//Compare ThreePlusDaysAhead
								tempCompare = this.ThreePlusDaysAhead.CompareTo(Y.ThreePlusDaysAhead);
								if(tempCompare != 0)
									return tempCompare*-1;
				*/
				#endregion
				return 0;
			}

			#endregion
		}

		public class Customer: IComparable
		{
			#region Declares
			public string CustomerName;
			public string Status;
			public string TruckName;
			#endregion

			#region Constructor
			public Customer(string customerName)
			{
				CustomerName = customerName;
			}
		
			#endregion

			#region IComparable Members

			public int CompareTo(object obj)
			{
				int tempCompare;
				Customer Y = (Customer) obj;

				//Compare Customer
				tempCompare = this.CustomerName.CompareTo(Y.CustomerName);
				if(tempCompare != 0)
					return tempCompare;

				return 0;
			}

			#endregion
		}

		public class CustomerCommodity: IComparable
		{
			#region Declares
			public string CustomerName;
			public string CommodityFullName;
			public double Qnt;
			#endregion

			#region Constructor
			public CustomerCommodity(string customerName, string commodityFullName)
			{
				CustomerName = customerName;
				CommodityFullName = commodityFullName;
			}
		
			#endregion

			#region IComparable Members

			public int CompareTo(object obj)
			{
				int tempCompare;
				CustomerCommodity Y = (CustomerCommodity) obj;

				//Compare Customer
				tempCompare = this.CustomerName.CompareTo(Y.CustomerName);
				if(tempCompare != 0)
					return tempCompare;

				//Compare Customer
				tempCompare = this.CommodityFullName.CompareTo(Y.CommodityFullName);
				if(tempCompare != 0)
					return tempCompare;

				return 0;
			}

			#endregion
		}
		#endregion
	}
}
