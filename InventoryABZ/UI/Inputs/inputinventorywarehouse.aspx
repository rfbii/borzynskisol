<%@ Register TagPrefix="cc1" Namespace="Cet" Assembly="Cet.ConfirmButton" %>
<%@ Page language="c#" Inherits="Inventory.UI.Inputs.InputInventoryWarehouse" Codebehind="InputInventoryWarehouse.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Inventory Warehouse</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C#" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<!--<style type="text/css">DIV.scroll { OVERFLOW: auto; ; WIDTH: expression(document.body.clientWidth); ; HEIGHT: expression(document.body.clientHeight-70) }
            </style>-->
      <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
	</head>
	<body class="scroll" bgcolor="white">
		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
			<div class="scroll" id="BodyDiv">
				<table width="600">
					<tr>
						<td height="70" align="center" valign="top" colspan="3">Input Inventory Warehouse</td>
					</tr>
					<tr>
						<td align="left" colspan="3"><font face="Tahoma" size="2">Warehouse</font></td>
					</tr>
					<tr>
						<td width="232" align="left" rowspan="2"><asp:listbox id="WarehouseList" runat="server" Width="232px" Height="136px" Font-Names="Tahoma" AutoPostBack="True" Enabled="False" onselectedindexchanged="WarehouseList_SelectedIndexChanged"></asp:listbox></td>
						<td width="192" valign="top" align="left"><asp:label id="warehouseName" runat="server" Width="192px" Font-Names="Tahoma" Font-Size="Small"></asp:label></td>
						<td width="100" align="left" valign="top"><asp:textbox id="warehouseKey" runat="server" Width="100px" Height="19" Visible="False"></asp:textbox></td>
					</tr>
					<tr>
						<td align="left" valign="middle" colspan="3"><asp:textbox id="warehouseCode" tabIndex="1" runat="server" EnterAsTab="" Width="40px" Enabled="False" MaxLength="2"></asp:textbox>&nbsp;&nbsp;<font face="Tahoma" size="2">Warehouse Code</font></td>
					</tr>
					<tr>
						<td align="left" valign="middle" colspan="3"><asp:textbox id="refreshRate" tabIndex="1" runat="server" EnterAsTab="" Width="50px" Enabled="True"></asp:textbox>&nbsp;&nbsp;<font face="Tahoma" size="2">Refresh 
								Rate (in Minutes)</font>
							<asp:RangeValidator id="RangeValidator1" runat="server" Display="Dynamic" ControlToValidate="refreshRate"
								MaximumValue="16000" MinimumValue="5" Type="Integer" ErrorMessage="Refresh Rate Must be more then 5"></asp:RangeValidator>
						</td>
					</tr>
					<tr>
						<td width="232" align="right" valign="bottom"><asp:button id="save" runat="server" Height="24" Width="160" Text="Save" EnterAsTab="" Enabled="True"
								tabIndex="2" onclick="save_Click"></asp:button></td>
						<td width="192" align="center" valign="bottom"><asp:button id="cancel" runat="server" Height="24" Width="160" Enabled="true" Text="Cancel"
								EnterAsTab="" CausesValidation="False" tabstop="" tabIndex="3" onclick="cancel_Click"></asp:button></td>
						<td width="160" align="left" valign="bottom"><asp:button id="Return" runat="server" Height="24" Width="160" Text="Return to Menu" Enabled="True"
								tabIndex="4" onclick="Return_Click"></asp:button></td>
					</tr>
					<tr>
						<td align="left" colspan="3"><asp:label id="Message" runat="server" Height="19" Width="552px" ForeColor="Red"></asp:label></td>
					</tr>
				</table>
				<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
			</div>
		</form>
	</body>
</html>