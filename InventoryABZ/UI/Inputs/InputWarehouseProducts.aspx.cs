using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb; 
using Oracle.DataAccess.Client;
using KeyCentral.Lookups;
using KeyCentral.Functions;

namespace Inventory.UI.Inputs
{

	/// <summary>
	/// Summary description for InputWarehouseProducts
	/// </summary>
	public partial class InputWarehouseProducts : BasePage
	{
		#region Declares
		protected string GrowerName = "";
		#endregion

		#region Events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Inventory Input") == false)
			{
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=");
			}
		}

		protected void Add_Click(object sender, System.EventArgs e)
		{			
			if(availableList.SelectedIndex!=-1)
			{
				string ProductKey = ParseTildaString(availableList.SelectedItem.Value,0);
				string CommodityKey = ParseTildaString(availableList.SelectedItem.Value,1);
				string StyleKey = ParseTildaString(availableList.SelectedItem.Value,2);
				string SizeKey = ParseTildaString(availableList.SelectedItem.Value,3);
				string VarietyKey = ParseTildaString(availableList.SelectedItem.Value,4);
				string GradeKey = ParseTildaString(availableList.SelectedItem.Value,5);
				string LabelKey = ParseTildaString(availableList.SelectedItem.Value,6);

				SqlConnection Conn = GetSQLConn();
				SqlCommand insertCMD = new SqlCommand("use Inventory; insert into [Warehouse_Products] (Warehouse_Key,Product_Key,Commodity_Key,Style_Key,Size_Key,Variety_Key,Grade_Key,Label_Key) values(@Warehouse_Key,@Product_Key,@Commodity_Key,@Style_Key,@Size_Key,@Variety_Key,@Grade_Key,@Label_Key)", Conn);
				insertCMD.Parameters.Add("@Warehouse_Key",warehouseKey.Text);
				insertCMD.Parameters.Add("@Product_Key",ProductKey);

				if(CommodityKey != "-1")
					insertCMD.Parameters.Add("@Commodity_Key",CommodityKey);
				else
					insertCMD.Parameters.Add("@Commodity_Key",DBNull.Value);

				if(StyleKey != "-1")
					insertCMD.Parameters.Add("@Style_Key",StyleKey);
				else
					insertCMD.Parameters.Add("@Style_Key",DBNull.Value);

				if(SizeKey != "-1")
					insertCMD.Parameters.Add("@Size_Key",SizeKey);
				else
					insertCMD.Parameters.Add("@Size_Key",DBNull.Value);

				if(VarietyKey != "-1")
					insertCMD.Parameters.Add("@Variety_Key",VarietyKey);
				else
					insertCMD.Parameters.Add("@Variety_Key",DBNull.Value);

				if(GradeKey != "-1")
					insertCMD.Parameters.Add("@Grade_Key",GradeKey);
				else
					insertCMD.Parameters.Add("@Grade_Key",DBNull.Value);

				if(LabelKey != "-1")
					insertCMD.Parameters.Add("@Label_Key",LabelKey);
				else
					insertCMD.Parameters.Add("@Label_Key",DBNull.Value);
				
				insertCMD.ExecuteNonQuery();
				Message.Text = "Product Added.";

				FillSelectedList();
				FillAvailableList();
				Conn.Close();
				Conn.Dispose();
			}
			else
			{
				Message.Text = "Please select a Product to add.";
			}
		}

		protected void Remove_Click(object sender, System.EventArgs e)
		{
			if(selectedList.SelectedIndex!=-1)
			{
				string ProductKey = ParseTildaString(selectedList.SelectedItem.Value,0);

				SqlConnection Conn = GetSQLConn();

				SqlCommand removeCMD = new SqlCommand("use Inventory; delete from [Warehouse_Products] where Warehouse_Key=@Warehouse_Key and Product_Key=@Product_Key", Conn);
				removeCMD.Parameters.Add("@Warehouse_Key",warehouseKey.Text);
				removeCMD.Parameters.Add("@Product_Key",ProductKey);

				removeCMD.ExecuteNonQuery();

				Message.Text = "Product Removed.";

				FillSelectedList();
				FillAvailableList();

				Conn.Close();
				Conn.Dispose();
			}
			else
			{
				Message.Text = "Please select a Product to remove.";
			}
		}

		protected void addAll_Click(object sender, System.EventArgs e)
		{	
			FillSelectedList();
			
			SqlConnection Conn = GetSQLConn();
					
			OracleConnection OracleConn = GetOracleConn();
			OracleCommand OracleData = new OracleCommand(FormatOracle(@"
				Select	Ic_Product_Produce.ProductIdx,
						Ic_Product_Produce.CmtyIdx,
						Ic_Product_Produce.StyleIdx,
						Ic_Product_Produce.SizeIdx,
						Ic_Product_Produce.VarietyIdx,
						Ic_Product_Produce.GradeIdx,
						Ic_Product_Produce.LabelIdx,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))) As FullName

				From	Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Product_Produce

				Where	Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx

				Group By
						Ic_Product_Produce.ProductIdx,
						Ic_Product_Produce.CmtyIdx,
						Ic_Product_Produce.StyleIdx,
						Ic_Product_Produce.SizeIdx,
						Ic_Product_Produce.VarietyIdx,
						Ic_Product_Produce.GradeIdx,
						Ic_Product_Produce.LabelIdx,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name))))))))))

				Order By Fullname"), OracleConn);
			
			DataSet dsRet = new DataSet();
			int x = 0;

			OracleData.BindByName = true;
		
			availableList.Items.Clear();

			Oracle.DataAccess.Client.OracleDataReader myReader = OracleData.ExecuteReader();

			while (myReader.Read()) 
			{
				x = KeyCentral.Functions.Misc.TextIsInListBox(this.selectedList,myReader.GetValue(7).ToString());
				if (x == -1)
				{
					selectedList.Items.Add(new ListItem(myReader.GetString(7),myReader.GetValue(0).ToString()));
					SqlCommand insertCMD = new SqlCommand("use Inventory; insert into [Warehouse_Products] (Warehouse_Key,Product_Key,Commodity_Key,Style_Key,Size_Key,Variety_Key,Grade_Key,Label_Key) values(@Warehouse_Key,@Product_Key,@Commodity_Key,@Style_Key,@Size_Key,@Variety_Key,@Grade_Key,@Label_Key)", Conn);
					insertCMD.Parameters.Add("@Warehouse_Key",warehouseKey.Text);
					insertCMD.Parameters.Add("@Product_Key",myReader.GetValue(0));

					if(!myReader.IsDBNull(1))
						insertCMD.Parameters.Add("@Commodity_Key",myReader.GetValue(1));
					else
						insertCMD.Parameters.Add("@Commodity_Key",DBNull.Value);

					if(!myReader.IsDBNull(2))
						insertCMD.Parameters.Add("@Style_Key",myReader.GetValue(2));
					else
						insertCMD.Parameters.Add("@Style_Key",DBNull.Value);

					if(!myReader.IsDBNull(3))
						insertCMD.Parameters.Add("@Size_Key",myReader.GetValue(3));
					else
						insertCMD.Parameters.Add("@Size_Key",DBNull.Value);

					if(!myReader.IsDBNull(4))
						insertCMD.Parameters.Add("@Variety_Key",myReader.GetValue(4));
					else
						insertCMD.Parameters.Add("@Variety_Key",DBNull.Value);

					if(!myReader.IsDBNull(5))
						insertCMD.Parameters.Add("@Grade_Key",myReader.GetValue(5));
					else
						insertCMD.Parameters.Add("@Grade_Key",DBNull.Value);

					if(!myReader.IsDBNull(6))
						insertCMD.Parameters.Add("@Label_Key",myReader.GetValue(6));
					else
						insertCMD.Parameters.Add("@Label_Key",DBNull.Value);

					insertCMD.ExecuteNonQuery();
				}
				else
				{
					selectedList.Items.Add(new ListItem(myReader.GetString(7),selectedList.Items[x].Value));
					selectedList.Items.Remove(selectedList.Items[x]);
				}
			}
			Message.Text = "All Products Added.";
			myReader.Close();
			myReader.Dispose();
			OracleData.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();
			Conn.Close();
		}

		protected void removeAll_Click(object sender, System.EventArgs e)
		{
			SqlConnection Conn = GetSQLConn();

			SqlCommand removeCMD = new SqlCommand("use Inventory; delete from [Warehouse_Products] where Warehouse_Key=@Warehouse_Key", Conn);
			removeCMD.Parameters.Add("@Warehouse_Key",warehouseKey.Text);
			removeCMD.ExecuteNonQuery();

			FillSelectedList();
			FillAvailableList();

			Message.Text = "All Products Removed.";

			Conn.Close();
			Conn.Dispose();
		}

		protected void Return_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Inventory/Inputs");
		}

		protected void WarehouseList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			warehouseKey.Text = "";
			warehouseName.Text = "";
			Message.Text = "";

			warehouseName.Text = WarehouseList.SelectedItem.Text;
			warehouseKey.Text = WarehouseList.SelectedItem.Value;

			FillSelectedList();
			FillAvailableList();
		}

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			FillWarehouseList();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Private helpers
		private void FillWarehouseList()
		{
			OracleConnection OracleConn = GetOracleConn();
			string oracleSQL = FormatOracle(@"
				Select
					Fc_Name_Location.Descr,
					Ic_Warehouse.WarehouseIdx
				From
					#COMPANY_NUMBER#.Ic_Warehouse,
					#COMPANY_NUMBER#.Fc_Name_Location
				Where
					Ic_Warehouse.NameIdx = Fc_Name_Location.NameIdx and
					Ic_Warehouse.NameLocationSeq = Fc_Name_Location.NameLocationSeq
				Order by Descr");
			OracleCommand OracleCmd = new OracleCommand(oracleSQL, OracleConn);
			OracleCmd.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader = OracleCmd.ExecuteReader();

			while (myReader.Read()) 
			{
				WarehouseList.Items.Add(new ListItem(myReader.GetValue(0).ToString(),myReader.GetValue(1).ToString()));
			}

			myReader.Close();
			myReader.Dispose();
			OracleCmd.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();
			warehouseName.Text = "";

			if(WarehouseList.Items.Count > 0)
				WarehouseList.Enabled = true;
			else
				WarehouseList.Enabled = false;
		}

		private void FillAvailableList()
		{
			availableList.Items.Clear();

			OracleConnection OracleConn = GetOracleConn();
			OracleCommand OracleData = new OracleCommand(FormatOracle(@"
				Select	Ic_Product_Produce.ProductIdx,
						Ic_Product_Produce.CmtyIdx,
						Ic_Product_Produce.StyleIdx,
						Ic_Product_Produce.SizeIdx,
						Ic_Product_Produce.VarietyIdx,
						Ic_Product_Produce.GradeIdx,
						Ic_Product_Produce.LabelIdx,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))) As FullName

				From	Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Product_Produce

				Where	Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx

				Group By
						Ic_Product_Produce.ProductIdx,
						Ic_Product_Produce.CmtyIdx,
						Ic_Product_Produce.StyleIdx,
						Ic_Product_Produce.SizeIdx,
						Ic_Product_Produce.VarietyIdx,
						Ic_Product_Produce.GradeIdx,
						Ic_Product_Produce.LabelIdx,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name))))))))))

				Order By Fullname"), OracleConn);

			DataSet dsRet = new DataSet();
			int x = 0;

			OracleData.BindByName = true;
			Oracle.DataAccess.Client.OracleDataReader myReader = OracleData.ExecuteReader();

			while (myReader.Read()) 
			{
				string availableListboxValue = "";

				availableListboxValue += myReader.GetValue(0).ToString();

				availableListboxValue += "~";

				if(!myReader.IsDBNull(1))
					availableListboxValue += myReader.GetValue(1).ToString();
				else
					availableListboxValue += "-1";

				availableListboxValue += "~";

				if(!myReader.IsDBNull(2))
					availableListboxValue += myReader.GetValue(2).ToString();
				else
					availableListboxValue += "-1";

				availableListboxValue += "~";

				if(!myReader.IsDBNull(3))
					availableListboxValue += myReader.GetValue(3).ToString();
				else
					availableListboxValue += "-1";

				availableListboxValue += "~";

				if(!myReader.IsDBNull(4))
					availableListboxValue += myReader.GetValue(4).ToString();
				else
					availableListboxValue += "-1";

				availableListboxValue += "~";

				if(!myReader.IsDBNull(5))
					availableListboxValue += myReader.GetValue(5).ToString();
				else
					availableListboxValue += "-1";

				availableListboxValue += "~";

				if(!myReader.IsDBNull(6))
					availableListboxValue += myReader.GetValue(6).ToString();
				else
					availableListboxValue += "-1";

				x = KeyCentral.Functions.Misc.TextIsInListBox(this.selectedList,myReader.GetValue(7).ToString());
				if (x == -1)
					availableList.Items.Add(new ListItem(myReader.GetString(7),availableListboxValue));
				else
				{
					selectedList.Items.Add(new ListItem(myReader.GetString(7),selectedList.Items[x].Value));
					selectedList.Items.Remove(selectedList.Items[x]);
				}
			}

			myReader.Close();
			myReader.Dispose();
			OracleData.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();
		}

		private void FillSelectedList()
		{
			selectedList.Items.Clear();

			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("use inventory; select Product_Key, Commodity_Key, Style_Key, Size_Key, Variety_Key, Grade_Key, Label_Key from [Warehouse_Products] where Warehouse_Key=@Warehouse_Key", Conn);
			selectCMD.Parameters.Add("@Warehouse_Key",warehouseKey.Text);

			SqlDataReader myReader = selectCMD.ExecuteReader();

			while(myReader.Read())
			{
				string selectedListboxValue = "";

				selectedListboxValue += myReader.GetValue(0).ToString();

				selectedListboxValue += "~";

				if(!myReader.IsDBNull(1))
					selectedListboxValue += myReader.GetValue(1).ToString();
				else
					selectedListboxValue += "-1";

				selectedListboxValue += "~";

				if(!myReader.IsDBNull(2))
					selectedListboxValue += myReader.GetValue(2).ToString();
				else
					selectedListboxValue += "-1";

				selectedListboxValue += "~";

				if(!myReader.IsDBNull(3))
					selectedListboxValue += myReader.GetValue(3).ToString();
				else
					selectedListboxValue += "-1";

				selectedListboxValue += "~";

				if(!myReader.IsDBNull(4))
					selectedListboxValue += myReader.GetValue(4).ToString();
				else
					selectedListboxValue += "-1";

				selectedListboxValue += "~";

				if(!myReader.IsDBNull(5))
					selectedListboxValue += myReader.GetValue(5).ToString();
				else
					selectedListboxValue += "-1";

				selectedListboxValue += "~";

				if(!myReader.IsDBNull(6))
					selectedListboxValue += myReader.GetValue(6).ToString();
				else
					selectedListboxValue += "-1";

				selectedList.Items.Add(new ListItem(GetProductName(myReader.GetValue(0).ToString()),selectedListboxValue));
			}

			myReader.Close();
			Conn.Close();
			Conn.Dispose();
		}

		private string GetProductName(string productIndex)
		{
			string returnProductName = "";

			OracleConnection OracleConn = GetOracleConn();
			string oracleSQL = FormatOracle(@"
				Select	Ic_Product_Produce.ProductIdx,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name)))))))))) As FullName

				From	Ic_Ps_Commodity,
						Ic_Ps_Style,
						Ic_Ps_Size,
						Ic_Ps_Variety,
						Ic_Ps_Grade,
						Ic_Ps_Label,
						Ic_Product_Produce

				Where	Ic_Ps_Commodity.CmtyIdx (+)= Ic_Product_Produce.CmtyIdx AND
						Ic_Ps_Style.StyleIdx (+)= Ic_Product_Produce.StyleIdx AND
						Ic_Ps_Size.SizeIdx (+)= Ic_Product_Produce.SizeIdx AND
						Ic_Ps_Variety.VarietyIdx (+)= Ic_Product_Produce.VarietyIdx AND
						Ic_Ps_Grade.GradeIdx (+)= Ic_Product_Produce.GradeIdx AND
						Ic_Ps_Label.LabelIdx (+)= Ic_Product_Produce.LabelIdx AND
						Ic_Product_Produce.ProductIdx = :ProductIndex

				Group By
						Ic_Product_Produce.ProductIdx,
						Concat(Ic_Ps_Commodity.Name,Concat(' ',Concat(Ic_Ps_Style.Name,Concat(' ',Concat(Ic_Ps_Size.Name,Concat(' ',Concat(Ic_Ps_Variety.Name,Concat(' ',Concat(Ic_Ps_Grade.Name,Concat(' ',Ic_Ps_Label.Name))))))))))

				Order By Fullname");

			OracleCommand OracleCmd = new OracleCommand(oracleSQL, OracleConn);
			OracleCmd.Parameters.Add(":ProductIndex", productIndex);

			OracleCmd.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader2 = OracleCmd.ExecuteReader();

			while (myReader2.Read())
			{
				returnProductName = myReader2.GetValue(1).ToString();
			}

			myReader2.Close();
			myReader2.Dispose();
			OracleCmd.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();

			return returnProductName;
		}

		public static string ParseTildaString(string stringToParse,int field)
		{
			string[] stringSplit = stringToParse.Split('~');
			
			if(stringSplit.Length <= field)
				return "";
			else
				return stringSplit[field];
		}
		#endregion
	}
}
