using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb; 
using Oracle.DataAccess.Client;
using KeyCentral.Functions;

namespace Inventory.UI.Inputs
{

	/// <summary>
	/// Summary description for InputInventoryWarehouse.
	/// </summary>
	public partial class InputInventoryWarehouse : BasePage
	{
		#region Declares
		#endregion

		#region events
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (CheckSecurity("Inventory Input") == false)
			{
                Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=");
			}

			if(refreshRate.Text == "")
			{
				SqlConnection Conn = Misc.GetSQLConn();

				SqlCommand selectCMD = new SqlCommand("use Inventory; select Rate from [Refresh_Rate]", Conn);

				SqlDataReader myReader = selectCMD.ExecuteReader();
				if(myReader.HasRows ==true)
				{
					myReader.Read();
					refreshRate.Text = Convert.ToString(myReader.GetInt32(0)/60);
				}
				myReader.Close();
				Misc.CleanUpSQL(Conn);
			}
		}

		protected void Return_Click(object sender, System.EventArgs e)
		{
            Response.Redirect("../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Inventory/Inputs");
		}

		protected void cancel_Click(object sender, System.EventArgs e)
		{
			warehouseCode.Enabled = false;
			warehouseCode.Text = "";
			//save.Enabled = false;
			WarehouseList.SelectedIndex = -1;
			WarehouseList.ClearSelection();
			warehouseKey.Text = "";
			warehouseName.Text = "";
			//refreshRate.Text = "";
			Message.Text = "";
		}

		protected void save_Click(object sender, System.EventArgs e)
		{
			int currentSelectedIndex = WarehouseList.SelectedIndex;
			SqlConnection Conn = Misc.GetSQLConn();

			if(WarehouseList.SelectedIndex != -1)
			{
				SqlCommand selectCMD = new SqlCommand("use Inventory; select Warehouse_Code, Warehouse_Key from [Warehouse] where Warehouse_Key = @warehouseKey", Conn);
				selectCMD.Parameters.Add("@warehouseKey",warehouseKey.Text);

				SqlDataReader myReader = selectCMD.ExecuteReader();
			
				//if exists, update
				if(myReader.HasRows ==true)
				{
					//if empty code, delete
					if(warehouseCode.Text == "")
					{
						myReader.Close();
						SqlCommand deleteCMD = new SqlCommand("use Inventory; delete from [Warehouse] where Warehouse_Key = @Warehouse_Key", Conn);
						deleteCMD.Parameters.Add("@Warehouse_Key",warehouseKey.Text);
						deleteCMD.ExecuteNonQuery();

						Message.Text = "Warehouse Code Deleted.";
					}
					else
					{
						myReader.Close();
						SqlCommand updateCMD = new SqlCommand("use Inventory;update [Warehouse] Set Warehouse_Code=@Warehouse_Code where Warehouse_Key=@Warehouse_Key", Conn);
						updateCMD.Parameters.Add("@Warehouse_Code",warehouseCode.Text);
						updateCMD.Parameters.Add("@Warehouse_Key",warehouseKey.Text);

						updateCMD.ExecuteNonQuery();

						Message.Text = "Warehouse Code Updated.";
					}
				}
				else
				{
					if(warehouseCode.Text == "")
					{
						myReader.Close();
						//Response.Write("<script>alert('No percentage to save.');</script>");
						Message.Text = "No warehouse code to save.";
					}
					else
					{
						myReader.Close();
						SqlCommand insertCMD = new SqlCommand("use Inventory;insert into [Warehouse] (Warehouse_Code, Warehouse_Key) values(@Warehouse_Code,@Warehouse_Key)", Conn);
						insertCMD.Parameters.Add("@Warehouse_Code",warehouseCode.Text);
						insertCMD.Parameters.Add("@Warehouse_Key",warehouseKey.Text);

						insertCMD.ExecuteScalar();

						Message.Text = "Warehouse Code Added.";
					}
				}

				WarehouseList.Items.Clear();

				FillWarehouseList();
			
				//re-select previous item
				WarehouseList.SelectedIndex = currentSelectedIndex;
				if(WarehouseList.SelectedItem.Text.Substring(2,1) == "-")
				{
					warehouseName.Text = WarehouseList.SelectedItem.Text.Substring(3);
				}
				else
				{
					warehouseName.Text = WarehouseList.SelectedItem.Text;
				}
				warehouseKey.Text = WarehouseList.Items[currentSelectedIndex].Value;
			}

			//insert/update Refresh Rate
			int rate = (Convert.ToInt32(refreshRate.Text.ToString())* 60 );
			
			if(refreshRate.Text != "")
			{
				SqlCommand selectCMD = new SqlCommand("use Inventory; select Rate from [Refresh_Rate]", Conn);

				SqlDataReader myReader = selectCMD.ExecuteReader();

				if(myReader.HasRows == true)
				{
					myReader.Close();
					SqlCommand updateCMD = new SqlCommand("use Inventory;update [Refresh_Rate] Set Rate=@Rate", Conn);
					updateCMD.Parameters.Add("@Rate",rate);

					updateCMD.ExecuteNonQuery();

					if(Message.Text != "" && Message.Text != "Refresh Rate Updated.")
						Message.Text += " Refresh Rate Updated.";
					else
						Message.Text = "Refresh Rate Updated.";
				}
				else
				{
					myReader.Close();

					SqlCommand insertCMD = new SqlCommand("use Inventory;insert into [Refresh_Rate] (Rate) values(@Rate)", Conn);
					insertCMD.Parameters.Add("@Rate",rate);

					insertCMD.ExecuteScalar();

					if(Message.Text != "" && Message.Text != "Refresh Rate Added.")
						Message.Text += " Refresh Rate Added.";
					else
						Message.Text = "Refresh Rate Added.";
				}
			}

			Misc.CleanUpSQL(Conn);
		}
		#endregion
			
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			FillWarehouseList();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		#region Private helpers
		protected void WarehouseList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			warehouseCode.Enabled = true;
			//save.Enabled = true;
			warehouseKey.Text = "";
			warehouseName.Text = "";
			Message.Text = "";

			//if warehouse has code, warehouseName is only the warehouse, excluding the code
			if(WarehouseList.SelectedItem.Text.Substring(2,1) == "-")
			{
				warehouseName.Text = WarehouseList.SelectedItem.Text.Substring(3);
			}
			else
			{
				warehouseName.Text = WarehouseList.SelectedItem.Text;
			}

			warehouseKey.Text = WarehouseList.SelectedItem.Value;

			SqlConnection Conn = GetSQLConn();
			SqlCommand selectCMD = new SqlCommand("use Inventory; select Warehouse_Code, Warehouse_Key from [Warehouse] where Warehouse_Key = @warehouseKey", Conn);
			selectCMD.Parameters.Add("@warehouseKey",warehouseKey.Text);

			SqlDataReader myReader = selectCMD.ExecuteReader();
			
			if(myReader.HasRows ==true)
			{
				myReader.Read();

				warehouseCode.Text = myReader.GetValue(0).ToString();
			}
			else
				warehouseCode.Text = "";

			myReader.Close();
			Misc.CleanUpSQL(Conn);
		}

		private void FillWarehouseList()
		{
			OracleConnection OracleConn = GetOracleConn();
			string oracleSQL = FormatOracle(@"
				Select
					Fc_Name_Location.Descr,
					Ic_Warehouse.WarehouseIdx
				From
					#COMPANY_NUMBER#.Ic_Warehouse,
					#COMPANY_NUMBER#.Fc_Name_Location
				Where
					Ic_Warehouse.NameIdx = Fc_Name_Location.NameIdx and
					Ic_Warehouse.NameLocationSeq = Fc_Name_Location.NameLocationSeq
				Order by Descr");
			OracleCommand OracleCmd = new OracleCommand(oracleSQL, OracleConn);
			OracleCmd.BindByName = true;

			Oracle.DataAccess.Client.OracleDataReader myReader = OracleCmd.ExecuteReader();

			while (myReader.Read()) 
			{
				warehouseKey.Text = myReader.GetValue(1).ToString();
				warehouseName.Text = myReader.GetValue(0).ToString();

				SqlConnection Conn = GetSQLConn();
				SqlCommand selectCMD = new SqlCommand("use Inventory; select Warehouse_Code, Warehouse_Key from [Warehouse] where Warehouse_Key = @warehouseKey", Conn);
				selectCMD.Parameters.Add("@warehouseKey",warehouseKey.Text);

				SqlDataReader myReader2 = selectCMD.ExecuteReader();
			
				if(myReader2.HasRows ==true)
				{
					myReader2.Read();

					WarehouseList.Items.Add(new ListItem(myReader2.GetValue(0).ToString() + "-" + myReader.GetValue(0).ToString(),myReader.GetValue(1).ToString()));
				}
				else
				{
					WarehouseList.Items.Add(new ListItem(myReader.GetValue(0).ToString(),myReader.GetValue(1).ToString()));
				}
				myReader2.Close();
			}

			myReader.Close();
			myReader.Dispose();
			OracleCmd.Dispose();
			OracleConn.Close();
			OracleConn.Dispose();
			warehouseName.Text = "";

			if(WarehouseList.Items.Count > 0)
				WarehouseList.Enabled = true;
			else
				WarehouseList.Enabled = false;
		}

		#endregion
	}
}
