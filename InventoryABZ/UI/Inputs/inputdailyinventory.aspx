<%@ Page language="c#" Inherits="Inventory.UI.Inputs.InputDailyInventory" Codebehind="InputDailyInventory.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Input Daily Inventory</title>
		<!-- Email Start -->
		<style type="text/css">.ReportTable.TD { FONT-SIZE: 8pt; FONT-FAMILY: Arial }
		.BottomBlack {FONT-SIZE: 10pt; BORDER-BOTTOM: black 1px solid; FONT-FAMILY: Arial }
		.BottomRightBlack {FONT-SIZE: 8pt; BORDER-BOTTOM: black 1px solid; BORDER-RIGHT: black 1px solid; FONT-FAMILY: Arial }
		.BottomLeftBlack {FONT-SIZE: 8pt; BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; FONT-FAMILY: Arial }
		.BottomLeftRightBlack {FONT-SIZE: 8pt; BORDER-BOTTOM: black 1px solid; BORDER-LEFT: black 1px solid; BORDER-RIGHT: black 1px solid; FONT-FAMILY: Arial }
		.BottomGray {BORDER-BOTTOM: lightgrey 1px solid; FONT-SIZE: 8pt; FONT-FAMILY: Arial}
		.Header {FONT-SIZE: 14pt; FONT-FAMILY: Arial}
		</style>
		<!-- Email End -->
		<meta http-equiv="CACHE-CONTROL" content="NO-CACHE"/>
		<meta http-equiv="PRAGMA" content="NO-CACHE"/>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C#" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<!--<style type="text/css">
			DIV.scroll { OVERFLOW: auto; WIDTH: expression(document.body.clientWidth-11); HEIGHT: expression(document.body.clientHeight-213) }
		</style>-->
        <style type="text/css">DIV.scroll { OVERFLOW: auto; WIDTH: 80%; HEIGHT: 80% }</style>
		<style type="text/css">
			DIV.scrollFooter
			{ 
				OVERFLOW: auto;
				WIDTH: expression(document.body.clientWidth);
				HEIGHT: expression(document.body.clientHeight-200);
			}
			DIV.scrollLeft
			{ 
				OVERFLOW: auto;
				WIDTH: 527px;
				HEIGHT: expression(document.body.clientHeight-232);
			}
			DIV.scrollRight
			{
				OVERFLOW: auto;
				WIDTH: expression(document.body.clientWidth-535);
				HEIGHT: expression(document.body.clientHeight-232);
			}
			DIV.scrollHeader
			{
				OVERFLOW: hidden;
				WIDTH: expression(document.body.clientWidth-550);
				/*HEIGHT: 144px;*/
			}
		</style>

		<script type="text/javascript">
function checkArrowsOnHand (field, evt)
{
	var keyCode = document.layers ? evt.which : document.all ? event.keyCode : document.getElementById ? evt.keyCode : 0;

	if (keyCode == 40) //'arrow down'
	{
		autotab(OnHandTextBoxAfter(field));
		return false;
	}
	else if (keyCode == 38) //'arrow up'
	{
		autotab(OnHandTextBoxBefore(field));
		return false;
	}
	else if (keyCode == 13) //'enter'
	{
		autotab(OnHandTextBoxAfter(field));
		return false;
	}
	else if (keyCode == 9 && evt.shiftKey) //'tab & shift'
	{
		autotab(OnHandTextBoxBefore(field));
		return false;
	}
	else if (keyCode == 9) //'tab'
	{
		autotab(OnHandTextBoxAfter(field));
		return false;
	}
	return true;
}

function checkArrowsProduction (field, evt)
{
	var keyCode = document.layers ? evt.which : document.all ? event.keyCode : document.getElementById ? evt.keyCode : 0;
	if (keyCode == 40) //'arrow down'
	{
		autotab(ProductionTextBoxAfter(field));
		return false;
	}
	else if (keyCode == 38) //'arrow up'
	{
		autotab(ProductionTextBoxBefore(field));
		return false;
	}
	else if (keyCode == 13) //'enter'
	{
		autotab(ProductionTextBoxAfter(field));
		return false;
	}
	else if (keyCode == 9 && evt.shiftKey) //'tab & shift'
	{
		autotab(ProductionTextBoxBefore(field));
		return false;
	}
	else if (keyCode == 9) //'tab'
	{
		autotab(ProductionTextBoxAfter(field));
		return false;
	}
	return true;
}

function autotab(destination)
{
	txtbox = document.getElementsByName(destination);
	txtbox[0].focus();
}

function OnHandTextBoxAfter(temp)
{
	txtbox = document.getElementsByName(temp);
	for(z = 0; z < document.getElementsByName('OnHandTextBox').length; z++)
	{
		if((document.getElementsByName('OnHandTextBox' + z) == document.getElementsByName(temp)) && (z != document.getElementsByName('OnHandTextBox').length - 1))
		{
			txtbox = document.getElementsByName('OnHandTextBox' + (z + 1));
			z = document.getElementsByName('OnHandTextBox').length;
		}
		
		if(z == document.getElementsByName('OnHandTextBox').length - 1)
		{
			txtbox = document.getElementsByName('OnHandTextBox' + 0);
		}
	}
	return txtbox[0].name;
}

function OnHandTextBoxBefore(temp)
{
	txtbox = document.getElementsByName(temp);
	for(z = 0; z < document.getElementsByName('OnHandTextBox').length; z++)
	{
		if((document.getElementsByName('OnHandTextBox' + z) == document.getElementsByName(temp)) && (z != 0))
		{
			txtbox = document.getElementsByName('OnHandTextBox' + (z - 1));
			z = document.getElementsByName('OnHandTextBox').length;
		}
		
		if(z == 0)
		{
			txtbox = document.getElementsByName('OnHandTextBox' + (document.getElementsByName('OnHandTextBox').length - 1));
		}
	}
	return txtbox[0].name;
}

function ProductionTextBoxAfter(temp)
{
	txtbox = document.getElementsByName(temp);
	for(z = 0; z < document.getElementsByName('ProductionTextBox').length; z++)
	{
		if((document.getElementsByName('ProductionTextBox' + z) == document.getElementsByName(temp)) && (z != document.getElementsByName('ProductionTextBox').length - 1))
		{
			txtbox = document.getElementsByName('ProductionTextBox' + (z + 1));
			z = document.getElementsByName('ProductionTextBox').length;
		}
		
		if(z == document.getElementsByName('ProductionTextBox').length - 1)
		{
			txtbox = document.getElementsByName('ProductionTextBox' + 0);
		}
	}
	return txtbox[0].name;
}

function ProductionTextBoxBefore(temp)
{
	txtbox = document.getElementsByName(temp);
	for(z = 0; z < document.getElementsByName('ProductionTextBox').length; z++)
	{
		if((document.getElementsByName('ProductionTextBox' + z) == document.getElementsByName(temp)) && (z != 0))
		{
			txtbox = document.getElementsByName('ProductionTextBox' + (z - 1));
			z = document.getElementsByName('ProductionTextBox').length;
		}
		
		if(z == 0)
		{
			txtbox = document.getElementsByName('ProductionTextBox' + (document.getElementsByName('ProductionTextBox').length - 1));
		}
	}
	return txtbox[0].name;
}

function HideButtons()
{
	document.all.buttons.style.visibility=document.all.buttons.style.visibility=='hidden' ? 'hidden' : 'hidden';
}

function ShowButtons()
{
	document.all.buttons.style.visibility=document.all.buttons.style.visibility=='hidden' ? '' : '';
}

function IsNumeric(sText)
{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;
   var numberOfPeriods = 0;

   for (i = 0; i < sText.length && IsNumber == true; i++)
   {
      Char = sText.charAt(i);
      if (ValidChars.indexOf(Char) == -1)
      {
         IsNumber = false;
      }
      if(Char == ".")
      	numberOfPeriods++;
      
      if(numberOfPeriods > 1)
      	IsNumber = false;
   }

   return IsNumber;
}

function ValidateAmount(form)
{
	textbox = document.getElementsByName(form);
	if (!IsNumeric(textbox[0].value) || textbox[0].value == "" || textbox[0].value == ".")
	{
      	//alert('Please enter only numbers or decimal points in the amount field.');
		textbox[0].focus();
		return false;
	}

	return true;
}

function RenewSession()
{
	document.images("renewSession").src = "../../../InventoryABZ/UI/Inputs/RenewSession.aspx?par=" + Math.random();
}
</script>
	</head>
	<body onbeforeprint="SetupPrinting();">
		<object id="factory" style="DISPLAY: none" codebase="http://www.meadroid.com/scriptx/ScriptX.cab#Version=6,1,431,2" classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814" viewastext=""></object>
		<script type="text/javascript" language="javascript">
		function SetupPrinting()
		{
			factory.printing.footer = "Printed: &d &t&bPage &p of &P&bInput Daily Inventory";
			factory.printing.header = "";
			factory.printing.leftMargin = .5;
			factory.printing.rightMargin = .5;
			factory.printing.topMargin = .5;
			factory.printing.bottomMargin = .5;
			factory.printing.portrait = false;
		}
		</script>

				<script type="text/javascript" language='javascript'>
	//<input name="Repeater1:_ctl22:TextBox1" type="text" value="0" id="Repeater1__ctl22_TextBox1" value="0" />
	//FormatNumber(Expression, NumDigitsAfterDecimal, IncludeLeadingDigit,
    //          UseParensForNegativeNumbers, GroupDigits)
	//======================================================================


function FormatNumber(num,decimalNum,bolLeadingZero,bolParens,bolCommas)
/**********************************************************************
	IN:
		NUM - the number to format
		decimalNum - the number of decimal places to format the number to
		bolLeadingZero - true / false - display a leading zero for
										numbers between -1 and 1
		bolParens - true / false - use parenthesis around negative numbers
		bolCommas - put commas as number separators.
 
	RETVAL:
		The formatted number!
 **********************************************************************/
{ 
    if (isNaN(parseInt(num))) return "NaN";

	var tmpNum = num;
	var iSign = num < 0 ? -1 : 1;		// Get sign of number
	
	// Adjust number so only the specified number of numbers after
	// the decimal point are shown.
	tmpNum *= Math.pow(10,decimalNum);
	tmpNum = Math.round(Math.abs(tmpNum))
	tmpNum /= Math.pow(10,decimalNum);
	tmpNum *= iSign;					// Readjust for sign

	// Create a string object to do our formatting on
	var tmpNumStr = new String(tmpNum);

	// See if we need to strip out the leading zero or not.
	if (!bolLeadingZero && num < 1 && num > -1 && num != 0)
		if (num > 0)
			tmpNumStr = tmpNumStr.substring(1,tmpNumStr.length);
		else
			tmpNumStr = "-" + tmpNumStr.substring(2,tmpNumStr.length);
		
	// See if we need to put in the commas
	if (bolCommas && (num >= 1000 || num <= -1000))
	{
		var iStart = tmpNumStr.indexOf(".");
		if (iStart < 0)
			iStart = tmpNumStr.length;

		iStart -= 3;
		while (iStart >= 1)
		{
			tmpNumStr = tmpNumStr.substring(0,iStart) + "," + tmpNumStr.substring(iStart,tmpNumStr.length)
			iStart -= 3;
		}		
	}

	// See if we need to use parenthesis
	if (bolParens && num < 0)
		tmpNumStr = "(" + tmpNumStr.substring(1,tmpNumStr.length) + ")";

	out = "-,"; // replace this
	add = "-"; // with this
	temp = tmpNumStr; // temporary holder

	while (temp.indexOf(out)>-1)
	{
		pos= temp.indexOf(out);
		temp = "" + (temp.substring(0, pos) + add + 
		temp.substring((pos + out.length), temp.length));
	}
			
	tmpNumStr = temp;

	return tmpNumStr;		// Return our formatted string!
}
function TotalAmounts()
{
	total = Number(0.0);
	for(i=1;i<53;i++)
	{
		textbox = document.getElementsByName('Repeater1:_ctl' + i + ':TextBox1');
		//alert(textbox[0].value);
		total = total + Number(textbox[0].value);
	}
	document.getElementById("TotalDiv").innerText= " " + FormatNumber(total,3,true,false,true);
	//alert(total);
}
function ResetAmounts()
{
	total = Number(0.0);
	for(i = 1; i < 53; i++)
	{
		textbox = document.getElementsByName('Repeater1:_ctl' + i + ':TextBox1');
		textbox[0].value = 0.0;
		total = total + Number(textbox[0].value);
		//alert(textbox[0].value);
	}
	document.getElementById("TotalDiv").innerText= " " + FormatNumber(total,3,true,false,true);
}
		
function replaceChars(entry)
{
	out = ","; // replace this
	add = ""; // with this
	temp = "" + entry; // temporary holder

	while (temp.indexOf(out)>-1)
	{
		pos= temp.indexOf(out);
		temp = "" + (temp.substring(0, pos) + add + 
		temp.substring((pos + out.length), temp.length));
	}
	return temp;
}
		function TotalOnHand()
		{
			totalOnHand = Number(0.0);
			
			for(i = 0; i < document.getElementsByName('OnHandTextBox').length; i++)
			{
				totalOnHand = totalOnHand + Number(replaceChars(document.getElementsByName('OnHandTextBox')[i].value));
			}

			for(i = 0; i < document.getElementsByName('GrandTotalOnHandTextBox').length; i++)
			{
				document.getElementsByName('GrandTotalOnHandTextBox')[i].value = FormatNumber(totalOnHand,0,false,false,true);
			}
		}
		
		function TotalProduction()
		{
			totalProduction = Number(0.0);
			
			for(i = 0; i < document.getElementsByName('ProductionTextBox').length; i++)
			{
				totalProduction = totalProduction + Number(replaceChars(document.getElementsByName('ProductionTextBox')[i].value));
			}

			for(i = 0; i < document.getElementsByName('GrandTotalProductionTextBox').length; i++)
			{
				document.getElementsByName('GrandTotalProductionTextBox')[i].value = FormatNumber(totalProduction,0,false,false,true);
			}
		}
		
		function UpdateSubtotal()
		{
			subtotal = Number(0.0);

			for(i = 0; i < document.getElementsByName('SubtotalTextBox').length; i++)
			{
				onHandValue = Number(replaceChars(document.getElementsByName('OnHandTextBox')[i].value));
				productionValue = Number(replaceChars(document.getElementsByName('ProductionTextBox')[i].value));
				shippedValue = Number(replaceChars(document.getElementsByName('ShippedTextBox')[i].value));
				subtotalValue = Number((onHandValue + productionValue) - shippedValue);

				document.getElementsByName('SubtotalTextBox')[i].value = FormatNumber(subtotalValue,0,false,false,true);
			}
			
			for(i = 0; i < document.getElementsByName('SubtotalTextBox').length; i++)
			{
				subtotal = subtotal + Number(replaceChars(document.getElementsByName('SubtotalTextBox')[i].value));
			}

			for(i = 0; i < document.getElementsByName('GrandTotalSubtotalTextBox').length; i++)
			{
				document.getElementsByName('GrandTotalSubtotalTextBox')[i].value = FormatNumber(subtotal,0,true,false,true);
			}
		}
		
		function UpdateProjected()
		{
			projected = Number(0.0);

			for(i = 0; i < document.getElementsByName('ProjectedTextBox').length; i++)
			{
				toShipValue = Number(replaceChars(document.getElementsByName('ToShipTextBox')[i].value));
				subtotalValue = Number(replaceChars(document.getElementsByName('SubtotalTextBox')[i].value));
				projectedValue = Number(subtotalValue - toShipValue);

				document.getElementsByName('ProjectedTextBox')[i].value = FormatNumber(projectedValue,0,false,false,true);
			}
			
			for(i = 0; i < document.getElementsByName('ProjectedTextBox').length; i++)
			{
				projected = projected + Number(replaceChars(document.getElementsByName('ProjectedTextBox')[i].value));
			}

			for(i = 0; i < document.getElementsByName('GrandTotalProjectedTextBox').length; i++)
			{
				document.getElementsByName('GrandTotalProjectedTextBox')[i].value = FormatNumber(projected,0,true,false,true);
			}
		}
		</script>

		<form id="Form1" method="post" runat="server">
			<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenHeader.html"-->
		    <div class="scroll" id="BodyDiv"> 
				<!--<div class="scrollHeader" id="HeaderDiv" ScrollWithBody="true">-->
				<div style='PADDING-LEFT: 15px;'>
				<table cellspacing="0" cellpadding="0" border="0" class="ReportTable">
					<!--<tr>
						<td height=5>&nbsp;</td>
					</tr>-->
					<tr>
						<td align="center" width="105"><asp:button id="save" EnterAsTab="" tabstop="" runat="server" Text="Save" Width="100"></asp:button></td>
						<td align="center" width="105"><asp:button id="saveExit" EnterAsTab="" tabstop="" runat="server" Text="Save & Exit" Width="100"></asp:button></td>
						<td align="center" width="105"><input type="reset" value="Cancel" style="WIDTH: 100px"/></td>
						<td align="center" width="105"><asp:button id="exit" runat="server" Text="Exit" Width="100" onclick="exit_Click"></asp:button></td>
					</tr>
				</table>
				<!-- Email Start -->
				<table cellspacing="0" cellpadding="0" border="0" class="ReportTable">
					<tr>
						<td class="Header" align="center">Input Daily Inventory</td>
					</tr>
					<tr>
						<td class="BottomBlack" nowrap="nowrap"><%= GetCommodity() %><%= GetWarehouse() %><%= GetStartDate() %><%= GetTime() %></td>
					</tr>
					<tr>
						<td width="100"><font size="2" face="Arial">&nbsp;</font></td>
					</tr>
				</table>
				</div>
				<table class="ReportTable">
					<tr>
					<asp:repeater id="Repeater1" runat="server">
						<ItemTemplate>
							<%# Columns()%>
						</ItemTemplate>
						<FooterTemplate>
								</tr>
							</table>
						</FooterTemplate>
					</asp:repeater>
				<!-- Email End -->
				<img name="renewSession" width="0" height="0"/>
				<!-- #include file="../../../KeyCentralABZ/UI/Include/ScreenFooter.html"-->
                </table>
			</div>
		</form>
	</body>
</html>