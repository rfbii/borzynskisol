using System;
using System.Collections.Generic;
using System.Text;
using KeyCentral.Functions;

namespace FreightConfirmationLib
{
	[MenuMap(Security = "Freight Confirmation Report", Description = "Freight Confirmation", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "/Freight", SortOrder = 10, Url = "../../../FreightConfirmationABZ/UI/Reports/CriteriaFreightConfirmation.aspx?ReportName=FreightConfirmationReport")]
	[MenuMap(Security = "", Description = "Freight", Type = "Menu,Report,Report_NoExcel,Report_BackOnly,Criteria,Input", Menu = "", SortOrder = 5, Url = "../../../KeyCentralABZ/UI/Menu/Menu.aspx?DisplayMenu=/Freight")]

    class Menumap
    {
    }
}
